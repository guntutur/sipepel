<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAP Pertambangan&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Pertambangan</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen BAP</li>
                <li class="active">BAP Pertambangan</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#edit_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                    <!-- <li><a href="#edit" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP via Excel</a></li>                       -->
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div class="tab-pane active" id="edit_new">
                        <div class="row" style="margin-top: 10px;">       
                            <div class="col-xs-3" id="vertical_tab"> <!-- required for floating -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#data_umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                <li><a href="#pencemaran_air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                <li><a href="#pencemaran_b3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                <li><a href="#lain_lain" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Lain-lain</a></li>
                              </ul>
                            </div>
                            <div class="col-xs-9">
                            <!-- Tab panes -->
                                <form role="form" id="add_form" action="<?php echo base_url('backend/bap_tambang/newSaveEdit') ?>" method="post">
                                    <input type="hidden" name="id_bap" value="<?php echo $bap->id_bap; ?>"/>
                                    <input type="hidden" name="id_bap_tambang" value="<?php echo $bap_tambang->id_bap_tambang; ?>"/>
                                    
                                    <input type="hidden" name="id_penc_air" value="<?php echo $pencemaran_air->id_penc_air; ?>"/>
                                    
                                    <input type="hidden" name="id_penc_udara" value="<?php echo $pencemaran_udara->id_penc_udara; ?>"/>
                                    <?php
                                        for($i=0; $i<count($uji_ambien); $i++) {
                                             echo '<input type="hidden" name="id_uji_ambien'.($i+1).'" value="'.$uji_ambien[$i]->id_uji_ambien.'"/>';
                                        } 
                                    ?>

                                    <input type="hidden" name="id_penc_pb3" value="<?php echo $pencemaran_pb3->id_penc_pb3; ?>"/>
                                    <?php
                                        for($i=0; $i<count($jenis_limbah_b3); $i++) {
                                             echo '<input type="hidden" name="id_jlb3'.($i+1).'" value="'.$jenis_limbah_b3[$i]->id_jlb3.'"/>';
                                        }

                                        for($i=0; $i<count($tps_b3); $i++) {
                                             echo '<input type="hidden" name="id_tps_b3'.($i+1).'" value="'.$tps_b3[$i]->id_tps_b3.'"/>';
                                        } 
                                    ?>
                    
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="data_umum">
                                            <h4 class="page-tab">Data Umum</h4>
                                            
                                            <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Tanggal</label>
                                                <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>" >                  
                                            </div>
                                            <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                            <input id="timepick1" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                        </div>
                                    </div>
                                    </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>Petugas Pengawas</label>
                                                    <select class="form-control input-sm" name="id_pegawai">
                                                        <option value=""></option>
                                                        <?php 
                                                            foreach ($pegawai as $p) {
                                                                echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                            }?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Beserta Anggota Pengawas:</label>
                                                    <select data-placeholder="Pilih Pengawas..." class="form-control input-sm multiselect" multiple="multiple" name="id_petugas_bap_list[]" style="border: 0!important;">
                                                        <?php 
                                                            foreach ($pegawai as $p) {
                                                                echo "<option value='$p->id_pegawai' ";
                                                                for ($i=0; $i < count($petugas_bap); $i++) { 
                                                                    if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                                        echo "selected";
                                                                    }
                                                                }
                                                                echo ">$p->nama_pegawai</option>";
                                                            }?>
                                                    </select>
                                                </div>
                                            </div> <br>
                                            <div class="form-group">
                                                <h4>Lokasi Pengawasan / Pembinaan</h4>    
                                                <div class="form-group ">
                                                    <label>Nama Usaha / Kegiatan</label>
                                                    <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                            <option value="Empty">&nbsp;</option>
                                                            <?php
                                                                foreach ($industri as $value) {
                                                                    echo '<option value="'.$value->id_industri.'"';
                                                                        if($bap->id_industri==$value->id_industri){
                                                                            echo 'selected';
                                                                        }
                                                                    echo '>'.$value->nama_industri.'</option>';
                                                                }
                                                            ?>
                                                    </select>
                                                    <!-- <input type="text" title="Nama Usaha/Kegiatan" value="<?php echo $bap->nama_industri?>" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                                            <input type="hidden" id="id_industri" name="id_industri" value="<?php echo $bap->id_industri?>"/> -->
                                                </div>
                                                <!-- <div id="industri-detail2"></div> -->
                                                <div id="container2" style="display:none"></div>
                                            </div> 
                                            <div class="form-group">
                                                <h4>Penanggung Jawab Usaha / Kegiatan</h4>
                                                <div class="form-group ">
                                                    <label>Nama</label>
                                                    <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Telepon</label>
                                                    <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Jabatan</label>
                                                    <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan;?>"/>
                                                </div> 
                                            </div>

                                            <h4 class="page-tab">Data Umum Usaha/Kegiatan</h4>
                                            <div class="form-group ">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" name="jumlah_karyawan" class="form-control input-sm has-feedback" value="<?php echo $bap_tambang->jumlah_karyawan;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jenis Pertambangan</label>
                                                <input type="text" name="jenis_tambang" class="form-control input-sm has-feedback" value="<?php echo $bap_tambang->jenis_tambang;?>" />
                                            </div>
                                            <div class="form-group ">
                                                <label>Izin Usaha</label>
                                                <input type="text" name="izin_usaha" class="form-control input-sm has-feedback" value="<?php echo $bap_tambang->izin_usaha;?>" />
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Dokumen Lingkungan</label>
                                                        <select class="form-control input-sm" name="dok_lingk">
                                                            <option value="1" <?php echo (($bap->dok_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                                                            <option value="0" <?php echo (($bap->dok_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tahun</label>
                                                        <input type="text" name="dok_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun;?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jenis</label>
                                                <!-- <select class="form-control input-sm" name="dok_lingk_jenis"> -->
                                                    <!-- <option value="" <?php // echo (($bap->dok_lingk_jenis== '') ? 'selected' : ''); ?> ></option> -->
                                                    <!-- <option value="Amdal" <?php // echo (($bap->dok_lingk_jenis== 'Amdal') ? 'selected' : ''); ?> >Amdal</option> -->
                                                    <!-- <option value="UKL-UPL" <?php // echo (($bap->dok_lingk_jenis== 'UKL-UPL') ? 'selected' : ''); ?> >UKL-UPL</option> -->
                                                    <!-- <option value="DELH" <?php // echo (($bap->dok_lingk_jenis== 'DELH') ? 'selected' : ''); ?> >DELH</option> -->
                                                    <!-- <option value="DPLH" <?php // echo (($bap->dok_lingk_jenis== 'DPLH') ? 'selected' : ''); ?> >DPLH</option> -->
                                                    <!-- <option value="SPPL" <?php // echo (($bap->dok_lingk_jenis== 'SPPL') ? 'selected' : ''); ?> >SPPL</option> -->
                                                <!-- </select> -->
                                                <select class="form-control input-sm" name="dok_lingk_jenis" id="jdl_name">
                                                    <option value="">-- Pilih salah satu --</option>
                                                    <?php 
                                                        foreach ($jenis_dok_lingkungan as $j) {
                                                            echo "<option value='$j->ket' ".(($bap->dok_lingk_jenis == $j->ket) ? 'selected' : '').">$j->ket</option>";
                                                    }?>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Izin Lingkungan</label>
                                                        <select class="form-control input-sm" name="izin_lingk">
                                                            <option value="1" <?php echo (($bap->izin_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                                                            <option value="0" <?php echo (($bap->izin_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tahun</label>
                                                        <input type="text" name="izin_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun;?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="pencemaran_air">
                                            <h4 class="page-tab">Pengendalian Pencemaran Air</h4>

                                            <label>a. Sumber Air dan Penggunaan</label>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Air Tanah</label>
                                                        <input type="text" name="ambil_air_tanah" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_tanah;?>"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Perizinan</label>
                                                        <select class="form-control input-sm" name="ambil_air_tanah_izin">
                                                            <option value="1" <?php echo (($pencemaran_air->ambil_air_tanah_izin== true) ? 'selected' : ''); ?>>Ada</option>
                                                            <option value="0" <?php echo (($pencemaran_air->ambil_air_tanah_izin== false) ? 'selected' : ''); ?>>Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Air Permukaan</label>
                                                        <input type="text" name="ambil_air_permukaan" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_permukaan;?>"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Perizinan</label>
                                                        <select class="form-control input-sm" name="ambil_air_permukaan_izin">
                                                            <option value="1" <?php echo (($pencemaran_air->ambil_air_permukaan_izin== true) ? 'selected' : ''); ?>>Ada</option>
                                                            <option value="0" <?php echo (($pencemaran_air->ambil_air_permukaan_izin== false) ? 'selected' : ''); ?>>Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>PDAM</label>
                                                <input type="text" name="ambil_air_pdam" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_pdam;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Lain-lain</label>
                                                <input type="text" name="ambil_air_lain" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_lain;?>"/>
                                            </div>

                                            <!-- <div class="form-group ">
                                                <label>b. Sumber Air Limbah</label>
                                                <input type="text" name="limb_sumber" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->limb_sumber;?>"/>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. Badan Air Penerima</label>
                                                <input type="text" name="bdn_terima" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->bdn_terima;?>"/>
                                            </div> -->

                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>b. Sumber Air Limbah</label>
                                                    <!-- <select name="limb_sumber" id="" class="form-control input-sm"> -->
                                                    <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                                    <!-- <option value=''></option> -->
                                                        <?php
                                                            $arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);
                                                            foreach ($limb_sumber as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                for ($i=0; $i < count($arr_limb_sumber) ; $i++) { 
                                                                
                                                                    if ($arr_limb_sumber[$i]==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>c. Badan Air Penerima</label>
                                                    <select name="bdn_terima" id="" class="form-control input-sm">
                                                    <option value=''></option>
                                                        <?php
                                                            foreach ($bdn_terima as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                    if ($pencemaran_air->bdn_terima==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>d. Sarana Pengolahan Air Limbah</label>
                                                <select class="form-control input-sm" name="sarana_olah_limbah">
                                                    <option value="1" <?php echo (($pencemaran_air->sarana_olah_limbah== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->sarana_olah_limbah== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jenis Sarana</label>
                                                <input type="text" name="sarana_jenis" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->sarana_jenis;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Kapasitas</label>
                                                <input type="text" name="sarana_kapasitas" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->sarana_kapasitas;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet S</label>
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_s?>" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_s?>" name="koord_outlet_jam_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">'</span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_s?>" name="koord_outlet_menit_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">"</span>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet E</label>
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_e?>" name="koord_outlet_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_e?>" name="koord_outlet_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">'</span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_e?>" name="koord_outlet_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">"</span>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>e. Lain-lain</label>
                                                <input type="text" name="lain_lain_pa" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->lain_lain;?>"/>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_udara">
                                            <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                            
                                            <div class="form-group ">
                                                <label>a. Data Kualitas Udara Ambien</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th class="text-center">Upwind</th>
                                                            <th class="text-center">Site</th>
                                                            <th class="text-center">Downwind</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>Pengujian Kualitas</label></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas1"><option value="1" <?php echo (($uji_ambien[0]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[0]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas2"><option value="1" <?php echo (($uji_ambien[1]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[1]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas3"><option value="1" <?php echo (($uji_ambien[2]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[2]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                            <td><select class="form-control input-sm" name="period1"><option value="Rutin" <?php echo (($uji_ambien[0]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[0]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period2"><option value="Rutin" <?php echo (($uji_ambien[1]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[1]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period3"><option value="Rutin" <?php echo (($uji_ambien[2]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[2]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Laboratorium Penguji</label></td>
                                                            <?php
                                                                for($i=0; $i<count($uji_ambien); $i++){
                                                                    echo "<td><select name='lab".($i+1)."' class='form-control input-sm '>";
                                                                        foreach ($lab as $l) {
                                                                            echo "<option value='$l->nama_lab' ".(($uji_ambien[$i]->lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                                        }
                                                                    echo "</select></td>";
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Pemenuhan BM</label></td>
                                                            <td><select class="form-control input-sm" name="bm_pemenuhan1"><option value="1" <?php echo (($uji_ambien[0]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[0]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                            <td><select class="form-control input-sm" name="bm_pemenuhan2"><option value="1" <?php echo (($uji_ambien[1]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[1]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                            <td><select class="form-control input-sm" name="bm_pemenuhan3"><option value="1" <?php echo (($uji_ambien[2]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[2]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" name="bm_param1" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[0]->bm_param;?>"/></td>
                                                            <td><input type="text" name="bm_param2" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[1]->bm_param;?>"/></td>
                                                            <td><input type="text" name="bm_param3" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[2]->bm_param;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" name="bm_period1" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[0]->bm_period;?>"/></td>
                                                            <td><input type="text" name="bm_period2" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[1]->bm_period;?>"/></td>
                                                            <td><input type="text" name="bm_period3" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[2]->bm_period;?>"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                    
                                            <div class="form-group ">
                                                <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                    <option value="Rutin" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Rutin') ? 'selected' : ''); ?> >Rutin</option>
                                                    <option value="Tidak Rutin" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Melaporkan') ? 'selected' : ''); ?> >Tidak Melaporkan</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. Lain-lain</label>
                                                <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_udara->lain_lain;?>"/>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_b3">
                                            <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
                                            <div class="form-group ">
                                                <label>a. Jenis Limbah B3 yang ditimbulkan</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Jenis Limbah B3</th>
                                                            <th class="text-center" style="width:15%">Jumlah (ton/hari)</th>
                                                            <th class="text-center">Pengelolaan</th>
                                                            <th class="text-left" style="width:30%">Pihak Ketiga</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        for($x=0;$x<count($jenis_limbah_b3);$x++){ 
                                                        ?>
                                                        <tr data-duplicate="jenis_limbah" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3 --</option>
                                                                    <?php foreach($jenis_limbah as $uk):
                                                                        echo "<option value='".$uk->ket."' ".(($jenis_limbah_b3[$x]->jenis==$uk->ket) ? 'selected' : '').">".$uk->ket."</option>";
                                                                    endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="jumlah[]" class="form-control input-sm has-feedback" value='<?php echo $jenis_limbah_b3[$x]->jumlah; ?>'/></td>
                                                            <td><select class='form-control input-sm' name='pengelolaan[]'><option value="">
                                                            <?php
                                                                 foreach ($bool as $f) {

                                                                     echo "<option value='".kelolaConv($f)."' " ;
                                                                     if (strcasecmp($jenis_limbah_b3[$x]->pengelolaan, kelolaConv($f)) == 0){
                                                                         echo "selected='true'";
                                                                     }
                                                                     echo ">".kelolaConv($f)."</option>";
                                                                 }
                                                                 echo "</select></td>";
                                                                 ?>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:63%">
                                                                        <select name="pihak_ke3[]" class="form-control input-sm has-feedback">
                                                                            <option value=''></option>";
                                                                            <?php 
                                                                                foreach ($pihak_ke3 as $p3) {
                                                                                    echo "<option value='".$p3->badan_hukum." ".$p3->nama_industri."' ".(($jenis_limbah_b3[$x]->pihak_ke3==($p3->badan_hukum." ".$p3->nama_industri)) ? 'selected' : '').">".$p3->badan_hukum." ".$p3->nama_industri."</option>";
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="co-lg-6" style="padding-right: 0;">
                                                                        <div class=" text-right">
                                                                            <div class="btn-group" style="padding-right: 17px;">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="jenis_limbah"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="jenis_limbah"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>


                                            <div class="form-group ">
                                                <label>b. Penyimpanan</label>
                                                <select class="form-control input-sm" name="b3_penyimpanan">
                                                    <option value="Pada TPS Limbah B3">Pada TPS Limbah B3</option>
                                                    <option value="Di luar TPS Limbah B3">Di luar TPS Limbah B3</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. TPS Limbah B3</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">TPS Limbah B3</th>
                                                            <th class="text-center">1</th>
                                                            <th class="text-center">2</th>
                                                            <th class="text-center">3</th>
                                                            <th class="text-center">4</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Perizinan</td>
                                                            <td><select name="tb_izin1" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[0]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[0]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[0]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin2" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[1]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[1]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[1]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin3" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[2]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[2]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[2]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin4" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[3]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[3]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[3]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nomor</td>
                                                            <td><input type="text" name="tb_izin_no1" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[0]->izin_no;?>"/></td>
                                                            <td><input type="text" name="tb_izin_no2" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[1]->izin_no;?>"/></td>
                                                            <td><input type="text" name="tb_izin_no3" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[2]->izin_no;?>"/></td>
                                                            <td><input type="text" name="tb_izin_no4" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[3]->izin_no;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal</td>
                                                            <td><input type="text" name="tb_izin_tgl1" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[0]->izin_tgl;?>"/></td>
                                                            <td><input type="text" name="tb_izin_tgl2" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[1]->izin_tgl;?>"/></td>
                                                            <td><input type="text" name="tb_izin_tgl3" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[2]->izin_tgl;?>"/></td>
                                                            <td><input type="text" name="tb_izin_tgl4" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[3]->izin_tgl;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenis Limbah B3</td>
                                                            <td><input type="text" name="tb_jenis1" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[0]->jenis;?>"/></td>
                                                            <td><input type="text" name="tb_jenis2" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[1]->jenis;?>"/></td>
                                                            <td><input type="text" name="tb_jenis3" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[2]->jenis;?>"/></td>
                                                            <td><input type="text" name="tb_jenis4" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[3]->jenis;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"><label>Lama Penyimpanan</label></td>
                                                        </tr>
                                                        <?php

                                                        $i = 1;
                                                        foreach ($tps_b3 as $tb3) {
                                                            $test = unserialize($tb3->lama_spn);
                                                            foreach ($test['tps'] as $v) {
                                                                $lama[] = $v;
                                                            }
                                                            $new['lama_tps_'.$i] = $lama;
                                                            unset($lama);
                                                            $i++;
                                                        }

                                                        $test = unserialize($tps_b3[0]->lama_spn);

                                                        $n = 0;
                                                        if ($test['jenis_limbah']!=null) {
                                                            foreach ($test['jenis_limbah'] as $key => $value) {
                                                                $f = 1;
                                                                $newest['jenis'] = $value;
                                                                foreach ($new as $x => $y) {
                                                                    $newest['tps_' . $f] = $y[$n];
                                                                    $f++;
                                                                }
                                                                $all_new[] = $newest;

                                                                unset($t);
                                                                $n++;
                                                            }
                                                        }
                                                    ?>
                                                    <?php if(isset($all_new)) {foreach ($all_new as $key => $value){ ?>
                                                            
                                                    <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                                                        <td>
                                                            <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                                                <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                                                <?php foreach($jenis_limbah as $uk):
                                                                    echo "<option value='".$uk->ket."'";
                                                                        if ($value['jenis'] == $uk->ket) echo "selected";
                                                                    echo ">$uk->ket</option>";
                                                                endforeach;?>
                                                                <?php // foreach($jenis_limbah_b3 as $uk): ?>
                                                                    <!-- <option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option> -->
                                                                <?php // endforeach;?>
                                                            </select>
                                                        </td>
                                                        <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" value="<?php echo $value['tps_1']?>" /></td>
                                                        <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" value="<?php echo $value['tps_2']?>" /></td>
                                                        <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" value="<?php echo $value['tps_3']?>" /></td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-lg-6" style="width:60%">
                                                                    <div class="text-left">
                                                                        <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" value="<?php echo $value['tps_4']?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="co-lg-6">
                                                                    <div class=" text-right" >
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php  } } else { ?>
                                                        <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                                                    <?php foreach($jenis_limbah_b3_data_master as $uk): ?>
                                                                        <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                                                                    <?php endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" /></td>
                                                            <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" /></td>
                                                            <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" /></td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:60%">
                                                                        <div class="text-left">
                                                                            <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-lg-6">
                                                                        <div class=" text-right" >
                                                                            <div class="btn-group">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php  } ?>
                                                        <?php
                                                    $x=1;
                                                    foreach($tps_b3 as $jl){
                                                        $koord_derajat_s[$x] = $jl->koord_derajat_s;
                                                        $koord_jam_s[$x] = $jl->koord_jam_s;
                                                        $koord_menit_s[$x] = $jl->koord_menit_s;
                                                        $koord_derajat_e[$x] = $jl->koord_derajat_e;
                                                        $koord_jam_e[$x] = $jl->koord_jam_e;
                                                        $koord_menit_e[$x] = $jl->koord_menit_e;
                                                        $x++;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>Koordinat</td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s1" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[1]?>" placeholder="00&deg;">
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s1" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[1]?>" placeholder="00&#39;">
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s1" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[1]?>" placeholder="00&quot;"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e1" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[1]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e1" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[1]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e1" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[1]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s2" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[2]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s2" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[2]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s2" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[2]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e2" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[2]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e2" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[2]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e2" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[2]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s3" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[3]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s3" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[3]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s3" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[3]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e3" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[3]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e3" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[3]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e3" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[3]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s4" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[4]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s4" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[4]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s4" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[4]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e4" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[4]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e4" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[4]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e4" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[4]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                        <tr>
                                                            <td>Ukuran (m2)</td>
                                                            <td><input type="text" name="tb_ukuran1" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[0]->ukuran;?>"/></td>
                                                            <td><input type="text" name="tb_ukuran2" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[1]->ukuran;?>"/></td>
                                                            <td><input type="text" name="tb_ukuran3" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[2]->ukuran;?>"/></td>
                                                            <td><input type="text" name="tb_ukuran4" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[3]->ukuran;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"><label>Kelengkapan</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Papan Nama dan Koordinat</td>
                                                            <td><select name="tb_ppn_nama_koord1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Simbol dan Label</td>
                                                            <td><select name="tb_simbol_label1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Saluran Ceceran Air Limbah</td>
                                                            <td><select name="tb_saluran_cecer1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bak Penampung Ceceran Air Limbah</td>
                                                            <td><select name="tb_bak_cecer1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kemiringan</td>
                                                            <td><select name="tb_kemiringan1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>SOP Pengelolaan dan Tanggap Darurat</td>
                                                            <td><select name="tb_sop_darurat1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Log Book</td>
                                                            <td><select name="tb_log_book1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>APAR</td>
                                                            <td><select name="tb_apar_p3k1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kotak P3K</td>
                                                            <td><select name="tb_p3k1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group ">
                                                <label>d. Neraca Limbah B3</label>
                                                <select class="form-control input-sm" name="b3_neraca">
                                                    <option value="1" <?php echo (($pencemaran_pb3->b3_neraca == true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_pb3->b3_neraca == false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>e. Manifest</label>
                                                <select class="form-control input-sm" name="b3_manifest">
                                                    <option value="1" <?php echo (($pencemaran_pb3->b3_manifest== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_pb3->b3_manifest== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>f. Pelaporan Neraca dan Manifest</label>
                                                <select class="form-control input-sm" name="b3_lapor_nm">
                                                    <option value="Rutin" <?php echo (($pencemaran_pb3->b3_lapor_nm == 'Rutin') ? 'selected' : ''); ?> >Rutin</option>
                                                    <option value="Tidak Rutin" <?php echo (($pencemaran_pb3->b3_lapor_nm == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan" <?php echo (($pencemaran_pb3->b3_lapor_nm == 'Tidak Melaporkan') ? 'selected' : ''); ?> >Tidak Melaporkan</option>
                                                </select>
                                            </div>
                                            
                                            <?php 
                                                $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                                $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                                $new = array();
                                                for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                                    $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                                }
                                            ?>

                                            <label>g. Limbah Padat Lain</label>

                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Jenis</th>
                                                        <th class="text-center">Jumlah (ton/hari)</th>
                                                        <th class="text-center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($new as $key => $value) { ?>
                                                    <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                        <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                                                        <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                                                        <td class="col-lg-2">
                                                            <div class=" text-right">
                                                                <div class="btn-group" style="padding-right: 17px;">
                                                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="form-group ">
                                                <label> Sarana Pengelolaan</label>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>1. Tong / Bak Sampah</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="padat_sarana_tong" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_sarana_tong;?>"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>2. TPS</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="padat_sarana_tps" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_sarana_tps;?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label> Pengelolaan</label>
                                                <!-- <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                    <option value="Dibakar" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dibakar') ? 'selected' : ''); ?> >Dibakar</option>
                                                    <option value="Diangkut oleh Pihak Ketiga" <?php //echo (($pencemaran_pb3->padat_kelola == 'Diangkut oleh Pihak Ketiga') ? 'selected' : ''); ?> >Diangkut oleh Pihak Ketiga</option>
                                                    <option value="Diangkut oleh Dinas Pertasih" <?php //echo (($pencemaran_pb3->padat_kelola == 'Diangkut oleh Dinas Pertasih') ? 'selected' : ''); ?> >Diangkut oleh Dinas Pertasih</option>
                                                    <option value="Dikelola oleh Masyarakat Sekitar" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dikelola oleh Masyarakat Sekitar') ? 'selected' : ''); ?> >Dikelola oleh Masyarakat Sekitar</option>
                                                    <option value="Dijual" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dijual') ? 'selected' : ''); ?> >Dijual</option>
                                                    <option value="other" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dibakar') || ($pencemaran_pb3->padat_kelola == 'Diangkut oleh Pihak Ketiga') || 
                                                                                       ////($pencemaran_pb3->padat_kelola == 'Diangkut oleh Dinas Pertasih') || ($pencemaran_pb3->padat_kelola == 'Dijual') || 
                                                                                      //// ($pencemaran_pb3->padat_kelola == 'Dikelola oleh Masyarakat Sekitar') ? '' : 'selected'); ?> >lainnya</option>
                                                </select>
                                                <input type="text" name="padat_kelola2" id="pe2" class="form-control input-sm has-feedback" style="display:none" value="<?php //echo $pencemaran_pb3->padat_kelola;?>"/> -->
                                                <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                    <?php 
                                                        $padat_kelola = $pencemaran_pb3->padat_kelola;
                                                        echo $padat_kelola;
                                                            if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola2' id='pe2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                        } else {
                                                            $temp = "Lainnya";
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola2' id='pe2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
                                                        }
                                                ?>
                                            </div>

                                            <div class="form-group ">
                                                <label>h. Lain-lain</label>
                                                <input type="text" name="lain_lain_pb3" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->lain_lain;?>"/>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="lain_lain">
                                            <h4 class="page-tab">Lain - Lain</h4>
                                            <div class="form-group ">
                                                <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"><?php echo $bap->catatan;?></textarea>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_tambang'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        
                        </div>
                        <!-- /.row -->
                    </div>

                    <div class="tab-pane " id="edit">

                        <div class="row">
                            <div class="modal-body">  
                                <div class="col-sm-12">
                                    <div class="col-sm-6 text-right">
                                        <h4 class="light">Form BAP Pertambangan</h4>
                                    </div>
                                    <div class="col-sm-6 text-left">
                                        <form action="<?php echo base_url('/backend/bap_tambang/getEditForm'); ?>" method="post">
                                            <input type="hidden" name="id_bap" value="<?php echo $bap->id_bap; ?>"/>
                                            <button type="submit" class="btn btn-primary btn-rounded-lg">Unduh Form BAP</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form role="form" action="<?php echo base_url('/backend/bap_tambang/saveEdit') ?>" method="post" id="form_add" enctype="multipart/form-data">
                                <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Tanggal</label>
                                    <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>" >                  
                                </div>
                                <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                            <input id="timepick2" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                        </div>
                                    </div>
                                    </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label>Petugas Pengawas</label>
                                        <select class="form-control input-sm" name="id_pegawai">
                                            <option value=""></option>
                                            <?php 
                                                foreach ($pegawai as $p) {
                                                    echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Beserta Anggota Pengawas:</label>
                                        <select data-placeholder="Pilih Pengawas..." class="form-control input-sm multiselect" multiple="multiple" name="id_petugas_bap_list[]" style="border: 0!important;">
                                            <?php 
                                                foreach ($pegawai as $p) {
                                                    echo "<option value='$p->id_pegawai' ";
                                                    for ($i=0; $i < count($petugas_bap); $i++) { 
                                                        if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                            echo "selected";
                                                        }
                                                    }
                                                    echo ">$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                </div>
                                <fieldset>
                                    <legend>Lokasi Pengawasan / Pembinaan</legend>    
                                    <div class="form-group ">
                                        <label>Nama Usaha / Kegiatan</label>
                                        <select class="form-control input-sm" name="id_industri" onchange="$(this).get_detail_industri(this, '#industri-detail1');">
                                        <option value=''></option>
                                            <?php 
                                                foreach ($industri as $i) {
                                                    echo "<option value='$i->id_industri' ".(($bap->id_industri== $i->id_industri) ? 'selected' : '').">$i->nama_industri</option>";
                                                }
                                                ?>
                                        </select>
                                    </div>
                                    <div id="industri-detail1"></div>
                                </fieldset> 
                                <fieldset>
                                    <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                    <div class="form-group ">
                                        <label>Nama</label>
                                        <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama;?>"/>
                                    </div>
                                    <div class="form-group ">
                                        <label>Telepon</label>
                                        <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp;?>"/>
                                    </div>
                                    <div class="form-group ">
                                        <label>Jabatan</label>
                                        <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan;?>"/>
                                    </div> 
                                </fieldset>
                                        <div class="form-group">
                                            <h4 class="light">Upload Form BAP Pertambangan</h4>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" readonly="">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file">
                                                        Browse… <input type="file" name="file" accept=".xlsx">
                                                    </span>
                                                </span>
                                            </div>
                                        </div>

                                <div class="form-group "> 
                                    <input type="hidden" name="id_bap" value="<?php echo $bap->id_bap;?>" />

                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_tambang'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                </div>

                            </form> 
                        </div>                                        
                    </div>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>

                
              <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                <br/><br/> -->

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->

            <?php
            function kelolaConv($value) {
                        if ($value == '1') {
                            return "Dikelola";
                        } else {
                            return "Tidak Dikelola";
                        }
                    }
            ?>
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->

<script type="text/javascript">
    $(document).ready(function () {

        $("select[name=id_industri]").change(function(){
                $(this).get_detail_industri(this, '#container2');
            });

        $('#si').change(function() {
            if($(this).val()  == 'other') {
                $('#si2').show();
                $('#si2').focus();
            } else {
                $('#si2').hide();
            }
        });

        $('#je').change(function() {
            if($(this).val()  == 'other') {
                $('#je2').show();
                $('#je2').focus();
            } else {
                $('#je2').hide();
            }
        });

        $('#pp').change(function() {
            if($(this).val()  == 'other') {
                $('#pp2').show();
                $('#pp2').focus();
            } else {
                $('#pp2').hide();
            }
        });

        $('#bocor').change(function() {
            if($(this).val()  == 'true') {
                $('#lok').show();
                $('#lok').focus();
            } else {
                $('#lok').hide();
            }
        });

        $('#pe').change(function() {
            if($(this).val()  == 'Lainnya') {
                $('#pe2').show();
                $('#pe2').focus();
            } else {
                $('#pe2').hide();
            }
        });

    });
</script>