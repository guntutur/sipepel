<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">RAPORT INDUSTRI&nbsp;&nbsp;<small>menyediakan raport industri untuk masing - masing item ketaatan</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Rekapitulas</li>
                <li class="active">Raport Tahunan Industri</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <ol class="breadcrumb default square rsaquo sm">
                <div class="row">
                    <div class="col-lg-6"> 
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Tahun Penilaian Industri :</label>
                            </div>
                            <div class="col-lg-6">
                                <select class="form-control input-sm" id="tahun">
                                    <option value="all" selected>Semua Tahun</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2014</option>
                                    <option value="2013">2013</option>
                                    <option value="2012">2012</option>
                                    <option value="2011">2011</option>
                                    <option value="2010">2010</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </ol>
            
            <div class="the-box ">
                
                <?php echo $this->session->flashdata('msg'); ?>
                <br/>

                 <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="10%">No.</th>
                                <th width="30%">Tahun</th>
                                <th width="30%">Nama Industri</th>
                                <th class="text-center">Raport</th>
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
                <!-- END DATA TABLE -->
                <div id="loading" style="display:none;"><div style="background-image: url('<?php echo site_url(); ?>assets/img/loading.gif');background-position: center center;background-repeat: no-repeat;height:50px;"></div></div>

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var base_url = '<?php echo site_url(); ?>';
    
    var Document = {
        param: {
            dataperpage: 10,
            bap_type: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/raport_industri/get_rekap_raport_industri"); ?>',
        search: function() {
            this.param.tahun = $('#tahun').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                beforeSend: function() { $('#loading').show(); },
                complete: function() { $('#loading').hide(); },
                success: function(result) {
                    console.log(result);
                    $('#pagination').html(result.pagination);
                    $('#start').text(result.total);                 
                    $('#nums').text(result.total);
                    Document.param.numpage = result.numpage;

                    var h = '', ind = {}, no = Document.param.curpage * Document.param.dataperpage;
                     
                    if (result.bap_data.length < 1) {
                        h += '<tr><td colspan=\'7\'>Data Tidak Ditemukan</td></tr>';
                    } else {
                        for (var i = 0; i < result.bap_data.length; i++) {
                            ind = result.bap_data[i];

                            h += '<tr>';
                            h += '<td>'+(no+=1)+'</td>';
                            h += '<td>'+ind.tahun+'</td>';
                            h += '<td>'+ ind.nama_industri +'</td>';
                           
                            h += '<td class="text-center">';
                            h += '<div class="btn-group">';
                            h += '<form action="'+ base_url + 'backend/raport_industri/generate_raport" method="post"><input type="hidden" name="id_bap" value="'+ ind.bap_terakhir +'">';
                            h += '<button class="btn btn-xs btn-warning" type="submit"><i class="fa fa-download"></i>&nbsp;Unduh</button>';
                            h += '</form>';
                            h += '</div>';
                            h += '</td>';

                            h += '</tr>'
                        }
                    }

                    $('#document-data').html(h); 
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        
        $('#tahun').change(function() {
            Document.search();
        });

        $.fn.confirm_bap = function (id) {
            $('input[id=id_bap_close]').val(id);
            $('#mdl_konfirm').modal('show');
        }
        
        Document.search();

        window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

    });
</script>