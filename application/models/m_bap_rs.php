<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// <!-- Document   : m_bap_rs.php  -->
// <!-- Created on : November 13, 2014 12:15  -->
// <!-- Author     : huda.jtk09@gmail.com  -->
// <!-- Description: Model for BAP Rumah Sakit  -->

class m_bap_rs extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * Get all BAP
	 *
	 * @return array
	 */
	function get_all() {
		$data = array();

		$query = $this->db->query("SELECT a.id_bap, c.nama_industri, b.bap_tgl, b.compare_status, b.is_compared, (SELECT COUNT(*) FROM history_item_teguran_bap d WHERE d.id_bap = a.id_bap) AS jml_teguran
									FROM bap_rs a
									LEFT JOIN bap b ON a.id_bap = b.id_bap
									INNER JOIN industri c ON b.id_industri = c.id_industri
									GROUP BY a.id_bap
									ORDER BY a.id_bap_rs ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}
	
}
?>