<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_bap_rm extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * Get all BAP
	 *
	 * @return array
	 */
	function get_all() {
		$data = array();

		$query = $this->db->query("SELECT a.id_bap, c.nama_industri, b.bap_tgl, b.compare_status, b.is_compared, COUNT(d.id_bap) AS jml_teguran
									FROM bap_rm a
									LEFT JOIN bap b ON a.id_bap = b.id_bap
									LEFT JOIN history_item_teguran_bap d ON b.id_bap = d.id_bap
									INNER JOIN industri c ON b.id_industri = c.id_industri
									GROUP BY b.id_bap
									ORDER BY a.id_bap_rm ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function get_bakumutu($where) {
		$data = array();
		
		$this->db->select("a.id_parameter_bap, a.ket as nama_parameter, a.nilai_parameter_bap, a.dasar_hukum, a.status");
		$this->db->from("parameter_bap a");
		$this->db->join('jenis_bap b', 'a.id_jenis_bap = b.id_jenis_bap', 'INNER');
		$this->db->where($where);
		$this->db->order_by('nama_parameter', 'ASC');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function get_bap_rm_by_id($id) {
		$data = array();

		$query = $this->db->query("SELECT id_history_teguran_bap, ket_parameter_bap as ket, nilai_parameter_bap as nilai_standard, nilai_parameter
									FROM history_item_teguran_bap
									WHERE id_bap = '$id'
									ORDER BY ket ASC");

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function delete_item_ketaatan($id_bap) {
		$query = $this->db->query("DELETE FROM history_item_teguran_bap where id_bap = '$id_bap'");

		if ($query > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
}
?>