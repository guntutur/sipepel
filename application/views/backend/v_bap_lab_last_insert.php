<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAP Laboratorium&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Laboratorium</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen BAP</li>
                <li class="active">BAP Laboratorium</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#edit_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">

                    <div class="tab-pane active" id="edit_new">
                        <div class="row" style="margin-top: 10px;">       
                            <div class="col-xs-3" id="vertical_tab"> <!-- required for floating -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#data_umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                <li><a href="#pencemaran_air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                <li><a href="#pencemaran_b3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                <li><a href="#lain_lain" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Lain-lain</a></li>
                              </ul>
                            </div>
                            <div class="col-xs-9">
                            <!-- Tab panes -->
                                <form role="form" id="add_form" action="<?php echo base_url('backend/bap_lab/newInsert') ?>" method="post">
                    
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="data_umum">
                                            <h4 class="page-tab">Data Umum</h4>
                                            
                                            <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Tanggal</label>
                                                <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>" >                  
                                            </div>
                                            <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                            <input id="timepick1" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                        </div>
                                    </div>
                                    </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>Petugas Pengawas</label>
                                                    <select class="form-control input-sm" name="id_pegawai">
                                                        <option value=""></option>
                                                        <?php 
                                                            foreach ($pegawai as $p) {
                                                                echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                            }?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Beserta Anggota Pengawas:</label>
                                                    <select data-placeholder="Pilih Pengawas..." class="form-control input-sm multiselect" multiple="multiple" name="id_petugas_bap_list[]" style="border: 0!important;">
                                                        <?php 
                                                            foreach ($pegawai as $p) {
                                                                echo "<option value='$p->id_pegawai' ";
                                                                for ($i=0; $i < count($petugas_bap); $i++) { 
                                                                    if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                                        echo "selected";
                                                                    }
                                                                }
                                                                echo ">$p->nama_pegawai</option>";
                                                            }?>
                                                    </select>
                                                </div>
                                            </div> <br>
                                            <div class="form-group">
                                                <h4>Lokasi Pengawasan / Pembinaan</h4>    
                                                <div class="form-group ">
                                                    <label>Nama Usaha / Kegiatan</label>
                                                    <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                        <option value="Empty">&nbsp;</option>
                                                        <?php
                                                            foreach ($industri as $value) {
                                                                echo '<option value="'.$value->id_industri.'"';
                                                                    if($bap->id_industri==$value->id_industri){
                                                                        echo 'selected';
                                                                    }
                                                                echo '>'.$value->nama_industri.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                    <!-- <input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                                    <input type="hidden" id="id_industri" name="id_industri" value="0"/> -->
                                                </div>
                                                <div id="container2" style="display:none"></div>
                                            </div> 
                                            <div class="form-group">
                                                <h4>Penanggung Jawab Usaha / Kegiatan</h4>
                                                <div class="form-group ">
                                                    <label>Nama</label>
                                                    <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Telepon</label>
                                                    <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Jabatan</label>
                                                    <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan;?>"/>
                                                </div> 
                                            </div>

                                            <h4 class="page-tab">Data Umum Usaha/Kegiatan</h4>
                                            <div class="form-group ">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" name="jumlah_karyawan" class="form-control input-sm has-feedback" value="<?php echo $bap_lab->jumlah_karyawan;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Status Akreditasi</label>
                                                <select class="form-control input-sm" name="status_akreditasi">
                                                    <option value="1" <?php echo (($bap_lab->status_akreditasi== true) ? 'selected' : ''); ?> >Ya</option>
                                                    <option value="0" <?php echo (($bap_lab->status_akreditasi== false) ? 'selected' : ''); ?> >Tidak</option>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jumlah Sampel</label>
                                                <input type="text" name="jml_sampel" class="form-control input-sm has-feedback" value="<?php echo $bap_lab->jml_sampel;?>" />
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Dokumen Lingkungan</label>
                                                        <select class="form-control input-sm" name="dok_lingk">
                                                            <option value="1" <?php echo (($bap->dok_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                                                            <option value="0" <?php echo (($bap->dok_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tahun</label>
                                                        <input type="text" name="dok_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun;?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jenis</label>
                                                <select class="form-control input-sm" name="dok_lingk_jenis">
                                                    <option value=""></option>
                                                    <?php 
                                                        foreach ($jenis_dok_lingkungan as $j) {
                                                            echo "<option value='$j->ket' ".(($bap->dok_lingk_jenis == $j->ket) ? 'selected' : '').">$j->ket</option>";
                                                    }?>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Izin Lingkungan</label>
                                                        <select class="form-control input-sm" name="izin_lingk">
                                                            <option value="1" <?php echo (($bap->izin_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                                                            <option value="0" <?php echo (($bap->izin_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tahun</label>
                                                        <input type="text" name="izin_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun;?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="pencemaran_air">
                                            <h4 class="page-tab">Pengendalian Pencemaran Air</h4>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group ">
                                                        <label>a. Sumber Air Limbah</label>
                                                        <select name="limb_sumber[]" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                                            <!-- <option value=''></option> -->
                                                            <?php
                                                                $arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);
                                                                foreach ($limb_sumber as $l) {
                                                                    echo "<option value='$l->ket' ";
                                                                    for ($i=0; $i < count($arr_limb_sumber) ; $i++) { 
                                                                    
                                                                        if ($arr_limb_sumber[$i]==$l->ket){
                                                                            echo 'selected';
                                                                        }
                                                                    }
                                                                    echo ">$l->ket</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group ">
                                                        <label>b. Badan Air Penerima</label>
                                                        <select name="bdn_terima" class="form-control input-sm">
                                                            <option value=''></option>
                                                            <?php
                                                                foreach ($bdn_terima as $l) {
                                                                    echo "<option value='$l->ket' ";
                                                                        if ($pencemaran_air->bdn_terima==$l->ket){
                                                                            echo 'selected';
                                                                        }
                                                                    echo ">$l->ket</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>c. IPAL</label>
                                                <select class="form-control input-sm" name="ipal" id="ipal">
                                                    <option value="1" <?php echo (($pencemaran_air->ipal== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->ipal== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="row" id="detail_ipal">
                                                <div class="col-lg-4">
                                                    <div class="form-group ">
                                                        <label>Sistem IPAL</label>
                                                        <select class="form-control input-sm" name="ipal_sistem" id="si">
                                                            <?php         
                                                                $si = $pencemaran_air->ipal_sistem;
                                                                if (($si === "F") || ($si === "F-B") || ($si === "F-B-K") || ($si === "F-K") || ($si === "F-K-B")) {
                                                                    foreach ($sistem_ipal as $p) {
                                                                        echo "<option value='$p->ket' ".(($si == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                                    }
                                                                    echo "<option value='other'>lainnya</option>";
                                                                    echo "</select>";
                                                                    echo "<input type='text' name='ipal_sistem2' id='si2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                                } else {
                                                                    
                                                                    foreach ($sistem_ipal as $p) {
                                                                        echo "<option value='$p->ket'>$p->ket</option>";
                                                                    }
                                                                    echo "<option value='other' selected>lainnya</option>";
                                                                    echo "</select>";
                                                                    echo "<input type='text' name='ipal_sistem2' id='si2' value='$si' class='form-control input-sm has-feedback'/>";
                                                                }
                                                            ?>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group ">
                                                        <label>Unit IPAL</label>
                                                        <select name="ipal_unit[]" id="ipal_unit" class="form-control input-sm has-feedback multiselect" multiple="multiple">
                                                            <?php
                                                                $arr_ipal_unit = explode(', ', $pencemaran_air->ipal_unit);
                                                                foreach ($ipal_unit as $iu) {
                                                                    echo "<option value='$iu->ket' " ;
                                                                    for ($i=0; $i < count($arr_ipal_unit); $i++) { 
                                                                    
                                                                        if ($arr_ipal_unit[$i]==$iu->ket){
                                                                            echo "selected='true'";
                                                                        }
                                                                    }
                                                                    echo ">$iu->ket</option>";
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group ">
                                                        <label>Kapasitas IPAL</label>
                                                        <input type="text" name="ipal_kapasitas" id="ipal_kapasitas" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ipal_kapasitas;?>"/>
                                                    </div>        
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>d. Perizinan</label>
                                                <select class="form-control input-sm" name="izin" id="izin">
                                                    <option value="1" <?php echo (($pencemaran_air->izin== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->izin== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="form-group " id="detail_izin">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Nomor</label>
                                                        <input type="text" name="izin_no" id="izin_no" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->izin_no;?>"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tanggal</label>
                                                        <input type="text" name="izin_tgl" id="izin_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $pencemaran_air->izin_tgl;?>">    
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>Debit Izin (m3/hari)</label>
                                                <input type="text" name="izin_debit" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->izin_debit;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet S</label>
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_s?>" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_s?>" name="koord_outlet_jam_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">'</span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_s?>" name="koord_outlet_menit_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">"</span>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet E</label>
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_e?>" name="koord_outlet_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_e?>" name="koord_outlet_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">'</span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_e?>" name="koord_outlet_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">"</span>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>e. Alat Ukur</label>
                                                <select class="form-control input-sm" name="au" id="au">
                                                    <option value="1" <?php echo (($pencemaran_air->au== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->au== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="form-group" id="detail_au" name="detail_au">
                                                <div class="form-group ">
                                                    <label>Jenis</label>
                                                    <select class="form-control input-sm" id="je" name="au_jenis">
                                                            <?php         
                                                                $auj = $pencemaran_air->au_jenis;
                                                                echo $auj;
                                                                    if (($auj == "Kumulatif") || ($auj == "V-notch") || ($auj == "Digital")) {
                                                                    foreach ($jenis_alat_ukur as $p) {
                                                                        echo "<option value='$p' ".(($auj == $p) ? 'selected' : '').">$p</option>";
                                                                    }
                                                            echo "</select>";
                                                            echo "<input type='text' name='au_jenis2' id='je2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                                } else {
                                                                    $temp = "other";
                                                                    foreach ($jenis_alat_ukur as $p) {
                                                                        echo "<option value='$p' ".(($temp == $p) ? 'selected' : '').">$p</option>";
                                                                    }
                                                            echo "</select>";
                                                            echo "<input type='text' name='au_jenis2' id='je2' value='$auj' class='form-control input-sm has-feedback'/>";
                                                                }
                                                        ?>
                                                </div>    
                                                <div class="form-group ">
                                                    <label>Ukuran (inchi)</label>
                                                    <input type="text" name="au_ukuran" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->au_ukuran;?>"/>
                                                </div>    
                                                <div class="form-group ">
                                                    <label>Kondisi (apabila ada)</label>
                                                    <select class="form-control input-sm" name="au_kondisi">
                                                        <option value="Berfungsi" <?php echo (($pencemaran_air->au_kondisi== 'Berfungsi') ? 'selected' : ''); ?>>Berfungsi</option>
                                                        <option value="Tidak Berfungsi" <?php echo (($pencemaran_air->au_kondisi== 'Tidak Berfungsi') ? 'selected' : ''); ?>>Tidak Berfungsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>f. Catatan debit harian</label>
                                                <select class="form-control input-sm" name="cat_deb_hari">
                                                    <option value="1" <?php echo (($pencemaran_air->cat_deb_hari== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->cat_deb_hari== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>g. Daur Ulang</label>
                                                <select class="form-control input-sm" name="daur_ulang">
                                                    <option value="1" <?php echo (($pencemaran_air->daur_ulang== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->daur_ulang== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Debit (m3/hari)</label>
                                                <input type="text" name="daur_ulang_debit" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->daur_ulang_debit;?>"/>
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>h. Pengujian Kualitas Air Limbah</label>
                                                <select class="form-control input-sm" name="uji_limbah" id="uji_limbah">
                                                    <option value="1" <?php echo (($pencemaran_air->uji_limbah== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->uji_limbah== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>  
                                            <div class="form-group" name="detail_uji" id="detail_uji">  
                                                <div class="form-group ">
                                                    <label>Periode Pengujian</label>
                                                    <select class="form-control input-sm" name="uji_period" id="pp">
                                                        <?php if ($pencemaran_air->uji_period == 'Setiap Bulan') { ?>
                                                        <option value="Setiap Bulan" selected>Setiap Bulan</option>
                                                        <option value="other" >lainnya</option>
                                                        <input type="text" name="uji_period2" id="pp2" class="form-control input-sm has-feedback" style="display:none;" value=""/>
                                                        <?php } else { ?>
                                                        <option value="Setiap Bulan">Setiap Bulan</option>
                                                        <option value="other" selected >lainnya</option>
                                                        <input type="text" name="uji_period2" id="pp2" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->uji_period?>"/>
                                                        <?php } ?>
                                                    </select>
                                                    <input type="text" name="uji_period2" id="pp2" class="form-control input-sm has-feedback" style="display:none;" value="<?php echo $pencemaran_air->uji_period;?>"/>
                                                </div>    
                                                <div class="form-group ">
                                                    <label>Laboratorium Pengujian</label>
                                                    <select name="uji_lab" class="form-control input-sm">
                                                        <?php
                                                            foreach ($lab as $l) {
                                                                echo "<option value='$l->nama_lab' ".(($pencemaran_air->uji_lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>    
                                                <div class="form-group ">
                                                    <label>Hasil Pengujian</label>
                                                    <select class="form-control input-sm" name="uji_hasil" id="uji_hasil">
                                                        <option value="Memenuhi" <?php echo (($pencemaran_air->uji_period== 'Memenuhi') ? 'selected' : ''); ?>>Memenuhi</option>
                                                        <option value="Tidak Memenuhi Baku Mutu" <?php echo (($pencemaran_air->uji_period== 'Tidak Memenuhi Baku Mutu') ? 'selected' : ''); ?>>Tidak Memenuhi Baku Mutu</option>
                                                    </select>
                                                </div>    
                                            </div>

                                            <div class="form-group ">
                                                <label>Apabila tidak memenuhi baku mutu, terjadi pada bulan</label>
                                                <input type="text" name="uji_tidak_bulan" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->uji_tidak_bulan;?>"/>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Parameter yang tidak memenuhi baku mutu</label>
                                                <input type="text" name="uji_tidak_param" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->uji_tidak_param;?>"/>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Pelaporan</label>
                                                <select class="form-control input-sm" name="uji_lapor">
                                                    <option value="Rutin" <?php echo (($pencemaran_air->uji_lapor== 'Rutin') ? 'selected' : ''); ?>>Rutin</option>
                                                    <option value="Tidak Rutin" <?php echo (($pencemaran_air->uji_lapor== 'Tidak Rutin') ? 'selected' : ''); ?>>Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan" <?php echo (($pencemaran_air->uji_lapor== 'Tidak Melaporkan') ? 'selected' : ''); ?>>Tidak Melaporkan</option>
                                                </select>
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>i. Bahan Kimia Pembantu IPAL</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Nama Bahan Kimia</th>
                                                            <th class="text-center">Jumlah Pemakaian (ton/hari)</th>
                                                            <th class="text-center"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $x=1;
                                                        foreach($bahan_kimia_ipal as $bki){ ?>
                                                            <tr data-duplicate="kimia-ipal" data-duplicate-min="1">
                                                                <td class="col-lg-5"><input type="text" name="bkpi_nama[]" value="<?php echo $bki->nama?>" class="form-control input-sm has-feedback" /></td></td>
                                                                <td class="col-lg-5"><input type="text" name="bkpi_jml[]" value="<?php echo $bki->jml_pemakaian?>" class="form-control input-sm has-feedback" /></td>
                                                                <td class="col-lg-2">
                                                                    <div class=" text-right">
                                                                        <div class="btn-group" style="padding-right: 17px;">
                                                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="kimia-ipal"><i class="fa fa-plus-circle"></i></button>
                                                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="kimia-ipal"><i class="fa fa-times-circle"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- echo "<input type='hidden' name='id_bahan_kimia_ipal$x' value='".$bki->id_bahan_kimia_ipal."'>";
                                                            echo "<tr><td><input type='text' name='bkpi_nama$x' class='form-control input-sm has-feedback ' value='".$bki->nama."'/></td><td><input type='text' name='bkpi_jml$x' class='form-control input-sm has-feedback ' value='".$bki->jml_pemakaian."' /></td></tr>"; -->

                                                            <?php $x++;
                                                        }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group ">
                                                <label>j. Kebocoran/Rembesan/Tumpahan/Bypass</label>
                                                <select class="form-control input-sm" name="bocor" id="bocor">
                                                    <option value="1" <?php echo (($pencemaran_air->bocor== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_air->bocor== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>    
                                            <div class="form-group " id="lok" style="<?php echo (($pencemaran_air->bocor== true) ? '' : 'display:none'); ?>">
                                                <label>Lokasi</label>
                                                <input type="text" name="bocor_lokasi" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->bocor_lokasi;?>">
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>k. Lain-lain</label>
                                                <input type="text" name="lain_lain_pa" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->lain_lain;?>"/>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_udara">
                                            <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                            
                                            <div class="form-group ">
                                                <label>a. Data Kualitas Udara Ambien</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th class="text-center">Upwind</th>
                                                            <th class="text-center">Site</th>
                                                            <th class="text-center">Downwind</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>Pengujian Kualitas</label></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas1"><option value="1" <?php echo (($uji_ambien[0]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[0]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas2"><option value="1" <?php echo (($uji_ambien[1]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[1]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas3"><option value="1" <?php echo (($uji_ambien[2]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[2]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                            <td><select class="form-control input-sm" name="period1"><option value="Rutin" <?php echo (($uji_ambien[0]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[0]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period2"><option value="Rutin" <?php echo (($uji_ambien[1]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[1]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period3"><option value="Rutin" <?php echo (($uji_ambien[2]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[2]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Laboratorium Penguji</label></td>
                                                            <?php
                                                                for($i=0; $i<count($uji_ambien); $i++){
                                                                    echo "<td><select name='lab".($i+1)."' class='form-control input-sm '>";
                                                                        foreach ($lab as $l) {
                                                                            echo "<option value='$l->nama_lab' ".(($uji_ambien[$i]->lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                                        }
                                                                    echo "</select></td>";
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Pemenuhan BM</label></td>
                                                            <td><select class="form-control input-sm" name="bm_pemenuhan1"><option value="1" <?php echo (($uji_ambien[0]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[0]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                            <td><select class="form-control input-sm" name="bm_pemenuhan2"><option value="1" <?php echo (($uji_ambien[1]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[1]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                            <td><select class="form-control input-sm" name="bm_pemenuhan3"><option value="1" <?php echo (($uji_ambien[2]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[2]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" name="bm_param1" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[0]->bm_param;?>"/></td>
                                                            <td><input type="text" name="bm_param2" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[1]->bm_param;?>"/></td>
                                                            <td><input type="text" name="bm_param3" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[2]->bm_param;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" name="bm_period1" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[0]->bm_period;?>"/></td>
                                                            <td><input type="text" name="bm_period2" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[1]->bm_period;?>"/></td>
                                                            <td><input type="text" name="bm_period3" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[2]->bm_period;?>"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                    
                                            <div class="form-group ">
                                                <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                    <option value="Rutin" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Rutin') ? 'selected' : ''); ?> >Rutin</option>
                                                    <option value="Tidak Rutin" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Melaporkan') ? 'selected' : ''); ?> >Tidak Melaporkan</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. Lain-lain</label>
                                                <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_udara->lain_lain;?>"/>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_b3">
                                            <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
                                            <div class="form-group ">
                                                <label>a. Jenis Limbah B3 yang ditimbulkan</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Jenis Limbah B3</th>
                                                            <th class="text-center" style="width:15%">Jumlah (ton/hari)</th>
                                                            <th class="text-center">Pengelolaan</th>
                                                            <th class="text-left" style="width:30%">Pihak Ketiga</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        for($x=0;$x<count($jenis_limbah_b3);$x++){ 
                                                        ?>
                                                        <tr data-duplicate="jenis_limbah" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3 --</option>
                                                                    <?php foreach($jenis_limbah_b3_data_master as $uk):
                                                                        echo "<option value='".$uk->ket."' ".(($jenis_limbah_b3[$x]->jenis==$uk->ket) ? 'selected' : '').">".$uk->ket."</option>";
                                                                    endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="jumlah[]" class="form-control input-sm has-feedback" value='<?php echo $jenis_limbah_b3[$x]->jumlah; ?>'/></td>
                                                            <td><select class='form-control input-sm' name='pengelolaan[]'><option value="">
                                                            <?php
                                                                 foreach ($bool as $f) {

                                                                     echo "<option value='".kelolaConv($f)."' " ;
                                                                     if (strcasecmp($jenis_limbah_b3[$x]->pengelolaan, kelolaConv($f)) == 0){
                                                                         echo "selected='true'";
                                                                     }
                                                                     echo ">".kelolaConv($f)."</option>";
                                                                 }
                                                                 echo "</select></td>";
                                                                 ?>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:63%">
                                                                        <select name="pihak_ke3[]" class="form-control input-sm has-feedback">
                                                                            <option value=''></option>";
                                                                            <?php 
                                                                                foreach ($pihak_ke3 as $p3) {
                                                                                    echo "<option value='".$p3->badan_hukum." ".$p3->nama_industri."' ".(($jenis_limbah_b3[$x]->pihak_ke3==($p3->badan_hukum." ".$p3->nama_industri)) ? 'selected' : '').">".$p3->badan_hukum." ".$p3->nama_industri."</option>";
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="co-lg-6" style="padding-right: 0;">
                                                                        <div class=" text-right">
                                                                            <div class="btn-group" style="padding-right: 17px;">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="jenis_limbah"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="jenis_limbah"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>


                                            <div class="form-group ">
                                                <label>b. Penyimpanan</label>
                                                <select class="form-control input-sm" name="b3_penyimpanan">
                                                    <option value="Pada TPS Limbah B3">Pada TPS Limbah B3</option>
                                                    <option value="Di luar TPS Limbah B3">Di luar TPS Limbah B3</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. TPS Limbah B3</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">TPS Limbah B3</th>
                                                            <th class="text-center">1</th>
                                                            <th class="text-center">2</th>
                                                            <th class="text-center">3</th>
                                                            <th class="text-center">4</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Perizinan</td>
                                                            <td><select name="tb_izin1" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[0]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[0]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[0]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin2" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[1]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[1]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[1]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin3" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[2]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[2]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[2]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin4" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[3]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[3]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[3]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nomor</td>
                                                            <td><input type="text" name="tb_izin_no1" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[0]->izin_no;?>"/></td>
                                                            <td><input type="text" name="tb_izin_no2" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[1]->izin_no;?>"/></td>
                                                            <td><input type="text" name="tb_izin_no3" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[2]->izin_no;?>"/></td>
                                                            <td><input type="text" name="tb_izin_no4" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[3]->izin_no;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal</td>
                                                            <td><input type="text" name="tb_izin_tgl1" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[0]->izin_tgl;?>"/></td>
                                                            <td><input type="text" name="tb_izin_tgl2" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[1]->izin_tgl;?>"/></td>
                                                            <td><input type="text" name="tb_izin_tgl3" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[2]->izin_tgl;?>"/></td>
                                                            <td><input type="text" name="tb_izin_tgl4" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $tps_b3[3]->izin_tgl;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenis Limbah B3</td>
                                                            <td><input type="text" name="tb_jenis1" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[0]->jenis;?>"/></td>
                                                            <td><input type="text" name="tb_jenis2" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[1]->jenis;?>"/></td>
                                                            <td><input type="text" name="tb_jenis3" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[2]->jenis;?>"/></td>
                                                            <td><input type="text" name="tb_jenis4" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[3]->jenis;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"><label>Lama Penyimpanan</label></yd>
                                                        </tr>
                                                        <?php

                                                        $i = 1;
                                                        foreach ($tps_b3 as $tb3) {
                                                            $test = unserialize($tb3->lama_spn);
                                                            foreach ($test['tps'] as $v) {
                                                                $lama[] = $v;
                                                            }
                                                            $new['lama_tps_'.$i] = $lama;
                                                            unset($lama);
                                                            $i++;
                                                        }

                                                        $test = unserialize($tps_b3[0]->lama_spn);

                                                        $n = 0;
                                                        foreach ($test['jenis_limbah'] as $key => $value) {
                                                            $f = 1;
                                                            $newest['jenis'] = $value;
                                                            foreach ($new as $x => $y) {
                                                                $newest['tps_'.$f] = $y[$n];
                                                                $f++;
                                                            }
                                                            $all_new[] = $newest;
                                                            
                                                            unset($t);
                                                            $n++;
                                                        }
                                                    ?>
                                                    <?php foreach ($all_new as $key => $value){ ?>
                                                            
                                                    <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                                                        <td>
                                                            <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                                                <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                                                <?php foreach($jenis_limbah_b3_data_master as $uk):
                                                                    echo "<option value='".$uk->ket."'";
                                                                        if ($value['jenis'] == $uk->ket) echo "selected";
                                                                    echo ">$uk->ket</option>";
                                                                endforeach;?>
                                                                <?php // foreach($jenis_limbah_b3 as $uk): ?>
                                                                    <!-- <option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option> -->
                                                                <?php // endforeach;?>
                                                            </select>
                                                        </td>
                                                        <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" value="<?php echo $value['tps_1']?>" /></td>
                                                        <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" value="<?php echo $value['tps_2']?>" /></td>
                                                        <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" value="<?php echo $value['tps_3']?>" /></td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-lg-6" style="width:60%">
                                                                    <div class="text-left">
                                                                        <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" value="<?php echo $value['tps_4']?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="co-lg-6">
                                                                    <div class=" text-right" >
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php  } ?>
                                                        <?php
                                                        $x=1;
                                                        foreach($tps_b3 as $jl){
                                                            $koord_derajat_s[$x] = $jl->koord_derajat_s;
                                                            $koord_jam_s[$x] = $jl->koord_jam_s;
                                                            $koord_menit_s[$x] = $jl->koord_menit_s;
                                                            $koord_derajat_e[$x] = $jl->koord_derajat_e;
                                                            $koord_jam_e[$x] = $jl->koord_jam_e;
                                                            $koord_menit_e[$x] = $jl->koord_menit_e;
                                                            $x++;
                                                        }
                                                    ?>
                                                    <tr>
                                                        <td>Koordinat</td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s1" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[1]?>" placeholder="00&deg;">
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s1" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[1]?>" placeholder="00&#39;">
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s1" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[1]?>" placeholder="00&quot;"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e1" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[1]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e1" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[1]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e1" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[1]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s2" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[2]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s2" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[2]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s2" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[2]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e2" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[2]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e2" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[2]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e2" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[2]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s3" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[3]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s3" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[3]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s3" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[3]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e3" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[3]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e3" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[3]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e3" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[3]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s4" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[4]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s4" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[4]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s4" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[4]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e4" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[4]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e4" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[4]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e4" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[4]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                        <tr>
                                                            <td>Ukuran</td>
                                                            <td><input type="text" name="tb_ukuran1" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[0]->ukuran;?>"/></td>
                                                            <td><input type="text" name="tb_ukuran2" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[1]->ukuran;?>"/></td>
                                                            <td><input type="text" name="tb_ukuran3" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[2]->ukuran;?>"/></td>
                                                            <td><input type="text" name="tb_ukuran4" class="form-control input-sm has-feedback" value="<?php echo $tps_b3[3]->ukuran;?>"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"><label>Kelengkapan</label></yd>
                                                        </tr>
                                                        <tr>
                                                            <td>Papan Nama dan Koordinat</td>
                                                            <td><select name="tb_ppn_nama_koord1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->ppn_nama_koord == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->ppn_nama_koord == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Simbol dan Label</td>
                                                            <td><select name="tb_simbol_label1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->simbol_label == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->simbol_label == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Saluran Ceceran Air Limbah</td>
                                                            <td><select name="tb_saluran_cecer1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->saluran_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->saluran_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bak Penampung Ceceran Air Limbah</td>
                                                            <td><select name="tb_bak_cecer1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->bak_cecer == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->bak_cecer == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kemiringan</td>
                                                            <td><select name="tb_kemiringan1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->kemiringan == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->kemiringan == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>SOP Pengelolaan dan Tanggap Darurat</td>
                                                            <td><select name="tb_sop_darurat1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->sop_darurat == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->sop_darurat == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Log Book</td>
                                                            <td><select name="tb_log_book1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->log_book == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->log_book == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>APAR</td>
                                                            <td><select name="tb_apar_p3k1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->apar_p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->apar_p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kotak P3K</td>
                                                            <td><select name="tb_p3k1" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[0]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[0]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k2" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[1]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[1]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k3" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[2]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[2]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k4" class="form-control input-sm"><option value="1" <?php echo (($tps_b3[3]->p3k == true) ? 'selected' : ''); ?>>Ada</option><option value="0" <?php echo (($tps_b3[3]->p3k == false) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group ">
                                                <label>d. Neraca Limbah B3</label>
                                                <select class="form-control input-sm" name="b3_neraca">
                                                    <option value="1" <?php echo (($pencemaran_pb3->b3_neraca == true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_pb3->b3_neraca == false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>e. Manifest</label>
                                                <select class="form-control input-sm" name="b3_manifest">
                                                    <option value="1" <?php echo (($pencemaran_pb3->b3_manifest== true) ? 'selected' : ''); ?> >Ada</option>
                                                    <option value="0" <?php echo (($pencemaran_pb3->b3_manifest== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>f. Pelaporan Neraca dan Manifest</label>
                                                <select class="form-control input-sm" name="b3_lapor_nm">
                                                    <option value="Rutin" <?php echo (($pencemaran_pb3->b3_lapor_nm == 'Rutin') ? 'selected' : ''); ?> >Rutin</option>
                                                    <option value="Tidak Rutin" <?php echo (($pencemaran_pb3->b3_lapor_nm == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan" <?php echo (($pencemaran_pb3->b3_lapor_nm == 'Tidak Melaporkan') ? 'selected' : ''); ?> >Tidak Melaporkan</option>
                                                </select>
                                            </div>

                                            <?php 
                                                $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                                $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                                $new = array();
                                                for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                                    $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                                }
                                            ?>
                                            
                                            <label>g. Limbah Padat Lain</label>
                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Jenis</th>
                                                        <th class="text-center">Jumlah (ton/hari)</th>
                                                        <th class="text-center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($new as $key => $value) { ?>
                                                    <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                        <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                                                        <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                                                        <td class="col-lg-2">
                                                            <div class=" text-right">
                                                                <div class="btn-group" style="padding-right: 17px;">
                                                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <!-- <div class="form-group ">
                                                <label> Jenis</label>
                                                <input type="text" name="padat_jenis" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_jenis;?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label> Jumlah</label>
                                                <input type="text" name="padat_jumlah" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_jumlah;?>"/>
                                            </div> -->
                                            <div class="form-group ">
                                                <label> Pengelolaan</label>
                                                <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                    <?php 
                                                        $padat_kelola = $pencemaran_pb3->padat_kelola;
                                                        if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                            echo "</select>";
                                                            echo "<input type='text' name='padat_kelola2' id='pe2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                        } else {
                                                            $temp = "Lainnya";
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                            echo "</select>";
                                                            echo "<input type='text' name='padat_kelola2' id='pe2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
                                                        }
                                                    ?>
                                            </div>

                                            <div class="form-group ">
                                                <label>h. Lain-lain</label>
                                                <input type="text" name="lain_lain_pb3" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->lain_lain;?>"/>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="lain_lain">
                                            <h4 class="page-tab">Lain - Lain</h4>
                                            <div class="form-group ">
                                                <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"><?php echo $bap->catatan;?></textarea>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_lab'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        
                        </div>
                        <!-- /.row -->
                    </div>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>

                
              <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                <br/><br/> -->

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->

            <?php 
            function kelolaConv($value) {
                        if ($value == '1') {
                            return "Dikelola";
                        } else {
                            return "Tidak Dikelola";
                        }
                    }
            ?>
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->

<script type="text/javascript">
    $(document).ready(function () {

        var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search_industri_bap/15',
            onSearchStart: function (query) {
                $('#id_industri').val(null);              
            },
            onSelect: function (suggestion) {
                $('#id_industri').val(suggestion.data).change();
                var id_last_input = $('#id_industri').val();
                $('#id_last_input').val(id_last_input);
                console.log($('#id_last_input').val());
            }
        });

        $("select[name=id_industri]").change(function(){
                $(this).get_detail_industri(this, '#container2');
            });

        $('#si').change(function() {
            if($(this).val()  == 'other') {
                $('#si2').show();
                $('#si2').focus();
            } else {
                $('#si2').hide();
            }
        });

        $('#je').change(function() {
            if($(this).val()  == 'other') {
                $('#je2').show();
                $('#je2').focus();
            } else {
                $('#je2').hide();
            }
        });

        $('#pp').change(function() {
            if($(this).val()  == 'other') {
                $('#pp2').show();
                $('#pp2').focus();
            } else {
                $('#pp2').hide();
            }
        });

        $('#bocor').change(function() {
            if($(this).val()  == true) {
                $('#lok').show();
                $('#lok').focus();
            } else {
                $('#lok').hide();
            }
        });

        $('#pe').change(function() {
            if($(this).val()  == 'Lainnya') {
                $('#pe2').show();
                $('#pe2').focus();
            } else {
                $('#pe2').hide();
            }
        });

        var ul = $("#uji_limbah option:selected").val();
        if (ul == "1") {
            $('#detail_uji').show();
        } else {
            $('#detail_uji').hide();
        }

        $('#uji_limbah').change(function() {
            if($(this).val() == '1') {
                $('#detail_uji').show();
            } else {
                $('#detail_uji').hide();
                $('#pp').val("other");
                $('#pp2').val(null);
                $('#uji_hasil').val("Tidak Memenuhi Baku Mutu");
                $('input[name=uji_tidak_bulan]').val('-');
                $('input[name=uji_tidak_param]').val('-');
            }
        });

        var ipal = $("#ipal option:selected").val();
        if (ipal == "1") {
            $('#detail_ipal').show();
        } else {
            $('#detail_ipal').hide();
            $('#si').val(null);
        }

        $('#ipal').change(function() {
            if($(this).val() == '1') {
                $('#detail_ipal').show();
            } else {
                $('#detail_ipal').hide();
                $('#si').val(null);
                $('#si2').val(null);
                $('#ipal_unit').val(null);
                $('#ipal_kapasitas').val(null);
            }
        });

        var izin = $("#izin option:selected").val();
        if (izin == "1") {
            $('#detail_izin').show();
        } else {
            $('#detail_izin').hide();
        }

        $('#izin').change(function() {
            if($(this).val() == '1') {
                $('#detail_izin').show();
            } else {
                $('#detail_izin').hide();
                $('#izin_no').val(null);
                $('#izin_tgl').val(null);
            }
        });

        var au = $("#au option:selected").val();
        if (au == "1") {
            $('#detail_au').show();
        } else {
            $('#detail_au').hide();
            $('#je').val(null);
        }

        $('#au').change(function() {
            if($(this).val() == '1') {
                $('#detail_au').show();
            } else {
                $('#detail_au').hide();
                $('#je').val(null);
                $('#je2').val(null);
            }
        });

    });
</script>