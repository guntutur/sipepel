<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 18/12/16
 * Time: 15:39
 */
?>

<div class="tab-pane" id="pencemaran_udara">
    <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
    <div class="form-group ">
        <div class="form-group">
            <label>a. Data Emisi Sumber Tidak Bergerak</label>
        </div>
        <div class="form-group">
            <div class="col-lg-6">
                <label>Sumber emisi     :</label>
            </div>
            <div class="col-lg-6">
                <select class="form-control input-sm" name="emboil" id="emboil">
                    <?php
                    if (isset($pencemaran_udara)) {
                        $se = $pencemaran_udara->sumber_emisi;
                        if (($se == "Boiler")) {
                            foreach ($emboil as $p) {
                                echo "<option value='$p' " . (($se == $p) ? 'selected' : '') . ">$p</option>";
                            }
                            echo "</select>";
                            echo "<input type='text' name='emisi_lain' id='emisi_lain' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                        } else {
                            $temp = "other";
                            foreach ($emboil as $p) {
                                echo "<option value='$p' " . (($temp == $p) ? 'selected' : '') . ">$p</option>";
                            }
                            echo "</select>";
                            echo "<input type='text' name='emisi_lain' id='emisi_lain' value='$se' class='form-control input-sm has-feedback'/>";
                        }
                    } else { ?>
                    <option value="Boiler">Boiler</option>
                    <option value="other">lainnya</option>
                </select>
                <input type="text" name="emisi_lain" id="emisi_lain" class="form-control input-sm has-feedback" style="display:none"/>
                <?php } ?>
            </div>
        </div>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th>Data Boiler</th>
                <th class="text-center">Steam</th>
                <th class="text-center">Steam</th>
                <th class="text-center">Oli</th>
                <th class="text-center">Lain-lain</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($emisi_boiler)) { ?>
                <tr>
                    <td><label>Jumlah</label></td>
                    <?php
                    $x=1;
                    foreach($emisi_boiler as $db){
                        echo "<input type='hidden' name='id_emisi_boiler$x' value='".$db->id_emisi_boiler."'>";
                        echo "<input type='hidden' name='id_jenis_emisi_boiler$x' value='".$db->id_jenis_emisi_boiler."'>";
                        echo "<td><input type='text' name='jml_boiler$x' class='form-control input-sm has-feedback ' value='".$db->jml_boiler."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Merk</label></td>
                    <?php
                    $x=1;
                    foreach($emisi_boiler as $db){
                        echo "<td><input type='text' name='merk$x' class='form-control input-sm has-feedback ' value='".$db->merk."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Type</label></td>
                    <?php
                    $x=1;
                    foreach($emisi_boiler as $db){
                        echo "<td><input type='text' name='type$x' class='form-control input-sm has-feedback ' value='".$db->type."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Kapasitas</label></td>
                    <?php
                    $x=1;
                    foreach($emisi_boiler as $db){
                        echo "<td><input type='text' name='kapasitas$x' class='form-control input-sm has-feedback ' value='".$db->kapasitas."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Bahan Bakar</label></td>
                    <?php
                    $x=1;
                    foreach($emisi_boiler as $db){

                        echo "<td><select name='bb$x' class='form-control input-sm '/>";
                        echo "<option value=''></option>";
                        foreach ($bb as $b) {
                            echo "<option value='$b->ket' ".(($db->bahan_bakar == $b->ket) ? 'selected' : '').">$b->ket</option>";
                        }
                        echo "</td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Jumlah Bahan Bakar (ton/hari)</label></td>
                    <?php
                    $x=1;
                    foreach($emisi_boiler as $db){
                        echo "<td><input type='text' name='jbb$x' class='form-control input-sm has-feedback ' value='".$db->jml_bahan_bakar."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
            <?php } else { ?>
                <tr>
                    <td><label>Jumlah </label></td>
                    <td><input type="text" name="jml_boiler1" id="jml_boiler" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="jml_boiler2" id="jml_boiler" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="jml_boiler3" id="jml_boiler" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="jml_boiler4" id="jml_boiler" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Merk</label></td>
                    <td><input type="text" name="merk1" id="merk" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="merk2" id="merk" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="merk3" id="merk" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="merk4" id="merk" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Type</label></td>
                    <td><input type="text" name="type1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="type2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="type3" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="type4" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Kapasitas</label></td>
                    <td><input type="text" id="kapasitas" name="kapasitas1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" id="kapasitas" name="kapasitas2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" id="kapasitas" name="kapasitas3" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" id="kapasitas" name="kapasitas4" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Bahan Bakar</label></td>
                    <td><select id="bb" name="bb1" class="form-control input-sm ">
                            <option value=''></option>
                            <?php
                            foreach ($bb as $b) {
                                echo "<option value='$b->ket'>$b->ket</option>";
                            }
                            ?></select></td>
                    <td><select id="bb" name="bb2" class="form-control input-sm ">
                            <option value=''></option>
                            <?php
                            foreach ($bb as $b) {
                                echo "<option value='$b->ket'>$b->ket</option>";
                            }
                            ?></select></td>
                    <td><select id="bb" name="bb3" class="form-control input-sm ">
                            <option value=''></option>
                            <?php
                            foreach ($bb as $b) {
                                echo "<option value='$b->ket'>$b->ket</option>";
                            }
                            ?></select></td>
                    <td><select id="bb" name="bb4" class="form-control input-sm ">
                            <option value=''></option>
                            <?php
                            foreach ($bb as $b) {
                                echo "<option value='$b->ket'>$b->ket</option>";
                            }
                            ?></select></td>
                </tr>
                <tr>
                    <td><label>Jumlah Bahan Bakar (ton/hari)</label></td>
                    <td><input type="text" id="jbb" name="jbb1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" id="jbb" name="jbb2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" id="jbb" name="jbb3" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" id="jbb" name="jbb4" class="form-control input-sm has-feedback " /></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <div class="form-group">
        <div class="form-group">
            <label>b. Data Cerobong/<i>Stackgas</i></label>
        </div>
        <div class="form-group">
            <div class="col-lg-6">
                <label>Jumlah Cerobong      :</label>
            </div>
            <div class="col-lg-6">
                <input type="text" name="jml_cerobong" id="jml_cerobong" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_udara) ? $pencemaran_udara->jml_cerobong : ""?>" />
            </div>
        </div>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th>b.1 Data Cerobong</th>
                <th class="text-center">1</th>
                <th class="text-center">2</th>
                <th class="text-center">3</th>
                <th class="text-center">4</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($data_cerobong)) { $c = 1; foreach ($data_cerobong as $dc) {
                $sampling_hole[$c] = adaConv($dc->sampling_hole);
                $tangga[$c] = adaConv($dc->tangga);
                $ppt[$c] = adaConv($dc->pengaman_tangga);
                $lantai[$c] = adaConv($dc->lantai);
                $c++;
            } ?>
                <tr>
                    <td><label>Tinggi Cerobong(m)</label></td>
                    <?php
                    $x=1;
                    foreach($data_cerobong as $dc){
                        echo "<input type='hidden' name='id_cerobong$x' value='".$dc->id_cerobong."' />";
                        echo "<td><input type='text' name='h_crb$x' class='form-control input-sm has-feedback ' value='".$dc->h_cerobong."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Diameter(m)</label></td>
                    <?php
                    $x=1;
                    foreach($data_cerobong as $dc){
                        echo "<td><input type='text' name='d_crb$x' class='form-control input-sm has-feedback ' value='".$dc->d_cerobong."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Sampling Hole</label></td>
                    <?php
                    for($p=1; $p<=count($data_cerobong); $p++) {
                        echo "<td><select class='form-control input-sm' name='sampling_hole$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp($sampling_hole[$p], adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Tangga</label></td>
                    <?php
                    for($p=1; $p<=count($data_cerobong); $p++) {
                        echo "<td><select class='form-control input-sm' name='tangga$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp($tangga[$p], adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Pagar pengaman tangga</label></td>
                    <?php
                    for($p=1; $p<=count($data_cerobong); $p++) {
                        echo "<td><select class='form-control input-sm' name='ppt$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp($ppt[$p], adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Lantai kerja</label></td>
                    <?php
                    for($p=1; $p<=count($data_cerobong); $p++) {
                        echo "<td><select class='form-control input-sm' name='lantai$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp($lantai[$p], adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
            <?php } else { ?>
                <tr>
                    <td><label>Tinggi Cerobong(m)</label></td>
                    <td><input type="text" name="h_crb1" id="h_crb" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="h_crb2" id="h_crb" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="h_crb3" id="h_crb" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="h_crb4" id="h_crb" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Diameter(m)</label></td>
                    <td><input type="text" name="d_crb1" id="d_crb" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="d_crb2" id="d_crb" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="d_crb3" id="d_crb" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="d_crb4" id="d_crb" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Sampling Hole</label></td>
                    <td><select class="form-control input-sm" name="sampling_hole1"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="sampling_hole2"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="sampling_hole3"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="sampling_hole4"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td><label>Tangga</label></td>
                    <td><select class="form-control input-sm" name="tangga1"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="tangga2"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="tangga3"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="tangga4"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td><label>Pagar pengaman tangga</label></td>
                    <td><select class="form-control input-sm" name="ppt1"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="ppt2"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="ppt3"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="ppt4"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td><label>Lantai kerja</label></td>
                    <td><select class="form-control input-sm" name="lantai1"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="lantai2"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="lantai3"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="lantai4"><option value=""></option><option value="ada">Ada</option><option value="Tidak ada">Tidak Ada</option></select></td>
                </tr>
            <?php } ?>

            <?php if (isset($uji_emisi)) { $c = 1; foreach ($uji_emisi as $ue) {
                $id_uji_emisi[$c] = $ue->id_uji_emisi;
                $uk[$c] = $ue->uji_kualitas;
                $per[$c] = $ue->period;
                $bme[$c] = $ue->bme_pemenuhan;
                $pe[$c] = $ue->pengendali_emisi;

                $c++;
            } ?>
                <tr>
                    <td><label>b.2 Pengujian Kualitas Emisi</label></td>
                    <?php
                    for($p=1; $p<=count($uji_emisi); $p++) {
                        echo "<td><select class='form-control input-sm' name='uji_kualitas_emisi$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp(adaConv($uk[$p]), adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Periode pengujian (per 6 bulan)</label></td>
                    <?php
                    for($p=1; $p<=count($uji_emisi); $p++) {
                        echo "<td><select class='form-control input-sm' name='uemis_period$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".rutinConv($f)."' " ;
                            if (strcasecmp($per[$p], rutinConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".rutinConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Laboratorium Pengujian</label></td>
                    <?php
                    $x=1;
                    foreach($uji_emisi as $ue){
                        echo "<input type='hidden' name='id_uji_emisi$x' value='".$ue->id_uji_emisi."' />";
                        echo "<td><select name='laborator$x' class='form-control input-sm '/>";
                        foreach ($lab as $l) {
                            echo "<option value='$l->nama_lab' ".(($ue->lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                        }
                        echo "</select></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Pemenuhan BME</label></td>
                    <?php
                    for($p=1; $p<=count($uji_emisi); $p++) {
                        echo "<td><select class='form-control input-sm' id='bmep$p'name='bme_pemenuhan$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".yaConv($f)."' " ;
                            if (strcasecmp(yaConv($bme[$p]), yaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".yaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Parameter tidak memenuhi BME</label></td>
                    <?php
                    $x=1;
                    foreach($uji_emisi as $ue){
                        echo "<td><input type='text' name='bme_param$x' class='form-control input-sm has-feedback ' value='".$ue->bme_param."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Periode tidak memenuhi BME</label></td>
                    <?php
                    $x=1;
                    foreach($uji_emisi as $ue){
                        echo "<td><input type='text' name='bme_period$x' class='form-control input-sm has-feedback ' value='".$ue->bme_period."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>b.3 Alat Pengendali Emisi</label></td>
                    <?php
                    for($p=1; $p<=count($uji_emisi); $p++) {
                        echo "<td><select class='form-control input-sm' name='pengendali_emisi$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp(adaConv($pe[$p]), adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Jenis</label></td>
                    <?php
                    $x=1;
                    foreach($uji_emisi as $ue){
                        echo "<td><input type='text' name='jenis_alat$x' class='form-control input-sm has-feedback ' value='".$ue->jenis."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
            <?php } else { ?>
                <tr>
                    <td><label>b.2 Pengujian Kualitas Emisi</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><select class='form-control input-sm' name='uji_kualitas_emisi".$i."'><option value=''></option><option value='Ada'>Ada</option><option value='Tidak ada'>Tidak Ada</option></select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Periode pengujian (per 6 bulan)</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><select class='form-control input-sm' name='uemis_period".$i."'><option value=''></option><option value='Rutin'>Rutin</option><option value='Tidak Rutin'>Tidak Rutin</option></select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Laboratorium Pengujian</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><select name='laborator".$i."' class='form-control input-sm'>";
                        echo "<option value=''></option>";
                        foreach ($lab as $key) {
                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                        }
                        echo '</select></td>';
                        // echo "<td><input type='text' name='laborator".$i."' class='form-control input-sm has-feedback ' /></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Pemenuhan BME</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><select class='form-control input-sm bmep' id='bmep".$i."' name='bme_pemenuhan".$i."'><option value=''></option><option value='Ya'>Ya</option><option value='Tidak'>Tidak</option></select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Parameter tidak memenuhi BME</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><input type='text' name='bme_param".$i."' id='bme_param".$i."' class='form-control input-sm has-feedback ' /></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Periode tidak memenuhi BME</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><input type='text' name='bme_period".$i."' id='bme_period".$i."' class='form-control input-sm has-feedback ' /></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>b.3 Alat Pengendali Emisi</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><select class='form-control input-sm ape' name='pengendali_emisi".$i."'><option value=''></option><option value='Ada'>Ada</option><option value='Tidak ada'>Tidak Ada</option></select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Jenis</label></td>
                    <?php
                    for ($i=1; $i<5; $i++) {
                        echo "<td><input type='text' name='jenis_alat".$i."' id='ape_j".$i."' class='form-control input-sm has-feedback ' /></td>";
                    }
                    ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <div class="form-group ">
        <label>a. Data Kualitas Udara Ambien</label>
        <table class="table table-th-block">
            <thead>
            <tr>
                <?php if (isset($uji_ambien)) { foreach($uji_ambien as $site) {
                    echo "<th class='text-center'>$site->lokasi</th>";
                } } else { ?>
                    <th>Data Kualitas Udara Ambien</th>
                    <th class="text-center">Upwind</th>
                    <th class="text-center">Site</th>
                    <th class="text-center">Downwind</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($uji_ambien)) { ?>
                <tr>
                    <td><label>Pengujian Kualitas</label></td>
                    <?php $c = 1; foreach ($uji_ambien as $dt) {
                        $id_uji_ambien[$c] = $dt->id_uji_ambien;
                        $tuk[$c] = adaConv($dt->uji_kualitas);
                        $per[$c] = $dt->period;
                        $lab_uk[$c] = $dt->lab;
                        $bm[$c] = $dt->bm_pemenuhan;
                        $bm_param[$c] = $dt->bm_param;
                        $bm_period[$c] = $dt->bm_period;
                        $c++;
                    }

                    for($p=1; $p<=3; $p++) {
                        echo "<input type='hidden' name='id_uji_ambien$p' value='".$id_uji_ambien[$p]."' />";
                        echo "<td><select class='form-control input-sm' name='uji_kualitas$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".adaConv($f)."' " ;
                            if (strcasecmp($tuk[$p], adaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".adaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Periode Pengujian (per 6 bulan)</label></td>
                    <?php
                    for($p=1; $p<=3; $p++) {
                        echo "<td><select class='form-control input-sm' name='period$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".rutinConv($f)."' " ;
                            if (strcasecmp($per[$p], rutinConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".rutinConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Laboratorium Penguji</label></td>
                    <?php
                    for($n=1; $n<=count($lab_uk); $n++){
                        echo "<td><select name='lab$n' class='form-control input-sm '>";
                        foreach ($lab as $l) {
                            echo "<option value='$l->nama_lab' ".(($lab_uk[$n] == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Pemenuhan BM</label></td>
                    <?php
                    for($p=1; $p<=3; $p++) {
                        echo "<td><select class='form-control input-sm' name='bm_pemenuhan$p' id='bmp$p'>";
                        $n=1;
                        foreach ($bool as $f) {
                            echo "<option value='".yaConv($f)."' " ;
                            if (strcasecmp(yaConv($bm[$p]), yaConv($f)) == 0){
                                echo "selected='true'";
                            }
                            echo ">".yaConv($f)."</option>";
                            $n++;
                        }
                        echo "</select></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Parameter Tidak Memenuhi BM</label></td>
                    <?php
                    for($n=1; $n<=count($bm_param); $n++){
                        echo "<td><input type='text' id='bm_param$n' name='bm_param$n' class='form-control input-sm has-feedback ' value='$bm_param[$n]'/></td>";
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Periode Tidak Memenuhi BM</label></td>
                    <?php
                    for($n=1; $n<=count($bm_param); $n++){
                        echo "<td><input type='text' id='bm_period$n' name='bm_period$n' class='form-control input-sm has-feedback ' value='$bm_period[$n]'/></td>";
                    }
                    ?>
                </tr>
            <?php } else {?>
                <tr>
                    <td><label>Pengujian Kualitas</label></td>
                    <td><select class="form-control input-sm" name="uji_kualitas1"></option><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="uji_kualitas2"></option><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="uji_kualitas3"></option><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td><label>Periode Pengujian (per 6 bulan)</label></td>
                    <td><select class="form-control input-sm" name="period1"></option><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                    <td><select class="form-control input-sm" name="period2"></option><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                    <td><select class="form-control input-sm" name="period3"></option><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                </tr>
                <tr>
                    <td><label>Laboratorium Pengujian</label></td>
                    <?php
                    for ($i=1; $i<4; $i++) {
                        echo "<td><select name='lab".$i."' class='form-control input-sm'>";
                        echo "<option value=''></option>";
                        foreach ($lab as $key) {
                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                        }
                        echo '</select></td>';
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Pemenuhan BM</label></td>
                    <td><select class="form-control input-sm bmp" id="bmp1" name="bm_pemenuhan1"></option><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                    <td><select class="form-control input-sm bmp" id="bmp2" name="bm_pemenuhan2"></option><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                    <td><select class="form-control input-sm bmp" id="bmp3" name="bm_pemenuhan3"></option><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                </tr>
                <tr>
                    <td><label>Parameter Tidak Memenuhi BM</label></td>
                    <td><input type="text" name="bm_param1" id="bm_param1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_param2" id="bm_param2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_param3" id="bm_param3" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Periode Tidak Memenuhi BM</label></td>
                    <td><input type="text" name="bm_period1" id="bm_period1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_period2" id="bm_period2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_period3" id="bm_period3" class="form-control input-sm has-feedback " /></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
    $lapor = $bool;
    $lapor[] = '2';
    ?>
    <div class="form-group ">
        <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
        <select class="form-control input-sm" name="pelaporan_ua_ue">
            <?php
            for($d=0; $d<3; $d++){
                echo "<option value='".rutinConv($lapor[$d])."'";
                if (isset($pencemaran_udara)) {
                    if (strcasecmp($pencemaran_udara->pelaporan_ua_ue, rutinConv($lapor[$d])) == 0) {
                        echo "selected";
                    }
                }
                echo ">".rutinConv($lapor[$d])."</option>";
            }
            ?>
        </select>
    </div>

    <div class="form-group ">
        <label>c. Lain-lain</label>
        <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_udara) ? $pencemaran_udara->lain_lain : ""; ?>"/>
    </div>

    <div class="row">
        <div class="col-sm-4 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Air</a>
        </div>
        <div class="col-sm-4 text-center">
            <input id="save-pencemaran_udara" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Udara"/>
        </div>
        <div class="col-sm-4 text-right">
            <a class="btn btn-warning NextStep">Pencemaran PB3<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>
