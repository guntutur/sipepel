<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document    : teguran_bap.php 
 // Modified on : December 22, 2014 20:31 
 // Modified by : huda.jtk09@gmail.com 
 // Description : Controller for Teguran BAP 

class teguran_bap extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_laporan_bap', 'lapDao', TRUE);

		$this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
	}

	public function index() {
		if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

		# load view
        $this->load->view('include/header');        
        $this->load->view('backend/v_teguran_bap');
        $this->load->view('include/footer');
	}

	public function manager() {
		if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

		# load view
        $this->load->view('include/header');        
        $this->load->view('backend/v_teguran_bap_manager');
        $this->load->view('include/footer');
	}

	public function download_laporan() {

		$file = file_get_contents(site_url().'assets/form/Surat_Teguran_BAP.rtf');

		header('Content-type: application/rtf');
		header('Content-Disposition: attachment; filename="Surat_Teguran_BAP.rtf"');

		echo $file;
	}

    public function get_rekap_teguran() {
        # get parameter pagination
        $input = array('dataperpage', 'bap_type', 'curpage');
        foreach ($input as $val) {
            $$val = $this->input->post($val); 
        }

        $total = $this->lapDao->get_usaha_teguran_size($bap_type);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $bap_data = $this->lapDao->get_usaha_teguran($bap_type, $dataperpage, $start);

        $data = array(
            'bap_data' => $bap_data,
            'pagination' => $this->functions->create_links($npage, $curpage, 3),
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['bap_data'] = $bap_data;

        echo json_encode($data);
        exit();
    }

	public function generate_laporan() {
		date_default_timezone_set("Asia/Jakarta");
		setlocale(LC_ALL, 'id_ID');

        $id_bap = $this->input->post('id_bap');
		$bap_data = $this->lapDao->get_data_umum($id_bap);
		$teguran_list = $this->bapDao->get_list_by_id('history_item_teguran_bap', 'id_bap', $id_bap);

		$parameter = '';
		$respon = '';
		$i = 1;
		foreach ($teguran_list as $teg) {
			$parameter = $parameter. '{\fs24 \dbch \af4 \hich \af3 \loch \f3 \lang1033 \langnp1033 \langfe1033 \langfenp1033 \par '. $i .'.\tab '. $teg->ket_parameter_bap.' ('. $teg->nilai_parameter .')} ';
			$respon = $respon. '{\fs24 \dbch \af4 \hich \af3 \loch \f3 \lang1033 \langnp1033 \langfe1033 \langfenp1033 \par \tab } ';
			$i++;
		}

		// read file
		$file_location = "assets/form/Surat_Teguran_BAP_2.rtf";
		$handle = fopen ($file_location, 'r');
		$content = fread($handle, filesize($file_location));
		
		fclose($handle);
		
		$param = array (
			'<date-now>' => ' aaa ',
			'<nama_usaha_kegiatan>' => $bap_data->nama_industri,
			'<alamat_usaha_kegiatan>' => $bap_data->alamat,
			'<tanggal>' => strftime("%d %B %Y", strtotime($bap_data->bap_tgl)), //date_format(new DateTime($bap_data->bap_tgl), 'd F Y'),
			'<parameter>' => $parameter,
			'<parameter-response>' => $respon
		);

		// replace content
		foreach($param as $from => $to) {
		   $content = str_replace($from, $to, $content);
		}
		
		// save file
		// file_put_contents($file_location.'s', $content);
		// fwrite($handle, $content);

		header('Content-type: application/rtf');
		header('Content-Disposition: attachment; filename="Surat_Teguran_BAP_'.$bap_data->nama_industri.'.rtf"');

		echo $content;
	}

    public function conf_upload() {

		$id_bap = $this->input->post('id_bap');

		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("ymdHi");
		$user = $this->tank_auth->get_personname();
		$path = "assets/bap_confirm/";

		# Generate filenames
		$newfilename = array();
		$i = 1;
		foreach($_FILES['userfile']['name'] as $k => $v) {
		    $ext = explode('.', $v);
		    $newfilename[$k] = strtolower("bapconf_".$datenow."_".$i.".".$ext[1]);
		    $i++;
		}

		$config['upload_path'] = realpath($path);
		$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|PNG';
		$config['max_size'] = '8000';
		$config['overwrite'] = TRUE;
		$config['file_name'] = $newfilename;
		
		$this->load->library('upload', $config);

		// huda : edit -> change do_upload to do_multi_upload (7 Jan 15, 16:19)
		if (!$this->upload->do_multi_upload('userfile')) {				
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', $this->upload->display_errors()));
		}else{								

			$url = array();
			foreach ($this->upload->get_multi_upload_data() as $value) {
				$url[]=$value['full_path'];
			}

			$param = array(
				'diupdate_oleh' => $user,
				'conf_pengirim' => $this->input->post('conf_pengirim'),
				'conf_via' => $this->input->post('conf_via'),
				'conf_tgl_kirim' => date("Y-m-d", strtotime($this->input->post('conf_tgl_kirim'))),
				'conf_url' => serialize($url),
				'conf_status' => true
			);

			# edit bap
			$this->bapDao->edit('bap', 'id_bap', $id_bap, $param);				

			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Berkas berhasil diunggah'));
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

    public function conf_manager() {

		$id_bap = $this->input->post('id_bap');

		$param = array(
			'status' => true
		);

		# edit bap
		$this->bapDao->edit('bap', 'id_bap', $id_bap, $param);				

		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'BAP berhasil dikonfirmasi'));
		
		redirect($_SERVER['HTTP_REFERER']);
    }
    	
	public function get_notif_bap() {

        $notif = $this->bapDao->get_notif_bap();
        $data['notif_bap'] = $notif;        

        echo json_encode($data);
        exit();	
	}

	public function count_notif_bap() {
		$jml_notif = $this->bapDao->count_notif_bap();
		$data['jml_notif_bap'] = $jml_notif;
		echo json_encode($data);
		exit();
	}

	public function get_pelanggaran() {
		header('Content-type: application/json');

		$id_bap = $this->input->post('id_bap');
		$data['id_bap'] = $id_bap;
		$data['industri'] = $this->input->post('industri');
		$data['tgl'] = $this->input->post('tgl');
		$data['teguran'] = $this->bapDao->get_list_by_id("history_item_teguran_bap", "id_bap", $id_bap);

		echo json_encode($data);
		exit();
	}

	public function tes() {
		date_default_timezone_set("Asia/Jakarta");
		setlocale(LC_ALL, 'id_ID');
		$datenow = strftime("%d %B %Y");

		echo "date: ". $datenow;

	}
}