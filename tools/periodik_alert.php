<?php
# author: adryan.ardiansyah@gmail.com

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "sil_app";

$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die('Koneksi database gagal dibuat');
$current_month = date('d.m');
// $current_month = '15.07';

if($conn) {
	# select database
	mysql_select_db($dbname);
	
	# get data semua industri
	$query = mysql_query("select i.id_industri, i.nama_industri, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Air Limbah' and id_industri = i.id_industri) as kualitas_air, (select group_concat(concat(d.laporan_bulan, ':', (select count(*) from detail_debit_air_limbah where id_debit_air_limbah = d.id_debit_air_limbah)) separator ';') as jumlah from debit_air_limbah d where d.id_industri = i.id_industri) as catatan_debit, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Emisi' and id_industri = i.id_industri) as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Ambien' and id_industri = i.id_industri) as kualitas_ambien, (select date_format(max(tgl), '%m.%Y') from manifest where id_penghasil_limbah = i.id_industri) as manifest, (select group_concat(date_format(periode_waktu, '%m.%Y')) from neraca_b3 where id_industri = i.id_industri) as neraca_b3 from industri i order by i.nama_industri;");
	
	echo "<pre>";
	$data = array();
	$query_insert = array();
	while($row = mysql_fetch_assoc($query)) {
		
		# nama_industri
		$data[$row['id_industri']]['nama_industri'] = $row['nama_industri'];
		
		if($current_month == '15.04' or $current_month == '15.07' or $current_month == '15.10' or $current_month == '15.12') {
			# ekstrak kualitas air
			$temp_catatan = array();               
			$laporan = explode(';', $row['kualitas_air']);
			foreach ($laporan as $key => $value) {                    
				$temp = explode('.', $value);                
				$temp_catatan[] = (int)$temp[0];
			}		
			// $data[$row['id_industri']]['kualitas_air'] = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');		
			$catatan_teguran = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			foreach ($catatan_teguran as $p => $periode) {
				foreach ($periode as $k => $v) {
					$periode_ke = 'Triwulan '.$k;
					foreach ($v as $key => $value) {
						if(isset($value) and $key == "Tidak Lengkap") {
							array_push($query_insert, "insert into history_teguran_periode_adm values(NULL, '".$row['id_industri']."', '".date('Y-m-d')."', '".$periode_ke."', '".serialize($value)."', 'Laporan Hasil Pengujian Kualitas Air Limbah', '".date('Y-m-d')."', '".date('Y-m-d')."', 'Cron Service', 'Cron Service')");					
						}
					}
				}
			}
		
						
			$temp_catatan = array();
			$laporan = explode(';', $row['catatan_debit']);
			foreach ($laporan as $key => $value) {                    
				$temp = explode(':', $value);
				$temp_catatan[] = (int)((isset($temp[1]) and ($temp[1] > 0)) ? (substr($temp[0], 0, 2)) : '');               
			}		
			// $data[$row['id_industri']]['catatan_debit'] = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			$catatan_teguran = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			foreach ($catatan_teguran as $p => $periode) {
				foreach ($periode as $k => $v) {
					$periode_ke = 'Triwulan '.$k;
					foreach ($v as $key => $value) {
						if(isset($value) and $key == "Tidak Lengkap") {						
							array_push($query_insert, "insert into history_teguran_periode_adm values(NULL, '".$row['id_industri']."', '".date('Y-m-d')."', '".$periode_ke."', '".serialize($value)."', 'Catatan Debit Harian Air Limbah', '".date('Y-m-d')."', '".date('Y-m-d')."', 'Cron Service', 'Cron Service')");						
						}
					}
				}
			}

			# extract manfest
			$temp_catatan = array();
			$laporan = explode(';', $row['manifest']);
			foreach ($laporan as $key => $value) {
				$temp = explode('.', $value);
				$temp_catatan[] = (int)$temp[0];
			}
			// $data[$row['id_industri']]['manifest'] = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			$catatan_teguran = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			foreach ($catatan_teguran as $p => $periode) {
				foreach ($periode as $k => $v) {
					$periode_ke = 'Triwulan '.$k;
					foreach ($v as $key => $value) {
						if(isset($value) and $key == "Tidak Lengkap") {
						
							array_push($query_insert, "insert into history_teguran_periode_adm values(NULL, '".$row['id_industri']."', '".date('Y-m-d')."', '".$periode_ke."', '".serialize($value)."', 'Manifest Limbah B3', '".date('Y-m-d')."', '".date('Y-m-d')."', 'Cron Service', 'Cron Service')");
							
						}
					}
				}
			}

			# extraction neraca
			$temp_catatan = array();
			$laporan = explode(';', $row['neraca_b3']);
			foreach ($laporan as $key => $value) {
				$temp = explode('.', $value);
				$temp_catatan[] = (int)$temp[0];
			}
			// $data[$row['id_industri']]['neraca_b3'] = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			$catatan_teguran = check_periode(chunk_value($temp_catatan, 'triwulan'), 'triwulan');
			foreach ($catatan_teguran as $p => $periode) {
				foreach ($periode as $k => $v) {
					$periode_ke = 'Triwulan '.$k;
					foreach ($v as $key => $value) {
						if(isset($value) and $key == "Tidak Lengkap") {
							
							array_push($query_insert, "insert into history_teguran_periode_adm values(NULL, '".$row['id_industri']."', '".date('Y-m-d')."', '".$periode_ke."', '".serialize($value)."', 'Neraca Limbah B3', '".date('Y-m-d')."', '".date('Y-m-d')."', 'Cron Service', 'Cron Service')");
							
						}
					}
				}
			}
		}

		if($current_month == '15.07' or $current_month == '15.12') {				
			# extract kualitas udara emisi            
			$temp_catatan = array();
			$laporan = explode(';', $row['kualitas_emisi']);
			foreach ($laporan as $key => $value) {                    
				$temp = explode('.', $value);
				$temp_catatan[] = (int)$temp[0];
			}
			// $data[$row['id_industri']]['kualitas_emisi'] = check_periode(chunk_value($temp_catatan, 'semester'), 'semester');      
			$catatan_teguran = check_periode(chunk_value($temp_catatan, 'semester'), 'semester');
			foreach ($catatan_teguran as $p => $periode) {
				foreach ($periode as $k => $v) {
					$periode_ke = 'Semester '.$k;
					foreach ($v as $key => $value) {
						if(isset($value) and $key == "Tidak Lengkap") {						
							array_push($query_insert, "insert into history_teguran_periode_adm values(NULL, '".$row['id_industri']."', '".date('Y-m-d')."', '".$periode_ke."', '".serialize($value)."', 'Laporan Hasil Pengujian Kualitas Udara Emisi Sumber Tidak Bergerak', '".date('Y-m-d')."', '".date('Y-m-d')."', 'Cron Service', 'Cron Service')");					
						}
					}
				}
			}
			
			# extract kualitas ambien
			$temp_catatan = array();
			$laporan = explode(';', $row['kualitas_ambien']);
			foreach ($laporan as $key => $value) {
				$temp = explode('.', $value);
				$temp_catatan[] = (int)$temp[0];
			}
			// $data[$row['id_industri']]['kualitas_ambien'] = check_periode(chunk_value($temp_catatan, 'semester'), 'semester');
			$catatan_teguran = check_periode(chunk_value($temp_catatan, 'semester'), 'semester');
			foreach ($catatan_teguran as $p => $periode) {
				foreach ($periode as $k => $v) {
					$periode_ke = 'Semester '.$k;
					foreach ($v as $key => $value) {
						if(isset($value) and $key == "Tidak Lengkap") {
							
							array_push($query_insert, "insert into history_teguran_periode_adm values(NULL, '".$row['id_industri']."', '".date('Y-m-d')."', '".$periode_ke."', '".serialize($value)."', 'Laporan Hasil Pengujian Kualitas Udara Ambien', '".date('Y-m-d')."', '".date('Y-m-d')."', 'Cron Service', 'Cron Service')");
							
						}
					}
				}
			}
		}		

	}
	
	# insert pelanggaran ke database
	foreach ($query_insert as $key => $value) {
		try {			
			mysql_query($value) or die('not');
		}catch(exception $e) {
		  echo "ex: ".$e; 
		}
	}	
}


# all function
function check_periode($data, $periode="triwulan") {  

	$results = array();	
	$master_data = array(
			'triwulan' => array(array(1,2,3), array(4,5,6), array(7,8,9), array(10,11,12)),
			'semester' => array(array(1,2,3,4,5,6), array(7,8,9,10,11,12))
		);

	$loop = ($periode == 'triwulan') ? 4 : 2;	
	
	foreach ($data as $k => $v) {     
		$tmp = array();  
		$bulan = array();
		for ($i=0; $i < $loop; $i++) { 
			if($i > $k) { $tmp[$i+1]['Belum Ada'] = array(); }
			else{    

				$temp = array();
				for ($j=0; $j < count($master_data[$periode][$i]); $j++) {					
					if(in_array($master_data[$periode][$i][$j], $v)) { $temp[$j] = 1; }
					else { 
						array_push($bulan, $master_data[$periode][$i][$j]);
						$temp[$j] = 0;
					}
				}
				
				if(all($temp)) $tmp[$i+1]['Lengkap'] = array();
				else $tmp[$i+1]['Tidak Lengkap'] = $bulan;          
			}
		}
	}

	$results[$periode] = $tmp;
	return $results;
}

function all($array) {
	for ($i=0; $i < count($array); $i++) { 
		if($array[$i] == 0) return 0;
	}
	return 1;
}

function chunk_value($data, $periode) {
	$temp = array();
	if($periode == "triwulan") {
		foreach($data as $k => $v) {		
			if($v <= 3) { $temp[0][] = $v; }
			if($v <= 6 and count(array_intersect($data, array(4,5,6))) > 0) { $temp[1][] = $v; }
			if($v <= 9 and count(array_intersect($data, array(7,8,9))) > 0) { $temp[2][] = $v; }
			if($v <= 12 and count(array_intersect($data, array(10,11,12))) > 0) { $temp[3][] = $v; }	
		}
	}else if($periode == "semester"){
		foreach($data as $k => $v) {		
			if($v <= 6) { $temp[0][] = $v; }			
			if($v <= 12 and count(array_intersect($data, array(7,8,9,10,11,12))) > 0) { $temp[1][] = $v; }	
		}
	}
	return $temp;
}
?>