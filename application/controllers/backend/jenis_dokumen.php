<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jenis_dokumen extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_jenis_dokumen', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types
        $types = $this->m_jenis_dokumen->get_all();
		
		# response paramter
        $data['jenis_dokumen'] = $types;        
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_jenis_dokumen', $data);
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'id_jenis' => array('SEARCH', 'OR', $query),
				'kategori_dokumen' => array('SEARCH', 'OR', $query),
				'ket' => array('SEARCH', 'OR', $query),
				'periode_pencatatan' => array('SEARCH', 'OR', $query)
			);

     	$total = $this->m_jenis_dokumen->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_jenis_dokumen->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_jenis_dokumen->get_jenis_dokumen_by_id($this->input->post('id_jenis')));
		exit();
	}
	
	public function edit() {
		// if($this->validate() or $this->input->post('id')) {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'kategori_dokumen' => $this->input->post('kategori_dokumen'),
			'ket' => $this->input->post('keterangan'),
			'jml_data_tahun' => $this->input->post('jml_data_tahun'),
			'periode_pencatatan' => $this->input->post('periode_pencatatan'),
			'status' => $this->input->post('status'),
			# 'is_have_parameter' => $this->input->post('is_have_parameter'),
			'periode_bulan' => $this->input->post('periode_bulan'),
			// 'toleransi_hari' => $this->input->post('toleransi_hari'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_jenis_dokumen->change_jenis_dokumen($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/jenis_dokumen');
	}
	
	public function register() {
		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'kategori_dokumen' => $this->input->post('kategori_dokumen'),
				'ket' => $this->input->post('keterangan'),
				'jml_data_tahun' => $this->input->post('jml_data_tahun'),
				'periode_pencatatan' => $this->input->post('periode_pencatatan'),
				'status' => $this->input->post('status'),
				# 'is_have_parameter' => $this->input->post('is_have_parameter'),
				'periode_bulan' => $this->input->post('periode_bulan'),
				// 'toleransi_hari' => $this->input->post('toleransi_hari'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_jenis_dokumen->insert_jenis_dokumen($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		redirect('backend/jenis_dokumen');
	}
	
	public function delete() {
		$this->m_jenis_dokumen->delete_jenis_dokumen($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/jenis_dokumen');
	}

	public function validate() {
		# check data yang duplicate
		if($this->m_jenis_dokumen->is_duplicate($this->input->post('kategori_dokumen'),$this->input->post('keterangan'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect('backend/jenis_dokumen');
		}

		return true;
	}
}
