<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading"><?php echo $title; ?>&nbsp;&nbsp;<small>detail dari pemantauan debit air limbah</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>                
                <li>Debit Air Limbah</li>
                <li class="active">Detail Bulan <?php echo $date; ?></li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" onclick="document.location='<?php echo site_url('backend/debit_air_limbah'); ?>';" class="btn btn-sm btn-info"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</button>
                        <button type="button" data-toggle="modal" data-target="#input_detail" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Tanggal</th>
                                <th width="15%">Debit</th>                                
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm" style="margin: 0;">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0,
            id: '<?php echo $this->uri->segment(4); ?>'
        },
        url: '<?php echo base_url("backend/debit_air_limbah/get_detail_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];                        
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        var nowTemp = $(this).format_date(dt.tgl_debit);      
                        var tmp = nowTemp.split(".");
                        t += '<td>'+ tmp[0] +' '+$(this).get_month(parseInt(tmp[1]))+' '+ tmp[2] +'</td>';               
                        // t += '<td>'+nowTemp+'</td>';                       
                        t += '<td>'+dt.debit+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';               
                        t += '<li><a onclick="$(this).edit('+dt.id_debit+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_debit+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

    $(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }

        $.fn.edit = function(id) {
            $.ajax({
                url: "<?php echo base_url('/backend/debit_air_limbah/get_detail'); ?>",
                type: 'POST',
                dataType: 'json',
                data: $.param({id_debit: id}),
                success: function(resp) {
                    
                    $('#input_detail form')[0].reset();
                    $('#input_detail form input[type=hidden]').val('');

                    $('input[id=detail_id]').val(resp.id_debit);
                    $('input[name=tgl_debit]').val($(this).format_date(resp.tgl_debit));
                    $('input[name=debit]').val(resp.debit);
                    $('input[name=id_debit]').val(resp.id_debit_air_limbah);

                    $('#input_detail form').attr('action', '<?php echo base_url('/backend/debit_air_limbah/edit_detail') ?>'); //this fails silently
                    $('#input_detail form').get(0).setAttribute('action', '<?php echo base_url('/backend/debit_air_limbah/edit_detail') ?>'); //this works

                    $('#input_detail').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            });          
        }

        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }

        Document.search();
    });
</script>

<!-- Modal -->
<div class="modal fade" id="input_detail" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="uploadLabel">Detail Debit Air Limbah Per Hari</h4>
            </div>
            <form action="<?php echo base_url('/backend/debit_air_limbah/insert_detail'); ?>" method="post" enctype="multipart/form-data">
                <div class="modal-body">   
                    <div class="row">
                        <div class="col-lg-6" style="padding-right: 5px;">
                            <label>Tanggal</label>
                            <div class="input-group input-group-sm">
                                <input type="text" name="tgl_debit" class="form-control datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>    
                        <div class="col-lg-6 form-group ">
                            <label>Debit</label>
                            <div class="input-group input-group-sm">                                
                                <input type="text" name="debit" id="debit" class="form-control" placeholder="0"/>
                                <span class="input-group-addon">m3/hari</span>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="detail_id" name="id" value="<?php echo $this->uri->segment(4)?>" />
                    <input type="hidden" name="id_debit" value="<?php echo $this->uri->segment(4)?>" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="do_upload" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
                
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Debit Air Limbah</h4>
            </div>
            <form action="<?php echo base_url('/backend/debit_air_limbah/delete_detail'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->