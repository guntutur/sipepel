<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class usaha_kegiatan extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_business_activity', '', TRUE);

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_business_activity');
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'p.ket' => array('SEARCH', '', $query)
			);

     	$total = $this->m_business_activity->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_business_activity->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_business_activity->get_business_activity_by_id($this->input->post('id_usaha_kegiatan')));
		exit();
	}
	
	public function edit() {

		//if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'ket' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_business_activity->change_business_activity($param, $this->input->post('id')))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		//}
			
		redirect('backend/usaha_kegiatan');
	}
	
	public function register() {

		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'ket' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_business_activity->insert_business_activity($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
			
		redirect('backend/usaha_kegiatan');
	}
	
	public function delete() {
		if($this->m_business_activity->delete_business_activity($this->input->post("id"))) 
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal dihapus'));		
		
		redirect('backend/usaha_kegiatan');
	}

	public function validate() {
		# check data yang duplicate
		if($this->m_business_activity->is_duplicate($this->input->post('description'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect('backend/usaha_kegiatan');
		}

		return true;
	}

}
