<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document   : laporan_bap.php 
 // Created on : November 18, 2014 15:49 
 // Author     : huda.jtk09@gmail.com 
 // Description: Controller for Rekapitulasi Laporan BAP 

class rekap_bap extends CI_Controller {

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_laporan_bap', 'lapDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view
	public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

        # load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_rekap_bap');
    }

    public function get_rekap() {
        # get parameter pagination
        $input = array('dataperpage', 'bap_type', 'curpage');
        foreach ($input as $val) {
            $$val = $this->input->post($val); 
        }

        $total = $this->lapDao->get_all_data_umum_size($bap_type);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $bap_data = $this->lapDao->get_all_data_umum($bap_type, $dataperpage, $start);

        foreach ($bap_data as $value) {
            
            $top_data = $this->lapDao->get_top($value->id_bap);

            if(isset($top_data)){
                $langgar = '';

                foreach ($top_data as $key => $top) {
                    $langgar = $langgar . '<label>'.$top->ket.'</label>';
                    
                    $pelanggaran = $this->lapDao->get_pelanggaran($value->id_bap, $top->id_parameter_bap);
                    
                    if (strcasecmp($top->ket,'lain - lain')==0) {
                        $langgar = $langgar . ' <div style="white-space: pre-wrap;">' . $pelanggaran[0]->nilai_parameter . '</div>'; //
                    } else {

                        for($i = 0; $i < count($pelanggaran); $i++) {
                            $val = $pelanggaran[$i];
                            
                            if (strcasecmp($val->id_parent_2,'top')==0) {
                                $langgar = $langgar . '<li>' . $val->ket_parameter_bap . ' (' . $val->nilai_parameter . ')</li>';

                            } else {
                                $parent_1 = $this->bapDao->get_obj_by_id('parameter_bap','id_parameter_bap', (int) $val->id_parent_1);
                                
                                if ($i == 0) {
                                    $langgar = $langgar . '<li>' . $parent_1->ket . '';
                                    $langgar = $langgar . '<ul><li>' . $val->ket_parameter_bap . ' (' . $val->nilai_parameter . ')</li>';
                                } else {
                                    if ($val->id_parent_1 == $pelanggaran[$i-1]->id_parent_1) {
                                        $langgar = $langgar . '<li>' . $val->ket_parameter_bap . ' (' . $val->nilai_parameter . ')</li>';
                                    } else {
                                        $langgar = $langgar . '<li>' . $parent_1->ket . '';
                                        $langgar = $langgar . '<ul><li>' . $val->ket_parameter_bap . ' (' . $val->nilai_parameter . ')</li>';
                                    }
                                }

                                if ($i == (count($pelanggaran)-1)) {
                                    $langgar = $langgar . '</ul>';
                                } else {
                                    if ($val->id_parent_1 != $pelanggaran[$i+1]->id_parent_1) {
                                        $langgar = $langgar . '</ul>';
                                    }
                                }

                            }
                        }
                    }

                    if ($key != (count($top_data)-1)) {
                        $langgar = $langgar . '<br> ';
                    }
                }

                $value->pelanggaran = $langgar;
                $value->ket = 'Tidak Taat';
            } else {
                $value->pelanggaran = ' - ';
                $value->ket = ' - ';
            }
        }

        $data = array(
            'bap_data' => $bap_data,
            'pagination' => $this->functions->create_links($npage, $curpage, 3),
            'numpage' => $npage - 1,
            'total' => $total
        );

        echo json_encode($data);
        exit();
    }

    public function tes($bap_data) {

        // $bap_data = $this->bapDao->get_list_sort('parameter_bap', 'jenis_bap', 'bap_golf', 'id_parameter_bap', 'desc');
        // $bap_data['xtc'] = new StdClass;
        // $bap_data['m2r'] = new StdClass;
        // $bap_data['brigez'] = new StdClass;
        // $bap_data['xtc']->ketua = 'Agi';
        // $bap_data['xtc']->markas = 'lembang';
        // $bap_data['m2r']->ketua = 'idzi';
        // $bap_data['m2r']->markas = 'dago';
        // $bap_data['brigez']->ketua = 'ronal';
        // $bap_data['brigez']->markas = 'ciparay';

        foreach ($bap_data as $key => $z) {
            echo $key .'. ';
            foreach ($z as $x => $y) {
                echo $x . ': ' . $y . '<br >';
            }
        }
    }

}
