<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class detail_lhu extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model        
        $this->load->model('m_detail_lhu');
        $this->load->model('m_lhu_air_limbah');
		
		$this->load->config('tank_auth', TRUE);
    }

    public function get_baku_mutu() { 

        $id_lhu = $this->input->post('id_laporan_hasil_uji');
        $lhu = $this->get_lhu($id_lhu);

        if($lhu->jenis_baku_mutu == 'Baku Mutu Izin') {
            // $jenis_lhu = ($lhu->jenis_lhu == 'LHU Air Limbah') ? "Air Limbah" : "Udara Emisi";
            $jenis_lhu = $this->get_jenis_lhu($lhu->jenis_lhu);
            $detail = $this->m_detail_lhu->get_parameter_izin(' b.id_industri = '.$lhu->id_industri.' and b.jenis_baku_mutu = "'.$jenis_lhu.'"', $lhu->id_laporan_hasil_uji);
        }else{
            $detail = $this->m_detail_lhu->get_lhu_by_id($id_lhu);
        }
		
        echo json_encode($detail);
        exit();
    }    

    public function register_detail() {

        $id_lhu = $this->input->post('id');

        # get detail lhu
        $detail_lhu = $this->m_detail_lhu->get_detail_by_id($id_lhu);   

        # untuk laporan yang tidak menggunakan baku mutu semua     

        # get detail baku mutu
        $standard = array();
        $detail = $this->m_detail_lhu->get_lhu_by_id($id_lhu);
        foreach ($detail as $k => $v) {
            $standard[$v->id_parameter_lhu] = array(
                    'nama_parameter' => $v->ket,
                    'nilai_internal' => $v->nilai_parameter_lhu_internal,
                    'nilai_eksternal' => $v->nilai_parameter_lhu_eksternal,
                    'hukum_internal' => $v->dasar_hukum_internal,
                    'hukum_eksternal' => $v->dasar_hukum_eksternal
                );
        }

        $param_lhu = $this->input->post('param_lhu');    
        $metode = $this->input->post('metode');        
        $pelanggaran = array();
        $nilai_standard = '';
        foreach($param_lhu as $k => $v) {
            $status = 0;
            if($detail_lhu->jenis_baku_mutu !== 'Tidak Ada') {
                # nilai baku mutu
                $nilai_standard = $standard[$k]['nilai_'.strtolower($detail_lhu->jenis_baku_mutu)];
                $nama_parameter = $standard[$k]['nama_parameter'];
                $dasar_hukum = $standard[$k]['hukum_'.strtolower($detail_lhu->jenis_baku_mutu)];

                # check pelanggaran                        
                if(strtolower($v) != 'tt' and strtolower($v) != 'td') {                 
                    // if($k == 59) { echo $v; exit(); } 
                    if (strpos($nilai_standard, '-') !== false) {
                        $tmp = explode('-', $nilai_standard);
                        $min = (float)$tmp[0]; $max = (float)$tmp[1];                
                        if(!((float)$v > $min and (float)$v <= $max)) {
                            $status = 1;                
                            $pelanggaran[$k] = array($v, $nilai_standard, $nama_parameter, $dasar_hukum);
                        }
                    }else{
                        if((float)$v > (float)$nilai_standard) {  
                            $status = 1;                           
                            $pelanggaran[$k] = array($v, $nilai_standard, $nama_parameter, $dasar_hukum);
                        }
                    }
                }
            }

            # parameter detail lhu
            $param_detail_lhu = array(
                    'id_laporan_hasil_uji' => $id_lhu,
                    'id_parameter_lhu' => $k,
                    'nilai_hasil_uji' => $v,
                    'nilai_baku_mutu' => (($nilai_standard != '') ? (string)$nilai_standard : '0'),
                    'status_pelanggaran' => $status,
                    'metode_pengujian' => (isset($metode[$k]) ? $metode[$k] : ''),
                    'tgl_pembuatan' => date("Y-m-d"),
                    'tgl_update' => date("Y-m-d"),
                    'dibuat_oleh' => $this->tank_auth->get_personname(),
                    'diupdate_oleh' => $this->tank_auth->get_personname()
                );
            $this->m_detail_lhu->insert_detail($param_detail_lhu);            
        }

        $this->m_detail_lhu->clear_history_pelanggaran($id_lhu);

        if(count($pelanggaran) > 0) {
            foreach ($pelanggaran as $key => $value) {
                $param_pelanggaran = array(
                        'id_laporan_hasil_uji' => $id_lhu,
                        'parameter_lhu' => $value[2],
                        'dasar_hukum'=> $value[3],
                        'nilai_hasil_uji' => $value[0],
                        'nilai_baku_mutu' => $value[1],
                        'tgl_pembuatan' => date("Y-m-d"),
                        'tgl_update' => date("Y-m-d"),
                        'dibuat_oleh' => $this->tank_auth->get_personname(),
                        'diupdate_oleh' => $this->tank_auth->get_personname()
                    );                
                $this->m_detail_lhu->insert_pelanggaran($param_pelanggaran);
            }
        }

        $this->session->set_flashdata('error', $pelanggaran);
        $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));

        redirect($_SERVER['HTTP_REFERER']);

    }    

    public function register_izin() {

        $this->load->model('m_izin_pembuangan');

        # get detail lhu
        $id_lhu = $this->input->post('id');       
        $detail = $this->m_detail_lhu->get_detail_by_id($id_lhu);        
        $pembuangan = $this->m_izin_pembuangan->get_izin_pembuangan(array('id_industri' => $detail->id_industri));      

        # get detail parameter        
        $jenis_lhu = ($detail->jenis_lhu == 'LHU Air Limbah') ? "Air Limbah" : "Udara Emisi";     
        $parameter = $this->m_detail_lhu->get_parameter_izin(' b.id_industri = '.$detail->id_industri.' and b.jenis_baku_mutu = "'.$jenis_lhu.'"', $detail->id_laporan_hasil_uji);
        
        $standard = array();
        foreach ($parameter as $k => $v) {
            $standard[$v->id_baku_mutu_izin] = array(
                    'nama_parameter' => $v->ket,
                    'nilai_baku_mutu' => $v->nilai_baku_mutu,
                    'dasar_hukum' => ''
                );
        }

        $param_lhu = $this->input->post('param_lhu');
        $metode = $this->input->post('metode');  

        $pelanggaran = array();
        $nilai_standard = '';

        foreach ($param_lhu as $k => $v) {

            $status = 0;
            $nilai_standard = $standard[$k]['nilai_baku_mutu'];
            $nama_parameter = $standard[$k]['nama_parameter'];
            $dasar_hukum = $pembuangan->dasar_hukum;

            if(strtolower($v) != 'tt' and strtolower($v) != 'td') {                 
                // if($k == 59) { echo $v; exit(); } 
                if (strpos($nilai_standard, '-') !== false) {
                    $tmp = explode('-', $nilai_standard);
                    $min = (float)$tmp[0]; $max = (float)$tmp[1];                
                    if(!((float)$v > $min and (float)$v <= $max)) {
                        $status = 1;                
                        $pelanggaran[$k] = array($v, $nilai_standard, $nama_parameter, $dasar_hukum);
                    }
                }else{
                    // echo $v.'-'.$nilai_standard."<br/>";
                    if((float)$v > (float)$nilai_standard) {  
                        $status = 1;                           
                        $pelanggaran[$k] = array($v, $nilai_standard, $nama_parameter, $dasar_hukum);
                    }
                }
            }           

            # parameter detail lhu
            $param_detail_lhu = array(
                    'id_laporan_hasil_uji' => $id_lhu,
                    'id_baku_mutu_izin' => $k,
                    'nilai_hasil_uji' => $v,
                    'nilai_baku_mutu' => (($nilai_standard != '') ? (string)$nilai_standard : '0'),
                    'status_pelanggaran' => $status,
                    'metode_pengujian' => (isset($metode[$k]) ? $metode[$k] : ''),
                    'tgl_pembuatan' => date("Y-m-d"),
                    'tgl_update' => date("Y-m-d"),
                    'dibuat_oleh' => $this->tank_auth->get_personname(),
                    'diupdate_oleh' => $this->tank_auth->get_personname()
                );
            $this->m_detail_lhu->insert_detail_izin($param_detail_lhu);      
        }

        // $this->functions->debug_var($pelanggaran);

        $this->m_detail_lhu->clear_history_pelanggaran($id_lhu);

        if(count($pelanggaran) > 0) {
            foreach ($pelanggaran as $key => $value) {
                $param_pelanggaran = array(
                        'id_laporan_hasil_uji' => $id_lhu,
                        'parameter_lhu' => $value[2],
                        'dasar_hukum'=> $value[3],
                        'nilai_hasil_uji' => $value[0],
                        'nilai_baku_mutu' => $value[1],
                        'tgl_pembuatan' => date("Y-m-d"),
                        'tgl_update' => date("Y-m-d"),
                        'dibuat_oleh' => $this->tank_auth->get_personname(),
                        'diupdate_oleh' => $this->tank_auth->get_personname()
                    );                
                $this->m_detail_lhu->insert_pelanggaran($param_pelanggaran);
            }
        }

        $this->session->set_flashdata('error', $pelanggaran);
        $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));

        redirect($_SERVER['HTTP_REFERER']);

    }

    public function get_detail_lhu() {
        # get parameter pagination
        $input = array('dataperpage', 'query', 'curpage');
        foreach ($input as $val)
            $$val = $this->input->post($val); 
        
        # ambil detail lhu untuk menentukan baku mutu mana yang digunakan        
        $lhu = $this->get_lhu($this->input->post('id'));
        $where = "d.id_laporan_hasil_uji = ".$lhu->id_laporan_hasil_uji;

        if($lhu->jenis_baku_mutu == "Baku Mutu Izin") {

            // $jenis_lhu = ($lhu->jenis_lhu == 'LHU Air Limbah') ? "Air Limbah" : "Udara Emisi";                        
            $jenis_lhu = $this->get_jenis_lhu($lhu->jenis_lhu);
            $total = $this->m_detail_lhu->get_num_rows_izin($where, $jenis_lhu);
            $npage = ceil($total / $dataperpage);

            $start = $curpage * $dataperpage;       
            $end = $start + $dataperpage;            
            $jml_baku_mutu = count($this->m_detail_lhu->get_parameter_izin(' b.id_industri = '.$lhu->id_industri.' and b.jenis_baku_mutu = "'.$jenis_lhu.'"', $lhu->id_laporan_hasil_uji));

            $types = $this->m_detail_lhu->get_all_izin($where, $jenis_lhu, $dataperpage, $start);
            $data = array(
                'data' => $types,
                'sisa' => $jml_baku_mutu - $total,
                'pagination' => '',
                'numpage' => $npage - 1,
                'total' => $total
            );

            $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

        }else{
            
            $total = $this->m_detail_lhu->get_num_rows($where);
            $npage = ceil($total / $dataperpage);

            $start = $curpage * $dataperpage;       
            $end = $start + $dataperpage;

    	    $jml_baku_mutu = count($this->m_detail_lhu->get_lhu_by_id($this->input->post('id')));         
            $types = $this->m_detail_lhu->get_all($where, $dataperpage, $start);
            $data = array(
                'data' => $types,
                'sisa' => $jml_baku_mutu - $total,
                'pagination' => '',
                'numpage' => $npage - 1,
                'total' => $total
            );

            $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

        }

        echo json_encode($data);
        exit();
    }

    public function get() {    	
    	echo json_encode($this->m_detail_lhu->get_data_by_id('id_detail_lhu = '.$this->input->post('id_detail_lhu')));
        exit();
    } 

    protected function get_lhu($id_lhu) {
        return $this->m_lhu_air_limbah->get_detail_by_id($id_lhu);
    }

    protected function get_jenis_lhu($jenis_baku_mutu) {
        switch ($jenis_baku_mutu) {
            case 'LHU Air Limbah': return "Air Limbah"; break;
            case 'LHU Udara Emisi': return "Udara Emisi"; break;            
        }
        return FALSE;
    }

}
?>