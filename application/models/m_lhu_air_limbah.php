<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_lhu_air_limbah extends CI_Model {

	private $table_name = 'laporan_hasil_uji';

	function __construct() {
		parent::__construct();

		$this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');		
	}

	function get_all($where='', $limit=0, $offset=0) {

		$data = array();

		// SELECT al . * , i.nama_industri, l.nama_lab, j.ket AS jenis_dok
		// FROM laporan_hasil_uji al
		// LEFT JOIN industri i ON al.id_industri = i.id_industri
		// INNER JOIN laboratorium l ON al.id_lab = l.id_lab
		// INNER JOIN jenis_dokumen j ON al.id_jenis = j.id_jenis
		// WHERE jenis_lhu LIKE
		// BINARY 'lhu_air_limbah'
		// LIMIT 0 , 30

		$this->db->select('al.*, i.nama_industri, l.nama_lab, (select count(*) from detail_lhu where id_laporan_hasil_uji = al.id_laporan_hasil_uji) as detail, (select count(*) from history_item_teguran_lhu where id_laporan_hasil_uji = al.id_laporan_hasil_uji) as teguran');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_industri = i.id_industri', 'inner');
		$this->db->join('laboratorium as l', 'al.id_lab = l.id_lab', 'inner');
		
		if(isset($where) and $where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		
		$this->db->order_by("al.tgl_pembuatan", "desc"); 
		
		$query = $this->db->get();		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {

		$this->db->select('al.*, i.nama_industri, l.nama_lab');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_industri = i.id_industri', 'inner');
		$this->db->join('laboratorium as l', 'al.id_lab = l.id_lab', 'inner');		

		if(isset($where) and $where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}

	function get_detail_by_id($id) {
		$this->db->select('l.*, (select nama_industri from industri where id_industri = l.id_industri) as nama_industri');
		$this->db->where('l.id_laporan_hasil_uji', $id);
		
		$query = $this->db->get($this->table_name.' as l');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_detail($where='') {

		$data = array();

		$this->db->select('*');
		$this->db->from('laporan_hasil_uji');
		$this->db->where($where);

		$query = $this->db->get();		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function insert($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function change_lhu($data, $id) {

		$this->db->where("id_laporan_hasil_uji", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}

	function delete_lhu($id) {
		$this->db->query('call hapus_laporan_hasil_uji('.$id.')');		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}

	function is_duplicate($id_industri, $laporan_bulan) {
		$this->db->where('id_industri = '.$id_industri." and laporan_bulan_tahun = '".$laporan_bulan."' and jenis_lhu = 'LHU Air Limbah'");
		$query = $this->db->get('laporan_hasil_uji');
		return ($query->num_rows() > 0) ? FALSE : TRUE;
	}

	function get_jumlah_teguran($id_industri) {
		$sql = "select * from history_item_teguran_lhu h where h.id_laporan_hasil_uji in (select id_laporan_hasil_uji from laporan_hasil_uji where jenis_lhu = 'LHU Air Limbah' and id_industri = ".$id_industri.")";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) return TRUE;
		return FALSE;
	}
}
?>