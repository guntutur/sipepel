<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">PERIODE ADMINISTRASI DOKUMEN&nbsp;&nbsp;<small>mengelola seluruh data periode administrasi dokumen</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Periode Administrasi Dokumen</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <!-- <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>         -->
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="well-info well-sm">                            
                            <span class="fa fa-info-circle fa-lg"></span>&nbsp;&nbsp;Seluruh dokumen yang terdaftar diberikan toleransi pengumpulan selama <strong>15 hari</strong>.
                        </div>
                    </div>
                </div>
                                
                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="10%">Kategori Dokumen</th>
								<th width="5%" class="text-center">#</th>
								<th>Nama Dokumen</th>
								<th width="10%">Periode Pencatatan</th>
                                <th width="10%">Periode Pelaporan Dokumen</th>
                                <!-- <th width="10%">Toleransi Teguran</th> -->
								<th width="10%">Jumlah Data/Tahun</th>
                                <th width="5%">Status</th>
                                <!-- <th class="text-right" width="5%"></th>									 -->
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                 <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm" style="margin: 0!important;">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/jenis_dokumen/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
					var kategori;
					var nomer = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var status = (dt.status == 1) ? 'Aktif': 'Tidak Aktif';
						var kat_dok = (dt.kategori_dokumen != kategori ) ? dt.kategori_dokumen : ' ';
						var no_dok = (dt.kategori_dokumen == kategori ) ? (nomer+=1) : nomer = 1;
                        t += '<tr>';
						t += '<td>'+kat_dok+'</td>';
						t += '<td class="text-center">'+no_dok+'</td>';
                        t += '<td>'+dt.ket+'</td>';
						t += '<td>'+dt.periode_pencatatan+'</td>';
                        t += '<td>Setiap '+((dt.periode_bulan == 'triwulan') ? '3' : '6')+' Bulan</td>';
						// t += '<td>'+dt.toleransi_hari+' Hari</td>';
						t += '<td>'+dt.jml_data_tahun+' Dokumen</td>';
                        t += '<td>'+status+'</td>';
                        // t += '<td class="text-right">';
                        // t += '<div class="btn-group">';
                        // t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        // t += '<ul class="dropdown-menu pull-right" role="menu">';
                        // t += '<li><a onclick="$(this).edit('+dt.id_jenis+')">Ubah Data</a></li>';
                        // t += '<li><a onclick="$(this).delete('+dt.id_jenis+')">Hapus Data</a></li>';
                        // t += '</ul></div></td>';
                        t += '</tr>';        
						kategori = dt.kategori_dokumen;						
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

	$(document).ready(function () {
		
		$("#form_add").validate({
            rules: {
                keterangan: {
                    required: true,
					minlength: 0,
                    maxlength: 250
                },
				status: {
					required: true
				},
				periode_bulan: {
                    required: true,
					// minlength: 0,
     //                maxlength: 10
                },
				// toleransi_hari: {
				// 	required: true,
				// 	minlength: 0,
    //                 maxlength: 10
				// },
				periode_pencatatan: {
					required: true,
					// minlength: 0,
     //                maxlength: 100
				},
				kategori_dokumen: {
					required: true,
					minlength: 0,
                    maxlength: 500
				},
				jml_data_tahun: {
					required: true,
					minlength: 0,
                    maxlength: 100
				} 
            },
            messages: {        
                keterangan: {
                    required: "Masukan deskripsi jenis dokumen.",
                    minlength: "panjang deskripsi minimal 0 karakter.",
                    maxlength: "panjang deskripsi maksimal 255 karakter."
                },
				status: {
                    required: "Silahkan pilih data."
                },
				periode_bulan: {
                    required: "Masukan periode bulan jenis dokumen.",
                    // minlength: "panjang deskripsi minimal 0 digit.",
                    // maxlength: "panjang deskripsi maksimal 10 digit."
                },
				periode_pencatatan: {
                    required: "Masukan periode pencatatan jenis dokumen.",
                    // minlength: "panjang deskripsi minimal 0 digit.",
                    // maxlength: "panjang deskripsi maksimal 100 digit."
    //             },
				// toleransi_hari: {
    //                 required: "Masukan toleransi hari jenis dokumen.",
    //                 minlength: "panjang deskripsi minimal 0 digit.",
    //                 maxlength: "panjang deskripsi maksimal 10 digit."
    //             },
                },
				kategori_dokumen: {
                    required: "Silahkan pilih data."
                },
				jml_data_tahun: {
                    required: "Masukan jml data tahun jenis dokumen.",
                    minlength: "panjang deskripsi minimal 0 digit.",
                    maxlength: "panjang deskripsi maksimal 100 digit."
                }
				
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
			

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/jenis_dokumen/get'); ?>",
                data: 'id_jenis=' + id,
                success: function (response) {
                    console.log(response);
                    $('input[id=edit_id]').val(response.id_jenis);
                    $('input[name=keterangan]').val(response.ket);
                    $('select[name=status]').val(response.status);
					$('select[name=kategori_dokumen]').val(response.kategori_dokumen);                    
                    $('select[name=periode_bulan]').val(response.periode_bulan);
                    // $('input[name=toleransi_hari]').val(response.toleransi_hari);
					$('input[name=jml_data_tahun]').val(response.jml_data_tahun);
					$('select[name=periode_pencatatan]').val(response.periode_pencatatan);

                    $('#myModal').modal('show');
                }
            })
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }    

        Document.search();    

	});
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Jenis Dokumen</h4>
            </div>
            <form role="form" data-action="<?php echo base_url('/backend/jenis_dokumen') ?>" method="post" id="form_add">
                <div class="modal-body">
					<div class="row">
						<div class="col-lg-6 form-group kategori_dokumen">
							<label>Kategori Dokumen</label>
							<select name="kategori_dokumen" id="kategori_dokumen" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Kategori Dokumen --</option>
								<?php 
									foreach ($this->functions->get_kategori_dokumen() as $key => $value) {
										echo '<option value="'.$key.'">'.$value.'</option>';
									}
								?>
							</select>   
						</div>
						<div class="col-lg-6 form-group periode_pencatatan">
							<label>Periode Pencatatan</label>
							<!-- <input type="text" name="periode_pencatatan" id="periode_pencatatan" class="form-control input-sm has-feedback numberonly" autofocus />									 -->
                            <select name="periode_pencatatan" id="periode_pencatatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Periode Pencatatan --</option>
                                <option value="Setiap Bulan">Setiap Bulan</option>
                                <option value="Setiap 3 Bulan">Setiap 3 Bulan</option>
                                <option value="Setiap 6 Bulan">Setiap 6 Bulan</option>
                                <option value="Setiap Diperlukan">Setiap Diperlukan</option>
                            </select> 
						</div>
					</div>
                    <div class="form-group keterangan">
                        <label>Nama Dokumen</label>
                        <input type="text" name="keterangan" id="keterangan" class="form-control input-sm has-feedback" placeholder="Nama Dokumen" autofocus />                        
                    </div>
                    <div class="row">
                        <div class="col-lg-6 form-group periode_bulan">
                            <label>Periode Pelaporan Dokumen</label>
                            <!-- <div class="input-group">
                                <span class="input-group-addon">Per</span>
                                <input type="text" name="periode_bulan" id="periode_bulan" class="form-control has-feedback input-sm numberonly" autofocus>
                                <span class="input-group-addon">Bulan</span>
                            </div> -->                                  
                            <select name="periode_bulan" id="periode_bulan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Periode Dokumen --</option>
                                <option value="triwulan">Setiap 3 Bulan (Triwulan)</option>
                                <option value="semester">Setiap 6 Bulan (Semester)</option>
                            </select> 
                        </div>
                        <!-- <div class="col-lg-6 form-group toleransi_hari">
                            <label>Toleransi Teguran</label>                        
                            <div class="input-group">
                                <input type="text" name="toleransi_hari" id="toleransi_hari" class="form-control has-feedback input-sm numberonly" autofocus>
                                <span class="input-group-addon">Hari </span>
                            </div>
                        </div> -->
                        <div class="col-lg-6 form-group jml_data_tahun">
                            <label>Jumlah Data/Tahun</label>                        
                            <input type="text" name="jml_data_tahun" id="jml_data_tahun" class="form-control input-sm has-feedback numberonly" placeholder="0" />
                        </div>
                    </div>
					<div class="row">						
						<div class="col-lg-6 form-group status">
							<label>Status</label>
							<select name="status" id="status" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Status --</option>
								<?php 
									$status = $this->functions->get_status();
									foreach($status as $k => $s) {
										echo '<option value="'.$k.'">'.$s.'</option>';
									}
								?>                      
							</select>							
						</div>
                        <div class="col-lg-6"></div>
					</div>
											
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Jenis Dokumen</h4>
            </div>
            <form action="<?php echo base_url('/backend/jenis_dokumen/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal 