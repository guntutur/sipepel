<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Rekapitulasi Administrasi LHU&nbsp;&nbsp;<small>rekapitulasi administrasi seluruh data laporan hasil uji</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Rekapitulasi</li>
                <li class="active">Rekapitulasi Administrasi</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

               <div class="row">                    
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />                            
                        </div>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <br/>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">                                                        
                            <tr>
                                <th width="4%">No</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th width="12%">Kualitas Air</th>
                                <th width="12%">Catatan Debit Air</th>
                                <th width="12%">Kualitas Udara Emisi</th>
                                <th width="12%">Kualitas Udara Ambien</th>
                                <th width="12%">Manifest</th>
                                <th width="12%">Nercara B3</th>
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
var Document = {
    param: {
        dataperpage: 10,
        query: '',
        curpage: 0,
        numpage: 0
    },
    url: '<?php echo base_url("backend/rekap_adm_swapantau/get_list"); ?>',
    search: function() {
        this.param.query = $('#query').val();
        this.param.curpage = 0;
        this.load_data();
        return false;
    },
    search_field: function(e) {

    },
    set_page: function(n) {
        this.param.curpage = n;
        this.load_data();
        return false;
    },
    prev_page: function() {
        if(this.param.curpage > 0) {
            this.param.curpage--;
            this.load_data();
        }
        return false;
    },
    next_page: function() {
        if(this.param.curpage < this.param.numpage) {
            this.param.curpage++;
            this.load_data();
        }
        return false;
    },
    load_data: function() {
        $.ajax({
            url: Document.url,
            type: 'POST',
            dataType: 'json',
            data: $.param(Document.param),
            success: function(d) {                
                $('#pagination').html(d.pagination);
                if(d.total < 10){ $('#start').text(d.total); }                    
                $('#nums').text(d.total);
                Document.param.numpage = d.numpage;
                var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;                

                $.each(d.data, function(i, v) {
                     /* iterate through array or object */
                    console.log(v);
                    t += '<tr>';
                    t += '<td>'+(no+=1)+'</td>';
                    t += '<td>'+v.nama_industri+'</td>';
                    t += '<td>'+$(this).parsing_data(v.kualitas_air)+'</td>';
                    t += '<td>'+$(this).parsing_data(v.catatan_debit)+'</td>';
                    t += '<td>'+$(this).parsing_data(v.kualitas_emisi)+'</td>';
                    t += '<td>'+$(this).parsing_data(v.kualitas_ambien)+'</td>';
                    t += '<td>'+$(this).parsing_data(v.manifest)+'</td>';
                    t += '<td>'+$(this).parsing_data(v.neraca_b3)+'</td>';                    
                    t += '</tr>';
                });
                
                $('#document-data').html(t);
            }
        })
        .fail(function(e) {
            console.log(e);
        });            
    }
} 

$(document).ready(function () {  
    Document.search();

    $.fn.parsing_data = function(array) {
        var label = '', res = '';
        $.each(array, function(i, v) {
            label = (i == 'triwulan') ? 'T' : 'S';
            $.each(v, function(ir, vr) {
                var value = (vr == "Belum Ada") ? '-' : vr;
                res += label + ir + ": " + value + '<br/>';
            });
        });
        return res;
    }
});
</script>