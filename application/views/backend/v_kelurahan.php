<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading"><?php echo $title; ?>&nbsp;&nbsp;<small>mengelola seluruh data kelurahan/desa</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
				<li>Kecamatan</li>
                <li class="active">Kelurahan/Desa</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">                
			
                <!-- <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/>           -->      

                <div class="row">
                    <div class="col-lg-6">
						<button type="button" onclick="document.location='<?php echo site_url('backend/kecamatan'); ?>';" class="btn btn-sm btn-info"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</button>
                        <button type="button" data-toggle="modal" data-target="#input_detail" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
								<th>Kelurahan/Desa</th>
                                <th width="20%">Status</th>                                
                                <th class="text-right" width="5%"></th>									
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="4">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0,
			id: '<?php echo $this->uri->segment(4); ?>'
        },
        url: '<?php echo base_url("backend/kelurahan/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);                    
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                        var status = (dt.status == 1) ? 'Aktif': 'Tidak Aktif';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
						t += '<td>'+dt.nama_kelurahan+'</td>';
                        t += '<td>'+status+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_kelurahan+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_kelurahan+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

	$(document).ready(function () {
		
		$("#form_add").validate({
            rules: {
                description: {
                    required: true,
					minlength: 0,
                    maxlength: 250
                },
				status: {
					required: true
				},
				kecamatan: {
					required: true
				}
            },
            messages: {        
                description: {
                    required: "Field tidak boleh kosong.",
                    minlength: "Field harus lebih dari 1 karakter.",
                    maxlength: "Field tidak boleh lebih dari 250 karakter."
                },
				status: {
                    required: "Field belum dipilih"
                },
				kecamatan: {
                    required: "Field belum dipilih"
                },
            }
        });


        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) { 
            // validator.resetForm();              
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/kelurahan/get'); ?>",
                data: $.param({id_kelurahan: id}),
                success: function (response) {
                    //console.log(response);
					$('#input_detail form')[0].reset();
                    $('#input_detail form input[type=hidden]').val('');
					$('input[id=detail_id]').val(response.id_kelurahan);
					
                    //$('input[id=edit_id]').val(response.id_kelurahan);
                    $('select[name=kecamatan]').val(response.id_kecamatan);
                    $('input[name=description]').val(response.ket);
                    $('select[name=status]').val(response.status);
                    $('#input_detail form').attr('action', '<?php echo base_url('/backend/kelurahan/edit') ?>'); //this fails silently
                    $('#input_detail form').get(0).setAttribute('action', '<?php echo base_url('/backend/kelurahan/edit') ?>'); //this works

                    $('#input_detail').modal('show');
                }
            })
			.fail(function() {
                console.log("error");
            });
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        

        Document.search();

	});
</script>

<!-- Modal -->
<div class="modal fade" id="input_detail" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="uploadLabel">Data Kelurahan/Desa</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/kelurahan/register') ?>" method="post" id="form_add">
                <div class="modal-body">																			
                    <div class="form-group description">
                        <label>Kelurahan/Desa</label>
                        <input type="text" name="description" id="description" class="form-control input-sm has-feedback" autofocus />                        	
                    </div>
					<!--<div class="form-group kecamatan">
						<label>Kecamatan</label>
						<select name="kecamatan" id="kecamatan" class="form-control input-sm">
							<option value="" disabled selected>-- Pilih Kecamatan --</option>
							<?php //foreach($kecamatan as $tmp): ?>
								<option value="<?php //echo $tmp->id_kecamatan;?>"><?php //echo $tmp->ket; ?></option>
							<?php //endforeach;?>                         
						</select>
                    </div>-->
                    <div class="form-group status">
                        <label>Status</label>
                        <select name="status" id="status" class="form-control input-sm">
                            <option value="" disabled selected>-- Pilih Status --</option>
                            <?php 
								$status = $this->functions->get_status();
								foreach($status as $k => $s) {
									echo '<option value="'.$k.'">'.$s.'</option>';
								}
							?>						
                        </select>
                        <!-- <span class="fa fa-cogs form-control-feedback"></span> -->
                    </div>															  						
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="detail_id" name="id" value="<?php echo $this->uri->segment(4)?>" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Kelurahan</h4>
            </div>
            <form action="<?php echo base_url('/backend/kelurahan/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->