<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_rekap_manifest extends CI_Model {

	private $table_name			= 'rekap_manifest';
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('al.*, 
		                  i.tlp, i.fax, i.email, i.nama_industri, i.badan_hukum, i.alamat, 
						  j.ket as usaha_kegiatan, 
						  k.ket as nama_jenis_industri, 
						  kel.ket as nama_kelurahan,
						  kec.ket as nama_kecamatan
						  ');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_penghasil_limbah = i.id_industri', 'left');
		$this->db->join('usaha_kegiatan as j', 'i.id_usaha_kegiatan = j.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri as k', 'i.id_jenis_industri = k.id_jenis_industri', 'left');
		$this->db->join('kelurahan as kel', 'i.id_kelurahan = kel.id_kelurahan', 'left');
		$this->db->join('kecamatan as kec', 'i.id_kecamatan = kec.id_kecamatan', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("i.badan_hukum", "asc"); 
		$this->db->order_by("i.nama_industri", "asc"); 
		$this->db->order_by("al.tgl_pembuatan", "desc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select('al.*, 
		                  i.tlp, i.fax, i.email, i.nama_industri, i.badan_hukum, i.alamat, 
						  j.ket as usaha_kegiatan, 
						  k.ket as nama_jenis_industri, 
						  kel.ket as nama_kelurahan,
						  kec.ket as nama_kecamatan
						  ');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_penghasil_limbah = i.id_industri', 'left');
		$this->db->join('usaha_kegiatan as j', 'i.id_usaha_kegiatan = j.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri as k', 'i.id_jenis_industri = k.id_jenis_industri', 'left');
		$this->db->join('kelurahan as kel', 'i.id_kelurahan = kel.id_kelurahan', 'left');
		$this->db->join('kecamatan as kec', 'i.id_kecamatan = kec.id_kecamatan', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_by_id($id) {
		$this->db->where('id_rekap_manifest', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete($id) {
		$data = $this->get_by_id($id);
		$this->db->where('id_rekap_manifest', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change($data, $id) {

		$this->db->where("id_rekap_manifest", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
}
?>