<?php

$lang = array(
    // MESSAGE
    'MESSAGE_INSERT_SUCCESS' => 'Data berhasil disimpan!',
    'MESSAGE_INSERT_FAILED' => 'Data gagal disimpan!',
    'MESSAGE_EDIT_SUCCESS' => 'Data berhasil diubah!',
    'MESSAGE_EDIT_FAILED' => 'Data gagal diubah!',
    'MESSAGE_OBJECT_NOT_FOUND' => 'Objek tidak ditemukan!',
    'MESSAGE_DELETE_CONFIRM' => 'Apakah Anda yakin akan menghapus data ini?',
    'MESSAGE_DELETE_SUCCESS' => 'Data berhasil dihapus!',
    'MESSAGE_DELETE_FAILED' => 'Data gagal dihapus!',
    'MESSAGE_DUPLICATE_DATA' => 'Data sudah tersedia!'   
);

/* End of file ./language/english/merchant_lang.php */