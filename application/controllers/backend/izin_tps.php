<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class izin_tps extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model
        $this->load->model('m_izin_tps');
        $this->load->model('m_industry');
		$this->load->model('m_data_master', '', TRUE);
		
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $data['industri'] = $this->m_industry->get_dropdown();

    	$this->load->view('include/header');
        $this->load->view('backend/v_izin_tps', $data);
        $this->load->view('include/footer');
    }

   public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				
			);

     	$total = $this->m_izin_tps->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_izin_tps->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_izin_tps->get_by_id($this->input->post('id_izin_tps')));
		exit();
	}

    public function register() {       

        date_default_timezone_set("Asia/Jakarta");
        $tgl_pembuatan = date("Y-m-d H:i:s");

        $param = array(                
                'id_industri' => $this->input->post('industri'),
                'no_izin' => $this->input->post('no_izin'),
				'status' => $this->input->post('status'),
                'tgl_izin' => date("Y-m-d", strtotime($this->input->post('tgl_izin'))),
				'tentang' => $this->input->post('tentang'),
                'tgl_pembuatan' => $tgl_pembuatan,
                'tgl_update' => $tgl_pembuatan,
                'dibuat_oleh' =>$this->tank_auth->get_personname(),
                'diupdate_oleh' => $this->tank_auth->get_personname()
            );
        
		
        if($this->m_izin_tps->insert($param))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
			
		redirect('backend/izin_tps');

    }
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'id_industri' => $this->input->post('industri'),
			'no_izin' => $this->input->post('no_izin'),
			'status' => $this->input->post('status'),
			'tgl_izin' => date('Y-m-d', strtotime($this->input->post('tgl_izin'))), 
			'tentang' => $this->input->post('tentang'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_izin_tps->change($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/izin_tps');
	}
	
	public function delete() {
		$this->m_izin_tps->delete($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/izin_tps');
	}
	
	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $izin = $this->m_izin_tps->get_izin_tps_detail_by_id($id);
		
		$jenis_data_master = "jenis_limbah";
		$data['data_acuan'] = $this->m_data_master->get_dropdown($jenis_data_master);

        $badan_hukum = $izin->badan_hukum;
		$industri = $izin->nama_industri;

        $data['title'] = "DATA TPS LIMBAH B3 ".$badan_hukum." ".$industri;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_detail_izin_tps', $data);
        $this->load->view('include/footer');
	}
	
	public function get_detail() {	
		$data = $this->m_izin_tps->get_detail_tps_by_id($this->input->post('id_tps_limbah'));
		$jenis_limbah = ($data->jenis_limbah_b3) ? unserialize($data->jenis_limbah_b3) : array();        
        $data->jenis_limbah_b3 = $jenis_limbah;
		
		echo json_encode($data);
        exit();
	}

	public function get_detail_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		/*$id_query = $id;*/
        /*$baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($id_query);*/

		
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = "id_izin_tps = ".$id ;
    
     	$total = $this->m_izin_tps->get_detail_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_izin_tps->get_detail_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }

	public function insert_detail() {
		$jenis_limbah_b3 = $this->functions->parsing_jenis_limbah_tps($this->input->post());
		if($this->validate_detail()) {
			$param = array(
					'id_izin_tps' => $this->input->post('id'),
					'jenis_limbah_b3' => $jenis_limbah_b3,
					'lama_penyimpanan' => $this->input->post('lama_penyimpanan'),
					'luas_tps' => $this->input->post('luas_tps'),
					'derajat_s' => $this->input->post('derajat_s'),
					'jam_s' => $this->input->post('jam_s'),
					'menit_s' => $this->input->post('menit_s'),
					'derajat_e' => $this->input->post('derajat_e'),
					'jam_e' => $this->input->post('jam_e'),
					'menit_e' => $this->input->post('menit_e')
				);

			if($this->m_izin_tps->insert_detail($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		// redirect('backend/debit_air_limbah/detail/'.$this->input->post('id')."/".);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function edit_detail() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		
		$jenis_limbah_b3 = $this->functions->parsing_jenis_limbah_tps($this->input->post());
		
		$param = array(			
				'jenis_limbah_b3' => $jenis_limbah_b3,
				'lama_penyimpanan' => $this->input->post('lama_penyimpanan'),
				'luas_tps' => $this->input->post('luas_tps'),
				'derajat_s' => $this->input->post('derajat_s'),
				'jam_s' => $this->input->post('jam_s'),
				'menit_s' => $this->input->post('menit_s'),
				'derajat_e' => $this->input->post('derajat_e'),
				'jam_e' => $this->input->post('jam_e'),
				'menit_e' => $this->input->post('menit_e')
				//'koordinat_tps' => $koor_tps
			);
		
		if($this->m_izin_tps->change_detail_tps($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_detail() {
		$this->m_izin_tps->delete_detail_tps($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function validate_detail() {
		# check data yang duplicate
		if($this->m_izin_tps->is_duplicate_detail($this->input->post('id'), $this->input->post('luas_tps'), $this->input->post('lama_penyimpanan'), $this->input->post('koordinat_tps'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}
	
	
}
?>