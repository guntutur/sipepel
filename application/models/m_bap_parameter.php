<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_bap_parameter extends CI_Model {

	private $table_name			= 'parameter_bap';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select("p.urutan, p.id_parameter_bap, p.ket as nama_parameter, p.nilai_parameter_bap, p.dasar_hukum, p.status, u.id_jenis_bap, u.ket as nama_usaha");
		$this->db->from($this->table_name." p");
		$this->db->join('jenis_bap u', 'u.id_jenis_bap = p.id_jenis_bap', 'left');

		if($where != '') {
			$this->db->where($where);
		}
		
		/*foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}*/

		$this->db->limit($limit, $offset);
		$this->db->order_by("p.urutan", "asc");  

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	
	function get_all_jenis_bap($where=array(), $limit=0, $offset=0) {

		$data = array();
		
		$this->db->select('d.*, (select count(*) from parameter_bap dd where dd.id_jenis_bap = d.id_jenis_bap and dd.nilai_parameter_bap != "-" and dd.nilai_parameter_bap != "") as jumlah');
		$this->db->from('jenis_bap as d');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("ket", "asc"); 

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	

	function get_num_rows($where) {
		$data = array();

		$this->db->select("p.urutan, p.id_parameter_bap, p.ket as nama_parameter, p.nilai_parameter_bap, p.dasar_hukum, p.status, u.id_jenis_bap, u.ket as nama_usaha");
		$this->db->from($this->table_name." p");
		$this->db->join('jenis_bap u', 'u.id_jenis_bap = p.id_jenis_bap', 'left');

		if($where != '') {
			$this->db->where($where);
		}
		
		/*foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}*/
		return $this->db->count_all_results();
	}
	
	function get_num_rows_jenis_bap($where) {
		$data = array();

		$this->db->select('d.*, (select count(*) from parameter_bap dd where dd.id_jenis_bap = d.id_jenis_bap and dd.nilai_parameter_bap != "-" ) as jumlah');
		$this->db->from('jenis_bap as d');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}
	
	
	function get_usaha_kegiatan_by_id($id) {
		$this->db->where('id_jenis_bap', $id);
		
		$query = $this->db->get('jenis_bap');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_bap_parameter_by_id($id) {
		$this->db->where('id_parameter_bap', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_bap_parameter($id) {
		$data = $this->get_bap_parameter_by_id($id);
		$this->db->where('id_parameter_bap', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_bap_parameter($data, $id) {

		$this->db->where("id_parameter_bap", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_bap_parameter($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function is_duplicate($id, $name, $parent) {
		$this->db->where('id_jenis_bap', $id);
		$this->db->where('ket', $name);
		$this->db->where('id_parent', $parent);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	function get_parameter($id)
    {
		$query = $this->db->query('select id_parameter_bap, ket, urutan 
									from parameter_bap
									where status = 1 AND id_jenis_bap = "'.$id.'"
									order by urutan asc');
        $num = $query->num_rows();
        if($num>0){
            return $query->result();
        }
        else{
            return 0;
        }
    }
	
}
?>