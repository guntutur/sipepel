<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Controller {

	private $path = '../assets/img/avatar/';

    function __construct() {
        parent::__construct();

        $this->load->model('users_management', '', TRUE);
        $this->load->model('users', '', TRUE);
        
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        $this->load->helper('email');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all users
        $users = $this->users_management->get_all();
		
		# response paramter
        $data['users'] = $users;        
		
		# load view
        $this->load->view('include/header');        
        $this->load->view('backend/users', $data);
        $this->load->view('include/footer');
    }
	
	public function get() {		
		echo json_encode($this->users_management->get_user_by_id($this->input->post('id')));
		exit();
	}

	public function get_password() {		
		
		$user_id = $this->session->userdata('user_id');

		if (!is_null($user = $this->users->get_user_by_id($user_id, TRUE))) {

			// Check if old password correct
			$hasher = new PasswordHash(
					$this->config->item('phpass_hash_strength', 'tank_auth'),
					$this->config->item('phpass_hash_portable', 'tank_auth'));
			if ($hasher->CheckPassword($this->input->post('password'), $user->password)) {			// success				
				echo json_encode(array('success' => TRUE, 'msg' => 'Sandi cocok'));
				exit();
			}
		}
		echo json_encode(array('success' => FALSE, 'msg' => 'Sandi tidak cocok'));
		exit();
	}
	
	public function edit() {

		$id = ($this->input->post('id')) ? $this->input->post('id') : $this->input->post('edit_person_id');

		$param['users'] = array(
			'name' => $this->input->post('personname'),
			'username' => $this->input->post('username'),
			'activated' => $this->input->post('activated'),
			'email' => $this->input->post('email')
		);

		if($this->input->post('user_level')) $param['group_id'] = $this->input->post('user_level');

		$update_user = $param;

		$image = $this->functions->uploaded_image("images", $this->path);
        if (isset($image['error'])) {
            $this->error['image'] = $image['error'];
            echo $this->error['image'];
        }else{
            $this->thumbs = $image['upload_data']['file_name'];
        }

		if ($this->thumbs) {
			$o = $this->users_management->get_users_profile($id);
			if($o->avatar!='avatar-1.jpg'){
				$this->functions->delete_image($o->avatar, $this->path);
			}
			$update_user['user_profile'] = array('avatar' => $this->thumbs);
		}
		
		if($this->users_management->change_user($update_user, $id))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		
		if($this->tank_auth->is_manager() or $this->tank_auth->is_admin())
			redirect('backend/user');
		else
			redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function force_reset() {
	
		$hasher = new PasswordHash(
					$this->config->item('phpass_hash_strength', 'tank_auth'),
					$this->config->item('phpass_hash_portable', 'tank_auth'));
		$hashed_password = $hasher->HashPassword($this->input->post('password'));
		
		$param = array('password' => $hashed_password);
		
		if ($this->users_management->change_user(array('users' => $param), $this->input->post('id')))		
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Sandi berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Sandi gagal diperbaharui'));
		redirect('backend/user');
	}
	
	public function delete() {
		$this->users->delete_user($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/user');
	}

	public function profil_user() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $data = array();

		$this->load->view('include/header');        
        $this->load->view('backend/profil_user', $data);
        $this->load->view('include/footer');
	}

	public function check_username() {
        $username = trim($this->input->get('username'));
        if ($username == '') {
            echo json_encode(array('success' => FALSE, 'msg' => 'Username minimal 3 karakter'));
            exit();
        }
        if ($users = $this->users_management->get_username($username)) {
            if ($users->id == $this->tank_auth->get_user_id()) {
                echo json_encode(array('success' => TRUE, 'msg' => ''));
                exit();
            }
            echo json_encode(array('success' => FALSE, 'msg' => 'Username sudah ada'));
            exit();
        }
        echo json_encode(array('success' => TRUE, 'msg' => ''));
        exit();
    }

    public function check_email_address() {
        $email = trim($this->input->get('email'));
        if ((!valid_email($email)) OR ($email == '')) {
            echo json_encode(array('success' => FALSE, 'msg' => 'Alamat email tidak valid'));
            exit();   
        }
        else {
            if ($users = $this->users_management->get_email($email)) {
                if ($users->id == $this->tank_auth->get_user_id()) {
                    echo json_encode(array('success' => TRUE, 'msg' => ''));
                    exit();
                }
                echo json_encode(array('success' => FALSE, 'msg' => 'Alamat email sudah ada'));
                exit();
            }
            echo json_encode(array('success' => TRUE, 'msg' => ''));
            exit();
        }
    }

}
