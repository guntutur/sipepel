			<!-- BEGIN TOP NAV -->
			<div class="top-navbar primary-color">
				<div class="top-navbar-inner">
					
					<!-- Begin Logo brand -->
					<a href="<?php base_url(); ?>">
						<div class="logo-brand"></div>
					</a>
					
					<!-- End Logo brand -->
					
					<div class="top-nav-content">
						
						<!-- Begin button sidebar left toggle -->
						<div class="btn-collapse-sidebar-left">
							<i class="fa fa-bars"></i>
						</div><!-- /.btn-collapse-sidebar-left -->
						<!-- End button sidebar left toggle -->					
						
						<!-- Begin button nav toggle -->
						<div class="btn-collapse-nav" data-toggle="collapse" data-target="#main-fixed-nav">
							<i class="fa fa-plus icon-plus"></i>
						</div><!-- /.btn-collapse-sidebar-right -->
						<!-- End button nav toggle -->
						
						
						<!-- Begin user session nav -->
						<ul class="nav-user navbar-right">
							<li class="dropdown">
							  <a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
								<!-- <img id="avatar" src="<?php echo base_url('assets/img/avatar/avatar-1.jpg'); ?>" class="avatar img-circle" alt="Avatar"> -->
								Halo, <strong><?php echo $this->tank_auth->get_personname(); ?></strong> &nbsp;&nbsp;
								<i class="fa fa-caret-down"></i>
							  </a>
							  
							  <ul class="dropdown-menu">
									<li>
										<div class="navbar-content">
											<div class="row">
												<div class="col-md-5">
													<img id="avatar" src="<?php echo base_url('assets/img/avatar/avatar-1.jpg'); ?>"
														alt="Alternate Text" class="img-responsive" style="border: 1px solid #ccc;" />
													<p class="text-center small">														
												</div>
												<div class="col-md-7">
													<span><?php echo $this->tank_auth->get_personname(); ?></span>
													<p class="text-muted small"><?php echo $this->tank_auth->get_email_address(); ?></p>													
													<div class="divider">
													</div>
													<a id="account_setting" class="btn btn-primary btn-sm active">Lihat Profil</a>
												</div>
											</div>
										</div>
										<div class="navbar-footer">
											<div class="navbar-footer-content">
												<div class="row">
													<div class="col-md-6">
														<a data-toggle="modal" data-target="#change_password" class="btn btn-default btn-sm"><i class="fa fa-lock"></i>&nbsp;&nbsp;Ubah Password</a>
													</div>
													<div class="col-md-6">
														<a href="<?php echo base_url('/auth/logout'); ?>" class="btn btn-danger btn-sm pull-right"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Keluar</a>
													</div>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</li>

						</ul>
						<!-- End user session nav -->
					
						<!-- Begin Collapse menu nav -->
						<div class="collapse navbar-collapse" id="main-fixed-nav">							
							<ul class="nav navbar-nav navbar-left">
								<!-- Begin nav notification -->
								<li class="dropdown" id="notif_bap">
									<a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
										<span id="count_notif_bap"></span>
										<i class="fa fa-bell"></i>
									</a>
									<ul class="dropdown-menu square with-triangle">
										<li>
											<div class="nav-dropdown-heading">
											Notifikasi BAP
											</div><!-- /.nav-dropdown-heading -->
											<div class="nav-dropdown-custom scroll-nav-dropdown">
											<div id="loading_bap" style="display:none;"><div style="background-image: url('<?php echo site_url(); ?>assets/img/loading.gif');background-position: center center;background-repeat: no-repeat;height:50px;"></div></div>
												<ul>
													<div id="target_notif_bap"></div>
												</ul>
											</div><!-- /.nav-dropdown-content scroll-nav-dropdown -->
											<button onclick="window.location.href='<?php echo base_url()?>backend/teguran_bap'" class="btn btn-primary btn-square btn-block">Lihat Semua Notifikasi</button>
										</li>
									</ul>
								</li>
								<!-- End nav notification -->
								<!-- Begin nav task -->
								<li class="dropdown" id="notif_lhu">
									<a href="#fakelink" class="dropdown-toggle" data-toggle="dropdown">
										<span id="count_notif_lhu"></span>
										<i class="fa fa-tasks"></i>
									</a>
									<ul class="dropdown-menu square margin-list-rounded with-triangle">
										<li>
											<div class="nav-dropdown-heading">
											Notifikasi LHU
											</div><!-- /.nav-dropdown-heading -->
											<div class="nav-dropdown-custom scroll-nav-dropdown">
											<div id="loading_lhu" style="display:none;"><div style="background-image: url('<?php echo site_url(); ?>assets/img/loading.gif');background-position: center center;background-repeat: no-repeat;height:50px;"></div></div>
												<ul>
													<div id="target_notif_lhu"></div>
												</ul>
											</div><!-- /.nav-dropdown-content scroll-nav-dropdown -->
											<button onclick="window.location.href='<?php echo base_url()?>backend/teguran_lhu'" class="btn btn-primary btn-square btn-block">Lihat Semua Notifikasi</button>
										</li>
									</ul>
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
						<!-- End Collapse menu nav -->
					</div><!-- /.top-nav-content -->
				</div><!-- /.top-navbar-inner -->
			</div><!-- /.top-navbar -->			
			<!-- END TOP NAV -->

			<script type="text/javascript">

				$(document).ready(function($) {

					var img = '<?php echo $this->tank_auth->get_avatar(); ?>';					
					if(img != ""){
						images = "<?php echo base_url('assets/img/avatar'); ?>/" + img;						
						$('#avatar').attr('src', images);
					}

					$("abbr.timeago").livequery(function() {
						$(this).timeago(); // Calling Timeago Funtion 
					});

					$("#notif_bap").click(function() {
						var base_url = '<?php echo site_url();?>';

						$.ajax({
							url: base_url + 'backend/teguran_bap/get_notif_bap',
							type: 'GET',
							dataType: 'json',
							beforeSend: function() { $('#loading_bap').show(); },
                			complete: function() { $('#loading_bap').hide(); },
                			success : function(data){
                				// console.log(data.notif);
                				// console.log(data.jml_notif);
                				var container = $("#target_notif_bap");
                				var t = '';

                				if(data.notif_bap != null){
                					d = {}, no = 0;
                    				for (var i = 0; i < data.notif_bap.length; i++) {
	                        			d = data.notif_bap[i];

	                        			var tgl_buat = d.tgl.split('-');
	                        			var new_tgl = [tgl_buat[2], $(this).get_month(parseInt(tgl_buat[1])), tgl_buat[0]].join(' ');
	                        			var bdn = (d.bdn_hukum != '') ? d.bdn_hukum+' ' : '';

	                        			t += '<li class="unread" style="padding: 5px 20px;">';
	                        			t += '<div><strong>'+bdn+d.nama_industri+'</strong></div>';
	                        			t += '<div><small><span style="color: red;">'+d.total_error+' pelanggaran</span> pada tanggal '+new_tgl+'</small></div>';
	                        			
	                        			<?php if($this->tank_auth->is_manager() or $this->tank_auth->is_admin()) { ?>
		                        			if(d.conf_status==0){
		                        				t += '<div><small><span style="color: red;">Belum dikonfirmasi Operator BAP</span></small></div>';
		                        			} else {
		                        				t += '<div><small><span style="color: green;">Sudah dikonfirmasi Operator BAP</span></small></div>';
		                        			}
		                        		<?php } else { ?>
		                        			t += '<div><small><span style="color: '+((d.perbaikan==0) ? 'red' : 'green')+';">'+d.perbaikan+' pelanggaran</span> Sudah diperbaiki</small></div>';
		                        		<?php } ?>

	                        			t += '<div><small class="small-caps" style="color: #aab2bd;">Dibuat oleh '+d.maker+' - <abbr class="timeago" title="'+d.tgl+'"></abbr></small></div>';
	                        			t += '</li>';

	                        			// t += '<li class="unread"><a href="#fakelink">';
	                        			// t += '<strong>'+bdn+' '+d.nama_industri + '</strong> : <br>' + d.ket + ', '+d.total_error+ ' Pelanggaran<br> tanggal pembuatan '+new_tgl+'';
	                        			// t += '<span class="small-caps-custom">Dibuat oleh <srtong>'+d.maker+'</srtong> <abbr class="timeago" title="'+d.tgl+'"></abbr></span>';
	                        			// t += '</a></li>';
                					}
                				}
                				container.html(t);
                			}
						})
					});

					$.getJSON('<?php echo site_url("backend/teguran_bap/count_notif_bap"); ?>', function(json, textStatus) {
						// console.log(textStatus);
						if(textStatus) {
							$('#count_notif_bap').html('<span class="badge badge-danger icon-count">'+json.jml_notif_bap+'</span>');
						}
					});

					$("#notif_lhu").click(function() {
						var base_url = '<?php echo site_url();?>';

						$.ajax({
							url: base_url + 'backend/teguran_lhu/get_notif_lhu',
							type: 'GET',
							dataType: 'json',
							beforeSend: function() { $('#loading_lhu').show(); },
                			complete: function() { $('#loading_lhu').hide(); },
                			success : function(data){
                				// console.log(data.notif);
                				// console.log(data.jml_notif);
                				var container = $("#target_notif_lhu");
                				var t = '';

                				if(data.notif_lhu != null){
                					d = {}, no = 0;
                    				for (var i = 0; i < data.notif_lhu.length; i++) {
	                        			d = data.notif_lhu[i];

	                        			var teguran_air = (d.teguran_air!=0) ? d.teguran_air : '-';
					                    var teguran_emisi = (d.teguran_emisi!=0) ? d.teguran_emisi : '-';

					                    if(teguran_air == '-' && teguran_emisi == '-') { continue; }

	                        			var tgl_buat = d.tgl.split('.');
	                        			var new_tgl = [$(this).get_month(parseInt(tgl_buat[0])), tgl_buat[1]].join(' ');
	                        			var bdn = (d.bdn_hukum != '') ? d.bdn_hukum+' ' : '';

	                        			t += '<li class="unread" style="padding: 5px 20px;">';
	                        			t += '<div><strong>'+bdn+d.nama_industri+'</strong></div>';
	                        			if(teguran_air=='-'){
	                        				if((d.id_air_limbah != null))
		                        				t += '<div><small><span style="color: green;">LHU Air Limbah - Tidak melanggar</span></small></div>';
		                        			else
		                        				t += '<div><small><span>LHU Air Limbah - Tidak ada data</span></small></div>';
		                        		} else {
		                        			t += '<div><small><span style="color: red;">LHU Air Limbah - '+teguran_air+' pelanggaran</span> '+new_tgl+'</small></div>';
		                        		}

		                        		if(teguran_emisi=='-'){
		                        			if((d.id_udara_emisi != null))
		                        				t += '<div><small><span style="color: green;">LHU Udara Emisi - Tidak melanggar</span></small></div>';
		                        			else
		                        				t += '<div><small><span>LHU Udara Emisi - Tidak ada data</span></small></div>';
		                        		} else {
		                        			t += '<div><small><span style="color: red;">LHU Udara Emisi - '+teguran_emisi+' pelanggaran</span> '+new_tgl+'</small></div>';
		                        		}
	                        			t += '<div><small class="small-caps" style="color: #aab2bd;">Dibuat oleh '+d.maker+' - <abbr class="timeago" title="'+d.timeago+'"></abbr></small></div>';
	                        			t += '</li>';

	                        			// t += '<li class="unread"><a href="#fakelink">';
	                        			// t += '<strong>'+bdn+' '+d.nama_industri + '</strong> : <br>' + d.ket + ', '+d.total_error+ ' Pelanggaran<br> tanggal pembuatan '+new_tgl+'';
	                        			// t += '<span class="small-caps-custom">Dibuat oleh <srtong>'+d.maker+'</srtong> <abbr class="timeago" title="'+d.tgl+'"></abbr></span>';
	                        			// t += '</a></li>';
                					}
                				}
                				container.html(t);
                			}
						})
					});

					$.getJSON('<?php echo site_url("backend/teguran_lhu/count_notif_lhu"); ?>', function(json, textStatus) {
						// console.log(textStatus);
						if(textStatus) {
							$('#count_notif_lhu').html('<span class="badge badge-danger icon-count">'+json.jml_notif_lhu+'</span>');
						}
					});

					$('#change_password').on('show.bs.modal', function(e) {

						$("label.error").hide();
  						$(".error").removeClass("error");

  						$('#change_password form')[0].reset();
						$('#change_password form input[type=hidden]').val('');

						$('label.error').hide();
						$('#save').prop({disabled: true});						
					
					});

					$("#form_change").validate({
						rules: {
							old_password: {
								required: true,
								minlength: 4,
			                    maxlength: 100
							},
							new_password: {
								required: true,
								minlength: 4,
			                    maxlength: 100
							},
							confirm_new_password: {
								required: true,
								equalTo: "#new_password",
								minlength: 4,
			                    maxlength: 100
							}
						},
						messages: {
							old_password: {
			                    required: "Masukkan sandi lama Anda",
			                    minlength: "Sandi lama harus lebih dari 6 karakter.",
			                    maxlength: "Sandi lama tidak boleh lebih dari 100 karakter."
			                },
							new_password: {
			                    required: "Masukkan sandi baru",
			                    minlength: "Sandi baru harus lebih dari 6 karakter.",
			                    maxlength: "Sandi baru tidak boleh lebih dari 100 karakter."
			                },
							confirm_new_password: {
			                    required: "Masukkan konfirmasi sandi baru",
			                    minlength: "Sandi baru harus lebih dari 6 karakter.",
			                    maxlength: "Sandi baru tidak boleh lebih dari 100 karakter."
			                }
						},
			            errorElement: 'label',
			            errorClass: 'error',
			            errorPlacement: function(error, element) {
			                if(element.parent('.input-group').length) {
			                    error.insertAfter(element.parent());
			                } else {
			                    error.insertAfter(element);
			                }
			            }
					});

					$("#form_profil").validate({
						rules: {
							personname: {
								required: true,
								minlength: 1,
			                    maxlength: 100
							},
							username: {
								required: true,
								minlength: 4,
			                    maxlength: 100
							},
							email: {
								required: true,
								email: true,
								minlength: 4,
			                    maxlength: 100
							}
						},
						messages: {
							personname: {
			                    required: "Masukkan nama lengkap",
			                    minlength: "Nama lengkap harus lebih dari 1 karakter.",
			                    maxlength: "Nama lengkap tidak boleh lebih dari 100 karakter."
			                },
							username: {
			                    required: "Masukkan username",
			                    minlength: "Username harus lebih dari 4 karakter.",
			                    maxlength: "Username tidak boleh lebih dari 100 karakter."
			                },
							email: {
			                    required: "Masukkan email",
			                    email: "Masukkan email yang valid",
			                    minlength: "Email harus lebih dari 4 karakter.",
			                    maxlength: "Email tidak boleh lebih dari 100 karakter."
			                }
						},
			            errorElement: 'label',
			            errorClass: 'error',
			            errorPlacement: function(error, element) {
			                if(element.parent('.input-group').length) {
			                    error.insertAfter(element.parent());
			                } else {
			                    error.insertAfter(element);
			                }
			            }
					});

					$('#account_setting').click(function() {
						$('input[id=edit_person_id]').val('<?php echo $this->tank_auth->get_user_id(); ?>');
			            $('input[name=personname]').val('<?php echo $this->tank_auth->get_personname(); ?>');
			            $('input[name=username]').val('<?php echo $this->tank_auth->get_username(); ?>');
			            $('input[name=activated]').val('<?php echo $this->tank_auth->get_status(); ?>');
			            $('input[name=email]').val('<?php echo $this->tank_auth->get_email_address(); ?>');

			            var img = '<?php echo $this->tank_auth->get_avatar(); ?>';
			            var images = "<?php echo base_url('assets/img/avatar/avatar-1.jpg'); ?>";
			            if(img != ""){
			                images = "<?php echo base_url('assets/img/avatar'); ?>";
			                images = images +"/"+img;
			            }
			            $('#bleh').attr('src', images);

			            $("label.error").hide();
			            $("#edit_username_error_as").html('');
			            $("#edit_email_error_as").html('');
			            $("#edit_person_name").removeClass('error');
			            $("#save-account").prop("disabled", false);
			            $('#mdl_account_setting').modal('show');

					});

					$("#old_password").bind('blur keyup', function(event) {
						// $("#old_password-error").hide();
						$.ajax({
							url: "<?php echo base_url('/backend/user/get_password'); ?>",
							type: "POST",
							dataType: "json",
							data: {password: $(this).val()},
							success: function(data) {																
								if(data.success) {
			                        $('#old_password-error').text(data.msg);
			                        $("#old_password-error").attr('class', 'success');  
			                        
			                        // $("#save").prop("disabled", false);
			                         if (!$('#old_password-error').hasClass('error')) {
			                            $("#save").prop("disabled", false);
			                        }
			                        
			                    }else if(!data.success) {
			                        $('#old_password-error').text(data.msg);
			                        $("#old_password-error").attr('class', 'error');    

			                        $("#save").prop("disabled", true);                      
			                    }
			                    $('#old_password-error').show();  
							}
						});						
					});

					$("#username").bind('focus', function(){
			            if ($('#edit_email_error_as').hasClass('error')) {
			                $("#save-account").prop("disabled", true);
			            }
			        });

			        $("#username").bind('blur keyup', function(){
			            $.ajax({
			                type: 'POST',
			                dataType: 'json',
			                url: '<?php echo base_url('/backend/user/check_username/?username='); ?>' + $(this).val(),
			                success: function (response) {
			                    if(response.success) {
			                        $('#edit_username_error_as').html('');
			                        $("#edit_username_error_as").attr('class', 'success');
			                        if (!$('#edit_email_error_as').hasClass('error')) {
			                            $("#save-account").prop("disabled", false);
			                        }
			                    }
			                    else if(!response.success) {
			                        $('#edit_username_error_as').html('<strong>' + response.msg + '</strong>');
			                        $("#edit_username_error_as").attr('class', 'error');
			                        $("#save-account").prop("disabled", true);                      
			                    }         
			                }
			            });
			        });

			        $("#email").bind('focus', function(){
			            if ($('#edit_username_error_as').hasClass('error')) {
			                $("#save-account").prop("disabled", true);
			            }
			        });

			        $("#email").bind('blur keyup', function(){
			            $.ajax({
			                type: 'POST',
			                dataType: 'json',
			                url: '<?php echo base_url('/backend/user/check_email_address/?email='); ?>' + $(this).val(),
			                success: function (response) {       
			                    if(response.success) {
			                        $('#edit_email_error_as').html('');
			                        $("#edit_email_error_as").attr('class', 'success');
			                        if (!$('#edit_username_error_as').hasClass('error')) {
			                            $("#save-account").prop("disabled", false);
			                        }
			                    }
			                    else if(!response.success) {
			                        $('#edit_email_error_as').html('<strong>' + response.msg + '</strong>');
			                        $("#edit_email_error_as").attr('class', 'error');
			                        $("#save-account").prop("disabled", true);                      
			                    }
			                }
			            });
			        });

					$.fn.readURL = function(input) {

			            if (input.files && input.files[0]) {
			                var reader = new FileReader();

			                reader.onload = function (e) {
			                	// $('#bleh').nailthumb({width:108,height:108});
			                    $('#bleh').attr('src', e.target.result);                    
			                }

			                reader.readAsDataURL(input.files[0]);
			            }
			        }
			        $("#img").change(function(){ $(this).readURL(this); });

				});

			</script>
			
			<!-- Modal -->
			<div class="modal fade" id="change_password" tabindex="-1" role="dialog" aria-labelledby="change_passowrdLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-primary no-border">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Ubah Sandi</h4>
						</div>
						<form accept-charset="utf-8" role="form" action="<?php echo base_url('auth/change_password') ?>" method="post" id="form_change">
							<div class="modal-body">								
								<div class="form-group old_password">
			                        <label>Sandi Lama</label>
			                        <div class="input-group input-group-sm">                                
	                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
	                                    <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Sandi lama" />                                
	                                </div> 	                            
			                        <label id="old_password-error" class="success" for="old_password"></label>		                        
			                    </div>   
			                    <div class="row">
			                    	<div class="col-lg-6 form-group new_password">
				                        <label>Sandi Baru</label>				                        
				                        <div class="input-group input-group-sm">                                
		                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
		                                    <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Sandi baru" />                                
		                                </div> 				                      
				                    </div>
			                    	<div class="col-lg-6 form-group confirm_new_password">
				                        <label>Ulangi Sandi Baru</label>				                        
				                        <div class="input-group input-group-sm">                                
		                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
		                                    <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" placeholder="Ulangi Sandi baru" />                                
		                                </div> 				                    
				                    </div>  
			                    </div>		                                
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
								<button type="submit" id="save" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="mdl_account_setting" tabindex="-1" role="dialog" aria-labelledby="change_passowrdLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header bg-primary no-border">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">Profil Account</h4>
						</div>
						<form accept-charset="utf-8" role="form" action="<?php echo base_url('backend/user/edit') ?>" method="post" id="form_profil" enctype="multipart/form-data">
							<div class="modal-body">	
								<div class="row">
				                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				                    	<a href="#" class="thumbnail">
			                                <img src="<?php echo base_url('assets/img/avatar/avatar-1.jpg'); ?>" alt="no-images" id="bleh" width="108px">                                
			                            </a>                             
			                            <span class="btn btn-primary btn-file btn-block btn-sm">
			                                Browse <input type="file" name="images" id="img" />
			                            </span>
				                    </div>
				                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				                    	<div>
				                    		<input type="hidden" name="activated" id="activated" class="form-control input-sm has-feedback" />
				                    	</div>
				                    	 <div class="form-group personname">
					                        <label>Nama Lengkap</label>
					                        <input type="text" name="personname" id="personname" class="form-control input-sm has-feedback" autofocus placeholder="Nama Lengkap" />                                             

					                    </div>
					                    <div class="form-group username">
					                        <label>Username</label>
					                        <input type="username" name="username" id="username" class="form-control input-sm has-feedback" placeholder="Username" />
					                        <span id="edit_username_error_as"></span>
					                    </div>
					                    <div class="form-group email">
					                        <label>Email</label>
					                        <input type="email" name="email" id="email" class="form-control input-sm has-feedback" placeholder="Email" />                                
					                        <span id="edit_email_error_as"></span>
					                    </div>
				                    </div>
			                    </div>
							</div>
							<div class="modal-footer">
								<input type="hidden" id="edit_person_id" name="id" value="" />
								<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
								<button type="submit" id="save" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>