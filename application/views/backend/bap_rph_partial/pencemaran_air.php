<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 18/12/16
 * Time: 12:48
 */
?>

<div class="tab-pane" id="pencemaran_air">
    <h4 class="page-tab">Pengendalian Pencemaran Air</h4>

    <label>a. Sumber Air dan Penggunaan</label>
    <div class="form-group ">
        <div class="row">
            <div class="col-sm-6">
                <label>Air Tanah</label>
                <input type="text" name="ambil_air_tanah" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_tanah : "";?>"/>
            </div>
            <div class="col-sm-6">
                <label>Perizinan</label>
                <select class="form-control input-sm" name="ambil_air_tanah_izin">
                    <option value="1" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_tanah_izin== true) ? 'selected' : '') : ""; ?>>Ada</option>
                    <option value="0" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_tanah_izin== false) ? 'selected' : '') : ""; ?>>Tidak Ada</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <div class="row">
            <div class="col-sm-12">
                <label>Air Permukaan</label>
                <input type="text" name="ambil_air_permukaan" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_permukaan : "";?>"/>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label>PDAM</label>
        <input type="text" name="ambil_air_pdam" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_pdam : "";?>"/>
    </div>
    <div class="form-group ">
        <label>Lain-lain</label>
        <input type="text" name="ambil_air_lain" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_lain : "";?>"/>
    </div>

    <div class="form-group ">
        <label>b. Penggunaan Air</label>
        <input type="text" name="penggunaan_air" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->penggunaan_air : "";?>"/>
    </div>

    <div class="form-group ">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group col-lg-6">
                    <label>c. Sumber Air Limbah</label>
                    <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                        <!-- <option value=''></option> -->
                        <?php
                        if (isset($pencemaran_air)) {$arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);}
                        foreach ($limb_sumber as $l) {
                            echo "<option value='$l->ket' ";
                            if (isset($arr_limb_sumber)) {
                                for ($i = 0; $i < count($arr_limb_sumber); $i++) {

                                    if ($arr_limb_sumber[$i] == $l->ket) {
                                        echo 'selected';
                                    }
                                }
                            }
                            echo ">$l->ket</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <label>d. Badan Air Penerima</label>
                <select name="bdn_terima" id="" class="form-control input-sm">
                    <option value=''></option>
                    <?php
                    foreach ($bdn_terima as $l) {
                        echo "<option value='$l->ket' ";
                        if (isset($pencemaran_air)) {
                            if ($pencemaran_air->bdn_terima == $l->ket) {
                                echo 'selected';
                            }
                        }
                        echo ">$l->ket</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group ">
        <label>e. Sarana Pengolahan Air Limbah</label>
        <select class="form-control input-sm" name="sarana_olah_limbah">
            <option value="1" <?php echo isset($pencemaran_air) ? (($pencemaran_air->sarana_olah_limbah== true) ? 'selected' : '') : ""; ?> >Ada</option>
            <option value="0" <?php echo isset($pencemaran_air) ? (($pencemaran_air->sarana_olah_limbah== false) ? 'selected' : '') : ""; ?> >Tidak Ada</option>
        </select>
    </div>
    <div class="form-group ">
        <label>Jenis Sarana</label>
        <input type="text" name="sarana_jenis" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_jenis : "";?>"/>
    </div>
    <div class="form-group ">
        <label>Kapasitas</label>
        <input type="text" name="sarana_kapasitas" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_kapasitas : "";?>"/>
    </div>
    <div class="form-group ">
        <div class="row">
            <div class="col-lg-6">
                <label>Koordinat Outlet S</label>
                <div class="row">
                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_derajat_s : ""?>" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding: 0; margin:0;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_jam_s : ""?>" name="koord_outlet_jam_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">'</span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_menit_s : ""?>" name="koord_outlet_menit_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">"</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label>Koordinat Outlet E</label>
                <div class="row">
                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_derajat_e : ""?>" name="koord_outlet_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding: 0; margin:0;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_jam_e : ""?>" name="koord_outlet_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">'</span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_menit_e : ""?>" name="koord_outlet_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">"</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row"> <!-- navigation -->
        <div class="col-sm-4 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Data Umum</a>
        </div>
        <div class="col-sm-4 text-center">
            <input id="save-pencemaran_air" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Air"/>
        </div>
        <div class="col-sm-4 text-right">
            <a class="btn btn-warning NextStep">Pencemaran Udara<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>