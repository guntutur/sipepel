<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">NERACA LIMBAH B3&nbsp;&nbsp;<small>mengelola seluruh data neraca limbah b3</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>SWAPANTAU</li>
                <li class="active">Neraca Limbah B3</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <!-- button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/>   -->

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#neraca_limbah_b3" onclick="$(this).reset_form_jenis_limbah('neraca_limbah_b3', true, 'jenis_limbah_b3');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th>Jenis Awal Limbah</th>                                
								<th>Dikelola</th>
								<th>Limbah Sisa</th>
								<th>Berkas</th>
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/neraca_b3/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : '-';
						var sisa_limbah = dt.total_jenis_limbah - dt.dikelola;
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
						t += '<td>';
						t += '<b>'+dt.badan_hukum+' '+dt.nama_industri + '</b><br/> Usaha/Kegiatan : ';
						t += dt.usaha_kegiatan + '<br/> Jenis/Kelas/Tipe : ';
						t += jenis_industri + '<br/> Periode Waktu : ';
						var nowTemp = $(this).format_date(dt.periode_waktu);    
						t += nowTemp + '';
                        t += '</td>';
						t += '<td><ol style="margin-left:-13px !important;">';
						if(dt.jenis_limbah.length > 0) {
							$.each(dt.jenis_limbah, function(k, v) {  
								t += '<li style="margin-left:-13px !important;">' + v + ' Ton</li>';
							});
						}
                        t += '</ol>';
						t += '<b>Total : '+dt.total_jenis_limbah+' Ton</b></td>';
						t += '<td>';
						t += dt.dikelola + ' Ton ';
                        t += '</td>';
						t += '<td>';
						t += sisa_limbah + ' Ton';
                        t += '</td>';
						var link = '<?php echo base_url("assets/neraca_b3/'+dt.nama_berkas+'"); ?>';
                        t += '<td><a href="'+link+'" target="_blank" title="Klik Untuk Download Berkas"><i class="fa fa-file-text icon-sidebar"></i></a></td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
						t += '<li><a onclick="$(this).upload_berkas('+dt.id_neraca+')">Upload Berkas</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_neraca+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_neraca+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }
	});

	$(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }
		
        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/neraca_b3/get'); ?>",
                data: 'id_neraca=' + id,
                success: function (response) {
					
					console.log(response);

                    $(this).reset_form('neraca_limbah_b3');
					
					$('input[id=edit_id]').val(response.id_neraca);
                    //$('select[name=nama_industri]').val(response.id_industri);
					$('select[name=industri]').chosen();
                    $('select[name=industri]').val(response.id_industri);
                    $('select[name=industri]').trigger("chosen:updated");
                    $('input[name=periode_waktu]').val($(this).format_date(response.periode_waktu));
                    $('input[name=dikelola]').val(response.dikelola);

					// # koordinat outlet ipal
                    var t = '';
                    if(response.jenis_limbah.length > 0) {
                        $(this).jenis_limbah_awal_template('jenis_limbah_awal', response.jenis_limbah);
                    }  

                    $('#neraca_limbah_b3 form').attr('action', '<?php echo base_url('/backend/neraca_b3/edit') ?>'); //this fails silently
                    $('#neraca_limbah_b3 form').get(0).setAttribute('action', '<?php echo base_url('/backend/neraca_b3/edit') ?>'); //this works
					
                    $('#neraca_limbah_b3').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
        
		$.fn.upload_berkas = function(id) {
            $('#upload_id').val(id);
            $('#upload').modal('show');
        }
		
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }   
		
		$.validator.setDefaults({ ignore: ":hidden:not(select)" });
		$("#form_add").validate({
            rules: {
                industri: {
                    required: true
                },
				periode_waktu: {
                    required: true
                },
				jenis_limbah: {
                    required: true
                },
				jml_ton: {
                    required: true
                },
                disimpan: {
                    required: true
                },
				dimanfaatkan: {
                    required: true
                },
				diolah:{
                    required: true
                },
				ditimbun:{
                    required: true
                },
				diserahkan:{
                    required: true
                },
				ekspor:{
                    required: true
                },
				lainnya:{
                    required: true
                }
            },
            messages: {        
                industri: {
                    required: "Silahkan pilih data industri."
                },
				 periode_waktu: {
                    required: "Tanggal belum dipilih."
                },
				jenis_limbah: {
                    required: "Pilih data."
                },
				jml_ton: {
                    required: "Field tidak boleh kosong."
                },
                disimpan: {
                    required: "Field tidak boleh kosong."
                },
                dimanfaatkan: {
                    required: "Field tidak boleh kosong."
                },
                diolah: {
                    required: "Field tidak boleh kosong."
                },
                ditimbun: {
                    required: "Field tidak boleh kosong."
                },
                diserahkan: {
                    required: "Field tidak boleh kosong."
                },
                ekspor: {
                    required: "Field tidak boleh kosong."
                },
                lainnya: {
                    required: "Field tidak boleh kosong."
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

		// var site = "<?php echo site_url(); ?>";
  //       $('#nama_industri').autocomplete({
  //           minChars: 2,
  //           type: 'POST',
  //           noCache: true,
  //           serviceUrl: site + 'backend/industri/search',
  //           onSearchStart: function (query) {
  //               $('#industri').val(null);              
  //           },
  //           onSelect: function (suggestion) {
  //               $('#industri').val(suggestion.data);
  //           }
  //       });	
			
		
        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="neraca_limbah_b3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Neraca Limbah B3</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/neraca_b3/register') ?>" method="post" id="form_add">
                <div class="modal-body">                    
                    <div class="row">
						<!-- <div class="col-lg-8 form-group nama_industri">
                            <label>Nama Usaha/Kegiatan</label>
                            <input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
							<input type="hidden" id="industri" name="industri" />                     
                        </div> -->
                        <div class="col-lg-8 form-group nama_industri">
                            <label>Nama Usaha/Kegiatan</label>                        
                            <select name="industri" id="industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                <option value="Empty">&nbsp;</option>
                                <?php
                                    foreach ($industri as $key => $value) {
                                        echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Periode Waktu</label>
                            <div class="input-group input-group-sm">
                                <input type="text" name="periode_waktu" class="form-control input-sm datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>
					<label>Jenis Awal Limbah</label>
                    <div id="jenis_limbah_awal"></div>
					<div id="box_jenis_limbah">
						<div class="form-group row" data-duplicate="koordinat" data-duplicate-min="1">
							<div class="col-lg-5">
								<label>Jenis Limbah</label>
								<div class="row">  
									<div class="col-lg-12">
										<select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm" required="required">
											<option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
											<?php foreach($data_acuan as $uk): ?>
												<option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
											<?php endforeach;?>                             
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-5">
								<label>Jumlah</label>
								<div class="row">
									 <div class="col-lg-12">
										<div class="input-group input-group-sm">
											<input type="text" name="jml_ton[]" id="jml_ton" class="form-control input-sm" value="" required="required">  
											<span class="input-group-addon">Ton</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-2" style="padding-left: 9px;">
								<label>&nbsp;</label>
								<div class="text-right">
									<div class="btn-group" style="padding-right: 0;">
										<button type="button" class="btn btn-sm btn-success" data-duplicate-add="koordinat"><i class="fa fa-plus-circle"></i></button>
										<button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="koordinat"><i class="fa fa-times-circle"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group status">
						<div class="row">
							<div class="col-lg-12">
								<label>Dikelola</label>
								<div class="input-group input-group-sm">                                
									<input type="text" name="dikelola" id="dikelola" class="form-control" placeholder="0" />
									<span class="input-group-addon">Ton</span>
								</div> 
							</div>
						</div>
					</div>   
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="upload" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="uploadLabel">Unggah Berkas Neraca B3</h4>
            </div>
            <form action="<?php echo base_url('/backend/neraca_b3/upload'); ?>" method="post" enctype="multipart/form-data">
				    <div class="modal-body">   
                        <div class="form-group">
                        <label>Upload Berkas</label>
                            <div class="input-group input-group-sm">
                                <input type="text" readonly="" class="form-control">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Browse ...<input type="file" name="userfile" />
                                    </span>
                                </span>
                            </div><!-- /.input-group -->
                        </div>
                    </div>
					<div class="modal-footer">
						<input type="hidden" id="upload_id" name="id" value="" />
						<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
						<button type="submit" name="do_upload" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Unggah</button>
					</div>
				</div>
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Izin Pembuangan Air Limbah</h4>
            </div>
            <form action="<?php echo base_url('/backend/neraca_b3/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->