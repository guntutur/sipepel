<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 18/12/16
 * Time: 15:39
 */
?>

<div class="tab-pane active" id="data_umum">
    <h4 class="page-tab"><i class="fa fa-newspaper-o icon-sidebar"></i>Data Umum Usaha/Kegiatan</h4>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>Tanggal Pengawasan</label>
            <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>">
        </div>
        <div class="form-group col-lg-6">
            <label>Pukul</label>
            <div class="bfh-timepicker">
                <input id="timepick2" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>Petugas Pengawas</label>
            <select class="form-control input-sm" name="id_pegawai">
                <option value=""></option>
                <?php
                foreach ($pegawai as $p) {
                    echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                }?>
            </select>
        </div>
        <div class="form-group col-lg-6">
            <label>Beserta Anggota Pengawas:</label>
            <select class="form-control input-sm multiselect" name="petugas_pengawas[]" id="ini_id_petugas_bap2" multiple="multiple" style="border: 0!important;">
                <?php
                foreach ($pegawai as $p) {
                    echo "<option value='$p->id_pegawai' ";
                    for ($i=0; $i < count($petugas_bap); $i++) {
                        if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                            echo "selected";
                        }
                    }
                    echo ">$p->nama_pegawai</option>";
                }?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <h4>Lokasi Pengawasan / Pembinaan</h4>
        <div class="form-group status">
            <label>Nama Usaha/Kegiatan</label>
            <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                <option value="Empty">&nbsp;</option>
                <?php
                foreach ($industri as $value) {
                    echo '<option value="'.$value->id_industri.'"';
                    if($bap->id_industri==$value->id_industri){
                        echo 'selected';
                    }
                    echo '>'.$value->nama_industri.'</option>';
                }
                ?>
            </select>
        </div>
        <div id="container2" style="display:none"></div>
    </div>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>Kapasitas Produksi (ton/bulan)</label>
            <input type="text" name="kapasitas_produksi" class="form-control input-sm has-feedback" value="<?php echo $bap_industri->kapasitas_produksi?>" />
        </div>
        <div class="form-group col-lg-6">
            <label>Proses Produksi</label>
            <select class="form-control input-sm multiselect" name="proses_produksi[]" id="ini_harusnya_pros" multiple="multiple" style="border: 0!important;">
                <?php
                $prosed = explode(', ', $bap_industri->proses_produksi);
                foreach ($proses_produksi as $pros){
                    echo "<option value='$pros->ket' ";
                    for ($i=0; $i < count($prosed); $i++) {
                        if($prosed[$i]==$pros->ket) {
                            echo "selected";
                        }
                    }
                    echo ">$pros->ket</option>";
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group ">
        <label>Jumlah Karyawan</label>
        <input type="text" name="jml_karyawan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_industri->jumlah_karyawan?>"/>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover" id="datatable-example">
            <thead class="the-box dark full">
            <tr>
                <!-- <div class="form-group "> -->
                <th>Dokumen Lingkungan</th>
                <th>Izin Lingkungan</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <!-- <div class="radio"> -->
                    <label>
                        <input class="dl" type="radio" name="dl" id="dl1" value="ada" <?php echo (($bap->dok_lingk==1) ? 'checked' : '')?>>Ada
                    </label>
                    <label>
                        <input class="dl" type="radio" name="dl" id="dl2" value="tidak ada" <?php echo (($bap->dok_lingk==0) ? 'checked' : '')?>>Tidak Ada
                    </label>
                    <!-- </div> -->
                </td>
                <!-- </div> -->
                <!-- <div class="form-group "> -->
                <td>
                    <!-- <div class="radio"> -->
                    <label>
                        <input type="radio" name="il" id="il1" value="ada" <?php echo (($bap->izin_lingk==1) ? 'checked' : '')?>>Ada
                    </label>
                    <label>
                        <input type="radio" name="il" id="il2" value="tidak ada" <?php echo (($bap->izin_lingk==0) ? 'checked' : '')?>>Tidak Ada
                    </label>
                    <!-- </div> -->
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group " id="jdl" name="jdl" style="display:none">
                        <!-- <div class="form-group "> -->
                        <label>Jenis Dokumen</label>
                        <select class="form-control input-sm" name="jdl_name" id="jdl_name">
                            <option value="">-- Pilih salah satu --</option>
                            <?php
                            foreach ($jenis_dok_lingkungan as $j) {
                                echo "<option value='$j->ket' ".(($bap->dok_lingk_jenis == $j->ket) ? 'selected' : '').">$j->ket</option>";
                            }?>
                        </select>
                        <!-- </div> -->
                        <!-- <div class="form-group "> -->
                        <label>Tahun</label>
                        <input type="text" name="dlt" id="dlt" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun; ?>"/>
                        <!-- </div> -->
                    </div>
                </td>
                <td>
                    <div class="form-group " id="il_tahun" style="display:none">
                        <label>Tahun</label>
                        <input type="text" name="il_tahun" id="ilt" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun; ?>"/>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <fieldset>
        <legend>Penanggung Jawab Usaha / Kegiatan</legend>
        <div class="form-group ">
            <label>Nama</label>
            <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama; ?>"/>
        </div>
        <div class="form-group ">
            <label>Telepon</label>
            <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp; ?>"/>
        </div>
        <div class="form-group ">
            <label>Jabatan</label>
            <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan; ?>"/>
        </div>
    </fieldset>

    <h4 class="page-tab">Lain - Lain</h4>
    <div class="form-group ">
        <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"><?php echo $bap->catatan; ?></textarea>
    </div>

    <div class="row"> <!-- navigation -->
        <div class="col-sm-6 text-left">
            <input id="save-datum" class="btn btn-md btn-success save-partially" value="Simpan Data Umum"/>
        </div>
        <div class="col-sm-6 text-right">
            <a class="btn btn-warning NextStep">Pencemaran Air<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>