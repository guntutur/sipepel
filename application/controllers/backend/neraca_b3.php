<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class neraca_b3 extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model
        $this->load->model('m_neraca_b3');
        $this->load->model('m_industry');
		$this->load->model('m_data_master', '', TRUE);
        // $this->load->model('m_jenis_dokumen');
		
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $data['industri'] = $this->m_industry->get_dropdown();
		$jenis_data_master = "jenis_limbah";
		$data['data_acuan'] = $this->m_data_master->get_dropdown($jenis_data_master);
        
    	$this->load->view('include/header');
        $this->load->view('backend/v_neraca_b3', $data);
        $this->load->view('include/footer');
    }

   public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				
			);

     	$total = $this->m_neraca_b3->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_neraca_b3->get_all($where, $dataperpage, $start);
	
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		$data = $this->m_neraca_b3->get_by_id($this->input->post('id_neraca'));
		$jenis_limbah_b3 = ($data->jenis_limbah) ? unserialize($data->jenis_limbah) : array();        
        $data->jenis_limbah = $jenis_limbah_b3;
		
		echo json_encode($data);
        exit();
	}

    public function register() {       

        date_default_timezone_set("Asia/Jakarta");
        $tgl_pembuatan = date("Y-m-d H:i:s");
		$jenis_limbah_b3 = $this->functions->parsing_jenis_limbah_awal($this->input->post());
        $param = array(                
                'id_industri' => $this->input->post('industri'),
                'periode_waktu' => date("Y-m-d", strtotime($this->input->post('periode_waktu'))),
                'dikelola' => $this->input->post('dikelola'),
				'total_jenis_limbah' => array_sum($this->input->post('jml_ton')),
                'jenis_limbah' => $jenis_limbah_b3,
                'tgl_pembuatan' => $tgl_pembuatan,
                'tgl_update' => $tgl_pembuatan,
                'dibuat_oleh' =>$this->tank_auth->get_personname(),
                'diupdate_oleh' => $this->tank_auth->get_personname()
            );
        
        if($this->m_neraca_b3->insert($param))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
			
		redirect('backend/neraca_b3');

    }
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$jenis_limbah_b3 = $this->functions->parsing_jenis_limbah_awal($this->input->post());
		$param = array(
			'id_industri' => $this->input->post('industri'),
			'periode_waktu' => date("Y-m-d", strtotime($this->input->post('periode_waktu'))),
			'dikelola' => $this->input->post('dikelola'),
			'total_jenis_limbah' => array_sum($this->input->post('jml_ton')),
			'jenis_limbah' => $jenis_limbah_b3,
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_neraca_b3->change($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/neraca_b3');
	}
	
	public function delete() {
		$this->m_neraca_b3->delete($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/neraca_b3');
	}
	
	public function upload() {
		$id = $this->input->post('id');

		$d = $this->m_neraca_b3->get_by_id($id);
		$i = $this->m_industry->get_industry_by_id($d->id_industri);		
		if($d) {

			$temp = pathinfo($_FILES["userfile"]["name"]);
			$path = "assets/neraca_b3/";			
			$newfilename = str_replace(' ', '_', strtolower(trim($i->nama_industri))).'_'.str_replace('-', '_', trim($d->periode_waktu)).".".$temp['extension'];
			echo $newfilename;

			$config['upload_path'] = realpath($path);
			$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|doc|DOC|docx|DOCX|xls|XLS|xlsx|XLSX';
			$config['max_size'] = '8000';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $newfilename;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload()) {
				
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', $this->upload->display_errors()));
			}else{
				$param = array('nama_berkas' => $newfilename);
				$this->m_neraca_b3->change($param, $id);
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Berkas berhasil diunggah'));
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}
}
?>