<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 // <!-- Document   : m_bap.php  -->
 // <!-- Created on : November 12, 2014  -->
 // <!-- Author     : Faridzi  -->
 // <!-- Hijacked by: huda.jtk09@gmail.com  --> 
 // <!-- Description: Model for BAP SPPBE  -->
class m_bap_rph extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * Insert to table
	 *
	 * @param	String, object
	 * @return	boolean
	 */
	function insert($table_name, $data) {
		$this->db->insert($table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function get_all() {
		$data = array();

		$query = $this->db->query("SELECT rph.id_bap, industri.nama_industri, bap.bap_tgl, bap.compare_status, bap.is_compared, (SELECT COUNT(*) FROM history_item_teguran_bap d WHERE d.id_bap = rph.id_bap) AS jml_teguran
									FROM bap_rph rph
									LEFT JOIN bap bap ON rph.id_bap = bap.id_bap
									INNER JOIN industri industri ON bap.id_industri = industri.id_industri
									GROUP BY rph.id_bap
									ORDER BY rph.id_bap_rph ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

}


?>