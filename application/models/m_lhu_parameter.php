<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_lhu_parameter extends CI_Model {

	private $table_name			= 'parameter_lhu';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('p.jenis_lhu, p.id_parameter_lhu, p.ket as nama_parameter, p.nilai_parameter_lhu_internal, 
		p.nilai_parameter_lhu_eksternal, p.dasar_hukum_internal, p.dasar_hukum_eksternal, p.status, u.id_jenis_industri, r.ket as data_acuan, 
		u.ket as nama_jenis_kegiatan, q.ket as usaha_kegiatan, (select count(*) from parameter_lhu dd where dd.jenis_lhu = p.jenis_lhu and dd.id_usaha_kegiatan = p.id_usaha_kegiatan and dd.id_jenis_industri = p.id_jenis_industri and dd.status > 0) as jumlah');
		$this->db->from($this->table_name .' as p');
		$this->db->join('jenis_industri u', 'u.id_jenis_industri = p.id_jenis_industri', 'left');
		$this->db->join('data_master_detail r', 'r.ket = p.id_jenis_industri', 'left');
		$this->db->join('usaha_kegiatan q', 'q.id_usaha_kegiatan = p.id_usaha_kegiatan', 'left');
		$this->db->where('p.status', 0);
		
		if($where != '') {
			$this->db->where($where);
		}
		
		$this->db->limit($limit, $offset);
		$this->db->order_by("p.jenis_lhu", "asc"); 
		$this->db->order_by("q.ket", "asc"); 
		$this->db->order_by("u.ket", "asc"); 
		$this->db->order_by("p.ket", "asc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		$this->db->select('p.jenis_lhu, p.id_parameter_lhu, p.ket as nama_parameter, p.nilai_parameter_lhu_internal, 
		p.nilai_parameter_lhu_eksternal, p.dasar_hukum_internal, p.dasar_hukum_eksternal, p.status, u.id_jenis_industri, 
		u.ket as nama_jenis_kegiatan, q.ket as usaha_kegiatan, (select count(*) from parameter_lhu dd where dd.id_usaha_kegiatan = p.id_usaha_kegiatan and dd.status > 0) as jumlah');
		$this->db->from($this->table_name .' as p');
		$this->db->join('jenis_industri u', 'u.id_jenis_industri = p.id_jenis_industri', 'left');
		$this->db->join('data_master_detail r', 'r.ket = p.id_jenis_industri', 'left');
		$this->db->join('usaha_kegiatan q', 'q.id_usaha_kegiatan = p.id_usaha_kegiatan', 'left');
		$this->db->where('p.status', 0);
		
		if($where != '') {
			$this->db->where($where);
		}
		
		return $this->db->count_all_results();
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_lhu_parameter_by_id($id) {
		$this->db->where('id_parameter_lhu', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_lhu_parameter($usaha_kegiatan, $jenis_industri, $jenis_lhu) {
		
		$this->db->where('id_usaha_kegiatan', $usaha_kegiatan);
		$this->db->where('id_jenis_industri', $jenis_industri);
		$this->db->where('jenis_lhu', $jenis_lhu);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_lhu_parameter($data, $id) {

		$this->db->where("id_parameter_lhu", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function change_lhu_parameter2($data, $usaha_kegiatan, $jenis_industri, $jenis_lhu) {

		$this->db->where("id_usaha_kegiatan", $usaha_kegiatan);
		$this->db->where("id_jenis_industri", $jenis_industri);
		$this->db->where("jenis_lhu", $jenis_lhu);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_lhu_parameter($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function get_lhu_parameter_detail_by_id($id) {
		$whr1 = 'p.id_parameter_lhu = "'.$id.'"';
		$query = $this->db->query('select p.*, q.ket as usaha_kegiatan, r.ket as nama_industri 
									from parameter_lhu as p 
									left join (
										select * from usaha_kegiatan
									) q on p.id_usaha_kegiatan = q.id_usaha_kegiatan
									left join (
										select * from jenis_industri
									) r on p.id_jenis_industri = r.id_jenis_industri
									where '.$whr1.'  AND 1=1' );
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	
	function get_detail_all($where='', $limit=0, $offset=0) {

		$data = array();

		$this->db->select('*');
		$this->db->from($this->table_name);		

		if($where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		
		$this->db->order_by("ket", "asc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	function get_detail_num_rows($where) {

		$this->db->select('*');
		$this->db->from($this->table_name);		

		if($where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}
	
	function get_detail_parameter_by_id($id) {
		$this->db->where('id_parameter_lhu', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function delete_detail_parameter($id) {
		$data = $this->get_detail_parameter_by_id($id);
		$this->db->where('id_parameter_lhu', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	function change_detail_parameter($data, $id) {

		$this->db->where("id_parameter_lhu", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_detail($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function is_duplicate($usaha_kegiatan, $jenis_industri, $jenis_lhu) {
		$this->db->where('id_usaha_kegiatan', $usaha_kegiatan);
		$this->db->where('id_jenis_industri', $jenis_industri);
		$this->db->where('jenis_lhu', $jenis_lhu);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	function is_duplicate_detail($usaha_kegiatan, $jenis_industri, $jenis_lhu, $ket) {
		$this->db->where('id_usaha_kegiatan', $usaha_kegiatan);
		$this->db->where('id_jenis_industri', $jenis_industri);
		$this->db->where('jenis_lhu', $jenis_lhu);
		$this->db->where('ket', $ket);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	// public function get_par_baku_mutu($usaha_kegiatan, $jenis_industri, $jenis_lhu){
	// 	$whr1 = 'p.id_usaha_kegiatan = "'.$usaha_kegiatan.'"';
	// 	$whr2 = 'p.id_jenis_industri = "'.$jenis_industri.'"';
	// 	$whr3 = 'p.jenis_lhu = "'.$jenis_lhu.'"';
	// 	$query = $this->db->query('select p.* 
	// 								from parameter_lhu as p 
	// 								where '.$whr1.'  AND '.$whr2.' AND '.$whr3.' AND p.status = 1' );
	// 	if ($query->num_rows() > 0) return $query->row();
	// 	return NULL;
	// }
	
}
?>