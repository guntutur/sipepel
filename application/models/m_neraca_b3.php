<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_neraca_b3 extends CI_Model {

	private $table_name			= 'neraca_b3';	
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('al.*, i.nama_industri, i.badan_hukum, j.ket as usaha_kegiatan, k.ket as nama_jenis_industri');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_industri = i.id_industri', 'left');
		$this->db->join('usaha_kegiatan as j', 'i.id_usaha_kegiatan = j.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri as k', 'i.id_jenis_industri = k.id_jenis_industri', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("i.badan_hukum", "asc"); 
		$this->db->order_by("i.nama_industri", "asc"); 
		$this->db->order_by("al.tgl_pembuatan", "desc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$jenis_limbah_b3 = ($row->jenis_limbah) ? unserialize($row->jenis_limbah) : array();        
				$row->jenis_limbah = $jenis_limbah_b3;
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select('al.*, i.nama_industri, i.badan_hukum,  j.ket as usaha_kegiatan, k.ket as jenis_industri');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_industri = i.id_industri', 'left');
		$this->db->join('usaha_kegiatan as j', 'i.id_usaha_kegiatan = j.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri as k', 'i.id_jenis_industri = k.id_jenis_industri', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_by_id($id) {
		$this->db->where('id_neraca', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_detail($where) {
		$this->db->where($where);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;	
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete($id) {
		$data = $this->get_by_id($id);
		$this->db->where('id_neraca', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change($data, $id) {

		$this->db->where("id_neraca", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

}
?>