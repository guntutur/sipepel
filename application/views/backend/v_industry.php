<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">NAMA USAHA/KEGIATAN&nbsp;&nbsp;<small>mengelola seluruh data nama usaha/kegiatan</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Nama Usaha/Kegiatan</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                        <button type="button" data-toggle="modal" data-target="#filter" class="btn btn-sm btn-info"><i class="fa fa-search"></i>&nbsp;Pencarian Data</button>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">                    
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="dark full">                                                
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th width="17%">Usaha/Kegiatan</th>
                                <th width="17%">Jenis Usaha/Kegiatan</th>
								<!-- <th>Keterangan</th> -->
                                <th width="24%">Kontak Kami</th>                                
                                <th class="text-right" width="5%"></th>                                    
                            </tr>                                                                         
                        </thead>                        
                        <tbody id="document-data">
                            <tr><td colspan="6">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            keyword: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/industri/get_list"); ?>',
        url_adv: '<?php echo base_url("backend/industri/get_list_advanced"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            // this.param.keyword = $('#keyword').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_adv: function() {
            this.param.keyword = $('#keyword').val();
            this.param.curpage = 0;
            this.load_data_adv();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);                              
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : '-';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        var nama = (dt.badan_hukum) ? dt.badan_hukum+' ' : '';
                        t += '<td><b>'+nama+dt.nama_industri+'</b><br/>';
						t += '<small>' + dt.alamat + '</small><br/>';
						t += '<small>Kec. ' + dt.nama_kecamatan + ', Kel. '+ dt.nama_kelurahan +'</small><br/>';
						t += '</td>';
                        t += '<td>'+$(this).return_value(dt.nama_usaha_kegiatan)+'</td>';   
                        t += '<td>'+$(this).return_value(dt.nama_jenis_industri)+'</td>';
                        t += '<td>';					
						t += 'Pimpinan: '+$(this).return_value(dt.pimpinan)+'<br/> ';
                        t += '<i class="fa fa-phone-square icon-sidebar"></i>'+dt.tlp+'<br/> ';
                        t += '<i class="fa fa-fax icon-sidebar"></i>'+dt.fax+' <br/> ';
                        t += '<i class="fa fa-envelope icon-sidebar"></i>'+dt.email+'';  
                        t += '</td>';

                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_industri+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_industri+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        },
        // huda : add -> advanced search (4 Feb 2015, 17:44)
        load_data_adv: function() {
            $.ajax({
                url: Document.url_adv,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);                              
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                        var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : '-';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        var nama = (dt.badan_hukum) ? dt.badan_hukum+' ' : '';
                        t += '<td><b>'+nama+dt.nama_industri+'</b><br/>';
                        t += '<small>' + dt.alamat + '</small><br/>';
                        t += '<small>Kec. ' + dt.nama_kecamatan + ', Kel. '+ dt.nama_kelurahan +'</small><br/>';
                        t += '</td>';
                        t += '<td>'+$(this).return_value(dt.nama_usaha_kegiatan)+'</td>';   
                        t += '<td>'+$(this).return_value(dt.nama_jenis_industri)+'</td>';
                        t += '<td>';                    
                        t += 'Pimpinan: '+$(this).return_value(dt.pimpinan)+'<br/> ';
                        t += '<i class="fa fa-phone-square icon-sidebar"></i>'+dt.tlp+'<br/> ';
                        t += '<i class="fa fa-fax icon-sidebar"></i>'+dt.fax+' <br/> ';
                        t += '<i class="fa fa-envelope icon-sidebar"></i>'+dt.email+'';  
                        t += '</td>';

                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_industri+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_industri+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }

    $(document).ready(function () {
        
        $("#form_add").validate({
            rules: {
                nama_industri: {
                    required: true,
                    maxlength: 250
                },
                tlp:{
                    required: true,
                    maxlength: 90
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 90
                },
                alamat: {
                    required: true,
                    maxlength: 90
                },
                fax: {
                    required: true,
                    maxlength: 90
                },				
                usaha_kegiatan: {
                    required: true,
                },
				nama_kecamatan: {
                    required: true,
                },
				nama_kelurahan: {
                    required: true,
                }
            },
            messages: {        
                nama_industri: {
                    required: "Masukan nama usaha/kegiatan",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 90 karakter"
                },
                tlp: {
                    required: "Masukan nomor telepon usaha/kegiatan.",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 90 karakter"
                },
                email: {
                    required: "Masukan email usaha/kegiatan",
                    email: "Masukkan email usaha/kegiatan yang valid",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 90 karakter"
                },
                alamat: {
                    required: "Masukan alamat usaha/kegiatan",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 90 karakter"
                },
                fax: {
                    required: "Masukan fax usaha/kegiatan",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 90 karakter"
                },               
                usaha_kegiatan: {
                    required: "Silahkan pilih data"
                },
				nama_kecamatan: {
                    required: "Silahkan pilih data"
                },
				nama_kelurahan: {
                    required: "Silahkan pilih data"
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
       
        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/industri/get'); ?>",
                data: 'id_industri=' + id,
                success: function (response) {                    
                    $('input[id=edit_id]').val(response.id_industri);
                    $('input[name=nama_industri]').val(response.nama_industri);
					$('select[name=badan_hukum]').val(response.badan_hukum);
					$('input[name=pimpinan]').val(response.pimpinan);
					$('input[name=hari_kerja]').val(response.hari_kerja_bulan);
					$('input[name=jam_kerja]').val(response.jam_kerja_hari);
					$('input[name=luas_area]').val(response.luas_area);
					$('input[name=luas_bangunan]').val(response.luas_bangunan);
                    $('input[name=tlp]').val(response.tlp);
                    $('input[name=email]').val(response.email);
                    $('textarea[name=alamat]').val(response.alamat);
                    $('input[name=fax]').val(response.fax);                    
                    $('select[name=usaha_kegiatan]').val(response.id_usaha_kegiatan);  
                    $(this).load_jenis_industri(response.id_usaha_kegiatan, response.id_jenis_industri);
					$('select[name=nama_kecamatan]').val(response.id_kecamatan);
                    $(this).load_kelurahan(response.id_kecamatan, response.id_kelurahan);

                    $('#myModal').modal('show');
                }
            })
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }
		
		// $('select[name=nama_kelurahan]').prop('disabled', true);		
        $('#nama_kelurahan').prop('disabled', true);
		$('#nama_kecamatan').change(function() {
			var id_kecamatan = $(this).val();
            $(this).load_kelurahan(id_kecamatan);			
		});

        $.fn.load_kelurahan = function(id_kecamatan, id_kel) {                        
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/kelurahan/get_by_kecamatan'); ?>",
                data: 'id_kecamatan=' + id_kecamatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {                            
                            opt += '<option value="'+v.id_kelurahan+'" '+ ((v.id_kelurahan == id_kel) ? "selected" : "") +'>'+v.ket+'</option>';
                        });
                        $('#nama_kelurahan').prop('disabled', false);
                    }else{
                        $('#nama_kelurahan').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#nama_kelurahan").html(opt);
                }
            });
        }

        $('#filter_nama_kelurahan').prop('disabled', true);
        $('#filter_nama_kecamatan').change(function() {
            var id_kecamatan = $(this).val();
            $(this).load_filter_kelurahan(id_kecamatan);           
        });

        $.fn.load_filter_kelurahan = function(id_kecamatan, id_kel) {                        
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/kelurahan/get_by_kecamatan'); ?>",
                data: 'id_kecamatan=' + id_kecamatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {                            
                            opt += '<option value="'+v.id_kelurahan+'" '+ ((v.id_kelurahan == id_kel) ? "selected" : "") +'>'+v.ket+'</option>';
                        });
                        $('#filter_nama_kelurahan').prop('disabled', false);
                    }else{
                        $('#filter_nama_kelurahan').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#filter_nama_kelurahan").html(opt);
                }
            });
        }
				
        $('#jenis_industri').prop('disabled', true);
		$('#usaha_kegiatan').change(function() {
			var id_usaha_kegiatan = $(this).val();
			$(this).load_jenis_industri(id_usaha_kegiatan);
		});

        $.fn.load_jenis_industri = function(id_usaha_kegiatan, id_jenis_industri) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/jenis_industri/get_by_usaha_kegiatan'); ?>",
                data: 'id_usaha_kegiatan=' + id_usaha_kegiatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {
                            opt += '<option value="'+v.id_jenis_industri+'" >'+v.ket+'</option>';
                        });
                        $('#jenis_industri').prop('disabled', false);
                    }else{
                        $('#jenis_industri').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#jenis_industri").html(opt);
                }
            });
        }

        $('#filter_jenis_industri').prop('disabled', true);        
        $('#filter_usaha_kegiatan').change(function() {
            var id_usaha_kegiatan = $(this).val();
            $(this).load_filter_jenis_industri(id_usaha_kegiatan);
        });

        $.fn.load_filter_jenis_industri = function(id_usaha_kegiatan, id_jenis_industri) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/jenis_industri/get_by_usaha_kegiatan'); ?>",
                data: 'id_usaha_kegiatan=' + id_usaha_kegiatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {
                            opt += '<option value="'+v.id_jenis_industri+'" >'+v.ket+'</option>';
                        });
                        $('#filter_jenis_industri').prop('disabled', false);
                    }else{
                        $('#filter_jenis_industri').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#filter_jenis_industri").html(opt);
                }
            });
        }

       $.fn.search_data = function() {
            var nama_industri = $('#filtering input[name=nama_industri]').val();         
            var usaha_kegiatan = $('#filtering select[name=usaha_kegiatan]').val(); 
            var jenis_industri = $('#filtering select[name=jenis_industri]').val();
            var pimpinan = $('#filtering input[name=pimpinan]').val();
            var alamat = $('#filtering textarea[name=alamat]').val();
            var kecamatan = $('#filtering select[name=nama_kecamatan]').val();
            var kelurahan = $('#filtering select[name=nama_kelurahan]').val();
            var keyword = nama_industri + ';' + usaha_kegiatan + ';' + jenis_industri + ';' + pimpinan + ';' + alamat + ';' + kecamatan + ';' + kelurahan;

            $('#keyword').val(keyword);

            Document.search_adv();
       }

        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Usaha/Kegiatan</h4>
            </div>
            <form role="form" data-action="<?php echo base_url('/backend/industri') ?>" method="post" id="form_add">
                <div class="modal-body">                    
                    <div class="row">
						<div class="col-lg-6 form-group usaha_kegiatan">
                            <label>Jenis Usaha/Kegiatan</label>
                            <select name="usaha_kegiatan" id="usaha_kegiatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Usaha/Kegiatan --</option>
                                <?php foreach($usaha_kegiatan as $uk): ?>
                                    <option value="<?php echo $uk->id_usaha_kegiatan;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>                        
                        </div>
                        <div class="col-lg-6 form-group jenis_industri">
                            <label>Jenis/Kelas/Tipe</label>
                            <select name="jenis_industri" id="jenis_industri" class="form-control input-sm">
                                <option value="" selected>-- Pilih Jenis/Tipe/Kelas --</option>                                           
                            </select>                            
                        </div>  
                    </div>
					<div class="row">
						<div class="col-lg-3 form-group badan_hukum">
							<label>Badan Usaha</label>
							<!-- <input type="text" name="badan_hukum" id="badan_hukum" class="form-control input-sm has-feedback" autofocus />							           -->
                            <select name="badan_hukum" id="badan_hukum" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Badan Usaha --</option>
                                <?php foreach($badan_usaha as $uk): ?>
                                    <option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>     
						</div>
						<div class="col-lg-9 form-group nama_industri">
							<label>Nama Usaha/Kegiatan</label>
							<input type="text" name="nama_industri" id="nama_industri" class="form-control input-sm has-feedback" autofocus />							         
						</div>
					</div>
					<div class="form-group pimpinan">
                        <label>Pimpinan Usaha/Kegiatan</label>
                        <input type="text" name="pimpinan" id="pimpinan" class="form-control input-sm has-feedback" autofocus />                               
                    </div>
					<div class="row">
                        <div class="col-lg-3 form-group ">
                            <label>Hari Kerja</label>
                            <div class="input-group input-group-sm">                                
                                <input type="text" name="hari_kerja" id="hari_kerja" class="form-control" placeholder="0"/>
                                <span class="input-group-addon">Hari/Bulan</span>
                            </div> 
                        </div>
                        <div class="col-lg-3 form-group">
                            <label>Jam Kerja</label>
                            <div class="input-group input-group-sm">                                
                                <input type="text" name="jam_kerja" id="jam_kerja" class="form-control" placeholder="0" />
                                <span class="input-group-addon">Jam/Hari</span>
                            </div> 
                        </div>
						<div class="col-lg-3 form-group">
                            <label>Luas Area</label>
                            <div class="input-group input-group-sm">                                
                                <input type="text" name="luas_area" id="luas_area" class="form-control" placeholder="0" />
                                <span class="input-group-addon">m2</span>
                            </div>                                            
                        </div>
						<div class="col-lg-3 form-group">
                            <label>Luas Bangunan</label>
                            <div class="input-group input-group-sm">                                
                                <input type="text" name="luas_bangunan" id="luas_bangunan" class="form-control" placeholder="0" />
                                <span class="input-group-addon">m2</span>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 form-group tlp">
                            <label>Telepon</label>
                            <input type="text" name="tlp" id="tlp" class="form-control has-feedback input-sm numberonly" autofocus />                            
                        </div>
                        <div class="col-lg-6 form-group fax">
                            <label>Fax</label>
                            <input type="text" name="fax" id="fax" class="form-control has-feedback input-sm numberonly" autofocus />                            
                        </div>
                    </div>
                    <div class="form-group email">
                        <label>Email</label>
                        <input type="text" name="email" id="email" class="form-control input-sm has-feedback" autofocus />                        
                    </div>
					<div class="row">
						<div class="col-lg-6 form-group nama_kecamatan">
                            <label>Kecamatan</label>
                            <select name="nama_kecamatan" id="nama_kecamatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Kecamatan --</option>
                                <?php foreach($kecamatan as $uk): ?>
                                    <option value="<?php echo $uk->id_kecamatan;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>                        
                        </div>
                        <div class="col-lg-6 form-group nama_kelurahan">
                            <label>Kelurahan</label>
                            <select name="nama_kelurahan" id="nama_kelurahan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Kelurahan --</option>                                                       
                            </select>                            
                        </div>  
                    </div>
                    <div class="form-group alamat">
                        <label>Alamat</label>
                        <textarea rows="4" name="alamat" id="alamat" class="form-control input-sm has-feedback" autofocus ></textarea>                        
                    </div>                                                                  
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data Usaha/Kegiatan</h4>
            </div>
            <form action="<?php echo base_url('/backend/industri/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<input type="hidden" name="keyword" id="keyword" class="form-control" value="" />
<div class="modal fade" id="filter" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Pencarian Data</h4>
            </div>
            <form action="" method="post" id="filtering">
                <div class="modal-body">
                    <div class="form-group nama_industri">
                        <label>Nama Usaha/Kegiatan</label>
                        <input type="text" name="nama_industri" id="nama_industri" class="form-control input-sm has-feedback" autofocus />                                   
                    </div>
                    <div class="row">
                        <div class="col-lg-6 form-group usaha_kegiatan">
                            <label>Jenis Usaha/Kegiatan</label>
                            <select name="usaha_kegiatan" id="filter_usaha_kegiatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Usaha/Kegiatan --</option>
                                <?php foreach($usaha_kegiatan as $uk): ?>
                                    <option value="<?php echo $uk->id_usaha_kegiatan;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>                        
                        </div>
                        <div class="col-lg-6 form-group jenis_industri">
                            <label>Jenis/Kelas/Tipe</label>
                            <select name="jenis_industri" id="filter_jenis_industri" class="form-control input-sm">
                                <option value="" selected>-- Pilih Jenis/Tipe/Kelas --</option>                                           
                            </select>                            
                        </div>  
                    </div>
                    <div class="form-group pimpinan">
                        <label>Pimpinan Usaha/Kegiatan</label>
                        <input type="text" name="pimpinan" id="pimpinan" class="form-control input-sm has-feedback" autofocus />                               
                    </div>
                    <div class="form-group alamat">
                        <label>Alamat</label>
                        <textarea rows="2" name="alamat" id="alamat" class="form-control input-sm has-feedback" autofocus ></textarea>                        
                    </div> 
                    <div class="row">
                        <div class="col-lg-6 form-group nama_kecamatan">
                            <label>Kecamatan</label>
                            <select name="nama_kecamatan" id="filter_nama_kecamatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Kecamatan --</option>
                                <?php foreach($kecamatan as $uk): ?>
                                    <option value="<?php echo $uk->id_kecamatan;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>                        
                        </div>
                        <div class="col-lg-6 form-group nama_kelurahan">
                            <label>Kelurahan</label>
                            <select name="nama_kelurahan" id="filter_nama_kelurahan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Kelurahan --</option>                                                       
                            </select>                            
                        </div>  
                    </div>                    
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-6" style="text-align:left;">
                            <button type="button" onclick="$(this).reset_form('filter');" class="btn btn-sm btn-warning"><i class="fa fa-eraser"></i>&nbsp;Bersihkan Form</button>
                        </div>
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                            <button type="button" onclick="$(this).search_data('filtering');" class="btn btn-sm btn-info" data-dismiss="modal"><i class="fa fa-search"></i>&nbsp;&nbsp;Cari Data</button>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal 