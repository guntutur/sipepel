		<!--
		===========================================================
		Placed at the end of the document so the pages load faster
		===========================================================
		-->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->		
		<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
                <script src="<?php echo base_url('assets/js/bootstrap.file-input.js ') ?>"></script>                
		
		<!-- PLUGINS -->
		<script src="<?php echo base_url('assets/plugins/retina/retina.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/nicescroll/jquery.nicescroll.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/backstretch/jquery.backstretch.min.js') ?>"></script>
                <script src="<?php echo base_url('assets/plugins/owl-carousel/owl.carousel.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/datatable/js/jquery.dataTables.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/datatable/js/bootstrap.datatable.js') ?>"></script>		
		<script src="<?php echo base_url('assets/plugins/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/newsticker/jquery.newsTicker.min.js') ?>"></script>
		
		<!-- MAIN APPS JS -->
		<script src="<?php echo base_url('assets/js/apps.js') ?>"></script>
		
		
        <!-- jQuery Validation -->        
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
                
	</body>

<!-- Mirrored from diliat.in/themeforest/sentir/1.2/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 23 Sep 2014 02:59:51 GMT -->
</html>