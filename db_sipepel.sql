-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 09 Nov 2018 pada 14.35
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecoterra_sipepel`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `hapus_laporan_hasil_uji` (IN `v_id_laporan_hasil_uji` INT)  begin
delete from history_item_teguran_lhu where id_laporan_hasil_uji = v_id_laporan_hasil_uji;
delete from detail_lhu where id_laporan_hasil_uji = v_id_laporan_hasil_uji;
delete from laporan_hasil_uji where id_laporan_hasil_uji = v_id_laporan_hasil_uji;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan_kimia_ipal`
--

CREATE TABLE `bahan_kimia_ipal` (
  `id_bahan_kimia_ipal` int(11) NOT NULL,
  `id_penc_air` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jml_pemakaian` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bahan_kimia_ipal`
--

INSERT INTO `bahan_kimia_ipal` (`id_bahan_kimia_ipal`, `id_penc_air`, `nama`, `jml_pemakaian`) VALUES
(8, 3, 'rahasia', 0.001),
(9, 3, 'rahasia1', 0.002),
(10, 4, '', 0),
(39, 7, 'kalium ', 0.0001),
(40, 7, 'sianida', 0.00004),
(43, 17, 'kalium', 111),
(44, 17, 'sianida', 100),
(47, 30, 'kalium', 0.0001),
(48, 30, 'sianida', 0.00003),
(49, 29, 'qwe', 0),
(50, 29, 'asd', 0),
(51, 31, 'karbon', 1),
(52, 31, 'monoksida', 0.0001),
(53, 32, 'karbon', 1),
(54, 32, 'kopi', 3),
(55, 42, 'assa', 1212),
(56, 42, 'sadx', 212),
(65, 51, 'asdas', 0),
(66, 52, 'asdasd', 8767),
(67, 52, 'hjdj', 978),
(68, 53, 'asdh', 87789),
(69, 53, 'ldlksd', 909),
(72, 56, 'asdasd', 123123),
(73, 56, 'a2323e', 0),
(74, 57, 'kimia', 3),
(75, 58, 'asd', 0),
(76, 58, 'asd', 0),
(77, 59, 'asd', 0),
(78, 59, 'asd', 0),
(81, 62, '', 0),
(84, 66, '', 0),
(85, 67, '', 0),
(86, 68, '', 0),
(87, 69, '', 0),
(88, 72, 'POLLYMER', 2),
(92, 76, 'Fero', 1),
(93, 76, 'Polimer', 0),
(94, 76, 'HcL', 1),
(95, 77, '', 0),
(104, 80, '', 0),
(105, 81, '', 0),
(106, 82, '', 0),
(107, 83, '', 0),
(108, 84, '', 0),
(109, 85, '', 0),
(110, 86, '', 0),
(111, 87, '', 0),
(112, 88, '', 0),
(113, 89, '', 0),
(114, 90, '', 0),
(115, 91, '', 0),
(116, 92, '', 0),
(120, 79, 'Polimer', 0),
(121, 79, 'DCA', 0),
(125, 74, 'L.T.I', 30),
(126, 74, 'Kapur', 16),
(127, 74, 'Dekaplok', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `baku_mutu_izin`
--

CREATE TABLE `baku_mutu_izin` (
  `id_baku_mutu_izin` int(11) NOT NULL,
  `id_pembuangan` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `ket` varchar(1000) NOT NULL,
  `nilai_baku_mutu` decimal(8,2) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `jenis_baku_mutu` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap`
--

CREATE TABLE `bap` (
  `id_bap` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `bap_hari` varchar(6) DEFAULT NULL,
  `bap_tgl` date DEFAULT NULL,
  `bap_jam` time DEFAULT NULL,
  `p2_nama` varchar(100) DEFAULT NULL,
  `p2_jabatan` varchar(100) DEFAULT NULL,
  `p2_alamat` text,
  `p2_telp` varchar(50) DEFAULT NULL,
  `catatan` text,
  `dok_lingk` tinyint(1) DEFAULT NULL,
  `dok_lingk_jenis` varchar(50) DEFAULT NULL,
  `dok_lingk_tahun` varchar(4) DEFAULT NULL,
  `izin_lingk` tinyint(1) DEFAULT NULL,
  `izin_lingk_tahun` varchar(4) DEFAULT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL,
  `conf_pengirim` varchar(255) DEFAULT NULL,
  `conf_via` varchar(255) DEFAULT NULL,
  `conf_tgl_kirim` date DEFAULT NULL,
  `conf_url` text,
  `conf_status` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `compare_status` tinyint(4) DEFAULT '0',
  `is_compared` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap`
--

INSERT INTO `bap` (`id_bap`, `id_industri`, `id_pegawai`, `bap_hari`, `bap_tgl`, `bap_jam`, `p2_nama`, `p2_jabatan`, `p2_alamat`, `p2_telp`, `catatan`, `dok_lingk`, `dok_lingk_jenis`, `dok_lingk_tahun`, `izin_lingk`, `izin_lingk_tahun`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `conf_pengirim`, `conf_via`, `conf_tgl_kirim`, `conf_url`, `conf_status`, `status`, `compare_status`, `is_compared`) VALUES
(3, 1, 1, '0', '2016-11-12', '13:45:00', 'pak rahasia', 'rahasia', NULL, '0812381283123', 'rahasia nih yaaa, rahasia!', 1, 'Amdal', '2016', 1, '2016', '2016-11-12', '2016-11-12', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(21, 1, 1, '0', '2016-12-09', '01:30:00', 'qwe', 'asd', NULL, '21221', '', 0, '', '', 0, '', '2016-12-05', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(25, 5, 1, '0', '2016-12-12', '13:00:00', 'pak ciwi', '4321', NULL, '1234', 'lain lain', 1, 'DELH', '2016', 1, '2011', '2016-12-17', '2016-12-17', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(29, 6, 4, NULL, '2016-12-18', '11:00:00', 'asd', 'asd', NULL, '123', 'rahasia lain', 1, 'Amdal', '2016', 1, '2016', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0),
(30, 3, 2, NULL, '2016-12-18', '12:15:00', 'anuan', 'rahasia', NULL, '123', 'rahasia pertama', 1, 'DELH', '2013', 1, '2011', '2016-12-19', '2016-12-19', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(33, 6, 4, NULL, '2016-12-18', '12:30:00', 'pak golf', 'dir', NULL, '123', 'rahasia pertama', 1, 'DELH', '2012', 1, '2016', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(37, 6, 1, NULL, '2016-12-18', '16:30:00', 'asd', 'asd', NULL, 'asd', 'asdasd', 0, 'DPLH', '', 0, '', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(39, 6, 1, NULL, '2016-12-18', '16:45:00', 'asd', 'asd', NULL, 'asd', 'asdads', 0, '', '', 0, '', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(40, 7, 1, '0', '2015-02-11', '16:45:00', 'elektri', 'sekretaris', NULL, '123', 'asdasdasdad', 0, '', '', 0, '', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(42, 7, 3, NULL, '2016-12-18', '17:15:00', 'alaihim', 'korlap', NULL, '1234', 'asdasd', 1, 'DELH', '2012', 1, '2016', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(43, 7, 3, NULL, '2016-12-18', '17:15:00', 'alaihim', 'korlap', NULL, '1234', 'asdasd', 1, 'DELH', '2012', 1, '2016', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(44, 7, 3, NULL, '2016-12-18', '17:15:00', 'alaihim', 'korlap', NULL, '1234', 'asdasd', 1, 'DELH', '2012', 1, '2016', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(45, 7, 1, NULL, '2016-12-18', '17:15:00', 'alaihim mustafa', 'danlap', NULL, '1234', 'rahasia pertama', 0, '', '', 0, '', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(46, 8, 1, '0', '2016-12-18', '21:15:00', 'pak limbuh', 'penjaga', NULL, '123', 'rahasia pertama', 0, '', '', 0, '', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(47, 8, 3, NULL, '2016-12-13', '21:30:00', 'pak limbuh', 'mandor', NULL, '129018', 'sdfsdfsdf', 0, '', '', 0, '', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(50, 9, 1, NULL, '2016-12-18', '23:30:00', 'pak jagal', 'jagal man', NULL, '123817', '', 0, '', '', 0, '', '2016-12-18', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0),
(51, 9, 1, NULL, '2016-12-13', '23:00:00', 'asd', 'asd', NULL, 'asd', '', 1, 'DELH', '2012', 1, '2015', '2016-12-19', '2016-12-19', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(52, 11, 4, '0', '2016-12-20', '21:00:00', 'pak sp', 'pengelola', NULL, '1231', 'sdsdf', 0, '', '', 0, '', '2016-12-20', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(53, 11, 1, NULL, '2016-12-20', '21:45:00', 'qwe', 'asd', NULL, 'asd', 'rahasia terakhir', 0, '', '', 1, '2015', '2016-12-20', '2016-12-20', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(54, 10, 3, '0', '2016-12-20', '22:45:00', 'pak rindu', 'penjaga', NULL, '1987', 'lain lain terakhir', 0, '', '', 0, '', '2016-12-21', '2016-12-21', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(55, 10, 4, NULL, '2016-12-21', '07:30:00', 'askd', 'kjsdhf', NULL, '8167', '', 1, 'UKL-UPL', '2012', 1, '2011', '2016-12-21', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(56, 10, 4, NULL, '2016-12-21', '07:45:00', 'hadoop', 'manager', NULL, '6666', '', 0, '', '', 0, '', '2016-12-21', '2016-12-21', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(57, 12, 2, NULL, '2016-12-21', '22:45:00', 'pak surya', 'rahasia', NULL, '023108', 'rahasia kesalahan', 1, 'DELH', '2015', 1, '2013', '2016-12-21', '2016-12-21', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(58, 12, 3, NULL, '2016-12-21', '23:15:00', 'pak rahasia', 'rahasia', NULL, '123123', 'sasddsa', 0, '', '', 0, '', '2016-12-21', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(59, 13, 1, NULL, '2016-12-22', '14:15:00', 'asdb', 'askd', NULL, 'aksd', '', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(60, 13, 3, NULL, '2016-12-22', '14:45:00', 'jsdhk', 'ansdnansd', NULL, 'lnasn', 'lain pertama pertama', 1, 'DELH', '2013', 1, '2012', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(61, 14, 2, NULL, '2016-12-22', '16:00:00', 'askdjh', 'jaksdkjahskjd', NULL, 'kjasdajsdh', '', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(62, 14, 4, NULL, '2016-12-22', '16:00:00', 'adasd', 'asdasd', NULL, 'asasd', 'asdasd', 1, 'DPLH', '2012', 1, '2015', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(63, 15, 1, NULL, '2016-12-21', '17:15:00', 'pak hantu', 'penjagal', NULL, '7126877', 'lain pertama', 0, '', '', 1, '2014', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(64, 15, 2, NULL, '2016-12-22', '17:30:00', 'asdasd', 'asdasd', NULL, 'asdasd', 'lain pertama partial', 1, 'DPLH', '2012', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(66, 7, 1, NULL, '2016-12-13', '19:15:00', 'asd', 'asd', NULL, 'asd', 'asd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(67, 7, 1, NULL, '2016-12-13', '19:15:00', 'asd', 'asd', NULL, 'asd', 'asd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(70, 7, 1, NULL, '2016-12-13', '19:15:00', 'asd', 'asd', NULL, 'asd', 'asd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(71, 6, 1, NULL, '2016-12-22', '20:15:00', 'asd', 'asd', NULL, 'asd', 'asdasd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(72, 6, 1, NULL, '2016-12-22', '20:15:00', 'asd', 'asd', NULL, 'asd', 'asdasd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 0),
(73, 12, 4, NULL, '2016-12-22', '20:30:00', 'asd', 'asd', NULL, 'asd', 'sdsad', 0, '', '', 0, '', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(74, 12, 4, NULL, '2016-12-22', '20:30:00', 'asd', 'asd', NULL, 'asd', 'sdsad', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(75, 12, 4, NULL, '2016-12-22', '20:30:00', 'asd', 'asd', NULL, 'asd', 'sdsad', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(76, 13, 4, NULL, '2016-11-29', '20:45:00', 'adasd', 'asdasd', NULL, 'sdasd', 'asdasd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(77, 13, 4, NULL, '2016-11-29', '20:45:00', 'adasd', 'asdasd', NULL, 'sdasd', 'asdasd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(78, 8, 1, NULL, '2016-12-15', '21:00:00', 'asd', 'asd', NULL, 'asd', 'dkjald', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(79, 8, 1, NULL, '2016-12-15', '21:00:00', 'asd', 'asd', NULL, 'asd', 'dkjald', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(80, 14, 1, NULL, '2016-12-15', '21:15:00', 'asd', 'asd', NULL, 'asd', 'jhsbdn', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(81, 14, 1, NULL, '2016-12-15', '21:15:00', 'asd', 'asd', NULL, 'asd', 'jhsbdn', 1, 'DELH', '213', 1, '2131', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(82, 10, 2, NULL, '2016-12-23', '21:30:00', 'asd', 'asd', NULL, 'asd', 'ahsnjdsd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(83, 10, 2, NULL, '2016-12-23', '21:30:00', 'asd', 'asd', NULL, 'asd', 'ahsnjdsd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(84, 10, 2, NULL, '2016-12-23', '21:30:00', 'asd', 'asd', NULL, 'asd', 'ahsnjdsd', 0, '', '', 0, '', '2016-12-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(85, 18, 7, NULL, '2017-04-04', '16:50:00', 'FAHMI ELHAQ', 'HRD', NULL, '085222060668', '', 1, 'UKL-UPL', '2009', 1, 'dipe', '2018-10-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(87, 17, 9, NULL, '2017-04-06', '16:30:00', 'Leo Liandra', 'Manager GA', NULL, '081320734735', '', 1, 'UKL-UPL', '2016', 1, '2016', '2018-10-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(88, 21, 7, NULL, '2017-04-10', '16:15:00', 'Tina Anggraeni', 'Staf umum dan Personalia', NULL, '082129168900', '', 1, 'UKL-UPL', '2006', 1, '2006', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(89, 19, 6, NULL, '2018-09-20', '17:00:00', 'Aritonang, SH', 'Kepala HRD', NULL, '081322292795', '', 1, 'UKL-UPL', '2014', 1, '2017', '2018-10-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(90, 20, 9, NULL, '2018-09-04', '16:30:00', 'Saemuri', 'Enginerering Manager', NULL, '081320497818', '', 1, 'UKL-UPL', '2007', 1, '', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(91, 22, 6, NULL, '2018-09-28', '16:30:00', 'Johan', 'Direktur', NULL, '022-5203311', '', 1, 'UKL-UPL', '2016', 1, '', '2018-10-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(92, 19, 5, NULL, '2018-10-22', '14:00:00', 'Agi', 'D', NULL, '085222271147', '', 0, '', '', 0, '', '2018-10-22', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(93, 19, 10, '0', '2018-10-20', '17:00:00', 'Jusuf Tjandra Djaja', 'Direktur', NULL, '0225951804', '', 1, 'UKL-UPL', '2014', 1, '2017', '2018-11-06', '2018-11-06', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(94, 23, 7, NULL, '2017-10-10', '10:45:00', 'Yudi Sutisna ', 'Direktur', NULL, '08164868216', '', 1, 'UKL-UPL', '2014', 1, '2016', '2018-11-01', NULL, 'Administrator', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0),
(96, 24, 10, '0', '2018-10-01', '16:15:00', 'Iwan Santoso', 'Direktur', NULL, '0225951632', '', 1, 'UKL-UPL', '2013', 1, '2016', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, NULL, 0, 0, 1, 1),
(97, 29, 9, NULL, '2018-02-02', '12:15:00', 'Nanang Juhana ', 'Devisi Umum', NULL, '085321373843', '', 1, 'UKL-UPL', '2015', 1, '2016', '2018-11-05', '2018-11-05', '', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_agro`
--

CREATE TABLE `bap_agro` (
  `id_bap_agro` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  `status_perusahaan` varchar(255) DEFAULT NULL,
  `kapasitas_produksi` int(11) DEFAULT NULL,
  `jenis_produk` varchar(255) DEFAULT NULL,
  `pakai_pupuk` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_agro`
--

INSERT INTO `bap_agro` (`id_bap_agro`, `id_bap`, `jumlah_karyawan`, `status_perusahaan`, `kapasitas_produksi`, `jenis_produk`, `pakai_pupuk`) VALUES
(3, 3, 100, 'PMA', 1000, 'rahasia', 'Organik'),
(19, 21, 0, 'PMDN', 12, '22', 'Organik'),
(23, 25, 20, 'PMDN', 100, 'pupuk', 'Anorganik'),
(26, 30, 200, 'PMDN', 100, 'rahasia', 'Organik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_golf`
--

CREATE TABLE `bap_golf` (
  `id_bap_golf` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jumlah_hole` int(11) DEFAULT NULL,
  `rata_pengunjung` int(11) DEFAULT NULL,
  `jml_membership` int(11) DEFAULT NULL,
  `pakai_pupuk` varchar(100) DEFAULT NULL,
  `pakai_pestisida` varchar(100) DEFAULT NULL,
  `pakai_oli` varchar(100) DEFAULT NULL,
  `simpan_oli_bekas` varchar(255) DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_golf`
--

INSERT INTO `bap_golf` (`id_bap_golf`, `id_bap`, `jumlah_hole`, `rata_pengunjung`, `jml_membership`, `pakai_pupuk`, `pakai_pestisida`, `pakai_oli`, `simpan_oli_bekas`, `jumlah_karyawan`) VALUES
(2, 33, 90, 80, 70, 'lumayan', 'sedikit', 'lumayan', 'sedikit', 100),
(5, 37, 0, 0, 0, 'asd', 'asd', 'asd', 'asd', 0),
(7, 39, 0, 0, 0, 'asd', 'asd', 'asd', 'asd', 0),
(8, 71, 0, 0, 0, 'asd', 'asd', 'asd', 'asd', 0),
(9, 72, 21, 12, 21, 'asd', 'asd', 'asd', 'asd', 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_hotel`
--

CREATE TABLE `bap_hotel` (
  `id_bap_hotel` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `skala_kegiatan` varchar(100) DEFAULT NULL,
  `jml_kamar` int(11) DEFAULT NULL,
  `tingkat_hunian` varchar(50) DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_hotel`
--

INSERT INTO `bap_hotel` (`id_bap_hotel`, `id_bap`, `skala_kegiatan`, `jml_kamar`, `tingkat_hunian`, `jumlah_karyawan`) VALUES
(1, 57, 'Bintang 5', 23, '3', 20),
(2, 58, 'Melati 3', 12, '1', 222),
(3, 73, 'Bintang 2', 12, '21', 12),
(4, 74, 'Bintang 2', 12, '21', 12),
(5, 75, 'Bintang 2', 12, '21', 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_industri`
--

CREATE TABLE `bap_industri` (
  `id_bap_industri` int(11) NOT NULL,
  `proses_produksi` varchar(255) DEFAULT NULL,
  `kapasitas_produksi` varchar(255) DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  `id_bap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_industri`
--

INSERT INTO `bap_industri` (`id_bap_industri`, `proses_produksi`, `kapasitas_produksi`, `jumlah_karyawan`, `id_bap`) VALUES
(1, 'Finishing, Washing', '100', 100, 40),
(4, 'Finishing', '100', 200, 45),
(5, 'Finishing, Washing', '12', 2, 66),
(9, 'Finishing, Washing', '12', 2, 70),
(10, 'Desizing, Finishing, Scouring', '23.471.281', 0, 85),
(12, 'Dyeing', '594.000', 1300, 87),
(13, 'Sizing, Weaving', '11000000', 150, 88),
(14, '', '15', 200, 89),
(15, 'Proses Esterifikasi, Proses Pencampuran Dyes, Proses Polymerisasi, Proses Sulphitasi', '216', 47, 90),
(16, 'Dyeing', 'Kain Grey = 20,83 dan Kain Jadi = 50', 130, 91),
(18, 'Dyeing', '3.300 lusin atau 15.000 kg', 200, 93),
(19, 'Finishing', '11000', 345, 94),
(21, 'Dyeing', 'Terpasang Benang Celup = 2000 Ton/Tahun ; Senyatanya Benang Celup 700 Ton/Tahun', 450, 96),
(22, '', '6300 ton/tahun', 73, 97);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_lab`
--

CREATE TABLE `bap_lab` (
  `id_bap_lab` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `status_akreditasi` tinyint(1) DEFAULT NULL,
  `jml_sampel` float DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_lab`
--

INSERT INTO `bap_lab` (`id_bap_lab`, `id_bap`, `status_akreditasi`, `jml_sampel`, `jumlah_karyawan`) VALUES
(1, 59, 1, 200, 123),
(2, 60, 0, 100, 12381238),
(3, 76, 1, 121, 23123),
(4, 77, 1, 121, 23123);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_plb3`
--

CREATE TABLE `bap_plb3` (
  `id_bap_plb3` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  `bidang_usaha` varchar(500) DEFAULT NULL,
  `izin_jenis` varchar(500) DEFAULT NULL,
  `izin_b3_no` varchar(250) DEFAULT NULL,
  `masa_berlaku` varchar(500) DEFAULT NULL,
  `jenis_limbah_b3` varchar(500) DEFAULT NULL,
  `kelola_prinsip` varchar(500) DEFAULT NULL,
  `proses_produksi` varchar(500) DEFAULT NULL,
  `kapasitas_produksi` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_plb3`
--

INSERT INTO `bap_plb3` (`id_bap_plb3`, `id_bap`, `jumlah_karyawan`, `bidang_usaha`, `izin_jenis`, `izin_b3_no`, `masa_berlaku`, `jenis_limbah_b3`, `kelola_prinsip`, `proses_produksi`, `kapasitas_produksi`) VALUES
(1, 46, 100, 'limbah', 'limba', 'limbah', 'limbah', 'Accu Bekas, B3 kadaluarsa, Bekas kemasan terkontaminasi limbah B3, Lampu TL Bekas, Limbah B3 cair, Lumpur IPAL', 'limbah', 'Finishing, Washing', '20'),
(2, 47, 100, 'limbah banyak', 'rahasia', 'rahasia', 'rahasia', 'Accu Bekas, B3 kadaluarsa', 'sedikit', 'Finishing, Washing', '100'),
(3, 78, 22, 'rahasia', 'rahasia', '13988389', 'rahasia', 'Accu Bekas, B3 kadaluarsa, Bekas kemasan terkontaminasi limbah B3', 'rahasia', 'Finishing, Washing', '12'),
(4, 79, 22, 'rahasia', 'rahasia', '13988389', 'rahasia', 'Accu Bekas, B3 kadaluarsa, Bekas kemasan terkontaminasi limbah B3', 'rahasia', 'Finishing, Washing', '12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_rm`
--

CREATE TABLE `bap_rm` (
  `id_bap_rm` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jml_meja` int(11) DEFAULT NULL,
  `izin_usaha` tinyint(1) DEFAULT NULL,
  `izin_usaha_tahun` varchar(4) DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_rm`
--

INSERT INTO `bap_rm` (`id_bap_rm`, `id_bap`, `jml_meja`, `izin_usaha`, `izin_usaha_tahun`, `jumlah_karyawan`) VALUES
(1, 54, 22, 1, '2012', 12),
(2, 56, 22, 1, '2014', 12),
(3, 82, 21, 0, '', 22),
(4, 83, 21, 0, '', 22),
(5, 84, 21, 0, '', 22);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_rph`
--

CREATE TABLE `bap_rph` (
  `id_bap_rph` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  `jumlah_ternak` int(11) DEFAULT NULL,
  `catatan_lain` varchar(250) DEFAULT NULL,
  `koord_rph_derajat_s` varchar(50) DEFAULT NULL,
  `koord_rph_jam_s` varchar(50) DEFAULT NULL,
  `koord_rph_menit_s` varchar(50) DEFAULT NULL,
  `koord_rph_derajat_e` varchar(50) DEFAULT NULL,
  `koord_rph_jam_e` varchar(50) DEFAULT NULL,
  `koord_rph_menit_e` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_rph`
--

INSERT INTO `bap_rph` (`id_bap_rph`, `id_bap`, `jumlah_karyawan`, `jumlah_ternak`, `catatan_lain`, `koord_rph_derajat_s`, `koord_rph_jam_s`, `koord_rph_menit_s`, `koord_rph_derajat_e`, `koord_rph_jam_e`, `koord_rph_menit_e`) VALUES
(1, 50, 12, 222, NULL, '1', '2', '3', '4', '5', '6'),
(2, 51, 12, 333, NULL, '6', '5', '4', '3', '2', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_rs`
--

CREATE TABLE `bap_rs` (
  `id_bap_rs` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jml_pasien` int(11) DEFAULT NULL,
  `jml_tempat_tidur` int(11) DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_rs`
--

INSERT INTO `bap_rs` (`id_bap_rs`, `id_bap`, `jml_pasien`, `jml_tempat_tidur`, `jumlah_karyawan`) VALUES
(1, 63, 300, 20, 300),
(2, 64, 536, 355, 13123);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_sppbe`
--

CREATE TABLE `bap_sppbe` (
  `id_bap_sppbe` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL,
  `kapasitas_pengisian` double(10,4) DEFAULT '0.0000',
  `koord_sppbe_derajat_s` varchar(255) DEFAULT NULL,
  `koord_sppbe_jam_s` varchar(255) DEFAULT NULL,
  `koord_sppbe_menit_s` varchar(255) DEFAULT NULL,
  `koord_sppbe_derajat_e` varchar(255) DEFAULT NULL,
  `koord_sppbe_jam_e` varchar(255) DEFAULT NULL,
  `koord_sppbe_menit_e` varchar(255) DEFAULT NULL,
  `jumlah_kolam_pemadam` int(11) DEFAULT NULL,
  `ukuran_kolam_pemadam` double(10,2) DEFAULT NULL,
  `jumlah_apar` int(11) DEFAULT NULL,
  `kapasitas_apar` double(10,2) DEFAULT NULL,
  `agen_dilayani` int(11) DEFAULT NULL,
  `jumlah_sumur_resapan` int(11) DEFAULT NULL,
  `ukuran_sumur_resapan` double(10,2) DEFAULT NULL,
  `jumlah_biopori` int(11) DEFAULT NULL,
  `catatan_lain` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_sppbe`
--

INSERT INTO `bap_sppbe` (`id_bap_sppbe`, `id_bap`, `jumlah_karyawan`, `kapasitas_pengisian`, `koord_sppbe_derajat_s`, `koord_sppbe_jam_s`, `koord_sppbe_menit_s`, `koord_sppbe_derajat_e`, `koord_sppbe_jam_e`, `koord_sppbe_menit_e`, `jumlah_kolam_pemadam`, `ukuran_kolam_pemadam`, `jumlah_apar`, `kapasitas_apar`, `agen_dilayani`, `jumlah_sumur_resapan`, `ukuran_sumur_resapan`, `jumlah_biopori`, `catatan_lain`) VALUES
(1, 52, 100, 200.0000, '1', '2', '3', '4', '5', '6', 2121, 12.00, 1212, 1212.00, 33, 2, 3.00, 3, NULL),
(2, 53, 222, 212.0000, '1', '2', '3', '4', '5', '6', 2, 2.00, 33, 5.00, 20, 3, 33.00, 32222, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bap_tambang`
--

CREATE TABLE `bap_tambang` (
  `id_bap_tambang` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `jenis_tambang` varchar(255) DEFAULT NULL,
  `izin_usaha` varchar(255) DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bap_tambang`
--

INSERT INTO `bap_tambang` (`id_bap_tambang`, `id_bap`, `jenis_tambang`, `izin_usaha`, `jumlah_karyawan`) VALUES
(1, 61, 'batu', '16258798', 12),
(2, 62, 'batu', '364629873', 121654),
(3, 80, 'batu', 'huekkl', 3747),
(4, 81, 'batu', 'huekkl', 3747);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('038bf516446ef4a32ec7101ae5a58d42', '114.124.245.179', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541405095, ''),
('1745d11b90c5f20a83da4e6e2b7f01c4', '158.69.225.37', 'Mozilla/5.0 (compatible; Dataprovider.com)', 1541545995, ''),
('1cec7daa5ef743f8c11059d06e48d6b3', '36.72.20.92', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1541042156, ''),
('274c9cd969ae4c4893b230d65f3eb63e', '182.0.196.136', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1540952959, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('30545bf360ae4100bb3c4bfe4729acac', '114.124.196.51', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406790, ''),
('35797d680d9da6183b8d7f3d749b324c', '114.124.197.156', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406792, ''),
('3f646fc7b7979c95dc08fc36b88961e6', '114.124.228.212', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541405094, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('5095e1243c0a1aa3abde0bfc9f8a1305', '114.124.140.83', 'Mozilla/5.0 (Linux; Android 8.0.0; SM-A600G Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mob', 1541042880, ''),
('51b391295d5d466d5cb8f903fa4eefec', '114.124.197.3', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406791, ''),
('551b8b1c329f6dbd46a821f783c5df1b', '114.124.197.3', 'Mozilla/5.0 (Windows NT 5.1; rv:49.0) Gecko/20100101 Firefox/49.0', 1541407637, ''),
('561f9958b4fdcb98d8f6dd4a148ea255', '120.188.66.234', 'Mozilla/5.0 (Windows NT 5.1; rv:49.0) Gecko/20100101 Firefox/49.0', 1541488434, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('5bfbd3531480bb663ce20de0e4930eeb', '220.247.175.58', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541578228, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('6567846c8a5bc99da41d346c367192c0', '114.124.197.3', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406793, ''),
('6a7e0a3abd3536ac5d645c951aa78c8f', '36.72.20.92', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1541055569, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('6c8e27a1d4378c8bd3f1871ac218571b', '120.188.66.234', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0', 1541492013, ''),
('78f16bc49fe69b3f12e11c041f3dc74f', '114.124.196.51', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406792, ''),
('799bef09da700613da340fd2facb5a3b', '114.124.197.3', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541407177, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('7cfd7c2d7e70d8e6cd7a710aeb88a9a2', '114.124.228.35', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406792, ''),
('85eeb2a1ac944fa74c6592d7992d1ab7', '120.188.66.234', 'Mozilla/5.0 (Windows NT 5.1; rv:49.0) Gecko/20100101 Firefox/49.0', 1541560063, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('8d5df2b623b967eb285068faf554688b', '114.124.245.34', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406789, ''),
('92f777a9ba85c6665f8a71eda0630a46', '114.124.139.118', 'Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-A600G Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/7', 1541502304, ''),
('a29b79c05cde355d73b70ddc6cda3ed3', '114.124.229.178', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406792, ''),
('afabbca876359f567defa45e665c4463', '114.124.197.3', 'Mozilla/5.0 (Windows NT 5.1; rv:49.0) Gecko/20100101 Firefox/49.0', 1541407636, ''),
('c4bd397dda14ef681ae47f4e8054f4a4', '120.188.66.234', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0', 1541493150, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('c670a4c0ccfffdff74da2099cae4cb58', '114.124.197.3', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406790, ''),
('c6a658a8ff35b2c446f14c88a2df7ddc', '124.81.123.98', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0', 1541491809, ''),
('c7874629cb0b15ede050f9e13d406986', '64.233.173.43', 'Mozilla/5.0 (Linux; Android 8.0.0; SM-A600G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.80 Mobile Safari/53', 1541484253, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('cd8a73a08af19cf07c25c2ef58894136', '202.134.2.13', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36', 1541503553, 'a:5:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"2\";s:8:\"username\";s:5:\"admin\";s:6:\"status\";s:1:\"1\";s:8:\"group_id\";s:1:\"2\";}'),
('d9da05a9436a062becf6de1f2ab71b4d', '120.188.66.234', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0', 1541492013, ''),
('e2ceceb74e1f9e8dcb8bdef2a5f30be5', '114.124.228.35', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406791, ''),
('e3fc356f01f599c8086e47dca2512d47', '114.124.139.118', 'Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-A600G Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/7', 1541502304, ''),
('f332d77d86cf741d5abc3e6932266abf', '114.124.197.3', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541406792, ''),
('f807a5d51500daacde6d0ed896aa94a2', '36.65.255.34', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36', 1541407609, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_cerobong`
--

CREATE TABLE `data_cerobong` (
  `id_cerobong` int(11) NOT NULL,
  `id_penc_udara` int(11) NOT NULL,
  `h_cerobong` varchar(255) DEFAULT NULL,
  `d_cerobong` varchar(255) DEFAULT NULL,
  `sampling_hole` tinyint(1) DEFAULT NULL,
  `tangga` tinyint(1) DEFAULT NULL,
  `pengaman_tangga` tinyint(1) DEFAULT NULL,
  `lantai` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_cerobong`
--

INSERT INTO `data_cerobong` (`id_cerobong`, `id_penc_udara`, `h_cerobong`, `d_cerobong`, `sampling_hole`, `tangga`, `pengaman_tangga`, `lantai`) VALUES
(17, 3, '2', '10', 1, 1, 0, 1),
(18, 3, '3', '13', 0, 1, 1, 0),
(19, 3, '', '', 0, 0, 0, 0),
(20, 3, '', '', 0, 0, 0, 0),
(21, 4, '', '', NULL, NULL, NULL, NULL),
(22, 4, '', '', NULL, NULL, NULL, NULL),
(23, 4, '', '', NULL, NULL, NULL, NULL),
(24, 4, '', '', NULL, NULL, NULL, NULL),
(72, 5, '10', '20', 1, 1, 1, 1),
(73, 5, '', '', 0, 0, 0, 0),
(74, 5, '', '', 0, 0, 0, 0),
(75, 5, '', '', 0, 0, 0, 0),
(84, 16, '12', '21', 1, 1, 0, 0),
(85, 16, '', '', NULL, NULL, NULL, NULL),
(86, 16, '', '', NULL, NULL, NULL, NULL),
(87, 16, '', '', NULL, NULL, NULL, NULL),
(88, 14, '12', '22', 1, 0, 1, 1),
(89, 14, '', '', 0, 0, 0, 0),
(90, 14, '', '', 0, 0, 0, 0),
(91, 14, '', '', 0, 0, 0, 0),
(92, 17, '12', '21', 1, 1, 0, 0),
(93, 17, '', '', NULL, NULL, NULL, NULL),
(94, 17, '', '', NULL, NULL, NULL, NULL),
(95, 17, '', '', NULL, NULL, NULL, NULL),
(96, 18, '12', '21', 1, 0, 0, 1),
(97, 18, '', '', NULL, NULL, NULL, NULL),
(98, 18, '', '', NULL, NULL, NULL, NULL),
(99, 18, '', '', NULL, NULL, NULL, NULL),
(100, 15, '', '', 0, 0, 0, 0),
(101, 15, '', '', 0, 0, 0, 0),
(102, 15, '', '', 0, 0, 0, 0),
(103, 15, '', '', 0, 0, 0, 0),
(104, 40, '', '', 1, 1, 1, NULL),
(105, 40, '', '', NULL, NULL, NULL, NULL),
(106, 40, '', '', 1, 1, 1, NULL),
(107, 40, '', '', NULL, NULL, NULL, NULL),
(108, 41, '', '', NULL, NULL, NULL, NULL),
(109, 41, '', '', NULL, NULL, NULL, NULL),
(110, 41, '', '', NULL, NULL, NULL, NULL),
(111, 41, '', '', NULL, NULL, NULL, NULL),
(112, 42, '', '', NULL, NULL, NULL, NULL),
(113, 42, '', '', NULL, NULL, NULL, NULL),
(114, 42, '', '', NULL, NULL, NULL, NULL),
(115, 42, '', '', NULL, NULL, NULL, NULL),
(124, 45, '22', '1', 1, 1, 1, NULL),
(125, 45, '', '', NULL, NULL, NULL, NULL),
(126, 45, '', '', NULL, NULL, NULL, NULL),
(127, 45, '', '', NULL, NULL, NULL, NULL),
(132, 47, '26', '1', 1, 1, 1, 1),
(133, 47, '22', '1', 1, 1, 1, 1),
(134, 47, '26', '1', 1, 1, 1, 1),
(135, 47, '-', '-', 0, 0, 0, 0),
(136, 48, '25 ', '0,5', 1, 1, 1, 1),
(137, 48, '', '', NULL, NULL, NULL, NULL),
(138, 48, '', '', NULL, NULL, NULL, NULL),
(139, 48, '', '', NULL, NULL, NULL, NULL),
(216, 49, '30 m', '100cm', 1, 1, 1, 1),
(217, 49, '', '', NULL, NULL, NULL, NULL),
(218, 49, '', '', NULL, NULL, NULL, NULL),
(219, 49, '', '', NULL, NULL, NULL, NULL),
(252, 46, '13.2', '53', 1, 1, 1, 1),
(253, 46, '', '', NULL, NULL, NULL, NULL),
(254, 46, '', '', NULL, NULL, NULL, NULL),
(255, 46, '', '', NULL, NULL, NULL, NULL),
(276, 51, '35', '1', 1, 1, 0, 1),
(277, 51, '', '', 0, 0, 0, 0),
(278, 51, '', '', 0, 0, 0, 0),
(279, 51, '', '', 0, 0, 0, 0),
(284, 43, '22', '1', 1, 1, 1, 1),
(285, 43, '', '', 0, 0, 0, 0),
(286, 43, '', '', 0, 0, 0, 0),
(287, 43, '', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_master`
--

CREATE TABLE `data_master` (
  `id_data_master` int(11) NOT NULL,
  `value` varchar(500) NOT NULL,
  `ket` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_master`
--

INSERT INTO `data_master` (`id_data_master`, `value`, `ket`) VALUES
(5, 'jenis_limbah', 'Jenis Limbah B3'),
(6, 'proses_produksi', 'Proses Produksi'),
(7, 'jenis_dok_lingkungan', 'Jenis Dokumen Lingkungan'),
(8, 'sistem_ipal', 'Sistem IPAL'),
(9, 'badan_air_penerima', 'Badan Air Penerima'),
(10, 'sumber_air_limbah', 'Sumber Air Limbah'),
(11, 'jenis_bahan_bakar', 'Jenis Bahan Bakar'),
(12, 'unit_ipal', 'Unit Ipal'),
(13, 'jenis_pengujian', 'Jenis Pengujian'),
(14, 'pengelolaan', 'Pengelolaan Limbah Padat dan B3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_master_detail`
--

CREATE TABLE `data_master_detail` (
  `id_data_master_detail` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `jenis_data_master` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_master_detail`
--

INSERT INTO `data_master_detail` (`id_data_master_detail`, `ket`, `jenis_data_master`, `status`) VALUES
(5, 'Buttom Ash', 'jenis_limbah', 1),
(6, 'Lumpur IPAL', 'jenis_limbah', 1),
(7, 'Minyak Pelumas Bekas', 'jenis_limbah', 1),
(8, 'Lampu TL Bekas', 'jenis_limbah', 1),
(9, 'Accu Bekas', 'jenis_limbah', 1),
(10, 'Bekas kemasan terkontaminasi limbah B3', 'jenis_limbah', 1),
(11, 'Majun terkontaminasi limbah B3', 'jenis_limbah', 1),
(12, 'Limbah Elektronik', 'jenis_limbah', 1),
(13, 'Limbah B3 cair', 'jenis_limbah', 1),
(14, 'Limbah Padat B3', 'jenis_limbah', 1),
(15, 'B3 kadaluarsa', 'jenis_limbah', 1),
(16, 'Amdal', 'jenis_dok_lingkungan', 1),
(17, 'UKL-UPL', 'jenis_dok_lingkungan', 1),
(18, 'DELH', 'jenis_dok_lingkungan', 1),
(19, 'DPLH', 'jenis_dok_lingkungan', 1),
(20, 'SPPL', 'jenis_dok_lingkungan', 1),
(21, 'F', 'sistem_ipal', 1),
(22, 'F-K', 'sistem_ipal', 1),
(23, 'F-B', 'sistem_ipal', 1),
(24, 'F-K-B', 'sistem_ipal', 1),
(25, 'F-B-K', 'sistem_ipal', 1),
(26, 'Washing', 'proses_produksi', 1),
(27, 'Finishing', 'proses_produksi', 1),
(28, 'Sungai Cisangkuy', 'badan_air_penerima', 1),
(29, 'Sungai Citalugtug', 'badan_air_penerima', 1),
(30, 'Wet Scrubber', 'sumber_air_limbah', 1),
(31, 'Batubara', 'jenis_bahan_bakar', 1),
(32, 'Biomassa', 'jenis_bahan_bakar', 1),
(33, 'Bahan Bakar Minyak', 'jenis_bahan_bakar', 1),
(34, 'Gas', 'jenis_bahan_bakar', 1),
(35, 'Ekualisasi', 'unit_ipal', 1),
(36, 'Aerasi', 'unit_ipal', 1),
(39, 'Total Logam', 'jenis_pengujian', 1),
(40, 'TCLP', 'jenis_pengujian', 1),
(41, 'Dibakar', 'pengelolaan', 1),
(42, 'Diangkut oleh Pihak Ketiga', 'pengelolaan', 1),
(43, 'Diangkut oleh Dinas Pertasih', 'pengelolaan', 1),
(44, 'Dikelola oleh Warga Sekitar', 'pengelolaan', 1),
(45, 'Dijual', 'pengelolaan', 1),
(46, 'Lainnya', 'pengelolaan', 1),
(47, 'Koagulasi', 'unit_ipal', 1),
(48, 'Flokulasi', 'unit_ipal', 1),
(49, 'Sedimentasi', 'unit_ipal', 1),
(50, 'Netralisasi', 'unit_ipal', 1),
(51, 'Printing', 'sumber_air_limbah', 1),
(52, 'Finishing', 'sumber_air_limbah', 1),
(53, 'Dyeing', 'sumber_air_limbah', 1),
(54, 'Sizing', 'sumber_air_limbah', 1),
(55, 'B', 'sistem_ipal', 1),
(56, 'Printing', 'proses_produksi', 1),
(57, 'Dyeing', 'proses_produksi', 1),
(58, 'Brushing', 'proses_produksi', 1),
(59, 'Sizing', 'proses_produksi', 1),
(60, 'Desizing', 'proses_produksi', 1),
(61, 'Spining', 'proses_produksi', 1),
(62, 'Scouring', 'proses_produksi', 1),
(63, 'Bleaching', 'proses_produksi', 1),
(64, 'Weaving', 'proses_produksi', 1),
(65, 'Kniting', 'proses_produksi', 1),
(66, 'Washing', 'sumber_air_limbah', 1),
(67, 'Weaving', 'sumber_air_limbah', 1),
(68, 'Filtrasi', 'unit_ipal', 1),
(69, 'Pre Treatment', 'unit_ipal', 1),
(70, 'Fly Ash', 'jenis_limbah', 1),
(71, 'Sungai Lagas', 'badan_air_penerima', 1),
(72, 'Sungai Cilisungan', 'badan_air_penerima', 1),
(73, 'Sungai Cikacembang', 'badan_air_penerima', 1),
(74, 'Sungai Cibalekambang', 'badan_air_penerima', 1),
(75, 'Sungai Cipadaulun', 'badan_air_penerima', 1),
(76, 'Sungai Ciwalengke', 'badan_air_penerima', 1),
(77, 'Sungai Cibiuk', 'badan_air_penerima', 1),
(78, 'Sungai Cijunti', 'badan_air_penerima', 1),
(79, 'Sungai Cimande', 'badan_air_penerima', 1),
(80, 'Sungai Cisuminta', 'badan_air_penerima', 1),
(81, 'Sungai Cidawolong', 'badan_air_penerima', 1),
(82, 'Sungai Cijalupang', 'badan_air_penerima', 1),
(83, 'Sungai Cipalasari', 'badan_air_penerima', 1),
(84, 'Sungai Citepus', 'badan_air_penerima', 1),
(85, 'Proses Pencampuran Dyes', 'proses_produksi', 1),
(86, 'Proses Sulphitasi', 'proses_produksi', 1),
(87, 'Proses Polymerisasi', 'proses_produksi', 1),
(88, 'Proses Esterifikasi', 'proses_produksi', 1),
(89, 'Tidak Ada Badan Air Penerima', 'badan_air_penerima', 1),
(90, 'Fly Ash / Button Ash', 'jenis_limbah', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `debit_air_limbah`
--

CREATE TABLE `debit_air_limbah` (
  `id_debit_air_limbah` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `metode_sampling` varchar(255) NOT NULL,
  `laporan_bulan` varchar(10) NOT NULL,
  `min` decimal(5,2) NOT NULL,
  `max` decimal(5,2) NOT NULL,
  `rerata` decimal(5,2) NOT NULL,
  `nama_berkas` varchar(500) NOT NULL,
  `status_upload` tinyint(4) NOT NULL DEFAULT '0',
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `debit_air_limbah`
--

INSERT INTO `debit_air_limbah` (`id_debit_air_limbah`, `id_industri`, `metode_sampling`, `laporan_bulan`, `min`, `max`, `rerata`, `nama_berkas`, `status_upload`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 5, 'komposit', '12.2016', '1.00', '2.00', '3.00', '', 0, '2016-12-11', '2016-12-11', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_lhu`
--

CREATE TABLE `detail_lhu` (
  `id_detail_lhu` int(11) NOT NULL,
  `id_laporan_hasil_uji` int(11) NOT NULL,
  `id_parameter_lhu` int(11) NOT NULL,
  `id_baku_mutu_izin` int(11) NOT NULL,
  `nilai_hasil_uji` decimal(5,2) NOT NULL,
  `nilai_baku_mutu` varchar(10) NOT NULL,
  `status_pelanggaran` tinyint(4) NOT NULL DEFAULT '0',
  `metode_pengujian` varchar(255) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_lhu`
--

INSERT INTO `detail_lhu` (`id_detail_lhu`, `id_laporan_hasil_uji`, `id_parameter_lhu`, `id_baku_mutu_izin`, `nilai_hasil_uji`, `nilai_baku_mutu`, `status_pelanggaran`, `metode_pengujian`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 2, 286, 0, '100.00', '230', 0, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(2, 2, 287, 0, '120.00', '750', 0, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(3, 2, 288, 0, '300.00', '825', 0, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(4, 2, 289, 0, '600.00', '20', 1, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(5, 4, 286, 0, '500.00', '230', 1, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(6, 4, 287, 0, '550.00', '750', 0, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(7, 4, 288, 0, '60.00', '825', 0, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(8, 4, 289, 0, '999.99', '20', 1, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `emisi_boiler`
--

CREATE TABLE `emisi_boiler` (
  `id_emisi_boiler` int(11) NOT NULL,
  `id_penc_udara` int(11) NOT NULL,
  `id_jenis_emisi_boiler` int(11) NOT NULL,
  `jml_boiler` varchar(255) DEFAULT NULL,
  `merk` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `kapasitas` varchar(255) DEFAULT NULL,
  `bahan_bakar` varchar(255) DEFAULT NULL,
  `jml_bahan_bakar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `emisi_boiler`
--

INSERT INTO `emisi_boiler` (`id_emisi_boiler`, `id_penc_udara`, `id_jenis_emisi_boiler`, `jml_boiler`, `merk`, `type`, `kapasitas`, `bahan_bakar`, `jml_bahan_bakar`) VALUES
(9, 3, 1, '1', 'cat', 'updown', '10', 'Bahan Bakar Minyak', '0.001'),
(10, 3, 2, '2', 'cat', 'rahasia', '20', 'Batubara', '0.002'),
(11, 3, 3, '3', 'cater', 'downup', '30', 'Biomassa', '0.003'),
(12, 3, 4, '4', 'pillar', 'rahasia', '40', 'Gas', '0.004'),
(13, 4, 1, '', '', '', '', '', ''),
(14, 4, 2, '', '', '', '', '', ''),
(15, 4, 3, '', '', '', '', '', ''),
(16, 4, 4, '', '', '', '', '', ''),
(17, 5, 1, '1', 'm', 't', 'k', 'Bahan Bakar Minyak', '4'),
(18, 5, 2, '2', 'e', 'y', 'a', 'Batubara', '3'),
(19, 5, 3, '3', 'r', 'p', 'p', 'Biomassa', '2'),
(20, 5, 4, '4', 'k', 'e', 'a', 'Gas', '1'),
(25, 14, 1, '1', 'm', 't', 'k', 'Bahan Bakar Minyak', '4'),
(26, 14, 2, '2', 'e', 'y', 'a', 'Batubara', '3'),
(27, 14, 3, '3', 'r', 'p', 'p', 'Biomassa', '2'),
(28, 14, 4, '4', 'k', 'e', 'a', 'Gas', '1'),
(29, 15, 1, '', '', '', '', '', ''),
(30, 15, 2, '', '', '', '', '', ''),
(31, 15, 3, '', '', '', '', '', ''),
(32, 15, 4, '', '', '', '', '', ''),
(33, 16, 1, '1', 'm', 't', 'k', 'Bahan Bakar Minyak', '4'),
(34, 16, 2, '2', 'e', 'y', 'p', 'Batubara', '3'),
(35, 16, 3, '3', 'r', 'p', 'a', 'Biomassa', '2'),
(36, 16, 4, '4', 'k', 'e', 's', 'Gas', '1'),
(37, 17, 1, '1', 'm', 't', 'k', 'Bahan Bakar Minyak', '4'),
(38, 17, 2, '2', 'e', 'y', 'a', 'Batubara', '3'),
(39, 17, 3, '3', 'r', 'p', 'p', 'Biomassa', '2'),
(40, 17, 4, '4', 'k', 'e', 'as', 'Gas', '1'),
(41, 18, 1, '1', 'm', 't', 'k', 'Bahan Bakar Minyak', '0.001'),
(42, 18, 2, '2', 'e', 'y', 'a', 'Batubara', '0.002'),
(43, 18, 3, '3', 'r', '', 'p', 'Biomassa', '0.003'),
(44, 18, 4, '4', 'k', 'e', 'a', 'Biomassa', '0.004'),
(45, 40, 1, '1', '', '', '17', 'Batubara', ''),
(46, 40, 2, '', '', '', '', '', ''),
(47, 40, 3, '1', '', '', '3000000', 'Batubara', ''),
(48, 40, 4, '', '', '', '', '', ''),
(49, 41, 1, '', '', '', '', '', ''),
(50, 41, 2, '', '', '', '', '', ''),
(51, 41, 3, '', '', '', '', '', ''),
(52, 41, 4, '', '', '', '', '', ''),
(53, 42, 1, '', '', '', '', '', ''),
(54, 42, 2, '', '', '', '', '', ''),
(55, 42, 3, '', '', '', '', '', ''),
(56, 42, 4, '', '', '', '', '', ''),
(57, 43, 1, '1', 'Basuki', 'Chain grade', '6 ton/jam', 'Batubara', '3,5 ton/hiar'),
(58, 43, 2, '', '', '', '', '', ''),
(59, 43, 3, '1', 'Basuki', 'Chain grade', '10 ton/jam', 'Batubara', '3,5 ton/hari'),
(60, 43, 4, '', '', '', '', '', ''),
(65, 45, 1, '1', 'Basuki', 'Chain grade', '6 ton/jam', 'Batubara', '3,5'),
(66, 45, 2, '', '', '', '', '', ''),
(67, 45, 3, '1', 'Basuki', 'Chain grade', '10 ton/jam', 'Batubara', '3,5'),
(68, 45, 4, '', '', '', '', '', ''),
(73, 47, 1, '1', 'Hamada', '-', '-', 'Batubara', '128'),
(74, 47, 2, '1', 'Basuki', '-', '-', 'Batubara', '80'),
(75, 47, 3, '1', 'Basuki', '-', '5.000.000 Kcal', 'Batubara', '-'),
(76, 47, 4, '-', '-', '-', '-', '', '-'),
(77, 48, 1, '1', 'Miura', '', '1,5 ton/jam', 'Biomassa', '5'),
(78, 48, 2, '', '', '', '', '', ''),
(79, 48, 3, '', '', '', '', '', ''),
(80, 48, 4, '', '', '', '', '', ''),
(85, 51, 1, '1', 'Alstom', '-', '6,7', 'Batubara', '8-11 ton/hari'),
(86, 51, 2, '1', 'Alstom', '-', '10', 'Batubara', '8-11 ton/hari'),
(87, 51, 3, '', '', '', '', '', ''),
(88, 51, 4, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_item_teguran_bap`
--

CREATE TABLE `history_item_teguran_bap` (
  `id_history_teguran_bap` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `nilai_parameter_bap` varchar(255) NOT NULL,
  `nilai_parameter` varchar(255) NOT NULL,
  `ket_parameter_bap` varchar(255) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL,
  `id_parent_1` varchar(50) NOT NULL,
  `id_parent_2` varchar(50) DEFAULT NULL,
  `perbaikan` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_item_teguran_bap`
--

INSERT INTO `history_item_teguran_bap` (`id_history_teguran_bap`, `id_bap`, `nilai_parameter_bap`, `nilai_parameter`, `ket_parameter_bap`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `id_parent_1`, `id_parent_2`, `perbaikan`) VALUES
(229, 25, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-17', NULL, 'Administrator', NULL, '26', '3', 0),
(230, 25, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Accu Bekas', '2016-12-17', NULL, 'Administrator', NULL, '30', '4', 0),
(231, 25, 'Ada', 'Tidak Ada', 'Papan nama dan koordinat TPS 1', '2016-12-17', NULL, 'Administrator', NULL, '41', '4', 0),
(232, 25, 'Ada', 'Rutin', 'Pelaporan Neraca dan Manifest', '2016-12-17', NULL, 'Administrator', NULL, '4', 'top', 0),
(233, 25, ' - ', 'lain lain', 'Lain - Lain', '2016-12-17', NULL, 'Administrator', NULL, '388', 'top', 0),
(234, 29, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(235, 29, ' - ', 'rahasia lain', 'Lain - Lain', '2016-12-18', NULL, 'Administrator', NULL, '390', 'top', 0),
(242, 37, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '57', 'top', 0),
(243, 37, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '57', 'top', 0),
(244, 37, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(245, 37, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(246, 37, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(247, 37, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(248, 37, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-18', NULL, 'Administrator', NULL, '59', 'top', 0),
(249, 37, ' - ', 'asdasd', 'Lain - Lain', '2016-12-18', NULL, 'Administrator', NULL, '390', 'top', 0),
(250, 39, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '57', 'top', 0),
(251, 39, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '57', 'top', 0),
(252, 39, 'Ada', 'Tidak Ada', 'Sarana Pengolahan Air Limbah', '2016-12-18', NULL, 'Administrator', NULL, '58', 'top', 0),
(253, 39, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Upwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(254, 39, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Upwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(255, 39, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(256, 39, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(257, 39, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(258, 39, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(259, 39, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(260, 39, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Downwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(261, 39, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-18', NULL, 'Administrator', NULL, '243', '242', 0),
(262, 39, ' - ', 'asdads', 'Lain - Lain', '2016-12-18', NULL, 'Administrator', NULL, '390', 'top', 0),
(353, 45, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '142', 'top', 0),
(354, 45, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '142', 'top', 0),
(355, 45, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-18', NULL, 'Administrator', NULL, '143', 'top', 0),
(356, 45, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-18', NULL, 'Administrator', NULL, '143', 'top', 0),
(357, 45, 'Berfungsi', 'Tidak Berfungsi', 'Kondisi (apabila ada)', '2016-12-18', NULL, 'Administrator', NULL, '150', '143', 0),
(358, 45, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 1', '2016-12-18', NULL, 'Administrator', NULL, '158', '144', 0),
(359, 45, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 1', '2016-12-18', NULL, 'Administrator', NULL, '158', '144', 0),
(360, 45, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-18', NULL, 'Administrator', NULL, '167', '144', 0),
(361, 45, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan B3 kadaluarsa', '2016-12-18', NULL, 'Administrator', NULL, '171', '145', 0),
(362, 45, 'Ada', 'Tidak Ada', 'Saluran ceceran air limbah TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '182', '145', 0),
(363, 45, 'Ada', 'Tidak Ada', 'Bak penampung ceceran air limbah TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '182', '145', 0),
(364, 45, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '182', '145', 0),
(365, 45, 'Ada', 'Tidak Ada', 'APAR TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '182', '145', 0),
(366, 45, 'Rutin', 'Tidak Rutin', 'Pelaporan Neraca dan Manifest', '2016-12-18', NULL, 'Administrator', NULL, '145', 'top', 0),
(367, 45, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-18', NULL, 'Administrator', NULL, '196', '145', 0),
(368, 45, ' - ', 'rahasia pertama', 'Lain - Lain', '2016-12-18', NULL, 'Administrator', NULL, '394', 'top', 0),
(384, 47, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '257', 'top', 0),
(385, 47, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-18', NULL, 'Administrator', NULL, '257', 'top', 0),
(386, 47, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-18', NULL, 'Administrator', NULL, '258', 'top', 0),
(387, 47, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-18', NULL, 'Administrator', NULL, '258', 'top', 0),
(388, 47, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-18', NULL, 'Administrator', NULL, '258', 'top', 0),
(389, 47, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-18', NULL, 'Administrator', NULL, '265', '258', 0),
(390, 47, 'Ada', 'Tidak Ada', 'Catatan debit harian', '2016-12-18', NULL, 'Administrator', NULL, '258', 'top', 0),
(391, 47, 'Setiap Bulan', 'setiap 3 bulan', 'Periode Pengujian', '2016-12-18', NULL, 'Administrator', NULL, '268', '258', 0),
(392, 47, 'Ada', 'Tidak Ada', 'Tangga cerobong 1', '2016-12-18', NULL, 'Administrator', NULL, '273', '259', 0),
(393, 47, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 1', '2016-12-18', NULL, 'Administrator', NULL, '273', '259', 0),
(394, 47, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 1', '2016-12-18', NULL, 'Administrator', NULL, '259', 'top', 0),
(395, 47, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 1', '2016-12-18', NULL, 'Administrator', NULL, '259', 'top', 0),
(396, 47, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Downwind', '2016-12-18', NULL, 'Administrator', NULL, '282', '259', 0),
(397, 47, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-18', NULL, 'Administrator', NULL, '282', '259', 0),
(398, 47, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Accu Bekas', '2016-12-18', NULL, 'Administrator', NULL, '287', '260', 0),
(399, 47, 'Ada', 'Tidak Ada', 'Saluran ceceran air limbah TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '298', '260', 0),
(400, 47, 'Ada', 'Tidak Ada', 'Bak penampung ceceran air limbah TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '298', '260', 0),
(401, 47, 'Ada', 'Tidak Ada', 'Kemiringan TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '298', '260', 0),
(402, 47, 'Ada', 'Tidak Ada', 'SOP pengelolaan dan tanggap darurat TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '298', '260', 0),
(403, 47, 'Ada', 'Tidak Ada', 'Kotak P3K TPS 1', '2016-12-18', NULL, 'Administrator', NULL, '298', '260', 0),
(404, 47, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-18', NULL, 'Administrator', NULL, '312', '260', 0),
(405, 47, ' - ', 'sdfsdfsdf', 'Lain - Lain', '2016-12-18', NULL, 'Administrator', NULL, '398', 'top', 0),
(442, 53, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-20', NULL, 'Administrator', NULL, '87', 'top', 0),
(443, 53, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Upwind', '2016-12-20', NULL, 'Administrator', NULL, '94', '89', 0),
(444, 53, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2016-12-20', NULL, 'Administrator', NULL, '94', '89', 0),
(445, 53, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-20', NULL, 'Administrator', NULL, '94', '89', 0),
(446, 53, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Downwind', '2016-12-20', NULL, 'Administrator', NULL, '94', '89', 0),
(447, 53, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-20', NULL, 'Administrator', NULL, '94', '89', 0),
(448, 53, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-20', NULL, 'Administrator', NULL, '90', 'top', 0),
(449, 53, ' - ', 'rahasia terakhir', 'Lain - Lain', '2016-12-20', NULL, 'Administrator', NULL, '408', 'top', 0),
(464, 56, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-21', NULL, 'Administrator', NULL, '77', 'top', 0),
(465, 56, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-21', NULL, 'Administrator', NULL, '77', 'top', 0),
(466, 56, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Upwind', '2016-12-21', NULL, 'Administrator', NULL, '253', '252', 0),
(467, 56, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2016-12-21', NULL, 'Administrator', NULL, '253', '252', 0),
(468, 56, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-21', NULL, 'Administrator', NULL, '253', '252', 0),
(469, 56, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-21', NULL, 'Administrator', NULL, '253', '252', 0),
(470, 56, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-21', NULL, 'Administrator', NULL, '79', 'top', 0),
(471, 58, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-21', NULL, 'Administrator', NULL, '98', 'top', 0),
(472, 58, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-21', NULL, 'Administrator', NULL, '98', 'top', 0),
(473, 58, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-21', NULL, 'Administrator', NULL, '99', 'top', 0),
(474, 58, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-21', NULL, 'Administrator', NULL, '99', 'top', 0),
(475, 58, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-21', NULL, 'Administrator', NULL, '99', 'top', 0),
(476, 58, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-21', NULL, 'Administrator', NULL, '106', '99', 0),
(477, 58, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Air Limbah', '2016-12-21', NULL, 'Administrator', NULL, '99', 'top', 0),
(478, 58, 'Setiap Bulan', '-', 'Periode Pengujian', '2016-12-21', NULL, 'Administrator', NULL, '109', '99', 0),
(479, 58, 'Memenuhi', '-', 'Hasil Pengujian', '2016-12-21', NULL, 'Administrator', NULL, '109', '99', 0),
(480, 58, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-21', NULL, 'Administrator', NULL, '114', '100', 0),
(481, 58, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan B3 kadaluarsa', '2016-12-21', NULL, 'Administrator', NULL, '101', 'top', 0),
(482, 58, 'Ada', 'Tidak Ada', 'SOP pengelolaan dan tanggap darurat TPS 1', '2016-12-21', NULL, 'Administrator', NULL, '123', '101', 0),
(483, 58, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-21', NULL, 'Administrator', NULL, '123', '101', 0),
(484, 58, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-21', NULL, 'Administrator', NULL, '137', '101', 0),
(485, 58, ' - ', 'sasddsa', 'Lain - Lain', '2016-12-21', NULL, 'Administrator', NULL, '392', 'top', 0),
(514, 60, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-22', NULL, 'Administrator', NULL, '319', 'top', 0),
(515, 60, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-22', NULL, 'Administrator', NULL, '320', 'top', 0),
(516, 60, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-22', NULL, 'Administrator', NULL, '319', 'top', 0),
(517, 60, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-22', NULL, 'Administrator', NULL, '326', '319', 0),
(518, 60, 'Ada', 'Tidak Ada', 'Catatan debit harian', '2016-12-22', NULL, 'Administrator', NULL, '319', 'top', 0),
(519, 60, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Air Limbah', '2016-12-22', NULL, 'Administrator', NULL, '319', 'top', 0),
(520, 60, 'Setiap Bulan', '-', 'Periode Pengujian', '2016-12-22', NULL, 'Administrator', NULL, '329', '319', 0),
(521, 60, 'Memenuhi', '-', 'Hasil Pengujian', '2016-12-22', NULL, 'Administrator', NULL, '329', '319', 0),
(522, 60, 'Rutin', 'Tidak Rutin', 'Pelaporan', '2016-12-22', NULL, 'Administrator', NULL, '329', '319', 0),
(523, 60, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Upwind', '2016-12-22', NULL, 'Administrator', NULL, '334', '320', 0),
(524, 60, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2016-12-22', NULL, 'Administrator', NULL, '334', '320', 0),
(525, 60, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-22', NULL, 'Administrator', NULL, '334', '320', 0),
(526, 60, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-22', NULL, 'Administrator', NULL, '334', '320', 0),
(527, 60, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-22', NULL, 'Administrator', NULL, '334', '320', 0),
(528, 60, 'Pada TPS Limbah B3', 'Di luar TPS Limbah B3', 'Penyimpanan', '2016-12-22', NULL, 'Administrator', NULL, '321', 'top', 0),
(529, 60, ' - ', 'lain pertama pertama', 'Lain - Lain', '2016-12-22', NULL, 'Administrator', NULL, '396', 'top', 0),
(536, 62, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-22', NULL, 'Administrator', NULL, '363', '358', 0),
(537, 62, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2016-12-22', NULL, 'Administrator', NULL, '363', '358', 0),
(538, 62, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-22', NULL, 'Administrator', NULL, '363', '358', 0),
(539, 62, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Accu Bekas', '2016-12-22', NULL, 'Administrator', NULL, '359', 'top', 0),
(540, 62, 'Pada TPS Limbah B3', 'Di luar TPS Limbah B3', 'Penyimpanan', '2016-12-22', NULL, 'Administrator', NULL, '359', 'top', 0),
(541, 62, 'Ada', 'Tidak Ada', 'Neraca Limbah B3', '2016-12-22', NULL, 'Administrator', NULL, '359', 'top', 0),
(542, 62, 'Ada', 'Tidak Ada', 'Manifest', '2016-12-22', NULL, 'Administrator', NULL, '359', 'top', 0),
(543, 62, 'Rutin', 'Tidak Rutin', 'Pelaporan Neraca dan Manifest', '2016-12-22', NULL, 'Administrator', NULL, '359', 'top', 0),
(544, 62, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-22', NULL, 'Administrator', NULL, '383', '359', 0),
(545, 62, ' - ', 'asdasd', 'Lain - Lain', '2016-12-22', NULL, 'Administrator', NULL, '400', 'top', 0),
(562, 64, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-22', NULL, 'Administrator', NULL, '198', 'top', 0),
(563, 64, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-22', NULL, 'Administrator', NULL, '199', 'top', 0),
(564, 64, 'Berfungsi', 'Tidak Berfungsi', 'Kondisi (apabila ada)', '2016-12-22', NULL, 'Administrator', NULL, '206', '199', 0),
(565, 64, 'Setiap Bulan', '3 bulan sekali', 'Periode Pengujian', '2016-12-22', NULL, 'Administrator', NULL, '209', '199', 0),
(566, 64, 'Rutin', 'Tidak Rutin', 'Pelaporan', '2016-12-22', NULL, 'Administrator', NULL, '209', '199', 0),
(567, 64, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Upwind', '2016-12-22', NULL, 'Administrator', NULL, '214', '200', 0),
(568, 64, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-22', NULL, 'Administrator', NULL, '214', '200', 0),
(569, 64, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-22', NULL, 'Administrator', NULL, '214', '200', 0),
(570, 64, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-22', NULL, 'Administrator', NULL, '214', '200', 0),
(571, 64, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Accu Bekas', '2016-12-22', NULL, 'Administrator', NULL, '201', 'top', 0),
(572, 64, 'Ada', 'Tidak Ada', 'Kemiringan TPS 1', '2016-12-22', NULL, 'Administrator', NULL, '226', '201', 0),
(573, 64, 'Ada', 'Tidak Ada', 'SOP pengelolaan dan tanggap darurat TPS 1', '2016-12-22', NULL, 'Administrator', NULL, '226', '201', 0),
(574, 64, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-22', NULL, 'Administrator', NULL, '226', '201', 0),
(575, 64, 'Ada', 'Tidak Ada', 'Neraca Limbah B3', '2016-12-22', NULL, 'Administrator', NULL, '201', 'top', 0),
(576, 64, 'Ada', 'Tidak Ada', 'Manifest', '2016-12-22', NULL, 'Administrator', NULL, '201', 'top', 0),
(577, 64, ' - ', 'lain pertama partial', 'Lain - Lain', '2016-12-22', NULL, 'Administrator', NULL, '406', 'top', 0),
(616, 33, 'Ada', 'Tidak Ada', 'Sarana Pengolahan Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '58', 'top', 0),
(617, 33, ' - ', 'rahasia pertama', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '390', 'top', 0),
(618, 57, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-27', NULL, 'Administrator', NULL, '99', 'top', 0),
(619, 57, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-27', NULL, 'Administrator', NULL, '99', 'top', 0),
(620, 57, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-27', NULL, 'Administrator', NULL, '99', 'top', 0),
(621, 57, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-27', NULL, 'Administrator', NULL, '106', '99', 0),
(622, 57, 'Ada', 'Tidak Ada', 'Catatan debit harian', '2016-12-27', NULL, 'Administrator', NULL, '99', 'top', 0),
(623, 57, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '99', 'top', 0),
(624, 57, 'Setiap Bulan', '-', 'Periode Pengujian', '2016-12-27', NULL, 'Administrator', NULL, '109', '99', 0),
(625, 57, 'Memenuhi', '-', 'Hasil Pengujian', '2016-12-27', NULL, 'Administrator', NULL, '109', '99', 0),
(626, 57, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-27', NULL, 'Administrator', NULL, '114', '100', 0),
(627, 57, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Accu Bekas', '2016-12-27', NULL, 'Administrator', NULL, '101', 'top', 0),
(628, 57, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-27', NULL, 'Administrator', NULL, '137', '101', 0),
(629, 57, ' - ', 'rahasia kesalahan', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '392', 'top', 0),
(630, 40, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '142', 'top', 0),
(631, 40, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '142', 'top', 0),
(632, 40, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-27', NULL, 'Administrator', NULL, '143', 'top', 0),
(633, 40, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-27', NULL, 'Administrator', NULL, '143', 'top', 0),
(634, 40, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-27', NULL, 'Administrator', NULL, '143', 'top', 0),
(635, 40, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-27', NULL, 'Administrator', NULL, '150', '143', 0),
(636, 40, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '143', 'top', 0),
(637, 40, 'Setiap Bulan', '-', 'Periode Pengujian', '2016-12-27', NULL, 'Administrator', NULL, '153', '143', 0),
(638, 40, 'Memenuhi', '-', 'Hasil Pengujian', '2016-12-27', NULL, 'Administrator', NULL, '153', '143', 0),
(639, 40, 'Ada', 'Tidak Ada', 'Tangga cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '158', '144', 0),
(640, 40, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '144', 'top', 0),
(641, 40, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(642, 40, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '163', '144', 0),
(643, 40, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '144', 'top', 0),
(644, 40, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Upwind', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(645, 40, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Upwind', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(646, 40, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(647, 40, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(648, 40, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(649, 40, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(650, 40, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(651, 40, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Downwind', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(652, 40, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-27', NULL, 'Administrator', NULL, '167', '144', 0),
(653, 40, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Limbah B3 cair', '2016-12-27', NULL, 'Administrator', NULL, '171', '145', 0),
(654, 40, 'Ada', 'Tidak Ada', 'Bak penampung ceceran air limbah TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '182', '145', 0),
(655, 40, 'Ada', 'Tidak Ada', 'Kemiringan TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '182', '145', 0),
(656, 40, 'Ada', 'Tidak Ada', 'SOP pengelolaan dan tanggap darurat TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '182', '145', 0),
(657, 40, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '182', '145', 0),
(658, 40, 'Ada', 'Tidak Ada', 'APAR TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '182', '145', 0),
(659, 40, ' - ', 'asdasdasdad', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '394', 'top', 0),
(660, 59, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '318', 'top', 1),
(661, 59, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '318', 'top', 1),
(662, 59, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-27', NULL, 'Administrator', NULL, '319', 'top', 1),
(663, 59, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-27', NULL, 'Administrator', NULL, '320', 'top', 1),
(664, 59, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-27', NULL, 'Administrator', NULL, '319', 'top', 1),
(665, 59, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-27', NULL, 'Administrator', NULL, '326', '319', 1),
(666, 59, 'Ada', 'Tidak Ada', 'Catatan debit harian', '2016-12-27', NULL, 'Administrator', NULL, '319', 'top', 1),
(667, 59, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '319', 'top', 1),
(668, 59, 'Setiap Bulan', '-', 'Periode Pengujian', '2016-12-27', NULL, 'Administrator', NULL, '329', '319', 1),
(669, 59, 'Memenuhi', '-', 'Hasil Pengujian', '2016-12-27', NULL, 'Administrator', NULL, '329', '319', 1),
(670, 59, 'Tidak Ada', 'Ada', 'Kebocoran/Rembesan/Tumpahan/Bypass', '2016-12-27', NULL, 'Administrator', NULL, '319', 'top', 1),
(671, 59, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-27', NULL, 'Administrator', NULL, '334', '320', 1),
(672, 59, 'Ada', 'Tidak Ada', 'Kemiringan TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '340', '322', 1),
(673, 59, 'Ada', 'Tidak Ada', 'SOP pengelolaan dan tanggap darurat TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '340', '322', 1),
(674, 59, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '340', '322', 1),
(675, 59, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-27', NULL, 'Administrator', NULL, '354', '321', 1),
(676, 46, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '257', 'top', 0),
(677, 46, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '257', 'top', 0),
(678, 46, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-27', NULL, 'Administrator', NULL, '258', 'top', 0),
(679, 46, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-27', NULL, 'Administrator', NULL, '258', 'top', 0),
(680, 46, 'Ada', 'Tidak Ada', 'Catatan debit harian', '2016-12-27', NULL, 'Administrator', NULL, '258', 'top', 0),
(681, 46, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '273', '259', 0),
(682, 46, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '273', '259', 0),
(683, 46, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 1', '2016-12-27', NULL, 'Administrator', NULL, '259', 'top', 0),
(684, 46, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Upwind', '2016-12-27', NULL, 'Administrator', NULL, '282', '259', 0),
(685, 46, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2016-12-27', NULL, 'Administrator', NULL, '282', '259', 0),
(686, 46, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '298', '260', 0),
(687, 46, 'Ada', 'Tidak Ada', 'APAR TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '298', '260', 0),
(688, 46, 'Ada', 'Tidak Ada', 'Kotak P3K TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '298', '260', 0),
(689, 46, 'Ada', 'Tidak Ada', 'Neraca Limbah B3', '2016-12-27', NULL, 'Administrator', NULL, '260', 'top', 0),
(690, 46, ' - ', 'rahasia pertama', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '398', 'top', 0),
(691, 61, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '356', 'top', 0),
(692, 61, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '356', 'top', 0),
(693, 61, 'Ada', 'Tidak Ada', 'Sarana Pengolahan Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '357', 'top', 0),
(694, 61, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '369', '359', 0),
(695, 61, 'Rutin', 'Tidak Rutin', 'Pelaporan Neraca dan Manifest', '2016-12-27', NULL, 'Administrator', NULL, '359', 'top', 0),
(696, 61, 'Dibakar', 'Dibakar', 'Pengelolaan', '2016-12-27', NULL, 'Administrator', NULL, '383', '359', 0),
(705, 51, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Upwind', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(706, 51, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Upwind', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(707, 51, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(708, 51, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(709, 51, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(710, 51, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(711, 51, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Downwind', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(712, 51, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Downwind', '2016-12-27', NULL, 'Administrator', NULL, '248', '247', 0),
(713, 54, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '77', 'top', 0),
(714, 54, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '77', 'top', 0),
(715, 54, 'Ada', 'Tidak Ada', 'Sarana Pengolahan Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '78', 'top', 0),
(716, 54, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2016-12-27', NULL, 'Administrator', NULL, '253', '252', 0),
(717, 54, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2016-12-27', NULL, 'Administrator', NULL, '253', '252', 0),
(718, 54, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Downwind', '2016-12-27', NULL, 'Administrator', NULL, '253', '252', 0),
(719, 54, ' - ', 'lain lain terakhir', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '404', 'top', 0),
(720, 63, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '198', 'top', 0),
(721, 63, 'Ada', 'Tidak Ada', 'IPAL', '2016-12-27', NULL, 'Administrator', NULL, '199', 'top', 0),
(722, 63, 'Ada', 'Tidak Ada', 'Perizinan', '2016-12-27', NULL, 'Administrator', NULL, '199', 'top', 0),
(723, 63, 'Ada', 'Tidak Ada', 'Alat Ukur', '2016-12-27', NULL, 'Administrator', NULL, '199', 'top', 0),
(724, 63, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2016-12-27', NULL, 'Administrator', NULL, '206', '199', 0),
(725, 63, 'Dikelola', 'Tidak Dikelola', 'Pengelolaan Accu Bekas', '2016-12-27', NULL, 'Administrator', NULL, '201', 'top', 0),
(726, 63, 'Ada', 'Tidak Ada', 'Kemiringan TPS 1', '2016-12-27', NULL, 'Administrator', NULL, '226', '201', 0),
(727, 63, ' - ', 'lain pertama', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '406', 'top', 0),
(728, 52, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '87', 'top', 0),
(729, 52, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2016-12-27', NULL, 'Administrator', NULL, '87', 'top', 0),
(730, 52, 'Ada', 'Tidak Ada', 'Sarana Pengolahan Air Limbah', '2016-12-27', NULL, 'Administrator', NULL, '88', 'top', 0),
(731, 52, ' - ', 'sdsdf', 'Lain - Lain', '2016-12-27', NULL, 'Administrator', NULL, '408', 'top', 0),
(829, 21, 'Ada', 'Tidak Ada', 'Dokumen Lingkungan', '2017-01-17', NULL, 'Administrator', NULL, '1', 'top', 0),
(830, 21, 'Ada', 'Tidak Ada', 'Izin Lingkungan', '2017-01-17', NULL, 'Administrator', NULL, '1', 'top', 0),
(831, 21, 'Tidak Ada', 'Ada', 'Kebocoran/Rembesan/Tumpahan/Bypass', '2017-01-17', NULL, 'Administrator', NULL, '2', '1', 0),
(832, 21, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(833, 21, 'Ada', 'Tidak Ada', 'Tangga cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(834, 21, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(835, 21, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(836, 21, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(837, 21, 'Ada', 'Tidak Ada', 'Tangga cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(838, 21, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(839, 21, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(840, 21, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(841, 21, 'Ada', 'Tidak Ada', 'Tangga cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(842, 21, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(843, 21, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(844, 21, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(845, 21, 'Ada', 'Tidak Ada', 'Tangga cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(846, 21, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(847, 21, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '17', '3', 0),
(848, 21, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(849, 21, 'Rutin', '', 'Periode pengujian (per 6 bulan) cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(850, 21, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '22', '3', 0),
(851, 21, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 1', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(852, 21, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(853, 21, 'Rutin', '', 'Periode pengujian (per 6 bulan) cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(854, 21, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '22', '3', 0),
(855, 21, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 2', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(856, 21, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(857, 21, 'Rutin', '', 'Periode pengujian (per 6 bulan) cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(858, 21, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '22', '3', 0),
(859, 21, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 3', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(860, 21, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(861, 21, 'Rutin', '', 'Periode pengujian (per 6 bulan) cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(862, 21, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '22', '3', 0),
(863, 21, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 4', '2017-01-17', NULL, 'Administrator', NULL, '3', 'top', 0),
(864, 21, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(865, 21, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(866, 21, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2017-01-17', NULL, 'Administrator', NULL, '26', '3', 0),
(867, 21, 'Dikelola', '', 'Pengelolaan ', '2017-01-17', NULL, 'Administrator', NULL, '30', '4', 0),
(868, 21, 'Ada', 'Rutin', 'Pelaporan Neraca dan Manifest', '2017-01-17', NULL, 'Administrator', NULL, '4', 'top', 0),
(888, 3, 'Ada', 'Tidak Ada', 'Perizinan', '2018-10-22', NULL, 'Administrator', NULL, '2', '1', 0),
(889, 3, 'Ada', 'Tidak Ada', 'Alat Ukur', '2018-10-22', NULL, 'Administrator', NULL, '2', '1', 0),
(890, 3, 'Berfungsi', '-', 'Kondisi (apabila ada)', '2018-10-22', NULL, 'Administrator', NULL, '9', '2', 0),
(891, 3, 'Memenuhi', 'Tidak Memenuhi Baku Mutu', 'Hasil Pengujian', '2018-10-22', NULL, 'Administrator', NULL, '13', '12', 0),
(892, 3, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 1', '2018-10-22', NULL, 'Administrator', NULL, '17', '3', 0),
(893, 3, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '17', '3', 0),
(894, 3, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '17', '3', 0),
(895, 3, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) cerobong 1', '2018-10-22', NULL, 'Administrator', NULL, '26', '3', 0),
(896, 3, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '3', 'top', 0),
(897, 3, 'Ada', 'Tidak Ada', 'Pengujian Kualitas Site', '2018-10-22', NULL, 'Administrator', NULL, '26', '3', 0),
(898, 3, 'Rutin', 'Tidak Rutin', 'Periode pengujian (per 6 bulan) Site', '2018-10-22', NULL, 'Administrator', NULL, '26', '3', 0),
(899, 3, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2018-10-22', NULL, 'Administrator', NULL, '26', '3', 0),
(900, 3, 'Ada', 'Tidak Ada', 'Bak penampung ceceran air limbah TPS 1', '2018-10-22', NULL, 'Administrator', NULL, '41', '4', 0),
(901, 3, 'Ada', 'Tidak Ada', 'APAR TPS 1', '2018-10-22', NULL, 'Administrator', NULL, '41', '4', 0),
(902, 3, 'Ada', 'Tidak Ada', 'Kotak P3K TPS 1', '2018-10-22', NULL, 'Administrator', NULL, '41', '4', 0),
(903, 3, 'Ada', 'Tidak Ada', 'Kemiringan TPS 2', '2018-10-22', NULL, 'Administrator', NULL, '41', '4', 0),
(904, 3, 'Ada', 'Tidak Ada', 'Kotak P3K TPS 2', '2018-10-22', NULL, 'Administrator', NULL, '41', '4', 0),
(905, 3, 'Ada', 'Tidak Melaporkan', 'Pelaporan Neraca dan Manifest', '2018-10-22', NULL, 'Administrator', NULL, '4', 'top', 0),
(906, 3, ' - ', 'rahasia nih yaaa, rahasia!', 'Lain - Lain', '2018-10-22', NULL, 'Administrator', NULL, '388', 'top', 0),
(922, 85, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 1', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(923, 85, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(924, 85, 'Ada', 'Tidak Ada', 'Tangga cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(925, 85, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(926, 85, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(927, 85, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '144', 'top', 0),
(928, 85, 'Rutin', '', 'Periode pengujian (per 6 bulan) cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '167', '144', 0),
(929, 85, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '163', '144', 0),
(930, 85, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '144', 'top', 0),
(931, 85, 'Ya', 'Tidak', 'Pemenuhan BM Upwind', '2018-10-22', NULL, 'Administrator', NULL, '167', '144', 0),
(932, 85, 'Ya', 'Tidak', 'Pemenuhan BM Site', '2018-10-22', NULL, 'Administrator', NULL, '167', '144', 0),
(933, 85, 'Ya', 'Tidak', 'Pemenuhan BM Downwind', '2018-10-22', NULL, 'Administrator', NULL, '167', '144', 0),
(934, 85, 'Ada', 'Tidak Ada', 'Neraca Limbah B3', '2018-10-22', NULL, 'Administrator', NULL, '145', 'top', 0),
(935, 85, 'Ada', 'Tidak Ada', 'Manifest', '2018-10-22', NULL, 'Administrator', NULL, '145', 'top', 0),
(936, 91, 'Memenuhi', 'Tidak Memenuhi Baku Mutu', 'Hasil Pengujian', '2018-10-22', NULL, 'Administrator', NULL, '153', '143', 0),
(937, 91, 'Ada', 'Tidak Ada', 'Sampling hole cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(938, 91, 'Ada', 'Tidak Ada', 'Tangga cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(939, 91, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(940, 91, 'Ada', 'Tidak Ada', 'Lantai kerja cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '158', '144', 0),
(941, 91, 'Ada', 'Tidak Ada', 'Pengujian kualitas emisi cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '144', 'top', 0),
(942, 91, 'Rutin', '', 'Periode pengujian (per 6 bulan) cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '167', '144', 0),
(943, 91, 'Ya', 'Tidak', 'Pemenuhan BME cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '163', '144', 0),
(944, 91, 'Ada', 'Tidak Ada', 'Alat pengendali emisi cerobong 2', '2018-10-22', NULL, 'Administrator', NULL, '144', 'top', 0),
(945, 91, 'Ada', 'Tidak Ada', 'Neraca Limbah B3', '2018-10-22', NULL, 'Administrator', NULL, '145', 'top', 0),
(946, 91, 'Ada', 'Tidak Ada', 'Manifest', '2018-10-22', NULL, 'Administrator', NULL, '145', 'top', 0),
(981, 96, 'Ada', 'Tidak Ada', 'Pagar pengaman tangga cerobong 1', '2018-11-05', NULL, 'Administrator', NULL, '158', '144', 0),
(982, 96, 'Ada', 'Tidak Ada', 'Perizinan TPS 1', '2018-11-05', NULL, 'Administrator', NULL, '182', '145', 0),
(983, 96, 'Ada', 'Tidak Ada', 'Papan nama dan koordinat TPS 1', '2018-11-05', NULL, 'Administrator', NULL, '182', '145', 0),
(1005, 93, 'Ada', 'Tidak Ada', 'Perizinan', '2018-11-06', NULL, 'Administrator', NULL, '143', 'top', 0),
(1006, 93, 'Berfungsi', 'Tidak Berfungsi', 'Kondisi (apabila ada)', '2018-11-06', NULL, 'Administrator', NULL, '150', '143', 0),
(1007, 93, 'Ada', 'Tidak Ada', 'Papan nama dan koordinat TPS 1', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1008, 93, 'Ada', 'Tidak Ada', 'Simbol dan label TPS 1', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1009, 93, 'Ada', 'Tidak Ada', 'Saluran ceceran air limbah TPS 1', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1010, 93, 'Ada', 'Tidak Ada', 'Bak penampung ceceran air limbah TPS 1', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1011, 93, 'Ada', 'Tidak Ada', 'Kemiringan TPS 1', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1012, 93, 'Ada', 'Tidak Ada', 'Log book TPS 1', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1013, 93, 'Ada', 'Tidak Ada', 'Papan nama dan koordinat TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1014, 93, 'Ada', 'Tidak Ada', 'Simbol dan label TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1015, 93, 'Ada', 'Tidak Ada', 'Saluran ceceran air limbah TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1016, 93, 'Ada', 'Tidak Ada', 'Bak penampung ceceran air limbah TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1017, 93, 'Ada', 'Tidak Ada', 'Kemiringan TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1018, 93, 'Ada', 'Tidak Ada', 'SOP pengelolaan dan tanggap darurat TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1019, 93, 'Ada', 'Tidak Ada', 'Log book TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1020, 93, 'Ada', 'Tidak Ada', 'APAR TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0),
(1021, 93, 'Ada', 'Tidak Ada', 'Kotak P3K TPS 2', '2018-11-06', NULL, 'Administrator', NULL, '182', '145', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_item_teguran_lhu`
--

CREATE TABLE `history_item_teguran_lhu` (
  `id_laporan_hasil_uji` int(11) NOT NULL,
  `parameter_lhu` varchar(255) NOT NULL,
  `dasar_hukum` text NOT NULL,
  `nilai_hasil_uji` varchar(10) NOT NULL,
  `nilai_baku_mutu` varchar(10) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `history_item_teguran_lhu`
--

INSERT INTO `history_item_teguran_lhu` (`id_laporan_hasil_uji`, `parameter_lhu`, `dasar_hukum`, `nilai_hasil_uji`, `nilai_baku_mutu`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(2, 'Opasitas', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '600', '20', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(4, 'Partikulat', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '500', '230', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(4, 'Opasitas', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '6000', '20', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history_teguran_periode_adm`
--

CREATE TABLE `history_teguran_periode_adm` (
  `id_history_teguran_periode_adm` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `ket_jenis_dokumen` varchar(255) NOT NULL,
  `periode_bulan` int(11) NOT NULL,
  `toleransi_hari` int(11) NOT NULL,
  `jml_dokumen` int(11) NOT NULL,
  `tgl_teguran` date NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `industri`
--

CREATE TABLE `industri` (
  `id_industri` int(11) NOT NULL,
  `id_jenis_industri` int(11) NOT NULL,
  `id_usaha_kegiatan` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `badan_hukum` varchar(255) NOT NULL,
  `nama_industri` varchar(255) NOT NULL,
  `tlp` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL,
  `pj_industri` varchar(255) DEFAULT NULL,
  `jml_pegawai` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `hari_kerja_bulan` varchar(100) DEFAULT NULL,
  `jam_kerja_hari` varchar(100) DEFAULT NULL,
  `luas_area` float DEFAULT NULL,
  `luas_bangunan` float DEFAULT NULL,
  `pimpinan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `industri`
--

INSERT INTO `industri` (`id_industri`, `id_jenis_industri`, `id_usaha_kegiatan`, `id_kecamatan`, `id_kelurahan`, `badan_hukum`, `nama_industri`, `tlp`, `email`, `alamat`, `fax`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `pj_industri`, `jml_pegawai`, `status`, `hari_kerja_bulan`, `jam_kerja_hari`, `luas_area`, `luas_bangunan`, `pimpinan`) VALUES
(1, 60, 11, 14, 192, '', 'Agro Makmur Senjaya', '081234532153', 'ams@gmail.com', 'rahasia', '06621134', '2016-10-28', '2016-10-28', 'Administrator', 'Administrator', NULL, NULL, NULL, '12', '12', 12, 12, 'Pak Bro'),
(2, 61, 22, 14, 195, '', 'Farma Farmasi', '1234', 'farma@mailinator.com', 'rahasia', '4321', '2016-12-10', '2016-12-10', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak pakan'),
(3, 60, 11, 14, 193, '', 'Argo yang lain', '1234', 'argo@mail.com', 'rahasia', '44321', '2016-12-10', '2016-12-10', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak pakin'),
(4, 61, 22, 16, 171, '', 'Kimia Farmasi', '1234', 'farmasi@gmail.com', 'terlampir', '4433', '2016-12-10', '2016-12-10', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak farma'),
(5, 60, 11, 16, 173, '', 'Argo Ciwi', '1234', 'ciwar@yahoo.com', 'terlampir', '4411', '2016-12-10', '2016-12-10', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak ciwar'),
(6, 27, 12, 5, 253, '', 'Golf sejati', '0192837', 'golf@lapang.com', 'terlampar', '12321', '2016-12-17', '2016-12-17', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak gol'),
(7, 5, 14, 15, 179, '', 'Elektron', '123', 'mega@tron.com', 'rahasia', '3321', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 333, 4, 'Megatron'),
(8, 23, 16, 17, 165, '', 'Pengelola Limbah', '102910', 'pengelol@limb.ah', 'rahasia', '2909209', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak pengel'),
(9, 21, 18, 16, 172, '', 'Rumah Potong Hewan', '5555', 'rum@h.jag', 'rahasia', '6666', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak jagal'),
(10, 20, 19, 10, 214, '', 'Restoran Rindu Bumi', '55555', 'punc@k.com', 'terlampir', '66666666', '2016-12-18', '2016-12-18', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak pun'),
(11, 70, 21, 15, 186, '', 'Perusahaan SPPBE', '123123', 'spp@be.com', 'rahasia', '123123', '2016-12-19', '2016-12-19', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak be'),
(12, 10, 13, 14, 193, '', 'Hotel Surya Indah', '5555', 'surya@email.com', 'rahasia', '666', '2016-12-21', '2016-12-21', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak surya'),
(13, 24, 15, 14, 195, '', 'Laboratorium ITB', '5', 'l@b.or', 'rahasia', '6', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak labori'),
(14, 22, 17, 1, 1, '', 'Tambang Batu Bara', '5', 't@mb.an', 'terlampir', '6', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'Pak tambun'),
(15, 7, 20, 5, 253, '', 'Rumah Sakit Anak Bandung', '5', '77@com.com', 'rahasia', '88', '2016-12-22', '2016-12-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '2', 3, 4, 'pak hanta'),
(16, 60, 11, 1, 1, '', 'ABC', '111', 'agimuhamad.s@gmail.com', 'ABC', '111', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '1', '1', 1, 1, 'ABC'),
(17, 1, 14, 17, 167, '', 'PT. BADJATEX', '02252003033', 'leo@badjatex.com', 'jl. Citepus No. 5 Desa Pasawahan, Kecamatan Dayeuhkolot, Kabupaten Bandung', '0225225120', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '24', '24', 0, 0, 'LEO LIANDRA'),
(18, 1, 14, 17, 167, '', 'PT. FAMATEX', '0225205088', 'fmx@yahoo.com', 'JL. Raya Dayeuhkolot No. 48 Bandung', '0225205082', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '26', '16', 20.395, 0, 'Fahmi Elhaq'),
(19, 1, 14, 22, 121, '', 'PT IWAMA PRIMA TEXTILE MILLS', '0227271122', 'iwama.majalaya@yahoo.com', 'Jl. Randukurung No. 8 Desa Padaulun Kecamatan Majalaya Kab. Bandung', '0227202668', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '25', '', 26395, 0, 'Aritonang'),
(20, 4, 14, 10, 214, '', 'PT Yorkshire Indonesia', '0227949581', 'saemuri@yorkindo.com', 'Jalan Cicalengka Majalaya Km 4', '0227949585', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '25', '8', 38610, 6760, 'Saemuri'),
(21, 1, 14, 17, 167, '', 'PT. INTI GUNAWANTEX', '0225203877', 'intigunawantex@yahoo.com', 'Jl. Raya Citepus No. 89 Kelurahan Pasawahan Kecamatan Dayeuhkolot', '0225203880', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '6', '25', 16492, 7000, 'Tina Anggraeni'),
(22, 1, 14, 17, 167, '', 'CV. PERAJUTAN SAHABAT', '0225203311', 'cvperajutansahabat@yahoo.co.id', 'Jl. Mengger N0.8 dan 9, Mohamad Toha Km. 5.6 Kabupaten Bandung ', '0225202155', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL, NULL, NULL, '25', '8', 16.265, 0, 'JOHAN'),
(23, 1, 14, 19, 145, '', 'PT. CAVE SUMBER BERKAT', '0225897458', 'cavesumberberkat@gmail.com', 'cilampeni rt.02 rw 06 Desa Cilampeni Kecamatan Katapang ', '0225897457', '2018-11-01', '2018-11-01', 'Administrator', 'Administrator', NULL, NULL, NULL, '26', '8', 17.698, 0, 'Yudi Sutisna'),
(24, 1, 14, 22, 118, '', 'PT. NIRWANA ABADI SENTOSA', '0225951595', 'binner.habeahan@nirwanagroup.co.id', 'Jl. Manirancan No. 207 Desa Majasetra Kecamatan Majalaya Kab. Bandung', '0225950313', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '25', '8', 21.003, 0, 'IWAN SANTOSO'),
(25, 1, 14, 23, 109, '', 'PT. JERDITEX', '0226671351', 'jrdytex@gmail.com', 'JL. Mencut No. 28 Desa nLagadar Kecamatan Margaasih Kabupaten Bandung ', '022671354', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '24', '8', 15098, 0, 'Teddy Suryadinata'),
(26, 1, 14, 23, 270, '', 'PT. SELATAN JAYA INDUSTRI', '0226677369', 'pt.sijii@gmail.com', 'Jl.. Cikuya Tonggoh no. 77 Desa Lagadar kecamatan margaasih', '0226677369', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '26', '8', 2255, 3000, 'Kho lok jin'),
(27, 87, 14, 19, 148, '', 'PT. SINAR RUNNERINDO', '0225891445', 'sinarrunnerindo@gmail.com', 'JL. Terusan Kopo km. 12,8 Desa Pangauban Kecamatan Katapang Kabupaten Bandung', '0225891446', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '26', '16', 17270, 0, 'The Siauw Tjuhiu'),
(28, 90, 14, 19, 145, '', 'PT. TRISCO', '0225897185', 'trsico@gmail.com', 'JL. Raya Kopo-Soreang km. 11,5 Desa Cilampeni Kecamatan Katapang Kabupaten Bandung', '0225897186', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '24', '24', 21023, 0, 'David Choen'),
(29, 4, 14, 19, 145, '', 'PT. LAUTAN WARNA SARI ', '0225892556', 'nannagjuhana2014@gmail.com', 'Kp. Blok Muara Ciwidey Desa Cilampeni Kecamatan Katapang', '0225892554', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '24', '8', 10, 0, 'Nanang Juhana '),
(30, 4, 14, 27, 81, '', 'PT. PRODIAN CHEMICALS INDONESIA', '0225945628', 'brilianto@prodian-chemical.com', 'Jl. Raya Banjaran KM 14,7 No.626 Pameungpeuk Kab. Bandung', '0225945626', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator', NULL, NULL, NULL, '20', '8', 17.319, 0, 'Robert Moor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `izin_pembuangan_limbah`
--

CREATE TABLE `izin_pembuangan_limbah` (
  `id_pembuangan` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `no_izin` varchar(100) NOT NULL,
  `tgl_izin` date NOT NULL,
  `tentang` text NOT NULL,
  `debit_perhari` decimal(8,2) NOT NULL,
  `debit_perbulan` decimal(8,2) NOT NULL,
  `proses` text NOT NULL,
  `badan_air_penerima` varchar(255) NOT NULL,
  `lama_pembuangan` int(11) NOT NULL,
  `baku_mutu_pembuangan` decimal(8,2) NOT NULL,
  `derajat_s` varchar(255) NOT NULL,
  `jam_s` varchar(255) NOT NULL,
  `menit_s` varchar(255) NOT NULL,
  `derajat_e` varchar(255) NOT NULL,
  `jam_e` varchar(255) NOT NULL,
  `menit_e` varchar(255) NOT NULL,
  `dasar_hukum` text NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `izin_tps_limbah`
--

CREATE TABLE `izin_tps_limbah` (
  `id_izin_tps` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `no_izin` varchar(100) NOT NULL,
  `tgl_izin` date NOT NULL,
  `tentang` text NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_bap`
--

CREATE TABLE `jenis_bap` (
  `id_jenis_bap` int(11) NOT NULL,
  `ket` varchar(500) NOT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_bap`
--

INSERT INTO `jenis_bap` (`id_jenis_bap`, `ket`, `value`) VALUES
(1, 'BAP Industri', 'bap_industri'),
(3, 'BAP Hotel', 'bap_hotel'),
(4, 'BAP Rumah Makan/Domestik', 'bap_rm'),
(5, 'BAP Agro', 'bap_agro'),
(6, 'BAP Golf', 'bap_golf'),
(7, 'BAP SPPBE', 'bap_sppbe'),
(8, 'BAP Rumah Sakit', 'bap_rs'),
(9, 'BAP RPH', 'bap_rph'),
(10, 'BAP Laboratorium', 'bap_lab'),
(11, 'BAP Pertambangan', 'bap_tambang'),
(12, 'BAP Pengelola Limbah B3', 'bap_plb3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_dokumen`
--

CREATE TABLE `jenis_dokumen` (
  `id_jenis` int(11) NOT NULL,
  `kategori_dokumen` varchar(500) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `jml_data_tahun` varchar(100) NOT NULL,
  `periode_pencatatan` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `periode_bulan` varchar(10) NOT NULL,
  `toleransi_hari` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_dokumen`
--

INSERT INTO `jenis_dokumen` (`id_jenis`, `kategori_dokumen`, `ket`, `jml_data_tahun`, `periode_pencatatan`, `status`, `periode_bulan`, `toleransi_hari`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 'Pengendalian Pencemaran Air', 'Laporan Hasil Pengujian Kualitas Air Limbah', '12', 'Setiap Bulan', 1, 'triwulan', 30, '2014-12-07', '2014-12-07', 'Administrator', 'Administrator'),
(2, 'Pengendalian Pencemaran Air', 'Catatan Debit Harian Air Limbah', '12', 'Setiap Bulan', 1, 'triwulan', 30, '2014-12-07', '2014-12-07', 'Administrator', 'Administrator'),
(3, 'Pengendalian Pencemaran Udara', 'Laporan Hasil Pengujian Kualitas Udara Emisi Sumber Tidak Bergerak', '2', 'Setiap 6 Bulan', 1, 'semester', 30, '2014-12-07', '2014-12-07', 'Administrator', 'Administrator'),
(4, 'Pengendalian Pencemaran Udara', 'Laporan Hasil Pengujian Kualitas Udara Ambien', '2', 'Setiap 6 Bulan', 1, 'semester', 30, '2014-12-07', '2014-12-07', 'Administrator', 'Administrator'),
(5, 'Pengendalian Limbah Padat dan B3', 'Manifest Limbah B3', '12', 'Setiap Diperlukan', 1, 'triwulan', 30, '2014-12-07', '2014-12-07', 'Administrator', 'Administrator'),
(6, 'Pengendalian Limbah Padat dan B3', 'Neraca Limbah B3', '12', 'Setiap Bulan', 1, 'triwulan', 30, '2014-12-07', '2014-12-07', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_emisi_boiler`
--

CREATE TABLE `jenis_emisi_boiler` (
  `id_jenis_emisi_boiler` int(11) NOT NULL,
  `nama_jenis_emisi_boiler` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_emisi_boiler`
--

INSERT INTO `jenis_emisi_boiler` (`id_jenis_emisi_boiler`, `nama_jenis_emisi_boiler`) VALUES
(1, 'Steam'),
(2, 'Steam 2'),
(3, 'Oli'),
(4, 'Lain-lain');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_industri`
--

CREATE TABLE `jenis_industri` (
  `id_jenis_industri` int(11) NOT NULL,
  `id_usaha_kegiatan` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_industri`
--

INSERT INTO `jenis_industri` (`id_jenis_industri`, `id_usaha_kegiatan`, `ket`, `status`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 14, 'Tekstil', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(2, 14, 'Kertas', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(3, 14, 'Industri Makanan', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(4, 14, 'Kimia Tekstil', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(5, 14, 'Elektrolpating', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(6, 20, 'Kelas A', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(7, 20, 'Kelas B', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(9, 13, 'Bintang 4', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(10, 13, 'Bintang 5', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(20, 19, 'Rumah Makan / Domestik', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(21, 18, 'RPH', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(22, 17, 'Pertambangan', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(23, 16, 'Pengelola Limbah B3', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(24, 15, 'Laboratorium', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(25, 14, 'Industri', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(26, 13, 'Hotel', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(27, 12, 'Golf', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(60, 11, 'Agro', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(61, 22, 'Farmasi', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(70, 21, 'SPPBE', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(75, 24, 'Geothermal', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(83, 25, 'PLTU', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(84, 20, 'Rumah Sakit', 1, '2015-01-02', '0000-00-00', 'Administrator', ''),
(85, 14, 'Pertenunan', 1, '2018-11-05', '0000-00-00', 'Administrator', ''),
(86, 14, 'Garmen', 1, '2018-11-05', '0000-00-00', 'Administrator', ''),
(87, 14, 'Sepatu', 1, '2018-11-05', '0000-00-00', 'Administrator', ''),
(88, 14, 'plastik', 1, '2018-11-05', '0000-00-00', 'Administrator', ''),
(89, 14, 'Penyempurnaan kulit', 1, '2018-11-05', '0000-00-00', 'Administrator', ''),
(90, 14, 'Pakaian Jadi', 1, '2018-11-05', '0000-00-00', 'Administrator', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_limbah_b3`
--

CREATE TABLE `jenis_limbah_b3` (
  `id_jlb3` int(11) NOT NULL,
  `id_penc_pb3` int(11) NOT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `pengelolaan` varchar(50) DEFAULT NULL,
  `pihak_ke3` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_limbah_b3`
--

INSERT INTO `jenis_limbah_b3` (`id_jlb3`, `id_penc_pb3`, `jenis`, `jumlah`, `pengelolaan`, `pihak_ke3`) VALUES
(11, 3, 'Accu Bekas', '0.001', 'Dikelola', 'CV. Selasih'),
(12, 3, 'Limbah B3 cair', '0.002', 'Dikelola', 'CV. Selasih'),
(13, 3, 'Limbah Padat B3', '0.003', 'Dikelola', 'CV. Seloka'),
(14, 4, NULL, '', '', ''),
(36, 15, 'Accu Bekas', '1', 'Tidak Dikelola', 'CV. Seloka'),
(40, 25, 'B3 kadaluarsa', '1', 'Dikelola', 'CV. Selasih'),
(41, 25, 'Limbah B3 cair', '2', 'Tidak Dikelola', 'CV. Seloka'),
(46, 29, 'Accu Bekas', '1', 'Dikelola', 'CV. Seloka'),
(47, 29, 'B3 kadaluarsa', '2', 'Tidak Dikelola', 'CV. Selasih'),
(48, 30, 'Accu Bekas', '11', 'Dikelola', 'CV. Selasih'),
(50, 32, 'Accu Bekas', '1', 'Tidak Dikelola', 'CV. Selasih'),
(51, 39, 'Accu Bekas', '2', 'Dikelola', 'CV. Selasih'),
(52, 39, 'B3 kadaluarsa', '2', 'Tidak Dikelola', 'CV. Seloka'),
(55, 41, 'Accu Bekas', '1', 'Tidak Dikelola', 'CV. Selasih'),
(56, 42, 'Accu Bekas', '1', 'Dikelola', 'CV. Seloka'),
(57, 43, 'Accu Bekas', '1', 'Dikelola', 'CV. Seloka'),
(58, 44, 'Accu Bekas', '1', 'Dikelola', 'CV. Selasih'),
(60, 46, 'Accu Bekas', '1', 'Tidak Dikelola', 'CV. Seloka'),
(62, 47, 'Accu Bekas', '2', 'Tidak Dikelola', 'CV. Seloka'),
(63, 48, 'Accu Bekas', '2', 'Tidak Dikelola', 'CV. Seloka'),
(67, 55, '', '', '', ''),
(68, 56, '', '', '', ''),
(69, 57, '', '', '', ''),
(70, 58, '', '', '', ''),
(72, 60, 'Limbah Batubara - Fly Ash', '48', 'Dikelola', ''),
(73, 61, 'Limbah Batubara - Fly Ash', '48', 'Dikelola', ''),
(74, 62, 'Limbah Batubara - Fly Ash', '48', 'Dikelola', ''),
(75, 63, 'Limbah Batubara - Fly Ash', '48', 'Dikelola', ''),
(76, 64, 'Lumpur IPAL', '0,13', 'Dikelola', ''),
(77, 65, 'Lumpur IPAL', '0,13', 'Dikelola', ''),
(79, 66, 'Lumpur IPAL', '0,03', 'Dikelola', ''),
(81, 67, 'Lumpur IPAL', '0,03', 'Dikelola', ''),
(82, 59, 'Minyak Pelumas Bekas', '', '', ''),
(83, 68, 'Fly Ash', '23.871', 'Dikelola', '0 PT. Tenang Jaya Sejahtera'),
(84, 68, 'Minyak Pelumas Bekas', '1.440', 'Dikelola', '0 PT. WGI'),
(85, 69, 'Lumpur IPAL', '2 kg', 'Dikelola', '0 PT. Tenang Jaya Sejahtera'),
(86, 69, 'Minyak Pelumas Bekas', '2', 'Dikelola', '0 PT. WGI'),
(93, 71, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(94, 71, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(95, 71, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(96, 71, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(97, 72, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(98, 72, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(99, 72, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(100, 72, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(101, 73, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(102, 73, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(103, 73, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(104, 73, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(105, 74, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(106, 74, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(107, 74, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(108, 74, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(109, 75, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(110, 75, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(111, 75, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(112, 75, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(113, 76, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(114, 76, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(115, 76, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(116, 77, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(117, 76, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(118, 77, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(119, 77, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(120, 77, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(121, 78, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(122, 78, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(123, 78, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(124, 78, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(125, 79, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(126, 79, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(127, 80, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(128, 82, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(129, 81, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(130, 79, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(131, 80, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(132, 82, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(133, 79, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(134, 81, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(135, 80, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(136, 82, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(137, 81, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(138, 80, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(139, 82, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(140, 81, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(141, 83, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(142, 83, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(143, 83, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(144, 83, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(145, 84, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(146, 84, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(147, 84, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(148, 84, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(149, 85, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(150, 85, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(151, 85, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(152, 85, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(153, 86, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(154, 86, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(155, 86, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(156, 86, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(157, 87, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(158, 87, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(159, 87, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(160, 87, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(161, 88, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(162, 88, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(163, 88, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(164, 88, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(165, 89, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(166, 89, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(167, 89, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(168, 89, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(169, 90, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(170, 90, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(171, 90, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(172, 90, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(173, 91, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(174, 91, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(175, 91, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(176, 91, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(177, 92, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(178, 92, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(179, 92, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(180, 92, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(181, 93, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(182, 93, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(183, 93, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(184, 93, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(185, 94, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(186, 95, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(187, 94, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(188, 94, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(189, 94, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(190, 95, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(191, 95, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(192, 95, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(193, 96, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(194, 96, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(195, 96, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(196, 96, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(197, 97, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(198, 97, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(199, 97, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(200, 97, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(201, 98, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(202, 98, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(203, 99, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(204, 98, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(205, 99, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(206, 98, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(207, 99, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(208, 99, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(209, 100, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(210, 100, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(211, 100, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(212, 100, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(213, 101, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(214, 101, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(215, 101, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(216, 101, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(217, 102, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(218, 102, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(219, 102, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(220, 102, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(221, 103, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(222, 103, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(223, 103, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(224, 103, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(225, 104, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(226, 104, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(227, 104, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(228, 104, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(229, 105, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(230, 105, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(231, 105, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(232, 105, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(233, 106, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(234, 106, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(235, 106, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(236, 106, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(237, 107, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(238, 107, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(239, 108, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(240, 107, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(241, 108, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(242, 107, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(243, 108, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(244, 108, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(245, 109, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(246, 109, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(247, 109, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(248, 109, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(249, 110, 'Lampu TL Bekas', '0,50', 'Tidak Dikelola', ''),
(250, 110, 'Lumpur IPAL', '2 ton', 'Tidak Dikelola', ''),
(251, 110, 'Minyak Pelumas Bekas', '9', 'Tidak Dikelola', ''),
(252, 110, 'Bekas kemasan terkontaminasi limbah B3', '6', 'Tidak Dikelola', ''),
(254, 70, 'Fly Ash / Button Ash', '34,74', 'Dikelola', '0 CV. HIBK'),
(255, 70, 'Lumpur IPAL', 'Tentatip', 'Dikelola', ''),
(257, 53, 'Fly Ash / Button Ash', '9,67', 'Dikelola', '0 PT. Jobs Colouring');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL,
  `tgl_update` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `ket`, `status`, `tgl_pembuatan`, `dibuat_oleh`, `diupdate_oleh`, `tgl_update`) VALUES
(1, 'Cikutra', 1, '2014-10-28', 'Indra Permana', '', '0000-00-00'),
(3, 'Cikutra Timur', 1, '2014-10-28', 'Indra Permana', '', '0000-00-00'),
(4, 'Arjasari', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(5, 'Baleendah', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(6, 'Banjaran', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(7, 'Bojongsoang', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(8, 'Cangkuang', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(9, 'Cicalengka', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(10, 'Cikancung', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(11, 'Cilengkrang', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(12, 'Cileunyi', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(13, 'Cimaung', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(14, 'Cimenyan', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(15, 'Ciparay', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(16, 'Ciwidey', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(17, 'Dayeuhkolot', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(18, 'Ibun', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(19, 'Katapang', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(20, 'Kertasari', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(21, 'Kutawaringin', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(22, 'Majalaya', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(23, 'Margaasih', 1, '2014-11-15', 'Guntur Santoso', 'Administrator', '2018-11-05'),
(24, 'Margahayu', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(25, 'Nagreg', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(26, 'Pacet', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(27, 'Pameungpeuk', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(28, 'Pangalengan', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(29, 'Paseh', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(30, 'Pasirjambu', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(31, 'Rancabali', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(32, 'Solokan Jeruk', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(33, 'Rancaekek', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00'),
(34, 'Soreang', 1, '2014-11-15', 'Guntur Santoso', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kelurahan` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelurahan`
--

INSERT INTO `kelurahan` (`id_kelurahan`, `id_kecamatan`, `ket`, `status`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 1, 'Cibeuying Kidul', 1, '2014-10-28', '0000-00-00', 'Indra Permana', ''),
(3, 3, 'Cibeuying Kidul', 1, '2014-10-28', '0000-00-00', 'Indra Permana', ''),
(4, 34, 'Cingcin', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(5, 34, 'Karamatmulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(6, 34, 'Pamekaran', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(7, 34, 'Panyirapan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(8, 34, 'Parungserab', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(9, 34, 'Sadu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(10, 34, 'Sekarwangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(11, 34, 'Soreang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(12, 34, 'Sukajadi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(13, 34, 'Sukanagara', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(14, 34, 'Bojongemas', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(15, 32, 'Bojongemas', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(16, 32, 'Cibodas', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(17, 32, 'Langensari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(18, 32, 'Padamukti', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(19, 32, 'Panyadap', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(20, 32, 'Rancakasumba', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(21, 32, 'Solokan Jeruk', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(22, 33, 'Bojongloa', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(23, 33, 'Bojongsalam', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(24, 33, 'Cangkuang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(25, 33, 'Haurpugur', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(26, 33, 'Jelegong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(27, 33, 'Linggar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(28, 33, 'Nanjungmekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(29, 33, 'Rancaekek Kencana', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(30, 33, 'Rancaekek Kulon', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(31, 33, 'Rancaekek Wetan	', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(32, 33, 'Sangiang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(33, 33, 'Sukamanah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(34, 33, 'Sukamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(35, 33, 'Tegal Sumedang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(36, 31, 'Alamendah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(37, 31, 'Cipelah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(38, 31, 'Indragiri', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(39, 31, 'Patengan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(40, 31, 'Sukaresmi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(41, 30, 'Cibodas', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(42, 30, 'Cikoneng', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(43, 30, 'Cisondari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(44, 30, 'Cukanggenteng', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(45, 30, 'Margamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(46, 30, 'Mekarmaju', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(47, 30, 'Mekarsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(48, 30, 'Pasirjambu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(49, 30, 'Sugihmukti', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(50, 30, 'Tenjolaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(51, 29, 'Cigentur', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(52, 29, 'Cijagra', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(53, 29, 'Cipaku', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(54, 29, 'Cipedes', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(55, 29, 'Drawati', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(56, 29, 'Karangtunggal', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(57, 29, 'Loa', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(58, 29, 'Mekarpawitan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(59, 29, 'Sindangsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(60, 29, 'Sukamanah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(61, 29, 'Sukamantri', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(62, 29, 'Tangsimekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(63, 28, 'Banjarsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(64, 28, 'Lamajang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(65, 28, 'Margaluyu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(66, 28, 'Margamekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(67, 28, 'Margamukti', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(68, 28, 'Margamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(69, 28, 'Pangalengan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(70, 28, 'Pulosari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(71, 28, 'Sukaluyu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(72, 28, 'Sukamanah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(73, 28, 'Tribaktimulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(74, 28, 'Wanasuka', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(75, 28, 'Warnasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(76, 27, 'Bojongkunci', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(77, 27, 'Bojongmanggu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(78, 27, 'Langonsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(79, 27, 'Rancamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(80, 27, 'Rancatungku', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(81, 27, 'Sukasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(82, 26, 'Cikawao', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(83, 26, 'Cikitu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(84, 26, 'Cinanggela', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(85, 26, 'Cipeujeuh', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(86, 26, 'Girimulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(87, 26, 'Mandalahaji', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(88, 26, 'Maruyung', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(89, 26, 'Mekarjaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(90, 26, 'Mekarsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(91, 26, 'Nagrak', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(92, 26, 'Pangauban', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(93, 26, 'Sukarame', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(94, 26, 'Tanjungwangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(95, 25, 'Bojong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(96, 25, 'Ciaro', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(97, 25, 'Ciherang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(98, 25, 'Citaman', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(99, 25, 'Ganjar Sabar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(100, 25, 'Mandalawangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(101, 25, 'Nagreg', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(102, 25, 'Nagreg Kendan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(103, 24, 'Margahayu Selatan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(104, 24, 'Margahayu Tengah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(105, 24, 'Sayati', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(106, 24, 'Sukamenak', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(107, 24, 'Sulaeman', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(108, 23, 'Cigondewah Hilir', 1, '2014-11-15', '2014-11-15', 'Guntur Santoso', 'Guntur Santoso'),
(109, 23, 'Lagadar', 1, '2014-11-15', '2014-11-15', 'Guntur Santoso', 'Guntur Santoso'),
(110, 23, 'Margaasih', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(111, 23, 'Mekar Rahayu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(112, 23, 'Nanjung', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(113, 23, 'Rahayu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(114, 22, 'Biru', 1, '2014-11-15', '2014-11-15', 'Guntur Santoso', 'Guntur Santoso'),
(115, 22, 'Bojong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(116, 22, 'Majakerta', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(117, 22, 'Majalaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(118, 22, 'Majasetra', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(119, 22, 'Neglasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(120, 22, 'Padamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(121, 22, 'Padaulun', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(122, 22, 'Sukamaju', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(123, 22, 'Sukamukti', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(124, 22, 'Wangisagara', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(125, 21, 'Buninagara', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(126, 21, 'Cibodas', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(127, 21, 'Cilame', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(128, 21, 'Gajahmekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(129, 21, 'Jatisari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(130, 21, 'Jelegong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(131, 21, 'Kopo', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(132, 21, 'Kutawaringin', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(133, 21, 'Padasuka', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(134, 21, 'Pameuntasan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(135, 21, 'Sukamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(136, 20, 'Cibeureum', 1, '2014-11-15', '2014-11-15', 'Guntur Santoso', 'Guntur Santoso'),
(137, 20, 'Cihawuk', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(138, 20, 'Cikembang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(139, 20, 'Neglawangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(140, 20, 'Resmi Tingal', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(141, 20, 'Santosa', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(142, 20, 'Sukapura', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(143, 20, 'Tarumajaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(144, 19, 'Banyusari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(145, 19, 'Cilampeni', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(146, 19, 'Gandasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(147, 19, 'Katapang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(148, 19, 'Pangauban', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(149, 19, 'Sangkanhurip', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(150, 19, 'Sukamukti', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(151, 18, 'Cibeet', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(152, 18, 'Dukuh', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(153, 18, 'Ibun', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(154, 18, 'Karyalaksana', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(155, 18, 'Laksana', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(156, 18, 'Lampegan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(157, 18, 'Mekarwangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(158, 18, 'Neglasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(159, 18, 'Pangguh', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(160, 18, 'Sudi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(161, 18, 'Talun', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(162, 18, 'Tanggulun', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(163, 17, 'Cangkuang Kulon', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(164, 17, 'Cangkuang Wetan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(165, 17, 'Citeureup', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(166, 17, 'Dayeuhkolot', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(167, 17, 'Pasawahan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(168, 17, 'Sukapura', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(169, 16, 'Ciwidey', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(170, 16, 'Lebakmuncang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(171, 16, 'Nengkelan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(172, 16, 'Panundaan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(173, 16, 'Panyocokan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(174, 16, 'Rawabogo', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(175, 16, 'Sukawening', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(176, 15, 'Babakan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(177, 15, 'Bumiwangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(178, 15, 'Ciheulang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(179, 15, 'Cikoneng', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(180, 15, 'Ciparay', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(181, 15, 'Gunungleutik', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(182, 15, 'Manggungharja', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(183, 15, 'Mekar Laksana', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(184, 15, 'Mekarsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(185, 15, 'Pakutandang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(186, 15, 'Sarimahi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(187, 15, 'Serangmekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(188, 15, 'Sigaracipta', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(189, 15, 'Sumbersari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(190, 14, 'Cibeunying', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(191, 14, 'Ciburial', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(192, 14, 'Cikadut', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(193, 14, 'Cimenyan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(194, 14, 'Mandalamekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(195, 14, 'Mekarmanik', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(196, 14, 'Mekarsaluyu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(197, 14, 'Padasuka', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(198, 14, 'Sindanglaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(199, 13, 'Campakamulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(200, 13, 'Cikalong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(201, 13, 'Cimaung', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(202, 13, 'Cipinang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(203, 13, 'Jagabaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(204, 13, 'Malasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(205, 12, 'Cibiru Hilir', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(206, 12, 'Cibiru Wetan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(207, 12, 'Cileunyi Kulon', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(208, 12, 'Cileunyi Wetan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(209, 12, 'Cimekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(210, 12, 'Cinunuk', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(211, 11, 'Cilengkrang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(212, 11, 'Cipanjalu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(213, 10, 'Cihanyir', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(214, 10, 'Cikancung', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(215, 9, 'Babakan Peuteuy', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(216, 9, 'Cicalengka Kulon', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(217, 9, 'Cicalengka Wetan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(218, 9, 'Cikuya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(219, 9, 'Dampit', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(220, 9, 'Margaasih', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(221, 9, 'Nagrog', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(222, 9, 'Narawita', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(223, 9, 'Panenjoan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(224, 9, 'Tanjungwangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(225, 9, 'Tenjolaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(226, 9, 'Waluya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(227, 8, 'Bandasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(228, 8, 'Cangkuang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(229, 8, 'Ciluncat', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(230, 8, 'Jatisari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(231, 8, 'Nagrak', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(232, 8, 'Pananjung', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(233, 8, 'Tanjungsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(234, 7, 'Bojongsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(235, 7, 'Bojongsoang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(236, 7, 'Buahbatu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(237, 7, 'Cipagalo', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(238, 7, 'Lengkong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(239, 7, 'Tegalluar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(240, 6, 'Banjaran Kulon', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(241, 6, 'Banjaran Wetan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(242, 6, 'Ciapus', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(243, 6, 'Kamasan', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(244, 6, 'Kiangroke', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(245, 6, 'Margahurip', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(246, 6, 'Mekarjaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(247, 6, 'Neglasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(248, 6, 'Pasirmulya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(249, 6, 'Sindangpanon', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(250, 6, 'Tarajusari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(251, 5, 'Andir', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(252, 5, 'Baleendah', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(253, 5, 'Bojongmalaka', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(254, 5, 'Jelekong', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(255, 5, 'Malakasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(256, 5, 'Manggahang', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(257, 5, 'Rancamanyar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(258, 5, 'Wargamekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(259, 4, 'Ancolmekar', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(260, 4, 'Arjasari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(261, 4, 'Baros', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(262, 4, 'Batukarut', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(263, 4, 'Lebakwangi', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(264, 4, 'Mangunjaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(265, 4, 'Mekarjaya', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(266, 4, 'Patrolsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(267, 4, 'Pinggirsari', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(268, 4, 'Rancakole', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(269, 4, 'Wargaluyu', 1, '2014-11-15', '0000-00-00', 'Guntur Santoso', ''),
(270, 23, 'cikuya ', 1, '2018-11-05', '0000-00-00', 'Administrator', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laboratorium`
--

CREATE TABLE `laboratorium` (
  `id_lab` int(11) NOT NULL,
  `nama_lab` varchar(255) NOT NULL,
  `no_lab` varchar(255) NOT NULL,
  `no_akreditasi` varchar(1000) NOT NULL,
  `tlp` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `fax` varchar(25) NOT NULL,
  `kategori_lab` varchar(255) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laboratorium`
--

INSERT INTO `laboratorium` (`id_lab`, `nama_lab`, `no_lab`, `no_akreditasi`, `tlp`, `email`, `alamat`, `fax`, `kategori_lab`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 'DLH - Air', '7', '7', '022987893798', 'email@gmail.com', 'Alamat', '022987983', 'Lab Pengujian Kualitas Air', '2015-01-04', '2018-10-22', 'Administrator', 'Administrator'),
(2, 'Sucofindo - Air', '6', '6', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Air', '2015-01-04', '2018-10-22', 'Administrator', 'Administrator'),
(3, 'PDAM Tirtawening - Air', '4', '4', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Air', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(4, 'BBPK - Air', 'LP-046-IDN', 'LP-046-IDN', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Air', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(5, 'Puslitbang SDA - Air', '5', '5', '0', 'puslitbang@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Air', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(6, 'Bina Lab - Air', '2', '2', '0', 'binalab@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Air', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(7, 'DLH - Udara', '1', '1', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Udara', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(8, 'BBPK - Udara', '3', '3', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Udara', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(9, 'PPSDAL - Udara', '8', '8', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Udara', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(10, 'Lab Kimia Fisik UNPAD - Udara', '9', '9', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Udara', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(11, 'Bina Lab - Udara', '10', '10', '0', 'email@gmail.com', 'Alamat', '0', 'Lab Pengujian Kualitas Udara', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan_hasil_uji`
--

CREATE TABLE `laporan_hasil_uji` (
  `id_laporan_hasil_uji` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `id_lab` int(11) NOT NULL,
  `no_lhu` varchar(255) NOT NULL,
  `laporan_bulan_tahun` varchar(10) NOT NULL,
  `jenis_sample` varchar(255) NOT NULL,
  `tgl_pengambilan_sample` date NOT NULL,
  `waktu_pengambilan_sample` time NOT NULL,
  `proses_produksi` text NOT NULL,
  `kapasitas_produksi` text NOT NULL,
  `koord_outlet_ipal` text NOT NULL,
  `jenis_bahan_bakar` varchar(100) NOT NULL,
  `koord_cerobong` text NOT NULL,
  `koord_titik_pengambilan_sample` text NOT NULL,
  `jenis_lhu` varchar(100) NOT NULL,
  `jenis_baku_mutu` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `pengirim` varchar(100) NOT NULL,
  `tgl_kirim` date NOT NULL,
  `via` varchar(30) NOT NULL,
  `barbuk` varchar(255) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `laporan_hasil_uji`
--

INSERT INTO `laporan_hasil_uji` (`id_laporan_hasil_uji`, `id_industri`, `id_lab`, `no_lhu`, `laporan_bulan_tahun`, `jenis_sample`, `tgl_pengambilan_sample`, `waktu_pengambilan_sample`, `proses_produksi`, `kapasitas_produksi`, `koord_outlet_ipal`, `jenis_bahan_bakar`, `koord_cerobong`, `koord_titik_pengambilan_sample`, `jenis_lhu`, `jenis_baku_mutu`, `status`, `pengirim`, `tgl_kirim`, `via`, `barbuk`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 20, 1, '11', '12.2016', 'a:1:{i:0;s:5:\"Inlet\";}', '2016-12-13', '20:56:00', 'a:1:{i:0;s:7:\"Washing\";}', 'a:1:{i:0;s:2:\"12\";}', 'a:1:{i:0;s:11:\"1:2:3/4:5:6\";}', '', '', '', 'LHU Air Limbah', 'Eksternal', 0, '', '0000-00-00', '', '', '2016-12-11', '2018-10-22', 'Administrator', 'Administrator'),
(2, 1, 1, '1111', '12.2016', '', '2016-12-11', '20:58:00', '', '', '', 'Batubara', 'a:1:{i:0;s:11:\"1:2:3/4:5:6\";}', '', 'LHU Udara Emisi', 'Internal', 0, '', '0000-00-00', '', '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(3, 2, 2, '1111', '12.2016', 'Site', '2016-12-11', '20:59:00', '', '', '', '', '', 'a:1:{i:0;s:11:\"1:2:3/4:5:6\";}', 'LHU Udara Ambien', 'Internal', 0, '', '0000-00-00', '', '', '0000-00-00', '0000-00-00', 'Administrator', 'Administrator'),
(4, 3, 1, '111', '12.2016', '', '2016-12-11', '21:07:00', '', '', '', 'Batubara', 'a:1:{i:0;s:11:\"1:2:3/4:5:6\";}', '', 'LHU Udara Emisi', 'Internal', 0, '', '0000-00-00', '', '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(5, 20, 1, '123', '10.2018', 'a:1:{i:0;s:6:\"Outlet\";}', '2018-10-15', '15:18:00', 'a:1:{i:0;s:23:\"Proses Pencampuran Dyes\";}', 'a:1:{i:0;s:2:\"20\";}', 'a:1:{i:0;s:18:\"06:08:55/107:22:44\";}', '', '', '', 'LHU Air Limbah', 'Internal', 0, '', '0000-00-00', '', '', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator'),
(6, 29, 1, 'LP-367-IDN', '04.2018', 'a:1:{i:0;s:6:\"Outlet\";}', '2018-04-19', '10:18:00', 'a:1:{i:0;s:9:\"Finishing\";}', 'a:1:{i:0;s:4:\"2000\";}', 'a:1:{i:0;s:18:\"06:59:11/107:33:05\";}', '', '', '', 'LHU Air Limbah', 'Eksternal', 0, '', '0000-00-00', '', '', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `manifest`
--

CREATE TABLE `manifest` (
  `id_manifest` int(11) NOT NULL,
  `id_penghasil_limbah` int(11) NOT NULL,
  `id_pengangkut_limbah` int(11) NOT NULL,
  `id_pengolah_limbah` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `tgl_angkut` date NOT NULL,
  `jenis_limbah` varchar(1000) NOT NULL,
  `nama_teknik` varchar(1000) NOT NULL,
  `karakteristik_limbah` varchar(1000) NOT NULL,
  `kode_limbah` varchar(1000) NOT NULL,
  `kelompok_kemasan` varchar(255) NOT NULL,
  `satuan_ukuran` varchar(255) NOT NULL,
  `total_kemasan` int(11) NOT NULL,
  `ket_tambahan` text NOT NULL,
  `tujuan` text NOT NULL,
  `nama_berkas` text NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `manifest`
--

INSERT INTO `manifest` (`id_manifest`, `id_penghasil_limbah`, `id_pengangkut_limbah`, `id_pengolah_limbah`, `tgl`, `tgl_angkut`, `jenis_limbah`, `nama_teknik`, `karakteristik_limbah`, `kode_limbah`, `kelompok_kemasan`, `satuan_ukuran`, `total_kemasan`, `ket_tambahan`, `tujuan`, `nama_berkas`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 1, 1, 2, '2016-12-11', '2016-12-07', 'B3 kadaluarsa', '', 'A', 'ASD', 'AS', 'ASD', 0, 'adh', 'a', '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `neraca_b3`
--

CREATE TABLE `neraca_b3` (
  `id_neraca` int(11) NOT NULL,
  `id_industri` int(11) NOT NULL,
  `periode_waktu` date NOT NULL,
  `jenis_limbah` text NOT NULL,
  `total_jenis_limbah` decimal(8,2) NOT NULL,
  `dikelola` decimal(8,2) NOT NULL,
  `nama_berkas` text NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `neraca_b3`
--

INSERT INTO `neraca_b3` (`id_neraca`, `id_industri`, `periode_waktu`, `jenis_limbah`, `total_jenis_limbah`, `dikelola`, `nama_berkas`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 4, '2016-12-15', 'a:2:{i:0;s:12:\"Accu Bekas:1\";i:1;s:16:\"Lampu TL Bekas:2\";}', '3.00', '11.00', '', '2016-12-15', '2016-12-15', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `parameter_bap`
--

CREATE TABLE `parameter_bap` (
  `id_parameter_bap` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `dasar_hukum` text,
  `id_jenis_bap` int(11) NOT NULL,
  `nilai_parameter_bap` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL,
  `jenis_bap` varchar(20) NOT NULL,
  `urutan` int(11) NOT NULL,
  `id_parent` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `parameter_bap`
--

INSERT INTO `parameter_bap` (`id_parameter_bap`, `ket`, `dasar_hukum`, `id_jenis_bap`, `nilai_parameter_bap`, `status`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `jenis_bap`, `urutan`, `id_parent`) VALUES
(1, 'DATA UMUM', '-', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 1, 'top'),
(2, 'PENGENDALIAN PENCEMARAN AIR', '-', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 2, '1'),
(3, 'PENGENDALIAN PENCEMARAN UDARA', '-', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 3, 'top'),
(4, 'PENGELOLAAN LIMBAH PADAT DAN B3', '-', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 4, 'top'),
(5, 'Dokumen Lingkungan', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 5, '1'),
(6, 'Izin Lingkungan', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 6, '1'),
(7, 'IPAL', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 7, '2'),
(8, 'Perizinan', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 8, '2'),
(9, 'Alat Ukur', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 9, '2'),
(10, 'Kondisi (apabila ada)', '', 5, 'Berfungsi', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 10, '9'),
(11, 'Catatan debit harian', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 11, '2'),
(12, 'Pengujian Kualitas Air Limbah', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 12, '2'),
(13, 'Periode Pengujian', '', 5, 'Setiap Bulan', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 13, '12'),
(14, 'Hasil Pengujian', '', 5, 'Memenuhi', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 14, '13'),
(15, 'Pelaporan', '', 5, 'Rutin', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 15, '12'),
(16, 'Kebocoran/Rembesan/Tumpahan/Bypass', '', 5, 'Tidak Ada', 1, '2015-01-02', '2015-02-12', 'Administrator', 'Administrator', 'bap_agro', 16, '2'),
(17, 'Data Cerobong', '', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 17, '3'),
(18, 'Sampling hole', '', 5, 'Ada', 1, '2015-01-02', '2015-02-12', 'Administrator', 'Administrator', 'bap_agro', 18, '17'),
(19, 'Tangga', '', 5, 'Ada', 1, '2015-01-02', '2015-02-12', 'Administrator', 'Administrator', 'bap_agro', 19, '17'),
(20, 'Pagar pengaman tangga', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 20, '17'),
(21, 'Lantai kerja', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 21, '17'),
(22, 'Pengujian kualitas emisi', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 22, '3'),
(23, 'Periode pengujian (per 6 bulan)', '', 5, 'Rutin', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 23, '22'),
(24, 'Pemenuhan BME', '', 5, 'Ya', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 24, '22'),
(25, 'Alat pengendali emisi', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 25, '3'),
(26, 'Data Kualitas Udara Ambien', '', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 26, '3'),
(27, 'Pengujian Kualitas', '', 5, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 27, '26'),
(28, 'Periode pengujian (per 6 bulan)', '', 5, 'Rutin', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 28, '26'),
(29, 'Pemenuhan BM', '', 5, 'Ya', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 29, '26'),
(30, 'Jenis limbah B3 yang ditimbulkan', '', 5, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_agro', 30, '4'),
(31, 'Pengelolaan Limbah B3', '', 5, 'Dikelola', 1, '2015-01-02', '2015-02-12', 'Administrator', 'Administrator', 'bap_agro', 31, '30'),
(40, 'Penyimpanan', '', 5, 'Pada TPS Limbah B3', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 42, '4'),
(41, 'TPS Limbah B3', '', 5, '-', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 43, '4'),
(42, 'Perizinan TPS', '', 5, 'Ada', 1, '2015-01-02', '2015-02-12', 'Administrator', 'Administrator', 'bap_agro', 44, '41'),
(44, 'Papan nama dan koordinat', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 45, '41'),
(45, 'Simbol dan label', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 46, '41'),
(46, 'Saluran ceceran air limbah', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 47, '41'),
(47, 'Bak penampung ceceran air limbah', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 48, '41'),
(48, 'Kemiringan', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 49, '41'),
(49, 'SOP pengelolaan dan tanggap darurat', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 50, '41'),
(50, 'Log book', '', 5, 'Ada', 1, '2015-01-02', '2015-01-08', 'Administrator', 'Administrator', 'bap_agro', 51, '41'),
(51, 'APAR', '', 5, 'Ada', 1, '2015-01-02', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 52, '41'),
(52, 'Neraca Limbah B3', '', 5, 'Ada', 1, '2015-01-02', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 55, '4'),
(53, 'Manifest', '', 5, 'Ada', 1, '2015-01-02', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 54, '4'),
(54, 'Pelaporan Neraca dan Manifest', '', 5, 'Ada', 1, '2015-01-02', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 56, '4'),
(55, 'Limbah Padat Lain', '', 5, '-', 1, '2015-01-02', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 57, '4'),
(56, 'Pengelolaan', '', 5, 'Dibakar', 1, '2015-01-02', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 58, '55'),
(57, 'DATA UMUM', '', 6, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_golf', 1, 'top'),
(58, 'PENGENDALIAN PENCEMARAN AIR', '', 6, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_golf', 2, 'top'),
(59, 'PENGELOLAAN LIMBAH PADAT', '', 6, '-', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 4, 'top'),
(60, 'Dokumen Lingkungan', '', 6, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 5, '57'),
(61, 'Izin Lingkungan', '', 6, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 6, '57'),
(62, 'Sarana Pengolahan Air Limbah', '', 6, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 7, '58'),
(66, 'Pengelolaan', '', 6, 'Dibakar', 1, '2015-01-02', '2015-01-05', 'Administrator', 'Administrator', 'bap_golf', 12, '59'),
(67, 'DATA UMUM', '', 9, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_rph', 1, 'top'),
(68, 'PENGENDALIAN PENCEMARAN AIR', '', 9, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_rph', 2, 'top'),
(69, 'PENGELOLAAN LIMBAH PADAT ', '', 9, '-', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 4, 'top'),
(70, 'Dokumen Lingkungan', '', 9, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 5, '67'),
(71, 'Izin Lingkungan', '', 9, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 6, '67'),
(72, 'Sarana Pengolahan Air Limbah', '', 9, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 7, '68'),
(76, 'Pengelolaan', '', 9, 'Dibakar', 1, '2015-01-02', '2015-01-05', 'Administrator', 'Administrator', 'bap_rph', 12, '69'),
(77, 'DATA UMUM', '', 4, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_rm', 1, 'top'),
(78, 'PENGENDALIAN PENCEMARAN AIR', '', 4, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_rm', 2, 'top'),
(79, 'PENGELOLAAN LIMBAH PADAT', '', 4, '-', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 4, 'top'),
(80, 'Dokumen Lingkungan', '', 4, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 5, '77'),
(81, 'Izin Lingkungan', '', 4, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 6, '77'),
(82, 'Sarana Pengolahan Air Limbah', '', 4, 'Ada', 1, '2015-01-02', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 7, '78'),
(86, 'Pengelolaan', '', 4, 'Dibakar', 1, '2015-01-02', '2015-01-05', 'Administrator', 'Administrator', 'bap_rm', 12, '79'),
(87, 'DATA UMUM', '', 7, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_sppbe', 1, 'top'),
(88, 'PENGENDALIAN PENCEMARAN AIR', '', 7, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_sppbe', 2, 'top'),
(89, 'PENGENDALIAN PENCEMARAN UDARA', '', 7, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_sppbe', 3, 'top'),
(90, 'PENGELOLAAN LIMBAH PADAT ', '', 7, '-', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_sppbe', 4, 'top'),
(91, 'Dokumen Lingkungan ', '', 7, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_sppbe', 5, '87'),
(92, 'Izin Lingkungan', '', 7, 'Ada', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator', 'bap_sppbe', 6, '87'),
(93, 'Sarana Pengolahan Air Limbah', '', 7, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_sppbe', 7, '88'),
(94, 'Data Kualitas Udara Ambien', '', 7, '-', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_sppbe', 8, '89'),
(95, 'Pengujian Kualitas', '', 7, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_sppbe', 9, '94'),
(96, 'Periode pengujian (per 6 bulan)', '', 7, 'Rutin', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_sppbe', 10, '94'),
(97, 'Pemenuhan BM', '', 7, 'Ya', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_sppbe', 11, '94'),
(98, 'DATA UMUM', '', 3, '-', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 1, 'top'),
(99, 'PENGENDALIAN PENCEMARAN AIR', '', 3, '-', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 2, 'top'),
(100, 'PENGENDALIAN PENCEMARAN UDARA', '', 3, '-', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 3, 'top'),
(101, 'PENGELOLAAN LIMBAH PADAT DAN B3', '', 3, '-', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 4, 'top'),
(102, 'Dokumen Lingkungan', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 5, '98'),
(103, 'Izin Lingkungan', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 6, '98'),
(104, 'IPAL', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 7, '99'),
(105, 'Perizinan', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 8, '99'),
(106, 'Alat Ukur', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 9, '99'),
(107, 'Kondisi (apabila ada)', '', 3, 'Berfungsi', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 10, '106'),
(108, 'Catatan debit harian', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 11, '99'),
(109, 'Pengujian Kualitas Air Limbah', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 12, '99'),
(110, 'Periode Pengujian', '', 3, 'Setiap Bulan', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 13, '109'),
(111, 'Hasil Pengujian', '', 3, 'Memenuhi', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 14, '109'),
(112, 'Pelaporan', '', 3, 'Rutin', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 15, '109'),
(113, 'Kebocoran/Rembesan/Tumpahan/Bypass', '', 3, 'Tidak Ada', 1, '2015-01-03', '2015-01-11', 'Administrator', 'Administrator', 'bap_hotel', 16, '99'),
(114, 'Data Kualitas Udara Ambien', '', 3, '-', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 17, '100'),
(115, 'Pengujian Kualitas', '', 3, 'Ada', 1, '2015-01-03', '2015-01-03', 'Administrator', 'Administrator', 'bap_hotel', 18, '114'),
(116, 'Periode pengujian (per 6 bulan)', '', 3, 'Rutin', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 19, '114'),
(117, 'Pemenuhan BM', '', 3, 'Ya', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 20, '114'),
(118, 'Pengelolaan Limbah B3', '', 3, 'Dikelola', 1, '2015-01-03', '2015-01-05', 'Administrator', 'Administrator', 'bap_hotel', 21, '101'),
(122, 'Penyimpanan', '', 3, 'pada TPS Limbah B3', 1, '2015-01-03', '2015-01-05', 'Administrator', 'Administrator', 'bap_hotel', 22, '101'),
(123, 'TPS Limbah B3', '', 3, '-', 1, '2015-01-03', '2015-01-05', 'Administrator', 'Administrator', 'bap_hotel', 23, '101'),
(124, 'Perizinan TPS', '', 3, 'Ada', 1, '2015-01-03', '2015-01-23', 'Administrator', 'Administrator', 'bap_hotel', 24, '123'),
(126, 'Papan nama dan koordinat', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 25, '123'),
(127, 'Simbol dan label', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 26, '123'),
(128, 'Saluran ceceran air limbah', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 27, '123'),
(129, 'Bak penampung ceceran air limbah', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 28, '123'),
(130, 'Kemiringan', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 29, '123'),
(131, 'SOP pengelolaan dan tanggap darurat', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 30, '123'),
(132, 'Log book', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 31, '123'),
(133, 'APAR/Kotak P3K', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 32, '123'),
(134, 'Neraca Limbah B3', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 33, '101'),
(135, 'Manifest', '', 3, 'Ada', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 34, '101'),
(136, 'Pelaporan Neraca dan Manifest', '', 3, 'Rutin', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 35, '101'),
(137, 'Limbah Padat Lain', '', 3, '-', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 36, '101'),
(141, 'Pengelolaan', '', 3, 'Dibakar', 1, '2015-01-03', '2015-01-08', 'Administrator', 'Administrator', 'bap_hotel', 37, '137'),
(142, 'DATA UMUM', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 1, 'top'),
(143, 'PENGENDALIAN PENCEMARAN AIR', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 2, 'top'),
(144, 'PENGENDALIAN PENCEMARAN UDARA', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 3, 'top'),
(145, 'PENGELOLAAN LIMBAH PADAT DAN B3', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 4, 'top'),
(146, 'Dokumen Lingkungan', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 5, '142'),
(147, 'Izin Lingkungan', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 6, '142'),
(148, 'IPAL', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 7, '143'),
(149, 'Perizinan', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 8, '143'),
(150, 'Alat Ukur', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 9, '143'),
(151, 'Kondisi (apabila ada)', '', 1, 'Berfungsi', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 10, '150'),
(152, 'Catatan debit harian', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 11, '143'),
(153, 'Pengujian Kualitas Air Limbah', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 12, '143'),
(154, 'Periode Pengujian', '', 1, 'Setiap Bulan', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 13, '153'),
(155, 'Hasil Pengujian', '', 1, 'Memenuhi', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 14, '153'),
(156, 'Pelaporan', '', 1, 'Rutin', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 15, '153'),
(157, 'Kebocoran/Rembesan/Tumpahan/Bypass', '', 1, 'Tidak Ada', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_industri', 16, '143'),
(158, 'Data Cerobong', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 17, '144'),
(159, 'Sampling hole', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 18, '158'),
(160, 'Tangga', '', 1, 'Ada', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_industri', 19, '158'),
(161, 'Pagar pengaman tangga', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 20, '158'),
(162, 'Lantai kerja', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 21, '158'),
(163, 'Pengujian kualitas emisi', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 22, '144'),
(164, 'Periode pengujian (per 6 bulan)', '', 1, 'Rutin', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 23, '163'),
(165, 'Pemenuhan BME', '', 1, 'Ya', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 24, '163'),
(166, 'Alat pengendali emisi', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 25, '144'),
(167, 'Data Kualitas Udara Ambien', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 26, '144'),
(168, 'Pengujian Kualitas', '', 1, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 27, '167'),
(169, 'Periode pengujian (per 6 bulan)', '', 1, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 28, '167'),
(170, 'Pemenuhan BM', '', 1, 'Ya', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 29, '167'),
(171, 'Jenis limbah B3 yang ditimbulkan', '', 1, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_industri', 30, '145'),
(172, 'Pengelolaan Limbah B3', '', 1, 'Dikelola', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_industri', 31, '171'),
(181, 'Penyimpanan', '', 1, 'Pada TPS Limbah B3', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 42, '145'),
(182, 'TPS Limbah B3', '', 1, '-', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 43, '145'),
(183, 'Perizinan TPS', '', 1, 'Ada', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_industri', 44, '182'),
(185, 'Papan nama dan koordinat', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 45, '182'),
(186, 'Simbol dan label', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 46, '182'),
(187, 'Saluran ceceran air limbah', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 47, '182'),
(188, 'Bak penampung ceceran air limbah', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 48, '182'),
(189, 'Kemiringan', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 49, '182'),
(190, 'SOP pengelolaan dan tanggap darurat', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 50, '182'),
(191, 'Log book', '', 1, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_industri', 51, '182'),
(192, 'APAR', '', 1, 'Ada', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 52, '182'),
(193, 'Neraca Limbah B3', '', 1, 'Ada', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 54, '145'),
(194, 'Manifest', '', 1, 'Ada', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 55, '145'),
(195, 'Pelaporan Neraca dan Manifest', '', 1, 'Rutin', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 56, '145'),
(196, 'Limbah Padat Lain', '', 1, '-', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 57, '145'),
(197, 'Pengelolaan', '', 1, 'Dibakar', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 58, '196'),
(198, 'DATA UMUM', '', 8, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 1, 'top'),
(199, 'PENGENDALIAN PENCEMARAN AIR', '', 8, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 2, 'top'),
(200, 'PENGENDALIAN PENCEMARAN UDARA', '', 8, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 3, 'top'),
(201, 'PENGELOLAAN LIMBAH PADAT DAN B3', '', 8, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 4, 'top'),
(202, 'Dokumen Lingkungan', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 5, '198'),
(203, 'Izin Lingkungan', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 6, '198'),
(204, 'IPAL', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 7, '199'),
(205, 'Perizinan', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 8, '199'),
(206, 'Alat Ukur', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 9, '199'),
(207, 'Kondisi (apabila ada)', '', 8, 'Berfungsi', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 10, '206'),
(208, 'Catatan debit harian', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 11, '199'),
(209, 'Pengujian Kualitas Air Limbah', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 12, '199'),
(210, 'Periode Pengujian', '', 8, 'Setiap Bulan', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 13, '209'),
(211, 'Hasil Pengujian', '', 8, 'Memenuhi', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 14, '209'),
(212, 'Pelaporan', '', 8, 'Rutin', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 15, '209'),
(213, 'Kebocoran/Rembesan/Tumpahan/Bypass	', '', 8, 'Tidak Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_rs', 16, '199'),
(214, 'Data Kualitas Udara Ambien', '', 8, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 17, '200'),
(215, 'Pengujian Kualitas', '', 8, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rs', 18, '214'),
(216, 'Periode pengujian (per 6 bulan)', '', 8, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 19, '214'),
(217, 'Pemenuhan BM', '', 8, 'Ya', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 20, '214'),
(218, 'Pengelolaan Limbah B3', '', 8, 'Dikelola', 1, '2015-01-04', '2015-01-05', 'Administrator', 'Administrator', 'bap_rs', 21, '201'),
(225, 'Penyimpanan', '', 8, 'Pada TPS Limbah B3', 1, '2015-01-04', '2015-01-05', 'Administrator', 'Administrator', 'bap_rs', 22, '201'),
(226, 'TPS Limbah B3', '', 8, '-', 1, '2015-01-04', '2015-01-05', 'Administrator', 'Administrator', 'bap_rs', 23, '201'),
(227, 'Perizinan TPS', '', 8, 'Ada', 1, '2015-01-04', '2015-01-23', 'Administrator', 'Administrator', 'bap_rs', 24, '226'),
(229, 'Papan nama dan koordinat', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 25, '226'),
(230, 'Simbol dan label', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 26, '226'),
(231, 'Saluran ceceran air limbah', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 27, '226'),
(232, 'Bak penampung ceceran air limbah', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 28, '226'),
(233, 'Kemiringan', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 29, '226'),
(234, 'SOP pengelolaan dan tanggap darurat', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 30, '226'),
(235, 'Log book', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 31, '226'),
(236, 'APAR/Kotak P3K', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 32, '226'),
(237, 'Neraca Limbah B3', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 33, '201'),
(238, 'Manifest', '', 8, 'Ada', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 34, '201'),
(239, 'Pelaporan Neraca dan Manifest', '', 8, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 35, '201'),
(240, 'Limbah Padat Lain', '', 8, '-', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 36, '201'),
(241, 'Pengelolaan', '', 8, 'Dibakar', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rs', 37, '240'),
(242, 'PENGENDALIAN PENCEMARAN UDARA', '', 6, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 3, 'top'),
(243, 'Data Kualitas Udara Ambien', '', 6, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 8, '242'),
(244, 'Pengujian Kualitas', '', 6, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_golf', 9, '243'),
(245, 'Periode pengujian (per 6 bulan)', '', 6, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_golf', 10, '243'),
(246, 'Pemenuhan BM', '', 6, 'Ya', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_golf', 11, '243'),
(247, 'PENGENDALIAN PENCEMARAN UDARA', '', 9, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 3, 'top'),
(248, 'Data Kualitas Udara Ambien', '', 9, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 8, '247'),
(249, 'Pengujian Kualitas', '', 9, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rph', 9, '248'),
(250, 'Periode pengujian (per 6 bulan)', '', 9, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rph', 10, '248'),
(251, 'Pemenuhan BM', '', 9, 'Ya', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rph', 11, '248'),
(252, 'PENGENDALIAN PENCEMARAN UDARA', '', 4, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 3, 'top'),
(253, 'Data Kualitas Udara Ambien', '', 4, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 8, '252'),
(254, 'Pengujian Kualitas', '', 4, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_rm', 9, '253'),
(255, 'Periode pengujian (per 6 bulan)', '', 4, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rm', 10, '253'),
(256, 'Pemenuhan BM', '', 4, 'Ya', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_rm', 11, '253'),
(257, 'DATA UMUM', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 1, 'top'),
(258, 'PENGENDALIAN PENCEMARAN AIR', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 2, 'top'),
(259, 'PENGENDALIAN PENCEMARAN UDARA', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 3, 'top'),
(260, 'PENGELOLAAN LIMBAH PADAT DAN B3', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 4, 'top'),
(261, 'Dokumen Lingkungan', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 5, '257'),
(262, 'Izin Lingkungan', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 6, '257'),
(263, 'IPAL', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 7, '258'),
(264, 'Perizinan', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 8, '258'),
(265, 'Alat Ukur', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 9, '258'),
(266, 'Kondisi (apabila ada)', '', 12, 'Berfungsi', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 10, '265'),
(267, 'Catatan debit harian', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 11, '258'),
(268, 'Pengujian Kualitas Air Limbah', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 12, '258'),
(269, 'Periode Pengujian', '', 12, 'Setiap Bulan', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 13, '268'),
(270, 'Hasil Pengujian', '', 12, 'Memenuhi', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 14, '268'),
(271, 'Pelaporan', '', 12, 'Rutin', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 15, '268'),
(272, 'Kebocoran/Rembesan/Tumpahan/Bypass', '', 12, 'Tidak Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 16, '258'),
(273, 'Data Cerobong', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 17, '259'),
(274, 'Sampling hole', '', 12, 'Ada', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_plb3', 18, '273'),
(275, 'Tangga', '', 12, 'Ada', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_plb3', 19, '273'),
(276, 'Pagar pengaman tangga', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 20, '273'),
(277, 'Lantai kerja', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 21, '273'),
(278, 'Pengujian kualitas emisi', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 22, '259'),
(279, 'Periode pengujian (per 6 bulan)', '', 12, 'Rutin', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 23, '278'),
(280, 'Pemenuhan BME', '', 12, 'Ya', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 24, '278'),
(281, 'Alat pengendali emisi', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 25, '259'),
(282, 'Data Kualitas Udara Ambien', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 26, '259'),
(284, 'Pengujian Kualitas', '', 12, 'Ada', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 27, '282'),
(285, 'Periode pengujian (per 6 bulan)', '', 12, 'Rutin', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_plb3', 28, '282'),
(286, 'Pemenuhan BM', '', 12, 'Ya', 1, '2015-01-04', '2015-01-08', 'Administrator', 'Administrator', 'bap_plb3', 29, '282'),
(287, 'Jenis limbah B3 yang ditimbulkan', '', 12, '-', 1, '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', 'bap_plb3', 30, '260'),
(288, 'Pengelolaan Limbah B3', '', 12, 'Dikelola', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_plb3', 31, '287'),
(297, 'Penyimpanan', '', 12, 'Pada TPS Limbah B3', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 42, '260'),
(298, 'TPS Limbah B3', '', 12, '-', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 43, '260'),
(299, 'Perizinan TPS', '', 12, 'Ada', 1, '2015-01-04', '2015-02-12', 'Administrator', 'Administrator', 'bap_plb3', 44, '298'),
(301, 'Papan nama dan koordinat', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 45, '298'),
(302, 'Simbol dan label', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 46, '298'),
(303, 'Saluran ceceran air limbah', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 47, '298'),
(304, 'Bak penampung ceceran air limbah', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 48, '298'),
(305, 'Kemiringan', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 49, '298'),
(306, 'SOP pengelolaan dan tanggap darurat', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 50, '298'),
(307, 'Log book', '', 12, 'Ada', 1, '2015-01-04', '2015-01-11', 'Administrator', 'Administrator', 'bap_plb3', 51, '298'),
(308, 'APAR', '', 12, 'Ada', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 52, '298'),
(309, 'Neraca Limbah B3', '', 12, 'Ada', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 54, '260'),
(310, 'Manifest', '', 12, 'Ada', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 55, '260'),
(311, 'Pelaporan Neraca dan Manifest', '', 12, 'Rutin', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 56, '260'),
(312, 'Limbah Padat Lain', '', 12, '-', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 57, '260'),
(313, 'Pengelolaan', '', 12, 'Dibakar', 1, '2015-01-04', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 58, '312'),
(317, 'Pengelolaan', '', 7, 'Dibakar', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_sppbe', 12, '90'),
(318, 'DATA UMUM', '', 10, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 1, 'top'),
(319, 'PENGENDALIAN PENCEMARAN AIR', '', 10, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 2, 'top'),
(320, 'PENGENDALIAN PENCEMARAN UDARA', '', 10, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 3, 'top'),
(321, 'PENGELOLAAN LIMBAH PADAT DAN B3', '', 10, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 4, 'top'),
(322, 'Dokumen Lingkungan', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 5, '318'),
(323, 'Izin Lingkungan', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 6, '318'),
(324, 'IPAL', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 7, '319'),
(325, 'Perizinan', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 8, '320'),
(326, 'Alat Ukur', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 9, '319'),
(327, 'Kondisi (apabila ada)', '', 10, 'Berfungsi', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 10, '326'),
(328, 'Catatan debit harian', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 11, '319'),
(329, 'Pengujian Kualitas Air Limbah', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 12, '319'),
(330, 'Periode Pengujian', '', 10, 'Setiap Bulan', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 13, '329'),
(331, 'Hasil Pengujian', '', 10, 'Memenuhi', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 14, '329'),
(332, 'Pelaporan', '', 10, 'Rutin', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 15, '329'),
(333, 'Kebocoran/Rembesan/Tumpahan/Bypass', '', 10, 'Tidak Ada', 1, '2015-01-05', '2015-01-11', 'Administrator', 'Administrator', 'bap_lab', 16, '319'),
(334, 'Data Kualitas Udara Ambien', '', 10, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 17, '320'),
(335, 'Pengujian Kualitas', '', 10, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 18, '334'),
(336, 'Periode pengujian (per 6 bulan)', '', 10, 'Rutin', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 19, '334'),
(337, 'Pemenuhan BM', '', 10, 'Ya', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 20, '334'),
(338, 'Pengelolaan Limbah B3', '', 10, 'Dikelola', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 21, '321'),
(339, 'Penyimpanan', '', 10, 'Pada TPS Limbah B3', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 22, '321'),
(340, 'TPS Limbah B3', '', 10, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_lab', 23, '322'),
(341, 'Perizinan TPS', '', 10, 'Ada', 1, '2015-01-05', '2015-01-23', 'Administrator', 'Administrator', 'bap_lab', 24, '340'),
(343, 'Papan nama dan koordinat', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 25, '340'),
(344, 'Simbol dan label', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 26, '340'),
(345, 'Saluran ceceran air limbah', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 27, '340'),
(346, 'Bak penampung ceceran air limbah', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 28, '340'),
(347, 'Kemiringan', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 29, '340'),
(348, 'SOP pengelolaan dan tanggap darurat', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 30, '340'),
(349, 'Log book', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 31, '340'),
(350, 'APAR/Kotak P3K', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 32, '340'),
(351, 'Neraca Limbah B3', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 33, '321'),
(352, 'Manifest', '', 10, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 34, '321'),
(353, 'Pelaporan Neraca dan Manifest', '', 10, 'Rutin', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 35, '321'),
(354, 'Limbah Padat Lain', '', 10, '-', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 36, '321'),
(355, 'Pengelolaan', '', 10, 'Dibakar', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_lab', 37, '354'),
(356, 'DATA UMUM', '', 11, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 1, 'top'),
(357, 'PENGENDALIAN PENCEMARAN AIR', '', 11, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 2, 'top'),
(358, 'PENGENDALIAN PENCEMARAN UDARA', '', 11, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 3, 'top'),
(359, 'PENGELOLAAN LIMBAH PADAT DAN B3', '', 11, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 4, 'top'),
(360, 'Dokumen Lingkungan', '', 11, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 5, '356'),
(361, 'Izin Lingkungan', '', 11, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 6, '356'),
(362, 'Sarana Pengolahan Air Limbah', '', 11, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 7, '357'),
(363, 'Data Kualitas Udara Ambien', '', 11, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 8, '358'),
(364, 'Pengujian Kualitas', '', 11, 'Ada', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 9, '363'),
(365, 'Periode pengujian (per 6 bulan)', '', 11, 'Rutin', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 10, '363'),
(366, 'Pemenuhan BM', '', 11, 'Ya', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 11, '363'),
(367, 'Pengelolaan Limbah B3', '', 11, 'Dikelola', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 12, '359'),
(368, 'Penyimpanan', '', 11, 'Pada TPS Limbah B3', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 13, '359'),
(369, 'TPS Limbah B3', '', 11, '-', 1, '2015-01-05', '2015-01-05', 'Administrator', 'Administrator', 'bap_tambang', 14, '359'),
(370, 'Perizinan TPS', '', 11, 'Ada', 1, '2015-01-05', '2015-01-23', 'Administrator', 'Administrator', 'bap_tambang', 15, '369'),
(372, 'Papan nama dan koordinat', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 16, '369'),
(373, 'Simbol dan label', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 17, '369'),
(374, 'Saluran ceceran air limbah', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 18, '369'),
(375, 'Bak penampung ceceran air limbah', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 19, '369'),
(376, 'Kemiringan', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 20, '369'),
(377, 'SOP pengelolaan dan tanggap darurat', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 21, '369'),
(378, 'Log book', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 22, '369'),
(379, 'APAR/Kotak P3K', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 23, '369'),
(380, 'Neraca Limbah B3', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 24, '359'),
(381, 'Manifest', '', 11, 'Ada', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 25, '359'),
(382, 'Pelaporan Neraca dan Manifest', '', 11, 'Rutin', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 26, '359'),
(383, 'Limbah Padat Lain', '', 11, '-', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 27, '359'),
(384, 'Pengelolaan', '', 11, 'Dibakar', 1, '2015-01-05', '2015-01-08', 'Administrator', 'Administrator', 'bap_tambang', 28, '383'),
(388, 'LAIN - LAIN', '', 5, '-', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 59, 'top'),
(389, 'Lain - Lain', '', 5, '', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 60, '388'),
(390, 'LAIN - LAIN', '', 6, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_golf', 13, 'top'),
(391, 'Lain - Lain', '', 6, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_golf', 14, '390'),
(392, 'LAIN - LAIN', '', 3, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_hotel', 38, 'top'),
(393, 'Lain - Lain', '', 3, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_hotel', 39, '392'),
(394, 'LAIN - LAIN', '', 1, '-', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 59, 'top'),
(395, 'Lain - Lain', '', 1, '', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 60, '394'),
(396, 'LAIN - LAIN', '', 10, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_lab', 38, 'top'),
(397, 'Lain - Lain', '', 10, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_lab', 39, '396'),
(398, 'LAIN - LAIN', '', 12, '-', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 59, 'top'),
(399, 'Lain - Lain', '', 12, '', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 60, '398'),
(400, 'LAIN - LAIN', '', 11, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_tambang', 29, 'top'),
(401, 'Lain - Lain', '', 11, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_tambang', 30, '400'),
(402, 'LAIN - LAIN', '', 9, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_rph', 13, 'top'),
(403, 'Lain - Lain', '', 9, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_rph', 14, '402'),
(404, 'LAIN - LAIN', '', 4, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_rm', 13, 'top'),
(405, 'Lain - Lain', '', 4, '', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_rm', 14, '404'),
(406, 'LAIN - LAIN', '', 8, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_rs', 38, 'top'),
(407, 'Lain - Lain', '', 8, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_rs', 39, '406'),
(408, 'LAIN - LAIN', '', 7, '-', 1, '2015-01-22', '2015-01-22', 'Administrator', 'Administrator', 'bap_sppbe', 13, 'top'),
(409, 'Lain - Lain', '', 7, '', 1, '2015-01-22', '2015-01-26', 'Administrator', 'Administrator', 'bap_sppbe', 14, '408'),
(410, 'Kotak P3K', '', 5, 'Ada', 1, '2015-01-26', '2015-01-26', 'Administrator', 'Administrator', 'bap_agro', 53, '41'),
(411, 'Kotak P3K', '', 1, 'Ada', 1, '2015-01-26', '2015-01-26', 'Administrator', 'Administrator', 'bap_industri', 53, '182'),
(412, 'Kotak P3K', '', 12, 'Ada', 1, '2015-01-26', '2015-01-26', 'Administrator', 'Administrator', 'bap_plb3', 53, '298');

-- --------------------------------------------------------

--
-- Struktur dari tabel `parameter_lhu`
--

CREATE TABLE `parameter_lhu` (
  `id_parameter_lhu` int(11) NOT NULL,
  `id_usaha_kegiatan` int(11) NOT NULL,
  `id_jenis_industri` varchar(255) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  `nilai_parameter_lhu_internal` varchar(255) NOT NULL,
  `nilai_parameter_lhu_eksternal` varchar(255) NOT NULL,
  `dasar_hukum_internal` text NOT NULL,
  `dasar_hukum_eksternal` text NOT NULL,
  `jenis_lhu` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `parameter_lhu`
--

INSERT INTO `parameter_lhu` (`id_parameter_lhu`, `id_usaha_kegiatan`, `id_jenis_industri`, `ket`, `satuan`, `nilai_parameter_lhu_internal`, `nilai_parameter_lhu_eksternal`, `dasar_hukum_internal`, `dasar_hukum_eksternal`, `jenis_lhu`, `status`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(2, 14, '2', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.5)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(3, 14, '3', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(4, 14, '4', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(5, 14, '1', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(6, 14, '1', 'BOD', 'mg/L', '60', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(7, 14, '1', 'COD', 'mg/L', '150', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(8, 14, '1', 'TSS', 'mg/L', '50', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(9, 14, '1', 'Fenol', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(10, 14, '1', 'Krom', 'mg/L', '1.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(11, 14, '1', 'Amonia', 'mg/L', '8.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(12, 14, '1', 'Sulfida', 'mg/L', '0.3', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(13, 14, '1', 'Minyak & Lemak', 'mg/L', '3.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(14, 14, '1', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(15, 14, '2', 'BOD', 'mg/L', '90', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.5)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(16, 14, '2', 'COD', 'mg/L', '175', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.5)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(17, 14, '2', 'TSS', 'mg/L', '80', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.5)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(18, 14, '5', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(19, 14, '5', 'TSS', 'mg/L', '20', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(20, 14, '5', 'CN', 'mg/L', '0.2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(21, 14, '5', 'Krom Total', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(22, 14, '5', 'Krom Heksavalen', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(23, 14, '5', 'Cu', 'mg/L', '0.6', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(24, 14, '5', 'Zn', 'mg/L', '1.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(25, 14, '5', 'Ni', 'mg/L', '1.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(26, 14, '5', 'Cd', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(27, 14, '5', 'Pb', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(28, 14, '5', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.2)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(29, 14, '3', 'Temperatur', 'mg/L', '38', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(30, 14, '3', 'Zat Padat Terlarut', 'mg/L', '2000', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(31, 14, '3', 'TSS', 'mg/L', '200', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(32, 14, '3', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(33, 14, '3', 'Fe', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(34, 14, '3', 'Mn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(35, 14, '3', 'Ba', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(36, 14, '3', 'Cu', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(37, 14, '3', 'Zn', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(38, 14, '3', 'Krom Heksavalen', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(39, 14, '3', 'Krom Total', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(40, 14, '3', 'Cd', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(41, 14, '3', 'Hg', 'mg/L', '0.002', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(42, 14, '3', 'Pb', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(43, 14, '3', 'Sn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(44, 14, '3', 'As', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(45, 14, '3', 'Se', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(46, 14, '3', 'Ni', 'mg/L', '0.2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(47, 14, '3', 'Co', 'mg/L', '0.4', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(48, 14, '3', 'CN', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(49, 14, '3', 'H2S', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(50, 14, '3', 'F', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(51, 14, '3', 'Cl2', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(52, 14, '3', 'NH3-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(53, 14, '3', 'NO3-N', 'mg/L', '20', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(54, 14, '3', 'NO2-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(55, 14, '3', 'BOD', 'mg/L', '50', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(56, 14, '3', 'COD', 'mg/L', '100', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(57, 14, '3', 'Senyawa Aktif Biru Metilen', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(58, 14, '3', 'Fenol', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(59, 14, '3', 'Minyak Nabati', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(60, 14, '3', 'Minyak Mineral', 'mg/L', '10', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(61, 14, '4', 'Temperatur', 'mg/L', '38', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(62, 14, '4', 'Zat Padat Terlarut', 'mg/L', '2000', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(63, 14, '4', 'TSS', 'mg/L', '200', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(64, 14, '4', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(65, 14, '4', 'Fe', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(66, 14, '4', 'Mn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(67, 14, '4', 'Ba', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(68, 14, '4', 'Cu', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(69, 14, '4', 'Zn', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(70, 14, '4', 'Krom Heksavalen', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(71, 14, '4', 'Krom Total', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(72, 14, '4', 'Cd', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(73, 14, '4', 'Hg', 'mg/L', '0.002', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(74, 14, '4', 'Pb', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(75, 14, '4', 'Sn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(76, 14, '4', 'As', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(77, 14, '4', 'Se', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(78, 14, '4', 'Ni', 'mg/L', '0.2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(79, 14, '4', 'Co', 'mg/L', '0.4', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(80, 14, '4', 'CN', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(81, 14, '4', 'H2S', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(82, 14, '4', 'F', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(83, 14, '4', 'Cl2', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(84, 14, '4', 'NH3-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(85, 14, '4', 'NO3-N', 'mg/L', '20', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(86, 14, '4', 'NO2-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(87, 14, '4', 'BOD', 'mg/L', '50', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(88, 14, '4', 'COD', 'mg/L', '100', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(89, 14, '4', 'Senyawa Aktif Biru Metilen', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(90, 14, '4', 'Fenol', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(91, 14, '4', 'Minyak Nabati', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(92, 14, '4', 'Minyak Mineral', 'mg/L', '10', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(93, 13, '9', '', NULL, '', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(94, 13, '10', '', NULL, '', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(95, 13, '9', 'BOD', 'mg/L', '30', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(96, 13, '9', 'COD', 'mg/L', '50', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(97, 13, '9', 'TSS', 'mg/L', '50', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(98, 13, '9', 'pH', '-', '6.0-9.0', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(99, 13, '10', 'BOD', 'mg/L', '30', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(100, 13, '10', 'COD', 'mg/L', '50', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(101, 13, '10', 'TSS', 'mg/L', '50', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(102, 13, '10', 'pH', 'mg/L', '6.0-9.0', '', 'Keputusan Menteri Negara Lingkungan Hidup \r\nNo. KEP-52/MENLH-LH/10/1995', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(103, 20, '6', '', NULL, '', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(104, 20, '7', '', NULL, '', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(105, 20, '6', 'TSS', 'mg/L', '30', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(106, 20, '6', 'BOD', 'mg/L', '30', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(107, 20, '6', 'COD', 'mg/L', '80', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(108, 20, '6', 'pH', '-', '6.0-9.0', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(109, 20, '6', 'Posfat', 'mg/L', '2', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(110, 20, '7', 'TSS', 'mg/L', '30', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(111, 20, '7', 'BOD', 'mg/L', '30', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(112, 20, '7', 'COD', 'mg/L', '80', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(113, 20, '7', 'pH', '-', '6.0-9.0', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(114, 20, '7', 'Posfat', 'mg/L', '2', '', 'KEP-58/MENLH/10/1995 (lampiran B)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(115, 11, '60', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(116, 11, '60', 'Temperatur', 'mg/L', '38', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(117, 11, '60', 'Zat Padat Terlarut', 'mg/L', '2000', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(118, 11, '60', 'TSS', 'mg/L', '200', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(119, 11, '60', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(120, 11, '60', 'Fe', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(121, 11, '60', 'Mn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(122, 11, '60', 'Ba', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(123, 11, '60', 'Cu', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(124, 11, '60', 'Zn', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(125, 11, '60', 'Krom Heksavalen', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(126, 11, '60', 'Krom Total', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(127, 11, '60', 'Cd', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(128, 11, '60', 'Hg', 'mg/L', '0.002', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(129, 11, '60', 'Pb', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(130, 11, '60', 'Sn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(131, 11, '60', 'As', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(132, 11, '60', 'Se', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(133, 11, '60', 'Ni', 'mg/L', '0.2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(134, 11, '60', 'Co', 'mg/L', '0.4', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(135, 11, '60', 'CN', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(136, 11, '60', 'H2S', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(137, 11, '60', 'F', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(138, 11, '60', 'Cl2', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(139, 11, '60', 'NH3-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(140, 11, '60', 'NO3-N', 'mg/L', '20', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(141, 11, '60', 'NO2-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(142, 11, '60', 'BOD', 'mg/L', '50', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(143, 11, '60', 'COD', 'mg/L', '100', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(144, 11, '60', 'Senyawa Aktif Biru Metilen', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(145, 11, '60', 'Fenol', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(146, 11, '60', 'Minyak Nabati', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(147, 11, '60', 'Minyak Mineral', 'mg/L', '10', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(148, 22, '61', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.20)', '', 'LHU Air Limbah', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(149, 22, '61', 'BOD', 'mg/L', '75', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.20)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(150, 22, '61', 'COD', 'mg/L', '150', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.20)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(151, 22, '61', 'TSS', 'mg/L', '75', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.20)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(152, 22, '61', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran II.20)', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(186, 19, '20', '', NULL, '', '', 'Keputusan Menteri Negara Lingkungan Hidup No. 112 tahun 2003', '', 'LHU Air Limbah', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(187, 19, '20', 'pH', 'mg/L', '6.0-9.0', '', 'Keputusan Menteri Negara Lingkungan Hidup No. 112 tahun 2003', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(188, 19, '20', 'BOD', 'mg/L', '100', '', 'Keputusan Menteri Negara Lingkungan Hidup No. 112 tahun 2003', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(189, 19, '20', 'TSS', 'mg/L', '100', '', 'Keputusan Menteri Negara Lingkungan Hidup No. 112 tahun 2003', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(190, 19, '20', 'Minyak & Lemak', 'mg/L', '10', '', 'Keputusan Menteri Negara Lingkungan Hidup No. 112 tahun 2003', '', 'LHU Air Limbah', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(223, 24, '75', '', NULL, '', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 0, '2015-01-02', '0000-00-00', 'Administrator', ''),
(224, 24, '75', 'Temperatur', 'mg/L', '38', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(225, 24, '75', 'Zat Padat Terlarut', 'mg/L', '2000', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(226, 24, '75', 'TSS', 'mg/L', '200', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(227, 24, '75', 'pH', '-', '6.0-9.0', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(228, 24, '75', 'Fe', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(229, 24, '75', 'Mn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(230, 24, '75', 'Ba', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(231, 24, '75', 'Cu', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(232, 24, '75', 'Zn', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(233, 24, '75', 'Krom Heksavalen', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(234, 24, '75', 'Krom Total', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(235, 24, '75', 'Cd', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(236, 24, '75', 'Hg', 'mg/L', '0.002', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(237, 24, '75', 'Pb', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(238, 24, '75', 'Sn', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(239, 24, '75', 'As', 'mg/L', '0.1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(240, 24, '75', 'Se', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(241, 24, '75', 'Ni', 'mg/L', '0.2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(242, 24, '75', 'Co', 'mg/L', '0.4', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(243, 24, '75', 'CN', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(244, 24, '75', 'H2S', 'mg/L', '0.05', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(245, 24, '75', 'F', 'mg/L', '2', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(246, 24, '75', 'Cl2', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(247, 24, '75', 'NH3-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(248, 24, '75', 'NO3-N', 'mg/L', '20', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(249, 24, '75', 'NO2-N', 'mg/L', '1', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(250, 24, '75', 'BOD', 'mg/L', '50', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(251, 24, '75', 'COD', 'mg/L', '100', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(252, 24, '75', 'Senyawa Aktif Biru Metilen', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(253, 24, '75', 'Fenol', 'mg/L', '0.5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(254, 24, '75', 'Minyak Nabati', 'mg/L', '5', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(255, 24, '75', 'Minyak Mineral', 'mg/L', '10', '', 'SK Gub. Jawa Barat No 6 tahun 1999 (lampiran III)', '', 'LHU Air Limbah', 1, '0000-00-00', '0000-00-00', '', ''),
(256, 14, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(257, 14, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(258, 14, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(259, 14, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(260, 14, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(261, 14, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(262, 14, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(263, 14, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(264, 14, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(265, 14, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(266, 14, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(267, 14, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(268, 14, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(269, 14, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(270, 14, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(271, 14, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(272, 14, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(273, 14, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(274, 14, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(275, 14, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(276, 14, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(277, 14, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(278, 11, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(279, 11, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(280, 11, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(281, 11, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(282, 11, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(283, 11, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(284, 11, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(285, 11, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(286, 11, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(287, 11, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(288, 11, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(289, 11, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(290, 11, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(291, 11, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(292, 11, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(293, 11, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(294, 11, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(295, 11, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(296, 11, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(297, 11, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(298, 11, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(299, 11, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(300, 24, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(301, 24, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(302, 24, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(303, 24, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(304, 24, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(305, 24, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(306, 24, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(307, 24, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(308, 24, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(309, 24, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(310, 24, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(311, 24, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(312, 24, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(313, 24, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(314, 24, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(315, 24, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(316, 24, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(317, 24, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(318, 24, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(319, 24, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(320, 24, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(321, 24, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(322, 21, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(323, 21, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(324, 21, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(325, 21, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(326, 21, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(327, 21, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(328, 21, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(329, 21, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(330, 21, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(331, 21, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(332, 21, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(333, 21, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(334, 21, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(335, 21, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(336, 21, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(337, 21, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(338, 21, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(339, 21, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(340, 21, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator');
INSERT INTO `parameter_lhu` (`id_parameter_lhu`, `id_usaha_kegiatan`, `id_jenis_industri`, `ket`, `satuan`, `nilai_parameter_lhu_internal`, `nilai_parameter_lhu_eksternal`, `dasar_hukum_internal`, `dasar_hukum_eksternal`, `jenis_lhu`, `status`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(341, 21, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(342, 21, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(343, 21, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(344, 22, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(345, 22, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(346, 22, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(347, 22, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(348, 22, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(349, 22, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(350, 22, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(351, 22, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(352, 22, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(353, 22, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(354, 22, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(355, 22, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(356, 22, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(357, 22, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(358, 22, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(359, 22, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(360, 22, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(361, 22, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(362, 22, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(363, 22, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(364, 22, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(365, 22, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(366, 12, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(367, 12, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(368, 12, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(369, 12, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(370, 12, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(371, 12, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(372, 12, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(373, 12, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(374, 12, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(375, 12, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(376, 12, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(377, 12, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(378, 12, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(379, 12, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(380, 12, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(381, 12, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(382, 12, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(383, 12, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(384, 12, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(385, 12, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(386, 12, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(387, 12, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(388, 13, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(389, 13, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(390, 13, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(391, 13, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(392, 13, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(393, 13, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(394, 13, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(395, 13, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(396, 13, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(397, 13, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(398, 13, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(399, 13, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(400, 13, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(401, 13, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(402, 13, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(403, 13, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(404, 13, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(405, 13, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(406, 13, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(407, 13, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(408, 13, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(409, 13, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(410, 15, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(411, 15, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(412, 15, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(413, 15, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(414, 15, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '150', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(415, 15, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(416, 15, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(417, 15, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(418, 15, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(419, 15, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(420, 15, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(421, 15, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(422, 15, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(423, 15, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(424, 15, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(425, 15, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(426, 15, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(427, 15, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(428, 15, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(429, 15, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(430, 15, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(431, 15, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(432, 16, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2016-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(433, 16, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2016-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(434, 16, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2016-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(435, 16, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2016-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(436, 16, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '160', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(437, 16, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(438, 16, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(439, 16, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(440, 16, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(441, 16, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(442, 16, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(443, 16, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(444, 16, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(445, 16, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(446, 16, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(447, 16, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(448, 16, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(449, 16, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(450, 16, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(451, 16, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(452, 16, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(453, 16, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(454, 17, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(455, 17, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(456, 17, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(457, 17, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(458, 17, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '170', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(459, 17, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(460, 17, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(461, 17, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(462, 17, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(463, 17, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(464, 17, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(465, 17, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(466, 17, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(467, 17, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(468, 17, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(469, 17, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(470, 17, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(471, 17, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(472, 17, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(473, 17, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(474, 17, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(475, 17, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(476, 18, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(477, 18, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(478, 18, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(479, 18, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(480, 18, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '170', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(481, 18, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(482, 18, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(483, 18, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(484, 18, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(485, 18, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(486, 18, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(487, 18, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(488, 18, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(489, 18, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(490, 18, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(491, 18, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(492, 18, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(493, 18, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(494, 18, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(495, 18, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(496, 18, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(497, 18, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(498, 19, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(499, 19, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(500, 19, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(501, 19, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(502, 19, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '170', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(503, 19, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(504, 19, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(505, 19, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(506, 19, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(507, 19, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(508, 19, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(509, 19, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(510, 19, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(511, 19, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(512, 19, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(513, 19, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(514, 19, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(515, 19, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(516, 19, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(517, 19, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(518, 19, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(519, 19, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(520, 20, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(521, 20, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(522, 20, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(523, 20, 'Biomassa', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 0, '2017-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(524, 20, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '170', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(525, 20, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran VI', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(526, 20, 'Bahan Bakar Minyak', 'Partikulat', 'mg/m3', '200', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(527, 20, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(528, 20, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '700', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(529, 20, 'Bahan Bakar Minyak', 'Opasitas', '%', '15', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran V', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(530, 20, 'Batubara', 'Partikulat', 'mg/m3', '230', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(531, 20, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(532, 20, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '825', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(533, 20, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran IV', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(534, 20, 'Biomassa', 'Partikulat', 'mg/m3', '300', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(535, 20, 'Biomassa', 'Sulfur Dioksida (SO2)', 'mg/m3', '600', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(536, 20, 'Biomassa', 'Nitrogen Oksida (NO2)', 'mg/m3', '800', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(537, 20, 'Biomassa', 'Hidrogen Klorida (HCl)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(538, 20, 'Biomassa', 'Gas Klorin (Cl2)', 'mg/m3', '5', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(539, 20, 'Biomassa', 'Ammonia (NH3)', 'mg/m3', '1', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(540, 20, 'Biomassa', 'Hidrogen Florida (HF)', 'mg/m3', '8', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(541, 20, 'Biomassa', 'Opasitas', '%', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 07 Tahun 2007 Tanggal 8 Mei 2007 Lampiran I', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(542, 25, 'Batubara', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(543, 25, 'Bahan Bakar Minyak', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(544, 25, 'Gas', '', NULL, '', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 0, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(545, 25, 'Batubara', 'Sulfur Dioksida (SO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(546, 25, 'Batubara', 'Nitrogen Oksida (NO2)', 'mg/m3', '750', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(547, 25, 'Batubara', 'Total Partikulat', 'mg/m3', '100', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(548, 25, 'Batubara', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(549, 25, 'Gas', 'Sulfur Dioksida (SO2)', 'mg/m3', '50', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(550, 25, 'Gas', 'Nitrogen Oksida (NO2)', 'mg/m3', '320', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(551, 25, 'Gas', 'Total Partikulat ', 'mg/m3', '30', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(552, 25, 'Bahan Bakar Minyak', 'Sulfur Dioksida (SO2)', 'mg/m3', '650', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(553, 25, 'Bahan Bakar Minyak', 'Nitrogen Oksida (NO2)', 'mg/m3', '450', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(554, 25, 'Bahan Bakar Minyak', 'Total Partikulat', 'mg/m3', '100', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator'),
(555, 25, 'Bahan Bakar Minyak', 'Opasitas', '%', '20', '', 'Peraturan Menteri Negara Lingkungan Hidup No. 21 Tahun 2008 Tanggal 1 Desember 2008 Lampiran I B', '', 'LHU Udara Emisi', 1, '0000-00-00', '2015-01-02', '', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `instansi` varchar(255) NOT NULL,
  `pangkat_gol` varchar(100) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `tlp` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `instansi`, `pangkat_gol`, `jabatan`, `tlp`, `email`, `alamat`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `user_id`, `avatar`, `status`) VALUES
(1, 'ERICK USMAN', '0', 'test', '-', 'Staf', '085222487748', 'Erickusman@gmail.com', 'Desa Cipinang RT02/11 Kecamatan Cimaung', '2014-10-22', '2018-10-22', 'administrator', 'Administrator', 2, '', 1),
(2, 'Sholehuddin', '09803804980934', 'bplh kabupaten bandung', 'III B', 'Staf', '09808308403', '', 'Jln Ciwidey No 12', '0000-00-00', '0000-00-00', 'Administrator', '', 0, '', 1),
(3, 'ENDANG WIDAYANTI', '196704231998032003', 'bplh kabupaten bandung', 'IV A', 'Kepala Bidang', '082117288217', 'endawid98@yahoo.com', 'Soreang KM 17', '0000-00-00', '2018-10-22', 'Administrator', 'Administrator', 0, '', 1),
(4, 'INDAH', '09384839479387', 'bplh kabupaten bandung', 'IV A', 'Staf', '09809830984', 'email@gmail.com', 'Jln Pasir Koja No 21', '0000-00-00', '2018-10-22', 'Administrator', 'Administrator', 0, '', 1),
(5, 'DESTI KUSMAWATI', '0', 'bplh kabupaten bandung', '-', 'PHL', '089526461110', 'destikusmawati92@gmail.com', 'Kp.Lembang Desa Kiangroke ', '0000-00-00', '2018-10-22', 'Administrator', 'Administrator', 0, '', 1),
(6, 'MUHAMMAD SYAFEI', '196804242005011009', 'bplh kabupaten bandung', 'III/d', 'Kasi Pengendalian Pencemaran Lingkungan', '081320928924', 'msyafei.88.ms@gmail.com', 'Jl. Raya Soreang Km.17 Soreang Kabupaten Bandung', '0000-00-00', '0000-00-00', 'Administrator', '', 0, '', 1),
(7, 'RINA HAUROUL MARDIYAH', '197508032010012001', 'bplh kabupaten bandung', 'III C', 'Pelaksana', '081222715234', 'rinahmardiyah@gmail.com', 'Jl. Raya Soreang Km 17 Soreang Kabupaten Bandung', '0000-00-00', '0000-00-00', 'Administrator', '', 0, '', 1),
(8, 'LENA ADHANI', '0', 'bplh kabupaten bandung', 'II', 'Pelaksana', '082115434823', 'lenaadhani01@gmail.com', 'Soreang', '0000-00-00', '0000-00-00', 'Administrator', '', 0, '', 1),
(9, 'ENDANG KURNIAWAN', '196802042006041009', 'bplh kabupaten bandung', 'III d', 'Pelaksana', '085320423019', '1.oneendang@gmail.com', 'Jl Raya Terusan Kopo Km 14 No. 320-Soreang', '0000-00-00', '2018-10-22', 'Administrator', 'Administrator', 0, '', 1),
(10, 'IRWAN RISNAWAN, AMD', '197408242007011006', 'bplh kabupaten bandung', 'III a', 'Pelaksana', '082216487758', 'Irwanrisnawan@gmail.com', 'Banjaran', '0000-00-00', '2018-11-05', 'Administrator', 'Administrator', 0, '', 1),
(11, 'Intan Kartini, SH', '198709212011012003', 'bplh kabupaten bandung', 'III a', 'Pelaksana', '0225206188', 'vera.ariesa@yahoo.com', 'Jl. Cisirung No.48 Kel.Pasawahan Kec. Dayeuh Kolot Kab. Bandung', '0000-00-00', '0000-00-00', 'Administrator', '', 0, '', 1),
(12, 'INTAN KARTINI, SH', '198709212011012003', 'bplh kabupaten bandung', 'III a', 'Pelaksana', '085321232111', 'intankartini@gmail.com', 'Gading Tutuka 1 Soreang', '0000-00-00', '0000-00-00', 'Administrator', '', 0, '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pencemaran_air`
--

CREATE TABLE `pencemaran_air` (
  `id_penc_air` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `ambil_air_tanah` float DEFAULT NULL,
  `ambil_air_tanah_izin` tinyint(1) DEFAULT NULL,
  `ambil_air_permukaan` float DEFAULT NULL,
  `ambil_air_permukaan_izin` tinyint(1) DEFAULT NULL,
  `ambil_air_pdam` float DEFAULT NULL,
  `ambil_air_lain` float DEFAULT NULL,
  `penggunaan_air` varchar(50) DEFAULT NULL,
  `limb_sumber` varchar(255) DEFAULT NULL,
  `bdn_terima` varchar(255) DEFAULT NULL,
  `sarana_olah_limbah` tinyint(1) DEFAULT NULL,
  `sarana_jenis` varchar(255) DEFAULT NULL,
  `sarana_kapasitas` varchar(255) DEFAULT NULL,
  `ipal` tinyint(1) DEFAULT NULL,
  `ipal_sistem` varchar(100) DEFAULT NULL,
  `ipal_unit` varchar(255) DEFAULT NULL,
  `ipal_kapasitas` varchar(255) DEFAULT NULL,
  `izin` tinyint(1) DEFAULT NULL,
  `izin_no` varchar(255) DEFAULT NULL,
  `izin_tgl` date DEFAULT NULL,
  `izin_debit` varchar(255) DEFAULT NULL,
  `koord_outlet_derajat_s` varchar(255) DEFAULT NULL,
  `koord_outlet_jam_s` varchar(255) DEFAULT NULL,
  `koord_outlet_menit_s` varchar(255) DEFAULT NULL,
  `koord_outlet_derajat_e` varchar(255) DEFAULT NULL,
  `koord_outlet_jam_e` varchar(255) DEFAULT NULL,
  `koord_outlet_menit_e` varchar(255) DEFAULT NULL,
  `au` tinyint(1) DEFAULT NULL,
  `au_jenis` varchar(100) DEFAULT NULL,
  `au_ukuran` varchar(50) DEFAULT NULL,
  `au_kondisi` varchar(50) DEFAULT NULL,
  `cat_deb_hari` tinyint(1) DEFAULT NULL,
  `daur_ulang` tinyint(1) DEFAULT NULL,
  `daur_ulang_debit` varchar(255) DEFAULT NULL,
  `uji_limbah` tinyint(1) DEFAULT NULL,
  `uji_period` varchar(100) DEFAULT NULL,
  `uji_lab` varchar(100) DEFAULT NULL,
  `uji_hasil` varchar(50) DEFAULT NULL,
  `uji_tidak_bulan` varchar(100) DEFAULT NULL,
  `uji_tidak_param` varchar(255) DEFAULT NULL,
  `uji_lapor` varchar(50) DEFAULT NULL,
  `bocor` tinyint(1) DEFAULT NULL,
  `bocor_lokasi` varchar(255) DEFAULT NULL,
  `lain_lain` text,
  `sarana_koord_derajat_s` varchar(255) DEFAULT NULL,
  `sarana_koord_jam_s` varchar(255) DEFAULT NULL,
  `sarana_koord_menit_s` varchar(255) DEFAULT NULL,
  `sarana_koord_derajat_e` varchar(255) DEFAULT NULL,
  `sarana_koord_jam_e` varchar(255) DEFAULT NULL,
  `sarana_koord_menit_e` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pencemaran_air`
--

INSERT INTO `pencemaran_air` (`id_penc_air`, `id_bap`, `ambil_air_tanah`, `ambil_air_tanah_izin`, `ambil_air_permukaan`, `ambil_air_permukaan_izin`, `ambil_air_pdam`, `ambil_air_lain`, `penggunaan_air`, `limb_sumber`, `bdn_terima`, `sarana_olah_limbah`, `sarana_jenis`, `sarana_kapasitas`, `ipal`, `ipal_sistem`, `ipal_unit`, `ipal_kapasitas`, `izin`, `izin_no`, `izin_tgl`, `izin_debit`, `koord_outlet_derajat_s`, `koord_outlet_jam_s`, `koord_outlet_menit_s`, `koord_outlet_derajat_e`, `koord_outlet_jam_e`, `koord_outlet_menit_e`, `au`, `au_jenis`, `au_ukuran`, `au_kondisi`, `cat_deb_hari`, `daur_ulang`, `daur_ulang_debit`, `uji_limbah`, `uji_period`, `uji_lab`, `uji_hasil`, `uji_tidak_bulan`, `uji_tidak_param`, `uji_lapor`, `bocor`, `bocor_lokasi`, `lain_lain`, `sarana_koord_derajat_s`, `sarana_koord_jam_s`, `sarana_koord_menit_s`, `sarana_koord_derajat_e`, `sarana_koord_jam_e`, `sarana_koord_menit_e`) VALUES
(3, 3, 10, 1, 12, 0, 10, 10, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 1, 'F-B-K', 'Jenis Unit 1', '200', 0, NULL, NULL, '1212', '1', '2', '3', '4', '5', '6', 0, NULL, NULL, '-', 1, 0, '12', 1, 'Setiap Bulan', 'Parahita', 'Tidak Memenuhi Baku Mutu', 'januari', 'rahasia', 'Rutin', 0, '', 'tidak ada', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 21, 0, 0, 0, 0, 0, 0, NULL, '', 'Ciliwung', NULL, NULL, NULL, 1, 'F', '', '123', 1, '12', '2016-12-07', '121', '', '', '', '', '', '', 1, 'Kumulatif', '', 'Berfungsi', 1, 1, '2222', 1, 'Setiap Bulan', 'BPLH', 'Memenuhi', '', '', 'Rutin', 1, 'qweqe', '', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 25, 100, 1, 120, 1, 200, 200, NULL, 'Pabrik', 'Citarum', NULL, NULL, NULL, 1, 'F-B-K', '', '22', 1, '12', '2016-12-12', '100', '12', '2', '3', '4', '5', '6', 1, 'V-notch', '12', 'Berfungsi', 1, 1, '1200', 1, 'Setiap Bulan', 'BPLH', 'Memenuhi', '-', '-', 'Rutin', 0, '', 'lain-lain pencemaran air', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 29, 12, 1, 12, 0, 12, 12, NULL, 'Pabrik', 'Citarum', 1, 'qwe', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6'),
(17, 30, 12, 1, 22, 0, 21, 12, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '111', '1', '2', '3', '4', '5', '6', 1, 'Digital', '12', 'Tidak Berfungsi', 1, 1, '100', 1, 'Setiap Bulan', 'Parahita', 'Tidak Memenuhi Baku Mutu', 'desember', 'co2', 'Tidak Rutin', 1, 'di atap', 'lain lain kedua', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 33, 12, 1, 0, 0, 200, 213, NULL, '', 'Ciliwung', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 37, 12, 1, 12, 1, 12, 12, NULL, '', 'Ciliwung', 1, 'asdad', 'qw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6'),
(28, 39, 12, 1, 12, 1, 12, 12, NULL, 'Pabrik', 'Ciliwung', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '100', '1', '2', '', '4', '5', '6', 0, NULL, NULL, '-', 1, 1, '10', 0, '-', NULL, '-', '-', '-', 'Rutin', 0, '', 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 45, 100, 0, 10, 0, 50, 12, NULL, 'Pabrik', 'Citarum', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '120', '1', '2', '3', '4', '5', '6', 1, 'V-notch', '12', 'Tidak Berfungsi', 1, 1, '22', 1, 'Setiap Bulan', 'BPLH', 'Memenuhi', '', '', 'Rutin', 0, '', 'rahasia kedua', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '11', '1', '', '3', '4', '5', '6', 1, 'V-notch', '12', 'Berfungsi', 0, 1, '100', 1, 'Setiap Bulan', 'Parahita', 'Memenuhi', '-', '-', 'Rutin', 0, '', 'rahasisa kedua', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 47, 1, 0, 2, 1, 23, 32, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '11', '1', '2', '3', '4', '5', '6', 0, NULL, NULL, '-', 0, 1, '12', 1, 'setiap 3 bulan', 'Parahita', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 50, 12, 1, 12, NULL, 21, 21, '212', 'Pabrik', 'Citarum', 1, 'rahasia', '231', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6', '5', '4', '3', '21', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 51, 21, 0, 21, NULL, 121, 2222, 'qeewe', 'Pabrik', 'Citarum', 1, 'rahasia', 'rahasia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 52, 12, 0, 21, NULL, 21, 12, '12', 'Pabrik', 'Citarum', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', ''),
(39, 53, 12, 1, 22, NULL, 22, 22, 'sdsad', 'Pabrik', 'Citarum', 1, 'asdasda', '2222', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6'),
(40, 54, 12, 0, 21, 0, 22, 221, NULL, 'Pabrik', 'Ciliwung', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', ''),
(41, 56, 12, 0, 22, 0, 200, 300, NULL, 'Pabrik', 'Ciliwung', 1, 'aja', '234234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6'),
(42, 58, 12, NULL, 21, NULL, 12, 21, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '22', '1', '2', '3', '1', '2', '3', 0, NULL, NULL, '-', 1, 0, '12', 0, '-', NULL, '-', '-', '-', 'Rutin', 0, '', 'asdasdad', NULL, NULL, NULL, NULL, NULL, NULL),
(51, 57, 12, NULL, 21, NULL, 12, 21, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '21', '1', '2', '3', '4', '5', '6', 0, NULL, NULL, '-', 0, 0, '222', 0, '-', NULL, '-', '-', '-', 'Rutin', 0, '', 'lain air', NULL, NULL, NULL, NULL, NULL, NULL),
(52, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '200', '1', '2', '3', '4', '5', '6', 0, NULL, NULL, '-', 0, 0, '200', 0, '-', NULL, '-', '-', '-', 'Rutin', 1, 'skdf', '', NULL, NULL, NULL, NULL, NULL, NULL),
(53, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '200', '1', '2', '3', '4', '5', '6', 0, NULL, NULL, '-', 0, 0, '23', 0, '-', NULL, '-', '-', '-', 'Tidak Rutin', 0, '', 'lain air', NULL, NULL, NULL, NULL, NULL, NULL),
(54, 61, 12, 1, 21, 1, 21, 12, NULL, 'Pabrik', 'Citarum', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'lain air', NULL, NULL, NULL, NULL, NULL, NULL),
(55, 62, 1312, 1, 13, 1, 13, 132, NULL, 'Pabrik', 'Citarum', 1, '', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '214', '54', '651', '6', '64', '646', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asdasd', NULL, NULL, NULL, NULL, NULL, NULL),
(56, 63, 12, NULL, 87, NULL, 6, 8, NULL, 'Pabrik', 'Citarum', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '12', '4', '5', '3', '6', '5', '7', 0, NULL, NULL, '-', 1, 1, '2', 1, 'Setiap Bulan', 'BPLH', 'Memenuhi', '', '', 'Rutin', 0, '', 'lain air', NULL, NULL, NULL, NULL, NULL, NULL),
(57, 64, 22, NULL, 31, NULL, 23, 22, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL, NULL, '22', '5', '6', '4', '3', '52', '11', 1, 'lain jenis alat ukur', '22', 'Tidak Berfungsi', 1, 0, '22', 1, '3 bulan sekali', 'BPLH', 'Memenuhi', 'januari', 'co2', 'Tidak Rutin', 0, '', 'lain air', NULL, NULL, NULL, NULL, NULL, NULL),
(58, 67, 12, 0, 21, 0, 222, 222, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '22', '2', '2', '2', '2', '2', '2', 0, NULL, NULL, '-', 0, 0, '22', 0, '-', NULL, '-', '-', '-', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 67, 12, 0, 21, 0, 222, 222, NULL, 'Pabrik', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '22', '2', '2', '2', '2', '2', '2', 0, NULL, NULL, '-', 0, 0, '22', 0, '-', NULL, '-', '-', '-', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(62, 70, 0, NULL, 0, NULL, 0, 0, NULL, '', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '', '', '', '', '', '', '', 0, NULL, NULL, '-', 0, 0, '23', 0, '-', NULL, '-', '', '', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(63, 72, 12, 1, 21, 1, 12, 12, NULL, 'Pabrik', 'Citarum', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 74, 0, NULL, 0, NULL, 0, 0, NULL, '', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '12', '', '', '', '', '', '', 0, NULL, NULL, '-', 0, 0, '111', 0, '-', NULL, '-', '', '', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(67, 75, 0, NULL, 0, NULL, 0, 0, NULL, '', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '12', '', '', '', '', '', '', 0, NULL, NULL, '-', 0, 0, '111', 0, '-', NULL, '-', '', '', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(68, 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Citarum', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '123', '', '', '', '', '', '', 0, NULL, NULL, '-', 0, 0, '323', 0, '-', NULL, '-', '', '', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(69, 79, 0, NULL, 0, NULL, 0, 0, NULL, '', 'Ciliwung', NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, '', '', '', '', '', '', '', 0, NULL, NULL, '-', 0, 0, '543', 0, '-', NULL, '-', '', '', 'Tidak Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(70, 81, 0, 1, 0, 1, 0, 0, NULL, 'Pabrik', 'Ciliwung', 1, 'asdasd', '3435', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '876', '876', '33', '7', '876', '78', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adasd', NULL, NULL, NULL, NULL, NULL, NULL),
(71, 83, 0, 0, 0, 0, 0, 0, NULL, '', 'Ciliwung', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', ''),
(72, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dyeing, Finishing, Washing', 'Citarum', NULL, NULL, NULL, 1, 'pre treatment', '', '1000', 1, '666/kep.019/IPBL/BPMP/2013', '2013-11-25', '100', '', '', '', '', '', '', 1, 'FLOWMETTER ', '', 'Berfungsi', 1, 0, '200', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(74, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dyeing', 'Sungai Cibalekambang', NULL, NULL, NULL, 1, 'F-K-B', 'Aerasi, Flokulasi, Koagulasi, Sedimentasi', '3000', 0, NULL, NULL, '327', '07', '02', '44,5', '107', '44', '36,4', 1, 'V-notch', '5 inci', 'Tidak Berfungsi', 1, 0, '327', 1, 'Setiap Bulan', 'DLH - Air', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(76, 91, 0, NULL, 0, NULL, 0, 0, NULL, '', 'Sungai Cipalasari', NULL, NULL, NULL, 1, 'F-B-K', 'Ekualisasi, Flokulasi, Koagulasi, Sedimentasi', '1500 m3/hari', 1, '666/Kep.007/IPBL/BPMP', '2010-03-18', '100', '06', '57', '50,94', '107', '37', '50,94', 1, 'Digital', '8', 'Berfungsi', 1, 0, '100', 1, 'Setiap Bulan', 'BBPK', 'Tidak Memenuhi Baku Mutu', 'Juli, Agustus 2017 dan Februari 2018', 'Sulfida', 'Rutin', 0, '-', '-', NULL, NULL, NULL, NULL, NULL, NULL),
(77, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Tidak Ada Badan Air Penerima', NULL, NULL, NULL, 1, 'recycle', '', '100 m3', 0, NULL, NULL, '0', '', '', '', '', '', '', 1, 'flowmetter', '0', 'Berfungsi', 1, 1, '79 m3', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(79, 96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dyeing', 'Sungai Cimande', NULL, NULL, NULL, 1, 'F-B-K', 'Aerasi, Ekualisasi, Filtrasi, Flokulasi, Koagulasi, Sedimentasi', '400 ', 1, '666/Kep.022/ IPBL/BPMP', '2013-12-06', '225', '07', '02', '006', '107', '45', '02,7', 1, 'Kumulatif', '', 'Berfungsi', 1, 0, '225', 1, 'Setiap Bulan', 'DLH - Air', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(80, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(81, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(82, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(83, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(84, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(85, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(86, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(87, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(88, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(89, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(90, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(91, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL),
(92, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Finishing', 'Sungai Cisangkuy', NULL, NULL, NULL, 1, 'B', 'Ekualisasi, Filtrasi, Koagulasi, Sedimentasi', '100', 1, '666/kep.032/IPBL/BPMP-2009', '2009-10-19', '3,9', '06', '59', '11', '107', '33', '05', 1, 'Flowmetter', '', 'Berfungsi', 1, 1, '2', 1, 'Setiap Bulan', 'DLH', 'Memenuhi', '', '', 'Rutin', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pencemaran_pb3`
--

CREATE TABLE `pencemaran_pb3` (
  `id_penc_pb3` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `b3_penyimpanan` varchar(50) DEFAULT NULL,
  `b3_uji` varchar(50) DEFAULT NULL,
  `b3_uji_jenis` varchar(50) DEFAULT NULL,
  `b3_uji_param` varchar(255) DEFAULT NULL,
  `b3_uji_lab` varchar(255) DEFAULT NULL,
  `b3_uji_waktu` varchar(255) DEFAULT NULL,
  `b3_neraca` tinyint(1) DEFAULT NULL,
  `b3_manifest` tinyint(1) DEFAULT NULL,
  `b3_lapor_nm` varchar(50) DEFAULT NULL,
  `padat_jenis` varchar(255) DEFAULT NULL,
  `padat_jumlah` varchar(255) DEFAULT NULL,
  `padat_sarana_tong` int(11) DEFAULT NULL,
  `padat_sarana_tps` int(11) DEFAULT NULL,
  `padat_kelola` varchar(255) DEFAULT NULL,
  `lain_lain` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pencemaran_pb3`
--

INSERT INTO `pencemaran_pb3` (`id_penc_pb3`, `id_bap`, `b3_penyimpanan`, `b3_uji`, `b3_uji_jenis`, `b3_uji_param`, `b3_uji_lab`, `b3_uji_waktu`, `b3_neraca`, `b3_manifest`, `b3_lapor_nm`, `padat_jenis`, `padat_jumlah`, `padat_sarana_tong`, `padat_sarana_tps`, `padat_kelola`, `lain_lain`) VALUES
(3, 3, 'Pada TPS Limbah B3', '1', 'Total Logam', 'rahasia ah', 'BPLH', '20 jam', 1, 1, 'Tidak Melaporkan', 'limbah, padat, lain', '0.001, 0.002, 0.003', 0, 0, 'Diangkut oleh Dinas Pertasih', 'tidak ada lagi'),
(4, 21, 'Pada TPS Limbah B3', '1', '', '', 'BPLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(15, 25, 'Pada TPS Limbah B3', '1', 'TCLP, Total Logam', 'qwe', 'BPLH', 'qwe', 1, 1, 'Rutin', 'accu, bekas', '1, 2', 0, 0, 'Dikelola oleh Warga Sekitar', 'asdasd'),
(17, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kalium, sianida', '12, 0.000001', 2, 3, 'Dikelola oleh Warga Sekitar', 'rahasia terakhir'),
(21, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kaluman, dead vomit', 'jasad, seringai', 12, 21, 'Dikelola oleh Warga Sekitar', 'raja terakhir'),
(22, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd, asd, asd', 'asd, asd, asd', 0, 0, 'Dibakar', '0'),
(24, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd, asd', 'asd, ads', 12, 22222, 'Dikelola oleh Warga Sekitar', 'asdasd'),
(25, 40, 'Pada TPS Limbah B3', '1', 'TCLP, Total Logam', 'asdasd', 'BPLH', 'asdasd', 1, 1, 'Rutin', 'asd', 'asd', 0, 0, 'Dijual', NULL),
(29, 45, 'Pada TPS Limbah B3', '0', 'TCLP, Total Logam', 'asd', 'BPLH', 'rahasia', 1, 1, 'Tidak Rutin', 'co2, karbon', '2, 3', 0, 0, 'Dibakar', 'rahasia terakhir'),
(30, 46, 'Pada TPS Limbah B3', 'Sudah', 'TCLP, Total Logam', 'rahasia', 'BPLH', 'rahasia', 0, 1, 'Rutin', 'limbah, padat', '12, 4488', 0, 0, 'hal yang lain', NULL),
(32, 47, 'Pada TPS Limbah B3', '0', 'TCLP, Total Logam', 'rem', 'BPLH', 'malam', 1, 1, 'Rutin', 'racun tikus', '2', 0, 0, 'Dibakar', 'rahasia terakhir'),
(33, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'piasu, pedang', '1, 3', 12, 21, 'Diangkut oleh Pihak Ketiga', 'rahasia pertama'),
(34, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jenis, jumlah', 'jumlah, jenis', 1, 2, 'Dijual', 'rahasia terakhir'),
(35, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', 'asdad', 12, 21, 'Diangkut oleh Pihak Ketiga', 'sadasd'),
(36, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jenis', 'jumlah', 11, 22, 'Dibakar', 'lain lain pb3'),
(37, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jenis', 'jumlah', 1, 1, 'Diangkut oleh Pihak Ketiga', 'lain pb3'),
(38, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jenis', 'jumlah', 1, 2, 'Dibakar', 'lain terkhir'),
(39, 58, 'Pada TPS Limbah B3', 'Sudah', 'TCLP, Total Logam', '12313', 'Parahita', 'sadas', 1, 1, 'Rutin', 'asd, dsa', 'asd, dsa', 21, 11, 'Dibakar', 'asdasd'),
(41, 57, 'Pada TPS Limbah B3', '0', 'TCLP, Total Logam', 'qweq', 'BPLH', 'dkhskjd', 1, 1, 'Rutin', 'asdhjg', '175612', 1, 2, 'Dibakar', 'lain pb3'),
(42, 59, 'Pada TPS Limbah B3', NULL, NULL, NULL, NULL, NULL, 1, 1, 'Rutin', 'asdjahgsd, hghj', '8767, 987', NULL, NULL, 'Dibakar', 'lain pb3'),
(43, 60, 'Di luar TPS Limbah B3', '0', 'TCLP, Total Logam', 'asdkash', 'BPLH', 'asdkj', 1, 1, 'Rutin', 'asdk, ljshdlj', '9778, 09876', 6, 8, 'Diangkut oleh Pihak Ketiga', 'lain terakhir'),
(44, 61, 'Pada TPS Limbah B3', NULL, NULL, NULL, NULL, NULL, 1, 1, 'Tidak Rutin', 'asdjhgasdh', '273649', 2, 222, 'Dibakar', 'lain pb3'),
(46, 62, 'Di luar TPS Limbah B3', NULL, NULL, NULL, NULL, NULL, 0, 0, 'Tidak Rutin', 'dasd, asdasd', '23123, 3323', 22, 333, 'Dibakar', '0'),
(47, 63, 'Pada TPS Limbah B3', 'Sudah', 'TCLP, Total Logam', 'asdgasdh', 'BPLH', 'sdhasdj', 1, 1, 'Rutin', 'asdajhd', '636', NULL, NULL, 'Dijual', '0'),
(48, 64, 'Pada TPS Limbah B3', '1', 'TCLP, Total Logam', 'asdag', 'BPLH', 'asdhjn', 0, 0, 'Rutin', 'jenis', '123123', 0, 0, 'pengelolaan yang lain', NULL),
(49, 72, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asdasd', 'dasdasd', 121212, 112, 'Dibakar', 'asdasd'),
(50, 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jahsdhg, asd, asd', 'kjsjdjkj, asd, asdsd', 11, 12, 'Dibakar', 'sdasd'),
(51, 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jahsdhg, asd, rwerwerw', 'kjsjdjkj, asdsd, 345345345', 11, 11, 'Dibakar', 'sdasd'),
(53, 93, 'Pada TPS Limbah B3', '0', '', '', '', '', 1, 1, 'Rutin', 'Sludge Ipal, Oli bekas, Lampu TL bekas, Kemasan bekas B3', '0,13, Tentatif, Tentatif, Tentatif', 0, 0, 'Diangkut oleh Pihak Ketiga', NULL),
(55, 87, 'Pada TPS Limbah B3', '1', '', '', '', '', 1, 1, 'Tidak Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(56, 90, 'Pada TPS Limbah B3', '1', 'TCLP', '', '', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(57, 90, 'Pada TPS Limbah B3', '1', 'TCLP', '', '', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(58, 90, 'Pada TPS Limbah B3', '1', 'TCLP', '', '', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(59, 90, 'Pada TPS Limbah B3', '1', 'TCLP', 'logam', 'Lab Kimia Fisik UNPAD', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(60, 85, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', 'sampah domestik', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(61, 85, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', 'sampah domestik', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(62, 85, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', 'sampah domestik', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(63, 85, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', 'sampah domestik', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(64, 89, 'Pada TPS Limbah B3', '1', '', '', '', '', 1, 1, 'Tidak Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(65, 89, 'Pada TPS Limbah B3', '1', '', '', '', '', 1, 1, 'Tidak Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(66, 91, 'Pada TPS Limbah B3', '1', '', '', '', '', 1, 1, 'Tidak Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(67, 91, 'Pada TPS Limbah B3', '1', '', '', '', '', 1, 1, 'Tidak Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(68, 88, 'Pada TPS Limbah B3', '1', '', '', '', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', ''),
(69, 94, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(70, 96, 'Pada TPS Limbah B3', '0', '', '', '', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Pihak Ketiga', NULL),
(71, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(72, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(73, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(74, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(75, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(76, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(77, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(78, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(79, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(80, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(81, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(82, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(83, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(84, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(85, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(86, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(87, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(88, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(89, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(90, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(91, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(92, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(93, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(94, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(95, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(96, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(97, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(98, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(99, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(100, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(101, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(102, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(103, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(104, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(105, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(106, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(107, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(108, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(109, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL),
(110, 97, 'Pada TPS Limbah B3', '0', '', '', 'DLH', '', 1, 1, 'Rutin', '', '', 0, 0, 'Diangkut oleh Dinas Pertasih', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pencemaran_udara`
--

CREATE TABLE `pencemaran_udara` (
  `id_penc_udara` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `pelaporan_ua_ue` varchar(50) DEFAULT NULL,
  `lain_lain` text,
  `sumber_emisi` varchar(255) NOT NULL,
  `jml_cerobong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pencemaran_udara`
--

INSERT INTO `pencemaran_udara` (`id_penc_udara`, `id_bap`, `pelaporan_ua_ue`, `lain_lain`, `sumber_emisi`, `jml_cerobong`) VALUES
(3, 3, 'Tidak Rutin', 'rahasia', 'rahasia sumber emisi', 2),
(4, 21, 'Rutin', '', 'Boiler', 0),
(5, 25, 'Rutin', 'lain-lain pencemaran udara', 'lainnya', 1),
(7, 29, 'Rutin', 'lain lain pb', '', NULL),
(10, 33, 'Rutin', 'rahasia lainnya', '', NULL),
(11, 37, 'Tidak Rutin', 'aadasdasd', '', NULL),
(13, 39, 'Tidak Rutin', 'asd', '', NULL),
(14, 40, 'Rutin', 'adasd', 'Boiler', 1),
(15, 30, 'Tidak Rutin', '', 'Boiler', 1),
(16, 45, 'Rutin', 'rahasia ke3', 'Boiler', 1),
(17, 46, 'Tidak Rutin', 'rahasi ake3', 'boiler yang lain', 1),
(18, 47, 'Rutin', 'rahasia ke3', 'Boiler', 1),
(19, 50, 'Rutin', 'rahasia kedua', '', NULL),
(20, 51, 'Tidak Rutin', 'rahasia keberapa ini', '', NULL),
(21, 52, 'Rutin', 'sdasd', '', NULL),
(22, 53, 'Rutin', 'lain lain udara', '', NULL),
(23, 54, 'Rutin', 'lain udara', '', NULL),
(24, 56, 'Rutin', 'lain udara', '', NULL),
(25, 58, 'Rutin', 'aasdd', '', NULL),
(27, 57, 'Tidak Melaporkan', 'lain udara', '', NULL),
(28, 59, 'Rutin', 'lain udara', '', NULL),
(29, 60, 'Tidak Rutin', 'lain udara', '', NULL),
(30, 61, 'Rutin', 'lain udara', '', NULL),
(31, 62, 'Rutin', 'lain udara', '', NULL),
(32, 63, 'Tidak Rutin', 'lain udara', '', NULL),
(34, 64, 'Tidak Melaporkan', 'lain udara', '', NULL),
(36, 72, 'Rutin', 'sjsdf', '', NULL),
(37, 75, 'Rutin', 'asdasd', '', NULL),
(38, 77, 'Rutin', 'sd', '', NULL),
(39, 81, 'Rutin', 'asdad', '', NULL),
(40, 85, 'Rutin', '', 'Boiler', 2),
(41, 92, 'Rutin', '', 'Boiler', 1),
(42, 92, 'Rutin', '', 'Boiler', 1),
(43, 93, 'Rutin', '', 'Boiler', 1),
(45, 89, 'Rutin', '', 'Boiler', 1),
(46, 88, 'Rutin', '', 'Boiler', 1),
(47, 87, 'Tidak Rutin', '', 'Boiler', 3),
(48, 90, 'Rutin', '', 'Serbuk Kayu Bakar', 1),
(49, 91, 'Rutin', '', 'Boiler', 2),
(51, 96, 'Rutin', '', 'Boiler', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengangkut_limbah`
--

CREATE TABLE `pengangkut_limbah` (
  `id_industri` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `badan_hukum` varchar(255) NOT NULL,
  `nama_industri` varchar(255) NOT NULL,
  `nomer_klh` varchar(1000) NOT NULL,
  `tlp` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `no_truck` text NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengangkut_limbah`
--

INSERT INTO `pengangkut_limbah` (`id_industri`, `id_kecamatan`, `id_kelurahan`, `badan_hukum`, `nama_industri`, `nomer_klh`, `tlp`, `email`, `alamat`, `fax`, `no_truck`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `status`) VALUES
(1, 4, 259, 'CV.', 'Maju Terus', '0987897987894', '0999808098', 'majuterus@gmail.com', 'Jln Ancol No 21', '0228976982', 'D 4001 XZ, D 0976 AA, Z 0978 GH', '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', NULL),
(2, 7, 234, 'PT.', 'RUT', '98734984793', '0229080980', 'rut@gmail.com', 'Jln Bojong No 23', '0227686872', 'D 3489 HF, D 2378 DS, D 9832 GI', '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengolah_limbah`
--

CREATE TABLE `pengolah_limbah` (
  `id_industri` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_kelurahan` int(11) NOT NULL,
  `badan_hukum` varchar(255) NOT NULL,
  `nama_industri` varchar(255) NOT NULL,
  `nomer_klh` varchar(1000) NOT NULL,
  `tlp` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengolah_limbah`
--

INSERT INTO `pengolah_limbah` (`id_industri`, `id_kecamatan`, `id_kelurahan`, `badan_hukum`, `nama_industri`, `nomer_klh`, `tlp`, `email`, `alamat`, `fax`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`, `status`) VALUES
(1, 19, 144, 'CV.', 'Seloka', '09809840808', '022987979', 'seloka@gmail.com', 'Jln Sari No 34', '02268786898', '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', NULL),
(2, 6, 240, 'CV.', 'Selasih', '980948304803', '02289378798', 'selasih@gmail.com', 'Jln Banjar No 24', '02263648799', '2015-01-04', '2015-01-04', 'Administrator', 'Administrator', NULL),
(3, 4, 260, '0', 'PT. Jaya Lestari Indonesia', '0', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(4, 5, 251, '0', 'PT. Mitrajaya Tri', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(5, 5, 251, '0', 'CV. Sonia Persada', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(6, 6, 242, '0', 'PT. Grasyela Nur Ramadhani', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(7, 5, 251, '0', 'PT. Sinerga', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(8, 6, 242, '0', 'PT. Sumber Usaha Gemilang', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(9, 5, 252, '0', 'PT. Jobs Colouring', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(10, 6, 241, '0', 'PT. Khalda Alam Utama', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(11, 4, 259, '0', 'PT. Indomas Putra Jaya', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(12, 6, 240, '0', 'PT. PPLI', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(13, 6, 240, '0', 'CV. HIBK', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(14, 5, 251, '0', 'PT. Kurnia Jaya', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(15, 6, 240, '0', 'PT. WGI', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(16, 6, 240, '0', 'PT. Tenang Jaya Sejahtera', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL),
(17, 6, 241, '0', 'PT. Samudera Jaya Indonesia', '', '0', 'email@gmail.com', 'Alamat', '0', '2018-10-22', '2018-10-22', 'Administrator', 'Administrator', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas_bap`
--

CREATE TABLE `petugas_bap` (
  `id_petugas_bap` int(11) NOT NULL,
  `id_bap` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `ket` varchar(255) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date DEFAULT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas_bap`
--

INSERT INTO `petugas_bap` (`id_petugas_bap`, `id_bap`, `id_pegawai`, `ket`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(8, 3, 3, '', '2016-11-12', NULL, 'Administrator', NULL),
(9, 3, 4, '', '2016-11-12', NULL, 'Administrator', NULL),
(20, 25, 4, '', '2016-12-17', NULL, 'Administrator', NULL),
(21, 29, 2, '', '2016-12-18', NULL, 'Administrator', NULL),
(25, 39, 1, '', '2016-12-18', NULL, 'Administrator', NULL),
(32, 40, 4, '', '2016-12-18', NULL, 'Administrator', NULL),
(33, 45, 3, '', '2016-12-18', NULL, 'Administrator', NULL),
(34, 45, 4, '', '2016-12-18', NULL, 'Administrator', NULL),
(35, 45, 2, '', '2016-12-18', NULL, 'Administrator', NULL),
(36, 46, 4, '', '2016-12-18', NULL, 'Administrator', NULL),
(37, 47, 2, '', '2016-12-18', NULL, 'Administrator', NULL),
(38, 50, 4, '', '2016-12-18', NULL, 'Administrator', NULL),
(39, 52, 4, '', '2016-12-20', NULL, 'Administrator', NULL),
(40, 52, 2, '', '2016-12-20', NULL, 'Administrator', NULL),
(42, 53, 2, '', '2016-12-20', NULL, 'Administrator', NULL),
(44, 54, 4, '', '2016-12-21', NULL, 'Administrator', NULL),
(45, 55, 2, '', '2016-12-21', NULL, 'Administrator', NULL),
(48, 56, 2, '', '2016-12-21', NULL, 'Administrator', NULL),
(49, 58, 2, '', '2016-12-21', NULL, 'Administrator', NULL),
(50, 57, 3, '', '2016-12-21', NULL, 'Administrator', NULL),
(51, 57, 1, '', '2016-12-21', NULL, 'Administrator', NULL),
(52, 57, 4, '', '2016-12-21', NULL, 'Administrator', NULL),
(53, 57, 2, '', '2016-12-21', NULL, 'Administrator', NULL),
(54, 59, 4, '', '2016-12-22', NULL, 'Administrator', NULL),
(56, 60, 2, '', '2016-12-22', NULL, 'Administrator', NULL),
(57, 61, 4, '', '2016-12-22', NULL, 'Administrator', NULL),
(59, 62, 2, '', '2016-12-22', NULL, 'Administrator', NULL),
(61, 63, 2, 'update', '2016-12-22', NULL, 'Administrator', NULL),
(62, 64, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(63, 64, 4, '', '2016-12-22', NULL, 'Administrator', NULL),
(64, 66, 1, '', '2016-12-22', NULL, 'Administrator', NULL),
(68, 70, 1, '', '2016-12-22', NULL, 'Administrator', NULL),
(70, 73, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(71, 74, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(72, 75, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(73, 76, 2, '', '2016-12-22', NULL, 'Administrator', NULL),
(74, 77, 2, '', '2016-12-22', NULL, 'Administrator', NULL),
(75, 78, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(76, 79, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(77, 80, 4, '', '2016-12-22', NULL, 'Administrator', NULL),
(78, 81, 4, '', '2016-12-22', NULL, 'Administrator', NULL),
(79, 82, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(80, 83, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(81, 84, 3, '', '2016-12-22', NULL, 'Administrator', NULL),
(82, 85, 5, '', '2018-10-22', NULL, 'Administrator', NULL),
(83, 87, 8, '', '2018-10-22', NULL, 'Administrator', NULL),
(90, 88, 5, '', '2018-10-22', NULL, 'Administrator', NULL),
(91, 94, 5, '', '2018-11-01', NULL, 'Administrator', NULL),
(98, 96, 1, '', '2018-11-05', NULL, 'Administrator', NULL),
(100, 93, 1, '', '2018-11-06', NULL, 'Administrator', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekap_manifest`
--

CREATE TABLE `rekap_manifest` (
  `id_rekap_manifest` int(11) NOT NULL,
  `id_penghasil_limbah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jenis_limbah` varchar(1000) NOT NULL,
  `jml_dokumen` int(11) NOT NULL,
  `nama_berkas` text NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rekap_manifest`
--

INSERT INTO `rekap_manifest` (`id_rekap_manifest`, `id_penghasil_limbah`, `tanggal`, `jenis_limbah`, `jml_dokumen`, `nama_berkas`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(1, 1, '2016-12-11', 'B3 kadaluarsa', 3, '', '2016-12-11', '2016-12-11', 'Administrator', 'Administrator'),
(4, 29, '2018-04-19', 'Bekas kemasan terkontaminasi limbah B3', 1, '', '2018-11-05', '2018-11-05', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'superadmin', 0),
(2, 'admin', 0),
(3, 'manager', 1),
(4, 'operator_bap', 0),
(5, 'operator_lhu', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_teguran_bap`
--

CREATE TABLE `surat_teguran_bap` (
  `id_surat_teguran_bap` int(11) NOT NULL,
  `id_bap` int(11) DEFAULT NULL,
  `deskripsi` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `status_close` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_teguran_lhu`
--

CREATE TABLE `surat_teguran_lhu` (
  `id_surat_teguran_lhu` int(11) NOT NULL,
  `id_laporan_hasil_uji` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `status_close` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tps_b3`
--

CREATE TABLE `tps_b3` (
  `id_tps_b3` int(11) NOT NULL,
  `id_penc_pb3` int(11) NOT NULL,
  `izin` tinyint(1) DEFAULT NULL,
  `izin_no` varchar(255) DEFAULT NULL,
  `izin_tgl` date DEFAULT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `lama_spn` varchar(255) DEFAULT NULL,
  `lama_spn_lumpur` varchar(100) DEFAULT NULL,
  `lama_spn_batu_bara` varchar(100) DEFAULT NULL,
  `lama_spn_pelumas` varchar(100) DEFAULT NULL,
  `lama_spn_aki_bekas` varchar(100) DEFAULT NULL,
  `lama_spn_ltl` varchar(100) DEFAULT NULL,
  `lama_spn_medis` varchar(100) DEFAULT NULL,
  `lama_spn_kmsn_b3` varchar(100) DEFAULT NULL,
  `lama_spn_majun_b3` varchar(100) DEFAULT NULL,
  `lama_spn_limbah_elektro` varchar(100) DEFAULT NULL,
  `lama_spn_limbah_kimia` varchar(100) DEFAULT NULL,
  `lama_spn_lain_nama` varchar(100) DEFAULT NULL,
  `lama_spn_lain_ket` varchar(100) DEFAULT NULL,
  `lama_spn_lain_nama2` varchar(100) DEFAULT NULL,
  `lama_spn_lain_ket2` varchar(100) DEFAULT NULL,
  `lama_spn_lain_nama3` varchar(100) DEFAULT NULL,
  `lama_spn_lain_ket3` varchar(100) DEFAULT NULL,
  `lama_spn_lain_nama4` varchar(100) DEFAULT NULL,
  `lama_spn_lain_ket4` varchar(100) DEFAULT NULL,
  `lama_spn_lain_nama5` varchar(100) DEFAULT NULL,
  `lama_spn_lain_ket5` varchar(100) DEFAULT NULL,
  `koord_derajat_s` varchar(255) DEFAULT NULL,
  `koord_jam_s` varchar(255) DEFAULT NULL,
  `koord_menit_s` varchar(255) DEFAULT NULL,
  `koord_derajat_e` varchar(255) DEFAULT NULL,
  `koord_jam_e` varchar(255) DEFAULT NULL,
  `koord_menit_e` varchar(255) DEFAULT NULL,
  `ukuran` varchar(50) DEFAULT NULL,
  `ppn_nama_koord` tinyint(1) DEFAULT NULL,
  `simbol_label` tinyint(1) DEFAULT NULL,
  `saluran_cecer` tinyint(1) DEFAULT NULL,
  `bak_cecer` tinyint(1) DEFAULT NULL,
  `kemiringan` tinyint(1) DEFAULT NULL,
  `sop_darurat` tinyint(1) DEFAULT NULL,
  `log_book` tinyint(1) DEFAULT NULL,
  `apar_p3k` tinyint(1) DEFAULT NULL,
  `p3k` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tps_b3`
--

INSERT INTO `tps_b3` (`id_tps_b3`, `id_penc_pb3`, `izin`, `izin_no`, `izin_tgl`, `jenis`, `lama_spn`, `lama_spn_lumpur`, `lama_spn_batu_bara`, `lama_spn_pelumas`, `lama_spn_aki_bekas`, `lama_spn_ltl`, `lama_spn_medis`, `lama_spn_kmsn_b3`, `lama_spn_majun_b3`, `lama_spn_limbah_elektro`, `lama_spn_limbah_kimia`, `lama_spn_lain_nama`, `lama_spn_lain_ket`, `lama_spn_lain_nama2`, `lama_spn_lain_ket2`, `lama_spn_lain_nama3`, `lama_spn_lain_ket3`, `lama_spn_lain_nama4`, `lama_spn_lain_ket4`, `lama_spn_lain_nama5`, `lama_spn_lain_ket5`, `koord_derajat_s`, `koord_jam_s`, `koord_menit_s`, `koord_derajat_e`, `koord_jam_e`, `koord_menit_e`, `ukuran`, `ppn_nama_koord`, `simbol_label`, `saluran_cecer`, `bak_cecer`, `kemiringan`, `sop_darurat`, `log_book`, `apar_p3k`, `p3k`) VALUES
(9, 3, 1, '12/12/12', '2016-11-12', 'rahasia', 'a:2:{s:12:\"jenis_limbah\";a:3:{i:0;s:10:\"Accu Bekas\";i:1;s:14:\"Limbah B3 cair\";i:2;s:15:\"Limbah Padat B3\";}s:3:\"tps\";a:3:{i:0;s:1:\"3\";i:1;s:0:\"\";i:2;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '10', 1, 1, 1, 0, 1, 1, 1, 0, 0),
(10, 3, 1, '21/21/21', '2016-11-21', 'rahasia satu lagi', 'a:2:{s:12:\"jenis_limbah\";a:3:{i:0;s:10:\"Accu Bekas\";i:1;s:14:\"Limbah B3 cair\";i:2;s:15:\"Limbah Padat B3\";}s:3:\"tps\";a:3:{i:0;s:0:\"\";i:1;s:1:\"6\";i:2;s:1:\"8\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7', '8', '9', '0', '10', '12', '12', 1, 1, 1, 1, 0, 1, 1, 1, 0),
(11, 3, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:3:{i:0;s:10:\"Accu Bekas\";i:1;s:14:\"Limbah B3 cair\";i:2;s:15:\"Limbah Padat B3\";}s:3:\"tps\";a:3:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 3, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:3:{i:0;s:10:\"Accu Bekas\";i:1;s:14:\"Limbah B3 cair\";i:2;s:15:\"Limbah Padat B3\";}s:3:\"tps\";a:3:{i:0;s:0:\"\";i:1;s:0:\"\";i:2;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 4, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 4, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 4, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 4, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 15, 1, '121212', '2016-12-17', 'beracun', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:1:\"1\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '8', 0, 1, 1, 1, 1, 1, 1, 1, 1),
(86, 15, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 15, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 15, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 25, 1, '1111', '2016-12-18', 'beracun', 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:13:\"B3 kadaluarsa\";i:1;s:14:\"Limbah B3 cair\";}s:3:\"tps\";a:2:{i:0;s:2:\"12\";i:1;s:2:\"21\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '12', 1, 1, 1, 0, 0, 0, 0, 0, 1),
(98, 25, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:13:\"B3 kadaluarsa\";i:1;s:14:\"Limbah B3 cair\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 25, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:13:\"B3 kadaluarsa\";i:1;s:14:\"Limbah B3 cair\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 25, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:13:\"B3 kadaluarsa\";i:1;s:14:\"Limbah B3 cair\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 29, 1, '1212', '2016-12-11', 'mematikan', 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:2:\"12\";i:1;s:2:\"21\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '7', 1, 1, 0, 0, 1, 1, 0, 0, 1),
(110, 29, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 29, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 29, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 30, 1, '1221', '2016-12-04', 'rusak', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:1:\"2\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '7', 1, 1, 1, 1, 1, 1, 0, 0, 0),
(114, 30, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 30, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 30, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 32, 1, '32131', '2016-12-04', 'bebau', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:1:\"9\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '7', 1, 1, 0, 0, 0, 0, 1, 1, 0),
(122, 32, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 32, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 32, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 39, 1, '131231', '2016-12-14', 'asdasd', 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:2:\"22\";i:1;s:2:\"33\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '2', '1', '3', '12', 1, 1, 1, 1, 1, 0, 0, 0, 0),
(126, 39, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 39, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 39, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:10:\"Accu Bekas\";i:1;s:13:\"B3 kadaluarsa\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 41, 1, '111', '2016-12-22', '', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:2:\"12\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '7', 1, 1, 1, 1, 1, 1, 1, 1, 0),
(138, 41, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 41, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, 41, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, 42, 1, '1313', '2016-12-22', 'beracun', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:1:\"2\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '6', '7', '8', 1, 1, 1, 1, 0, 0, 0, 0, 1),
(142, 42, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 42, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 42, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 43, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 43, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(147, 43, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(148, 43, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 44, 1, '12122', '2016-12-22', 'asdasd', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:2:\"12\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '7', 1, 1, 1, 1, 1, 1, 0, 0, 0),
(150, 44, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 44, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(152, 44, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 46, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, 46, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 46, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, 46, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, 47, 1, '12313', '2016-12-15', 'asdad', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:3:\"sad\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '5', 1, 1, 1, 1, 0, 1, 1, 1, 0),
(166, 47, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(167, 47, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, 47, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, 48, 1, '12313', '2016-12-14', 'sasd', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:5:\"12232\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '2', '3', '4', '5', '6', '7', 1, 1, 1, 1, 0, 0, 0, 0, 1),
(170, 48, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(171, 48, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, 48, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:10:\"Accu Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, 55, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(186, 55, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(187, 55, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(188, 55, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(189, 56, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(190, 56, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(191, 56, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(192, 56, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(193, 57, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(194, 57, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(195, 58, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(196, 58, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(197, 57, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(198, 58, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(199, 57, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(200, 58, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(205, 60, 1, '658.31/07/III/BPMP', '2012-03-16', 'fly ash', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:2:\"90\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06', '58', '14', '107', '36', '9', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(206, 60, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(207, 60, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(208, 60, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(209, 61, 1, '658.31/07/III/BPMP', '2012-03-16', 'fly ash', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:2:\"90\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06', '58', '14', '107', '36', '9', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(210, 61, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(211, 61, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(212, 61, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(213, 62, 1, '658.31/07/III/BPMP', '2012-03-16', 'fly ash', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:2:\"90\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06', '58', '14', '107', '36', '9', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(214, 62, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(215, 62, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, 62, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, 63, 1, '658.31/07/III/BPMP', '2012-03-16', 'fly ash', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:2:\"90\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06', '58', '14', '107', '36', '9', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(218, 63, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, 63, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, 63, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:25:\"Limbah Batubara - Fly Ash\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, 64, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, 64, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, 64, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, 64, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, 65, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, 65, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, 65, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, 65, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(233, 66, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(234, 66, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(235, 66, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(236, 66, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(237, 67, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(239, 67, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(241, 67, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(243, 67, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(245, 59, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(246, 59, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(247, 59, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(248, 59, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(249, 68, 1, '658.31/13/II/BPMP', '2012-04-20', '2', 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:7:\"Fly Ash\";i:1;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:2:{i:0;s:3:\"90 \";i:1;s:2:\"90\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06', '58', '33.8', '107', '36', '32.2', '', 1, 1, 1, 1, 1, 1, 1, 1, NULL),
(250, 68, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:7:\"Fly Ash\";i:1;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(251, 68, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:7:\"Fly Ash\";i:1;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(252, 68, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:2:{i:0;s:7:\"Fly Ash\";i:1;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(253, 69, 1, '658.31/34/XII/BPMP', '2013-12-10', 'sludge IPAL', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:1:{i:0;s:2:\"90\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '06', '59', '13', '107', '33', '14', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(254, 69, 1, '658.31/34/XII/BPMP', '2013-12-10', 'oli bekas', 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8', '', '', '9', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(255, 69, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(256, 69, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";a:1:{i:0;s:20:\"Minyak Pelumas Bekas\";}s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(269, 71, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(270, 71, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(271, 71, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(272, 71, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(273, 72, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(274, 72, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(275, 72, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(276, 72, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(277, 73, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(278, 73, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(279, 73, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(280, 73, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(281, 74, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(282, 74, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(283, 74, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(284, 74, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(285, 75, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(286, 75, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(287, 75, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(288, 75, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(289, 76, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(290, 76, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(291, 76, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(292, 77, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(293, 76, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(294, 77, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(295, 77, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tps_b3` (`id_tps_b3`, `id_penc_pb3`, `izin`, `izin_no`, `izin_tgl`, `jenis`, `lama_spn`, `lama_spn_lumpur`, `lama_spn_batu_bara`, `lama_spn_pelumas`, `lama_spn_aki_bekas`, `lama_spn_ltl`, `lama_spn_medis`, `lama_spn_kmsn_b3`, `lama_spn_majun_b3`, `lama_spn_limbah_elektro`, `lama_spn_limbah_kimia`, `lama_spn_lain_nama`, `lama_spn_lain_ket`, `lama_spn_lain_nama2`, `lama_spn_lain_ket2`, `lama_spn_lain_nama3`, `lama_spn_lain_ket3`, `lama_spn_lain_nama4`, `lama_spn_lain_ket4`, `lama_spn_lain_nama5`, `lama_spn_lain_ket5`, `koord_derajat_s`, `koord_jam_s`, `koord_menit_s`, `koord_derajat_e`, `koord_jam_e`, `koord_menit_e`, `ukuran`, `ppn_nama_koord`, `simbol_label`, `saluran_cecer`, `bak_cecer`, `kemiringan`, `sop_darurat`, `log_book`, `apar_p3k`, `p3k`) VALUES
(296, 77, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(297, 78, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(298, 78, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 78, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(300, 78, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(301, 79, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(302, 79, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(303, 80, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(304, 82, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(305, 79, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(306, 81, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(307, 82, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(308, 80, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(309, 79, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(310, 80, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(311, 81, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(312, 82, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(313, 80, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(314, 81, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(315, 82, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(316, 81, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(317, 83, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(318, 83, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(319, 83, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(320, 83, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(321, 84, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(322, 84, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(323, 84, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(324, 84, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 85, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(326, 85, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(327, 85, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(328, 85, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(329, 86, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(330, 86, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(331, 86, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(332, 86, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(333, 87, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(334, 87, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(335, 87, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(336, 87, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(337, 88, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(338, 88, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(339, 88, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(340, 88, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(341, 89, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(342, 89, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(343, 89, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(344, 89, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(345, 90, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(346, 90, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(347, 90, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(348, 90, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(349, 91, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(350, 91, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(351, 91, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(352, 91, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(353, 92, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(354, 92, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(355, 92, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(356, 92, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(357, 93, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(358, 93, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(359, 93, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, 93, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(361, 94, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(362, 94, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(363, 94, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(364, 95, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(365, 94, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(366, 95, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(367, 95, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(368, 95, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(369, 96, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(370, 96, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(371, 96, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(372, 96, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 97, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(374, 97, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(375, 97, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(376, 97, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(377, 98, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(378, 99, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(379, 98, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(380, 99, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(381, 98, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(382, 99, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(383, 98, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(384, 99, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(385, 100, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(386, 100, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(387, 100, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(388, 100, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(389, 101, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(390, 101, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(391, 101, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(392, 101, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(393, 102, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(394, 102, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(395, 102, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(396, 102, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(397, 103, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(398, 103, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(399, 103, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(400, 103, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(401, 104, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(402, 104, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(403, 104, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(404, 104, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, 105, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(406, 105, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(407, 105, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(408, 105, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(409, 106, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(410, 106, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(411, 106, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(412, 106, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(413, 107, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(414, 107, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(415, 108, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(416, 107, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(417, 108, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(418, 108, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(419, 107, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(420, 108, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(421, 109, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(422, 109, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(423, 109, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(424, 109, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(425, 110, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, 1),
(426, 110, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(427, 110, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(428, 110, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";s:0:\"\";s:3:\"tps\";a:1:{i:0;s:0:\"\";}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(433, 70, 0, NULL, NULL, '', 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 0, 1, 1, 1, 1, 1, 1, 1, 1),
(434, 70, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(435, 70, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(436, 70, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(441, 53, 1, '658.31/58/XII/BPMP', '2012-12-06', 'Fly ash/Bottom ash', 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 1, 0, 1, 1),
(442, 53, 1, '', '1970-01-01', '', 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0),
(443, 53, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(444, 53, 3, NULL, NULL, NULL, 'a:2:{s:12:\"jenis_limbah\";b:0;s:3:\"tps\";b:0;}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tps_limbah`
--

CREATE TABLE `tps_limbah` (
  `id_tps_limbah` int(11) NOT NULL,
  `id_izin_tps` int(11) NOT NULL,
  `jenis_limbah_b3` varchar(255) NOT NULL,
  `lama_penyimpanan` int(11) NOT NULL,
  `luas_tps` decimal(8,2) NOT NULL,
  `derajat_s` varchar(255) NOT NULL,
  `jam_s` varchar(255) NOT NULL,
  `menit_s` varchar(255) NOT NULL,
  `derajat_e` varchar(255) NOT NULL,
  `jam_e` varchar(255) NOT NULL,
  `menit_e` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `uji_ambien`
--

CREATE TABLE `uji_ambien` (
  `id_uji_ambien` int(11) NOT NULL,
  `id_penc_udara` int(11) NOT NULL,
  `lokasi` varchar(50) DEFAULT NULL,
  `uji_kualitas` tinyint(1) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `lab` varchar(100) DEFAULT NULL,
  `bm_pemenuhan` tinyint(1) DEFAULT NULL,
  `bm_param` varchar(100) DEFAULT NULL,
  `bm_period` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `uji_ambien`
--

INSERT INTO `uji_ambien` (`id_uji_ambien`, `id_penc_udara`, `lokasi`, `uji_kualitas`, `period`, `lab`, `bm_pemenuhan`, `bm_param`, `bm_period`) VALUES
(7, 3, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(8, 3, 'Site', 0, 'Tidak Rutin', 'Parahita', 1, '-', '-'),
(9, 3, 'Downwind', 1, 'Rutin', 'BPLH', 0, 'rahasia lagi', 'lagi lagi rahasia'),
(10, 4, 'Upwind', 1, 'Rutin', '', 0, 'qwe', 'ewq'),
(11, 4, 'Site', 1, 'Rutin', '', 0, 'qwe', 'ewq'),
(12, 4, 'Downwind', 1, 'Rutin', '', 0, 'qee', 'ewq'),
(13, 5, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(14, 5, 'Site', 1, 'Tidak Rutin', 'Parahita', 1, '-', '-'),
(15, 5, 'Downwind', 1, 'Rutin', 'Parahita', 1, '-', '-'),
(19, 7, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(20, 7, 'Site', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(21, 7, 'Downwind', 1, 'Rutin', 'Parahita', 0, '', ''),
(28, 10, 'Upwind', 1, 'Rutin', 'BPLH', 1, '', ''),
(29, 10, 'Site', 1, 'Rutin', 'BPLH', 1, '', ''),
(30, 10, 'Downwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(31, 11, 'Upwind', 1, 'Rutin', 'BPLH', 0, 'asd', 'asd'),
(32, 11, 'Site', 1, 'Tidak Rutin', 'BPLH', 1, '', ''),
(33, 11, 'Downwind', 0, 'Rutin', 'Parahita', 0, 'asd', 'asdasd'),
(37, 13, 'Upwind', 0, 'Tidak Rutin', 'Parahita', 0, 'sasd', 'kjh'),
(38, 13, 'Site', 0, 'Tidak Rutin', 'Parahita', 0, 'jh', 'kjhkjh'),
(39, 13, 'Downwind', 0, 'Tidak Rutin', 'BPLH', 0, 'jh', 'kjh'),
(40, 14, 'Upwind', 0, 'Tidak Rutin', 'BPLH', 0, 'asd', 'asd'),
(41, 14, 'Site', 0, 'Tidak Rutin', 'BPLH', 0, 'asd', 'asd'),
(42, 14, 'Downwind', 0, 'Tidak Rutin', 'BPLH', 0, 'sd', 'asd'),
(43, 15, 'Upwind', 1, 'Rutin', 'BPLH', 0, 'asd', 'sad'),
(44, 15, 'Site', 1, 'Rutin', 'BPLH', 0, 'sdasd', 'dasd'),
(45, 15, 'Downwind', 1, 'Rutin', 'BPLH', 0, 'sadas', 'dsd'),
(46, 16, 'Upwind', 1, 'Rutin', 'BPLH', 1, '', ''),
(47, 16, 'Site', 1, 'Rutin', 'Parahita', 1, '', ''),
(48, 16, 'Downwind', 1, 'Rutin', 'BPLH', 0, 'kesatu', 'kedua'),
(49, 17, 'Upwind', 1, 'Tidak Rutin', 'BPLH', 1, '-', '-'),
(50, 17, 'Site', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(51, 17, 'Downwind', 1, 'Rutin', 'Parahita', 0, 'asdasd', 'sdasd'),
(52, 18, 'Upwind', 1, 'Rutin', 'BPLH', 1, '', ''),
(53, 18, 'Site', 1, 'Rutin', 'BPLH', 1, '', ''),
(54, 18, 'Downwind', 1, 'Tidak Rutin', 'Parahita', 0, 'asd', 'xc'),
(55, 19, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(56, 19, 'Site', 1, 'Rutin', 'Parahita', 1, '-', '-'),
(57, 19, 'Downwind', 1, 'Rutin', 'BPLH', 0, 'asdasd', 'asdad'),
(58, 20, 'Upwind', 0, 'Tidak Rutin', 'BPLH', 0, 'asd', 'wqe'),
(59, 20, 'Site', 0, 'Tidak Rutin', 'BPLH', 0, 'dsa', 'ewq'),
(60, 20, 'Downwind', 0, 'Tidak Rutin', 'BPLH', 1, '', ''),
(61, 21, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(62, 21, 'Site', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(63, 21, 'Downwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(64, 22, 'Upwind', 0, 'Rutin', 'BPLH', 1, '', ''),
(65, 22, 'Site', 0, 'Rutin', 'BPLH', 1, '', ''),
(66, 22, 'Downwind', 0, 'Tidak Rutin', 'Parahita', 0, 'asd', 'asd'),
(67, 23, 'Upwind', 1, 'Rutin', 'BPLH', 0, 'asd', 'asd'),
(68, 23, 'Site', 1, 'Rutin', 'BPLH', 0, 'asd', 'asdasd'),
(69, 23, 'Downwind', 1, 'Tidak Rutin', 'BPLH', 1, '-', '-'),
(70, 24, 'Upwind', 0, 'Rutin', 'BPLH', 1, '', ''),
(71, 24, 'Site', 0, 'Rutin', 'BPLH', 1, '', ''),
(72, 24, 'Downwind', 0, 'Rutin', 'BPLH', 0, 'asd', 'sadasd'),
(73, 25, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(74, 25, 'Site', 1, 'Rutin', 'Parahita', 1, '-', '-'),
(75, 25, 'Downwind', 1, 'Rutin', 'BPLH', 0, '1qwe', 'sdsd'),
(79, 27, 'Upwind', 1, 'Rutin', 'BPLH', 0, 'ad', 'djsd'),
(80, 27, 'Site', 1, 'Rutin', 'BPLH', 1, '', ''),
(81, 27, 'Downwind', 1, 'Rutin', 'BPLH', 1, '', ''),
(82, 28, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(83, 28, 'Site', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(84, 28, 'Downwind', 1, 'Rutin', 'Parahita', 0, 'adkj', 'kha'),
(85, 29, 'Upwind', 1, 'Tidak Rutin', 'Parahita', 1, '', ''),
(86, 29, 'Site', 0, 'Tidak Rutin', 'BPLH', 1, '', ''),
(87, 29, 'Downwind', 0, 'Rutin', 'Parahita', 0, 'asd', 'dsadas'),
(88, 30, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(89, 30, 'Site', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(90, 30, 'Downwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(91, 31, 'Upwind', 1, 'Rutin', 'BPLH', 0, 'asdhg', '87y'),
(92, 31, 'Site', 1, 'Rutin', 'BPLH', 0, 'g', 'hjnhb'),
(93, 31, 'Downwind', 1, 'Rutin', 'BPLH', 0, 'kgk', ' nm'),
(94, 32, 'Upwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(95, 32, 'Site', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(96, 32, 'Downwind', 1, 'Rutin', 'BPLH', 1, '-', '-'),
(100, 34, 'Upwind', 0, 'Rutin', 'BPLH', 0, 'asdasd', 'asdasdasd'),
(101, 34, 'Site', 1, 'Tidak Rutin', 'BPLH', 1, '', ''),
(102, 34, 'Downwind', 0, 'Rutin', 'Parahita', 1, '', ''),
(112, 36, 'Upwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(113, 36, 'Site', 1, 'Rutin', 'Parahita', 1, '', ''),
(114, 36, 'Downwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(118, 37, 'Upwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(119, 37, 'Site', 1, 'Rutin', 'Parahita', 1, '', ''),
(120, 37, 'Downwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(124, 38, 'Upwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(125, 38, 'Site', 1, 'Rutin', 'Parahita', 1, '', ''),
(126, 38, 'Downwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(130, 39, 'Upwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(131, 39, 'Site', 1, 'Rutin', 'Parahita', 1, '', ''),
(132, 39, 'Downwind', 1, 'Rutin', 'Parahita', 1, '', ''),
(133, 40, 'Upwind', 1, 'Rutin', 'PDAM', 0, '-', '-'),
(134, 40, 'Site', 1, 'Rutin', 'PDAM', 0, '-', '-'),
(135, 40, 'Downwind', 1, 'Rutin', '', 0, '-', '-'),
(136, 41, 'Upwind', 1, 'Rutin', '', 0, '-', '-'),
(137, 41, 'Site', 1, 'Rutin', '', 0, '-', '-'),
(138, 41, 'Downwind', 1, 'Rutin', '', 0, '-', '-'),
(139, 42, 'Upwind', 1, 'Rutin', '', 0, '-', '-'),
(140, 42, 'Site', 1, 'Rutin', '', 0, '-', '-'),
(141, 42, 'Downwind', 1, 'Rutin', '', 0, '-', '-'),
(142, 43, 'Upwind', 1, 'Rutin', 'DLH - Air', 1, '-', '-'),
(143, 43, 'Site', 1, 'Rutin', 'DLH - Air', 1, '-', '-'),
(144, 43, 'Downwind', 1, 'Rutin', 'DLH - Air', 1, '-', '-'),
(148, 45, 'Upwind', 1, 'Rutin', 'PPSDAL', 1, '', ''),
(149, 45, 'Site', 0, 'Rutin', 'Puslitbang', 1, '', ''),
(150, 45, 'Downwind', 1, 'Rutin', 'PPSDAL', 1, '', ''),
(154, 47, 'Upwind', 1, 'Rutin', 'PPSDAL', 1, '-', '-'),
(155, 47, 'Site', 1, 'Rutin', 'PPSDAL', 1, '-', '-'),
(156, 47, 'Downwind', 1, 'Rutin', 'PPSDAL', 1, '-', '-'),
(157, 48, 'Upwind', 1, 'Rutin', 'PDAM', 1, '', ''),
(158, 48, 'Site', 1, 'Rutin', 'PDAM', 1, '', ''),
(159, 48, 'Downwind', 1, 'Rutin', 'PDAM', 1, '', ''),
(217, 49, 'Upwind', 1, 'Rutin', 'Lab', 1, '-', '-'),
(218, 49, 'Site', 1, 'Rutin', 'Lab', 1, '-', '-'),
(219, 49, 'Downwind', 1, 'Rutin', 'Lab', 1, '-', '-'),
(244, 46, 'Upwind', 1, 'Rutin', 'PPSDAL', 1, '', ''),
(245, 46, 'Site', 0, 'Tidak Rutin', '', 1, '', ''),
(246, 46, 'Downwind', 1, 'Rutin', 'PPSDAL', 1, '', ''),
(250, 51, 'Upwind', 1, 'Rutin', 'DLH - Air', 1, '-', '-'),
(251, 51, 'Site', 1, 'Rutin', 'DLH - Air', 1, '-', '-'),
(252, 51, 'Downwind', 1, 'Rutin', 'DLH - Air', 1, '-', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `uji_emisi`
--

CREATE TABLE `uji_emisi` (
  `id_uji_emisi` int(11) NOT NULL,
  `id_penc_udara` int(11) NOT NULL,
  `uji_kualitas` tinyint(1) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `lab` varchar(100) DEFAULT NULL,
  `bme_pemenuhan` tinyint(1) DEFAULT NULL,
  `bme_param` varchar(100) DEFAULT NULL,
  `bme_period` varchar(100) DEFAULT NULL,
  `pengendali_emisi` tinyint(1) DEFAULT NULL,
  `jenis` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `uji_emisi`
--

INSERT INTO `uji_emisi` (`id_uji_emisi`, `id_penc_udara`, `uji_kualitas`, `period`, `lab`, `bme_pemenuhan`, `bme_param`, `bme_period`, `pengendali_emisi`, `jenis`) VALUES
(17, 3, 1, 'Tidak Rutin', 'BPLH', 1, '-', '-', 1, 'rahasia alat pengendali emisi'),
(18, 3, 1, 'Rutin', 'Parahita', 1, '-', '-', 0, '-'),
(19, 3, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(20, 3, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(21, 4, NULL, '', '', NULL, '', '', NULL, ''),
(22, 4, NULL, '', '', NULL, '', '', NULL, ''),
(23, 4, NULL, '', '', NULL, '', '', NULL, ''),
(24, 4, NULL, '', '', NULL, '', '', NULL, ''),
(68, 5, 1, 'Rutin', 'BPLH', 1, '-', '-', 1, 'rahasia'),
(69, 5, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(70, 5, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(71, 5, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(80, 16, 1, 'Rutin', 'BPLH', 1, '', '', 1, 'cluter'),
(81, 16, NULL, '', '', NULL, '', '', NULL, ''),
(82, 16, NULL, '', '', NULL, '', '', NULL, ''),
(83, 16, NULL, '', '', NULL, '', '', NULL, ''),
(84, 14, 0, 'Tidak Rutin', 'BPLH', 0, 'as', 'asd', 0, '-'),
(85, 14, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(86, 14, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(87, 14, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(88, 17, 1, 'Rutin', 'Parahita', 1, '-', '-', 0, '-'),
(89, 17, NULL, '', '', NULL, '', '', NULL, ''),
(90, 17, NULL, '', '', NULL, '', '', NULL, ''),
(91, 17, NULL, '', '', NULL, '', '', NULL, ''),
(92, 18, 0, 'Rutin', 'BPLH', 1, '', '', 0, ''),
(93, 18, NULL, '', '', NULL, '', '', NULL, ''),
(94, 18, NULL, '', '', NULL, '', '', NULL, ''),
(95, 18, NULL, '', '', NULL, '', '', NULL, ''),
(96, 15, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(97, 15, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(98, 15, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(99, 15, 0, 'Tidak Rutin', 'BPLH', 0, '', '', 0, ''),
(100, 40, 1, 'Rutin', 'PDAM', 1, '-', '-', 1, ''),
(101, 40, NULL, '', '', NULL, '', '', NULL, ''),
(102, 40, 1, 'Rutin', 'PDAM', 1, '-', '-', 1, ''),
(103, 40, NULL, '', '', NULL, '', '', NULL, ''),
(104, 41, NULL, '', '', NULL, '', '', NULL, ''),
(105, 41, NULL, '', '', NULL, '', '', NULL, ''),
(106, 41, NULL, '', '', NULL, '', '', NULL, ''),
(107, 41, NULL, '', '', NULL, '', '', NULL, ''),
(108, 42, NULL, '', '', NULL, '', '', NULL, ''),
(109, 42, NULL, '', '', NULL, '', '', NULL, ''),
(110, 42, NULL, '', '', NULL, '', '', NULL, ''),
(111, 42, NULL, '', '', NULL, '', '', NULL, ''),
(120, 45, NULL, 'Rutin', 'PPSDAL', 1, '', '', 1, 'cyclon'),
(121, 45, NULL, '', '', NULL, '', '', NULL, ''),
(122, 45, NULL, '', '', NULL, '', '', NULL, ''),
(123, 45, NULL, '', '', NULL, '', '', NULL, ''),
(128, 47, 1, 'Rutin', 'PPSDAL', 1, '-', '-', 1, 'wet scrubber'),
(129, 47, 1, 'Rutin', 'PPSDAL', 1, '-', '-', 1, 'wet scrubber'),
(130, 47, 1, 'Rutin', 'PPSDAL', 1, '-', '-', 1, 'wet scrubber'),
(131, 47, 0, 'Tidak Rutin', '', NULL, '-', '-', 0, '-'),
(132, 48, 1, 'Rutin', 'PDAM', 1, '', '', 0, ''),
(133, 48, NULL, '', '', NULL, '', '', NULL, ''),
(134, 48, NULL, '', '', NULL, '', '', NULL, ''),
(135, 48, NULL, '', '', NULL, '', '', NULL, ''),
(212, 49, 1, 'Rutin', 'Lab', 1, '-', '-', 1, 'Cyclon'),
(213, 49, NULL, '', '', NULL, '', '', NULL, ''),
(214, 49, NULL, '', '', NULL, '', '', NULL, ''),
(215, 49, NULL, '', '', NULL, '', '', NULL, ''),
(248, 46, 1, 'Rutin', 'PPSDAL', 1, '', '', 1, 'Wet scubber'),
(249, 46, NULL, '', '', NULL, '', '', NULL, ''),
(250, 46, NULL, '', '', NULL, '', '', NULL, ''),
(251, 46, NULL, '', '', NULL, '', '', NULL, ''),
(272, 51, 1, 'Rutin', 'DLH - Air', 1, '-', '-', 1, 'Wet Scruber'),
(273, 51, 0, 'Tidak Rutin', 'DLH - Air', 0, '', '', 0, ''),
(274, 51, 0, 'Tidak Rutin', 'DLH - Air', 0, '', '', 0, ''),
(275, 51, 0, 'Tidak Rutin', 'DLH - Air', 0, '', '', 0, ''),
(280, 43, 1, 'Rutin', 'PPSDAL - Udara', 1, '-', '-', 1, 'cyclon'),
(281, 43, 0, 'Tidak Rutin', 'DLH - Air', 0, '', '', 0, ''),
(282, 43, 0, 'Tidak Rutin', 'DLH - Air', 0, '', '', 0, ''),
(283, 43, 0, 'Tidak Rutin', 'DLH - Air', 0, '', '', 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `usaha_kegiatan`
--

CREATE TABLE `usaha_kegiatan` (
  `id_usaha_kegiatan` int(11) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `tgl_update` date NOT NULL,
  `dibuat_oleh` varchar(255) NOT NULL,
  `diupdate_oleh` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `usaha_kegiatan`
--

INSERT INTO `usaha_kegiatan` (`id_usaha_kegiatan`, `ket`, `status`, `tgl_pembuatan`, `tgl_update`, `dibuat_oleh`, `diupdate_oleh`) VALUES
(11, 'Agro', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(12, 'Golf', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(13, 'Hotel', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(14, 'Industri', 1, '2015-01-02', '2018-11-05', 'Administrator', 'Administrator'),
(15, 'Laboratorium', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(16, 'Pengelola Limbah B3', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(17, 'Pertambangan', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(18, 'RPH', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(19, 'Rumah Makan / Domestik', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(20, 'Rumah Sakit', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(21, 'SPPBE', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(22, 'Farmasi', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(24, 'Geothermal', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(25, 'PLTU', 1, '2015-01-02', '2015-01-02', 'Administrator', 'Administrator'),
(27, 'perajutan', 1, '2018-11-05', '2018-11-05', 'Administrator', 'Administrator'),
(28, 'Garmen', 1, '2018-11-05', '2018-11-05', 'Administrator', 'Administrator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `group_id` int(11) NOT NULL DEFAULT '300'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`, `group_id`) VALUES
(1, 'Superadmin', 'superadmin', '$2a$08$bkyqqS.L0F6jVzUCF/u./OLo9dtciaARLiQ2vIeHADPROHDF.c4/y', 'superadmin@superadmin.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '::1', '2014-09-25 13:25:47', '2014-09-24 11:02:14', '2014-09-24 23:25:47', 1),
(2, 'Administrator', 'admin', '$2a$08$NXyVfncBMaerDfLjuTSnhOk6G65mzkFdP8cLHkaelV/ztsIbWrEMm', 'admin@admin.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '220.247.175.58', '2018-11-07 01:24:35', '2014-09-25 10:27:35', '2018-11-07 07:24:35', 2),
(3, 'Operator', 'operator', '$2a$08$Xxuk5w2BZfxNa4467ugniO1I7F2Fv2hLOuRMsiwR7co5/Is59Zn3G', 'operator@operator.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '::1', '0000-00-00 00:00:00', '2014-09-25 10:55:08', '2014-09-28 18:53:36', 4),
(4, 'Operator', 'adryan', '$2a$08$C4ElJCkwqsLRLJ.UvSS1t.wZ8DzoLMSnlSdOPf9Hj60sZSjIDlZ1G', 'a@a.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '::1', '0000-00-00 00:00:00', '2014-09-30 10:22:24', '2014-09-29 20:22:24', 3),
(5, 'User LHU 1', 'userlhu1', '$2a$08$kZQgAGRve/VuUeNxDvO6qO.AgyGjmGvEzDUf9FQzRnikrgYKZzmqi', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:20:57', '2018-10-22 02:20:57', 5),
(6, 'User LHU 2', 'userlhu2', '$2a$08$v42hsrjnDCrzuQFJbNUXK.TQrRx3770dMnim3HRyoSUELpR3xxG7K', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:21:20', '2018-10-22 02:21:20', 5),
(7, 'User LHU 3', 'userlhu3', '$2a$08$Wge1gcSmXY846uGGahUVOu/gac461IeJorx8moMywJOcMYwMhUdfS', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:21:40', '2018-10-22 02:21:40', 5),
(8, 'User BAP 1', 'userbap1', '$2a$08$IPB9FOHZ8QF/EZ4eOLk0vewLNGYrguxvGDGye8.eRh5yEG2dU70fW', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:21:57', '2018-10-22 02:21:57', 4),
(9, 'User BAP 2', 'userbap2', '$2a$08$iEfi2lOze9y3s0mu.8D9zejpM7R2Cg03qAuDS8vYWqtYsj.eQNxla', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:22:16', '2018-10-22 02:22:16', 4),
(10, 'User BAP 3', 'userbap3', '$2a$08$bh48uJ3hOqqVeh7frvNqEux7kSnEhnRBBDtxDkyGDIrlLbLW20xsG', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:22:33', '2018-10-22 02:22:33', 4),
(11, 'Manajer 1', 'manajer1', '$2a$08$JPKsSzKyen340Hm1ZNSgxuxdkvGlfzjmxo5pAO3eg0FA0iyIGFSpu', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:23:28', '2018-10-22 02:23:28', 3),
(12, 'Manajer 2', 'manajer2', '$2a$08$oqqV8BLLLXzBgSE.C93ZgeajvQ0khfnfaVX..XZnzM7pr244F3OPW', 'user@gmail.com', 1, 0, NULL, NULL, NULL, NULL, NULL, '114.124.167.130', '0000-00-00 00:00:00', '2018-10-21 21:23:58', '2018-10-22 02:23:58', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_autologin`
--

CREATE TABLE `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'avatar-1.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `name`, `phone`, `country`, `website`, `avatar`) VALUES
(1, 5, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(2, 5, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(3, 6, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(4, 7, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(5, 8, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(6, 9, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(7, 10, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(8, 11, NULL, NULL, NULL, NULL, 'avatar-1.jpg'),
(9, 12, NULL, NULL, NULL, NULL, 'avatar-1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bahan_kimia_ipal`
--
ALTER TABLE `bahan_kimia_ipal`
  ADD PRIMARY KEY (`id_bahan_kimia_ipal`),
  ADD KEY `fk_relationship_37` (`id_penc_air`);

--
-- Indeks untuk tabel `baku_mutu_izin`
--
ALTER TABLE `baku_mutu_izin`
  ADD PRIMARY KEY (`id_baku_mutu_izin`),
  ADD KEY `id_industri_baku_mutu_izin` (`id_industri`),
  ADD KEY `id_pembuangan_baku_mutu_izin` (`id_pembuangan`);

--
-- Indeks untuk tabel `bap`
--
ALTER TABLE `bap`
  ADD PRIMARY KEY (`id_bap`),
  ADD KEY `fk_relationship_17` (`id_pegawai`),
  ADD KEY `fk_relationship_5` (`id_industri`);

--
-- Indeks untuk tabel `bap_agro`
--
ALTER TABLE `bap_agro`
  ADD PRIMARY KEY (`id_bap_agro`),
  ADD KEY `fk_relationship_46` (`id_bap`);

--
-- Indeks untuk tabel `bap_golf`
--
ALTER TABLE `bap_golf`
  ADD PRIMARY KEY (`id_bap_golf`),
  ADD KEY `fk_relationship_44` (`id_bap`);

--
-- Indeks untuk tabel `bap_hotel`
--
ALTER TABLE `bap_hotel`
  ADD PRIMARY KEY (`id_bap_hotel`),
  ADD KEY `fk_relationship_27` (`id_bap`);

--
-- Indeks untuk tabel `bap_industri`
--
ALTER TABLE `bap_industri`
  ADD PRIMARY KEY (`id_bap_industri`),
  ADD KEY `fk_relationship_39` (`id_bap`);

--
-- Indeks untuk tabel `bap_lab`
--
ALTER TABLE `bap_lab`
  ADD PRIMARY KEY (`id_bap_lab`),
  ADD KEY `fk_bap_lab` (`id_bap`);

--
-- Indeks untuk tabel `bap_plb3`
--
ALTER TABLE `bap_plb3`
  ADD PRIMARY KEY (`id_bap_plb3`),
  ADD KEY `fk_relationship_50` (`id_bap`);

--
-- Indeks untuk tabel `bap_rm`
--
ALTER TABLE `bap_rm`
  ADD PRIMARY KEY (`id_bap_rm`),
  ADD KEY `fk_relationship_45` (`id_bap`);

--
-- Indeks untuk tabel `bap_rph`
--
ALTER TABLE `bap_rph`
  ADD PRIMARY KEY (`id_bap_rph`),
  ADD KEY `fk_relationship_49` (`id_bap`);

--
-- Indeks untuk tabel `bap_rs`
--
ALTER TABLE `bap_rs`
  ADD PRIMARY KEY (`id_bap_rs`),
  ADD KEY `fk_relationship_47` (`id_bap`);

--
-- Indeks untuk tabel `bap_sppbe`
--
ALTER TABLE `bap_sppbe`
  ADD PRIMARY KEY (`id_bap_sppbe`),
  ADD KEY `fk_relationship_48` (`id_bap`);

--
-- Indeks untuk tabel `bap_tambang`
--
ALTER TABLE `bap_tambang`
  ADD PRIMARY KEY (`id_bap_tambang`),
  ADD KEY `fk_bap_tambang` (`id_bap`);

--
-- Indeks untuk tabel `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indeks untuk tabel `data_cerobong`
--
ALTER TABLE `data_cerobong`
  ADD PRIMARY KEY (`id_cerobong`),
  ADD KEY `fk_relationship_41` (`id_penc_udara`);

--
-- Indeks untuk tabel `data_master`
--
ALTER TABLE `data_master`
  ADD PRIMARY KEY (`id_data_master`);

--
-- Indeks untuk tabel `data_master_detail`
--
ALTER TABLE `data_master_detail`
  ADD PRIMARY KEY (`id_data_master_detail`);

--
-- Indeks untuk tabel `debit_air_limbah`
--
ALTER TABLE `debit_air_limbah`
  ADD PRIMARY KEY (`id_debit_air_limbah`),
  ADD KEY `id_industri` (`id_industri`);

--
-- Indeks untuk tabel `detail_lhu`
--
ALTER TABLE `detail_lhu`
  ADD PRIMARY KEY (`id_detail_lhu`),
  ADD KEY `fk_relationship_20` (`id_laporan_hasil_uji`);

--
-- Indeks untuk tabel `emisi_boiler`
--
ALTER TABLE `emisi_boiler`
  ADD PRIMARY KEY (`id_emisi_boiler`),
  ADD KEY `fk_relationship_40` (`id_penc_udara`),
  ADD KEY `fk_relationship_42` (`id_jenis_emisi_boiler`);

--
-- Indeks untuk tabel `history_item_teguran_bap`
--
ALTER TABLE `history_item_teguran_bap`
  ADD PRIMARY KEY (`id_history_teguran_bap`),
  ADD KEY `fk_relationship_7` (`id_bap`);

--
-- Indeks untuk tabel `history_item_teguran_lhu`
--
ALTER TABLE `history_item_teguran_lhu`
  ADD KEY `fk_relationship_8` (`id_laporan_hasil_uji`);

--
-- Indeks untuk tabel `history_teguran_periode_adm`
--
ALTER TABLE `history_teguran_periode_adm`
  ADD PRIMARY KEY (`id_history_teguran_periode_adm`),
  ADD KEY `fk_relationship_19` (`id_industri`);

--
-- Indeks untuk tabel `industri`
--
ALTER TABLE `industri`
  ADD PRIMARY KEY (`id_industri`),
  ADD KEY `fk_relationship_2` (`id_usaha_kegiatan`),
  ADD KEY `fk_kecamatan` (`id_kecamatan`),
  ADD KEY `fk_kelurahan` (`id_kelurahan`);

--
-- Indeks untuk tabel `izin_pembuangan_limbah`
--
ALTER TABLE `izin_pembuangan_limbah`
  ADD PRIMARY KEY (`id_pembuangan`),
  ADD KEY `fk_industri_3` (`id_industri`);

--
-- Indeks untuk tabel `izin_tps_limbah`
--
ALTER TABLE `izin_tps_limbah`
  ADD PRIMARY KEY (`id_izin_tps`),
  ADD KEY `fk_industri_2` (`id_industri`);

--
-- Indeks untuk tabel `jenis_bap`
--
ALTER TABLE `jenis_bap`
  ADD PRIMARY KEY (`id_jenis_bap`);

--
-- Indeks untuk tabel `jenis_dokumen`
--
ALTER TABLE `jenis_dokumen`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `jenis_emisi_boiler`
--
ALTER TABLE `jenis_emisi_boiler`
  ADD PRIMARY KEY (`id_jenis_emisi_boiler`);

--
-- Indeks untuk tabel `jenis_industri`
--
ALTER TABLE `jenis_industri`
  ADD PRIMARY KEY (`id_jenis_industri`),
  ADD KEY `fk_id_usaha` (`id_usaha_kegiatan`);

--
-- Indeks untuk tabel `jenis_limbah_b3`
--
ALTER TABLE `jenis_limbah_b3`
  ADD PRIMARY KEY (`id_jlb3`),
  ADD KEY `fk_relationship_34` (`id_penc_pb3`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indeks untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`),
  ADD KEY `id_kecamatan` (`id_kecamatan`);

--
-- Indeks untuk tabel `laboratorium`
--
ALTER TABLE `laboratorium`
  ADD PRIMARY KEY (`id_lab`);

--
-- Indeks untuk tabel `laporan_hasil_uji`
--
ALTER TABLE `laporan_hasil_uji`
  ADD PRIMARY KEY (`id_laporan_hasil_uji`),
  ADD KEY `fk_relationship_3` (`id_industri`),
  ADD KEY `fk_relationship_9` (`id_lab`);

--
-- Indeks untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `manifest`
--
ALTER TABLE `manifest`
  ADD PRIMARY KEY (`id_manifest`),
  ADD KEY `id_penghasil_limbah` (`id_penghasil_limbah`),
  ADD KEY `id_pengangkut_limbah` (`id_pengangkut_limbah`),
  ADD KEY `id_pengolah_limbah` (`id_pengolah_limbah`);

--
-- Indeks untuk tabel `neraca_b3`
--
ALTER TABLE `neraca_b3`
  ADD PRIMARY KEY (`id_neraca`),
  ADD KEY `id_industri_neraca` (`id_industri`);

--
-- Indeks untuk tabel `parameter_bap`
--
ALTER TABLE `parameter_bap`
  ADD PRIMARY KEY (`id_parameter_bap`);

--
-- Indeks untuk tabel `parameter_lhu`
--
ALTER TABLE `parameter_lhu`
  ADD PRIMARY KEY (`id_parameter_lhu`),
  ADD KEY `FK_RELATIONSHIP_ID_UK` (`id_usaha_kegiatan`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indeks untuk tabel `pencemaran_air`
--
ALTER TABLE `pencemaran_air`
  ADD PRIMARY KEY (`id_penc_air`),
  ADD KEY `fk_relationship_31` (`id_bap`);

--
-- Indeks untuk tabel `pencemaran_pb3`
--
ALTER TABLE `pencemaran_pb3`
  ADD PRIMARY KEY (`id_penc_pb3`),
  ADD KEY `fk_relationship_32` (`id_bap`);

--
-- Indeks untuk tabel `pencemaran_udara`
--
ALTER TABLE `pencemaran_udara`
  ADD PRIMARY KEY (`id_penc_udara`),
  ADD KEY `fk_relationship_33` (`id_bap`);

--
-- Indeks untuk tabel `pengangkut_limbah`
--
ALTER TABLE `pengangkut_limbah`
  ADD PRIMARY KEY (`id_industri`),
  ADD KEY `fk_kec2` (`id_kecamatan`),
  ADD KEY `fk_kel2` (`id_kelurahan`);

--
-- Indeks untuk tabel `pengolah_limbah`
--
ALTER TABLE `pengolah_limbah`
  ADD PRIMARY KEY (`id_industri`),
  ADD KEY `fk_kec1` (`id_kecamatan`),
  ADD KEY `fk_kel1` (`id_kelurahan`);

--
-- Indeks untuk tabel `petugas_bap`
--
ALTER TABLE `petugas_bap`
  ADD PRIMARY KEY (`id_petugas_bap`),
  ADD KEY `fk_relationship_15` (`id_bap`),
  ADD KEY `fk_relationship_16` (`id_pegawai`);

--
-- Indeks untuk tabel `rekap_manifest`
--
ALTER TABLE `rekap_manifest`
  ADD PRIMARY KEY (`id_rekap_manifest`),
  ADD KEY `id_penghasil_limbah_rekap` (`id_penghasil_limbah`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `surat_teguran_bap`
--
ALTER TABLE `surat_teguran_bap`
  ADD PRIMARY KEY (`id_surat_teguran_bap`),
  ADD KEY `fk_relationship_10` (`id_bap`);

--
-- Indeks untuk tabel `surat_teguran_lhu`
--
ALTER TABLE `surat_teguran_lhu`
  ADD PRIMARY KEY (`id_surat_teguran_lhu`),
  ADD KEY `fk_relationship_11` (`id_laporan_hasil_uji`);

--
-- Indeks untuk tabel `tps_b3`
--
ALTER TABLE `tps_b3`
  ADD PRIMARY KEY (`id_tps_b3`),
  ADD KEY `fk_relationship_35` (`id_penc_pb3`);

--
-- Indeks untuk tabel `tps_limbah`
--
ALTER TABLE `tps_limbah`
  ADD PRIMARY KEY (`id_tps_limbah`),
  ADD KEY `fk_tps_limbah_2` (`id_izin_tps`);

--
-- Indeks untuk tabel `uji_ambien`
--
ALTER TABLE `uji_ambien`
  ADD PRIMARY KEY (`id_uji_ambien`),
  ADD KEY `fk_relationship_36` (`id_penc_udara`);

--
-- Indeks untuk tabel `uji_emisi`
--
ALTER TABLE `uji_emisi`
  ADD PRIMARY KEY (`id_uji_emisi`),
  ADD KEY `fk_relationship_43` (`id_penc_udara`);

--
-- Indeks untuk tabel `usaha_kegiatan`
--
ALTER TABLE `usaha_kegiatan`
  ADD PRIMARY KEY (`id_usaha_kegiatan`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Indeks untuk tabel `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bahan_kimia_ipal`
--
ALTER TABLE `bahan_kimia_ipal`
  MODIFY `id_bahan_kimia_ipal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT untuk tabel `baku_mutu_izin`
--
ALTER TABLE `baku_mutu_izin`
  MODIFY `id_baku_mutu_izin` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bap`
--
ALTER TABLE `bap`
  MODIFY `id_bap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT untuk tabel `bap_agro`
--
ALTER TABLE `bap_agro`
  MODIFY `id_bap_agro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `bap_golf`
--
ALTER TABLE `bap_golf`
  MODIFY `id_bap_golf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `bap_hotel`
--
ALTER TABLE `bap_hotel`
  MODIFY `id_bap_hotel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `bap_industri`
--
ALTER TABLE `bap_industri`
  MODIFY `id_bap_industri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `bap_lab`
--
ALTER TABLE `bap_lab`
  MODIFY `id_bap_lab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `bap_plb3`
--
ALTER TABLE `bap_plb3`
  MODIFY `id_bap_plb3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `bap_rm`
--
ALTER TABLE `bap_rm`
  MODIFY `id_bap_rm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `bap_rph`
--
ALTER TABLE `bap_rph`
  MODIFY `id_bap_rph` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `bap_rs`
--
ALTER TABLE `bap_rs`
  MODIFY `id_bap_rs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `bap_sppbe`
--
ALTER TABLE `bap_sppbe`
  MODIFY `id_bap_sppbe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `bap_tambang`
--
ALTER TABLE `bap_tambang`
  MODIFY `id_bap_tambang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `data_cerobong`
--
ALTER TABLE `data_cerobong`
  MODIFY `id_cerobong` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=288;

--
-- AUTO_INCREMENT untuk tabel `data_master`
--
ALTER TABLE `data_master`
  MODIFY `id_data_master` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `data_master_detail`
--
ALTER TABLE `data_master_detail`
  MODIFY `id_data_master_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT untuk tabel `debit_air_limbah`
--
ALTER TABLE `debit_air_limbah`
  MODIFY `id_debit_air_limbah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `detail_lhu`
--
ALTER TABLE `detail_lhu`
  MODIFY `id_detail_lhu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `emisi_boiler`
--
ALTER TABLE `emisi_boiler`
  MODIFY `id_emisi_boiler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT untuk tabel `history_item_teguran_bap`
--
ALTER TABLE `history_item_teguran_bap`
  MODIFY `id_history_teguran_bap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1022;

--
-- AUTO_INCREMENT untuk tabel `history_teguran_periode_adm`
--
ALTER TABLE `history_teguran_periode_adm`
  MODIFY `id_history_teguran_periode_adm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `industri`
--
ALTER TABLE `industri`
  MODIFY `id_industri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `izin_pembuangan_limbah`
--
ALTER TABLE `izin_pembuangan_limbah`
  MODIFY `id_pembuangan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `izin_tps_limbah`
--
ALTER TABLE `izin_tps_limbah`
  MODIFY `id_izin_tps` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `jenis_bap`
--
ALTER TABLE `jenis_bap`
  MODIFY `id_jenis_bap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `jenis_dokumen`
--
ALTER TABLE `jenis_dokumen`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `jenis_emisi_boiler`
--
ALTER TABLE `jenis_emisi_boiler`
  MODIFY `id_jenis_emisi_boiler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `jenis_industri`
--
ALTER TABLE `jenis_industri`
  MODIFY `id_jenis_industri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT untuk tabel `jenis_limbah_b3`
--
ALTER TABLE `jenis_limbah_b3`
  MODIFY `id_jlb3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT untuk tabel `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id_kelurahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT untuk tabel `laboratorium`
--
ALTER TABLE `laboratorium`
  MODIFY `id_lab` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `laporan_hasil_uji`
--
ALTER TABLE `laporan_hasil_uji`
  MODIFY `id_laporan_hasil_uji` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `manifest`
--
ALTER TABLE `manifest`
  MODIFY `id_manifest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `neraca_b3`
--
ALTER TABLE `neraca_b3`
  MODIFY `id_neraca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `parameter_bap`
--
ALTER TABLE `parameter_bap`
  MODIFY `id_parameter_bap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=413;

--
-- AUTO_INCREMENT untuk tabel `parameter_lhu`
--
ALTER TABLE `parameter_lhu`
  MODIFY `id_parameter_lhu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556;

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `pencemaran_air`
--
ALTER TABLE `pencemaran_air`
  MODIFY `id_penc_air` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT untuk tabel `pencemaran_pb3`
--
ALTER TABLE `pencemaran_pb3`
  MODIFY `id_penc_pb3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT untuk tabel `pencemaran_udara`
--
ALTER TABLE `pencemaran_udara`
  MODIFY `id_penc_udara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT untuk tabel `pengangkut_limbah`
--
ALTER TABLE `pengangkut_limbah`
  MODIFY `id_industri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pengolah_limbah`
--
ALTER TABLE `pengolah_limbah`
  MODIFY `id_industri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `petugas_bap`
--
ALTER TABLE `petugas_bap`
  MODIFY `id_petugas_bap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT untuk tabel `rekap_manifest`
--
ALTER TABLE `rekap_manifest`
  MODIFY `id_rekap_manifest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `surat_teguran_bap`
--
ALTER TABLE `surat_teguran_bap`
  MODIFY `id_surat_teguran_bap` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `surat_teguran_lhu`
--
ALTER TABLE `surat_teguran_lhu`
  MODIFY `id_surat_teguran_lhu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tps_b3`
--
ALTER TABLE `tps_b3`
  MODIFY `id_tps_b3` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=445;

--
-- AUTO_INCREMENT untuk tabel `tps_limbah`
--
ALTER TABLE `tps_limbah`
  MODIFY `id_tps_limbah` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `uji_ambien`
--
ALTER TABLE `uji_ambien`
  MODIFY `id_uji_ambien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT untuk tabel `uji_emisi`
--
ALTER TABLE `uji_emisi`
  MODIFY `id_uji_emisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;

--
-- AUTO_INCREMENT untuk tabel `usaha_kegiatan`
--
ALTER TABLE `usaha_kegiatan`
  MODIFY `id_usaha_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `bahan_kimia_ipal`
--
ALTER TABLE `bahan_kimia_ipal`
  ADD CONSTRAINT `fk_relationship_37` FOREIGN KEY (`id_penc_air`) REFERENCES `pencemaran_air` (`id_penc_air`);

--
-- Ketidakleluasaan untuk tabel `baku_mutu_izin`
--
ALTER TABLE `baku_mutu_izin`
  ADD CONSTRAINT `id_industri_baku_mutu_izin` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`),
  ADD CONSTRAINT `id_pembuangan_baku_mutu_izin` FOREIGN KEY (`id_pembuangan`) REFERENCES `izin_pembuangan_limbah` (`id_pembuangan`);

--
-- Ketidakleluasaan untuk tabel `bap`
--
ALTER TABLE `bap`
  ADD CONSTRAINT `fk_relationship_17` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`),
  ADD CONSTRAINT `fk_relationship_5` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `bap_agro`
--
ALTER TABLE `bap_agro`
  ADD CONSTRAINT `fk_relationship_46` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_golf`
--
ALTER TABLE `bap_golf`
  ADD CONSTRAINT `fk_relationship_44` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_hotel`
--
ALTER TABLE `bap_hotel`
  ADD CONSTRAINT `fk_relationship_27` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_industri`
--
ALTER TABLE `bap_industri`
  ADD CONSTRAINT `fk_relationship_39` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_lab`
--
ALTER TABLE `bap_lab`
  ADD CONSTRAINT `fk_bap_lab` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_plb3`
--
ALTER TABLE `bap_plb3`
  ADD CONSTRAINT `fk_relationship_50` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_rm`
--
ALTER TABLE `bap_rm`
  ADD CONSTRAINT `fk_relationship_45` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_rph`
--
ALTER TABLE `bap_rph`
  ADD CONSTRAINT `fk_relationship_49` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_rs`
--
ALTER TABLE `bap_rs`
  ADD CONSTRAINT `fk_relationship_47` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_sppbe`
--
ALTER TABLE `bap_sppbe`
  ADD CONSTRAINT `fk_relationship_48` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `bap_tambang`
--
ALTER TABLE `bap_tambang`
  ADD CONSTRAINT `fk_bap_tambang` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `data_cerobong`
--
ALTER TABLE `data_cerobong`
  ADD CONSTRAINT `fk_relationship_41` FOREIGN KEY (`id_penc_udara`) REFERENCES `pencemaran_udara` (`id_penc_udara`);

--
-- Ketidakleluasaan untuk tabel `detail_lhu`
--
ALTER TABLE `detail_lhu`
  ADD CONSTRAINT `fk_relationship_20` FOREIGN KEY (`id_laporan_hasil_uji`) REFERENCES `laporan_hasil_uji` (`id_laporan_hasil_uji`);

--
-- Ketidakleluasaan untuk tabel `emisi_boiler`
--
ALTER TABLE `emisi_boiler`
  ADD CONSTRAINT `fk_relationship_40` FOREIGN KEY (`id_penc_udara`) REFERENCES `pencemaran_udara` (`id_penc_udara`),
  ADD CONSTRAINT `fk_relationship_42` FOREIGN KEY (`id_jenis_emisi_boiler`) REFERENCES `jenis_emisi_boiler` (`id_jenis_emisi_boiler`);

--
-- Ketidakleluasaan untuk tabel `history_item_teguran_bap`
--
ALTER TABLE `history_item_teguran_bap`
  ADD CONSTRAINT `fk_relationship_7` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `history_item_teguran_lhu`
--
ALTER TABLE `history_item_teguran_lhu`
  ADD CONSTRAINT `fk_relationship_8` FOREIGN KEY (`id_laporan_hasil_uji`) REFERENCES `laporan_hasil_uji` (`id_laporan_hasil_uji`);

--
-- Ketidakleluasaan untuk tabel `history_teguran_periode_adm`
--
ALTER TABLE `history_teguran_periode_adm`
  ADD CONSTRAINT `fk_relationship_19` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `industri`
--
ALTER TABLE `industri`
  ADD CONSTRAINT `fk_kecamatan` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  ADD CONSTRAINT `fk_kelurahan` FOREIGN KEY (`id_kelurahan`) REFERENCES `kelurahan` (`id_kelurahan`),
  ADD CONSTRAINT `fk_relationship_2` FOREIGN KEY (`id_usaha_kegiatan`) REFERENCES `usaha_kegiatan` (`id_usaha_kegiatan`);

--
-- Ketidakleluasaan untuk tabel `izin_pembuangan_limbah`
--
ALTER TABLE `izin_pembuangan_limbah`
  ADD CONSTRAINT `fk_industri_3` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `izin_tps_limbah`
--
ALTER TABLE `izin_tps_limbah`
  ADD CONSTRAINT `fk_industri_2` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `jenis_industri`
--
ALTER TABLE `jenis_industri`
  ADD CONSTRAINT `fk_id_usaha` FOREIGN KEY (`id_usaha_kegiatan`) REFERENCES `usaha_kegiatan` (`id_usaha_kegiatan`);

--
-- Ketidakleluasaan untuk tabel `jenis_limbah_b3`
--
ALTER TABLE `jenis_limbah_b3`
  ADD CONSTRAINT `fk_relationship_34` FOREIGN KEY (`id_penc_pb3`) REFERENCES `pencemaran_pb3` (`id_penc_pb3`);

--
-- Ketidakleluasaan untuk tabel `laporan_hasil_uji`
--
ALTER TABLE `laporan_hasil_uji`
  ADD CONSTRAINT `fk_relationship_3` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`),
  ADD CONSTRAINT `fk_relationship_9` FOREIGN KEY (`id_lab`) REFERENCES `laboratorium` (`id_lab`);

--
-- Ketidakleluasaan untuk tabel `manifest`
--
ALTER TABLE `manifest`
  ADD CONSTRAINT `id_pengangkut_limbah` FOREIGN KEY (`id_pengangkut_limbah`) REFERENCES `pengangkut_limbah` (`id_industri`),
  ADD CONSTRAINT `id_penghasil_limbah` FOREIGN KEY (`id_penghasil_limbah`) REFERENCES `industri` (`id_industri`),
  ADD CONSTRAINT `id_pengolah_limbah` FOREIGN KEY (`id_pengolah_limbah`) REFERENCES `pengolah_limbah` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `neraca_b3`
--
ALTER TABLE `neraca_b3`
  ADD CONSTRAINT `id_industri_neraca` FOREIGN KEY (`id_industri`) REFERENCES `industri` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `parameter_lhu`
--
ALTER TABLE `parameter_lhu`
  ADD CONSTRAINT `FK_RELATIONSHIP_ID_UK` FOREIGN KEY (`id_usaha_kegiatan`) REFERENCES `usaha_kegiatan` (`id_usaha_kegiatan`);

--
-- Ketidakleluasaan untuk tabel `pencemaran_air`
--
ALTER TABLE `pencemaran_air`
  ADD CONSTRAINT `fk_relationship_31` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `pencemaran_pb3`
--
ALTER TABLE `pencemaran_pb3`
  ADD CONSTRAINT `fk_relationship_32` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `pencemaran_udara`
--
ALTER TABLE `pencemaran_udara`
  ADD CONSTRAINT `fk_relationship_33` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `pengangkut_limbah`
--
ALTER TABLE `pengangkut_limbah`
  ADD CONSTRAINT `fk_kec2` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  ADD CONSTRAINT `fk_kel2` FOREIGN KEY (`id_kelurahan`) REFERENCES `kelurahan` (`id_kelurahan`);

--
-- Ketidakleluasaan untuk tabel `pengolah_limbah`
--
ALTER TABLE `pengolah_limbah`
  ADD CONSTRAINT `fk_kec1` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  ADD CONSTRAINT `fk_kel1` FOREIGN KEY (`id_kelurahan`) REFERENCES `kelurahan` (`id_kelurahan`);

--
-- Ketidakleluasaan untuk tabel `petugas_bap`
--
ALTER TABLE `petugas_bap`
  ADD CONSTRAINT `fk_relationship_15` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`),
  ADD CONSTRAINT `fk_relationship_16` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`);

--
-- Ketidakleluasaan untuk tabel `rekap_manifest`
--
ALTER TABLE `rekap_manifest`
  ADD CONSTRAINT `id_penghasil_limbah_rekap` FOREIGN KEY (`id_penghasil_limbah`) REFERENCES `industri` (`id_industri`);

--
-- Ketidakleluasaan untuk tabel `surat_teguran_bap`
--
ALTER TABLE `surat_teguran_bap`
  ADD CONSTRAINT `fk_relationship_10` FOREIGN KEY (`id_bap`) REFERENCES `bap` (`id_bap`);

--
-- Ketidakleluasaan untuk tabel `surat_teguran_lhu`
--
ALTER TABLE `surat_teguran_lhu`
  ADD CONSTRAINT `fk_relationship_11` FOREIGN KEY (`id_laporan_hasil_uji`) REFERENCES `laporan_hasil_uji` (`id_laporan_hasil_uji`);

--
-- Ketidakleluasaan untuk tabel `tps_b3`
--
ALTER TABLE `tps_b3`
  ADD CONSTRAINT `fk_relationship_35` FOREIGN KEY (`id_penc_pb3`) REFERENCES `pencemaran_pb3` (`id_penc_pb3`);

--
-- Ketidakleluasaan untuk tabel `tps_limbah`
--
ALTER TABLE `tps_limbah`
  ADD CONSTRAINT `fk_tps_limbah_2` FOREIGN KEY (`id_izin_tps`) REFERENCES `izin_tps_limbah` (`id_izin_tps`);

--
-- Ketidakleluasaan untuk tabel `uji_ambien`
--
ALTER TABLE `uji_ambien`
  ADD CONSTRAINT `fk_relationship_36` FOREIGN KEY (`id_penc_udara`) REFERENCES `pencemaran_udara` (`id_penc_udara`);

--
-- Ketidakleluasaan untuk tabel `uji_emisi`
--
ALTER TABLE `uji_emisi`
  ADD CONSTRAINT `fk_relationship_43` FOREIGN KEY (`id_penc_udara`) REFERENCES `pencemaran_udara` (`id_penc_udara`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
