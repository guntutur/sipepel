<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_kelurahan extends CI_Model {

	private $table_name			= 'kelurahan';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('kec.id_kecamatan, kec.ket as nama_kecamatan, kel.ket as nama_kelurahan, kel.status, kel.id_kelurahan')->from($this->table_name .' as kel');
		$this->db->join('kecamatan kec', 'kel.id_kecamatan = kec.id_kecamatan', 'left');

		/*foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}*/
		
		if($where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("kec.ket", "asc");
		$this->db->order_by("kel.ket", "asc"); 
		$this->db->order_by("kel.tgl_pembuatan", "desc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		$this->db->select('kec.id_kecamatan, kec.ket as nama_kecamatan, kel.ket as nama_kelurahan, kel.status')->from($this->table_name .' as kel');
		$this->db->join('kecamatan kec', 'kel.id_kecamatan = kec.id_kecamatan', 'left');

		/*foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}*/
		
		if($where != '') {
			$this->db->where($where);
		}
		
		return $this->db->count_all_results();
	}
	
	function get_dropdown() {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("ket", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_kelurahan_by_id($id) {
		$this->db->where('id_kelurahan', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function get_kelurahan_by_kecamatan($id) {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("ket", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 
		if($id) {
			$this->db->where("id_kecamatan", $id);
		}

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_kelurahan($id) {
		$data = $this->get_kelurahan_by_id($id);
		$this->db->where('id_kelurahan', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_kelurahan($data, $id) {

		$this->db->where("id_kelurahan", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_kelurahan($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate($data) {
        foreach ($data as $key => $value) {
            $this->db->where($key, $value);
        }        
        $this->db->from($this->table_name);        
        $res = $this->db->count_all_results();

        if($res > 0) return true;
        else return false;
    }
}
?>