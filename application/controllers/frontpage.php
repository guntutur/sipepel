<?php

if (!defined('BASEPATH'))
    die();

class frontpage extends Main_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
        
    }

    public function index() {       

        $this->load->view('include/header');
        $this->load->view('frontpage');
        $this->load->view('include/footer');
    }

}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
