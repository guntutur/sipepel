<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Rekapitulasi Laporan Hasil Uji&nbsp;&nbsp;<small>rekapitulasi seluruh data laporan hasil uji</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Rekapitulasi</li>
                <li class="active">Rekapitulasi LHU</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

               <div class="row">                    
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />                            
                        </div>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <br/>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>                                
                                <th width="5%">No.</th>
                                <th>Nama Usaha/Kegiatan</th>                               
                                <th>Laporan Bulan</th>								
                                <th>Status LHU</th>
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

var Document = {
    param: {
        dataperpage: 10,
        query: '',
        curpage: 0,
        numpage: 0
    },
    url: '<?php echo base_url("backend/rekap_lhu/get_list"); ?>',
    search: function() {
        this.param.query = $('#query').val();
        this.param.curpage = 0;
        this.load_data();
        return false;
    },
    search_field: function(e) {

    },
    set_page: function(n) {
        this.param.curpage = n;
        this.load_data();
        return false;
    },
    prev_page: function() {
        if(this.param.curpage > 0) {
            this.param.curpage--;
            this.load_data();
        }
        return false;
    },
    next_page: function() {
        if(this.param.curpage < this.param.numpage) {
            this.param.curpage++;
            this.load_data();
        }
        return false;
    },
    load_data: function() {
        $.ajax({
            url: Document.url,
            type: 'POST',
            dataType: 'json',
            data: $.param(Document.param),
            success: function(d) {
                // console.log(d);
                $('#pagination').html(d.pagination);
                if(d.total < 10){ $('#start').text(d.total); }                    
                $('#nums').text(d.total);
                Document.param.numpage = d.numpage;
                var t = '', status = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                for (var i = 0; i < d.data.length; i++) {
                    dt = d.data[i];
                    t += '<tr>';
                    t += '<td>'+(no+=1)+'</td>';
                    t += '<td>'+dt.nama_industri+'</td>';
                    
                    if(dt.laporan_bulan_tahun) {
                        var tmp = dt.laporan_bulan_tahun.split(".");
                        t += '<td>'+$(this).get_month(parseInt(tmp[0]))+' '+ tmp[1] +'</td>';
                    }else{
                        t += '<td>'+$(this).return_value(dt.laporan_bulan_tahun)+'</td>';					
                    }

                    if(dt.teguran_air == '' && dt.teguran_emisi == '' && dt.teguran_ambien == '') {
                        status = 'Taat';
                    }else{
                        status = 'Tidak Taat';
                    }

                    t += '<td>'+((dt.laporan_bulan_tahun) ? status : '-')+'</td>';
                    // t += '<td></td>';
                    // t += '<td>'+$(this).status_lhu(dt.status)+'</td>';
                    t += '</tr>';
                }
                
                $('#document-data').html(t);
            }
        })
        .fail(function(e) {
            console.log(e);
        });            
    }
} 

$(document).ready(function () {  
    
    $.fn.search = function(e, v) {
        if(e.keyCode == 13 || v.value == '') {
            Document.search();                
        }
    }    

    Document.search();
});
</script>