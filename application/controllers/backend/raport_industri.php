<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class raport_industri extends CI_Controller {

    function __construct() {
        parent::__construct();

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->load->model('m_industry', 'IndDao', TRUE);
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types
		
		# response paramter     
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_raport_industri');
        $this->load->view('include/footer');
    }

    public function get_rekap_raport_industri() {
        # get parameter pagination
        $input = array('dataperpage', 'tahun', 'curpage');
        foreach ($input as $val) {
            $$val = $this->input->post($val); 
        }

        $total = $this->IndDao->count_raport_bap($tahun);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $bap_data = $this->IndDao->get_raport_bap($tahun, $dataperpage, $start);

        $data = array(
            'bap_data' => $bap_data,
            'pagination' => $this->functions->create_links($npage, $curpage, 3),
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['bap_data'] = $bap_data;

        echo json_encode($data);
        exit();
    }

    /**
     *  Author : Huda
     *  Modified by guntutur@gmail.com*
     * @return  rtf file
     */
    public function generate_raport() {
        # get id_bap from post
        $id_bap = $this->input->post('id_bap');

        $data = $this->get_bap($id_bap);

        $bap = $data['bap'];
        $pencemaran_air = $data['pencemaran_air'];
        $izin_limbah = $data['izin_limbah'];
        $izin_air_limbah = $data['izin_air_limbah'];
        $izin_tps_limbah = $data['izin_tps_limbah'];
        $data_cerobong = $data['data_cerobong'];
        $uji_emisi = $data['uji_emisi'];
        $uji_ambien = $data['uji_ambien'];
        $pencemaran_pb3 = $data['pencemaran_pb3'];
        // $jenis_limbah_b3 = $bap_data['jenis_limbah_b3'];
        $tps_b3 = $data['tps_b3'];

        if ($uji_emisi != NULL) {
            $objue = '';
            $objbme = '';
            $objuer = '';
            $objbmer = '';
            $i = 1;
            foreach ($uji_emisi as $emisi) {
                $objue = $objue.'{'.'Cerobong '. $i .' :\line'.' - '.(($emisi->pengendali_emisi==0) ? '\cf6 '.$this->adaConv($emisi->pengendali_emisi).'' : $this->adaConv($emisi->pengendali_emisi)).'\line}';
                $objuer = $objuer.'{Cerobong '.$i.' :\line - '.(($emisi->pengendali_emisi==0) ? '\cf6 Tidak Taat' : 'Taat').' \line}';
                $objbme = $objbme.'{'.'Cerobong '. $i .' :\line'.' - '.(($emisi->bme_pemenuhan==0) ? '\cf6 '.$this->adaConv($emisi->bme_pemenuhan).'' : $this->adaConv($emisi->bme_pemenuhan)).'\line}';
                $objbmer = $objbmer.'{'.'Cerobong '.$i.' :\line - '.(($emisi->bme_pemenuhan==0) ? '\cf6 Tidak Taat' : 'Taat').' \line}';
                $i++;
            }
        } else {
            $objue = "-";
            $objbme = "-";
            $objuer = "-";
            $objbmer = "-";
        }
        if($data_cerobong != NULL) {
            $objdc = '';
            $objdcr = '';
            $i = 1;
            foreach ($data_cerobong as $crb) {
                $objdc = $objdc.'{'.'\fs24 Cerobong '. $i .' :\line'.'\fs18 - Sampling Hole: '.(($crb->sampling_hole==0) ? '\cf6 '.$this->adaConv($crb->sampling_hole).'' : $this->adaConv($crb->sampling_hole)).'\line'.'\cf0 - Tangga: '.(($crb->tangga==0) ? '\cf6 '.$this->adaConv($crb->tangga).'' : $this->adaConv($crb->tangga)).'\line'.'\cf0 - Lantai Kerja: '.(($crb->lantai==0) ? '\cf6 '.$this->adaConv($crb->lantai).'' : $this->adaConv($crb->lantai)).'\line}';
                $objdcr = $objdcr.'{\fs24 Cerobong '. $i .' :\line \fs18 - Sampling Hole: '.(($crb->sampling_hole==0) ? '\cf6 Tidak Taat' : 'Taat').'\line'.'\cf0 - Tangga: '.(($crb->tangga==0) ? '\cf6 Tidak Taat' : 'Taat').'\line'.'\cf0 - Lantai Kerja: '.(($crb->lantai==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                $i++;
            }
        } else {
            $objdc = "-";
            $objdcr = '-';
        } 
        if ($uji_ambien != NULL) {
            $objua = '';
            $objuar = '';
            $i = 1;
            foreach ($uji_ambien as $ua) {
                $objua = $objua.'{'.$ua->lokasi.' : '.(($ua->uji_kualitas==0) ? '\cf6 '.$this->adaConv($ua->uji_kualitas).'' : $this->adaConv($ua->uji_kualitas)).'\line}';
                $objuar = $objuar.'{'.$ua->lokasi.' : '.(($ua->uji_kualitas==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                $i++;
            }
        } else {
            $objua = "-";
            $objuar = "-";
        }
        if ($tps_b3 != NULL) {
            $ppn = '';
            $sop = '';
            $smbl = '';
            $log = '';
            $apar = '';
            $ppnr = '';
            $sopr = '';
            $smblr = '';
            $logr = '';
            $aparr = '';
            $i = 1;
            foreach ($tps_b3 as $tps) {
                $ppn = $ppn.'{'.'TPS '. $i .' :\line'.' - '.(($tps->ppn_nama_koord==0) ? '\cf6 '.$this->adaConv($tps->ppn_nama_koord).'' : $this->adaConv($tps->ppn_nama_koord)).'\line}';
                if($tps->ppn_nama_koord!=null){
                    $ppnr = $ppnr.'{TPS '. $i .' :\line - '.(($tps->ppn_nama_koord==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                } else { $ppnr = $ppnr.'{TPS '. $i .' :\line - \line}'; }
                $sop = $sop.'{'.'TPS '. $i .' :\line'.' - '.(($tps->sop_darurat==0) ? '\cf6 '.$this->adaConv($tps->sop_darurat).'' : $this->adaConv($tps->sop_darurat)).'\line}';
                if($tps->sop_darurat!=null){
                    $sopr = $sopr.'{TPS '. $i .' :\line - '.(($tps->sop_darurat==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                } else { $sopr = $sopr.'{TPS '. $i .' :\line - \line}'; }
                $smbl = $smbl.'{'.'TPS '. $i .' :\line'.' - '.(($tps->simbol_label==0) ? '\cf6 '.$this->adaConv($tps->simbol_label).'' : $this->adaConv($tps->simbol_label)).'\line}';
                if($tps->simbol_label!=null){
                    $smblr = $smblr.'{TPS '. $i .' :\line - '.(($tps->simbol_label==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                } else { $smblr = $smblr.'{TPS '. $i .' :\line - \line}'; }
                $log = $log.'{'.'TPS '. $i .' :\line'.' - '.(($tps->log_book==0) ? '\cf6 '.$this->adaConv($tps->log_book).'' : $this->adaConv($tps->log_book)).'\line}';
                if($tps->log_book!=null){
                    $logr = $logr.'{TPS '. $i .' :\line - '.(($tps->log_book==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                } else { $logr = $logr.'{TPS '. $i .' :\line - \line}'; }
                $apar = $apar.'{'.'TPS '. $i .' :\line'.' - '.(($tps->apar_p3k==0) ? '\cf6 '.$this->adaConv($tps->apar_p3k).'' : $this->adaConv($tps->apar_p3k)).'\line}';
                if($tps->apar_p3k!=null){
                    $aparr = $aparr.'{TPS '. $i .' :\line - '.(($tps->apar_p3k==0) ? '\cf6 Tidak Taat' : 'Taat').'\line}';
                } else { $aparr = $aparr.'{TPS '. $i .' :\line - \line}'; }
                $i++;
            }
            if($tps_b3[0]->izin!=null){ // only check first row, assummed if not null, then it has permission
                $iztp = "Ada";
                $iztpr = "Taat";
            } else {
                $iztp = "{\cf6 Tidak Ada}";
                $iztpr = "{\cf6 Tidak Taat}";
            }
        } else {
            $ppn = '-';
            $sop = '-';
            $smbl = '-';
            $log = '-';
            $apar = '-';
            $ppnr = '-';
            $sopr = '-';
            $smblr = '-';
            $logr = '-';
            $aparr = '-';
            $iztp = "-";
            $iztpr = "-";
        }
        if($pencemaran_pb3 != NULL) {
            if($pencemaran_pb3->b3_penyimpanan != NULL) {
                $b3_simpan = (($pencemaran_pb3->b3_penyimpanan=='Pada TPS Limbah B3') ? $pencemaran_pb3->b3_penyimpanan : '{\cf6 '.$pencemaran_pb3->b3_penyimpanan.'}');
                $b3_simpanr = (($pencemaran_pb3->b3_penyimpanan=='Pada TPS Limbah B3') ? 'Taat' : '{\cf6 Tidak Taat}');
            } else {
                $b3_simpan = '-';
                $b3_simpanr = '-';
            }
            if ($pencemaran_pb3->b3_lapor_nm != NULL) {
                $b3_nm = (($pencemaran_pb3->b3_lapor_nm=='Rutin') ? $pencemaran_pb3->b3_lapor_nm : '{\cf6 '.$pencemaran_pb3->b3_lapor_nm.'}');
                $b3_nmr = (($pencemaran_pb3->b3_lapor_nm=='Rutin') ? 'Taat' : '{\cf6 Tidak Taat}');
            } else {
                $b3_nm = '-';
                $b3_nmr = '-';
            }
        } else {
            $b3_simpan = '-';
            $b3_nm = '-';
        }
        if($pencemaran_air != NULL) {
            if($pencemaran_air->ipal != NULL) {
                $ipal = (($pencemaran_air->ipal==0) ? '{\cf6 '.$this->adaConv($pencemaran_air->ipal).'}' : $this->adaConv($pencemaran_air->ipal));
                $ipalr = (($pencemaran_air->ipal==0) ? '{\cf6 Tidak Taat}' : 'Taat');
            } else {
                $ipal = '-';
                $ipalr = '-';
            }
            if ($pencemaran_air->au != NULL) {
                $au = (($pencemaran_air->au==0) ? '{\cf6 '.$this->adaConv($pencemaran_air->au).'}' : $this->adaConv($pencemaran_air->au));
                $aur = (($pencemaran_air->au==0) ? '{\cf6 Tidak Taat}' : 'Taat');
            } else {
                $au = '-';
                $aur = '-';
            }
            if ($pencemaran_air->bocor != NULL) {
                $bocor = (($pencemaran_air->bocor==1) ? '{\cf6 '.$this->adaConv($pencemaran_air->bocor).'}' : $this->adaConv($pencemaran_air->bocor));
                $bocorr = (($pencemaran_air->bocor==1) ? '{\cf6 Tidak Taat}' : 'Taat');
            } else {
                $bocor = '-';
                $bocorr = '-';
            }
            if ($pencemaran_air->uji_limbah != NULL) {
                $uji_limbah = (($pencemaran_air->uji_limbah==0) ? '{\cf6 '.$this->adaConv($pencemaran_air->uji_limbah).'}' : $this->adaConv($pencemaran_air->uji_limbah));
                $uji_limbahr = (($pencemaran_air->uji_limbah==0) ? '{\cf6 Tidak Taat}' : 'Taat');
            } else {
                $uji_limbah = '-';
                $uji_limbahr = '-';
            }
            if ($pencemaran_air->izin != NULL) {
                $izin_limbah_air = (($pencemaran_air->izin==0) ? '{\cf6 '.$this->adaConv($pencemaran_air->izin).'}' : $this->adaConv($pencemaran_air->izin));
                $iz = (($pencemaran_air->izin==0) ? '{\cf6 Tidak Taat}' : 'Taat');
            } else {
                $izin_limbah_air = '-';
                $iz = "-"; 
            }
            if ($pencemaran_air->daur_ulang_debit != NULL) {
                $debit_bap = $pencemaran_air->daur_ulang_debit." m3";
            } else {
                $debit_bap = '-';
            }
        } else {
            $ipal = '-';
            $au = '-';
            $bocor = '-';
            $uji_limbah = '-';
            $debit_bap = '-';
        }
        if($bap->dok_lingk==0){
            $a = '{\cf6 Tidak Ada}';
            $b = '{\cf6 Tidak Taat}';
        } else {
            $a = 'Ada';
            $b = 'Taat';
        }
        if ($izin_limbah!=NULL){
            $izin_limbah_b3 = "Ada";
            $debit_standard = $izin_limbah->debit_perbulan." m3";
        } else { $izin_limbah_b3 = '{\cf6 Tidak Ada}'; $debit_standard = "-"; }
        if ($izin_tps_limbah!=NULL){
            $izin_tps_b3 = "Ada";
        } else { $izin_tps_b3 = '{\cf6 Tidak Ada}';}
        if(($debit_bap!='-') && ($debit_standard!='-')){
            if($debit_bap>$debit_standard){
                $debitr="{\cf6 Tidak Taat}";
            } else { $debitr="Taat";}
        } else {
            $debitr="-";
        }

        # ------------------------------------ get lhu --------------------

        $this->load->model('m_lhu_air_limbah');
        # jumlah dokumen air limbah
        $where = 'id_industri = '.$bap->id_industri.' and jenis_lhu = "LHU Air Limbah" and substring(tgl_pembuatan, 4, 4) = "'.date('Y').'"';
        $sum_air = (count($this->m_lhu_air_limbah->get_detail($where)) > 0) ? count($this->m_lhu_air_limbah->get_detail($where)) : 0;        

        # catatan debit
        $this->load->model('m_debit_air_limbah');
        $where = 'id_industri = '.$bap->id_industri.' and substring(laporan_bulan, 4, 4) = "'.date('Y').'"';
        $sum_catatan = (count($this->m_debit_air_limbah->get_debit_air_limbah($where)) > 0) ? count($this->m_debit_air_limbah->get_debit_air_limbah($where)) : 0;

        $adm_air = ($sum_air >= 9 and $sum_catatan >= 9) ? 'Taat' : '{\cf6 Tidak Taat}';

        # jumlah dokumen udara
        $where = 'id_industri = '.$bap->id_industri.' and jenis_lhu = "LHU Udara Emisi" and substring(tgl_pembuatan, 4, 4) = "'.date('Y').'"';
        $sum_emisi = (count($this->m_lhu_air_limbah->get_detail($where)) > 0) ? count($this->m_lhu_air_limbah->get_detail($where)) : 0;

        $adm_emisi = ($sum_emisi >= 2) ? 'Taat' : '{\cf6 Tidak Taat}';

        # neraca b3
        $this->load->model('m_neraca_b3');
        $where = 'id_industri = '.$bap->id_industri.' and substring(periode_waktu, 1, 4) = "'.date('Y').'"';
        $sum_neraca = (count($this->m_neraca_b3->get_detail($where)) > 0) ? count($this->m_neraca_b3->get_detail($where)) : 0;

        $adm_neraca = ($sum_neraca == 3 or $sum_neraca == 4) ? 'Taat' : '{\cf6 Tidak Taat}';

        # manifest
        $this->load->model('m_manifest');
        $where = 'id_penghasil_limbah = '.$bap->id_industri.' and substring(tgl, 1, 4) = "'.date('Y').'"';
        $sum_manifest = (count($this->m_manifest->get_detail($where)) > 0) ? count($this->m_manifest->get_detail($where)) : 0;

        $adm_manifest = ($sum_manifest >= 3) ? 'Taat' : '{\cf6 Tidak Taat}';

        # pemenuhan baku mutu air
        $pemenuhan_air = (!$this->m_lhu_air_limbah->get_jumlah_teguran($bap->id_industri)) ? 'Terpenuhi' : '{\cf6 Tidak Terpenuhi}';
        $adm_pemenuhan_air = (!$this->m_lhu_air_limbah->get_jumlah_teguran($bap->id_industri)) ? 'Taat' : '{\cf6 Tidak Taat}';

        # pemenuhan baku mutu air
        $this->load->model('m_lhu_udara_emisi');
        $pemenuhan_emisi = (!$this->m_lhu_udara_emisi->get_jumlah_teguran($bap->id_industri)) ? 'Terpenuhi' : '{\cf6 Tidak Terpenuhi}';
        $adm_pemenuhan_emisi = (!$this->m_lhu_udara_emisi->get_jumlah_teguran($bap->id_industri)) ? 'Taat' : '{\cf6 Tidak Taat}';

        # -----------------------------------------------------------------

        $file_location = "assets/form/raport_perusahaan.rtf";
        $handle = fopen ($file_location, 'r');
        $content = fread($handle, filesize($file_location));

        // print_r($content); die();
        
        fclose($handle);
        
        $param = array (
            '#badan_hukum' => $bap->badan_hukum,
            '#nama_industri' => $bap->nama_industri,
            '#alamat' => $bap->alamat,
            '#usaha_kegiatan' => $bap->usaha_kegiatan,
            '#telepon' => $bap->tlp,
            '#tipe_industri' => $bap->jenis_industri,
            '#fax' => $bap->fax,
            '#pimpinan' => $bap->pimpinan,
            '#email' => $bap->email,
            '#celldl1' => $a,
            '#celldlr1' => $b,
            '#cell01' => $izin_limbah_b3,
            '#cell11' => $izin_limbah_air,
            '#cell12' => $iz,
            '#cell81' => $debit_standard,
            '#cell80' => $debit_bap,
            '#cell82' => $debitr,
            '#cell30' => $ipal,
            '#cell03' => $ipalr,
            '#cell40' => $au,
            '#cell04' => $aur,
            '#cell05' => $bocorr,
            '#cell60' => $uji_limbah,
            '#cell06' => $uji_limbahr,
            '#cell90' => $b,
            '#cellpe' => $objue,
            '#cellpr' => $objuer,
            '#cellcrb' => $objdc,
            '#celldcrr' => $objdcr,
            '#celluaue' => $objua,
            '#celluarue' => $objuar,
            '#cellbme' => $objbme,
            '#cellbmr' => $objbmer,
            '#cell50' => $bocor,
            '#cell160' => $izin_tps_b3,
            '#celltpsb3' => $iztp,
            '#celltpsbr3' => $iztpr,
            '#celldl2' => $a,
            '#celldlr2' => $b,
            '#celldl3' => $a,
            '#celldlr3' => $b,
            '#cell200' => $b3_simpan,
            '#cellb3r' => $b3_simpanr,
            '#cell220' => $ppn,
            '#cell230' => $sop,
            '#cell240' => $smbl,
            '#cell250' => $log,
            '#cell260' => $apar,
            '#cell270' => $b3_nm,
            '#cellppnr' => $ppnr,
            '#cellsop' => $sopr,
            '#cellsmbl' => $smblr,
            '#celllog' => $logr,
            '#cellapar' => $aparr,
            '#cellnm' => $b3_nmr,
            '#sum_air' => $sum_air,
            '#sum_catatan' => $sum_catatan,
            '#cell22' => $adm_air,
            '#sum_emisi' => $sum_emisi,
            '#adm_emisi' => $adm_emisi,
            '#sum_neraca' => $sum_neraca,
            '#adm_neraca' => $adm_neraca,
            '#sum_manifest' => $sum_manifest,
            '#adm_mani' => $adm_manifest,
            '#cell70' => $pemenuhan_air,
            '#cell71' => $adm_pemenuhan_air
        );

        foreach($param as $from => $to) {
           $content = str_replace($from, $to, $content);
        }
        
        # throw file
        $bdn_hukum = rtrim($bap->badan_hukum, ".");
        header('Content-type: application/rtf');
        header('Content-Disposition: attachment; filename="Raport_Industri_'.$bdn_hukum.'_'.$bap->nama_industri.'.rtf"');

        echo $content;
    }

    /**
     *  Author : Huda
     *  Modified by guntutur@gmail.com
     * @param   int
     * @return  array of object
     */
    private function get_bap($id_bap) {

        $bap = $this->bapDao->get_header_raport_bap($id_bap);

        $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

        $izin_limbah = $this->bapDao->get_obj_by_id("izin_pembuangan_limbah", "id_industri", $bap->id_industri);

        $izin_air_limbah = $this->bapDao->get_obj_by_id("izin_pembuangan_limbah", "id_industri", $bap->id_industri);

        $izin_tps_limbah = $this->bapDao->get_obj_by_id("izin_tps_limbah", "id_industri", $bap->id_industri);        

        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

        $data_cerobong = $this->bapDao->get_list_by_id("data_cerobong", "id_penc_udara", $pencemaran_udara->id_penc_udara);

        $uji_emisi = $this->bapDao->get_list_by_id("uji_emisi", "id_penc_udara", $pencemaran_udara->id_penc_udara);

        $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

        $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);

        // $jenis_limbah_b3 = $this->bapDao->get_list_by_id("jenis_limbah_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

        $tps_b3 = $this->bapDao->get_list_by_id("tps_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

        $data['bap'] = $bap;
        $data['pencemaran_air'] = $pencemaran_air;
        $data['izin_limbah'] = $izin_limbah;
        $data['izin_air_limbah'] = $izin_air_limbah;
        $data['izin_tps_limbah'] = $izin_tps_limbah;
        $data['data_cerobong'] = $data_cerobong;
        $data['uji_emisi'] = $uji_emisi;
        $data['uji_ambien'] = $uji_ambien;
        $data['pencemaran_pb3'] = $pencemaran_pb3;
        // $data['jenis_limbah_b3'] = $jenis_limbah_b3;
        $data['tps_b3'] = $tps_b3;

        return $data;
    }

    /**
     * Huda : convert boolean to string (Ada / Tidak Ada)
     *
     * @param   boolean
     * @return  String
     */
    private function adaConv($value) {
        if ($value == NULL) {
            return "";
        } else if ($value == 1) {
            return "Ada";
        } else if ($value == 0) {
            return "Tidak Ada";
        }
    }

}
