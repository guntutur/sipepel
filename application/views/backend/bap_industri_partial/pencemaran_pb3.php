<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 18/12/16
 * Time: 15:39
 */
?>

<div class="tab-pane" id="pencemaran_b3">
    <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
    <div class="form-group ">
        <label>a. Jenis Limbah B3 yang ditimbulkan</label>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th class="text-center">Jenis Limbah B3</th>
                <th class="text-center" style="width:15%">Jumlah (ton/hari)</th>
                <th class="text-center">Pengelolaan</th>
                <th class="text-left" style="width:30%">Pihak Ketiga</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($jenis_limbah_b3)) { for($x=0;$x<count($jenis_limbah_b3);$x++){ ?>
                <tr data-duplicate="jenis_limbah" data-duplicate-min="1">
                    <td>
                        <select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm">
                            <option value="" disabled selected>-- Pilih Jenis Limbah B3 --</option>
                            <?php foreach($jenis_limbah_b3_data_master as $uk):
                                echo "<option value='".$uk->ket."' ".(($jenis_limbah_b3[$x]->jenis==$uk->ket) ? 'selected' : '').">".$uk->ket."</option>";
                            endforeach;?>
                        </select>
                    </td>
                    <td><input type="text" name="jumlah[]" class="form-control input-sm has-feedback" value='<?php echo $jenis_limbah_b3[$x]->jumlah; ?>'/></td>
                    <td>
                        <select class='form-control input-sm' name='pengelolaan[]'><option value="">
                                <?php foreach ($bool as $f) {
                                    echo "<option value='".kelolaConv($f)."' " ;
                                    if (strcasecmp($jenis_limbah_b3[$x]->pengelolaan, kelolaConv($f)) == 0){
                                        echo "selected='true'";
                                    }
                                    echo ">".kelolaConv($f)."</option>";
                                } ?>
                        </select>
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-lg-6" style="width:63%">
                                <select name="pihak_ke3[]" class="form-control input-sm has-feedback">
                                    <option value=''></option>";
                                    <?php
                                    foreach ($pihak_ke3 as $p3) {
                                        echo "<option value='".$p3->badan_hukum." ".$p3->nama_industri."' ".(($jenis_limbah_b3[$x]->pihak_ke3==($p3->badan_hukum." ".$p3->nama_industri)) ? 'selected' : '').">".$p3->badan_hukum." ".$p3->nama_industri."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="co-lg-6" style="padding-right: 0;">
                                <div class=" text-right">
                                    <div class="btn-group" style="padding-right: 17px;">
                                        <button type="button" class="btn btn-sm btn-success" data-duplicate-add="jenis_limbah"><i class="fa fa-plus-circle"></i></button>
                                        <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="jenis_limbah"><i class="fa fa-times-circle"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php } } else { ?>
                <tr data-duplicate="jenis_limbah" data-duplicate-min="1">
                    <td>
                        <select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm limbah_b3">
                            <option value="" disabled selected>-- Pilih Jenis Limbah B3 --</option>
                            <?php foreach($jenis_limbah_b3_data_master as $uk): ?>
                                <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                    <td><input type="text" name="jumlah[]" class="form-control input-sm has-feedback" /></td>
                    <td><select class="form-control input-sm" name="pengelolaan[]"><option value=""></option><option value="Dikelola">Dikelola</option><option value="Tidak Dikelola">Tidak Dikelola</option></select></td>
                    <td>
                        <div class="row">
                            <div class="col-lg-6" style="width:63%">
                                <select name="pihak_ke3[]" class="form-control input-sm has-feedback">
                                    <option value=""></option>
                                    <?php
                                    foreach ($pihak_ke3 as $p3) {
                                        echo "<option value='".$p3->badan_hukum." ".$p3->nama_industri."'>".$p3->badan_hukum." ".$p3->nama_industri."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="co-lg-6" style="padding-right: 0;">
                                <div class=" text-right">
                                    <div class="btn-group" style="padding-right: 17px;">
                                        <button type="button" id="add_jenis_limbah" class="btn btn-sm btn-success" data-duplicate-add="jenis_limbah"><i class="fa fa-plus-circle"></i></button>
                                        <button type="button" id="remove_jenis_limbah" class="btn btn-sm btn-danger" data-duplicate-remove="jenis_limbah"><i class="fa fa-times-circle"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>


    <div class="form-group ">
        <label>b. Penyimpanan</label>
        <select class="form-control input-sm" name="b3_penyimpanan">
            <option value="Pada TPS Limbah B3" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_penyimpanan == 'Pada TPS Limbah B3') ? 'selected' : '' : ""; ?>>Pada TPS Limbah B3</option>
            <option value="Di luar TPS Limbah B3" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_penyimpanan == 'Di luar TPS Limbah B3') ? 'selected' : '' : ""; ?>>Di luar TPS Limbah B3</option>
        </select>
    </div>

    <div class="form-group ">
        <label>c. TPS Limbah B3</label>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th class="text-center">TPS Limbah B3</th>
                <th class="text-center">1</th>
                <th class="text-center">2</th>
                <th class="text-center">3</th>
                <th class="text-center">4</th>
            </tr>
            </thead>
            <tbody>
            <?php if(isset($tps_b3)) { ?>
                <tr>
                    <td>Perizinan</td>
                    <td><select name="tb_izin1" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[0]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[0]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[0]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                    <td><select name="tb_izin2" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[1]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[1]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[1]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                    <td><select name="tb_izin3" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[2]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[2]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[2]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                    <td><select name="tb_izin4" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[3]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[3]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[3]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Nomor</td>
                    <?php
                    $x=1;
                    foreach ($tps_b3 as $tb3) {
                        echo "<input type='hidden' name='id_tps_b3$x' value='".$tb3->id_tps_b3."' />";
                        echo "<td><input type='text' name='tb_izin_no$x' class='form-control input-sm has-feedback' value='".$tb3->izin_no."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <?php
                    $x=1;
                    foreach ($tps_b3 as $tb3) {
                        echo "<td><input type='text' name='tb_izin_tgl$x' class='form-control input-sm datepicker' data-date-format='yyyy-mm-dd' placeholder='yyyy-mm-dd' value='".$tb3->izin_tgl."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td>Jenis Limbah</td>
                    <?php
                    $x=1;
                    foreach ($tps_b3 as $tb3) {
                        echo "<td><input type='text' name='tb_jenis$x' class='form-control input-sm has-feedback' value='".$tb3->jenis."'/></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td colspan="4"><label>Lama Penyimpanan</label></td>
                </tr>
                <?php
                if($tps_b3!=null) {
                    $i = 1;
                    foreach ($tps_b3 as $tb3) {
                        $test = unserialize($tb3->lama_spn);
                        foreach ($test['tps'] as $v) {
                            $lama[] = $v;
                        }
                        $new['lama_tps_' . $i] = $lama;
                        unset($lama);
                        $i++;
                    }

                    $test = unserialize($tps_b3[0]->lama_spn);

                    $n = 0;
                    if ($test['jenis_limbah']!=null) {
                        foreach ($test['jenis_limbah'] as $key => $value) {
                            $f = 1;
                            $newest['jenis'] = $value;
                            foreach ($new as $x => $y) {
                                $newest['tps_' . $f] = $y[$n];
                                $f++;
                            }
                            $all_new[] = $newest;

                            unset($t);
                            $n++;
                        }
                    }
                }
                ?>
                <?php if(isset($all_new)) { foreach ($all_new as $key => $value){ ?>

                    <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                        <td>
                            <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                <?php foreach($jenis_limbah_b3_data_master as $uk):
                                    echo "<option value='".$uk->ket."'";
                                    if ($value['jenis'] == $uk->ket) echo "selected";
                                    echo ">$uk->ket</option>";
                                endforeach;?>
                                <?php // foreach($jenis_limbah_b3 as $uk): ?>
                                <!-- <option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option> -->
                                <?php // endforeach;?>
                            </select>
                        </td>
                        <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" value="<?php echo $value['tps_1']?>" /></td>
                        <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" value="<?php echo $value['tps_2']?>" /></td>
                        <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" value="<?php echo $value['tps_3']?>" /></td>
                        <td>
                            <div class="row">
                                <div class="col-lg-6" style="width:60%">
                                    <div class="text-left">
                                        <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" value="<?php echo $value['tps_4']?>" />
                                    </div>
                                </div>
                                <div class="co-lg-6">
                                    <div class=" text-right" >
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php  } } else { ?>
                    <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                        <td>
                            <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                <?php foreach($jenis_limbah_b3_data_master as $uk): ?>
                                    <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>
                            </select>
                        </td>
                        <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" /></td>
                        <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" /></td>
                        <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" /></td>
                        <td>
                            <div class="row">
                                <div class="col-lg-6" style="width:60%">
                                    <div class="text-left">
                                        <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" />
                                    </div>
                                </div>
                                <div class="co-lg-6">
                                    <div class=" text-right" >
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php  }  ?>
                <?php
                $x=1;
                foreach($tps_b3 as $jl){
                    $koord_derajat_s[$x] = $jl->koord_derajat_s;
                    $koord_jam_s[$x] = $jl->koord_jam_s;
                    $koord_menit_s[$x] = $jl->koord_menit_s;
                    $koord_derajat_e[$x] = $jl->koord_derajat_e;
                    $koord_jam_e[$x] = $jl->koord_jam_e;
                    $koord_menit_e[$x] = $jl->koord_menit_e;
                    $x++;
                }
                ?>
                <tr>
                    <td>Koordinat</td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s1" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[1]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s1" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[1]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s1" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[1]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e1" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[1]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e1" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[1]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e1" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[1]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s2" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[2]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s2" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[2]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s2" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[2]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e2" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[2]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e2" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[2]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e2" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[2]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s3" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[3]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s3" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[3]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s3" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[3]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e3" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[3]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e3" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[3]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e3" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[3]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s4" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[4]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s4" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[4]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s4" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[4]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e4" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[4]?>" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e4" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[4]?>" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e4" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[4]?>" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Ukuran (m2)</td>
                    <?php
                    $x=1;
                    foreach ($tps_b3 as $tb3) {
                        echo "<td><input type='text' name='tb_ukuran$x' value='".$tb3->ukuran."' class='form-control input-sm has-feedback' /></td>";
                        $x++;
                    }
                    ?>
                </tr>
                <tr>
                    <td colspan="4"><label>Kelengkapan</label></td>
                </tr>
                <tr>
                    <td>Papan Nama dan Koordinat</td>
                    <?php
                    $c = 1;
                    foreach ($tps_b3 as $dt) {
                        $ppn[$c] = adaConv($dt->ppn_nama_koord);
                        $simbol[$c] = adaConv($dt->simbol_label);
                        $salcer[$c] = adaConv($dt->saluran_cecer);
                        $bakcer[$c] = adaConv($dt->bak_cecer);
                        $kemiringan[$c] = adaConv($dt->kemiringan);
                        $sop[$c] = adaConv($dt->sop_darurat);
                        $log[$c] = adaConv($dt->log_book);
                        $apar[$c] = adaConv($dt->apar_p3k);
                        $p3k[$c] = adaConv($dt->p3k);
                        $c++;
                    }

                    for($p=1; $p<=4; $p++) {
                        if($ppn[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_ppn_nama_koord$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($ppn[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_ppn_nama_koord$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Simbol dan Label</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($simbol[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_simbol_label$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($simbol[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_simbol_label$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Saluran Ceceran Air Limbah</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($salcer[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_saluran_cecer$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($salcer[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_saluran_cecer$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Bak Penampung Ceceran Air Limbah</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($bakcer[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_bak_cecer$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($bakcer[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_bak_cecer$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Kemiringan</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($kemiringan[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_kemiringan$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($kemiringan[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_kemiringan$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>SOP Pengelolaan dan Tanggap Darurat</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($sop[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_sop_darurat$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($sop[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_sop_darurat$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Log Book</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($log[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_log_book$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($log[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_log_book$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>APAR</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($apar[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_apar_p3k$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($apar[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_apar_p3k$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
                <tr>
                    <td>Kotak P3K</td>
                    <?php
                    for($p=1; $p<=4; $p++) {
                        if($p3k[$p]!=null) {
                            echo "<td><select class='form-control input-sm' name='tb_p3k$p'>";
                            $n=1;
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' " ;
                                if (strcasecmp($p3k[$p], adaConv($f)) == 0){
                                    echo "selected='true'";
                                }
                                echo ">".adaConv($f)."</option>";
                                $n++;
                            }
                            echo "</select></td>";
                        } else {
                            echo "<td><select class='form-control input-sm' name='tb_p3k$p'>";
                            foreach ($bool as $f) {
                                echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                            }
                            echo "</select></td>";
                        }
                    }
                    ?>
                </tr>
            <?php } else { ?>
                <tr>
                    <td>Perizinan</td>
                    <td><select name="tb_izin1" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_izin2" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_izin3" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_izin4" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Nomor</td>
                    <td><input type="text" name="tb_izin_no1" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_izin_no2" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_izin_no3" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_izin_no4" class="form-control input-sm has-feedback" /></td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td><input type="text" name="tb_izin_tgl1" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                    <td><input type="text" name="tb_izin_tgl2" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                    <td><input type="text" name="tb_izin_tgl3" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                    <td><input type="text" name="tb_izin_tgl4" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                </tr>
                <tr>
                    <td>Jenis Limbah B3</td>
                    <td><input type="text" name="tb_jenis1" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_jenis2" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_jenis3" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_jenis4" class="form-control input-sm has-feedback" /></td>
                </tr>
                <tr class="tb_lama_spn">
                    <td colspan="4"><label>Lama Penyimpanan (hari)</label></td>
                </tr>
                <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                    <td>
                        <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                            <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                            <?php foreach($jenis_limbah_b3_data_master as $uk): ?>
                                <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                    <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" /></td>
                    <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" /></td>
                    <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" /></td>
                    <td>
                        <div class="row">
                            <div class="col-lg-6" style="width:60%">
                                <div class="text-left">
                                    <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" />
                                </div>
                            </div>
                            <div class="co-lg-6">
                                <div class=" text-right" >
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                        <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Koordinat</td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s1" id="derajat_s" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s1" id="jam_s" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s1" id="menit_s" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e1" id="derajat_e" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e1" id="jam_e" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e1" id="menit_e" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s2" id="derajat_s" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s2" id="jam_s" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s2" id="menit_s" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e2" id="derajat_e" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e2" id="jam_e" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e2" id="menit_e" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s3" id="derajat_s" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s3" id="jam_s" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s3" id="menit_s" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e3" id="derajat_e" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e3" id="jam_e" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e3" id="menit_e" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12 ">
                                <label>S</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_s4" id="derajat_s" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_s4" id="jam_s" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_s4" id="menit_s" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-right: 14px;">
                            <div class="col-lg-12">
                                <label>E</label>
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_derajat_e4" id="derajat_e" class="form-control input-sm" value="" placeholder="00&deg;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_jam_e4" id="jam_e" class="form-control input-sm" value="" placeholder="00&#39;">
                                    </div>
                                    <div class="col-lg-4" style="padding-right: 0;">
                                        <input type="text" name="koord_menit_e4" id="menit_e" class="form-control input-sm" value="" placeholder="00&quot;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Ukuran (m2)</td>
                    <td><input type="text" name="tb_ukuran1" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_ukuran2" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_ukuran3" class="form-control input-sm has-feedback" /></td>
                    <td><input type="text" name="tb_ukuran4" class="form-control input-sm has-feedback" /></td>
                </tr>
                <tr>
                    <td colspan="4"><label>Kelengkapan</label></td>
                </tr>
                <tr>
                    <td>Papan Nama dan Koordinat</td>
                    <td><select name="tb_ppn_nama_koord1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_ppn_nama_koord2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_ppn_nama_koord3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_ppn_nama_koord4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Simbol dan Label</td>
                    <td><select name="tb_simbol_label1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_simbol_label2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_simbol_label3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_simbol_label4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Saluran Ceceran Air Limbah</td>
                    <td><select name="tb_saluran_cecer1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_saluran_cecer2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_saluran_cecer3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_saluran_cecer4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Bak Penampung Ceceran Air Limbah</td>
                    <td><select name="tb_bak_cecer1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_bak_cecer2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_bak_cecer3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_bak_cecer4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Kemiringan</td>
                    <td><select name="tb_kemiringan1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_kemiringan2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_kemiringan3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_kemiringan4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>SOP Pengelolaan dan Tanggap Darurat</td>
                    <td><select name="tb_sop_darurat1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_sop_darurat2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_sop_darurat3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_sop_darurat4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Log Book</td>
                    <td><select name="tb_log_book1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_log_book2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_log_book3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_log_book4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>APAR</td>
                    <td><select name="tb_apar_p3k1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_apar_p3k2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_apar_p3k3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_apar_p3k4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td>Kotak P3K</td>
                    <td><select name="tb_p3k1" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_p3k2" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_p3k3" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                    <td><select name="tb_p3k4" class="form-control input-sm"><option value=""></option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>e. Pengujian Kualitas Limbah B3</label>
            <select class="form-control input-sm" name="b3_uji">
                <option value="Sudah" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_uji == '1') ? 'selected' : '' : ""; ?>>Sudah</option>
                <option value="Belum" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_uji == '0') ? 'selected' : '' : ""; ?>>Belum</option>
            </select>
        </div>
        <div class="form-group col-lg-6">
            <label>Jenis Pengujian</label>
            <select class="form-control input-sm" name="b3_uji_jenis" id="b3_uji_jenis" multiple="multiple"  style="border: 0!important">
                <option value=""></option>
                <?php
                if (isset($pencemaran_pb3)) {$jenis_uji = explode(', ', $pencemaran_pb3->b3_uji_jenis);}
                foreach ($jenis_pengujian as $j){
                    echo "<option value='$j->ket' ";
                    if (isset($pencemaran_pb3)) {
                        for ($i = 0; $i < count($jenis_uji); $i++) {
                            if ($jenis_uji[$i] == $j->ket) {
                                echo "selected";
                            }
                        }
                    }
                    echo ">$j->ket</option>";
                }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group ">
        <label>Parameter Uji</label>
        <input type="text" name="b3_uji_param" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->b3_uji_param : ""?>" />
    </div>
    <div class="form-group ">
        <label>Laboratorium Penguji</label>
        <select name="b3_uji_lab" class="form-control input-sm has-feedback">
            <option value=""></option>>
            <?php
            foreach ($lab as $l) {
                echo "<option value='$l->nama_lab' ";
                if (isset($pencemaran_pb3)) {
                    if ($pencemaran_pb3->b3_uji_lab == $l->nama_lab) {
                        echo "selected";
                    }
                }
                echo ">".$l->nama_lab."</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group ">
        <label>Waktu Pengujian</label>
        <input type="text" name="b3_uji_waktu" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->b3_uji_waktu : ""?>" />
    </div>
    <div class="form-group ">
        <label>f. Neraca Limbah B3</label>
        <select class="form-control input-sm" name="b3_neraca">
            <option value="Ada" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_neraca == '1') ? 'selected' : '' : ""; ?>>Ada</option>
            <option value="Tidak ada" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_neraca == '0') ? 'selected' : '' : ""; ?>>Tidak Ada</option>
        </select>
    </div>

    <div class="form-group ">
        <label>g. Manifest</label>
        <select class="form-control input-sm" name="b3_manifest">
            <option value="Ada" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_manifest == '1') ? 'selected' : '' : ""; ?>>Ada</option>
            <option value="Tidak ada" <?php echo isset($pencemaran_pb3) ? ($pencemaran_pb3->b3_manifest == '0') ? 'selected' : '' : ""; ?>>Tidak Ada</option>
        </select>
    </div>
    <?php
    $lapor = $bool;
    $lapor[] = '2';
    ?>
    <div class="form-group ">
        <label>h. Pelaporan Neraca dan Manifest</label>
        <select class="form-control input-sm" name="b3_lapor_nm">
            <?php
            for($d=0; $d<3; $d++){
                echo "<option value='".rutinConv($lapor[$d])."'";
                if (isset($pencemaran_pb3)) {
                    if (strcasecmp($pencemaran_pb3->b3_lapor_nm, rutinConv($lapor[$d])) == 0) {
                        echo "selected";
                    }
                }
                echo ">".rutinConv($lapor[$d])."</option>";
            }
            ?>
        </select>
    </div>
    <?php $new = array();
    if (isset($pencemaran_pb3) && !empty($pencemaran_pb3)) {
        $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
        $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
        for ($i = 0; $i < count($arr_padat_jenis); $i++) {
            $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
        }
    }
    ?>

    <label>i. Limbah Padat Lain</label>

    <table class="table table-th-block">
        <thead>
        <tr>
            <th class="text-center">Jenis</th>
            <th class="text-center">Jumlah (ton/hari)</th>
            <th class="text-center"></th>
        </tr>
        </thead>
        <tbody>
        <?php if (isset($new)) { foreach ($new as $key => $value) { ?>
            <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                <td class="col-lg-5"><input type="number" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                <td class="col-lg-2">
                    <div class=" text-right">
                        <div class="btn-group" style="padding-right: 17px;">
                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } } else { ?>
            <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" /></td></td>
                <td class="col-lg-5"><input type="number" name="padat_jumlah[]" class="form-control input-sm has-feedback" /></td>
                <td class="col-lg-2">
                    <div class=" text-right">
                        <div class="btn-group" style="padding-right: 17px;">
                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <div class="form-group ">
        <label>d. Pengelolaan</label>
        <select class="form-control input-sm" id="pjpl" name="padat_kelola">
            <?php if (isset($pencemaran_pb3)) {
                $padat_kelola = $pencemaran_pb3->padat_kelola;
                echo $padat_kelola;
                if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                    foreach ($pengelolaan as $p) {
                        echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                    }
                    echo "</select>";
                    echo "<input type='text' name='padat_kelola_lain' id='pjpl2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                } else {
                    $temp = "Lainnya";
                    foreach ($pengelolaan as $p) {
                        echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                    }
                    echo "</select>";
                    echo "<input type='text' name='padat_kelola_lain' id='pjpl2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
                } } else { ?>
            <?php foreach ($pengelolaan as $pk) {
                echo "<option value='$pk->ket'>$pk->ket</option>";
            } ?>
        </select>
        <input type="text" name="padat_kelola_lain" id="pjpl2" class="form-control input-sm has-feedback" style="display:none"/>
        <?php } ?>
    </div>

    <div class="form-group ">
        <label>j. Lain-lain</label>
        <input type="text" name="lain_lain_pb3" id="" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->lain_lain : ""?>"/>
    </div>

    <div class="row">
        <div class="col-sm-6 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Udara</a>
        </div>
        <div class="col-sm-6 text-right">
            <input id="save-pencemaran_b3" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran LB3"/>
        </div>
        <!--need to disable because edit_from_form assumed all section (datum, pencemaran air, pencemaran udara, pencemaran pb3) already has their own data-->
        <!--<div class="col-sm-4 text-right">
            <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
        </div>-->
    </div>
</div>
