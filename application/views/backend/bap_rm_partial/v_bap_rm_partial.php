<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 20/12/16
 * Time: 22:23
 */
?>

<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAP Rumah Makan&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Rumah Makan</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen BAP</li>
                <li class="active">BAP Rumah Makan</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" style=""><a href="#edit_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                    <li><button type="button" class="btn btn-success" onclick="window.location='<?php echo base_url('backend/bap_rm/exit_check_status/'.$bap->id_bap); ?>'"><i class="glyphicon glyphicon-check"></i>&nbsp;&nbsp;Selesai</button></li>

                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="edit_new" style="">
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-xs-3" id="vertical_tab" style="display: none"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a href="#data_umum" id="Data Umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                    <li><a href="#pencemaran_air" id="Pencemaran Air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                    <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                    <li><a href="#pencemaran_b3" id="Limbah Padat dan B3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                    <li><a href="#lain_lain" id="Keterangan Tambahan" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Keterangan Tambahan</a></li>
                                </ul>
                            </div>
                            <div class="col-xs-12">
                                <!-- Tab panes -->
                                <form role="form" action="<?php echo base_url('/backend/bap_rm/edit_from_form') ?>" method="post" id="add_form" enctype="multipart/form-data">
                                    <div class="tab-content">

                                        <?php $this->load->view('backend/bap_rm_partial/data_umum'); ?>

                                        <?php $this->load->view('backend/bap_rm_partial/pencemaran_air'); ?>

                                        <?php $this->load->view('backend/bap_rm_partial/pencemaran_udara'); ?>

                                        <?php $this->load->view('backend/bap_rm_partial/pencemaran_pb3'); ?>

                                    </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>


        <?php
        function adaConv($value) {
            if ($value == true) {
                return "Ada";
            } else {
                return "Tidak Ada";
            }
        }

        # function to convert boolean to string (Ya / Tidak)
        function yaConv($value) {
            if ($value == true) {
                return "Ya";
            } else {
                return "Tidak";
            }
        }

        function rutinConv($value) {
            if ($value == '1') {
                return "Rutin";
            } else if ($value == '0'){
                return "Tidak Rutin";
            } else {
                return "Tidak Melaporkan";
            }
        }

        ?>

    </div><!-- /.the-box .default -->
    <!-- END DATA TABLE -->

</div><!-- /.container-fluid -->
</div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->
<script type="text/javascript">
    $("#timepick1").timepicker({
        showMeridian: false
    });

    $("#timepick2").timepicker({
        showMeridian: false
    });
</script>
<script type="text/javascript">

    $(document).ready(function () {
        /* prev next submit handler */
        $(".save-partially").click(function () {
            /*manual validation for save partially*/
            /*show again head-info*/
            $("#save-part-dismiss").hide();
            $("#head-info").show();
            $("#save-part-button").show();
            /*data umum*/
            if($(this).attr('id')=='save-datum') {
                if (!$("input[name=bap_tgl]").val()) { alert("Simpan Sebagian : Tanggal Pengawasan tidak boleh kosong!"); return false };
                if (!$("select[name=id_pegawai]").val()) { alert("Simpan Sebagian : Petugas Pengawas boleh kosong!"); return false };
                if ($("#id_industri").val() == 'Empty') { alert("Simpan Sebagian : Nama Usaha/Kegiatan boleh kosong!"); return false };
                if (!$("input[name=p2_nama]").val()) { alert("Simpan Sebagian : Nama Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=p2_telp]").val()) { alert("Simpan Sebagian : Telepon Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=p2_jabatan]").val()) { alert("Simpan Sebagian : Jabatan Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=jml_meja]").val()) { alert("Simpan Sebagian : Jumlah Meja/Kursi tidak boleh kosong!"); return false };

                if($("#dl1").is(":checked")) {
                    if (!$("input[name=jdl").val()) { alert("Simpan Sebagian : Pilih Jenis Dokumen Lingkungan!"); return false };
                    if (!$("input[name=dlt").val()) { alert("Simpan Sebagian : Tahun Dokumen Lingkungan tidak boleh kosong!"); return false };
                }

                if($("#il1").is(":checked")) {
                    if (!$("input[name=il_tahun").val()) { alert("Simpan Sebagian : Tahun Izin Lingkungan tidak boleh kosong!"); return false };
                }

                if($("#iu1").is(":checked")) {
                    if (!$("input[name=iu_tahun").val()) { alert("Simpan Sebagian : Tahun Izin Usaha tidak boleh kosong!"); return false };
                }

                var info = $("#save-datum").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#data_umum");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else if($(this).attr('id')=='save-pencemaran_air') { /*pencemaran air*/
                if (!$("select[name=bdn_terima]").val()) { alert("Simpan Sebagian : Badan Air Penerima tidak boleh kosong!"); return false };

                if($("#sarolim1").is(":checked")) {
                    if (!$("input[name=sarana_jenis]").val()) { alert("Simpan Sebagian : Jenis Sarana tidak boleh kosong!"); return false };
                    if (!$("input[name=kapasitas]").val()) { alert("Simpan Sebagian : Kapasitas Sarana tidak boleh kosong!"); return false };
                }

                var info = $("#save-pencemaran_air").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_air");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else  if ($(this).attr('id')=='save-pencemaran_udara') {
                if($("#bmp1").val()=="0") {
                    if (!$("#bm_param1").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Upwind) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period1").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Upwind) tidak boleh kosong!"); return false };
                }

                if($("#bmp2").val()=="0") {
                    if (!$("#bm_param2").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Site) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period2").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Site) tidak boleh kosong!"); return false };
                }

                if($("#bmp3").val()=="0") {
                    if (!$("#bm_param3").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Downwind) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period3").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Downwind) tidak boleh kosong!"); return false };
                }

                var info = $("#save-pencemaran_udara").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_udara");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else if($(this).attr('id')=='save-pencemaran_b3') {
                if (!$("input[name=padat_sarana_tong]").val()) { alert("Simpan Sebagian : Tong/Bak Sampah tidak boleh kosong!"); return false };
                if (!$("input[name=padat_sarana_tps]").val()) { alert("Simpan Sebagian : TPS tidak boleh kosong!"); return false };

                var info = $("#save-pencemaran_b3").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_b3");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            }

            $("#save-part").modal("show");

            $("button#button-save-part").unbind().click(function () {
                /*collect ids*/
                var ids = {
                    'id_bap': $("#id_bap").val(),
                    'id_bap_rm': $("#id_bap_rm").val(),
                    'id_pencemaran_air': $("#id_pencemaran_air").val(),
                    'id_pencemaran_udara': $("#id_pencemaran_udara").val(),
                    'id_plb3': $("#id_plb3").val()
                };
                $('.spinner-loader').show();

                $.post('<?php echo base_url("backend/bap_rm/save_part")?>', {
                    data : serializedData,
                    dest : dest,
                    ids : ids
                }, function (ret) {
                    if(ret.status) {
                        console.log(ret);
                        /*append ids to hidden field*/
                        $("#id_bap").val(ret.id.id_bap);
                        $("#id_bap_rm").val(ret.id.id_bap_rm);
                        $("#id_pencemaran_air").val(ret.id.id_pencemaran_air);
                        $("#id_pencemaran_udara").val(ret.id.id_pencemaran_udara);
                        $("#id_plb3").val(ret.id.id_plb3);

                        //$("#save-part").modal("hide");
                        $("#head-info").hide();
                        $("#save-part-button").hide();
                        $("#save-part-dismiss").show();
                        $("#save-part-info").text("Data "+info[1]+" "+info[2]+" berhasil disimpan");
                        var nextId = $elem.next().attr("id");
                        $('[href=#'+nextId+']').tab('show');
                    } else {
                        /*force to check if datum is not yet saved*/
                        $("#save-part").modal("hide");
                        $("#save-datum-first").modal("show");
                    }
                }, "json");

                $('.spinner-loader').hide();
            });
        });
        /* prev next submit handler */

        var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search_industri_bap/19',
            onSearchStart: function (query) {
                $('#id_industri').val(null);
            },
            onSelect: function (suggestion) {
                $('#id_industri').val(suggestion.data).change();
                var id_last_input = $('#id_industri').val();
                $('#id_last_input').val(id_last_input);
                console.log($('#id_last_input').val());
            }
        });

        $("select[name=id_industri]").change(function(){
            $(this).get_detail_industri(this, '#container2');
        });

        $('#id_petugas_bap1').multiselect({
            checkboxName: 'petugas_bap[]'
        });

        $('#id_petugas_bap2').multiselect({
            checkboxName: 'petugas_bap[]'
        });

        var dl = $('input[name=dl]:checked').val();
        if (dl == "ada") {
            $('#jdl').show();
        } else {
            $('#jdl').hide();
        }

        var il = $('input[name=il]:checked').val();
        if (il == "ada") {
            $('#il_tahun').show();
        } else {
            $('#il_tahun').hide();
            $('#il_tahun').val(null);
        }

        var iu = $('input[name=iu]:checked').val();
        if (iu == "ada") {
            $('#iu_tahun').show();
        } else {
            $('#iu_tahun').hide();
            $('#iu_tahun').val(null);
        }

        var sarolim = $('input[name=sarolim]:checked').val();
        if (sarolim == "ada") {
            $('#sarolim').show();
        } else {
            $('#sarolim').hide();
            $('#sarana_jenis').val(null);
            $('#kapasitas').val(null);
            $('#koord_outlet_s').val(null);
            $('#koord_outlet_e').val(null);
        }

        $('#dl1').click(function (e) {
            $('#jdl').show();
            $('#jdl_name').val(null);
            $('#dlt').val(null);
        });

        $('#dl2').click(function (e) {
            $('#jdl').hide();
            $('#jdl_name').val(null);
            $('#dlt').val(null);
        });

        $('#sarolim1').click(function (e) {
            // alert(e);
            $('#sarolim').show();
        });

        $('#sarolim2').click(function (e) {
            // alert(e);
            $('#sarolim').hide();
            $('#sarana_jenis').val(null);
            $('#kapasitas').val(null);
            $('#koord_outlet_s').val(null);
            $('#koord_outlet_e').val(null);
        });

        $('#il1').click(function (e) {
            $('#il_tahun').show();
            $('#ilt').val(null);
        });

        $('#il2').click(function (e) {
            $('#il_tahun').hide();
            $('#ilt').val(null);
        });

        $('#iu1').click(function (e) {
            $('#iu_tahun').show();
            $('#iut').val(null);
        });

        $('#iu2').click(function (e) {
            $('#iu_tahun').hide();
            $('#iut').val(null);
        });

        $('#pjpl').change(function() {
            if($(this).val()  == 'Lainnya') {
                $('#pjpl2').show();
                $('#pjpl2').focus();
            } else {
                $('#pjpl2').hide();
            }
        });

    });
</script>

<div class="modal fade" id="save-part" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Simpan data BAP Sebagian</h4>
            </div>
            <div class="modal-body">
                <p id="head-info">Data berikut akan disimpan: </p>
                <p id="save-part-info"></p>
                <?php if (!isset($last_insert)) { ?>
                <input type="hidden" id="id_bap" value="<?php echo $bap->id_bap; ?>" />
                <input type="hidden" id="id_bap_rm" value="<?php echo $bap_rm->id_bap_rm; ?>" />
                <input type="hidden" id="id_pencemaran_air" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->id_penc_air : ''; ?>" />
                <input type="hidden" id="id_pencemaran_udara" value="<?php echo isset($pencemaran_udara) ? $pencemaran_udara->id_penc_udara: ''; ?>" />
                <input type="hidden" id="id_plb3" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->id_penc_pb3 : ''; ?>" />
                <!--duplicate input hidden below is on purpose-->
                <!--so if user wants to submit all data at once it will be-->
                <!--directed to the 'usual' edit form, notice that the form name is not changed-->
                <input type="hidden" id="edit_id" name="id" value="<?php echo $bap->id_bap; ?>" />
                <!--only bap not having id, because the id is already taken for save partially above-->
                <input type="hidden" name="id_bap_rm" value="<?php echo $bap_rm->id_bap_rm; ?>" />
                <input type="hidden" id="id_penc_air" name="id_penc_air" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->id_penc_air : ''; ?>" />
                <input type="hidden" id="id_penc_udara" name="id_penc_udara" value="<?php echo isset($pencemaran_udara) ? $pencemaran_udara->id_penc_udara: ''; ?>" />
                <input type="hidden" id="id_penc_pb3" name="id_penc_pb3" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->id_penc_pb3 : ''; ?>" />
                <?php } else { ?>
                    <input type="hidden" id="id_bap" value="" />
                    <input type="hidden" id="id_bap_rm" value="" />
                    <input type="hidden" id="id_pencemaran_air" value="" />
                    <input type="hidden" id="id_pencemaran_udara" value="" />
                    <input type="hidden" id="id_plb3" value="" />
                <?php } ?>
                <center><img class="spinner-loader" style="display: none;" src="<?php echo base_url('assets/img/spinner.gif') ?>" class="img-responsive" alt="memuat"></center>
            </div>
            <div class="modal-footer">
                <div id="save-part-button">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" id="button-save-part" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;Simpan</button>
                </div>
                <div id="save-part-dismiss" style="display: none">
                    <button type="button" class="btn btn-success" data-dismiss="modal"><i class="glyphicon glyphicon-check"></i>&nbsp;&nbsp;Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="save-datum-first" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
            </div>
            <div class="modal-body">
                <p>Silahkan simpan Data Umum BAP terlebih dahulu! </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="button-save-datum" class="btn btn-default" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;&nbsp;Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->
