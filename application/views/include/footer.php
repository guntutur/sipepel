		<!--
		===========================================================
		Placed at the end of the document so the pages load faster
		===========================================================
		-->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->

		<?php echo $this->load->view('include/form_validation'); ?>

		<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.file-input.js ') ?>"></script>                
		<script src="<?php echo base_url('assets/plugins/retina/retina.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/nicescroll/jquery.nicescroll.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/backstretch/jquery.backstretch.min.js') ?>"></script>
 
		<!-- PLUGINS -->
		<script src="<?php echo base_url('assets/plugins/skycons/skycons.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/prettify/prettify.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/magnific-popup/jquery.magnific-popup.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/owl-carousel/owl.carousel.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/chosen/chosen.jquery.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/icheck/icheck.min.js') ?>"></script>		
		<script src="<?php echo base_url('assets/plugins/mask/jquery.mask.min.js') ?>"></script>
		
		
		<script src="<?php echo base_url('assets/plugins/markdown/markdown.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/markdown/to-markdown.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/markdown/bootstrap-markdown.js') ?>"></script>		
		<script src="<?php echo base_url('assets/plugins/newsticker/jquery.newsTicker.min.js') ?>"></script>		
		
		<!-- FULL CALENDAR JS -->
		<script src="<?php echo base_url('assets/plugins/fullcalendar/lib/jquery-ui.custom.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/full-calendar.js') ?>"></script>	
		<script src="<?php echo base_url('assets/js/jquery.duplicate.min.js') ?>"></script>
    
        <!-- jQuery Validation -->        
        <script src="<?php echo base_url('assets/js/jquery.validate.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/nailthumb/jquery.nailthumb.1.1.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.flexslider.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.autocomplete.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/moment.js') ?>"></script>							
		<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.js') ?>"></script>
		<script src="<?php echo base_url('assets/plugins/FeedEk/FeedEk.js') ?>"></script>					
		<script src="<?php echo base_url('assets/plugins/multiselect/bootstrap-multiselect.js') ?>"></script>				
				
		<!-- MAIN APPS JS -->
		<script src="<?php echo base_url('assets/js/apps.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/demo-panel.js') ?>"></script>	
        
	</body>
</html>