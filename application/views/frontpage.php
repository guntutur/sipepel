
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div style="float:left;"><a class="navbar-brand" href="" style="padding: 0; margin: 0;">
					<img src="<?php echo base_url('assets/img/logo_sil.png'); ?>" alt="Sistem Informasi Lingkungan" style="height: 50px; padding: 0; margin:0;" />
				</a></div>

				
			</div>
			<div style="float:right;"><a class="navbar-brand navbar-right" href="<?php echo base_url('backend/'); ?>">Login</a></div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<!-- <div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#about">About</a></li>
					<li><a href="#services">Services</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</div> --><!-- /.navbar-collapse -->
		</div><!-- /.container -->
	</nav>

	<script type="text/javascript">
		$(document).ready(function($) {			
			$('.flexslider').flexslider({
				animation: "slide",				
				itemWidth: 200,
				controlNav: false			
			});			
		});
	</script>

    <div class="container">

		<div class="row" style="margin-top: 20px;">
			<div class="col-lg-12">			
				<!-- Place somewhere in the <body> of your page -->
				<h4 class="small-title"><strong><i class="fa fa-camera"></i>&nbsp;&nbsp;FOTO KEGIATAN PEMBINAAN DAN PENGAWASAN</strong></h4>									
				<div class="flexslider">
					<ul class="slides">
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-1.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-2.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-3.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-4.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-5.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-6.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-7.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-8.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-9.jpg'); ?>" />
						</li>
						<li>
						  <img src="<?php echo base_url('assets/img/photo/small/img-10.jpg'); ?>" />
						</li>
						<!-- items mirrored twice, total of 12 -->
					</ul>
				</div>
				<!-- <div class="well-ticker">10/10/2014 13:14 WIB - Seorang nenek dibunuh dengan kejamnya</div>
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">				
						<h4 class="small-title"><strong><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;PROPORSI INDUSTRI</strong></h4>
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">				
						<h4 class="small-title"><strong><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;PROPORSI INDUSTRI BASAH</strong></h4>
					</div>					
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">				
						<h4 class="small-title"><strong><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;PRESENTASE PENAATAN 2013</strong></h4>
					</div>					
				</div> -->
			</div>
		</div>


		<div class="row">   
		    <!--<div class="col-lg-6"> -->

		        <!-- BEGIN WEATHER WIDGET 2 -->
		        <!--<div class="panel panel-info panel-square panel-no-border">
		            <div class="panel-heading">
		                <h3 class="panel-title white-text">SOREANG CITY, ID</h3>
		            </div>
		            <div class="panel-body bg-info">
		                <div class="row">
		                    <div class="col-sm-7">
		                        <div class="weather-widget horizontal">
		                            <div class="row">
		                                <div class="col-xs-6 text-center">
		                                    <canvas id="partly-cloudy-day" width="115" height="115"></canvas>
		                                </div>
		                                <div class="col-xs-6">
		                                    <h2 class="bolded degrees white-text">32<i class="wi-degrees"></i></h2>
		                                    <p class="white-text">Partly cloudy</p>
		                                    <p class="white-text">15km/h - 37%</p>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-sm-5">
		                        <div class="row">
		                            <div class="col-xs-4 text-center">
		                                <h4 class="white-text">SAT</h4>
		                                <canvas id="cloudy" width="35" height="35"></canvas>
		                                <h4 class="bolded white-text">30<i class="wi-degrees"></i></h4>
		                            </div>
		                            <div class="col-xs-4 text-center">
		                                <h4 class="white-text">SUN</h4>
		                                <canvas id="wind" width="35" height="35"></canvas>
		                                <h4 class="bolded white-text">28<i class="wi-degrees"></i></h4>
		                            </div>
		                            <div class="col-xs-4 text-center">
		                                <h4 class="white-text">MON</h4>
		                                <canvas id="clear-day" width="35" height="35"></canvas>
		                                <h4 class="bolded white-text">33<i class="wi-degrees"></i></h4>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div> -->
		        <!-- END WEATHER WIDGET 2 -->

		    <!--</div> -->

		    <!--<div class="col-lg-12 col-md-12 col-sm-12">
		        <div class="the-box no-border full">
		            <button class="btn btn-block btn-primary btn-square" id="w-newsticker-next"><i class="fa fa-chevron-up"></i></button>
		            <ul class="widget-newsticker media-list" id="divRss" style="height: 145px; overflow: hidden;">
		               
		            </ul>
		            <button class="btn btn-block btn-primary btn-square" id="w-newsticker-prev"><i class="fa fa-chevron-down"></i></button>
		        </div>

		    </div>
		    <div class="col-lg-6 col-md-12 col-sm-12">
		    </div>-->
		</div><!-- /.row -->

        
        <div class="the-box no-padding">

            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#ketaatan" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;GRAFIK KETAATAN</a></li>
                <li ><a href="#proporsi" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;GRAFIK PROPORSI INDUSTRI</a></li>
                <li ><a href="#penyebaran" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;GRAFIK PENYEBARAN INDUSTRI</a></li>                      
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="ketaatan">
                    <!-- chart -->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-6">
                            <div id="chart1" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                        </div>

                        <div class="col-lg-6">
                            <div id="chart2" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>

                    <br>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-12">
                            <div id="chart3" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

                            <br>
                            <div id="chart8" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="proporsi">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-6">
                            <div id="chart4" style="width: 485px; height: 500px; margin: 0 auto"></div>
                        </div>

                        <div class="col-lg-6">
                            <div id="chart6" style="width: 485px; height: 500px; margin: 0 auto"></div>
                        </div>
                    </div>

                    <br>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-12">
                            <div id="chart7" style="width: 970px; height: 500px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="penyebaran">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-lg-12">
                            <div id="chart5" style="width: 970px; height: 400px; margin: 0 auto"></div>
                        </div>
                        <div class="col-lg-12">
                            <div id="chart9" style="width: 970px; height: 400px; margin: 0 auto"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /.container -->

<script type="text/javascript">
    $(document).ready(function () {
        var url = [
            'http://rss.detik.com/index.php/detikcom',
            'http://rss.detik.com/index.php/hot',
            'http://rss.detik.com/index.php/sport',
            'http://rss.detik.com/index.php/inet',
            'http://www.kompas.com/getrss/nasional',
            'http://www.kompas.com/getrss/regional',
            'http://www.kompas.com/getrss/internasional',
            'http://www.kompas.com/getrss/kesehatan',
            'http://www.kompas.com/getrss/perempuan',
            'http://www.kompas.com/getrss/properti'
        ];

        $("#divRss").empty();
        $.each(url, function (index, value) {
            $('#divRss').FeedEk({
                FeedUrl: value,
                MaxCount: 100,
                DateFormat: 'LLL',
                DateFormatLang: 'id',
				DescCharacterLimit: 250
            });
        });

        // highcart usable
        $(function() {
            var year = [];
            var total = [];
            
            $.getJSON('backend/dashboard/get_year_langgar_bap', function(data) {

                year = data.year;
                total = data.total;

                $('#chart1').highcharts({
                    chart : {
                        type : 'column'
                    },
                    title : {
                        text : 'Perusahaan Tidak Taat BAP'
                    },
                    xAxis : {
                        title : {
                            text : 'Tahun'
                        },
                        categories : year
                    },
                    yAxis : {
                        allowDecimals: false,
                        title : {
                            text : 'Jumlah'
                        },
                        labels : {
                            formatter : function() {
                                return this.value + ''
                            }
                        }
                    },
                    tooltip : {
                        crosshairs : true,
                        shared : true,
                        valueSuffix : ''
                    },
                    plotOptions : {
                        spline : {
                            marker : {
                                radius : 4,
                                lineColor : '#666666',
                                lineWidth : 1
                            }
                        }
                    },
                    series : [{
                        name : 'Jumlah Perusahaan',
                        data : total
                    }]
                });
            });
        });

        $(function() {
            var year = [];
            var total = [];
            
            $.getJSON('backend/dashboard/get_year_langgar_lhu', function(data) {

                year = data.year;
                total = data.total;

                $('#chart2').highcharts({
                    chart : {
                        type : 'column'
                    },
                    title : {
                        text : 'Perusahaan Tidak Taat LHU'
                    },
                    xAxis : {
                        title : {
                            text : 'Tahun'
                        },
                        categories : year
                    },
                    yAxis : {
                        allowDecimals: false,
                        title : {
                            text : 'Jumlah'
                        },
                        labels : {
                            formatter : function() {
                                return this.value + ''
                            }
                        }
                    },
                    tooltip : {
                        crosshairs : true,
                        shared : true,
                        valueSuffix : ''
                    },
                    plotOptions : {
                        spline : {
                            marker : {
                                radius : 4,
                                lineColor : '#666666',
                                lineWidth : 1
                            }
                        }
                    },
                    series : [{
                        name : 'Jumlah Perusahaan',
                        data : total
                    }]
                });
            });
        });

        $(function() {
            var parameter_lhu = [];
            var total = [];
            
            $.getJSON('backend/dashboard/get_item_langgar_lhu', function(data) {
                
                parameter_lhu = data.parameter_lhu;
                total = data.total;

                $('#chart3').highcharts({
                    chart : {
                        type : 'column'
                    },
                    title : {
                        text : 'Pelanggaran LHU Berdasarkan Parameter Baku Mutu'
                    },
                    subtitle : {
                        text : ''
                    },
                    xAxis : {
                        title : {
                            text : 'Nama Parameter'
                        },
                        categories : parameter_lhu
                    },
                    yAxis : {
                        allowDecimals: false,
                        title : {
                            text : 'Jumlah'
                        },
                        labels : {
                            formatter : function() {
                                return this.value + ''
                            }
                        }
                    },
                    tooltip : {
                        crosshairs : true,
                        shared : true,
                        valueSuffix : ''
                    },
                    plotOptions : {
                        spline : {
                            marker : {
                                radius : 4,
                                lineColor : '#666666',
                                lineWidth : 1
                            }
                        }
                    },
                    series : [{
                        name : 'Parameter Baku Mutu',
                        data : total
                    }]
                });
            });
        });
        
        // huda : add -> chart with dummy data (9 February 2015, 16:41) //------------ start ------------//
		$(function () {
		    $('#chart4').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: 'Proporsi Industri Berdasarkan Jenis'
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                    }
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Proporsi Industri',
		            data: [
		                ['Tekstil & Produk Tekstil',   77.0],
		                ['Lain - lain',       10.0],
		                ['Makanan Minuman',    5.0],
		                ['Kimia Tekstil',     3.0],
		                ['Elektroplating',   2.0],
		                ['Sepatu',   1.0],
		                ['Pengecatan Tabung',   1.0]
		            ]
		        }]
		    });
		});
		
		$(function () {
			$('#chart5').highcharts({
	            chart : {
	                type : 'column'
	            },
	            title : {
	                text : 'Penyebaran Industri Berdasarkan Kecamatan'
	            },
	            xAxis : {
		            type: 'category',
		            labels: {
                		rotation: -45
                	}
	            },
	            yAxis : {
	                allowDecimals: false,
	                title : {
	                    text : 'Jumlah Industri'
	                }
	            },
	            tooltip : {
	                crosshairs : true,
	                shared : true,
	                pointFormat : 'Jumlah Industri:  <b>{point.y}</b>'
	            },
	            plotOptions : {
	                spline : {
	                    marker : {
	                        radius : 4,
	                        lineColor : '#666666',
	                        lineWidth : 1
	                    }
	                }
	            },
	            series : [{
	                name : 'Kecamatan',
	                data: [
						['Arjasari',	10],
						['Baleendah',	20],
						['Banjaran',	30],
						['Cicalengka',	25],
						['Ciparay',	50],
						['Dayeuhkolot',	20],
						['Ibun',	22],
						['Katapang',	15],
						['Kertasari',	70],
						['Kutawaringin',	45],
						['Majalaya',	10],
						['Pacet',	40],
						['Pameungpeuk',	30],
						['Pangalengan',	50],
						['Paseh',	20],
						['Rancaekek',	30],
						['Soreang',	25]
		            ]
	            }]
	        });
		});
		
		$(function () {
		    $('#chart6').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: 'Proporsi Industri Basah Berdasarkan Jenis'
		        },
		        tooltip: {
		            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                    style: {
		                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                    }
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Proporsi Industri Basah',
		            data: [
		                ['Tekstil & Produk Tekstil',   135],
		                ['Lain - lain',       9],
		                ['Makanan Minuman',    6],
		                ['Kimia Tekstil',     5],
		                ['Elektroplating',   5],
		                ['Farmasi',   2],
		                ['Kertas',   2],
		            ]
		        }]
		    });
		});

		
		$(function () {
			$('#chart7').highcharts({
	            chart : {
	                type : 'column'
	            },
	            title : {
	                text : 'Proporsi Jenis Industri yang Mengeluarkan Air Limbah Per Kecamatan'
	            },
	            xAxis : {
		            type: 'category',
		            labels: {
                		rotation: -45
                	}
	            },
	            yAxis : {
	                allowDecimals: false,
	                title : {
	                    text : 'Jumlah Industri'
	                }
	            },
	            tooltip : {
	                crosshairs : true,
	                shared : true,
	                pointFormat : 'Jumlah Industri:  <b>{point.y}</b>'
	            },
	            plotOptions : {
	                spline : {
	                    marker : {
	                        radius : 4,
	                        lineColor : '#666666',
	                        lineWidth : 1
	                    }
	                }
	            },
	            series : [{
	                name : 'Kecamatan',
	                data: [
						['Arjasari',	10],
						['Baleendah',	15],
						['Banjaran',	20],
						['Cicalengka',	30],
						['Ciparay',	25],
						['Dayeuhkolot',	20],
						['Ibun',	20],
						['Katapang',	40],
						['Kertasari',	50],
						['Kutawaringin',	60],
						['Majalaya',	40],
						['Pacet',	20],
						['Pameungpeuk',	30],
						['Pangalengan',	25],
						['Paseh',	20],
						['Rancaekek',	10],
						['Soreang',	25]
		            ]
	            }]
	        });
		});

		$(function () {
		    $('#chart8').highcharts({
		        title: {
		            text: 'Persentase Penaatan Usaha/Kegiatan dalam Pengendalian Pencemaran Lingkungan',
		            x: -20 //center
		        },
		        xAxis: {
		            title: {
		                text: 'Tahun'
		            },
		            categories: ['2010', '2011', '2012', '2013', '2014']
		        },
		        yAxis: {
		            title: {
		                text: 'Persentase Penaatan'
		            },
		            plotLines: [{
		                value: 0,
		                width: 1,
		                color: '#808080'
		            }]
		        },
		        tooltip: {
		            valueSuffix: '%'
		        },
		        legend: {
		            layout: 'vertical',
		            align: 'right',
		            verticalAlign: 'middle',
		            borderWidth: 0
		        },
		        series: [{
		            name: 'PPA',
		            data: [38, 42, 44, 50, 48]
		        }, {
		            name: 'PPU',
		            data: [42, 46, 48, 54, 52]
		        }, {
		            name: 'RAT',
		            data: [46, 48, 52, 58, 56]
		        }, {
		            name: 'PLB3',
		            data: [56, 50, 54, 60, 58]
		        }]
		    });
		});
		
        $(function () {
            $('#chart9').highcharts({
                chart : {
                    type : 'column'
                },
                title : {
                    text : 'Penyebaran Industri Berdasarkan Usaha Kegiatan'
                },
                xAxis : {
                    type: 'category',
                    labels: {
                        rotation: -45
                    }
                },
                yAxis : {
                    allowDecimals: false,
                    title : {
                        text : 'Jumlah Industri'
                    }
                },
                tooltip : {
                    crosshairs : true,
                    shared : true,
                    pointFormat : 'Jumlah Industri:  <b>{point.y}</b>'
                },
                plotOptions : {
                    spline : {
                        marker : {
                            radius : 4,
                            lineColor : '#666666',
                            lineWidth : 1
                        }
                    }
                },
                series : [{
                    name : 'Usaha Kegiatan',
                    data: [
                        ['Agro',    25],
                        ['Golf',   8],
                        ['Hotel',    30],
                        ['Industri',  60],
                        ['Laboratorium', 12],
                        ['Pengelola Limbah B3', 10],
                        ['Pertambangan',    12],
                        ['RPH',    8],
                        ['Rumah Makan',   45],
                        ['Rumah Sakit',    20],
                        ['SPPBE',    12],
                        ['Farmasi',   8],
                        ['Geothermal', 3],
                        ['PLTU', 2],
                    ]
                }]
            });
        });
        
		//------------ end ------------//
    });
</script>
<script src="<?php echo base_url()?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/js/exporting.js"></script>