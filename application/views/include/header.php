<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Sentir, Responsive admin and dashboard UI kits template">
		<meta name="keywords" content="admin,bootstrap,template,responsive admin,dashboard template,web apps template">
		<meta name="author" content="Ari Rusmanto, Isoh Design Studio, Warung Themes">
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/img/favicon.ico'); ?>" type="image/x-icon">
		<title>SIL :: BPLH Kab. Bandung</title>
 

		<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
		<link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
		
		<!-- PLUGINS CSS -->
		<link href="<?php echo base_url('assets/plugins/weather-icon/css/weather-icons.min.css') ?>" rel="stylesheet">		
		<link href="<?php echo base_url('assets/plugins/prettify/prettify.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/magnific-popup/magnific-popup.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/owl-carousel/owl.carousel.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/owl-carousel/owl.theme.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/owl-carousel/owl.transitions.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/chosen/chosen.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/icheck/skins/all.css') ?>" rel="stylesheet">			
		<link href="<?php echo base_url('assets/css/flexslider.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/jquery.autocomplete.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/markdown/bootstrap-markdown.min.css') ?>" rel="stylesheet">		
		<link href="<?php echo base_url('assets/plugins/salvattore/salvattore.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/fullcalendar/fullcalendar/fullcalendar.print.css') ?>" rel="stylesheet" media="print">
		<link href="<?php echo base_url('assets/plugins/nailthumb/jquery.nailthumb.1.1.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/FeedEk/FeedEk.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/datepicker/datepicker.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/plugins/multiselect/bootstrap-multiselect.css') ?>" rel="stylesheet">
				
		<!-- MAIN CSS (REQUIRED ALL PAGE)-->
		<link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/font-myriad-pro.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/style-responsive.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/bootstrap.vertical-tabs.min.css') ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
 
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery.livequery.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery.timeago.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery.autocomplete.js') ?>"></script>

		<?php echo $this->load->view('include/sync_detail'); ?>
		
	</head>
	<body class="tooltips">