<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Manajemen User&nbsp;&nbsp;<small>mengelola seluruh data user</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen User</li>
                <li class="active">Users</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">
				
				<div class="row">
                    <div class="col-lg-6">
                        <?php if ($this->tank_auth->is_admin() or $this->tank_auth->is_super_admin()) { ?>
							<button type="button" id="add_account" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
							<br/><br/>
						<?php } ?>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Lengkap</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Sebagai</th>
								<th>Status</th>
                                <th>Terakhir Login</th>
                                <th class="text-right"></th>									
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            foreach ($users as $u) {
                                if ($u->group_id == 1) {
                                    continue;
                                }
                                ?>
                                <tr class="gradeA">
                                    <td class="text-center"><?php echo ++$no; ?></td>
                                    <td><?php echo $u->name; ?></td>
                                    <td><?php echo $u->username; ?></td>                                    
                                    <td><?php echo $u->email; ?></td>
                                    <td><?php echo $this->functions->get_role($u->group_id); ?></td>
									<td><?php if ($u->activated == 1) { echo "Aktif"; } else {echo "Tidak Aktif";} ?></td>
                                    <td><?php echo $this->functions->time_elapsed_string($u->last_login); ?></td>
									
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">                                               
                                                <li><a href="#" class="edit-row" data-id="<?php echo $u->id; ?>">Ubah Data</a></li>
                                                <li><a href="#" class="delete-row" data-id="<?php echo $u->id; ?>">Hapus Data</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#" class="reset-row" data-id="<?php echo $u->id; ?>">Reset Sandi</a></li>
                                            </ul>
                                        </div>
                                    </td>									
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->

        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
    $(document).ready(function () {

        $("#form_add").validate({
            rules: {
                personname: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                username: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
				activated: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 100
                },
                confirm_password: {
                    required: true,
                    equalTo: "#password",
                    minlength: 6,
                    maxlength: 100
                },
                // email: {
                //     required: true,
                //     email: true,
                //     minlength: 4,
                //     maxlength: 100
                // },
                user_level: {
                    required: true
                }
            },
            messages: {
                personname: {
                    required: "Masukkan nama lengkap",
                    minlength: "Nama lengkap harus lebih dari 1 karakter",
                    maxlength: "Nama lengkap tidak boleh lebih dari 100 karakter"
                },
                username: {
                    required: "Masukan username yang valid",
                    minlength: "Username harus lebih dari 3 karaketer",
                    maxlength: "Username tidak boleh lebih dari 100 karakter"
                },
				activated: {
                    required: "Pilih status user"
                },
                password: {
                    required: "Masukkan sandi yang valid",
                    minlength: "Sandi harus lebih dari 6 karakter",
                    maxlength: "Sandi harus kurang dari 100 karakter"
                },
                confirm_password: {
                    required: "Masukkan sandi yang valid",
                    minlength: "Sandi harus lebih dari 6 karakter",
                    maxlength: "Sandi harus kurang dari 100 karakter"
                },
                // email: {
                //     required: "Masukkan email yang valid",
                //     email: "Masukkan email yang valid",
                //     minlength: "Email harus lebih dari 4 karakter",
                //     maxlength: "Email tidak boleh lebih dari 100 karakter"
                // },
                user_level: {
                    required: "Silahkan pilih data"
                },
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#form_reset").validate({
            rules: {
                reset_password: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                }                
            },
            messages: {
                reset_password: {
                    required: "Sandi tidak boleh kosong.",
                    minlength: "Sandi harus lebih dari 1 karakter",
                    maxlength: "Sandi harus kurang dari 100 karakter"
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });     
        
        $('#add_account').click(function(event) {
            $('#mdl_account form')[0].reset();
            $('#mdl_account form input[type=hidden]').val('');

            $("label.error").hide();
            $(".error").removeClass("error"); 

            $('.password').show();
            $('input[name=password]').prop('disabled', false);
            $('.confirm_password').show();
            $('input[name=confirm_password]').prop('disabled', false);


            $('#mdl_account').modal('show');
        });

        $('.edit-row').click(function (e) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo base_url('/backend/user/get'); ?>',
                data: 'id=' + $(this).data('id'),
                success: function (response) {
                    
                    $('#mdl_account form')[0].reset();
                    $('#mdl_account form input[type=hidden]').val('');

                    $("label.error").hide();
                    $(".error").removeClass("error");

                    $('input[id=edit_id]').val(response.id);
                    $('input[name=personname]').val(response.name);
					$('select[name=activated]').val(response.activated);
                    $('input[name=username]').val(response.username);
                    $('input[name=email]').val(response.email);
                    $('select[name=user_level]').val(response.group_id);

                    $('.password').hide();
                    $('input[name=password]').prop('disabled', true);
                    $('.confirm_password').hide();
                    $('input[name=confirm_password]').prop('disabled', true);

                    $('#mdl_account form').attr('action', '<?php echo base_url('/backend/user/edit') ?>'); //this fails silently
                    $('#mdl_account form').get(0).setAttribute('action', '<?php echo base_url('/backend/user/edit') ?>'); //this works

                    $('#mdl_account').modal('show');
                }
            })
        });
        $('.reset-row').click(function (e) {

            $('input[id=reset_edit_id]').val($(this).data('id'));
            $('#myModalReset').modal('show');
        });

        $('.delete-row').click(function (e) {
            $('input[id=deleted_id]').val($(this).data('id'));
            $('#delete').modal('show');
        });

        $.fn.read_url = function (input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);                    
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#images").change(function(){ $(this).read_url(this); });

    });
</script>


<!-- Modal -->
<div class="modal fade" id="mdl_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Pendaftaran User</h4>
            </div>
            <form role="form" action="<?php echo base_url('auth/register') ?>" method="post" id="form_add">
                <div class="modal-body">
                    <div class="form-group personname">
                        <label>Nama Lengkap</label>
                        <input type="text" name="personname" id="personname" class="form-control input-sm has-feedback" autofocus placeholder="Nama Lengkap" />                                             
                    </div>
                    <div class="form-group username">
                        <label>Username</label>
                        <input type="username" name="username" id="username" class="form-control input-sm has-feedback" placeholder="Username" />                                          
                    </div>
                    <div class="form-group email">
                        <label>Email</label>
                        <input type="email" name="email" id="email" class="form-control input-sm has-feedback" placeholder="Email" />                                
                    </div>
                    <div class="form-group password">
                        <label>Sandi</label>
                        <input type="password" name="password" id="password" class="form-control input-sm has-feedback" placeholder="Sandi" />                                
                    </div>
                    <div class="form-group confirm_password">
                        <label>Konfirmasi Sandi</label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control input-sm has-feedback" placeholder="Ulang Sandi" />                                
                    </div>
                    <div class="form-group user_level">
                        <label>Hak Akses</label>
                        <select name="user_level" id="user_level" class="form-control input-sm">
                            <option value="" disabled selected>-- Pilih Hak Akses --</option>
                            <?php
                            $roles = $this->functions->get_role();
                            foreach ($roles as $k => $r) {
                                if ($k == 1) continue;
                                echo '<option value="' . $k . '">' . $r . '</option>';
                            }
                            ?>                                  
                        </select>                                
                    </div>
					<div class="form-group activated">
                        <label>Status</label>
                        <select name="activated" id="activated" class="form-control input-sm">
                            <option value="" disabled selected>-- Pilih Status --</option>
                            <?php
                            $status = $this->functions->get_status();
                            foreach ($status as $k => $r) {
                                echo '<option value="' . $k . '">' . $r . '</option>';
                            }
                            ?>                                  
                        </select>                                
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalReset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Reset Sandi User</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/user/force_reset') ?>" id="form_reset" method="post">
                <div class="modal-body">
                    <div class="form-group reset_password">
                        <label>Password</label>
                        <input type="password" name="reset_password" id="reset_password" class="form-control input-sm has-feedback" placeholder="Password" />                        
                    </div>																  						
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="reset_edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Account</h4>
            </div>
            <form action="<?php echo base_url('/backend/user/delete'); ?>" id="" method="post">
                <div class="modal-body">
                    <p>Apakah Anda yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 

