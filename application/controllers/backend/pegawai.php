<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pegawai extends CI_Controller {

	private $error = array();
	private $path = '../assets/img/avatar';

    function __construct() {
        parent::__construct();

        $this->load->model('m_employee', '', TRUE);
      
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_employee');
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'nama_pegawai' => array('SEARCH', 'OR', $query),
				'nip' => array('SEARCH', 'OR', $query),				
				'status' => array('', 'AND', 1)
			);

     	$total = $this->m_employee->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_employee->get_list($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_employee->get_employee_by_id($this->input->post('id_pegawai')));
		exit();
	}

	public function register() {

		date_default_timezone_set("Asia/Jakarta");
				
		$param = array(
			'nama_pegawai' => $this->input->post('personname'),
			// 'user_id' => $res['user_id'],
			'nip' => $this->input->post('nip'),
			'instansi' => 'bplh kabupaten bandung',
			'pangkat_gol' => $this->input->post('classs'),
			'jabatan' => $this->input->post('position'),
			'tlp' => $this->input->post('phone'),				
			'email' => $this->input->post('email'),
			'alamat' => $this->input->post('address'),					
			'status'=> 1,					
			'dibuat_oleh' => $this->tank_auth->get_personname()					
		);
	

		# if success
		if($this->m_employee->insert_employee($param))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));

		redirect('backend/pegawai');		
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		
		// Grab data pegawai
		$param = array(
			'nama_pegawai' => $this->input->post('personname'),					
			'nip' => $this->input->post('nip'),				
			'pangkat_gol' => $this->input->post('classs'),
			'jabatan' => $this->input->post('position'),
			'tlp' => $this->input->post('phone'),
			'email' => $this->input->post('email'),			
			'alamat' => $this->input->post('address'),									
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => date("y-m-d h:i:s")
		);

		$employee = $this->m_employee->change_employee($param, $this->input->post('id'));

		if($employee) {
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		}else{
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		}				
	
		redirect('backend/pegawai');
	}	
	
	public function delete() {
		$this->m_employee->delete_employee($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/pegawai');
	}

}
