<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Document   : m_rekap_izin.php 
// Created on : January 05, 2015 17:57 
// Author     : huda.jtk09@gmail.com 
// Description: Model for Rekap Izin and other relatives 

class m_rekap_izin extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * huda : get all izin pembuangan limbah who have tgl izin + 5 years < today - 3 month
	 *
	 * @param	Date 
	 * @return	Array of Object
	 */
	function get_izin_pembuangan($date_param, $limit=0, $offset=0) {
		$data = array();

		$query = $this->db->query("SELECT b.nama_industri, c.ket as usaha_kegiatan, d.ket as jenis_industri, b.alamat, e.ket as kecamatan, f.ket as kelurahan, a.tgl_izin
									FROM izin_pembuangan_limbah a 
									LEFT JOIN industri b ON a.id_industri = b.id_industri
									LEFT JOIN usaha_kegiatan c ON b.id_usaha_kegiatan = c.id_usaha_kegiatan
									LEFT JOIN jenis_industri d ON b.id_jenis_industri = d.id_jenis_industri
									LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kecamatan
									LEFT JOIN kelurahan f ON b.id_kelurahan = f.id_kelurahan
									WHERE a.tgl_izin <= '".$date_param."'
									ORDER BY a.tgl_izin ASC
									LIMIT ".$offset.", ".$limit);
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * huda : get all izin pembuangan limbah who have (tgl izin + 5 years < today - 3 month) size
	 *
	 * @param	Date 
	 * @return	int
	 */
	function get_izin_pembuangan_size($date_param) {
		
		$query = $this->db->query("SELECT a.id_pembuangan
									FROM izin_pembuangan_limbah a
									WHERE a.tgl_izin <= '".$date_param."'");
		
		return ($query->num_rows() > 0) ? $query->num_rows() : 0;
	}

	/**
	 * huda : get all izin tps limbah who have tgl izin + 5 years < today - 3 month
	 *
	 * @param	Date 
	 * @return	Array of Object
	 */
	function get_izin_tps($date_param, $limit=0, $offset=0) {
		$data = array();

		$query = $this->db->query("SELECT b.nama_industri, c.ket as usaha_kegiatan, d.ket as jenis_industri, b.alamat, e.ket as kecamatan, f.ket as kelurahan, a.tgl_izin
									FROM izin_tps_limbah a 
									LEFT JOIN industri b ON a.id_industri = b.id_industri
									LEFT JOIN usaha_kegiatan c ON b.id_usaha_kegiatan = c.id_usaha_kegiatan
									LEFT JOIN jenis_industri d ON b.id_jenis_industri = d.id_jenis_industri
									LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kecamatan
									LEFT JOIN kelurahan f ON b.id_kelurahan = f.id_kelurahan
									WHERE a.tgl_izin <= '".$date_param."'
									ORDER BY a.tgl_izin ASC
									LIMIT ".$offset.", ".$limit);
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * huda : get all izin tps limbah who have (tgl izin + 5 years < today - 3 month) size
	 *
	 * @param	Date 
	 * @return	int
	 */
	function get_izin_tps_size($date_param) {
		
		$query = $this->db->query("SELECT a.id_izin_tps
									FROM izin_tps_limbah a
									WHERE a.tgl_izin <= '".$date_param."'");
		
		return ($query->num_rows() > 0) ? $query->num_rows() : 0;
	}
}
?>