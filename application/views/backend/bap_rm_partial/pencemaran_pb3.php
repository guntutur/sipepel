<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 20/12/16
 * Time: 21:47
 */
?>

<div class="tab-pane" id="pencemaran_b3">
    <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
    <!-- <div class="row"> -->
    <?php if (isset($pencemaran_pb3)) {
        $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
        $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
        $new = array();
        for ($i = 0; $i < count($arr_padat_jenis); $i++) {
            $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
        }
    }
    ?>

    <table class="table table-th-block">
        <thead>
        <tr>
            <th class="text-center">a. Jenis</th>
            <th class="text-center">b. Jumlah (ton/hari)</th>
            <th class="text-center"></th>
        </tr>
        </thead>
        <tbody>
        <?php if (isset($pencemaran_pb3)) { foreach ($new as $key => $value) { ?>
            <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                <td class="col-lg-2">
                    <div class=" text-right">
                        <div class="btn-group" style="padding-right: 17px;">
                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } } else { ?>
            <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="" /></td></td>
                <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="" /></td>
                <td class="col-lg-2">
                    <div class=" text-right">
                        <div class="btn-group" style="padding-right: 17px;">
                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div class="form-group ">
        <label>c. Sarana Pengelolaan</label>
        <div class="form-group">
            <div class="form-group col-sm-6">
                <label>1. Tong/Bak Sampah</label>
                <input type="text" name="padat_sarana_tong" id="" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->padat_sarana_tong : ""; ?>" placeholder="unit" />
            </div>
            <div class="form-group col-sm-6">
                <label>2. TPS</label>
                <input type="text" name="padat_sarana_tps" id="" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->padat_sarana_tong : ""; ?>"placeholder="unit" />
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label>d. Pengelolaan</label>
        <select class="form-control input-sm" id="pjpl" name="padat_kelola">
            <?php if (isset($pencemaran_pb3)) {
            $padat_kelola = $pencemaran_pb3->padat_kelola;
            echo $padat_kelola;
            if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                foreach ($pengelolaan as $p) {
                    echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                }
                echo "</select>";
                echo "<input type='text' name='padat_kelola_lain' id='pjpl2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
            } else {
                $temp = "Lainnya";
                foreach ($pengelolaan as $p) {
                    echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                }
                echo "</select>";
                echo "<input type='text' name='padat_kelola_lain' id='pjpl2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
            } } else { ?>
            <?php foreach ($pengelolaan as $pk) {
                echo "<option value='$pk->ket'>$pk->ket</option>";
            } ?>
        </select>
        <input type="text" name="padat_kelola_lain" id="pjpl2" class="form-control input-sm has-feedback" style="display:none"/>
        <?php } ?>
    </div>
    <fieldset>
        <legend>e. Lain-lain</legend>
        <div class="form-group ">
            <textarea class="form-control input-sm no-resize bold-border" name="lain_lain_padat" ><?php echo isset($pencemaran_pb3) ? $pencemaran_pb3->lain_lain : "";?></textarea>
        </div>
    </fieldset>
    <div class="row">
        <div class="col-sm-6 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Udara</a>
        </div>
        <div class="col-sm-6 text-right">
            <input id="save-pencemaran_b3" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran LB3"/>
        </div>
    </div>
</div>
