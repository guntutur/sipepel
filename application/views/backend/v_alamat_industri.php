<?php 
    if(isset($list)){
        foreach ($list as $row) {
    ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>
                        <label>Alamat           : </label> <?php echo $row->alamat;?> <br>
                        <label>Desa / Kelurahan : </label> <?php echo $row->kelurahan;?> <br>
                        <label>Kecamatan        : </label> <?php echo $row->kecamatan;?> <br>
                        <label>Telp             : </label> <?php echo $row->tlp;?> <br>
                        <label>Fax              : </label> <?php echo $row->fax;?>  <br>
                        <label>Izin TPS         : </label> <?php echo $row->izin_tps;?>  <br>
                        <label>Tanggal Izin     : </label> <?php echo $row->tgl_perizinan;?>  <br>
                        <label>Jumlah TPS       : </label> <?php echo $row->jml_tps;?>  <br>
                    </div>
                </div>
            </div>
<?php }
} else {?>
    <div class="panel panel-default">
        <div class="panel-body bg-danger">
            <div ><label>Data Usaha / Kegiatan tidak ditemukan</label></div>
       </div>
   </div>
<?php }?>