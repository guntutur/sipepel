<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 20/12/16
 * Time: 21:46
 */
?>

<div class="tab-pane" id="pencemaran_udara">
    <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>

    <div class="form-group ">
        <label>a. Data Kualitas Udara Ambien</label>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th></th>
                <?php if (isset($uji_ambien)) { foreach($uji_ambien as $site) {
                    echo "<th class='text-center'>$site->lokasi</th>";
                } } else { ?>
                <th class="text-center">Upwind</th>
                <th class="text-center">Site</th>
                <th class="text-center">Downwind</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($uji_ambien)) { ?>
            <tr>
                <td><label>Pengujian Kualitas</label></td>
                <?php
                $c = 1;
                foreach ($uji_ambien as $dt) {
                    $id_uji_ambien[$c] = $dt->id_uji_ambien;
                    $tuk[$c] = adaConv($dt->uji_kualitas);
                    $per[$c] = $dt->period;
                    $lab_uk[$c] = $dt->lab;
                    $bm[$c] = $dt->bm_pemenuhan;
                    $bm_param[$c] = $dt->bm_param;
                    $bm_period[$c] = $dt->bm_period;
                    $c++;
                }

                for($p=1; $p<=3; $p++) {
                    echo "<input type='hidden' name='id_uji_ambien$p' value='".$id_uji_ambien[$p]."' />";
                    echo "<td><select class='form-control input-sm' name='uji_kualitas$p'>";
                    $n=1;
                    foreach ($bool as $f) {
                        echo "<option value='".adaConv($f)."' " ;
                        if (strcasecmp($tuk[$p], adaConv($f)) == 0){
                            echo "selected='true'";
                        }
                        echo ">".adaConv($f)."</option>";
                        $n++;
                    }
                    echo "</select></td>";
                }
                ?>
            </tr>
            <tr>
                <td><label>Periode Pengujian (per 6 bulan)</label></td>
                <?php
                for($p=1; $p<=3; $p++) {
                    echo "<td><select class='form-control input-sm' name='period$p'>";
                    $n=1;
                    foreach ($bool as $f) {
                        echo "<option value='".rutinConv($f)."' " ;
                        if (strcasecmp($per[$p], rutinConv($f)) == 0){
                            echo "selected='true'";
                        }
                        echo ">".rutinConv($f)."</option>";
                        $n++;
                    }
                    echo "</select></td>";
                }
                ?>
            </tr>
            <tr>
                <td><label>Laboratorium Penguji</label></td>
                <?php
                for($n=1; $n<=count($lab_uk); $n++){
                    echo "<td><select name='lab$n' class='form-control input-sm '>";
                    foreach ($lab as $l) {
                        echo "<option value='$l->nama_lab' ".(($lab_uk[$n] == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                    }
                    echo "</select></td>";
                }
                ?>
            </tr>
            <tr>
                <td><label>Pemenuhan BM</label></td>
                <?php
                for($p=1; $p<=3; $p++) {
                    echo "<td><select class='form-control input-sm' name='bm_pemenuhan$p' id='bmp$p'>";
                    $n=1;
                    foreach ($bool as $f) {
                        echo "<option value='".yaConv($f)."' " ;
                        if (strcasecmp(yaConv($bm[$p]), yaConv($f)) == 0){
                            echo "selected='true'";
                        }
                        echo ">".yaConv($f)."</option>";
                        $n++;
                    }
                    echo "</select></td>";
                }
                ?>
            </tr>
            <tr>
                <td><label>Parameter Tidak Memenuhi BM</label></td>
                <?php
                for($n=1; $n<=count($bm_param); $n++){
                    echo "<td><input type='text' name='bm_param$n' class='form-control input-sm has-feedback ' value='$bm_param[$n]'/></td>";
                }
                ?>
            </tr>
            <tr>
                <td><label>Periode Tidak Memenuhi BM</label></td>
                <?php
                for($n=1; $n<=count($bm_param); $n++){
                    echo "<td><input type='text' name='bm_period$n' class='form-control input-sm has-feedback ' value='$bm_period[$n]'/></td>";
                }
                ?>
            </tr>
            <?php } else { ?>
                <tr>
                    <td><label>Pengujian Kualitas</label></td>
                    <td><select class="form-control input-sm" name="uji_kualitas1"></option><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="uji_kualitas2"></option><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                    <td><select class="form-control input-sm" name="uji_kualitas3"></option><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                </tr>
                <tr>
                    <td><label>Periode Pengujian (per 6 bulan)</label></td>
                    <td><select class="form-control input-sm" name="period1"></option><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                    <td><select class="form-control input-sm" name="period2"></option><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                    <td><select class="form-control input-sm" name="period3"></option><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                </tr>
                <tr>
                    <td><label>Laboratorium Pengujian</label></td>
                    <?php
                    for ($i=1; $i<4; $i++) {
                        echo "<td><select name='lab".$i."' class='form-control input-sm'>";
                        echo "<option value=''></option>";
                        foreach ($lab as $key) {
                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                        }
                        echo '</select></td>';
                    }
                    ?>
                </tr>
                <tr>
                    <td><label>Pemenuhan BM</label></td>
                    <td><select class="form-control input-sm bmp" id="bmp1" name="bm_pemenuhan1"></option><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                    <td><select class="form-control input-sm bmp" id="bmp2" name="bm_pemenuhan2"></option><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                    <td><select class="form-control input-sm bmp" id="bmp3" name="bm_pemenuhan3"></option><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                </tr>
                <tr>
                    <td><label>Parameter Tidak Memenuhi BM</label></td>
                    <td><input type="text" name="bm_param1" id="bm_param1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_param2" id="bm_param2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_param3" id="bm_param3" class="form-control input-sm has-feedback " /></td>
                </tr>
                <tr>
                    <td><label>Periode Tidak Memenuhi BM</label></td>
                    <td><input type="text" name="bm_period1" id="bm_period1" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_period2" id="bm_period2" class="form-control input-sm has-feedback " /></td>
                    <td><input type="text" name="bm_period3" id="bm_period3" class="form-control input-sm has-feedback " /></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?php
    $lapor = $bool;
    $lapor[] = '2';
    ?>
    <div class="form-group ">
        <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
        <select class="form-control input-sm" name="pelaporan_ua_ue">
            <?php
            for($d=0; $d<3; $d++){
                echo "<option value='".rutinConv($lapor[$d])."'";
                if (isset($pencemaran_udara)) {
                    if (strcasecmp($pencemaran_udara->pelaporan_ua_ue, rutinConv($lapor[$d])) == 0) {
                        echo "selected";
                    }
                }
                echo ">".rutinConv($lapor[$d])."</option>";
            }
            ?>
        </select>
    </div>

    <div class="form-group ">
        <label>c. Lain-lain</label>
        <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_udara) ? $pencemaran_udara->lain_lain : ""; ?>"/>
    </div>

    <div class="row">
        <div class="col-sm-4 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Air</a>
        </div>
        <div class="col-sm-4 text-center">
            <input id="save-pencemaran_udara" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Udara"/>
        </div>
        <div class="col-sm-4 text-right">
            <a class="btn btn-warning NextStep">Pencemaran PB3<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>
