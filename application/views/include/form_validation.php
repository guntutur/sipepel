<!-- 
	Description : Javascript for form validation in all BAP, note that this function only read form id = "add_form"
					so if you want to use it in another form, give the form the same id as mentioned, and don't forget the rule
	Author 		: guntutur@gmail.com
	note 		: this section has been moved from footer.php, now this should be pretty neat
-->

<script type="text/javascript">

	$(document).ready(function(){

        $("#add_form").validate({

            highlight : function(element) {
                // alert(input);
                $(element).closest('.form-group').addClass('has-error');
                var div = $(element).parent().parent().parent().parent();
                if ($(div).find("div.tab-pane.active:has(div.has-error)").length == 0) {
                     $(div).find("div.tab-pane:has(div.has-error)").each(function (index, tab) {
                        var id = $(tab).attr("id");
                    $('a[href="#' + id + '"]').tab('show');
                     });
                    
                }
                
            },

            unhighlight : function(element) {
                // alert(input);
                $(element).closest('.form-group').removeClass('has-error');
                var div = $(element).parent().parent().parent().parent();
                if ($(div).find("div.tab-pane.active:has(div.has-error)").length == 0) {
                     $(div).find("div.tab-pane:has(div.has-error)").each(function (index, tab) {
                        var id = $(tab).attr("id");
                    $('a[href="#' + id + '"]').tab('show');
                     });
                    
                }
                
            },
            
            debug: true,
            ignore : [],
            rules : {
                // BASICALLY DIPAKAI SEMUA BAP
                bap_tgl : {
                    required : true
                },
                id_pegawai : {
                    required : true
                },
                pengelolaan : {
                    required : true
                },
                jenis_limbah : {
                    required : true
                },
                // jml_karyawan : {
                //     required : true
                // },
                petugas_pengawas : {
                    required : true
                },
                nama_industri : {
                    required : true
                },
                // BASICALLY DIPAKAI SEMUA BAP
                // BAP GOLF
                jumlah_karyawan : {
                    required : true
                },
                jumlah_hole : {
                    required : true
                },
                rata_pengunjung : {
                    required : true
                },
                jml_membership : {
                    required : true
                },
                pakai_pestisida : {
                    required : true
                },
                pakai_oli : {
                    required : true
                },
                simpan_oli_bekas : {
                    required : true
                },
                dok_lingk_tahun : {
                    required : function(element) {
                                return $('select[name=dok_lingk]').val() == '1'
                            }
                },
                izin_lingk_tahun : {
                    required : function(element) {
                                return $('select[name=izin_lingk]').val() == '1'
                            }
                },
                // ambil_air_tanah : {
                //     required : true
                // },
                // ambil_air_permukaan : {
                //     required : true
                // },
                // ambil_air_pdam : {
                //     required : true
                // },
                // ambil_air_lain : {
                //     required : true
                // },
                sarana_jenis : {
                    required : function(element) {
                                return $('select[name=sarana_olah_limbah]').val() == '1'
                            }
                },
                sarana_kapasitas : {
                    required : function(element) {
                                return $('select[name=sarana_olah_limbah]').val() == '1'
                            }
                },
                // BAP GOLF
                // BAP HOTEL
                jml_kamar : {
                    required : true
                },
                tingkat_hunian : { // BAP LABORATORIUM JUGA PAKAI
                    required : true
                },
                // BAP HOTEL
                // BAP LABORATORIUM
                // BAP LABORATORIUM
                // BAP PERTAMBANGAN
                jenis_tambang : {
                    required : true
                },
                izin_usaha : {
                    required : true
                },
                // BAP PERTAMBANGAN
                // BAP RPH
                koord_rph_derajat_s : {
                    required : true
                },
                koord_rph_jam_s : {
                    required : true
                },
                koord_rph_menit_s : {
                    required : true
                },
                koord_rph_derajat_e : {
                    required : true
                },
                koord_rph_jam_e : {
                    required : true
                },
                koord_rph_menit_e : {
                    required : true
                },
                jumlah_ternak : {
                    required : true
                },
                penggunaan_air : {
                    required : true
                },
                // BAP RPH
                // BAP RM
                jml_meja : {
                    required : true,
                    minlength : 1,
                    maxlength : 5
                },
                kapasitas : {
                    required : '#sarolim1:checked'
                },
                sarana_jenis : {
                    required : '#sarolim1:checked'
                },
                iu_tahun : {
                    required : '#iu1:checked'
                },
                limbah_jenis : {
            		required : true
                },
                limbah_jml : {
                    required : true
                },
                // BAP RM
                // BAP RS
                jml_pasien : {
                    required : true
                },
                jml_tempat_tidur : {
                    required : true
                },
                // BAP RS
                // BAP SPPBE
                kapasitas_pengisian : {
                    required : true
                },
                air_guna : {
                    required : true
                },
                koord_sppbe_derajat_s : {
                    required : true
                },
                koord_sppbe_jam_s : {
                    required : true
                },
                koord_sppbe_menit_s : {
                    required : true
                },
                koord_sppbe_derajat_e : {
                    required : true
                },
                koord_sppbe_jam_e : {
                    required : true
                },
                koord_sppbe_menit_e : {
                    required : true
                },
                // BAP SPPBE
                // BAP AGRO
                peru_stat : {
                    required : true
                },
                peru_stat_lain : {
                    required : function(element) {
                                return $("#peru_stat").val() == 'other'
                            }
                },
                jenis_produk : {
                    required : true
                },
                pakai_pupuk : { // BAP GOLF JUGA PAKAI
                    required : true
                },
                // BAP AGRO
                // DIPAKAI BERSAMA AGRO, INDUSTRI, PLB3
                proses_produksi : {
                    required : true
                },
                kapasitas_produksi : {
                    required : true
                },
                // DIPAKAI BERSAMA AGRO, INDUSTRI, PLB#
                // BAP PLB3
		        bidang_usaha : {
                    required : true
                },
                izin_jenis : {
                    required : true
                },
                izin_b3_no : {
                    required : true
                },
                masa_berlaku : {
                    required : true
                },
                jenis_limbah_b3 : {
                    required : true
                },
                kelola_prinsip : {
                    required : true
                },
                // BAP PLB3
                p2_nama : {
                    required : true
                },
                p2_telp : {
                    required : true
                },
                p2_jabatan : {
                    required : true
                },
                limb_sumber : {
                    required : true
                },
                bdn_terima : {
                    required : true
                },
                ipal_sistem2 : {
                    required : function(element) {
                                return $("#si").val() == 'other'
                            }
                },
                ipal_unit : {
                    required : function(element) {
                                return $("#ipal").val() == 'Ada'
                            }
                },
                ipal_kapasitas : {
                    required : function(element) {
                                return $("#ipal").val() == 'Ada'
                            }
                },
                izin_no : {
                    required : function(element) {
                                return $("#izin").val() == 'Ada'
                            }
                },
                izin_tgl : {
                    required : function(element) {
                                return $("#izin").val() == 'Ada'
                            }
                },
                izin_debit : {
                    required : true
                },
                au_jenis2 : {
                    required : function(element) {
                                return $("#je").val() == 'other'
                            }
                },
                // au_ukuran : {
                //     required : function(element) {
                //                 return $("#au").val() == 'Ada'
                //             }
                // },
                cat_deb_hari : {
                    required : true
                },
                daur_ulang_debit : {
                	required : true
                },
                // uji_period2 : {
                //     required : function(element) {
                //                 return $("#pp").val() == 'other' && $("#au").val() == 'Ada'
                //             }
                // },
                uji_lab : {
                    required : function(element) {
                                return $("#pp").val() == 'other' && $("#au").val() == 'Ada'
                            }
                },
                uji_tidak_bulan : {
                    required : function(element) {
                                return $("#uji_hasil").val() == 'Tidak Memenuhi Baku Mutu'
                            }
                },
                uji_tidak_param : {
                    required : function(element) {
                                return $("#uji_hasil").val() == 'Tidak Memenuhi Baku Mutu'
                            }
                },
                bocor_lokasi : {
                    required : function(element) {
                                return $("#bocor").val() == 'Ada'
                            }
                },
                emisi_lain : {
                    required : function(element) {
                                return $("#emboil").val() == 'other'
                            }
                },
                uji_kualitas1 : {
                    required : true
                },
                uji_kualitas2 : {
                    required : true
                },
                uji_kualitas3 : {
                    required : true
                },
                period1 : {
                    required : true
                },
                period2 : {
                    required : true
                },
                period3 : {
                    required : true
                },
                bm_pemenuhan1 : {
                    required : true
                },
                bm_pemenuhan2 : {
                    required : true
                },
                bm_pemenuhan3 : {
                    required : true
                },
                bm_param1 : {
                    required : function(element) {
                                return (($(".bmp").val() == 'Tidak') || ($(".bmp").val() == '0'))
                            }
                },
                bm_param2 : {
                    required : function(element) {
                                return (($(".bmp").val() == 'Tidak') || ($(".bmp").val() == '0'))
                            }
                },
                bm_param3 : {
                    required : function(element) {
                                return (($(".bmp").val() == 'Tidak') || ($(".bmp").val() == '0'))
                            }
                },
                bm_period1 : {
                    required : function(element) {
                                return (($(".bmp").val() == 'Tidak') || ($(".bmp").val() == '0'))
                            }
                },
                bm_period2 : {
                    required : function(element) {
                                return (($(".bmp").val() == 'Tidak') || ($(".bmp").val() == '0'))
                            }
                },
                bm_period3 : {
                    required : function(element) {
                                return (($(".bmp").val() == 'Tidak') || ($(".bmp").val() == '0'))
                            }
                },
                // b3_uji_param : {
                //     required : true
                // },
                // b3_uji_lab : {
                //     required : true
                // },
                // b3_uji_waktu : {
                //     required : true
                // },
                // need confirmation to enable these 2 item below
                // padat_jenis : {
                //     required : true
                // },
                // padat_jumlah : {
                //     required : true
                // },
                padat_kelola_lain : {
                    required : function(element) {
                                return $("#pjpl").val() == 'Lainnya'
                            }
                },
                padat_sarana_tong : {
                    required : true
                },
                padat_sarana_tps : {
                    required : true
                },
                jdl_name : {
                    required : '#dl1:checked'
                },
                dlt : {
                    required : '#dl1:checked'
                },
                il_tahun : {
                    required : '#il1:checked'
                }
            },

            submitHandler: function (form) {
                if($(form).valid())
                    // meng-true kan semua input/select yg di disable agar tidak menghasilkan nilai false pada saat insert ke database
                    for(var x=2; x<4; x++){
                        $('select[name=tb_izin'+x+']').prop('disabled', false);
                        $('input[name=tb_izin_no'+x+']').prop('disabled', false);
                        $('input[name=tb_izin_tgl'+x+']').prop('disabled', false);
                        $('input[name=tb_jenis'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_lumpur'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_batubara'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_pelumas'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_aki'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_ltl'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_kmsn'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_majun'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_elektro'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_kimia'+x+']').prop('disabled', false);
                        $('input[name=tb_lama_spn_lain_ket'+x+']').prop('disabled', false);
                        $('input[name=koord_derajat_s'+x+']').prop('disabled', false);
                        $('input[name=koord_jam_s'+x+']').prop('disabled', false);
                        $('input[name=koord_menit_s'+x+']').prop('disabled', false);
                        $('input[name=koord_derajat_e'+x+']').prop('disabled', false);
                        $('input[name=koord_jam_e'+x+']').prop('disabled', false);
                        $('input[name=koord_menit_e'+x+']').prop('disabled', false);
                        $('input[name=koord_derajat_s'+x+']').prop('disabled', false);
                        $('input[name=koord_jam_s'+x+']').prop('disabled', false);
                        $('input[name=koord_menit_s'+x+']').prop('disabled', false);
                        $('input[name=koord_derajat_e'+x+']').prop('disabled', false);
                        $('input[name=koord_jam_e'+x+']').prop('disabled', false);
                        $('input[name=koord_menit_e'+x+']').prop('disabled', false);
                        $('input[name=tb_ukuran'+x+']').prop('disabled', false);
                        $('select[name=tb_ppn_nama_koord'+x+']').prop('disabled', false);
                        $('select[name=tb_simbol_label'+x+']').prop('disabled', false);
                        $('select[name=tb_saluran_cecer'+x+']').prop('disabled', false);
                        $('select[name=tb_bak_cecer'+x+']').prop('disabled', false);
                        $('select[name=tb_kemiringan'+x+']').prop('disabled', false);
                        $('select[name=tb_sop_darurat'+x+']').prop('disabled', false);
                        $('select[name=tb_log_book'+x+']').prop('disabled', false);
                        $('select[name=tb_apar_p3k'+x+']').prop('disabled', false);
                    }
                    form.submit();
                return false;
            }
        });
    });
</script>