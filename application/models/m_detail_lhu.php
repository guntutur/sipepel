<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_detail_lhu extends CI_Model {

	private $table_name = 'detail_lhu';

	public function __construct() {
		parent::__construct();

		$this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
        $this->load->model('m_lhu_air_limbah');
	}

	public function get_all($where='', $limit=0, $offset=0) {	

		$data = array();

		
		$this->db->select('d.*, (select ket from parameter_lhu where id_parameter_lhu = d.id_parameter_lhu) as nama_parameter');
		$this->db->from($this->table_name.' as d');
		
		if(isset($where) and $where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		
		$this->db->order_by("d.id_detail_lhu", "asc"); 
		
		$query = $this->db->get();		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	public function get_all_izin($where='', $jenis_lhu, $limit=0, $offset=0) {	

		$data = array();

		
		$this->db->select('d.*, (select ket from baku_mutu_izin where id_baku_mutu_izin = d.id_baku_mutu_izin and jenis_baku_mutu = "'.$jenis_lhu.'") as nama_parameter');
		$this->db->from($this->table_name.' as d');
		
		if(isset($where) and $where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		
		$this->db->order_by("d.id_detail_lhu", "asc"); 
		
		$query = $this->db->get();		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {

		$this->db->select('d.*, (select ket from parameter_lhu where id_parameter_lhu = d.id_parameter_lhu) as nama_parameter');
		$this->db->from($this->table_name.' as d');

		if(isset($where) and $where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}

	function get_num_rows_izin($where, $jenis_lhu) {

		$this->db->select('d.*, (select ket from baku_mutu_izin where id_baku_mutu_izin = d.id_baku_mutu_izin and jenis_baku_mutu = "'.$jenis_lhu.'") as nama_parameter');
		$this->db->from($this->table_name.' as d');

		if(isset($where) and $where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}

	function get_lhu_by_id($id_lhu) {

		$data = array();

		$sql = '';
		// $sql .= 'select p.*, (select nilai_hasil_uji from detail_lhu where id_parameter_lhu = p.id_parameter_lhu and id_laporan_hasil_uji = r.id_laporan_hasil_uji) as nilai_uji, (select metode_pengujian from detail_lhu where id_parameter_lhu = p.id_parameter_lhu and id_laporan_hasil_uji = r.id_laporan_hasil_uji) as metode from parameter_lhu p ';
		// $sql .= 'inner join ( ';
		// $sql .= 'select l.id_laporan_hasil_uji, l.id_industri, l.jenis_lhu, l.jenis_baku_mutu, i.id_usaha_kegiatan, i.id_jenis_industri ';
		// $sql .= 'from laporan_hasil_uji l ';
		// $sql .= 'inner join industri i on i.id_industri = l.id_industri where l.id_laporan_hasil_uji = '.$id_lhu.') as r ';
		// $sql .= 'on p.id_usaha_kegiatan = r.id_usaha_kegiatan AND p.id_jenis_industri= r.id_jenis_industri AND p.jenis_lhu like binary r.jenis_lhu ';
		// $sql .= 'where p.ket <> "" AND r.jenis_baku_mutu in ("Internal", "External", "Tidak Ada")';

		$sql .= 'select p.*, (select nilai_hasil_uji from detail_lhu where id_parameter_lhu = p.id_parameter_lhu and id_laporan_hasil_uji = r.id_laporan_hasil_uji) as nilai_uji, (select metode_pengujian from detail_lhu where id_parameter_lhu = p.id_parameter_lhu and id_laporan_hasil_uji = r.id_laporan_hasil_uji) as metode from parameter_lhu p ';
		$sql .= 'inner join ( ';
		$sql .= 'select l.id_laporan_hasil_uji, l.id_industri, l.jenis_lhu, l.jenis_baku_mutu, l.jenis_bahan_bakar, i.id_usaha_kegiatan, CONVERT(i.id_jenis_industri, char(255)) as id_jenis_industri ';
		$sql .= 'from laporan_hasil_uji l ';
		$sql .= 'inner join industri i on i.id_industri = l.id_industri where l.id_laporan_hasil_uji = '.$id_lhu.') as r ';
		$sql .= 'on p.id_usaha_kegiatan = r.id_usaha_kegiatan AND (p.id_jenis_industri = r.id_jenis_industri OR p.id_jenis_industri = r.jenis_bahan_bakar) AND p.jenis_lhu like binary r.jenis_lhu ';
		$sql .= 'where p.ket <> "" AND r.jenis_baku_mutu in ("Internal", "External", "Tidak Ada") ';

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_parameter_izin($where, $id_lhu) {		

		$data = array();
		$sql = '';
		$sql .= 'select b.*, (select nilai_hasil_uji from detail_lhu where id_baku_mutu_izin = b.id_baku_mutu_izin and id_laporan_hasil_uji = '.$id_lhu.') as nilai_uji, (select metode_pengujian from detail_lhu where id_baku_mutu_izin = b.id_baku_mutu_izin  and id_laporan_hasil_uji = '.$id_lhu.') as metode ';
		$sql .= 'from baku_mutu_izin b ';
		$sql .= 'where '.$where;

		// $this->db->where($where);
		// $query = $this->db->get('baku_mutu_izin');

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_data_by_id($where) {

		$this->db->select('d.*, (select satuan from parameter_lhu where id_parameter_lhu = d.id_parameter_lhu) as satuan, (select ket from parameter_lhu where id_parameter_lhu = d.id_parameter_lhu) as nama_parameter');

		if(isset($where) and $where != '') {
			$this->db->where($where);
		}	
		
		$query = $this->db->get($this->table_name.' as d');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_data_by_id_izin($where) {

		$this->db->select('d.*, (select satuan from baku_mutu_izin where id_baku_mutu_izin = d.id_baku_mutu_izin) as satuan, (select ket from baku_mutu_izin where id_baku_mutu_izin = d.id_baku_mutu_izin) as nama_parameter');

		if(isset($where) and $where != '') {
			$this->db->where($where);
		}	
		
		$query = $this->db->get($this->table_name.' as d');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_detail_by_id($id) {
		$this->db->select('l.*, (select nama_industri from industri where id_industri = l.id_industri) as nama_industri');
		$this->db->where('l.id_laporan_hasil_uji', $id);
		
		$query = $this->db->get('laporan_hasil_uji as l');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function insert_detail($data) {

		$where = 'id_laporan_hasil_uji = '.$data['id_laporan_hasil_uji'].' AND id_parameter_lhu = '.$data['id_parameter_lhu'];
		$d = $this->get_data_by_id($where);

		if(count($d) > 0) {
			$this->db->update($this->table_name, $data, $where);
		}else{
			$this->db->insert($this->table_name, $data);		
		}

		return $this->db->affected_rows() > 0;
	}

	function insert_detail_izin($data) {

		$where = 'id_laporan_hasil_uji = '.$data['id_laporan_hasil_uji'].' AND id_baku_mutu_izin = '.$data['id_baku_mutu_izin'];
		$d = $this->get_data_by_id_izin($where);

		if(count($d) > 0) {
			$this->db->update($this->table_name, $data, $where);
		}else{
			$this->db->insert($this->table_name, $data);		
		}

		return $this->db->affected_rows() > 0;
	}

	function insert_pelanggaran($data) {
		$this->db->insert('history_item_teguran_lhu', $data);		
		return $this->db->affected_rows() > 0;
	}

	function clear_history_pelanggaran($id_lhu) {		
		$this->db->where('id_laporan_hasil_uji', $id_lhu);
		$this->db->delete('history_item_teguran_lhu');
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
}
?>