<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document   : laporan_lhu.php 
 // Created on : November 19, 2014 16:32 
 // Author     : huda.jtk09@gmail.com 
 // Description: Controller for Laporan LHU 

class laporan_lhu extends CI_Controller {

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_industry', 'inDao', TRUE);
        $this->load->model('m_rekap_lhu', 'rekDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view
	public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_laporan_lhu');
        $this->load->view('include/footer');
    }

    public function get_list() {
        $list_industri = $this->inDao->get_all_industry_detail();
        
        $list_teguran = $this->rekDao->get_all_teguran_lhu();

        foreach ($list_teguran as $t) {
            
        }

        $data['industri'] = $list_industri;

        // print_r($data);
        echo json_encode($data);
        exit();
        
    }

    public function tes() {
        $age['Peter'] = "35";
        $age['Ben'] = "37";
        $age['Joe'] = "43";

        echo "peter: ".$age['Peter'];
        if (array_key_exists('Joe',$age)) {
            echo "tes: ".$age['Joe'];
        }
        echo "<br> end";

    }

}
