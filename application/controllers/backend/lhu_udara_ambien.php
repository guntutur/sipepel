<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class lhu_udara_ambien extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model
        $this->load->model('m_lhu_udara_ambien');
        $this->load->model('m_industry');
        $this->load->model('m_laboratory');
        $this->load->model('m_detail_lhu');
        		
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $data['industri'] = $this->m_industry->get_dropdown();
        $data['labs'] = $this->m_laboratory->get_dropdown();
        
    	$this->load->view('include/header');
        $this->load->view('backend/v_lhu_udara_ambien', $data);
        $this->load->view('include/footer');
    }

    public function register() {

        date_default_timezone_set("Asia/Jakarta");
        $tgl_pembuatan = date("Y-m-d H:i:s");

        $o = $this->m_lhu_udara_ambien->is_duplicate($this->input->post('industri'), $this->input->post('lap_bln'));    
        if($o) {
            // $koordinat_sample = serialize($this->input->post('koordinat_sample'));
            $koordinat_sample = $this->functions->parsing_koordinat($this->input->post());

            $param = array(                
                    'id_industri' => $this->input->post('industri'),
                    'id_lab' => $this->input->post('labs'),                         
                    'no_lhu' => $this->input->post('no_lhu'),
                    'jenis_lhu' => 'LHU Udara Ambien',
                    'jenis_baku_mutu' => $this->input->post('jenis_bakumutu'),
                    'laporan_bulan_tahun' => $this->input->post('lap_bln'),                
                    'tgl_pengambilan_sample' => date("Y-m-d", strtotime($this->input->post('tgl_pengambilan'))),
                    'waktu_pengambilan_sample' => date("G:i:s", strtotime($this->input->post('waktu_pengambilan'))),
                    'jenis_sample' => $this->input->post('jenis_sample'),
                    'koord_titik_pengambilan_sample' => $koordinat_sample,
                    // 'jml_titik_sample' => $this->input->post('jml_titik_sample'),
                    'tgl_pembuatan' => $tgl_pembuatan,
                    'tgl_update' => $tgl_pembuatan,
                    'dibuat_oleh' =>$this->tank_auth->get_personname(),
                    'diupdate_oleh' => $this->tank_auth->get_personname()
                );
            
            if($this->m_lhu_udara_ambien->insert($param))
                $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
            else
                $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
        }else{
            $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia.<br/>Silahkan ulangi proses input data.'));
        }

        redirect('backend/lhu_udara_ambien');
    }

    public function get_list() {
        # get parameter pagination
        $input = array('dataperpage', 'query', 'curpage');
        foreach ($input as $val)
            $$val = $this->input->post($val); 
     
        $where = "`al`.`jenis_lhu` = 'LHU Udara Ambien' AND (`i`.`nama_industri` LIKE '%".$query."%' OR `l`.`nama_lab` LIKE '%".$query."%' OR `al`.`no_lhu` LIKE '%".$query."%' OR `al`.`laporan_bulan_tahun` LIKE '%".$query."%')";

        $total = $this->m_lhu_udara_ambien->get_num_rows($where);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $types = $this->m_lhu_udara_ambien->get_all($where, $dataperpage, $start);
        $data = array(
            'data' => $types,
            'pagination' => '',
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

        echo json_encode($data);
        exit();
        
    }  

    public function get() {     

        $data = $this->m_lhu_udara_ambien->get_detail_by_id($this->input->post('id_laporan_hasil_uji'));
        $koordinat_sample = ($data->koord_titik_pengambilan_sample) ? unserialize($data->koord_titik_pengambilan_sample) : array();        
        $data->koord_titik_pengambilan_sample = $koordinat_sample;

        echo json_encode($data);
        exit();
    }

    public function edit() {
        
        date_default_timezone_set("Asia/Jakarta");
        $tgl_update = date("Y-m-d H:i:s:");

        // $o = $this->m_lhu_udara_ambien->is_duplicate($this->input->post('industri'), $this->input->post('lap_bln'));    
        // if($o) {

            // $koordinat_sample = serialize($this->input->post('koordinat_sample'));
        $koordinat_sample = $this->functions->parsing_koordinat($this->input->post());

        $param = array(                
                'id_industri' => $this->input->post('industri'),
                'id_lab' => $this->input->post('labs'),                         
                'no_lhu' => $this->input->post('no_lhu'),
                'jenis_lhu' => 'LHU Udara Ambien',
                'jenis_baku_mutu' => $this->input->post('jenis_bakumutu'),
                'laporan_bulan_tahun' => $this->input->post('lap_bln'),                
                'tgl_pengambilan_sample' => date("Y-m-d", strtotime($this->input->post('tgl_pengambilan'))),
                'waktu_pengambilan_sample' => date("G:i:s", strtotime($this->input->post('waktu_pengambilan'))),
                'jenis_sample' => $this->input->post('jenis_sample'),
                'koord_titik_pengambilan_sample' => $koordinat_sample,
                'tgl_pembuatan' => $tgl_pembuatan,
                'tgl_update' => $tgl_pembuatan,
                'dibuat_oleh' =>$this->tank_auth->get_personname(),
                'diupdate_oleh' => $this->tank_auth->get_personname()
            );
        
        if($this->m_lhu_udara_ambien->change_lhu($param, $this->input->post('id')))
            $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
        else
            $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));        
        // }else{
            $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia.<br/>Silahkan ulangi proses input data.'));
        // }
          
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete() {
        $this->m_lhu_udara_ambien->delete_lhu($this->input->post("id"));        
        $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function detail() {
        $id = $this->uri->segment(4);

        $detail = $this->m_detail_lhu->get_detail_by_id($id);
        $tgl_laporan = explode('.', $detail->laporan_bulan_tahun);
        
        $data['action'] = ($detail->jenis_baku_mutu == 'Baku Mutu Izin') ? 'register_izin' : 'register_detail';
        $data['title'] = 'DETAIL LHU AIR LIMBAH '.$detail->nama_industri." ( ".$detail->laporan_bulan_tahun." )";

        $this->load->view('include/header');
        $this->load->view('backend/v_detail_lhu', $data);
        $this->load->view('include/footer');
    }

}
?>