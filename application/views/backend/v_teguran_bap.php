<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Laporan Teguran BAP&nbsp;&nbsp;<small>Generate Laporan Teguran BAP</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Teguran</li>
                <li class="active">Generate Laporan Teguran BAP</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <ol class="breadcrumb default square rsaquo sm">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Pilih Jenis BAP :</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control input-sm" id="bap_type">
                                    <option value="all">Semua BAP</option>
                                    <option value="bap_agro">BAP Agro</option>
                                    <option value="bap_golf">BAP Golf</option>
                                    <option value="bap_hotel">BAP Hotel</option>
                                    <option value="bap_industri">BAP Industri</option>
                                    <option value="bap_lab">BAP Laboratorium</option>
                                    <option value="bap_plb3">BAP Pengelola Limbah B3</option>
                                    <option value="bap_rm">BAP Rumah Makan</option>
                                    <option value="bap_rph">BAP RPH / Peternakan</option>
                                    <option value="bap_rs">BAP Rumah Sakit</option>
                                    <option value="bap_sppbe">BAP SPPBE</option>
                                    <option value="bap_tambang">BAP Pertambangan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-6 text-right">
                        <form action="<?php // echo base_url('/backend/export_excel/teguran_bap');?>" method="post">
                            <button class="btn btn-primary"><i class="fa fa-download"></i>&nbsp;Export to Excel</button>
                            <input type="hidden" name="export" value="export"/>
                        <form>
                    </div> -->
                </div>
            
            </ol>
            
            <div class="the-box ">
                
                <?php echo $this->session->flashdata('msg'); ?>

                 <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th>Jenis Usaha/Kegiatan</th>
                                <th>Alamat Usaha/Kegiatan</th>
                                <th>Tanggal Pemeriksaan BAP</th>
                                <th>Surat Teguran</th>                                  
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
                <!-- END DATA TABLE -->
                <div id="loading" style="display:none;"><div style="background-image: url('<?php echo site_url(); ?>assets/img/loading.gif');background-position: center center;background-repeat: no-repeat;height:50px;"></div></div>

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var base_url = '<?php echo site_url(); ?>';
    
    var Document = {
        param: {
            dataperpage: 10,
            bap_type: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/teguran_bap/get_rekap_teguran"); ?>',
        search: function() {
            this.param.bap_type = $('#bap_type').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                beforeSend: function() { $('#loading').show(); },
                complete: function() { $('#loading').hide(); },
                success: function(result) {
                    console.log(result);
                    $('#pagination').html(result.pagination);
                    $('#start').text(result.total);                 
                    $('#nums').text(result.total);
                    Document.param.numpage = result.numpage;

                    var h = '', ind = {}, no = Document.param.curpage * Document.param.dataperpage;
                     
                    if (result.bap_data.length < 1) {
                        h += '<tr><td colspan=\'7\'>Data Tidak Ditemukan</td></tr>';
                    } else {
                        for (var i = 0; i < result.bap_data.length; i++) {
                            ind = result.bap_data[i];

                            h += '<tr>';
                            h += '<td class="text-center">'+(no+=1)+'</td>';
                            h += '<td>'+ ind.nama_industri +'</td>';
                            h += '<td>'+ ind.jenis_industri +'</td>';
                            h += '<td>'+ ind.alamat +'</td>';
                            h += '<td>'+ $(this).format_date(ind.bap_tgl) +'</td>';
                           
                            h += '<td>';
                            h += '<div class="btn-group btn-group-xs">';
                            h += '<form action="'+ base_url + 'backend/teguran_bap/generate_laporan" method="post"><input type="hidden" name="id_bap" value="'+ ind.id_bap +'" /><button class="btn btn-xs btn-warning" type="submit"><i class="fa fa-download"></i>&nbsp;Unduh</button>';
                            h += '<button type="button" class="btn btn-xs btn-success" onclick="$(this).confirm_bap('+ind.id_bap+')"><i class="fa fa-upload"></i>&nbsp;Unggah</button> </form>';
                            h += '</div>';
                            h += '</td>';

                            h += '<tr>'
                        }
                    }

                    $('#document-data').html(h); 
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        
        $('#bap_type').change(function() {
            Document.search();
        });
        
        // $('#search').click(function() {
          
        //     var type = $('#bap_type').prop('value');
          
        //     $.ajax({
        //         type : 'POST',
        //         url : base_url + 'backend/teguran_bap/get_rekap_teguran',
        //         dataType: 'json',
        //         data : {'bap_type' : type},
        //         beforeSend: function() { $('#loading').show(); },
        //         complete: function() { $('#loading').hide(); },
        //         success : function(result){ 
        //             console.log(result);

        //             var h = '', ind = {}, no = 0;
                    
        //             if (result.bap_data.length < 1) {
        //                 h += '<tr><td colspan=\'7\'>Data Tidak Ditemukan</td></tr>';
        //             } else {
        //                 for (var i = 0; i < result.bap_data.length; i++) {
        //                     ind = result.bap_data[i];

        //                     h += '<tr>';
        //                     h += '<td class="text-center">'+(no+=1)+'</td>';
        //                     h += '<td>'+ ind.nama_industri +'</td>';
        //                     h += '<td>'+ ind.jenis_industri +'</td>';
        //                     h += '<td>'+ ind.alamat +'</td>';
        //                     h += '<td>'+ $(this).format_date(ind.bap_tgl) +'</td>';
        //                     // h += '<td> <form action="'+ base_url + 'backend/teguran_bap/generate_laporan" method="post"><input type="hidden" name="id_bap" value="'+ ind.id_bap +'" /><button class="btn btn-sm btn-warning" type="submit"><i class="fa fa-download"></i>&nbsp;Generate</button></form> </td>';

        //                     h += '<td>';
        //                     h += '<div class="btn-group btn-group-xs">';
        //                     h += '<form action="'+ base_url + 'backend/teguran_bap/generate_laporan" method="post"><input type="hidden" name="id_bap" value="'+ ind.id_bap +'" /><button class="btn btn-warning" type="submit"><i class="fa fa-download"></i>&nbsp;Unduh</button>';
        //                     h += '<button type="button" class="btn btn-default" onclick="$(this).confirm_bap('+ind.id_bap+')"><i class="fa fa-upload"></i>&nbsp;Unggah</button> </form>';
        //                     h += '</div>';
        //                     h += '</td>';

        //                     h += '<tr>'
        //                 }
        //             }

        //             $('#document-data').html(h);     
        //         }
        //     });
        // });

        $.fn.confirm_bap = function (id) {
            $('input[id=id_bap_close]').val(id);
            $('#mdl_konfirm').modal('show');
        }
        
        Document.search();

        window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

    });
</script>

<!-- Modal -->
<div class="modal fade" id="mdl_konfirm" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="uploadLabel">Konfirmasi Teguran BAP</h4>
            </div>
            <form action="<?php echo base_url('/backend/teguran_bap/conf_upload') ?>" method="post" id="form_add" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group status">                        
                        <label>Nama Pengirim/ Eksekutor</label>
                        <input type="text" title="Nama Pengirim/ Eksekutor" class="form-control input-sm" name="conf_pengirim">                                    
                    </div>
                    <div class="row">                                                               
                        <div class="form-group col-lg-6">                        
                            <label>Via</label>
                            <input type="text" title="Via" value="" class="form-control input-sm" name="conf_via">                                    
                        </div>
                        <div class="form-group  col-lg-6">
                            <label>Tanggal Kirim</label>
                            <div class="input-group input-group-sm">
                                <input type="text" name="conf_tgl_kirim" class="form-control datepicker" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>   
                    </div>
                    <div class="form-group">
                        <label>Upload Berkas</label>
                        <div class="input-group input-group-sm">
                            <input type="text" readonly="" class="form-control">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    Browse… <input type="file" name="userfile[]" multiple />
                                </span>
                            </span>
                        </div><!-- /.input-group -->
                    </div>                                                                                                  
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id_bap_close" name="id_bap" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>