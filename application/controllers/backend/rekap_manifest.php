<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class rekap_manifest extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_rekap_manifest', '', TRUE);
		$this->load->model('m_industry', '', TRUE);
		$this->load->model('m_pengangkut_limbah', '', TRUE);
		$this->load->model('m_pengolah_limbah', '', TRUE);
		$this->load->model('m_data_master', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types
		$data['industri'] = $this->m_industry->get_dropdown();
		$data['pengangkut'] = $this->m_pengangkut_limbah->get_dropdown();
		$data['pengolah'] = $this->m_pengolah_limbah->get_dropdown();
		$jenis_data_master = "jenis_limbah";
		$data['data_acuan'] = $this->m_data_master->get_dropdown($jenis_data_master);
		
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_rekap_manifest', $data);
        $this->load->view('include/footer');
    }
	
	public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				
			);

     	$total = $this->m_rekap_manifest->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_rekap_manifest->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_rekap_manifest->get_by_id($this->input->post('id_rekap_manifest')));
		exit();
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'id_penghasil_limbah' => $this->input->post('penghasil_limbah'),
			'tanggal' => date("Y-m-d", strtotime($this->input->post('tanggal'))),
			'jenis_limbah' => $this->input->post('jenis_limbah'),
			'jml_dokumen' => $this->input->post('jml_dokumen'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_rekap_manifest->change($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/rekap_manifest');
	}
	
	public function register() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_pembuatan = date("Y-m-d H:i:s:");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'id_penghasil_limbah' => $this->input->post('penghasil_limbah'),
			'tanggal' => date("Y-m-d", strtotime($this->input->post('tanggal'))),
			'jenis_limbah' => $this->input->post('jenis_limbah'),
			'jml_dokumen' => $this->input->post('jml_dokumen'),
			'tgl_pembuatan' => $tgl_pembuatan,
			'dibuat_oleh' => $this->tank_auth->get_personname(),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_rekap_manifest->insert($param))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
			
		redirect('backend/rekap_manifest');
	}
	
	public function delete() {
		$this->m_rekap_manifest->delete($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data gagal dihapus'));
		redirect('backend/rekap_manifest');
	}
	
	public function upload() {
		$id = $this->input->post('id');

		$d = $this->m_rekap_manifest->get_by_id($id);
		$i = $this->m_industry->get_industry_by_id($d->id_penghasil_limbah);		
		if($d) {

			$temp = pathinfo($_FILES["userfile"]["name"]);
			$path = "assets/manifest/";
			$newfilename = str_replace(' ', '_', strtolower(trim($i->nama_industri))).'_'.str_replace('.', '_', $d->tgl).".".$temp['extension'];

			$config['upload_path'] = realpath($path);
			$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png|doc|DOC|docx|DOCX|xls|XLS|xlsx|XLSX';
			$config['max_size'] = '8000';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $newfilename;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload()) {
				
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', $this->upload->display_errors()));
			}else{
				$param = array('nama_berkas' => $newfilename);
				$this->m_rekap_manifest->change($param, $id);
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Berkas berhasil diunggah'));
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}
	
}
