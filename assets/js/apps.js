$(document).ready(function(){

	/** SIDEBAR FUNCTION **/
	$('.sidebar-left ul.sidebar-menu li a').click(function() {
		"use strict";
		$('.sidebar-left li').removeClass('active');
		$(this).closest('li').addClass('active');	
		var checkElement = $(this).next();
			if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
				$(this).closest('li').removeClass('active');
				checkElement.slideUp('fast');
			}
			if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
				$('.sidebar-left ul.sidebar-menu ul:visible').slideUp('fast');
				checkElement.slideDown('fast');
			}
			if($(this).closest('li').find('ul').children().length == 0) {
				return true;
			} else {
				return false;	
			}		
	});

	if ($(window).width() < 1025) {
		$(".sidebar-left").removeClass("sidebar-nicescroller");
		$(".sidebar-right").removeClass("right-sidebar-nicescroller");
		$(".nav-dropdown-content").removeClass("scroll-nav-dropdown");
	}
	/** END SIDEBAR FUNCTION **/
	
	
	/** BUTTON TOGGLE FUNCTION **/
	$(".btn-collapse-sidebar-left").click(function(){
		"use strict";
		$(".top-navbar").toggleClass("toggle");
		$(".sidebar-left").toggleClass("toggle");
		$(".page-content").toggleClass("toggle");
		$(".icon-dinamic").toggleClass("rotate-180");
		
		if ($(window).width() > 991) {
			if($(".sidebar-right").hasClass("toggle-left") === true){
				$(".sidebar-right").removeClass("toggle-left");
				$(".top-navbar").removeClass("toggle-left");
				$(".page-content").removeClass("toggle-left");
				$(".sidebar-left").removeClass("toggle-left");
				if($(".sidebar-left").hasClass("toggle") === true){
					$(".sidebar-left").removeClass("toggle");
				}
				if($(".page-content").hasClass("toggle") === true){
					$(".page-content").removeClass("toggle");
				}
			}
		}
	});
	$(".btn-collapse-sidebar-right").click(function(){
		"use strict";
		$(".top-navbar").toggleClass("toggle-left");
		$(".sidebar-left").toggleClass("toggle-left");
		$(".sidebar-right").toggleClass("toggle-left");
		$(".page-content").toggleClass("toggle-left");
	});
	$(".btn-collapse-nav").click(function(){
		"use strict";
		$(".icon-plus").toggleClass("rotate-45");
	});
	/** END BUTTON TOGGLE FUNCTION **/
	
	
	/** BEGIN PREETY PRINT **/
	$(window).load(function() {
    "use strict";
	prettyPrint()})
	/** END PREETY PRINT **/
	

	/** BEGIN TOOLTIP FUNCTION **/
	$('.tooltips').tooltip({
	  selector: "[data-toggle=tooltip]",
	  container: "body"
	})
	$('.popovers').popover({
	  selector: "[data-toggle=popover]",
	  container: "body"
	})
	/** END TOOLTIP FUNCTION **/
	
	
	/** NICESCROLL AND SLIMSCROLL FUNCTION **/
    $(".sidebar-nicescroller").niceScroll({
		cursorcolor: "#121212",
		cursorborder: "0px solid #fff",
		cursorborderradius: "0px",
		cursorwidth: "0px"
	});
    $(".sidebar-nicescroller-visible-scroller").niceScroll({
		cursorcolor: "#121212",
		cursorborder: "0px solid #fff",
		cursorborderradius: "0px",
		cursorwidth: "5px",
		cursoropacitymax: 0.2
	});
	$(".sidebar-nicescroller").getNiceScroll().resize();
    $(".right-sidebar-nicescroller").niceScroll({
		cursorcolor: "#111",
		cursorborder: "0px solid #fff",
		cursorborderradius: "0px",
		cursorwidth: "0px"
	});
	$(".right-sidebar-nicescroller").getNiceScroll().resize();
	
	$(function () {
		"use strict";
		$('.scroll-nav-dropdown').slimScroll({
			height: '350px',
			position: 'right',
			size: '4px',
			railOpacity: 0.3
		});
	});
	
	$(function () {
		"use strict";
		$('.scroll-chat-widget').slimScroll({
			height: '330px',
			position: 'right',
			size: '4px',
			railOpacity: 0.3,
			railVisible: true,
			alwaysVisible: true,
			start : 'bottom'
		});
	});
	if ($(window).width() < 768) {
		$(".chat-wrap").removeClass("scroll-chat-widget");
	}
	/** END NICESCROLL AND SLIMSCROLL FUNCTION **/
	
	
	
	
	/** BEGIN BACK TO TOP **/
	$(function () {
		$("#back-top").hide();
	});
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
	/** END BACK TO TOP **/
	

	
	
	/** BEGIN PANEL HEADER BUTTON COLLAPSE **/
	$(function () {
		"use strict";
		$('.collapse').on('show.bs.collapse', function() {
			var id = $(this).attr('id');
			$('button.to-collapse[data-target="#' + id + '"]').html('<i class="fa fa-chevron-up"></i>');
		});
		$('.collapse').on('hide.bs.collapse', function() {
			var id = $(this).attr('id');
			$('button.to-collapse[data-target="#' + id + '"]').html('<i class="fa fa-chevron-down"></i>');
		});
		
		$('.collapse').on('show.bs.collapse', function() {
			var id = $(this).attr('id');
			$('a.block-collapse[href="#' + id + '"] span.right-icon').html('<i class="glyphicon glyphicon-minus icon-collapse"></i>');
		});
		$('.collapse').on('hide.bs.collapse', function() {
			var id = $(this).attr('id');
			$('a.block-collapse[href="#' + id + '"] span.right-icon').html('<i class="glyphicon glyphicon-plus icon-collapse"></i>');
		});
	});
	/** END PANEL HEADER BUTTON COLLAPSE **/
	
	
	/** BEGIN DATATABLE EXAMPLE **/
	// if ($('#datatable-example').length > 0){
	// 	$('#datatable-example').dataTable();
		// $("#datatable-example").dynatable();
	// }
		
	/** BEGIN SUMMERNOTE **/
	// if ($('.summernote-lg').length > 0){
	// 	$('.summernote-lg').summernote({
	// 		height: 400
	// 	});
	// }
	
	// if ($('.summernote-sm').length > 0){
	// 	$('.summernote-sm').summernote({
	// 		height: 200,
	// 		  toolbar: [
	// 			//['style', ['style']], // no style button
	// 			['style', ['bold', 'italic', 'underline', 'clear']],
	// 			['font', ['strike']],
	// 			['fontsize', ['fontsize']],
	// 			['color', ['color']],
	// 			['para', ['ul', 'ol', 'paragraph']],
	// 			['height', ['height']],
	// 			//['insert', ['picture', 'link']], // no insert buttons
	// 			//['table', ['table']], // no table button
	// 			//['help', ['help']] //no help button
	// 		  ]
	// 	});
	// }
	/** END SUMMERNOTE **/
	

   

	/** BEGIN CHOSEN JS **/
	$(function () {
		"use strict";
		var configChosen = {
		  '.chosen-select'           : {width: "100%"},
		  '.chosen-select-deselect'  : {allow_single_deselect:true},
		  '.chosen-select-no-single' : {disable_search_threshold:10},
		  '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		  '.chosen-select-width'     : {width:"100%"}
		}
		for (var selector in configChosen) {
		  $(selector).chosen(configChosen[selector]);
		}
	});
	/** END CHOSEN JS **/
	
	
	/** BEGIN BACK STRETCH **/
	if ($('#weather-widget-1').length > 0){
		$("#weather-widget-1").backstretch("assets/img/photo/medium/img-12.jpg");
	}
	if ($('#weather-widget-2').length > 0){
		$("#weather-widget-2").backstretch("assets/img/photo/medium/img-13.jpg");
	}
	if ($('#search-heading').length > 0){
		$("#search-heading").backstretch("assets/img/photo/medium/img-12.jpg");
	}
	if ($('#property-search-wrap').length > 0){
		$("#property-search-wrap").backstretch("assets/img/photo/medium/img-19.jpg");
	}
	/** END BACK STRETCH **/
	
	
	/** BEGIN BACK STRETCH **/
	if ($('#example-datatable').length > 0){
		$('#example-datatable').dataTable();
	}
	/** END BACK STRETCH **/
   

	/** BEGIN ICHECK **/
	/** Minimal Skins **/
	if ($('.i-black').length > 0){
		$('input.i-black').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal',
			increaseArea: '20%'
		});
	}
	if ($('.i-red').length > 0){
		$('input.i-red').iCheck({
			checkboxClass: 'icheckbox_minimal-red',
			radioClass: 'iradio_minimal-red',
			increaseArea: '20%'
		});
	}
	if ($('.i-green').length > 0){
		$('input.i-green').iCheck({
			checkboxClass: 'icheckbox_minimal-green',
			radioClass: 'iradio_minimal-green',
			increaseArea: '20%'
		});
	}
	if ($('.i-blue').length > 0){
		$('input.i-blue').iCheck({
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue',
			increaseArea: '20%'
		});
	}
	if ($('.i-aero').length > 0){
		$('input.i-aero').iCheck({
			checkboxClass: 'icheckbox_minimal-aero',
			radioClass: 'iradio_minimal-aero',
			increaseArea: '20%'
		});
	}
	if ($('.i-grey').length > 0){
		$('input.i-grey').iCheck({
			checkboxClass: 'icheckbox_minimal-grey',
			radioClass: 'iradio_minimal-grey',
			increaseArea: '20%'
		});
	}
	if ($('.i-orange').length > 0){
		$('input.i-orange').iCheck({
			checkboxClass: 'icheckbox_minimal-orange',
			radioClass: 'iradio_minimal-orange',
			increaseArea: '20%'
		});
	}
	if ($('.i-yellow').length > 0){
		$('input.i-yellow').iCheck({
			checkboxClass: 'icheckbox_minimal-yellow',
			radioClass: 'iradio_minimal-yellow',
			increaseArea: '20%'
		});
	}
	if ($('.i-pink').length > 0){
		$('input.i-pink').iCheck({
			checkboxClass: 'icheckbox_minimal-pink',
			radioClass: 'iradio_minimal-pink',
			increaseArea: '20%'
		});
	}
	if ($('.i-purple').length > 0){
		$('input.i-purple').iCheck({
			checkboxClass: 'icheckbox_minimal-purple',
			radioClass: 'iradio_minimal-purple',
			increaseArea: '20%'
		});
	}
		
	/** Square Skins **/
	if ($('.i-black-square').length > 0){
		$('input.i-black-square').iCheck({
			checkboxClass: 'icheckbox_square',
			radioClass: 'iradio_square',
			increaseArea: '20%'
		});
	}
	if ($('.i-red-square').length > 0){
		$('input.i-red-square').iCheck({
			checkboxClass: 'icheckbox_square-red',
			radioClass: 'iradio_square-red',
			increaseArea: '20%'
		});
	}
	if ($('.i-green-square').length > 0){
		$('input.i-green-square').iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
			increaseArea: '20%'
		});
	}
	if ($('.i-blue-square').length > 0){
		$('input.i-blue-square').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	}
	if ($('.i-aero-square').length > 0){
		$('input.i-aero-square').iCheck({
			checkboxClass: 'icheckbox_square-aero',
			radioClass: 'iradio_square-aero',
			increaseArea: '20%'
		});
	}
	if ($('.i-grey-square').length > 0){
		$('input.i-grey-square').iCheck({
			checkboxClass: 'icheckbox_square-grey',
			radioClass: 'iradio_square-grey',
			increaseArea: '20%'
		});
	}
	if ($('.i-orange-square').length > 0){
		$('input.i-orange-square').iCheck({
			checkboxClass: 'icheckbox_square-orange',
			radioClass: 'iradio_square-orange',
			increaseArea: '20%'
		});
	}
	if ($('.i-yellow-square').length > 0){
		$('input.i-yellow-square').iCheck({
			checkboxClass: 'icheckbox_square-yellow',
			radioClass: 'iradio_square-yellow',
			increaseArea: '20%'
		});
	}
	if ($('.i-pink-square').length > 0){
		$('input.i-pink-square').iCheck({
			checkboxClass: 'icheckbox_square-pink',
			radioClass: 'iradio_square-pink',
			increaseArea: '20%'
		});
	}
	if ($('.i-purple-square').length > 0){
		$('input.i-purple-square').iCheck({
			checkboxClass: 'icheckbox_square-purple',
			radioClass: 'iradio_square-purple',
			increaseArea: '20%'
		});
	}
		
	/** Flat Skins **/
	if ($('.i-black-flat').length > 0){
		$('input.i-black-flat').iCheck({
			checkboxClass: 'icheckbox_flat',
			radioClass: 'iradio_flat',
			increaseArea: '20%'
		});
	}
	if ($('.i-red-flat').length > 0){
		$('input.i-red-flat').iCheck({
			checkboxClass: 'icheckbox_flat-red',
			radioClass: 'iradio_flat-red',
			increaseArea: '20%'
		});
	}
	if ($('.i-green-flat').length > 0){
		$('input.i-green-flat').iCheck({
			checkboxClass: 'icheckbox_flat-green',
			radioClass: 'iradio_flat-green',
			increaseArea: '20%'
		});
	}
	if ($('.i-blue-flat').length > 0){
		$('input.i-blue-flat').iCheck({
			checkboxClass: 'icheckbox_flat-blue',
			radioClass: 'iradio_flat-blue',
			increaseArea: '20%'
		});
	}
	if ($('.i-aero-flat').length > 0){
		$('input.i-aero-flat').iCheck({
			checkboxClass: 'icheckbox_flat-aero',
			radioClass: 'iradio_flat-aero',
			increaseArea: '20%'
		});
	}
	if ($('.i-grey-flat').length > 0){
		$('input.i-grey-flat').iCheck({
			checkboxClass: 'icheckbox_flat-grey',
			radioClass: 'iradio_flat-grey',
			increaseArea: '20%'
		});
	}
	if ($('.i-orange-flat').length > 0){
		$('input.i-orange-flat').iCheck({
			checkboxClass: 'icheckbox_flat-orange',
			radioClass: 'iradio_flat-orange',
			increaseArea: '20%'
		});
	}
	if ($('.i-yellow-flat').length > 0){
		$('input.i-yellow-flat').iCheck({
			checkboxClass: 'icheckbox_flat-yellow',
			radioClass: 'iradio_flat-yellow',
			increaseArea: '20%'
		});
	}
	if ($('.i-pink-flat').length > 0){
		$('input.i-pink-flat').iCheck({
			checkboxClass: 'icheckbox_flat-pink',
			radioClass: 'iradio_flat-pink',
			increaseArea: '20%'
		});
	}
	if ($('.i-purple-flat').length > 0){
		$('input.i-purple-flat').iCheck({
			checkboxClass: 'icheckbox_flat-purple',
			radioClass: 'iradio_flat-purple',
			increaseArea: '20%'
		});
	}
	/** END ICHECK **/
	

   

	/** BEGIN INPUT FILE **/
	if ($('.btn-file').length > 0){
		$(document)
			.on('change', '.btn-file :file', function() {
				"use strict";
				var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				input.trigger('fileselect', [numFiles, label]);
		});
		$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
			
			var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;
			
			if( input.length ) {
				input.val(log);
			} else {
				// if( log ) alert(log);
				if (log);
			}
		});
	}
	/** END INPUT FILE **/
	
	
	if($('.datepicker').length > 0) {	
		$('.datepicker').datepicker({autoclose: true, locale: 'id'}); 
	}

	if($('.multiselect').length > 0) {
		$('.multiselect').multiselect({buttonWidth: '100%', buttonClass: 'btn btn-default'});
	}

		
	/** BEGIN TIMEPICKER **/
	if ($('.timepicker').length > 0){
		$('.timepicker').timepicker({format: 'HH:mm', showSeconds: false, showMeridian: false, secondStep: 1, minuteStep: 1});
	}
	/** END TIMEPICKER **/
	
	
	
	/** BEGIN INPUT MASK **/
	$(function () {
		"use strict";
		$('.cc_masking').mask('0000-0000-0000-0000');
		$('.cc_security_masking').mask('0000');
		$('.date_masking').mask('00/00/0000');
		$('.time_masking').mask('00:00:00');
		$('.date_time_masking').mask('00/00/0000 00:00:00');
		$('.phone_us_masking').mask('(000) 000-0000');
		$('.cpf_masking').mask('000.000.000-00', {reverse: true});
		$('.money_masking').mask('000.000.000.000.000,00', {reverse: true});
		$('.money2_masking').mask("#.##0,00", {reverse: true, maxlength: false});
		$('.ip_address_masking').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation: {'Z': {pattern: /[0-9]/, optional: true}}});
		$('.ip_address_masking').mask('099.099.099.099');
		$('.percent_masking').mask('##0,00%', {reverse: true});				
	});
	/** END INPUT MASK **/
	
	
	
	/** BEGIN EXAMPLE SIMPLE WIZARD **/
	if ($('.NextStep').length > 0){
		$('.NextStep').click(function(){
		"use strict";
		  var nextId = $(this).parents('.tab-pane').next().attr("id");
		  $('[href=#'+nextId+']').tab('show');
		})
	}
	if ($('.PrevStep').length > 0){
		$('.PrevStep').click(function(){
		"use strict";
		  var prevId = $(this).parents('.tab-pane').prev().attr("id");
		  $('[href=#'+prevId+']').tab('show');
		})
	}
	/** END EXAMPLE SIMPLE WIZARD **/
	
	
	
	/** BEGIN SVG WEATHER ICON **/
	if (typeof Skycons !== 'undefined'){
	var icons = new Skycons(
	  {"color": "#fff"},
	  {"resizeClear": true}
	  ),
          list  = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
          ],
          i;

      for(i = list.length; i--; )
      icons.set(list[i], list[i]);
      icons.play();
	};
	/** END SVG WEATHER ICON **/

	

	/** BEGIN SLIDER **/
	if ($('#sl1').length > 0){
		$('#sl1').slider({
		  formater: function(value) {
			return 'Current value: '+value;
		  }
		});
	}
	if ($('#sl2').length > 0){
		$('#sl2').slider();
	}
	if ($('#eg').length > 0){
		$('#eg input').slider();
	}
	/** END SLIDER **/
	
	

   
	/** BEGIN MAGNIFIC POPUP **/
	if ($('.magnific-popup-wrap').length > 0){
			$('.magnific-popup-wrap').each(function() {
			"use strict";
				$(this).magnificPopup({
					delegate: 'a.zooming',
					type: 'image',
					removalDelay: 300,
					mainClass: 'mfp-fade',
					gallery: {
					  enabled:true
					}
				});
			}); 
	}
	
	if ($('.inline-popups').length > 0){
			$('.inline-popups').magnificPopup({
			  delegate: 'a',
			  removalDelay: 500,
			  callbacks: {
				beforeOpen: function() {
				   this.st.mainClass = this.st.el.attr('data-effect');
				}
			  },
			  midClick: true
			});
	}
	$('.magnific-img').magnificPopup({
		type:'image',
		removalDelay: 300,
		mainClass: 'mfp-fade'
	});
	/** END MAGNIFIC POPUP **/
	


	/** BEGIN OWL CAROUSEL **/
	if ($('#owl-carousel-single-1').length > 0){
		$("#owl-carousel-single-1").owlCarousel({
		  navigation : true,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem:true,
		  navigationText : ["&lsaquo;","&rsaquo;"],
		  autoPlay : true,
		  stopOnHover : true,
		});
	}
	
	if ($('#owl-carousel-single-2').length > 0){
		$("#owl-carousel-single-2").owlCarousel({
		  navigation : false,
		  slideSpeed : 400,
		  paginationSpeed : 400,
		  singleItem:true
		});
	}
	
	if ($('#owl-lazy-load').length > 0){
	  $("#owl-lazy-load").owlCarousel({
		items : 5,
		lazyLoad : true,
		navigation : true
	  });
	}
	
	if ($('#owl-lazy-load-autoplay').length > 0){
	  $("#owl-lazy-load-autoplay").owlCarousel({
		autoPlay: 3000,
		items : 5,
		lazyLoad : true
	  });
	}
	
	if ($('#owl-lazy-load-gallery').length > 0){
	  $("#owl-lazy-load-gallery").owlCarousel({
		items : 5,
		lazyLoad : true,
		navigation : true
	  });
	}
	
	
	var Owltime = 7;
	var $progressBar,
	  $bar, 
	  $elem, 
	  isPause, 
	  tick,
	  percentTime;
	 
	if ($('#owl-single-progress-bar').length > 0){
		$("#owl-single-progress-bar").owlCarousel({
		  slideSpeed : 500,
		  paginationSpeed : 500,
		  singleItem : true,
		  afterInit : progressBar,
		  afterMove : moved,
		  startDragging : pauseOnDragging
		});
	}
 
	function progressBar(elem){
	  $elem = elem;
	  buildProgressBar();
	  start();
	}
 
	function buildProgressBar(){
	  $progressBar = $("<div>",{
		id:"OwlprogressBar"
	  });
	  $bar = $("<div>",{
		id:"Owlbar"
	  });
	  $progressBar.append($bar).prependTo($elem);
	}
		 
	function start() {
	  percentTime = 0;
	  isPause = false;
	  tick = setInterval(interval, 10);
	};
 
	function interval() {
	  if(isPause === false){
		percentTime += 1 / Owltime;
		$bar.css({
		   width: percentTime+"%"
		 });
		if(percentTime >= 100){
		  $elem.trigger('owl.next')
		}
	  }
	}
		 
	function pauseOnDragging(){
	  isPause = true;
	}
 
	function moved(){
	  clearTimeout(tick);
	  start();
	}
	/** END OWL CAROUSEL **/
		
	
	/** BEGIN STORE DESIGN JS FUNCTION **/
	if ($('#store-item-carousel-1').length > 0){
	$("#store-item-carousel-1").owlCarousel({
		autoPlay: 4000,
		items : 4,
		itemsDesktop      : [1199,4],
		itemsDesktopSmall : [979,3],
		itemsTablet       : [768,2],
		itemsMobile       : [479,1],
		lazyLoad : true,
		autoHeight : true
	  });
	}
	if ($('#store-item-carousel-2').length > 0){
	  $("#store-item-carousel-2").owlCarousel({
			navigation : false,
			pagination: false,
			slideSpeed : 1000,
			paginationSpeed : 400,
			singleItem:true
	  });
	}
	if ($('#store-item-carousel-3').length > 0){
		$("#store-item-carousel-3").owlCarousel({
			autoPlay: 4000,
			items : 4,
			itemsDesktop      : [1199,4],
			itemsDesktopSmall : [979,3],
			itemsTablet       : [768,2],
			itemsMobile       : [479,1],
			lazyLoad : true,
			autoHeight : true,
			navigation : false,
			pagination: false
		  });
	}
	/** END STORE DESIGN JS FUNCTION **/
	
	
	
	/** BEGIN TILES JS FUNCTION **/
	  if ($('#tiles-slide-1').length > 0){
		  $("#tiles-slide-1").owlCarousel({
				navigation : true,
				pagination: false,
				slideSpeed : 1000,
				paginationSpeed : 400,
				singleItem:true,
				autoPlay: 3000,
				theme : "my-reminder",
				navigationText : ["&larr;","&rarr;"],
		  });
	  }
	  if ($('#tiles-slide-2').length > 0){
		  $("#tiles-slide-2").owlCarousel({
				navigation : false,
				pagination: false,
				slideSpeed : 1000,
				paginationSpeed : 400,
				singleItem:true,
				autoPlay: 3000,
				transitionStyle : 'backSlide',
				stopOnHover: true
		  });
	  }
	  if ($('#tiles-slide-3').length > 0){
		  $("#tiles-slide-3").owlCarousel({
				navigation : false,
				pagination: false,
				slideSpeed : 1000,
				paginationSpeed : 400,
				singleItem:true,
				autoPlay: 3235,
				stopOnHover: true
		  });
	  }
	/** END TILES JS FUNCTION **/
	
	/** BEGIN MY PHOTOS COLLECTION FUNCTION **/
	if ($('#photo-collection-1').length > 0){
	  $("#photo-collection-1").owlCarousel({
			navigation : false,
			pagination: false,
			slideSpeed : 1000,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay: 3000,
			transitionStyle : 'fadeUp'
	  });
	}
	/** BEGIN MY PHOTOS COLLECTION FUNCTION **/
	
	if ($('.widget-newsticker').length > 0){
		$('.widget-newsticker').newsTicker({
			row_height: 135,
			max_rows: 3,
			speed: 600,
			direction: 'up',
			duration: 4000,
			autostart: 1,
			pauseOnHover: 1,
			prevButton: $('#w-newsticker-prev'),
			nextButton: $('#w-newsticker-next')
		});
	}

	if ($('.widget-currency-ticker').length > 0){
		$('.widget-currency-ticker').newsTicker({
			row_height: 41,
			max_rows: 10,
			speed: 500,
			direction: 'up',
			duration: 3000,
			autostart: 1,
			pauseOnHover: 1
		});
	}
	
	
	if ($('#tiles-slide-4').length > 0){
	  $("#tiles-slide-4").owlCarousel({
			navigation : false,
			pagination: false,
			slideSpeed : 1000,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay: 3235,
			stopOnHover: true
	  });
	}
	
	if ($('#tiles-slide-5').length > 0){
	  $("#tiles-slide-5").owlCarousel({
			navigation : false,
			pagination: false,
			slideSpeed : 1000,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay: 3235,
			stopOnHover: true
	  });
	}
	
	if ($('#tiles-slide-6').length > 0){
	  $("#tiles-slide-6").owlCarousel({
			navigation : false,
			pagination: false,
			slideSpeed : 1000,
			paginationSpeed : 400,
			singleItem:true,
			autoPlay: 3235,
			stopOnHover: true
	  });
	}
	
	if ($('#remove-top-notification').length > 0){
		$("#remove-top-notification").click(function(){
			"use strict";
			$("body").removeClass("has-top-notification");
			$(".sidebar-right-heading").removeClass("has-top-notification");
			$(".top-navbar").removeClass("has-top-notification");
			$(".sidebar-left").removeClass("has-top-notification");
			$(".sidebar-right").removeClass("has-top-notification");
			$(".top-notification").addClass("close-notification");
		});
	}
	
	
	$(".btn-collapse-sidebar-left-2").click(function(){
		"use strict";
		$(".top-navbar").toggleClass("toggle");
		$(".sidebar-left").toggleClass("hsl-toggle");
		$(".page-content").toggleClass("hsl-toggle");
		if ($(window).width() > 991) {
			if($(".sidebar-right").hasClass("toggle-left") === true){
				$(".sidebar-right").removeClass("toggle-left");
				$(".top-navbar").removeClass("toggle-left");
				$(".page-content").removeClass("toggle-left");
				$(".sidebar-left").removeClass("toggle-left");
				if($(".sidebar-left").hasClass("toggle") === true){
					$(".sidebar-left").removeClass("toggle");
				}
				if($(".page-content").hasClass("toggle") === true){
					$(".page-content").removeClass("toggle");
				}
			}
		}
		if ($(window).width() < 991) {
			$(".sidebar-left").toggleClass("hsl-toggle-left");
			$(".page-content").toggleClass("hsl-toggle-left");
		}
	});
	
	// # FORM RESET	
	if($('#myModal').length > 0){
		$('#myModal').on('hide.bs.modal', function() {
			$('#myModal form')[0].reset();
			$('#myModal form input[type=hidden]').val('');

			$("label.error").hide();
  			$(".error").removeClass("error");  			

		}).on('show.bs.modal', function(e) {
			var act = $("#myModal form").data('action');		

			if(e.relatedTarget) act += '/register';
			else act += '/edit';

			// console.log(e);
			$("#myModal form").attr('action',act);
		});
		
		if($('#delete').length > 0){
			$('.delete-row').click(function(e) {
				$('input[id=deleted_id]').val($(this).data('id'));
				$('#delete').modal('show');
			})
		}
	};	
	
	// # NUMBER ONLY	
	$('.numberonly').on('keydown', function(event) {
		// Allow special chars + arrows 
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
			|| event.keyCode == 27 || event.keyCode == 13 
			|| (event.keyCode == 65 && event.ctrlKey === true) 
			|| (event.keyCode >= 35 && event.keyCode <= 39)){
				return;
		}else {
			// If it's not a number stop the keypress
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});

	$.fn.get_month = function(id) {
		switch (id) {
			case 1: return "Januari"; break;
			case 2: return "Februari"; break;
			case 3: return "Maret"; break;
			case 4: return "April"; break;
			case 5: return "Mei"; break;
			case 6: return "Juni"; break;
			case 7: return "Juli"; break;
			case 8: return "Agustus"; break;
			case 9: return "September"; break;
			case 10: return "Oktober"; break;
			case 11: return "November"; break;
			case 12: return "Desember"; break;
		}
	}

	window.setTimeout(function () { $(".alert").alert('close'); }, 8000);	

	$.fn.format_date = function (date) {
	    var d = new Date(date || Date.now()),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [day, month, year].join('.');
	}

	$.fn.status_lhu = function(id) {
		switch(id) {
			case '1': return '<span class="label label-success">Close</span>'; break;			
			default: return '<span class="label label-danger">Open</span>'; break;
		}
	}

	var addFormGroup = function (event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.form-group');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
        var $formGroupClone = $formGroup.clone();

        $(this)
            .toggleClass('btn-default btn-add btn-danger btn-remove')
            .html('<i class="fa fa-minus-circle"></i>');

        $formGroupClone.find('input').val('');
        $formGroupClone.insertAfter($formGroup);

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
            $lastFormGroupLast.find('.btn-add').attr('disabled', true);
        }
    };

    var removeFormGroup = function (event) {
        event.preventDefault();

        var $formGroup = $(this).closest('.form-group');
        var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

        var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
        if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
            $lastFormGroupLast.find('.btn-add').attr('disabled', false);
        }

        $formGroup.remove();
    };

    var countFormGroup = function ($form) {
        return $form.find('.form-group').length;
    };

    $(document).on('click', '.btn-add', addFormGroup);
    $(document).on('click', '.btn-remove', removeFormGroup);

    $.fn.reset_form = function(form_id, active, target) {

    	// # clear all form
    	$('#'+form_id+' form')[0].reset();
		$('#'+form_id+' form input[type=hidden]').val('');		
		$('#'+form_id+' form').find('input:text, input:password, select, textarea').val('');
        $('#'+form_id+' form select').trigger("chosen:updated");
		$('#'+form_id+' form').find('input:radio, input:checkbox').prop('checked', false);

		if(active) {
			var t = '';
			t += '<div class="form-group input-group input-group-sm">';
			t += '<span class="input-group-addon"><i class="fa fa-globe"></i></span>';
			t += '<input type="text" name="'+target+'[]" class="form-control" placeholder="(12D:12:34,25:23),(10D:11:32,40:13)">';
			t += '<span class="input-group-btn"><button type="button" class="btn btn-default btn-add"><i class="fa fa-plus-circle"></i></button></span>';
			t += '</div>';
			$('#' + target).html(t);
		}
	}
	
	$.fn.reset_form_exclude_add = function(form_id, active, target) {
		
		// # clear all form
    	$('#'+form_id+' form')[0].reset();
		$('#'+form_id+' form').find('input:text, input:password, select, textarea').val('');
		$('#'+form_id+' form').find('input:radio, input:checkbox').prop('checked', false);
	
		if(active) {
			var t = '';
			t += '<div class="form-group input-group input-group-sm">';
			t += '<span class="input-group-addon"><i class="fa fa-globe"></i></span>';
			t += '<input type="text" name="'+target+'[]" class="form-control" placeholder="(12D:12:34,25:23),(10D:11:32,40:13)">';
			t += '</div>';
			$('#' + target).html(t);
		}
	}	
	$.fn.reset_form_jenis_limbah = function(form_id, active, target) {
		
		// # clear all form
    	$('#'+form_id+' form')[0].reset();
		$('#'+form_id+' form').find('input:text, input:password, select, textarea').val('');
		$('#'+form_id+' form').find('input:radio, input:checkbox').prop('checked', false);
		
		
		if(active) {
			var t = '';
			t += '<div class="form-group input-group input-group-sm">';
			t += '<span class="input-group-addon"><i class="fa fa-globe"></i></span>';
			t += '<input type="text" name="'+target+'[]" class="form-control" placeholder="Limbah Batubara:15">';
			t += '<span class="input-group-btn"><button type="button" class="btn btn-default btn-add"><i class="fa fa-plus-circle"></i></button></span>';
			t += '</div>';
			$('#' + target).html(t);
		}
	}

	$.fn.return_value = function(variable, digit) {
		if(digit) {
			return (variable) ? variable : '0';
		}
		return (variable) ? variable : '-';
	}

	$.fn.koordinat_template = function(target, data) {

		var t = '';
		$.each(data, function(i, v) {	

			var koor = v.split('/');
			var s = koor[0].split(':');
			var e = koor[1].split(':');

			t += '<div class="form-group row" data-duplicate="koordinat">';
			t += '<div class="col-lg-5">';
			t += '<label>Koordinat S</label>';
			t += '<div class="row">';                    
			t += '<div class="col-lg-4" style="padding-right: 0;">';
			t += '<input type="text" name="s_deg[]" class="form-control input-sm derajat" value="'+s[0]+'" required="required" placeholder="00&deg;">';
			t += '</div>';
			t += '<div class="col-lg-4" style="padding-right: 0;">';
			t += '<input type="text" name="s_squo[]" class="form-control input-sm" value="'+s[1]+'" required="required" placeholder="00&#39;">';
			t += '</div>';
			t += '<div class="col-lg-4" style="padding-right: 0;">';
			t += '<input type="text" name="s_quo[]" class="form-control input-sm" value="'+s[2]+'" required="required" placeholder="00&quot;">';
			t += '</div>';
			t += '</div>';
			t += '</div>';
			t += '<div class="col-lg-5">';
			t += '<label>Koordinat E</label>';
			t += '<div class="row">';
			t += '<div class="col-lg-4" style="padding-right: 0;">';
			t += '<input type="text" name="e_deg[]" class="form-control input-sm" value="'+e[0]+'" required="required" placeholder="00&deg;">';
			t += '</div>';
			t += '<div class="col-lg-4" style="padding-right: 0;">';
			t += '<input type="text" name="e_squo[]" class="form-control input-sm" value="'+e[1]+'" required="required" placeholder="00&#39;">';
			t += '</div>';
			t += '<div class="col-lg-4" style="padding-right: 0;">';
			t += '<input type="text" name="e_quo[]" class="form-control input-sm" value="'+e[2]+'" required="required" placeholder="00&quot;">';
			t += '</div>';
			t += '</div>';
			t += '</div>';
			t += '<div class="col-lg-2" style="padding-left: 9px;">';
			t += '<label>&nbsp;</label>';
			t += '<div class="text-right">';
			t += '<div class="btn-group" style="padding-right: 0;">';
			t += '<button type="button" class="btn btn-sm btn-success" data-duplicate-add="koordinat"><i class="fa fa-plus-circle"></i></button>';
			t += '<button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="koordinat"><i class="fa fa-times-circle"></i></button>';
			t += '</div>';
			t += '</div>';
			t += '</div>';
			t += '</div>';
		});
		
		$('#' + target).html(t);
	}

	$.fn.clear_koor = function(form_id, active, target) {
		// # clear all form
    	$('#'+form_id+' form')[0].reset();
		$('#'+form_id+' form input[type=hidden]').val('');
		$('#'+form_id+' form').find('input:text, input:password, select, textarea').val('');
		$('#'+form_id+' form').find('input:radio, input:checkbox').prop('checked', false);

		if(active) {
			$('#'+target).html('');
		}
	}

	$.fn.add_simbol = function(a, b) {

        if(b.indexOf('-') != -1) {        	
            var temp = b.split('-');
            if(parseFloat(a) > parseFloat(temp[0]) && parseFloat(a) < parseFloat(temp[1])) return a;
            if(parseFloat(a) < parseFloat(temp[0])) return '< '+a;
            if(parseFloat(a) > parseFloat(temp[1])) return '> '+a;            
        }

        if(parseFloat(a) != parseFloat(b)) {
        	if(parseFloat(a) > parseFloat(b)){ return '> ' + a; }
        }

        return a;

    }
	
	$.fn.jenis_limbah_awal_template = function(target, data) {

		var t = '';
		$.each(data, function(i, v) {	

			var koor = v.split('/');
			var e = koor[0].split(':');
			
			t += '<div class="form-group row" data-duplicate="koordinat">   ';                     
			t += '	<div class="col-lg-5">';
			t += '		<label>Jenis Limbah</label>';
			t += '		<div class="row">  ';
			t += '			<div class="col-lg-12">';
			t += '				<select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm" required="required">';
			t += '					<?php foreach($data_acuan as $uk): ?>';
			t += '						<option value="'+e[0]+'">'+e[0]+'</option>';
			t += '					<?php endforeach;?>         ';                    
			t += '				</select>';
			t += '			</div>';
			t += '		</div>';
			t += '	</div>';
			t += '	<div class="col-lg-5">';
			t += '		<label>Jumlah (Ton)</label>';
			t += '		<div class="row">';
			t += '			 <div class="col-lg-12">';
			t += '				<input type="text" name="jml_ton[]" class="form-control input-sm" value="'+e[1]+'" required="required">  ';
			t += '			</div>';
			t += '		</div>';
			t += '	</div>';
			t += '	<div class="col-lg-2" style="padding-left: 9px;">';
			t += '		<label>&nbsp;</label>';
			t += '		<div class="text-right">';
			t += '			<div class="btn-group" style="padding-right: 0;">';
			t += '				<button type="button" class="btn btn-sm btn-success" data-duplicate-add="koordinat"><i class="fa fa-plus-circle"></i></button>';
			t += '				<button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="koordinat"><i class="fa fa-times-circle"></i></button>';
			t += '			</div>';
			t += '		</div>';
			t += '	</div>';
			t += '</div>';
		});
		
		$('#' + target).html(t);
	}
	
	$.fn.jenis_limbah_tps_template = function(target, data) {

		var t = '';
		$.each(data, function(i, v) {	

			var koor = v.split('/');
			var e = koor[0].split(':');
			
			t += '<div class="form-group row" data-duplicate="koordinat" data-duplicate-min="1">   ';                     
			t += '	<div class="col-lg-10">';
			t += '		<div class="row">  ';
			t += '			<div class="col-lg-12">';
			t += '				<select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm" required="required">';
			t += '					<?php foreach($data_acuan as $uk): ?>';
			t += '						<option value="'+e[0]+'">'+e[0]+'</option>';
			t += '					<?php endforeach;?>         ';                    
			t += '				</select>';
			t += '			</div>';
			t += '		</div>';
			t += '	</div>';
			t += '	<div class="col-lg-2" style="padding-left: 9px;">';
			t += '		<div class="text-right">';
			t += '			<div class="btn-group" style="padding-right: 0;">';
			t += '				<button type="button" class="btn btn-sm btn-success" data-duplicate-add="koordinat"><i class="fa fa-plus-circle"></i></button>';
			t += '				<button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="koordinat"><i class="fa fa-times-circle"></i></button>';
			t += '			</div>';
			t += '		</div>';
			t += '	</div>';
			t += '</div>';
		});
		
		$('#' + target).html(t);
	}	

	$.fn.kapasitas = function(target, data) {		
		var t = '';
		$.each(data, function(i, v) {			
			if (v != '') {
				t += '<div class="input-group input-group-sm" data-duplicate="kapasitas"> ';
				t += '<span class="input-group-addon" style="padding: 0 5px 0 5px;"> ';
				t += '<button type="button" class="btn btn-xs btn-success" data-duplicate-add="kapasitas"><i class="fa fa-plus-circle"></i></button> ';
				t += '<button type="button" class="btn btn-xs btn-danger" data-duplicate-remove="kapasitas"><i class="fa fa-times-circle"></i></button> ';
				t += '</span> ';
				t += '<input type="text" name="kapasitas_produksi[]" id="kapasitas_produksi" class="form-control" value="'+v+'" style="width: 100%;" /> ';
				t += '<span class="input-group-addon">Ton/Bulan</span> ';
				t += '</div> ';
			}
		});

		$('#' + target).html(t);
	}
});
