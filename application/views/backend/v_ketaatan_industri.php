<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">LAPORAN KETAATAN INDUSTRI&nbsp;&nbsp;<small>menyediakan laporan mengenai ketaatan masing - masing industri</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Rekapitulas</li>
                <li class="active">Laporan Ketaatan Industri</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6"> 
                        <div class="input-group input-group-sm">
							Tahun Ketaatan Industri :
                            <select id="year">
								<option value="none">-- Pilih Tahun --</option>
                                <option>2015</option>
								<option>2014</option>
								<option>2013</option>
								<option>2012</option>
								<option>2011</option>
								<option>2010</option>
							</select>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">                          
                            <tr>
                                <th width="5%" rowspan="2">#</th>
                                <th rowspan="2">Nama Usaha/Kegiatan</th>
                                <!-- <th width="20%" rowspan="2">Jenis Usaha/Kegiatan</th> -->
								<th colspan="2" style="text-align:center;">Air</th>
                                <th colspan="2" style="text-align:center;">Udara</th>
								<th colspan="2" style="text-align:center;">Padat & B3</th>
								<th rowspan="2" style="text-align:center;">Akumulasi Ketaatan</th>
								<th rowspan="2" style="text-align:center;">Keterangan</th>    
                                <th rowspan="2" style="text-align:center;">Surat Apresiasi</th>                             
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">0</span> dari <span id="nums">0</span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
    
    var base_url = '<?php echo site_url(); ?>';

    var Document = {
        param: {
            dataperpage: 10,
            year: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/ketaatan_industri/get_rekap"); ?>',
        search: function() {
            this.param.year = $('#year').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                beforeSend: function() { $('#loading').show(); },
                complete: function() { $('#loading').hide(); },
                success: function(result) {
                    console.log(result);
                    $('#pagination').html(result.pagination);
                    $('#start').text(result.total);                 
                    $('#nums').text(result.total);
                    Document.param.numpage = result.numpage;

                    var h = '<tr>';
                        h += '<td width="5%"></td>';
                        h += '<td></td>';
                        // h += '<td></td>';
						h += '<td style="text-align:center;"><b>Adm</b></td>';
                        h += '<td style="text-align:center;"><b>Teknis</b></td>';
						h += '<td style="text-align:center;"><b>Adm</b></td>';
                        h += '<td style="text-align:center;"><b>Teknis</b></td>';
						h += '<td style="text-align:center;"><b>Adm</b></td>';
                        h += '<td style="text-align:center;"><b>Teknis</b></td>';
						h += '<td></td>';
						h += '<td></td>';
                        h += '<td></td>';
                        h += '</tr>';
                    var ind = {}, no = Document.param.curpage * Document.param.dataperpage;
                    
                    if (result.rekap.length < 1) {
                        h += '<tr><td colspan=\'12\'>Data Tidak Ditemukan</td></tr>';
                    } else {
                        for (var i = 0; i < result.rekap.length; i++) {
                            ind = result.rekap[i];

                            h += '<tr>';
                            h += '<td class="text-center">'+(no+=1)+'</td>';
                            h += '<td><b>'+ind.nama_industri+'</b><br/>';
                            h += '<small>' + ind.alamat + '</small><br/>';
                            h += '<small>Kec. ' + ind.kecamatan + ', Kel. '+ ind.kelurahan +'</small><br/>';
                            h += '</td>';
                            
                            // h += '<td>Usaha/Kegiatan: '+ ind.usaha_kegiatan +'<br/>';
                            // h += 'Jenis/Tipe/Kelas: '+ ((ind.jenis_industri != null ) ? ind.jenis_industri : '- ');
                            // h += '</td>';
                            h += '<td style="text-align:center;">'+ ind.airadm +'</td>';
                            h += '<td style="text-align:center;">'+ ind.airteknis +'</td>';
                            h += '<td style="text-align:center;">'+ ind.udaraadm +'</td>';
                            h += '<td style="text-align:center;">'+ ind.udarateknis +'</td>';
                            h += '<td style="text-align:center;">'+ ind.b3adm +'</td>';
                            h += '<td style="text-align:center;">'+ ind.b3teknis +'</td>';
                            h += '<td style="text-align:center;">'+ ind.akumulasi +'</td>';
                            
                            if (ind.keterangan == ' - ') {
                                h += '<td style="text-align:center;">'+ ind.keterangan +'</td>';
                            } else {
                                h += '<td>'+ ind.keterangan +'</td>';
                            }

                            if (ind.akumulasi == 'Taat') {
                                h += '<td style="text-align:center;">';
                                h += '<form action="'+ base_url + 'backend/ketaatan_industri/generate_apresiasi" method="post">';
                                h += '<input type="hidden" name="id_industri" value="'+ ind.id_industri +'" />';
                                h += '<input type="hidden" name="year" value="'+ Document.param.year +'" />';
                                h += '<button class="btn btn-xs btn-warning" type="submit"><i class="fa fa-download"></i>&nbsp;Unduh</button>';
                                h += '</form>';
                                h += '</td>';
                            } else {
                                h += '<td style="text-align:center;"> - </td>';
                            }

                            h += '<tr>'
                        }
                    }

                    $('#document-data').html(h); 
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        },
        start: function() {
        	var h = '<tr>';
	            h += '<td width="5%"></td>';
	            h += '<td></td>';
	            // h += '<td></td>';
				h += '<td style="text-align:center;"><b>Adm</b></td>';
	            h += '<td style="text-align:center;"><b>Teknis</b></td>';
				h += '<td style="text-align:center;"><b>Adm</b></td>';
	            h += '<td style="text-align:center;"><b>Teknis</b></td>';
				h += '<td style="text-align:center;"><b>Adm</b></td>';
	            h += '<td style="text-align:center;"><b>Teknis</b></td>';
				h += '<td></td>';
				h += '<td></td>';
                h += '<td></td>';
	            h += '</tr>';
                h += '<tr><td colspan=\'12\'>Data Tidak Ditemukan</td></tr>';
        	$('#document-data').html(h); 
        }
    } 

    $(document).ready(function () {
        
        $('#year').change(function() {
        	if ($('#year').val()!= 'none') {
            	Document.search();
        	};
        });

		Document.start();

        window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

    });
</script>