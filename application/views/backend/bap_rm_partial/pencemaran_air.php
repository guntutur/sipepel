<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 20/12/16
 * Time: 21:45
 */
?>

<div class="tab-pane" id="pencemaran_air">
    <h4 class="page-tab">Pengendalian Pencemaran Air</h4>
    <fieldset>
        <div class="form-group">
            <label>a. Sumber Air dan Penggunaan</label>
            <!-- </div> -->
            <div class="form-group">
                <div class="form-group col-lg-6">
                    <label>Air Tanah</label>
                    <input type="text" name="air_tanah" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_tanah : ""; ?>"/>
                    <div class="radio">
                        <label>Perizinan    : </label>
                        <label>
                            <input type="radio" name="izin_air_tanah" value="ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_tanah_izin==1) ? 'checked' : '') : ""?>>Ada
                        </label>
                        <label>
                            <input type="radio" name="izin_air_tanah" value="tidak ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_tanah_izin==0) ? 'checked' : ''): ""?>>Tidak Ada
                        </label>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label>Air Permukaan</label>
                    <input type="text" name="air_permukaan" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_permukaan : ""; ?>"/>
                    <div class="radio">
                        <label>Perizinan    : </label>
                        <label>
                            <input type="radio" name="izin_air_permukaan" value="ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_permukaan_izin==1) ? 'checked' : '') : ""?>>Ada
                        </label>
                        <label>
                            <input type="radio" name="izin_air_permukaan" value="tidak ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_permukaan_izin==1) ? 'checked' : '') : ""?>>Tidak Ada
                        </label>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <label>Air PDAM</label>
                    <input type="text" name="air_pdam" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_pdam : ""; ?>"/>
                </div>
                <div class="form-group col-lg-6">
                    <label>Air Lain-lain</label>
                    <input type="text" name="air_lain" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_lain : ""; ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-lg-6">
                <label>b. Sumber Air Limbah</label>
                <!-- <select name="limb_sumber" id="" class="form-control input-sm"> -->
                <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                    <!-- <option value=''></option> -->
                    <?php
                    if (isset($pencemaran_air)) {$arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);};
                    foreach ($limb_sumber as $l) {
                        echo "<option value='$l->ket' ";
                        if (isset($pencemaran_air)) {
                            for ($i = 0; $i < count($arr_limb_sumber); $i++) {

                                if ($arr_limb_sumber[$i] == $l->ket) {
                                    echo 'selected';
                                }
                            }
                        }
                        echo ">$l->ket</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label>c. Badan Air Penerima</label>
                <select name="bdn_terima" id="" class="form-control input-sm">
                    <option value=''></option>
                    <?php
                    foreach ($bdn_terima as $l) {
                        echo "<option value='$l->ket' ";
                        if (isset($pencemaran_air)) {
                            if ($pencemaran_air->bdn_terima == $l->ket) {
                                echo 'selected';
                            }
                        }
                        echo ">$l->ket</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group ">
            <div class="form-group ">
                <label>e. Sarana Pengolahan Air Limbah</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="sarolim" id="sarolim1" value="ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->sarana_olah_limbah==1) ? 'checked' : '') : ""?>>Ada
                    </label>
                    <label>
                        <input type="radio" name="sarolim" id="sarolim2" value="tidak ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->sarana_olah_limbah==0) ? 'checked' : '') : ""?>>Tidak Ada
                    </label>
                </div>
            </div>
            <div class="form-group " id="sarolim" name="sarolim" style="display:none">
                <div class="form-group">
                    <label>Jenis Sarana</label>
                    <input type="text" name="sarana_jenis" id="sarana_jenis" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_jenis : ""; ?>"/>
                </div>
                <div class="form-group ">
                    <label>Kapasitas</label>
                    <input type="text" name="kapasitas" id="kapasitas" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_kapasitas : ""; ?>"/>
                </div>
                <div class="form-group ">
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Koordinat Outlet S</label>
                            <div class="form-group status">
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_koord_derajat_s : ""?>" name="xkoord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4" style="padding: 0; margin:0;">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_koord_jam_s : ""?>" name="xkoord_outlet_jam_s" id="jam_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                            <span class="input-group-addon">'</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_koord_menit_s : ""?>" name="xkoord_outlet_menit_s" id="menit_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                            <span class="input-group-addon">"</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label>Koordinat Outlet E</label>
                            <div class="form-group status">
                                <div class="row">
                                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_koord_derajat_e : ""?>" name="xkoord_outlet_derajat_e" id="derajat_e" class="form-control input-sm has-feedback" placeholder="0"  />
                                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4" style="padding: 0; margin:0;">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_koord_jam_e : ""?>" name="xkoord_outlet_jam_e" id="jam_e" class="form-control input-sm has-feedback" placeholder="0"  />
                                            <span class="input-group-addon">'</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                        <div class="input-group input-group-sm">
                                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->sarana_koord_menit_e : ""?>" name="xkoord_outlet_menit_e" id="menit_e" class="form-control input-sm has-feedback" placeholder="0"  />
                                            <span class="input-group-addon">"</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="row">
        <div class="col-sm-4 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Data Umum</a>
        </div>
        <div class="col-sm-4 text-center">
            <input id="save-pencemaran_air" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Air"/>
        </div>
        <div class="col-sm-4 text-right">
            <a class="btn btn-warning NextStep">Pencemaran Udara<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>
