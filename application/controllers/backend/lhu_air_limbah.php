<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class lhu_air_limbah extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model
        $this->load->model('m_lhu_air_limbah');
        $this->load->model('m_data_master');
        $this->load->model('m_laboratory');
		$this->load->model('m_lhu_parameter');
        $this->load->model('m_detail_lhu');
        $this->load->model('m_industry');
		
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');
        
        $data['labs'] = $this->m_laboratory->get_dropdown();
        $data['industri'] = $this->m_industry->get_dropdown();
        $data['proses_produksi'] = $this->m_data_master->get_detail_data_master(array('jenis_data_master' => 'proses_produksi'));

    	$this->load->view('include/header');
        $this->load->view('backend/v_lhu_air_limbah', $data);
        $this->load->view('include/footer');
    }

    public function get_list() {
        # get parameter pagination
        $input = array('dataperpage', 'query', 'curpage');
        foreach ($input as $val)
            $$val = $this->input->post($val); 
        
        $where = "`al`.`jenis_lhu` = 'LHU Air Limbah' AND (`i`.`nama_industri` LIKE '%".$query."%' OR `l`.`nama_lab` LIKE '%".$query."%' OR `al`.`no_lhu` LIKE '%".$query."%' OR `al`.`laporan_bulan_tahun` LIKE '%".$query."%')";

        $total = $this->m_lhu_air_limbah->get_num_rows($where);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $types = $this->m_lhu_air_limbah->get_all($where, $dataperpage, $start);
        $data = array(
            'data' => $types,
            'pagination' => '',
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

        echo json_encode($data);
        exit();
        
    }

    public function register() {       

        date_default_timezone_set("Asia/Jakarta");
        $tgl_pembuatan = date("Y-m-d H:i:s");

        $o = $this->m_lhu_air_limbah->is_duplicate($this->input->post('industri'), $this->input->post('lap_bln'));    
        if($o) {
            # parsing derajat
            $koordinat_ipal = $this->functions->parsing_koordinat($this->input->post());
            
            $param = array(                
                    'id_industri' => $this->input->post('industri'),
                    'id_lab' => $this->input->post('labs'),                
                    'no_lhu' => $this->input->post('no_lhu'),
                    'jenis_lhu' => 'LHU Air Limbah',
                    'jenis_baku_mutu' => $this->input->post('jenis_bakumutu'),
                    'laporan_bulan_tahun' => $this->input->post('lap_bln'),
                    'jenis_sample' => serialize($this->input->post('jenis_sample')),
                    'tgl_pengambilan_sample' => date("Y-m-d", strtotime($this->input->post('tgl_pengambilan'))),
                    'waktu_pengambilan_sample' => date("G:i:s", strtotime($this->input->post('waktu_pengambilan'))),
                    'proses_produksi' => serialize($this->input->post('proses_produksi')),
                    'kapasitas_produksi' => serialize($this->input->post('kapasitas_produksi')),
                    'koord_outlet_ipal' => $koordinat_ipal,                
                    'tgl_pembuatan' => $tgl_pembuatan,
                    'tgl_update' => $tgl_pembuatan,
                    'dibuat_oleh' =>$this->tank_auth->get_personname(),
                    'diupdate_oleh' => $this->tank_auth->get_personname()
                );
           
            if($this->m_lhu_air_limbah->insert($param))
                $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
            else
                $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
        }else{
            $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia.<br/>Silahkan ulangi proses input data.'));
        }

        redirect('backend/lhu_air_limbah');

    }

    public function get() {   

        $data = $this->m_lhu_air_limbah->get_detail_by_id($this->input->post('id_laporan_hasil_uji'));

        # koordinat
        $koordinat_ipal = ($data->koord_outlet_ipal) ? unserialize($data->koord_outlet_ipal) : array();        
        $data->koord_outlet_ipal = $koordinat_ipal;

        # kapasitas_produksi
        if($data->kapasitas_produksi) {
            $kapasitas = ($data->kapasitas_produksi) ? unserialize($data->kapasitas_produksi) : array();
            $data->kapasitas_produksi = $kapasitas;
        }

        # proses_produksi
        if($data->proses_produksi) {
            $proses = ($data->proses_produksi) ? unserialize($data->proses_produksi) : array();
            $data->proses_produksi = $proses;
        }

        # proses_produksi
        if($data->jenis_sample) {
            $jenis_sample = ($data->jenis_sample) ? unserialize($data->jenis_sample) : array();
            $data->jenis_sample = $jenis_sample;
        }

        echo json_encode($data);
        exit();
    }

    public function edit() {
        
        date_default_timezone_set("Asia/Jakarta");
        $tgl_update = date("Y-m-d H:i:s:");

        // $o = $this->m_lhu_air_limbah->is_duplicate($this->input->post('industri'), $this->input->post('lap_bln'));    
        // if($o) {
             # parsing derajat
        $koordinat_ipal = $this->functions->parsing_koordinat($this->input->post());

        $param = array(                
                'id_industri' => $this->input->post('industri'),
                'id_lab' => $this->input->post('labs'),                
                'no_lhu' => $this->input->post('no_lhu'),
                'jenis_lhu' => 'LHU Air Limbah',
                'jenis_baku_mutu' => $this->input->post('jenis_bakumutu'),
                'laporan_bulan_tahun' => $this->input->post('lap_bln'),
                'jenis_sample' => serialize($this->input->post('jenis_sample')),
                'tgl_pengambilan_sample' => date("Y-m-d", strtotime($this->input->post('tgl_pengambilan'))),
                'waktu_pengambilan_sample' => date("G:i:s", strtotime($this->input->post('waktu_pengambilan'))),
                'proses_produksi' => serialize($this->input->post('proses_produksi')),
                'kapasitas_produksi' => serialize($this->input->post('kapasitas_produksi')),             
                'koord_outlet_ipal' => $koordinat_ipal,                
                'tgl_update' => $tgl_update,                
                'diupdate_oleh' => $this->tank_auth->get_personname()
            );
        
        if($this->m_lhu_air_limbah->change_lhu($param, $this->input->post('id')))
            $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
        else
            $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));        
        // }else{
            // $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia.<br/>Silahkan ulangi proses input data.'));
        // }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete() {
        $this->m_lhu_air_limbah->delete_lhu($this->input->post("id"));        
        $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function detail() {
        $id = $this->uri->segment(4);

        $detail = $this->m_detail_lhu->get_detail_by_id($id);
        $tgl_laporan = explode('.', $detail->laporan_bulan_tahun);

        $data['action'] = ($detail->jenis_baku_mutu == 'Baku Mutu Izin') ? 'register_izin' : 'register_detail';
        $data['title'] = 'DETAIL LHU AIR LIMBAH '.$detail->nama_industri." ( ".$detail->laporan_bulan_tahun." )";

        $this->load->view('include/header');
        $this->load->view('backend/v_detail_lhu', $data);
        $this->load->view('include/footer');
    }
}
?>