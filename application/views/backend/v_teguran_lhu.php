<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Teguran Laporan Hasil Uji&nbsp;&nbsp;<small>laporan teguran seluruh data laporan hasil uji</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Teguran</li>
                <li class="active">Teguran LHU</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

               <div class="row">                    
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />                            
                        </div>
                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <br/>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>                                
                                <th width="5%">No.</th>
                                <th>Nama Usaha/Kegiatan</th>                     
                                <th>Laporan Bulan</th>
                                <th>Parameter Tidak Memenuhi Baku Mutu</th>  
                                <th>Surat Teguran</th>                                                         
                                <?php if($this->tank_auth->is_manager() or $this->tank_auth->is_admin()) { ?>
                                <th>Status LHU</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

var Document = {
    param: {
        dataperpage: 10,
        query: '',
        curpage: 0,
        numpage: 0
    },
    url: '<?php echo base_url("backend/teguran_lhu/get_list"); ?>',
    search: function() {
        this.param.query = $('#query').val();
        this.param.curpage = 0;
        this.load_data();
        return false;
    },
    search_field: function(e) {

    },
    set_page: function(n) {
        this.param.curpage = n;
        this.load_data();
        return false;
    },
    prev_page: function() {
        if(this.param.curpage > 0) {
            this.param.curpage--;
            this.load_data();
        }
        return false;
    },
    next_page: function() {
        if(this.param.curpage < this.param.numpage) {
            this.param.curpage++;
            this.load_data();
        }
        return false;
    },
    load_data: function() {
        $.ajax({
            url: Document.url,
            type: 'POST',
            dataType: 'json',
            data: $.param(Document.param),
            success: function(d) {
                console.log(d);
                $('#pagination').html(d.pagination);
                if(d.total < 10){ $('#start').text(d.total); }                    
                $('#nums').text(d.total);
                Document.param.numpage = d.numpage;
                var t = '', status = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                for (var i = 0; i < d.data.length; i++) {
                    dt = d.data[i];

                    var teguran_air = (dt.teguran_air) ? dt.teguran_air.split(';').join(', ') : '-';
                    var teguran_emisi = (dt.teguran_emisi) ? dt.teguran_emisi.split(';').join(', ') : '-';
                    // var teguran_ambien = (dt.teguran_ambien) ? dt.teguran_ambien.split(';').join(', ') : '-';

                    if(teguran_air == '-' && teguran_emisi == '-') { continue; }

                    t += '<tr>';
                    t += '<td>'+(no+=1)+'</td>';
                    t += '<td>'+dt.nama_industri+'</td>';

                    var tmp = dt.laporan_bulan_tahun.split(".");
                    t += '<td>'+$(this).get_month(parseInt(tmp[0]))+' '+ tmp[1] +'</td>';

                    // t += '<td>'+$(this).return_value(dt.laporan_bulan_tahun)+'</td>';   
                    t += '<td>';
                    t += '<label>LHU Air Limbah</label> <br/> ' + teguran_air + '<br/><br/>';
                    t += '<label>LHU Udara Emisi</label> <br/> ' + teguran_emisi + '<br/>';
                    // t += '<label>LHU Udara Ambien</label> <br/> ' + teguran_ambien + '<br/>';
                    t += '</td>';
                    t += '<td>';
                    t += '<div class="btn-group btn-group-xs">';
                    var id_lhu = $(this).return_value(dt.id_industri, true) + '-' + $(this).return_value(dt.id_air_limbah, true) + '-' + $(this).return_value(dt.id_udara_emisi, true);
                    t += '<a class="btn btn-warning" href="'+'<?php echo site_url("backend/teguran_lhu/generate_laporan/'+id_lhu+'"); ?>'+'" data-toggle="tooltip" data-placement="bottom" title="Unduh"><i class="fa fa-download"></i></a>';
                    t += '<button type="button" class="btn btn-success" onclick="$(this).upload(\''+id_lhu+'\')" data-toggle="tooltip" data-placement="bottom" title="Unggah"><i class="fa fa-upload"></i></button>';
                    t += '</div>';
                    t += '</td>';
                    t += '<td>'+$(this).status_lhu(dt.status_lhu)+'</td>';
                    <?php if($this->tank_auth->is_manager() or $this->tank_auth->is_admin()) { ?>                    
                    t += '<td><a onclick="$(this).tutup_lhu(\''+id_lhu+'\')" class="btn btn-xs btn-danger"><i class="fa fa-times-circle"></i>&nbsp;Tutup</a></td>';
                    <?php } ?>
                    t += '</tr>';
                }
                
                $('#document-data').html(t);
            }
        })
        .fail(function(e) {
            console.log(e);
        });            
    }
} 

$(document).ready(function () {  
    
    $.fn.search = function(e, v) {
        if(e.keyCode == 13 || v.value == '') {
            Document.search();                
        }
    }

    $.fn.tutup_lhu = function(id) {
        $.ajax({
            url: '<?php echo site_url("backend/teguran_lhu/tutup_teguran"); ?>',
            type: 'POST',            
            data: {id_lhu: id},
            success: function(data) {
                location.reload(); 
            }
        })  
        .fail(function() {
            console.log("error");
        });
        
    }

    $.fn.upload = function(id) {        
        $('input[name=id_lhu]').val(id);
        $('#mdl_konfirmasi').modal('show');
    }

    Document.search();
});
</script>

<!-- Modal -->
<div class="modal fade" id="mdl_konfirmasi" tabindex="-1" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="uploadLabel">Konfirmasi Teguran LHU</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/teguran_lhu/konfirmasi') ?>" method="post" id="form_add" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group status">                        
                        <label>Nama Pengirim/ Eksekutor</label>
                        <input type="text" title="Nama Pengirim/ Eksekutor" value="" class="form-control input-sm" id="nama_pengirim" name="nama_pengirim">                                    
                    </div>
                    <div class="row">                                                               
                        <div class="form-group col-lg-6">                        
                            <label>Via</label>
                            <input type="text" title="Via" value="" class="form-control input-sm" id="via" name="via">                                    
                        </div>
                        <div class="form-group  col-lg-6">
                            <label>Tanggal Kirim</label>
                            <div class="input-group input-group-sm">
                                <input type="text" name="tgl_pengambilan" class="form-control datepicker" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>   
                    </div>
                    <div class="form-group">
                        <label>Upload Berkas</label>
                        <div class="input-group input-group-sm">
                            <input type="text" readonly="" class="form-control">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    Browse… <input type="file" name="userfile" />
                                </span>
                            </span>
                        </div><!-- /.input-group -->
                    </div>                                                                                                  
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id_lhu" name="id_lhu" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>