<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_laboratory extends CI_Model {

	private $table_name			= 'laboratorium';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('*')->from($this->table_name);

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		// Produces: LIMIT 20, 10 (in MySQL. Other databases have slightly different syntax)
		$this->db->order_by('nama_lab', 'asc');
		$this->db->order_by('tgl_pembuatan', 'desc');	

		$query = $this->db->get();

		// $query = $this->db->query("select * from laboratorium u order by u.tgl_pembuatan desc");	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		$this->db->select('*')->from($this->table_name);

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		return $this->db->count_all_results();
	}

	function get_dropdown() {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("nama_lab", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_laboratory_by_id($id) {
		$this->db->where('id_lab', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_laboratory($id) {
		// $data = $this->get_laboratory_by_id($id);
		$this->db->where('id_lab', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_laboratory($data, $id) {

		$this->db->where("id_lab", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_laboratory($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate($data) {
        foreach ($data as $key => $value) {
            $this->db->where($key, $value);
        }        
        $this->db->from($this->table_name);        
        $res = $this->db->count_all_results();

        if($res > 0) return true;
        else return false;
    }
}
?>