<?php 

 // Document   : m_bap_kelola_b3.php 
// Last edited: December 7, 2014
//  Author     : guntutur@gmail.com 
//  Description: Model for BAP Pengelola Limbah B3 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_bap_plb3 extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * Get all BAP
	 *
	 * @return array
	 */
	function get_all() {
		$data = array();

		$query = $this->db->query("SELECT a.id_bap, c.nama_industri, b.compare_status, b.is_compared, b.bap_tgl, COUNT(d.id_bap) AS jml_teguran
									FROM bap_plb3 a
									LEFT JOIN bap b ON a.id_bap = b.id_bap
									LEFT JOIN history_item_teguran_bap d ON b.id_bap = d.id_bap
									INNER JOIN industri c ON b.id_industri = c.id_industri
									GROUP BY b.id_bap
									ORDER BY a.id_bap_plb3 ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function delete_item_ketaatan($id_bap) {
		$query = $this->db->query("DELETE FROM history_item_teguran_bap where id_bap = '$id_bap'");

		if ($query > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>