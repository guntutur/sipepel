<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Document   : m_bap.php 
// Created on : October 28, 2014 11:29 
// Author     : huda.jtk09@gmail.com 
// Description: Model for BAP and other relatives 

class m_bap extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * Insert to table and return the id
	 *
	 * @param	String, object
	 * @return	int
	 */
	function insert_getID($table_name, $data){
		$this->db->insert($table_name, $data);
		
		return $this->db->insert_id();
	}

	/**
	 * Insert to table and return the status
	 *
	 * @param	String, object
	 * @return	boolean
	 */
	function insert_getStat($table_name, $data){
		$this->db->insert($table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	/**
	 * Edit data
	 *
	 * @param	String, String, int, object
	 * @return	boolean
	 */
	function edit($table_name, $column_name, $id, $data) {
		$this->db->where($column_name, $id);

		$this->db->update($table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	/**
	 * Delete data
	 *
	 * @param	String, stripcslashes(str)ng, int
	 * @return	bool
	 */
	function delete($table_name, $column_name, $id) {
		$this->db->where($column_name, $id);
		
		$this->db->delete($table_name);
		
		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Get data by Id
	 * same as function below this, add join to get nama_industri for editing purpose
	 * @param	String, String, int	 
	 * @return	object
	 */
	function get_obj_by_id_edit($table_name, $column_name, $id) {

		$this->db->where($column_name, $id);
		$this->db->join('industri b', "a.id_industri = b.id_industri", 'inner');
		$query = $this->db->get($table_name.' a');
		
		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return NULL;
		}
	}

	
	/**
	 * Get data by Id
	 *
	 * @param	String, String, int	 
	 * @return	object
	 */
	function get_obj_by_id($table_name, $column_name, $id) {

		$this->db->where($column_name, $id);
		
		$query = $this->db->get($table_name);
		
		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return NULL;
		}
	}

	/**
	 * Get data by Id
	 *
	 * @param	String, String, int	 
	 * @return	Array of Obj
	 */
	function get_list_by_id($table_name, $column_name, $id) {
		$this->db->where($column_name, $id);
		
		$query = $this->db->get($table_name);
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	function get_petugas_bap($id_bap) {
		$data = array();

		$query = $this->db->query("SELECT b.nama_pegawai, b.nip, b.jabatan, b.id_pegawai
									FROM petugas_bap a
									LEFT JOIN pegawai b ON a.id_pegawai = b.id_pegawai
									WHERE a.id_bap = ".$id_bap."
									ORDER BY b.nama_pegawai ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * Delete data pencemaran
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_penc_air($id_bap) {
		$penc_air = $this->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

		$this->db->where("id_penc_air", $penc_air->id_penc_air);
		$this->db->delete("bahan_kimia_ipal");
		
		$this->db->where("id_bap", $id_bap);
		$this->db->delete("pencemaran_air");

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function delete_penc_udara($id_bap) {
		$penc_udara = $this->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

		# delete uji_ambien
		$this->db->where("id_penc_udara", $penc_udara->id_penc_udara);
		$this->db->delete("uji_ambien");
		# delete uji_emisi
		$this->db->where("id_penc_udara", $penc_udara->id_penc_udara);
		$this->db->delete("uji_emisi");
		# delete data_cerobong
		$this->db->where("id_penc_udara", $penc_udara->id_penc_udara);
		$this->db->delete("data_cerobong");
		# delete emisi_boiler
		$this->db->where("id_penc_udara", $penc_udara->id_penc_udara);
		$this->db->delete("emisi_boiler");
		
		$this->db->where("id_bap", $id_bap);
		$this->db->delete("pencemaran_udara");

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function delete_penc_pb3($id_bap) {
		$penc_pb3 = $this->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
		
		# delete jenis_limbah_b3
		$this->db->where("id_penc_pb3", $penc_pb3->id_penc_pb3);
		$this->db->delete("jenis_limbah_b3");

		#delete tps_b3
		$this->db->where("id_penc_pb3", $penc_pb3->id_penc_pb3);
		$this->db->delete("tps_b3");

		# delete pencemaran_pb3
		$this->db->where("id_bap", $id_bap);
		$this->db->delete("pencemaran_pb3");

		if ($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get parameter_bap
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_parameter_bap($where) {

		$this->db->select("a.id_parameter_bap, a.ket as nama_parameter, a.nilai_parameter_bap, a.dasar_hukum, a.status, a.id_parent as id_parent_1, b.id_parent as id_parent_2");
		$this->db->from("parameter_bap a");
		$this->db->join('parameter_bap b', 'a.id_parent = b.id_parameter_bap', 'LEFT');
		$this->db->where($where);
		$this->db->order_by('a.urutan', 'ASC');

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	/**
	 * huda: get data by Id and sort by request
	 *
	 * @param	String, String, int/String, String, String
	 * @return	Array of Obj
	 */
	function get_list_sort($table_name, $column_name, $id, $order_by, $direction) {
		$this->db->where($column_name, $id);
		$this->db->order_by($order_by, $direction);
		
		$query = $this->db->get($table_name);
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	// function to retrieve all data needed for bap notification
	// author : guntutur@gmail.com
	function get_notif_bap() {
		$data = array();

		$this->db->select("a.id_bap, b.badan_hukum as bdn_hukum, b.nama_industri as nama_industri, a.dibuat_oleh as maker, a.bap_tgl as tgl, count(e.id_bap) as total_error, sum(e.perbaikan=1)	 as perbaikan, a.conf_status as conf_status");
		$this->db->from("bap a");
		$this->db->join('industri b', 'a.id_industri = b.id_industri', 'LEFT');
		$this->db->join('history_item_teguran_bap e', 'a.id_bap = e.id_bap', 'INNER');
		$this->db->where("a.id_bap IN (SELECT DISTINCT e.id_bap
					FROM history_item_teguran_bap e
					GROUP BY e.id_bap)");
		$this->db->where("a.status != 1");
		$this->db->group_by('e.id_bap');
		$this->db->order_by('a.bap_tgl', 'DESC');

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	function count_notif_bap() {

		// $sum = $this->db->query("SELECT count(id_bap) as total_notif FROM bap where conf_status != 1")->row()->total_notif;
		$sum = $this->db->query("SELECT count(a.id_bap) as total_notif FROM bap a WHERE a.id_bap IN (SELECT DISTINCT e.id_bap FROM history_item_teguran_bap e GROUP BY e.id_bap) and a.status != 1 ORDER BY a.bap_tgl DESC")->row()->total_notif;
		return $sum;
	}

	function get_proses_produksi() {

		$this->db->select('*');
		$this->db->from('data_master_detail');
		$this->db->where('jenis_data_master', 'proses_produksi');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row->ket;
			}
			return $data;
		}

		return NULL;
	}

	/**
	 * Get all BAP
	 *	author : guntutur@gmail.com
	 * @return array
	 */
	function get_all_bap($where, $limit=0, $offset=0) {
		$data = array();

		$this->db->select('a.id_bap as id_bap, c.alamat as alamat, c.nama_industri as nama_industri, d.ket as jenis_usaha, a.bap_tgl as tgl, COUNT(b.id_bap) AS jml_teguran, sum(b.perbaikan=1) as teguran_diperbaiki');
		$this->db->from('bap a');
		$this->db->join('history_item_teguran_bap b', 'a.id_bap = b.id_bap', 'LEFT');
		$this->db->join('industri c', 'a.id_industri = c.id_industri', 'INNER');
		$this->db->join('usaha_kegiatan d', 'c.id_usaha_kegiatan = d.id_usaha_kegiatan', 'INNER');
		// $this->db->where('b.perbaikan != 1');

		if($where != '') {
			$this->db->where($where);
		}

		$this->db->where('a.status != 1');
		$this->db->group_by('a.id_bap');
		$this->db->order_by('a.bap_tgl', 'DESC');
		$this->db->limit($limit, $offset);

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * Get count all bap, same as above, differ in return
	 *	author : guntutur@gmail.com
	 * @return array
	 */
	function get_all_bap_num_row($where) {
		$this->db->select('a.id_bap as id_bap, c.nama_industri as nama_industri, d.ket as jenis_bap, a.bap_tgl as tgl');
		$this->db->from('bap a');
		$this->db->join('history_item_teguran_bap b', 'a.id_bap = b.id_bap', 'LEFT');
		$this->db->join('industri c', 'a.id_industri = c.id_industri', 'INNER');
		$this->db->join('usaha_kegiatan d', 'c.id_usaha_kegiatan = d.id_usaha_kegiatan', 'INNER');

		if($where != '') {
			$this->db->where($where);
		}

		$this->db->group_by('a.id_bap');
		$this->db->order_by('a.id_bap', 'ASC');

		$query = $this->db->get();

		return $query->num_rows();

		// $query = $this->db->query("SELECT a.id_bap as id_bap, c.nama_industri as nama_industri, d.ket as jenis_bap, 
		// a.bap_tgl as tgl, COUNT(b.id_bap) AS jml_teguran
		// FROM bap a
		// LEFT JOIN history_item_teguran_bap b ON a.id_bap = b.id_bap
		// INNER JOIN parameter_bap e on b.id_parent_1 = e.id_parent
		// INNER JOIN industri c ON a.id_industri = c.id_industri
		// INNER JOIN jenis_bap d ON d.id_jenis_bap = e.id_jenis_bap
		// GROUP BY b.id_bap
		// ORDER BY a.id_bap ASC");

		// return $query->num_rows();
	}

	function perbaiki_item_teguran_bap($id_bap, $check) {

		$query = $this->get_list_by_id('history_item_teguran_bap', 'id_bap', $id_bap);
		
		$i=0;
		foreach ($query as $key) {
			if($check[$i]!=0){
				$data = array(
	               'perbaikan' => 1
	            );
				$this->db->where('id_history_teguran_bap', $key->id_history_teguran_bap);
				$this->db->update('history_item_teguran_bap', $data);
			} else {
				$data = array(
	               'perbaikan' => 0
	            );
				$this->db->where('id_history_teguran_bap', $key->id_history_teguran_bap);
				$this->db->update('history_item_teguran_bap', $data);
			}
			$i++;
		}
		return NULL;
	}

	function get_header_raport_bap($id_bap) {
		$this->db->select("a.id_bap, a.id_industri, a.dok_lingk, c.badan_hukum, c.nama_industri, c.alamat, d.ket as jenis_industri, e.ket as usaha_kegiatan, c.pimpinan, c.tlp, c.fax, c.email");
		$this->db->from("bap a");
		$this->db->join('industri c', 'a.id_industri = c.id_industri', 'LEFT');
		$this->db->join('jenis_industri d', 'c.id_jenis_industri = d.id_jenis_industri', 'INNER');
		$this->db->join('usaha_kegiatan e', 'd.id_usaha_kegiatan = e.id_usaha_kegiatan', 'INNER');
		$this->db->where("a.id_bap", $id_bap);
		$this->db->order_by('YEAR(a.bap_tgl)', 'DESC');

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return NULL;
		}
	}

	// originally taken from m_laporan_bap->get_usaha_teguran($bap_type, $limit=0, $offset=0)
	// modified by guntutur@gmail.com
	function get_teguran_bap_excel() {
		$data = array();

			$query = 'SELECT a.id_bap, c.nama_industri as nama_industri, d.ket as jenis_industri, c.alamat as alamat, a.bap_tgl as tgl, a.conf_pengirim as conf_pengirim, a.conf_status as conf_status, a.status as status
						FROM bap a 
						LEFT JOIN industri c ON a.id_industri = c.id_industri
						INNER JOIN jenis_industri d ON c.id_jenis_industri = d.id_jenis_industri
						WHERE a.id_bap IN (SELECT DISTINCT e.id_bap
											FROM history_item_teguran_bap e
											GROUP BY e.id_bap)
						ORDER BY d.ket, a.id_bap ASC';
		
		$results = $this->db->query($query);

		if ($results->num_rows() > 0) {
			foreach ($results->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	/**
	 * Get all BAP
	 *	author : guntutur@gmail.com
	 * @return array
	 */
	function get_teguran_perbaikan_bap_excel() {
		$data = array();

		$this->db->select('a.id_bap as id_bap, c.alamat as alamat, c.nama_industri as nama_industri, d.ket as jenis_usaha, a.bap_tgl as tgl, COUNT(b.id_bap) AS jml_teguran');
		$this->db->from('bap a');
		$this->db->join('history_item_teguran_bap b', 'a.id_bap = b.id_bap', 'LEFT');
		$this->db->join('industri c', 'a.id_industri = c.id_industri', 'INNER');
		$this->db->join('usaha_kegiatan d', 'c.id_usaha_kegiatan = d.id_usaha_kegiatan', 'INNER');
		$this->db->where('b.perbaikan != 1');

		$this->db->group_by('b.id_bap');
		$this->db->order_by('a.id_bap', 'ASC');

		$query = $this->db->get();
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function select_table($table){
		$data = array();

		$this->db->select('*')->from($table);
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;	
	}

    function select_table_array($table, $col_id, $id, $list = FALSE, $counter=0){
        $data = array();

        $this->db->select('*')->from($table)->where($col_id, $id);
        $query = $this->db->get();

        if ($list) {
            if ($query->num_rows() > 0) {
                if ($counter!=0) {
                    for ($i=0; $i<$counter; $i++) {
                        $data[] = $query->result_array()[$i];
                    }
                } else {
                    foreach ($query->result_array() as $row) {
                        $data[] = $row;
                    }
                }
            }
        }  else {
            if ($query->num_rows() == 1) {
                $data = $query->row_array();
            }
        }

        return $data;
    }

	function collectStatus($id_bap, $bap_type) {
        $data['status'] = false;

        $check = $this->db->query("SELECT compare_status from bap WHERE id_bap = $id_bap")->row()->compare_status;

        if ($check) {
            $data['status'] = true;
        } else {
            $data['bap'] = $this->db->query("select * FROM bap WHERE id_bap = $id_bap")->row();
            $data[$bap_type] = $this->db->query("select * FROM $bap_type WHERE id_bap = $id_bap")->row();
            $data['pencemaran_air'] = $this->db->query("select * FROM pencemaran_air WHERE id_bap = $id_bap")->row();
            $data['pencemaran_udara'] = $this->db->query("select * FROM pencemaran_udara WHERE id_bap = $id_bap")->row();
            $data['pencemaran_pb3'] = $this->db->query("select * FROM pencemaran_pb3 WHERE id_bap = $id_bap")->row();
            /*always check if data is comparable or not*/
            /*this is the only gateway to update the status anyway beside from http post*/
            $this->updateCompareStatus($id_bap, $data);
        }
        return $data;
    }

    function updateCompareStatus($id_bap, $data) {
        if(!empty($data['pencemaran_air']) AND !empty($data['pencemaran_udara']) AND !empty($data['pencemaran_pb3'])) {
            $this->db->where("id_bap", $id_bap);
            $this->db->update("bap", array("compare_status" => 1));

            $data['status'] = true;
        }
    }

    function updateIsCompared($id_bap) {
        $this->db->where("id_bap", $id_bap);
        $this->db->update("bap", array("is_compared" => 1));

        $data['status'] = true;
    }

    /*what te fvkc is this for?*/
    /*function collectStatusEdit($id_bap) {
        $data['status'] = false;

        $check = $this->db->query("SELECT compare_status from bap WHERE id_bap = $id_bap")->row()->compare_status;

        if ($check) {
            $data['status'] = true;
        } else {
            $data['bap'] = $this->db->query("select * FROM bap WHERE id_bap = $id_bap")->row();
            $data['bap_agro'] = $this->db->query("select * FROM bap_agro WHERE id_bap = $id_bap")->row();
            $data['pencemaran_air'] = $this->db->query("select * FROM pencemaran_air WHERE id_bap = $id_bap")->row();
            $data['pencemaran_udara'] = $this->db->query("select * FROM pencemaran_udara WHERE id_bap = $id_bap")->row();
            $data['pencemaran_pb3'] = $this->db->query("select * FROM pencemaran_pb3 WHERE id_bap = $id_bap")->row();
        }

        return $data;
    }*/
}