<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_izin_tps extends CI_Model {

	private $table_name			= 'izin_tps_limbah';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('al.*, i.nama_industri, i.badan_hukum, j.ket as usaha_kegiatan, k.ket as nama_jenis_industri, (select count(*) from tps_limbah a where a.id_izin_tps = al.id_izin_tps) as jumlah');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_industri = i.id_industri', 'left');
		$this->db->join('usaha_kegiatan as j', 'i.id_usaha_kegiatan = j.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri as k', 'i.id_jenis_industri = k.id_jenis_industri', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("i.badan_hukum", "asc"); 
		$this->db->order_by("i.nama_industri", "asc"); 
		$this->db->order_by("al.tgl_pembuatan", "desc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select('al.*, i.nama_industri,i.badan_hukum, j.ket as usaha_kegiatan, k.ket as jenis_industri,(select count(*) from tps_limbah a where a.id_izin_tps = al.id_izin_tps) as jumlah');
		$this->db->from($this->table_name .' as al');
		$this->db->join('industri as i', 'al.id_industri = i.id_industri', 'left');
		$this->db->join('usaha_kegiatan as j', 'i.id_usaha_kegiatan = j.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri as k', 'i.id_jenis_industri = k.id_jenis_industri', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_by_id($id) {
		$this->db->where('id_izin_tps', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete($id) {
		$data = $this->get_by_id($id);
		$this->db->where('id_izin_tps', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change($data, $id) {

		$this->db->where("id_izin_tps", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	
	function get_izin_tps_detail_by_id($id) {
		$whr1 = 'p.id_izin_tps = "'.$id.'"';
		$query = $this->db->query('select p.*, q.nama_industri, q.badan_hukum 
									from izin_tps_limbah as p 
									left join (
										select * from industri
									) q on p.id_industri = q.id_industri
									where '.$whr1.'  AND 1=1' );
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}	
	
	function get_detail_all($where='', $limit=0, $offset=0) {

		$data = array();

		$this->db->select('*');
		$this->db->from('tps_limbah');		

		if($where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		 
		
		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$jenis_limbah = ($row->jenis_limbah_b3) ? unserialize($row->jenis_limbah_b3) : array();        
				$row->jenis_limbah_b3 = $jenis_limbah;
				$data[] = $row;
			}
		}

		return $data;
	}
	
	function get_detail_num_rows($where) {

		$this->db->select('*');
		$this->db->from('tps_limbah');		

		if($where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}
	
	function get_detail_tps_by_id($id) {
		$this->db->where('id_tps_limbah', $id);
		
		$query = $this->db->get('tps_limbah');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function delete_detail_tps($id) {
		$data = $this->get_detail_tps_by_id($id);
		$this->db->where('id_tps_limbah', $id);
		$this->db->delete('tps_limbah');
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	function change_detail_tps($data, $id) {

		$this->db->where("id_tps_limbah", $id);
		$this->db->update('tps_limbah', $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_detail($data){
		$this->db->insert('tps_limbah', $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function is_duplicate_detail($id, $luas_tps, $lama_penyimpanan, $derajat_s, $jam_s, $menit_s, $derajat_e, $jam_e, $menit_e) {
		$this->db->where('id_izin_tps', $id);
		$this->db->where('luas_tps', $luas_tps);
		$this->db->where('lama_penyimpanan', $lama_penyimpanan);
		$this->db->where('derajat_s', $derajat_s);
		$this->db->where('jam_s', $jam_s);
		$this->db->where('menit_s', $menit_s);
		$this->db->where('derajat_e', $derajat_e);
		$this->db->where('jam_e', $jam_e);
		$this->db->where('menit_e', $menit_e);
		$this->db->from('tps_limbah');
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}

}
?>