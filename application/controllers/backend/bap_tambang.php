<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Document   : bap_tambang.php 
// Created on : December 08, 2014 16:45 
// Author     : huda.jtk09@gmail.com
// Description: Controller for BAP Pertambangan 

class bap_tambang extends CI_Controller {

	private $id_usaha_kegiatan = 17; #id_usaha_kegiatan hotel
	private $bool = array('0', '1');
    private $day_id = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
    private $jenis_perusahaan = array('PMA', 'PMDN', 'other');
    private $periode_uji = array('Setiap Bulan', 'other');
    private $emboil = array('Boiler', 'other');
    private $padat_kelola = array('Dibakar', 'Diangkut oleh Pihak Ketiga', 'Diangkut oleh Dinas Pertasih', 'Dijual', 'Lainnya');
    private $jenis_alat_ukur = array('Kumulatif', 'V-notch', 'Digital', 'other');
	
    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_bap_tambang', 'tamDao', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);
        $this->load->model('m_employee', 'empDao', TRUE);
        $this->load->model('m_laporan_bap', 'lapDao', TRUE);
        $this->load->model('m_data_master', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);

		$this->load->library('PHPExcel');
    }

    # default view
	public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all bap tambang
        $bap_tambang = $this->tamDao->get_all();
		
		# get all industri
		// $industri = $this->indDao->get_all_industri($this->id_usaha_kegiatan);
		$industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

		# get all industri
		$pegawai = $this->empDao->get_all_employee();

		# get all lab
		$data['lab'] = $this->bapDao->select_table('laboratorium');
		
		# get all pihak ke 3
		$data['pihak_ke3'] = $this->bapDao->select_table('pengolah_limbah');

		# INI DIA DATA ACUAN
		$data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
		$data['proses_produksi'] = $this->m_data_master->get_dropdown("proses_produksi");
		$data['ipal'] = $this->m_data_master->get_dropdown("sistem_ipal");
		$data['bb'] = $this->m_data_master->get_dropdown("jenis_bahan_bakar");
		$data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
		$data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
		$data['ipal_unit'] = $this->m_data_master->get_dropdown("unit_ipal");
		$data['jenis_pengujian'] = $this->m_data_master->get_dropdown("jenis_pengujian");
		$data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");
		$data['jenis_limbah'] = $this->m_data_master->get_dropdown("jenis_limbah");

		# response paramter
        $data['bap_tambang'] = $bap_tambang;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;        
		
        # parameter for view
        $data['tab_list'] = 'active';
		$data['tab_view'] = '';

		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_tambang', $data);
    }
	
	public function view() {
        # get id bap from view
        $id_bap = $this->input->post('id_bap');

        // redirect if id_bap is null
        if (empty($id_bap)) {
            redirect('backend/bap_tambang');
        };

        $bap = $this->bapDao->get_obj_by_id("bap", "id_bap", $id_bap);
        $bap_tambang = $this->bapDao->get_obj_by_id("bap_tambang", "id_bap", $id_bap);
        $industri =	$this->bapDao->get_obj_by_id("industri", "id_industri", $bap->id_industri);
        $pegawai = $this->bapDao->get_obj_by_id("pegawai", "id_pegawai", $bap->id_pegawai);
        $petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

        $data['bap'] = $bap;
        $data['bap_tambang'] = $bap_tambang;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;
        $data['petugas_bap'] = $petugas_bap;

        /*pencemaran air*/
        $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);
        if ($pencemaran_air) {
            $bahan_kimia_ipal = $this->bapDao->get_list_by_id("bahan_kimia_ipal", "id_penc_air", $pencemaran_air->id_penc_air);

            $data['pencemaran_air'] = $pencemaran_air;
            $data['bahan_kimia_ipal'] = $bahan_kimia_ipal;
        }

        /*pencemaran udara*/
        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
        if ($pencemaran_udara) {
            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
        }


        /*pencemaran pb3*/
        $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
        if ($pencemaran_pb3) {
            $jenis_limbah_b3 = $this->bapDao->get_list_by_id("jenis_limbah_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);
            $tps_b3 = $this->bapDao->get_list_by_id("tps_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

            $data['pencemaran_pb3'] = $pencemaran_pb3;
            $data['jenis_limbah_b3'] = $jenis_limbah_b3;
            $data['tps_b3'] = $tps_b3;
        }

        # parameter for view
        $data['tab_list'] = '';
        $data['tab_view'] = 'active';

        # load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_tambang', $data);
	}

	public function newInsert() {
		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();

		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			// 'url' => $this->input->post(''),
			// 'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => date("Y-m-d", strtotime($this->input->post('bap_tgl'))),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->input->post('dok_lingk'),
			'dok_lingk_jenis' => $this->input->post('dok_lingk_jenis'),
			'dok_lingk_tahun' => $this->input->post('dok_lingk_tahun'),
			'izin_lingk' => $this->input->post('izin_lingk'),
			'izin_lingk_tahun' => $this->input->post('izin_lingk_tahun'),
			'tgl_pembuatan' => $datenow,
			'dibuat_oleh' => $user,
			'catatan' => $this->input->post('catatan')
			);

		# insert bap
		$id_bap = $this->bapDao->insert_getID('bap',$bap);

		$bap['id_bap'] = $id_bap;

		# Petugas BAP Add procedur
		$id_petugas_bap_list = $this->input->post('id_petugas_bap_list');

		if ($id_petugas_bap_list[0]!=null) {
			foreach ($id_petugas_bap_list as $idpbap) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $idpbap,
					// 'ket' => '',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);

				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
			}
		}
		
		# array for table bap_tambang
		$bap_tambang = array(
			'id_bap' => $id_bap,
			'jumlah_karyawan' => $this->input->post('jumlah_karyawan'),
			'jenis_tambang' => $this->input->post('jenis_tambang'),
			'izin_usaha' => $this->input->post('izin_usaha'),
			);

		# insert bap_tambang
		$this->bapDao->insert_getStat('bap_tambang', $bap_tambang);

		$limb_sumber = $this->input->post('limb_sumber');
		$temp = '';
		foreach ($limb_sumber as $key) {
			$temp .= $key.", ";
		}

		$finlim_sumber = rtrim($temp, ', ');

		# array for pencemaran_air
		$pencemaran_air = array(
			'id_bap' => $id_bap,
			'ambil_air_tanah' => $this->input->post('ambil_air_tanah'),
			'ambil_air_tanah_izin' => $this->input->post('ambil_air_tanah_izin'),
			'ambil_air_permukaan' => $this->input->post('ambil_air_permukaan'),
			'ambil_air_permukaan_izin' => $this->input->post('ambil_air_permukaan_izin'),
			'ambil_air_pdam' => $this->input->post('ambil_air_pdam'),
			'ambil_air_lain' => $this->input->post('ambil_air_lain'),
			'limb_sumber' => $finlim_sumber,
			'bdn_terima' => $this->input->post('bdn_terima'),
			'sarana_olah_limbah' => $this->input->post('sarana_olah_limbah'),
			'sarana_jenis' => $this->input->post('sarana_jenis'),
			'sarana_kapasitas' => $this->input->post('sarana_kapasitas'),
			'koord_outlet_derajat_s' => $this->input->post('koord_outlet_derajat_s'),
			'koord_outlet_jam_s' => $this->input->post('koord_outlet_jam_s'),
			'koord_outlet_menit_s' => $this->input->post('koord_outlet_menit_s'),
			'koord_outlet_derajat_e' => $this->input->post('koord_outlet_derajat_e'),
			'koord_outlet_jam_e' => $this->input->post('koord_outlet_jam_e'),
			'koord_outlet_menit_e' => $this->input->post('koord_outlet_menit_e'),
			'lain_lain' => $this->input->post('lain_lain_pa')
			);
		
		# insert pencemaran_air
		$id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);
		
		# array for pencemaran_udara
		$pencemaran_udara = array(
			'id_bap' => $id_bap,
			'pelaporan_ua_ue' => $this->input->post('pelaporan_ua_ue'),
			'lain_lain' => $this->input->post('lain_lain_pu')
			);

		# insert pencemaran_udara
		$id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);
		
		# collecting data for uji_ambien
		$cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
		$uji_ambien_list = array();
		
		foreach ($cols as $i => $val) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $val,
				'uji_kualitas' => $this->input->post('uji_kualitas'.$i),
				'period' => $this->input->post('period'.$i),
				'lab' => $this->input->post('lab'.$i),
				'bm_pemenuhan' => $this->input->post('bm_pemenuhan'.$i),
				'bm_param' => $this->input->post('bm_param'.$i),
				'bm_period' => $this->input->post('bm_period'.$i)
				);
			
			$uji_ambien_list[] = $uji_ambien;

			# insert uji_ambien
			$this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
		}

		$padat_jenis_tmp = $this->input->post('padat_jenis');
		$empty = '';
		foreach ($padat_jenis_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jenis = rtrim($empty, ', ');

		$padat_jumlah_tmp = $this->input->post('padat_jumlah');
		$empty = '';
		foreach ($padat_jumlah_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jumlah = rtrim($empty, ', ');

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'b3_penyimpanan' => $this->input->post('b3_penyimpanan'),
			'b3_neraca' => $this->input->post('b3_neraca'),
			'b3_manifest' => $this->input->post('b3_manifest'),
			'b3_lapor_nm' => $this->input->post('b3_lapor_nm'),
			'padat_jenis' => $padat_jenis,
			'padat_jumlah' => $padat_jumlah,
			'padat_sarana_tong' => $this->input->post('padat_sarana_tong'),
			'padat_sarana_tps' => $this->input->post('padat_sarana_tps'),
			
			'lain_lain' => $this->input->post('lain_lain_pb3')
			);

		if ($this->input->post('padat_kelola1') != 'Lainnya') {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola1');
		} else {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola2');
		}

		# insert pencemaran_pb3
		$id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

 		# array for jenis_limbah_b3
		$jenis_limbah_baru = $this->input->post('jenis_limbah');
		$jumlah = $this->input->post('jumlah');
		$pengelolaan = $this->input->post('pengelolaan');
		$pihak_ke3 = $this->input->post('pihak_ke3');

		$jenis_limbah_b3_list = array();

		for ($i=0; $i<count($jenis_limbah_baru); $i++) {
			$jenis_limbah_b3 = array(
				'id_penc_pb3' => $id_penc_pb3,
				'jenis' => $jenis_limbah_baru[$i],
				'jumlah' => $jumlah[$i],
				'pengelolaan' => $pengelolaan[$i],
				'pihak_ke3' => $pihak_ke3[$i]
				);

				$jenis_limbah_b3_list[] = $jenis_limbah_b3;
				
				#insert jenis_limbah_b3
				$this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
		}

		# collecting data for tps_b3
		$cols = array('1', '2', '3', '4');
		$tps_b3_list = array();

		foreach ($cols as $i) {

			if($this->input->post('tb_izin'.$i) == 'Ada'){
				$izin_no = $this->input->post('tb_izin_no'.$i);
				$izin_tgl = date("Y-m-d", strtotime($this->input->post('tb_izin_tgl'.$i)));
			} else {
				$izin_no = null;
				$izin_tgl = null;
			}
			$tps_b3 = array(
				'id_penc_pb3' => $id_penc_pb3,
				'izin' => $this->boolConverter($this->input->post('tb_izin'.$i)),
				'izin_no' => $izin_no,
				'izin_tgl' => $izin_tgl,
				'jenis' => $this->input->post('tb_jenis'.$i),
				'lama_spn' => serialize(array('jenis_limbah' => $this->input->post('jenis_limbah_lama_spn'), 'tps' => $this->input->post('tb_lama_spn_'.$i))),
				'koord_derajat_s' => $this->input->post('koord_derajat_s'.$i), 
				'koord_jam_s' => $this->input->post('koord_jam_s'.$i), 
				'koord_menit_s' => $this->input->post('koord_menit_s'.$i), 
				'koord_derajat_e' => $this->input->post('koord_derajat_e'.$i),	
				'koord_jam_e' => $this->input->post('koord_jam_e'.$i), 
				'koord_menit_e' => $this->input->post('koord_menit_e'.$i),
				'ukuran' => $this->input->post('tb_ukuran'.$i),
				'ppn_nama_koord' => $this->input->post('tb_ppn_nama_koord'.$i),
				'simbol_label' => $this->input->post('tb_simbol_label'.$i),
				'saluran_cecer' => $this->input->post('tb_saluran_cecer'.$i),
				'bak_cecer' => $this->input->post('tb_bak_cecer'.$i),
				'kemiringan' => $this->input->post('tb_kemiringan'.$i),
				'sop_darurat' => $this->input->post('tb_sop_darurat'.$i),
				'log_book' => $this->input->post('tb_log_book'.$i), 
				'apar_p3k' => $this->input->post('tb_apar_p3k'.$i),
				'p3k' => $this->input->post('tb_p3k'.$i)
				);

			if ($this->input->post('tb_izin'.$i) != 3) {
				$tps_b3_list[] = $tps_b3;
			} else {
				$tps_b3['lama_spn_lumpur'] = NULL;
				$tps_b3['lama_spn_batu_bara'] = NULL;
				$tps_b3['lama_spn_pelumas'] = NULL;
				$tps_b3['lama_spn_aki_bekas'] = NULL;
				$tps_b3['lama_spn_ltl'] = NULL;
				$tps_b3['lama_spn_kmsn_b3'] = NULL;
				$tps_b3['lama_spn_majun_b3'] = NULL;
				$tps_b3['lama_spn_limbah_elektro'] = NULL;
				$tps_b3['lama_spn_limbah_kimia'] = NULL;
				$tps_b3['lama_spn_lain_ket'] = NULL;
				$tps_b3['ukuran'] = NULL;
				$tps_b3['izin_no'] = NULL;
				$tps_b3['izin_tgl'] = NULL;
				$tps_b3['jenis'] = NULL;
				$tps_b3['koord_derajat_s'] = null;
				$tps_b3['koord_jam_s'] = null;
				$tps_b3['koord_menit_s'] = null;
				$tps_b3['koord_derajat_e'] = null;
				$tps_b3['koord_jam_e'] = null;
				$tps_b3['koord_menit_e'] = null;
				$tps_b3['ppn_nama_koord'] = NULL;
				$tps_b3['simbol_label'] = NULL;
				$tps_b3['saluran_cecer'] = NULL;
				$tps_b3['bak_cecer'] = NULL;
				$tps_b3['kemiringan'] = NULL;
				$tps_b3['sop_darurat'] = NULL;
				$tps_b3['log_book'] = NULL;
				$tps_b3['apar_p3k'] = NULL;
				$tps_b3['p3k'] = NULL;
			}

			$this->bapDao->insert_getStat('tps_b3', $tps_b3);
		}

        $this->bapDao->collectStatus($id_bap, "bap_lab");

		# notification message
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
			
		redirect('backend/bap_tambang');
	}

	public function newSaveEdit() {
		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();

		# Get ID
		$id_bap = $this->input->post('id_bap');

		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			// 'url' => $this->input->post(''),
			// 'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => date("Y-m-d", strtotime($this->input->post('bap_tgl'))),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->input->post('dok_lingk'),
			'dok_lingk_jenis' => $this->input->post('dok_lingk_jenis'),
			'dok_lingk_tahun' => $this->input->post('dok_lingk_tahun'),
			'izin_lingk' => $this->input->post('izin_lingk'),
			'izin_lingk_tahun' => $this->input->post('izin_lingk_tahun'),
			'catatan' => $this->input->post('catatan'),
			'tgl_update' => $datenow,
			'diupdate_oleh' => $user
			);

		# edit bap
		$this->bapDao->edit('bap', 'id_bap', $id_bap, $bap);

		$bap['id_bap'] = $id_bap;

		# Petugas BAP update procedur
		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);

		$id_petugas_bap_list = $this->input->post('id_petugas_bap_list');

		if ($id_petugas_bap_list[0]!=null) {
			foreach ($id_petugas_bap_list as $idpbap) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $idpbap,
					'ket' => 'update',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);

				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
			}
		}

		# array for table bap_tambang
		$bap_tambang = array(
			'id_bap' => $id_bap,
			'jumlah_karyawan' => $this->input->post('jumlah_karyawan'),
			'jenis_tambang' => $this->input->post('jenis_tambang'),
			'izin_usaha' => $this->input->post('izin_usaha'),
			);
		
		$id_bap_tambang = $this->input->post('id_bap_tambang');

		# edit bap_tambang
		$this->bapDao->edit('bap_tambang', 'id_bap_tambang', $id_bap_tambang, $bap_tambang);

		$limb_sumber = $this->input->post('limb_sumber');
		$temp = '';
		foreach ($limb_sumber as $key) {
			$temp .= $key.", ";
		}

		$finlim_sumber = rtrim($temp, ', ');

		# array for pencemaran_air
		$pencemaran_air = array(
			'ambil_air_tanah' => $this->input->post('ambil_air_tanah'),
			'ambil_air_tanah_izin' => $this->input->post('ambil_air_tanah_izin'),
			'ambil_air_permukaan' => $this->input->post('ambil_air_permukaan'),
			'ambil_air_permukaan_izin' => $this->input->post('ambil_air_permukaan_izin'),
			'ambil_air_pdam' => $this->input->post('ambil_air_pdam'),
			'ambil_air_lain' => $this->input->post('ambil_air_lain'),
			'limb_sumber' => $finlim_sumber,
			'bdn_terima' => $this->input->post('bdn_terima'),
			'sarana_olah_limbah' => $this->input->post('sarana_olah_limbah'),
			'sarana_jenis' => $this->input->post('sarana_jenis'),
			'sarana_kapasitas' => $this->input->post('sarana_kapasitas'),
			'koord_outlet_derajat_s' => $this->input->post('koord_outlet_derajat_s'),
			'koord_outlet_jam_s' => $this->input->post('koord_outlet_jam_s'),
			'koord_outlet_menit_s' => $this->input->post('koord_outlet_menit_s'),
			'koord_outlet_derajat_e' => $this->input->post('koord_outlet_derajat_e'),
			'koord_outlet_jam_e' => $this->input->post('koord_outlet_jam_e'),
			'koord_outlet_menit_e' => $this->input->post('koord_outlet_menit_e'),
			'lain_lain' => $this->input->post('lain_lain_pa')
			);
		
		$id_penc_air = $this->input->post('id_penc_air');

		# edit pencemaran_air
		$this->bapDao->edit('pencemaran_air', 'id_penc_air', $id_penc_air, $pencemaran_air);

		# array for pencemaran_udara
		$pencemaran_udara = array(
			'id_bap' => $id_bap,
			'pelaporan_ua_ue' => $this->input->post('pelaporan_ua_ue'),
			'lain_lain' => $this->input->post('lain_lain_pu')
			);

		$id_penc_udara = $this->input->post('id_penc_udara');

		# edit pencemaran_udara
		$this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $id_penc_udara, $pencemaran_udara);
		
		# collecting data for uji_ambien
		$cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
		$uji_ambien_list = array();
		
		foreach ($cols as $i => $val) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $val,
				'uji_kualitas' => $this->input->post('uji_kualitas'.$i),
				'period' => $this->input->post('period'.$i),
				'lab' => $this->input->post('lab'.$i),
				'bm_pemenuhan' => $this->input->post('bm_pemenuhan'.$i),
				'bm_param' => $this->input->post('bm_param'.$i),
				'bm_period' => $this->input->post('bm_period'.$i)
				);
			
			$id_uji_ambien = $this->input->post('id_uji_ambien'.$i);
			
			# edit uji_ambien
			$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien, $uji_ambien);

			$uji_ambien_list[] = $uji_ambien;
		}

		$padat_jenis_tmp = $this->input->post('padat_jenis');
		$empty = '';
		foreach ($padat_jenis_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jenis = rtrim($empty, ', ');

		$padat_jumlah_tmp = $this->input->post('padat_jumlah');
		$empty = '';
		foreach ($padat_jumlah_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jumlah = rtrim($empty, ', ');

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'b3_penyimpanan' => $this->input->post('b3_penyimpanan'),
			'b3_neraca' => $this->input->post('b3_neraca'),
			'b3_manifest' => $this->input->post('b3_manifest'),
			'b3_lapor_nm' => $this->input->post('b3_lapor_nm'),
			'padat_jenis' => $padat_jenis,
			'padat_jumlah' => $padat_jumlah,
			'padat_sarana_tong' => $this->input->post('padat_sarana_tong'),
			'padat_sarana_tps' => $this->input->post('padat_sarana_tps'),
			
			'lain_lain' => $this->input->post('lain_lain_pb3')
			);

		if ($this->input->post('padat_kelola1') != 'Lainnya') {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola1');
		} else {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola2');
		}
		
		$id_penc_pb3 = $this->input->post('id_penc_pb3');

		# edit pencemaran_pb3
		$this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $id_penc_pb3, $pencemaran_pb3);

 		# array for jenis_limbah_b3
		$this->bapDao->delete('jenis_limbah_b3', 'id_penc_pb3', $id_penc_pb3);

		$jenis_limbah_baru = $this->input->post('jenis_limbah');
		$jumlah = $this->input->post('jumlah');
		$pengelolaan = $this->input->post('pengelolaan');
		$pihak_ke3 = $this->input->post('pihak_ke3');

		$jenis_limbah_b3_list = array();

		for ($i=0; $i<count($jenis_limbah_baru); $i++) {
			$jenis_limbah_b3 = array(
				'id_penc_pb3' => $id_penc_pb3,
				'jenis' => $jenis_limbah_baru[$i],
				'jumlah' => $jumlah[$i],
				'pengelolaan' => $pengelolaan[$i],
				'pihak_ke3' => $pihak_ke3[$i]
				);

				$jenis_limbah_b3_list[] = $jenis_limbah_b3;
				
				#insert jenis_limbah_b3
				$this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
		}

		$this->bapDao->delete('tps_b3', 'id_penc_pb3', $id_penc_pb3);

		# collecting data for tps_b3
		$cols = array('1', '2', '3', '4');
		$tps_b3_list = array();

		foreach ($cols as $i) {

			if($this->input->post('tb_izin'.$i) == 'Ada'){
				$izin_no = $this->input->post('tb_izin_no'.$i);
				$izin_tgl = date("Y-m-d", strtotime($this->input->post('tb_izin_tgl'.$i)));
			} else {
				$izin_no = null;
				$izin_tgl = null;
			}

			$tps_b3 = array(
				'id_penc_pb3' => $id_penc_pb3,
				'izin' => $this->boolConverter($this->input->post('tb_izin'.$i)),
				'izin_no' => $izin_no,
				'izin_tgl' => $izin_tgl,
				'jenis' => $this->input->post('tb_jenis'.$i),
				'lama_spn' => serialize(array('jenis_limbah' => $this->input->post('jenis_limbah_lama_spn'), 'tps' => $this->input->post('tb_lama_spn_'.$i))),
				'koord_derajat_s' => $this->input->post('koord_derajat_s'.$i), 
				'koord_jam_s' => $this->input->post('koord_jam_s'.$i), 
				'koord_menit_s' => $this->input->post('koord_menit_s'.$i), 
				'koord_derajat_e' => $this->input->post('koord_derajat_e'.$i),	
				'koord_jam_e' => $this->input->post('koord_jam_e'.$i), 
				'koord_menit_e' => $this->input->post('koord_menit_e'.$i),
				'ukuran' => $this->input->post('tb_ukuran'.$i),
				'ppn_nama_koord' => $this->input->post('tb_ppn_nama_koord'.$i),
				'simbol_label' => $this->input->post('tb_simbol_label'.$i),
				'saluran_cecer' => $this->input->post('tb_saluran_cecer'.$i),
				'bak_cecer' => $this->input->post('tb_bak_cecer'.$i),
				'kemiringan' => $this->input->post('tb_kemiringan'.$i),
				'sop_darurat' => $this->input->post('tb_sop_darurat'.$i),
				'log_book' => $this->input->post('tb_log_book'.$i), 
				'apar_p3k' => $this->input->post('tb_apar_p3k'.$i),
				'p3k' => $this->input->post('tb_p3k'.$i)
				);

			if ($this->input->post('tb_izin'.$i) != 3) {
				$tps_b3_list[] = $tps_b3;
			} else {
				$tps_b3['lama_spn_lumpur'] = NULL;
				$tps_b3['lama_spn_batu_bara'] = NULL;
				$tps_b3['lama_spn_pelumas'] = NULL;
				$tps_b3['lama_spn_aki_bekas'] = NULL;
				$tps_b3['lama_spn_ltl'] = NULL;
				$tps_b3['lama_spn_kmsn_b3'] = NULL;
				$tps_b3['lama_spn_majun_b3'] = NULL;
				$tps_b3['lama_spn_limbah_elektro'] = NULL;
				$tps_b3['lama_spn_limbah_kimia'] = NULL;
				$tps_b3['lama_spn_lain_ket'] = NULL;
				$tps_b3['ukuran'] = NULL;
				$tps_b3['izin_no'] = NULL;
				$tps_b3['izin_tgl'] = NULL;
				$tps_b3['jenis'] = NULL;
				$tps_b3['koord_derajat_s'] = null;
				$tps_b3['koord_jam_s'] = null;
				$tps_b3['koord_menit_s'] = null;
				$tps_b3['koord_derajat_e'] = null;
				$tps_b3['koord_jam_e'] = null;
				$tps_b3['koord_menit_e'] = null;
				$tps_b3['ppn_nama_koord'] = NULL;
				$tps_b3['simbol_label'] = NULL;
				$tps_b3['saluran_cecer'] = NULL;
				$tps_b3['bak_cecer'] = NULL;
				$tps_b3['kemiringan'] = NULL;
				$tps_b3['sop_darurat'] = NULL;
				$tps_b3['log_book'] = NULL;
				$tps_b3['apar_p3k'] = NULL;
				$tps_b3['p3k'] = NULL;
			}

			$this->bapDao->insert_getStat('tps_b3', $tps_b3);
		}

		# Compare BAP
		$data['bap'] = $bap;
		$data['pencemaran_air'] = $pencemaran_air;
		$data['uji_ambien'] = $uji_ambien_list;
		$data['pencemaran_pb3'] = $pencemaran_pb3;
		$data['jenis_limbah_b3'] = $jenis_limbah_b3_list;
		$data['tps_b3'] = $tps_b3_list;
		$data['datenow'] = $datenow;
		$data['user'] = $user;

		$total_error = $this->compareBap($data, 'update');


		# error message
		if ($total_error > 0) {
			$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP: '.$total_error.' item'));
		}

		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data BAP berhasil diperbaharui'));
			
		redirect('backend/bap_tambang');
	}

	public function edit() {
		# get id bap from view
		$id_bap = $this->input->post('id_bap');
        $last_insert = false;
		
		// redirect if id_bap is null
		if (empty($id_bap)) {
	        redirect('backend/bap_tambang');
		};

        $check = $this->bapDao->collectStatus($id_bap, "bap_tambang");

        if ($check['status']) {

            $bap = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);

            $petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

            # get all industri
            // $industri = $this->indDao->get_all_industri($this->id_usaha_kegiatan);
            $industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

            # get all pegawai
            $pegawai = $this->empDao->get_all_employee();

            $bap_tambang = $this->bapDao->get_obj_by_id("bap_tambang", "id_bap", $id_bap);

            $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

            $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);

            $jenis_limbah_b3 = $this->bapDao->get_list_by_id("jenis_limbah_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

            $tps_b3 = $this->bapDao->get_list_by_id("tps_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

            # get all lab
            $data['lab'] = $this->bapDao->select_table('laboratorium');

            # get all pihak ke 3
            $data['pihak_ke3'] = $this->bapDao->select_table('pengolah_limbah');

            # INI DIA DATA ACUAN
            $data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
            $data['proses_produksi'] = $this->m_data_master->get_dropdown("proses_produksi");
            $data['ipal'] = $this->m_data_master->get_dropdown("sistem_ipal");
            $data['bb'] = $this->m_data_master->get_dropdown("jenis_bahan_bakar");
            $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
            $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
            $data['ipal_unit'] = $this->m_data_master->get_dropdown("unit_ipal");
            $data['jenis_pengujian'] = $this->m_data_master->get_dropdown("jenis_pengujian");
            $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");
            $data['jenis_limbah'] = $this->m_data_master->get_dropdown("jenis_limbah");
            $data['bool'] = $this->bool;

            $data['bap'] = $bap;
            $data['bap_tambang'] = $bap_tambang;
            $data['industri'] = $industri;
            $data['pegawai'] = $pegawai;
            $data['petugas_bap'] = $petugas_bap;
            $data['pencemaran_air'] = $pencemaran_air;
            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
            $data['pencemaran_pb3'] = $pencemaran_pb3;
            $data['jenis_limbah_b3'] = $jenis_limbah_b3;
            $data['tps_b3'] = $tps_b3;

            # load view
            $this->load->view('include/header');
            $this->load->view('include/footer');
            $this->load->view('backend/v_bap_tambang_edit', $data);
        } else {
            $this->partialEdit($id_bap, $check, $last_insert);
        }
	}

	public function tipe_edit() {
        $last_insert = false;
		if($this->input->post('id_last_input') != null) {

			$id_last_input = $this->input->post('id_last_input');
			$jenis_bap = 'bap_tambang';
			$id_bap = $this->indDao->get_last_input_id_bap($id_last_input, $jenis_bap);
            $last_insert = true;
			if($id_bap == null){

				$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Data BAP Pertambangan pada Nama Usaha/Kegiatan yang anda pilih tidak ditemukan!, silahkan masukkan data baru, atau pilih Nama Usaha/Kegiatan yang lain'));
			
				redirect('backend/bap_tambang');
			}
		} else {

			$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Silahkan pilih Nama Usaha/Kegiatan terlebih dahulu!'));
			
			redirect('backend/bap_tambang');
		}

        $check = $this->bapDao->collectStatus($id_bap, "bap_tambang");

        if ($check['status']) {

            $bap = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);

            $petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

            # get all industri
            $industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

            # get all pegawai
            $pegawai = $this->empDao->get_all_employee();

            $bap_tambang = $this->bapDao->get_obj_by_id("bap_tambang", "id_bap", $id_bap);

            $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

            $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);

            $jenis_limbah_b3 = $this->bapDao->get_list_by_id("jenis_limbah_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

            $tps_b3 = $this->bapDao->get_list_by_id("tps_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

            # get all lab
            $data['lab'] = $this->bapDao->select_table('laboratorium');

            # get all pihak ke 3
            $data['pihak_ke3'] = $this->bapDao->select_table('pengolah_limbah');

            # INI DIA DATA ACUAN
            $data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
            $data['proses_produksi'] = $this->m_data_master->get_dropdown("proses_produksi");
            $data['ipal'] = $this->m_data_master->get_dropdown("sistem_ipal");
            $data['bb'] = $this->m_data_master->get_dropdown("jenis_bahan_bakar");
            $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
            $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
            $data['ipal_unit'] = $this->m_data_master->get_dropdown("unit_ipal");
            $data['jenis_pengujian'] = $this->m_data_master->get_dropdown("jenis_pengujian");
            $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");
            $data['jenis_limbah'] = $this->m_data_master->get_dropdown("jenis_limbah");
            $data['bool'] = $this->bool;

            $data['bap'] = $bap;
            $data['bap_tambang'] = $bap_tambang;
            $data['industri'] = $industri;
            $data['pegawai'] = $pegawai;
            $data['petugas_bap'] = $petugas_bap;
            $data['pencemaran_air'] = $pencemaran_air;
            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
            $data['pencemaran_pb3'] = $pencemaran_pb3;
            $data['jenis_limbah_b3'] = $jenis_limbah_b3;
            $data['tps_b3'] = $tps_b3;

            # load view
            $this->load->view('include/header');
            $this->load->view('include/footer');
            $this->load->view('backend/v_bap_tambang_last_insert', $data);
        } else {
            $this->partialEdit($id_bap, $check, $last_insert);
        }
	}

	# delete bap
	public function delete() {
		$id_bap = $this->input->post('id');

		$this->bapDao->delete('bap_tambang', 'id_bap', $id_bap);

		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('surat_teguran_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);

		$this->bapDao->delete_penc_air($id_bap);
		$this->bapDao->delete_penc_udara($id_bap);
		$this->bapDao->delete_penc_pb3($id_bap);

		$this->bapDao->delete('bap', 'id_bap', $id_bap);
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/bap_tambang');
	}

	# function to compare bap value with item ketaatan bap
	public function compareBap($data, $action) {
		$item_teguran = array();
		$parameter_bap = $this->lapDao->get_ketaatan_bap('bap_tambang');

		foreach ($parameter_bap as $p) {
			$param[$p->ket] = $p->nilai_parameter_bap;
			$stat[$p->ket] = $p->status;
			$parent[$p->ket] = new StdClass;
			$parent[$p->ket]->one = $p->id_parent_1;
			$parent[$p->ket]->two = $p->id_parent_2;
		}

		$bap = $data['bap'];
		$pencemaran_air = $data['pencemaran_air'];
		$uji_ambien = $data['uji_ambien'];
		$pencemaran_pb3 = $data['pencemaran_pb3'];
		$jenis_limbah_b3 = $data['jenis_limbah_b3'];
		$tps_b3 = $data['tps_b3'];
		$datenow = $data['datenow'];
		$user = $data['user'];

		$id_bap = $bap['id_bap'];

		if ($stat['Dokumen Lingkungan']) {
			if ($this->valChecker($this->adaConv($bap['dok_lingk']),$param['Dokumen Lingkungan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Dokumen Lingkungan'],
					'nilai_parameter' => $this->adaConv($bap['dok_lingk']),
					'ket_parameter_bap' => 'Dokumen Lingkungan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Dokumen Lingkungan']->one,
					'id_parent_2' => $parent['Dokumen Lingkungan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Izin Lingkungan']) {
			if ($this->valChecker($this->adaConv($bap['izin_lingk']),$param['Izin Lingkungan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Izin Lingkungan'],
					'nilai_parameter' => $this->adaConv($bap['izin_lingk']),
					'ket_parameter_bap' => 'Izin Lingkungan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Izin Lingkungan']->one,
					'id_parent_2' => $parent['Izin Lingkungan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Sarana Pengolahan Air Limbah']) {
			if ($this->valChecker($this->adaConv($pencemaran_air['sarana_olah_limbah']),$param['Sarana Pengolahan Air Limbah'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Sarana Pengolahan Air Limbah'],
					'nilai_parameter' => $this->adaConv($pencemaran_air['sarana_olah_limbah']),
					'ket_parameter_bap' => 'Sarana Pengolahan Air Limbah',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Sarana Pengolahan Air Limbah']->one,
					'id_parent_2' => $parent['Sarana Pengolahan Air Limbah']->two
					);
				$item_teguran[] = $item;
			}
		}

		foreach ($uji_ambien as $ua) {
			if ($stat['Pengujian Kualitas']) {
				if ($this->valChecker($this->adaConv($ua['uji_kualitas']),$param['Pengujian Kualitas'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pengujian Kualitas'],
						'nilai_parameter' => $this->adaConv($ua['uji_kualitas']),
						'ket_parameter_bap' => 'Pengujian Kualitas '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pengujian Kualitas']->one,
						'id_parent_2' => $parent['Pengujian Kualitas']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Periode pengujian (per 6 bulan)']) {
				if ($this->valChecker($ua['period'],$param['Periode pengujian (per 6 bulan)'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Periode pengujian (per 6 bulan)'],
						'nilai_parameter' => $ua['period'],
						'ket_parameter_bap' => 'Periode pengujian (per 6 bulan) '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Periode pengujian (per 6 bulan)']->one,
						'id_parent_2' => $parent['Periode pengujian (per 6 bulan)']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Pemenuhan BM']) {
				if ($this->valChecker($this->yaConv($ua['bm_pemenuhan']),$param['Pemenuhan BM'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pemenuhan BM'],
						'nilai_parameter' => $this->yaConv($ua['bm_pemenuhan']),
						'ket_parameter_bap' => 'Pemenuhan BM '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pemenuhan BM']->one,
						'id_parent_2' => $parent['Pemenuhan BM']->two
						);
					$item_teguran[] = $item;
				}
			}
		}

		foreach ($jenis_limbah_b3 as $jlb3) {
			if ($stat['Pengelolaan Limbah B3']) {
				if ($this->valChecker($jlb3['pengelolaan'],$param['Pengelolaan Limbah B3'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pengelolaan Limbah B3'],
						'nilai_parameter' => $jlb3['pengelolaan'],
						'ket_parameter_bap' => 'Pengelolaan '.$jlb3['jenis'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pengelolaan Limbah B3']->one,
						'id_parent_2' => $parent['Pengelolaan Limbah B3']->two
						);
					$item_teguran[] = $item;
				}
			}
		}

		$t = 1;
		foreach ($tps_b3 as $tps) {
			if ($stat['Perizinan TPS']) {
				if ($this->valChecker($this->adaConv($tps['izin']),$param['Perizinan TPS'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Perizinan TPS'],
						'nilai_parameter' => $this->adaConv($tps['izin']),
						'ket_parameter_bap' => 'Perizinan TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Perizinan TPS']->one,
						'id_parent_2' => $parent['Perizinan TPS']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Papan nama dan koordinat']) {
				if ($this->valChecker($this->adaConv($tps['ppn_nama_koord']),$param['Papan nama dan koordinat'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Papan nama dan koordinat'],
						'nilai_parameter' => $this->adaConv($tps['ppn_nama_koord']),
						'ket_parameter_bap' => 'Papan nama dan koordinat'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Papan nama dan koordinat']->one,
						'id_parent_2' => $parent['Papan nama dan koordinat']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Simbol dan label']) {
				if ($this->valChecker($this->adaConv($tps['simbol_label']),$param['Simbol dan label'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Simbol dan label'],
						'nilai_parameter' => $this->adaConv($tps['simbol_label']),
						'ket_parameter_bap' => 'Simbol dan label'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Simbol dan label']->one,
						'id_parent_2' => $parent['Simbol dan label']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Saluran ceceran air limbah']) {
				if ($this->valChecker($this->adaConv($tps['saluran_cecer']),$param['Saluran ceceran air limbah'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Saluran ceceran air limbah'],
						'nilai_parameter' => $this->adaConv($tps['saluran_cecer']),
						'ket_parameter_bap' => 'Saluran ceceran air limbah'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Saluran ceceran air limbah']->one,
						'id_parent_2' => $parent['Saluran ceceran air limbah']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Bak penampung ceceran air limbah']) {
				if ($this->valChecker($this->adaConv($tps['bak_cecer']),$param['Bak penampung ceceran air limbah'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Bak penampung ceceran air limbah'],
						'nilai_parameter' => $this->adaConv($tps['bak_cecer']),
						'ket_parameter_bap' => 'Bak penampung ceceran air limbah'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Bak penampung ceceran air limbah']->one,
						'id_parent_2' => $parent['Bak penampung ceceran air limbah']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Kemiringan']) {
				if ($this->valChecker($this->adaConv($tps['kemiringan']),$param['Kemiringan'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Kemiringan'],
						'nilai_parameter' => $this->adaConv($tps['kemiringan']),
						'ket_parameter_bap' => 'Kemiringan'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Kemiringan']->one,
						'id_parent_2' => $parent['Kemiringan']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['SOP pengelolaan dan tanggap darurat']) {
				if ($this->valChecker($this->adaConv($tps['sop_darurat']),$param['SOP pengelolaan dan tanggap darurat'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['SOP pengelolaan dan tanggap darurat'],
						'nilai_parameter' => $this->adaConv($tps['sop_darurat']),
						'ket_parameter_bap' => 'SOP pengelolaan dan tanggap darurat'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['SOP pengelolaan dan tanggap darurat']->one,
						'id_parent_2' => $parent['SOP pengelolaan dan tanggap darurat']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Log book']) {
				if ($this->valChecker($this->adaConv($tps['log_book']),$param['Log book'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Log book'],
						'nilai_parameter' => $this->adaConv($tps['log_book']),
						'ket_parameter_bap' => 'Log book'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Log book']->one,
						'id_parent_2' => $parent['Log book']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['APAR']) {
				if ($this->valChecker($this->adaConv($tps['apar_p3k']),$param['APAR'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['APAR'],
						'nilai_parameter' => $this->adaConv($tps['apar_p3k']),
						'ket_parameter_bap' => 'APAR'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['APAR']->one,
						'id_parent_2' => $parent['APAR']->two
						);
					$item_teguran[] = $item;
				}
			}
			
			if ($stat['Kotak P3K']) {
				if ($this->valChecker($this->adaConv($tps['p3k']),$param['Kotak P3K'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Kotak P3K'],
						'nilai_parameter' => $this->adaConv($tps['p3k']),
						'ket_parameter_bap' => 'Kotak P3K'.' TPS '.$t,
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Kotak P3K']->one,
						'id_parent_2' => $parent['Kotak P3K']->two
						);
					$item_teguran[] = $item;
				}
			}

			$t = $t + 1;
		}

		if ($stat['Penyimpanan']) {
			if ($this->valChecker($pencemaran_pb3['b3_penyimpanan'],$param['Penyimpanan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Penyimpanan'],
					'nilai_parameter' => $pencemaran_pb3['b3_penyimpanan'],
					'ket_parameter_bap' => 'Penyimpanan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Penyimpanan']->one,
					'id_parent_2' => $parent['Penyimpanan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Neraca Limbah B3']) {
			if ($this->valChecker($this->adaConv($pencemaran_pb3['b3_neraca']),$param['Neraca Limbah B3'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Neraca Limbah B3'],
					'nilai_parameter' => $this->adaConv($pencemaran_pb3['b3_neraca']),
					'ket_parameter_bap' => 'Neraca Limbah B3',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Neraca Limbah B3']->one,
					'id_parent_2' => $parent['Neraca Limbah B3']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Manifest']) {
			if ($this->valChecker($this->adaConv($pencemaran_pb3['b3_manifest']),$param['Manifest'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Manifest'],
					'nilai_parameter' => $this->adaConv($pencemaran_pb3['b3_manifest']),
					'ket_parameter_bap' => 'Manifest',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Manifest']->one,
					'id_parent_2' => $parent['Manifest']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Pelaporan Neraca dan Manifest']) {
			if ($this->valChecker($pencemaran_pb3['b3_lapor_nm'],$param['Pelaporan Neraca dan Manifest'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Pelaporan Neraca dan Manifest'],
					'nilai_parameter' => $pencemaran_pb3['b3_lapor_nm'],
					'ket_parameter_bap' => 'Pelaporan Neraca dan Manifest',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Pelaporan Neraca dan Manifest']->one,
					'id_parent_2' => $parent['Pelaporan Neraca dan Manifest']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Pengelolaan']) {
			if (strcasecmp($pencemaran_pb3['padat_kelola'],$param['Pengelolaan'])==0) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Pengelolaan'],
					'nilai_parameter' => $pencemaran_pb3['padat_kelola'],
					'ket_parameter_bap' => 'Pengelolaan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Pengelolaan']->one,
					'id_parent_2' => $parent['Pengelolaan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Lain - Lain']) {
			if (!empty($bap['catatan']) ) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => ' - ',
					'nilai_parameter' => $bap['catatan'],
					'ket_parameter_bap' => 'Lain - Lain',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Lain - Lain']->one,
					'id_parent_2' => $parent['Lain - Lain']->two
					);
				$item_teguran[] = $item;
			}
		}
		
		if ($action=='update') {
			# history_item_teguran_bap update procedur
			$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);
		}

		foreach ($item_teguran as $item) {
			#insert history_item_teguran_bap
			$this->bapDao->insert_getStat('history_item_teguran_bap', $item);
		}

        /*update status is_compared to 1*/
        $this->bapDao->updateIsCompared($id_bap);

		return count($item_teguran);
	}

	public function getPelanggaran() {
		$id_bap = $this->input->post('id_bap');

		$data['industri'] = $this->input->post('industri');
		$data['tgl'] = $this->input->post('tgl');
		$data['teguran'] = $this->bapDao->get_list_by_id("history_item_teguran_bap", "id_bap", $id_bap);

	    echo json_encode($data);
		exit();
	}


	# function to check equality of value (string) (case-insensitive)
	# return FALSE if value equals
	public function valChecker($str1, $str2) {
		if (strcasecmp($str1, $str2) == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	# function to convert string to boolean
	private function boolConverter($value) {
		if (strcasecmp($value, "ada") == 0 || strcasecmp($value, "ya") == 0 || strcasecmp($value, "sudah") == 0) {
			return true;
		} else if (strcasecmp($value, "tidak ada") == 0 || strcasecmp($value, "tidak") == 0 || strcasecmp($value, "belum") == 0) {
			return false;
		} else if (strcasecmp($value, "") == 0) {
			return NULL;
		} else {return 3;}
	}

    # function to convert boolean to string (Ada / Tidak Ada)
	private function adaConv($value) {
        if ($value == true) {
            return "Ada";
        } else {
            return "Tidak Ada";
        }
    }

    # function to convert boolean to string (Ya / Tidak)
    private function yaConv($value) {
        if ($value == true) {
            return "Ya";
        } else {
            return "Tidak";
        }
    }

    function tes () {
    	$a = $this->bapDao->get_all_data_umum('bap_hotel');

    	foreach ($a as $key => $value) {
    		echo $value;
    		foreach ($value as $x => $y) {
    			echo $x . ': ' . $y . '<br >';
    		}
    	}
    }

    public function exit_check_status($id_bap) {
        $this->bapDao->collectStatus($id_bap, "bap_tambang");
        redirect("backend/bap_tambang");
    }

    public function save_part() {
        $stat['status'] = false;

        $posted = $this->input->post('data');
        $ids = $this->input->post('ids');
        parse_str($posted, $data);

        if(!empty($ids)) {
            if ($this->input->post('dest') == "datum") {
                $stat['id'] = $this->upsert_bap($data, $ids);
                $stat['status'] = true;
            } elseif ($this->input->post('dest') == "pencemaran_air") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_air($data, $ids);
                    $stat['status'] = true;
                }
            } elseif ($this->input->post('dest') == "pencemaran_udara") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_udara($data, $ids);
                    $stat['status'] = true;
                }
            } elseif ($this->input->post('dest') == "pencemaran_b3") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_b3($data, $ids);
                    $stat['status'] = true;
                }
            }
        }

        echo json_encode($stat);
    }

    public function upsert_bap($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # data bap
        $bap = array(
            'id_industri' => $data['id_industri'],
            'id_pegawai' => $data['id_pegawai'],
            'bap_tgl' => date("Y-m-d", strtotime($data['bap_tgl'])),
            'bap_jam' => $data['bap_jam'],
            'p2_nama' => $data['p2_nama'],
            'p2_jabatan' => $data['p2_jabatan'],
            'p2_telp' => $data['p2_telp'],
            'dok_lingk' => $data['dok_lingk'],
            'dok_lingk_jenis' => $data['dok_lingk_jenis'],
            'dok_lingk_tahun' => $data['dok_lingk_tahun'],
            'izin_lingk' => $data['izin_lingk'],
            'izin_lingk_tahun' => $data['izin_lingk_tahun'],
            'tgl_pembuatan' => $datenow,
            'dibuat_oleh' => $user,
            'catatan' => $data['catatan']
        );

        # array for table bap_tambang
        $bap_tambang = array(
            'id_bap' => $ids['id_bap'],
            'jumlah_karyawan' => @$data['jumlah_karyawan'],
            'jenis_tambang' => @$data['jenis_tambang'],
            'izin_usaha' => @$data['izin_usaha'],
        );

        # data petugas bap
        $id_petugas_bap = isset($data['id_petugas_bap_list']) ? $data['id_petugas_bap_list'] : null;

        if($ids['id_bap']=="") {
            $id_bap = $this->bapDao->insert_getID('bap',$bap);

            $bap_tambang['id_bap'] = $id_bap;
            $id_bap_tambang = $this->bapDao->insert_getID('bap_tambang', $bap_tambang);

            if ($id_petugas_bap[0] != null) {
                $datenow = date('Y-m-d');

                foreach ($id_petugas_bap as $idpbap) {
                    $petugas_bap = array(
                        'id_bap' => $id_bap,
                        'id_pegawai' => $idpbap,
                        'tgl_pembuatan' => $datenow,
                        'dibuat_oleh' => $user
                    );

                    #insert petugas_bap
                    $this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
                }
            }

            $ret_ids['id_bap'] = $id_bap;
            $ret_ids['id_bap_tambang'] = $id_bap_tambang;
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;
            $this->bapDao->edit('bap', 'id_bap', $ids['id_bap'], $bap);
            $this->bapDao->edit('bap_tambang', 'id_bap_tambang', $ids['id_bap_tambang'], $bap_tambang);

            $this->bapDao->delete('petugas_bap', 'id_bap', $ids['id_bap']);

            if ($id_petugas_bap[0] != null) {
                $datenow = date('Y-m-d');

                foreach ($id_petugas_bap as $idpbap) {
                    $petugas_bap = array(
                        'id_bap' => $ids['id_bap'],
                        'id_pegawai' => $idpbap,
                        'tgl_pembuatan' => $datenow,
                        'dibuat_oleh' => $user
                    );

                    #insert petugas_bap
                    $this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
                }
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_air($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # sumber limbah
        $limb_sumber = isset($data['limb_sumber']) ? $data['limb_sumber'] : "";
        if (!empty($limb_sumber )) {
            $temp = '';
            foreach ($limb_sumber as $key) {
                $temp .= $key . ", ";
            }

            $finlim_sumber = rtrim($temp, ', ');
        } else { $finlim_sumber = ""; }

        # array for pencemaran_air
        $pencemaran_air = array(
            'id_bap' => $ids['id_bap'],
            'ambil_air_tanah' => @$data['ambil_air_tanah'],
            'ambil_air_tanah_izin' => @$data['ambil_air_tanah_izin'],
            'ambil_air_permukaan' => @$data['ambil_air_permukaan'],
            'ambil_air_permukaan_izin' => @$data['ambil_air_permukaan_izin'],
            'ambil_air_pdam' => @$data['ambil_air_pdam'],
            'ambil_air_lain' => @$data['ambil_air_lain'],
            'limb_sumber' => $finlim_sumber,
            'bdn_terima' => @$data['bdn_terima'],
            'sarana_olah_limbah' => @$data['sarana_olah_limbah'],
            'sarana_jenis' => @$data['sarana_jenis'],
            'sarana_kapasitas' => @$data['sarana_kapasitas'],
            'koord_outlet_derajat_s' => @$data['koord_outlet_derajat_s'],
            'koord_outlet_jam_s' => @$data['koord_outlet_jam_s'],
            'koord_outlet_menit_s' => @$data['koord_outlet_menit_s'],
            'koord_outlet_derajat_e' => @$data['koord_outlet_derajat_e'],
            'koord_outlet_jam_e' => @$data['koord_outlet_jam_e'],
            'koord_outlet_menit_e' => @$data['koord_outlet_menit_e'],
            'lain_lain' => @$data['lain_lain_pa']
        );

        if($ids['id_pencemaran_air']=="") {
            $id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = $id_penc_air;
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            $this->bapDao->edit('pencemaran_air', 'id_penc_air', $ids['id_pencemaran_air'], $pencemaran_air);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = $ids['id_pencemaran_air'];
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_udara($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # array for pencemaran_udara
        $pencemaran_udara = array(
            'id_bap' => $ids['id_bap'],
            'pelaporan_ua_ue' => @$data['pelaporan_ua_ue'],
            'lain_lain' => @$data['lain_lain_pu']
        );

        if($ids['id_pencemaran_udara']=="") {
            # insert pencemaran_udara
            $id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);

            # collecting data for uji_ambien
            $cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
            $uji_ambien_list = array();

            foreach ($cols as $i => $val) {
                $uji_ambien = array(
                    'id_penc_udara' => $id_penc_udara,
                    'lokasi' => $val,
                    'uji_kualitas' => @$data['uji_kualitas'.$i],
                    'period' => @$data['period'.$i],
                    'lab' => @$data['lab'.$i],
                    'bm_pemenuhan' => @$data['bm_pemenuhan'.$i],
                    'bm_param' => @$data['bm_param'.$i],
                    'bm_period' => @$data['bm_period'.$i]
                );

                $uji_ambien_list[] = $uji_ambien;

                #insert uji_ambien
                $this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = $id_penc_udara;
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            # edit pencemaran_udara
            $this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $ids['id_pencemaran_udara'], $pencemaran_udara);

            # collecting data for uji_ambien
            $cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
            $uji_ambien_list = array();

            for($i=1; $i<=3; $i++){
                $id_uji_ambien[$i] = @$data['id_uji_ambien'.$i];
            }

            $this->bapDao->delete('uji_ambien', 'id_penc_udara', $ids['id_pencemaran_udara']);

            $i = 1;
            foreach ($cols as $i => $val) {
                $uji_ambien = array(
                    'id_penc_udara' => $ids['id_pencemaran_udara'],
                    'lokasi' => $val,
                    'uji_kualitas' => @$data['uji_kualitas'.$i],
                    'period' => @$data['period'.$i],
                    'lab' => @$data['lab'.$i],
                    'bm_pemenuhan' => @$data['bm_pemenuhan'.$i],
                    'bm_param' => @$data['bm_param'.$i],
                    'bm_period' => @$data['bm_period'.$i]
                );

                $uji_ambien_list[] = $uji_ambien;

                #insert uji_ambien
                //$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien[$i], $uji_ambien);
                $this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
                $i++;
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = $ids['id_pencemaran_udara'];
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_b3($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        $padat_jenis_tmp = @$data['padat_jenis'];
        $empty = '';
        foreach ($padat_jenis_tmp as $key) {
            $empty .= $key.', ';
        }
        $padat_jenis = rtrim($empty, ', ');

        $padat_jumlah_tmp = @$data['padat_jumlah'];
        $empty = '';
        foreach ($padat_jumlah_tmp as $key) {
            $empty .= $key.', ';
        }
        $padat_jumlah = rtrim($empty, ', ');

        # data pencemaran_pb3
        $pencemaran_pb3 = array(
            'id_bap' => $ids['id_bap'],
            'b3_penyimpanan' => @$data['b3_penyimpanan'],
            'b3_neraca' => @$data['b3_neraca'],
            'b3_manifest' => @$data['b3_manifest'],
            'b3_lapor_nm' => @$data['b3_lapor_nm'],
            'padat_jenis' => $padat_jenis,
            'padat_jumlah' => $padat_jumlah,
            'padat_sarana_tong' => @$data['padat_sarana_tong'],
            'padat_sarana_tps' => @$data['padat_sarana_tps'],

            'lain_lain' => $data['lain_lain_pb3']
        );

        if (@$data['padat_kelola'] != 'Lainnya') {
            $pencemaran_pb3['padat_kelola'] = @$data['padat_kelola'];
        } else {
            $pencemaran_pb3['padat_kelola'] = @$data['padat_kelola_lain'];
        }

        # data jenis_limbah_b3 yang ditimbulkan
        $jenis_limbah_baru = isset($data['jenis_limbah']) ? $data['jenis_limbah'] : array("");
        $jumlah = isset($data['jumlah']) ? $data['jumlah'] : array();
        $pengelolaan = isset($data['pengelolaan']) ? $data['pengelolaan'] : array();
        $pihak_ke3 = isset($data['pihak_ke3']) ? $data['pihak_ke3'] : array();

        $jenis_limbah_b3_list = array();

        # data tps_b3
        $cols = array('1', '2', '3', '4');
        $tps_b3_list = array();

        if ($ids['id_plb3']=="") {

            # insert pencemaran_pb3
            $id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

            # insert jenis_limbah_b3
            if (!empty($jenis_limbah_baru)) {
                for ($i = 0; $i < count($jenis_limbah_baru); $i++) {
                    $jenis_limbah_b3 = array(
                        'id_penc_pb3' => $id_penc_pb3,
                        'jenis' => $jenis_limbah_baru[$i],
                        'jumlah' => $jumlah[$i],
                        'pengelolaan' => $pengelolaan[$i],
                        'pihak_ke3' => $pihak_ke3[$i]
                    );

                    $jenis_limbah_b3_list[] = $jenis_limbah_b3;

                    $this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
                }
            }

            # insert tps_b3
            foreach($cols as $i) {

                if(@$data['tb_izin'.$i] == 'Ada'){
                    $izin_no = @$data['tb_izin_no'.$i];
                    $izin_tgl = date("Y-m-d", strtotime(@$data['tb_izin_tgl'.$i]));
                } else {
                    $izin_no = null;
                    $izin_tgl = null;
                }

                $tps_b3 = array(
                    'id_penc_pb3' => $id_penc_pb3,
                    'izin' => $this->boolConverter(@$data['tb_izin'.$i]),
                    'izin_no' => $izin_no,
                    'izin_tgl' => $izin_tgl,
                    'jenis' => @$data['tb_jenis'.$i],
                    'lama_spn' => serialize(array('jenis_limbah' => isset($data['jenis_limbah_lama_spn']) ? @$data['jenis_limbah_lama_spn'] : "", 'tps' => isset($data['tb_lama_spn_'.$i]) ? @$data['tb_lama_spn_'.$i] : "")),
                    'koord_derajat_s' => @$data['koord_derajat_s'.$i],
                    'koord_jam_s' => @$data['koord_jam_s'.$i],
                    'koord_menit_s' => @$data['koord_menit_s'.$i],
                    'koord_derajat_e' => @$data['koord_derajat_e'.$i],
                    'koord_jam_e' => @$data['koord_jam_e'.$i],
                    'koord_menit_e' => @$data['koord_menit_e'.$i],
                    'ukuran' => @$data['tb_ukuran'.$i],
                    'ppn_nama_koord' => $this->boolConverter(@$data['tb_ppn_nama_koord'.$i]),
                    'simbol_label' => $this->boolConverter(@$data['tb_simbol_label'.$i]),
                    'saluran_cecer' => $this->boolConverter(@$data['tb_saluran_cecer'.$i]),
                    'bak_cecer' => $this->boolConverter(@$data['tb_bak_cecer'.$i]),
                    'kemiringan' => $this->boolConverter(@$data['tb_kemiringan'.$i]),
                    'sop_darurat' => $this->boolConverter(@$data['tb_sop_darurat'.$i]),
                    'log_book' => $this->boolConverter(@$data['tb_log_book'.$i]),
                    'apar_p3k' => $this->boolConverter(@$data['tb_apar_p3k'.$i]),
                    'p3k' => $this->boolConverter(@$data['tb_p3k'.$i])
                );
                if(@$data['tb_izin'.$i] != 3) {
                    $tps_b3_list[] = $tps_b3;
                } else {
                    $tps_b3['lama_spn_lumpur'] = NULL;
                    $tps_b3['lama_spn_batu_bara'] = NULL;
                    $tps_b3['lama_spn_pelumas'] = NULL;
                    $tps_b3['lama_spn_aki_bekas'] = NULL;
                    $tps_b3['lama_spn_ltl'] = NULL;
                    $tps_b3['lama_spn_kmsn_b3'] = NULL;
                    $tps_b3['lama_spn_majun_b3'] = NULL;
                    $tps_b3['lama_spn_limbah_elektro'] = NULL;
                    $tps_b3['lama_spn_limbah_kimia'] = NULL;
                    $tps_b3['lama_spn_lain_ket'] = NULL;
                    $tps_b3['ukuran'] = NULL;
                    $tps_b3['izin_no'] = NULL;
                    $tps_b3['izin_tgl'] = NULL;
                    $tps_b3['jenis'] = NULL;
                    $tps_b3['koord_derajat_s'] = null;
                    $tps_b3['koord_jam_s'] = null;
                    $tps_b3['koord_menit_s'] = null;
                    $tps_b3['koord_derajat_e'] = null;
                    $tps_b3['koord_jam_e'] = null;
                    $tps_b3['koord_menit_e'] = null;
                    $tps_b3['ppn_nama_koord'] = NULL;
                    $tps_b3['simbol_label'] = NULL;
                    $tps_b3['saluran_cecer'] = NULL;
                    $tps_b3['bak_cecer'] = NULL;
                    $tps_b3['kemiringan'] = NULL;
                    $tps_b3['sop_darurat'] = NULL;
                    $tps_b3['log_book'] = NULL;
                    $tps_b3['apar_p3k'] = NULL;
                    $tps_b3['p3k'] = NULL;
                }

                $this->bapDao->insert_getStat('tps_b3', $tps_b3);
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = $id_penc_pb3;
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            #insert pencemaran_pb3
            $this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $ids['id_plb3'], $pencemaran_pb3);

            $this->bapDao->delete('jenis_limbah_b3', 'id_penc_pb3', $ids['id_plb3']);

            for ($i=0; $i<count($jenis_limbah_baru); $i++) {
                $jenis_limbah_b3 = array(
                    'id_penc_pb3' => $ids['id_plb3'],
                    'jenis' => $jenis_limbah_baru[$i],
                    'jumlah' => $jumlah[$i],
                    'pengelolaan' => $pengelolaan[$i],
                    'pihak_ke3' => $pihak_ke3[$i]
                );

                $jenis_limbah_b3_list[] = $jenis_limbah_b3;

                #insert jenis_limbah_b3
                $this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
            }

            $this->bapDao->delete('tps_b3', 'id_penc_pb3', $ids['id_plb3']);

            $cols = array('1', '2', '3', '4');
            $tps_b3_list = array();
            foreach($cols as $i) {

                if(@$data['tb_izin'.$i] == 'Ada'){
                    $izin_no = @$data['tb_izin_no'.$i];
                    $izin_tgl = date("Y-m-d", strtotime(@$data['tb_izin_tgl'.$i]));
                } else {
                    $izin_no = null;
                    $izin_tgl = null;
                }

                $tps_b3 = array(
                    'id_penc_pb3' => $ids['id_plb3'],
                    'izin' => $this->boolConverter(@$data['tb_izin'.$i]),
                    'izin_no' => $izin_no,
                    'izin_tgl' => $izin_tgl,
                    'jenis' => @$data['tb_jenis'.$i],
                    //'lama_spn' => serialize(array('jenis_limbah' => @$data['jenis_limbah_lama_spn'], 'tps' => @$data['tb_lama_spn_'.$i])),
                    'lama_spn' => serialize(array('jenis_limbah' => isset($data['jenis_limbah_lama_spn']) ? @$data['jenis_limbah_lama_spn'] : "", 'tps' => isset($data['tb_lama_spn_'.$i]) ? @$data['tb_lama_spn_'.$i] : "")),
                    'koord_derajat_s' => @$data['koord_derajat_s'.$i],
                    'koord_jam_s' => @$data['koord_jam_s'.$i],
                    'koord_menit_s' => @$data['koord_menit_s'.$i],
                    'koord_derajat_e' => @$data['koord_derajat_e'.$i],
                    'koord_jam_e' => @$data['koord_jam_e'.$i],
                    'koord_menit_e' => @$data['koord_menit_e'.$i],
                    'ukuran' => @$data['tb_ukuran'.$i],
                    'ppn_nama_koord' => $this->boolConverter(@$data['tb_ppn_nama_koord'.$i]),
                    'simbol_label' => $this->boolConverter(@$data['tb_simbol_label'.$i]),
                    'saluran_cecer' => $this->boolConverter(@$data['tb_saluran_cecer'.$i]),
                    'bak_cecer' => $this->boolConverter(@$data['tb_bak_cecer'.$i]),
                    'kemiringan' => $this->boolConverter(@$data['tb_kemiringan'.$i]),
                    'sop_darurat' => $this->boolConverter(@$data['tb_sop_darurat'.$i]),
                    'log_book' => $this->boolConverter(@$data['tb_log_book'.$i]),
                    'apar_p3k' => $this->boolConverter(@$data['tb_apar_p3k'.$i]),
                    'p3k' => $this->boolConverter(@$data['tb_p3k'.$i])
                );
                if(@$data['tb_izin'.$i] != 3) {
                    $tps_b3_list[] = $tps_b3;
                } else {
                    $tps_b3['lama_spn_lumpur'] = NULL;
                    $tps_b3['lama_spn_batu_bara'] = NULL;
                    $tps_b3['lama_spn_pelumas'] = NULL;
                    $tps_b3['lama_spn_aki_bekas'] = NULL;
                    $tps_b3['lama_spn_ltl'] = NULL;
                    $tps_b3['lama_spn_kmsn_b3'] = NULL;
                    $tps_b3['lama_spn_majun_b3'] = NULL;
                    $tps_b3['lama_spn_limbah_elektro'] = NULL;
                    $tps_b3['lama_spn_limbah_kimia'] = NULL;
                    $tps_b3['lama_spn_lain_ket'] = NULL;
                    $tps_b3['ukuran'] = NULL;
                    $tps_b3['izin_no'] = NULL;
                    $tps_b3['izin_tgl'] = NULL;
                    $tps_b3['jenis'] = NULL;
                    $tps_b3['koord_derajat_s'] = null;
                    $tps_b3['koord_jam_s'] = null;
                    $tps_b3['koord_menit_s'] = null;
                    $tps_b3['koord_derajat_e'] = null;
                    $tps_b3['koord_jam_e'] = null;
                    $tps_b3['koord_menit_e'] = null;
                    $tps_b3['ppn_nama_koord'] = NULL;
                    $tps_b3['simbol_label'] = NULL;
                    $tps_b3['saluran_cecer'] = NULL;
                    $tps_b3['bak_cecer'] = NULL;
                    $tps_b3['kemiringan'] = NULL;
                    $tps_b3['sop_darurat'] = NULL;
                    $tps_b3['log_book'] = NULL;
                    $tps_b3['apar_p3k'] = NULL;
                    $tps_b3['p3k'] = NULL;
                }

                $this->bapDao->insert_getStat('tps_b3', $tps_b3);
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_tambang'] = $ids['id_bap_tambang'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = $ids['id_plb3'];
        }

        return $ret_ids;
    }

    public function getComparableStatus() {
        $data = $this->bapDao->collectStatus($this->input->post("id_bap"), "bap_tambang");
        echo json_encode($data);
    }

    public function newCompare() {
        date_default_timezone_set("Asia/Jakarta");

        /* ----------------------------------------------- */
        /* BEGIN COMPARE BAP */
        /* ----------------------------------------------- */
        /*data umum & bap */
        $data['bap'] = $this->bapDao->select_table_array("bap", "id_bap", $this->input->post("id"), FALSE);

        /*pencemaran air*/
        $data['pencemaran_air'] = $this->bapDao->select_table_array("pencemaran_air", "id_bap", $this->input->post("id"), FALSE);

        /*pencemaran_udara*/
        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $this->input->post("id"));
        $data['uji_ambien'] = $this->bapDao->select_table_array("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara, TRUE);

        /*pencemaran pb3*/
        $data['pencemaran_pb3'] = $this->bapDao->select_table_array("pencemaran_pb3", "id_bap", $this->input->post("id"), FALSE);
        $data['jenis_limbah_b3'] = $this->bapDao->select_table_array("jenis_limbah_b3", "id_penc_pb3", $data['pencemaran_pb3']['id_penc_pb3'], TRUE);
        $tps_b3 = $this->bapDao->select_table_array("tps_b3", "id_penc_pb3", $data['pencemaran_pb3']['id_penc_pb3'], TRUE);
        foreach ($tps_b3 as $row) {
            if ($row['izin']!=3) {
                $data['tps_b3'][] = $row;
            }
        }

        /*additional data*/
        $data['datenow'] = date("Y-m-d H:i:s:");
        $data['user'] = $this->tank_auth->get_personname();
        //echo "<pre>"; print_r($data); die();

        $total_error = $this->compareBap($data, 'update');

        if ($total_error > 0) {
            $this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP: '.$total_error.' item'));
        } else {
            $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Tidak ditemukan pelanggaran'));
        }

        /* ----------------------------------------------- */
        /* END COMPARE BAP */
        /* ----------------------------------------------- */

        redirect('backend/bap_tambang');
    }

    public function partialEdit($id_bap, $status, $last_insert) {
        // ---------------------------------- DATA ACUAN TJOY ---------------------------- //

        /*param for nullify all the ids in view*/
        if ($last_insert) {
            $data['last_insert'] = $last_insert;
        }

        $data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
        $data['ipal'] = $this->m_data_master->get_dropdown("sistem_ipal");
        $data['bb'] = $this->m_data_master->get_dropdown("jenis_bahan_bakar");
        $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
        $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
        $data['ipal_unit'] = $this->m_data_master->get_dropdown("unit_ipal");
        $data['jenis_pengujian'] = $this->m_data_master->get_dropdown("jenis_pengujian");
        $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");
        $data['jenis_limbah_b3_data_master'] = $this->m_data_master->get_dropdown("jenis_limbah");
        $pihak_ke3 = $this->bapDao->select_table('pengolah_limbah');
        $data['pihak_ke3'] = $pihak_ke3;
        $lab = $this->bapDao->select_table('laboratorium');
        $data['lab'] = $lab;

        // ---------------------------------- DATA ACUAN TJOY ---------------------------- //

        /*static private variable*/
        $data['day_id'] = $this->day_id;
        $data['arr_padat_kelola'] = $this->padat_kelola;
        $data['jenis_alat_ukur'] = $this->jenis_alat_ukur;
        $data['bool'] = $this->bool;
        $data['periode_uji'] = $this->periode_uji;
        $data['emboil'] = $this->emboil;
        $data['jenis_perusahaan'] = $this->jenis_perusahaan;


        if(!empty($status['bap']) AND !empty($status['bap_tambang'])) { // data umum and bap

            $bap_tambang = $this->bapDao->get_obj_by_id("bap_tambang", "id_bap", $id_bap);

            $data['bap'] = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);
            $data['bap_tambang'] = $bap_tambang;
            $data['industri'] = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);
            $data['pegawai'] = $this->empDao->get_all_employee();
            $data['petugas_bap'] = $this->bapDao->get_petugas_bap($id_bap);
        }

        if(!empty($status['pencemaran_air'])) { // pencemaran air
            $data['pencemaran_air'] = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);
            $data['bahan_kimia_ipal'] = $this->bapDao->get_list_by_id("bahan_kimia_ipal", "id_penc_air", $data['pencemaran_air']->id_penc_air);
        }

        if(!empty($status['pencemaran_udara'])) { // pencemaran udara
            $data['pencemaran_udara'] = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
            $data['uji_ambien'] = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $data['pencemaran_udara']->id_penc_udara);
        }

        if(!empty($status['pencemaran_pb3'])) { // pencemaran b3
            $data['pencemaran_pb3'] = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
        }

        //echo "<pre>"; print_r($data['bool']); die();

        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/bap_tambang_partial/v_bap_tambang_partial', $data);
    }
}
