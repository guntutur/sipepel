<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Rekapitulasi Masa Kontrak Izin&nbsp;&nbsp;<small>Rekapitulasi Masa Kontrak Izin</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Rekapitulasi</li>
                <li class="active">Masa Kontrak Izin</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <ol class="breadcrumb default square rsaquo sm">

                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Pilih Jenis Izin :</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control input-sm" id="rekap_type">
                                    <option value="izin_pembuangan">Izin Pembuangan Air Limbah</option>
                                    <option value="izin_tps">Izin TPS Limbah</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            
            </ol>
            
            <div class="the-box ">
                
                <?php echo $this->session->flashdata('msg'); ?>

                 <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th width="20%">Jenis Usaha/Kegiatan</th>
                                <th>Tanggal Izin</th>
                                <th>Berlaku Hingga</th>
                                <th>Status</th>                                  
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
                <!-- END DATA TABLE -->
                <div id="loading" style="display:none;"><div style="background-image: url('<?php echo site_url(); ?>assets/img/loading.gif');background-position: center center;background-repeat: no-repeat;height:50px;"></div></div>

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
    
    var Document = {
        param: {
            dataperpage: 10,
            rekap_type: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/rekap_izin/get_rekap"); ?>',
        search: function() {
            this.param.rekap_type = $('#rekap_type').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                beforeSend: function() { $('#loading').show(); },
                complete: function() { $('#loading').hide(); },
                success: function(result) {
                    console.log(result);
                    $('#pagination').html(result.pagination);
                    $('#start').text(result.total);                 
                    $('#nums').text(result.total);
                    Document.param.numpage = result.numpage;

                    var h = '', ind = {}, no = Document.param.curpage * Document.param.dataperpage;
                    
                    if (result.rekap.length < 1) {
                        h += '<tr><td colspan=\'7\'>Data Tidak Ditemukan</td></tr>';
                    } else {
                        for (var i = 0; i < result.rekap.length; i++) {
                            ind = result.rekap[i];

                            h += '<tr>';
                            h += '<td class="text-center">'+(no+=1)+'</td>';
                            h += '<td><b>'+ind.nama_industri+'</b><br/>';
                            h += '<small>' + ind.alamat + '</small><br/>';
                            h += '<small>Kec. ' + ind.kecamatan + ', Kel. '+ ind.kelurahan +'</small><br/>';
                            h += '</td>';
                            
                            h += '<td>Usaha/Kegiatan: '+ ind.usaha_kegiatan +'<br/>';
                            h += 'Jenis/Tipe/Kelas: '+ ((ind.jenis_industri != null ) ? ind.jenis_industri : '- ');
                            h += '</td>';

                            h += '<td>'+ $(this).format_date(ind.tgl_izin) +'</td>';
                            h += '<td>'+ $(this).format_date(ind.tgl_habis) +'</td>';
                            h += '<td>'+ ind.status +'</td>';
                            h += '<tr>'
                        }
                    }

                    $('#document-data').html(h); 
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        
        $('#rekap_type').change(function() {
            Document.search();
        });

        Document.search();

        window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

    });
</script>