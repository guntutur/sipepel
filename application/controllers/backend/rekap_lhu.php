<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class rekap_lhu extends CI_Controller {

    function __construct() {
        parent::__construct();	
		
        $this->load->model('m_rekap_lhu', '', TRUE);
		
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_rekap_lhu');
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'i.nama_industri' => array('SEARCH', 'OR', $query),
				'b.nama_lab' => array('SEARCH', 'OR', $query),
				'l.laporan_bulan_tahun' => array('SEARCH', 'OR', $query),
			);

     	$total = $this->m_rekap_lhu->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_rekap_lhu->get_all($where, $dataperpage, $start);
	    // print_r($types);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
		
	public function do_upload(){
			
		# config
		$this->load->library('upload');
		$this->upload->initialize(array(			
			"allowed_types" => "doc|DOC|docx|DOCX",
            "upload_path"   => realpath(APPPATH . "../swapantau")					
        ));
		
		if($this->upload->do_multi_upload("files")){
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diunggah'));
		}
		
		echo "error : ". $this->upload->display_errors();
	}
	
}
