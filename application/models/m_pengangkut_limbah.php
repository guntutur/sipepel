<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_pengangkut_limbah extends CI_Model {

	private $table_name			= 'pengangkut_limbah';			
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('i.*, k.ket as nama_kecamatan, l.ket as nama_kelurahan');
		$this->db->from($this->table_name .' as i');
		$this->db->join('kecamatan k', 'k.id_kecamatan = i.id_kecamatan', 'left');
		$this->db->join('kelurahan l', 'l.id_kelurahan = i.id_kelurahan', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("i.badan_hukum", "asc"); 
		$this->db->order_by("i.nama_industri", "asc"); 
		
	

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select('i.*, k.ket as nama_kecamatan, l.ket as nama_kelurahan');
		$this->db->from($this->table_name .' as i');
		$this->db->join('kecamatan k', 'k.id_kecamatan = i.id_kecamatan', 'left');
		$this->db->join('kelurahan l', 'l.id_kelurahan = i.id_kelurahan', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	function get_dropdown() {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("nama_industri", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_industry_by_id($id) {
		$this->db->where('id_industri', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_industry($id) {
		$data = $this->get_industry_by_id($id);
		$this->db->where('id_industri', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_industry($data, $id) {

		$this->db->where("id_industri", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_industry($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function is_duplicate($badan_hukum, $nama_industri) {
		$this->db->where('badan_hukum', $badan_hukum);
		$this->db->where('nama_industri', $nama_industri);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	function search_industry($keyword, $limit=10) {
        $arr = array();
        $this->db->select('id_industri, nama_industri');
        $this->db->from($this->table_name);
        $this->db->like('nama_industri', $keyword);
        $this->db->limit($limit, 0);
        $query = $this->db->get();
        
        $data = $query->result();
        if(count($data) > 0) {
            foreach ($data as $row) {
                $arr['query'] = $keyword;
                $arr['suggestions'][] = array(
                    'value' => $row->nama_industri,
                    'data' => $row->id_industri
                );
            }
        }else {
            $arr['query'] = $keyword;
            $arr['suggestions'] = array();
        }
        
        return $arr;
    }
}
?>