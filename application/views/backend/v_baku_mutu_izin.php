<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAKU MUTU IZIN &nbsp;<font size="6pt;"><small><b><?php echo $title; ?></b></small></font></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
				<li>Izin Pembuangan Limbah B3</li>
                <li class="active">Baku Mutu Izin</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <!-- button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/>   -->

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" onclick="document.location='<?php echo site_url('backend/izin_pembuangan'); ?>';" class="btn btn-sm btn-info"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</button>
                        <button type="button" data-toggle="modal" data-target="#input_detail" onclick="$(this).reset_form_exclude_add('input_detail', true, 'koor_tps');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>                
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
								<th>Jenis Baku Mutu</th>
                                <th>Parameter</th>
                                <th>Baku Mutu</th>
								<th>Satuan</th>  
								<th>Status</th>  
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="7">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start"></span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0,
			id: '<?php echo $this->uri->segment(4); ?>'
        },
        url: '<?php echo base_url("backend/izin_pembuangan/get_detail_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
					var lhu;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var status = (dt.status == 1) ? 'Aktif': 'Tidak Aktif';
						var jenis_lhu = (dt.jenis_baku_mutu != lhu ) ? dt.jenis_baku_mutu : ' ';
						var no_lhu = (dt.jenis_baku_mutu == lhu ) ? (nomer+=1) : nomer = 1;
                        t += '<tr>';
						t += '<td>'+jenis_lhu+'</td>';
						t += '<td class="text-center">'+no_lhu+'</td>';
						t += '<td>'+dt.ket+'</td>';
						t += '<td>'+dt.nilai_baku_mutu+'</td>';
						t += '<td>'+dt.satuan+'</td>';
						t += '<td>'+status+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_baku_mutu_izin+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_baku_mutu_izin+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';   
						lhu = dt.jenis_baku_mutu;						
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        
        $("#form_add").validate({
            rules: {
                ket:{
                    required: true,
                    maxlength: 100
                },
                nilai_baku_mutu: {
                    required: true
                },
				satuan: {
                    required: true
                },
                status: {
                    required: true
                },
				jenis_baku_mutu: {
                    required: true
                }
            },
            messages: {        
                status: {
                    required: "Field belum dipilih."
                },
				jenis_baku_mutu: {
                    required: "Field belum dipilih."
                },
				status: {
                    required: "Field belum dipilih."
                },
                ket: {
                    required: "Masukan nama parameter.",
                    maxlength: "Nama nomer izi maksimal memiliki 100 karakter"
                },
                nilai_baku_mutu: {
                    required: "Masukan Nilai Baku Mutu."
                },
				satuan: {
                    required: "Masukan Satuan Baku Mutu."
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }
	});

	$(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }
		
        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/izin_pembuangan/get_detail'); ?>",
                data: $.param({id_baku_mutu_izin: id}),
                success: function (response) {
                    
                   $('input[id=detail_id]').val(response.id_baku_mutu_izin);
					
					$('select[name=status]').val(response.status);
					$('select[name=jenis_baku_mutu]').val(response.jenis_baku_mutu);
                    $('input[name=satuan]').val(response.satuan);
					$('input[name=ket]').val(response.ket);
					$('input[name=nilai_baku_mutu]').val(response.nilai_baku_mutu);
					
                    $('#input_detail form').attr('action', '<?php echo base_url('/backend/izin_pembuangan/edit_detail') ?>'); //this fails silently
                    $('#input_detail form').get(0).setAttribute('action', '<?php echo base_url('/backend/izin_pembuangan/edit_detail') ?>'); //this works
					
                    $('#input_detail').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        

        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="input_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Baku Mutu Izin </h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/izin_pembuangan/insert_detail') ?>" method="post" id="form_add">
                <div class="modal-body"> 
					 <div class="row">
                        <div class="col-lg-6 form-group ket">
                            <label>Parameter</label>
                            <input type="text" name="ket" id="ket" class="form-control input-sm has-feedback" autofocus />                            
                        </div>
    					<div class="col-lg-3 form-group nilai_baku_mutu">
                            <label>Nilai</label>
                            <input type="text" name="nilai_baku_mutu" id="nilai_baku_mutu" class="form-control input-sm has-feedback" autofocus />                            
                        </div>
						<div class="col-lg-3 form-group satuan">
							<label>Satuan</label>
							<input type="text" name="satuan" id="satuan" class="form-control input-sm has-feedback" autofocus />							
						</div>
                    </div>
					<div class="row">
						<div class="col-lg-6 form-group satuan">
							<label>Jenis Baku Mutu</label>
							<select name="jenis_baku_mutu" id="jenis_baku_mutu" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Jenis Baku Mutu --</option>
								<?php 
									$status = $this->functions->get_jenis_baku_mutu_izin();
									foreach($status as $k => $s) {
										echo '<option value="'.$k.'">'.$s.'</option>';
									}
								?>                      
							</select>			
						</div>
						<div class="col-lg-6 form-group status">
							<label>Status</label>
							<select name="status" id="status" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Status --</option>
								<?php 
									$status = $this->functions->get_status();
									foreach($status as $k => $s) {
										echo '<option value="'.$k.'">'.$s.'</option>';
									}
								?>                      
							</select>							
						</div>
					</div>                                                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="detail_id" name="id" value="<?php echo $this->uri->segment(4)?>" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Baku Mutu Izin</h4>
            </div>
            <form action="<?php echo base_url('/backend/izin_pembuangan/delete_detail'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->