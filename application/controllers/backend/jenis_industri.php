<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class jenis_industri extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_industry_type', '', TRUE);
		$this->load->model('m_business_activity', '', TRUE);
		$this->load->model('m_bap_parameter', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		$usaha_kegiatan = $this->m_business_activity->get_dropdown();
		
		# response paramter              
		$data['usaha_kegiatan'] = $usaha_kegiatan;
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_industry_types', $data);
        $this->load->view('include/footer');
    }
	
	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $par_bap = $this->m_business_activity->get_business_activity_by_id($id);	

        $data['title'] = "JENIS/KELAS/TIPE USAHA KEGIATAN ".$par_bap->ket;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_industry_types', $data);
        $this->load->view('include/footer');
	}
	
    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		
		foreach ($input as $val)
     		$$val = $this->input->post($val); 
			
		$where = "(u.ket LIKE '%". $query ."%'  OR p.ket LIKE '%". $query ."%' ) AND p.id_usaha_kegiatan = ".$id;

     	$total = $this->m_industry_type->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_industry_type->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_industry_type->get_industry_type_by_id($this->input->post('id_jenis_industri')));
		exit();
	}
	
	public function get_by_usaha_kegiatan() {
		echo json_encode($this->m_industry_type->get_it_by_uk($this->input->post('id_usaha_kegiatan')));
		exit();
	}
	
	public function get_by_usaha_kegiatan_merge_data_acuan() {
		echo json_encode($this->m_industry_type->get_id_by_usaha_kegiatan($this->input->post('id_usaha_kegiatan')));
		exit();
	}
	
	public function edit() {

		
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");

		$param = array(				
			'ket' => $this->input->post('description'),
			'status' => $this->input->post('status'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_industry_type->change_industry_type($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		
			
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function register() {

		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");

			$param = array(
				'id_usaha_kegiatan' => $this->input->post('id'),
				'ket' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => '', # $this->tank_auth->get_personname(),
				'tgl_update' => '' # $tgl_update
			);
			
			if($this->m_industry_type->insert_industry_type($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function delete() {
		$this->m_industry_type->delete_industry_type($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function validate() {
		# check data yang duplicate
		if($this->m_industry_type->is_duplicate($this->input->post('description'), $this->input->post('id'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}

}
