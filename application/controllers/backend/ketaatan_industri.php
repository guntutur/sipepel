<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document    : ketaatan_industri.php 
 // Modified on : January 07, 2015 18:04 
 // Modified by : huda.jtk09@gmail.com 
 // Description : Controller for Rekapitulasi Ketaatan Industri 

class ketaatan_industri extends CI_Controller {

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_ketaatan_industri', 'ketDao', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);
        $this->load->model('m_bap', 'bapDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view
    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types
		
		# response paramter     
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_ketaatan_industri');
        $this->load->view('include/footer');
    }


    public function get_rekap() {
        # get parameter pagination
        $input = array('dataperpage', 'year', 'curpage');
        foreach ($input as $val) {
            $$val = $this->input->post($val); 
        }

        $total = $this->ketDao->get_all_industri_size();

        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $rekap = $this->ketDao->get_all_industri($dataperpage, $start);

        $rekap = $this->fill_ketaatan($rekap, $year);

        $data = array(
            'rekap' => $rekap,
            'pagination' => $this->functions->create_links($npage, $curpage, 3),
            'numpage' => $npage - 1,
            'total' => $total
        );

        echo json_encode($data);
        exit();
    }

    private function fill_ketaatan($rekap, $year) {

        # load model
        $this->load->model('m_lhu_air_limbah');
        $this->load->model('m_debit_air_limbah');
        $this->load->model('m_neraca_b3');
        $this->load->model('m_manifest');
        $this->load->model('m_lhu_udara_emisi');       

        foreach ($rekap as $val) {           
                 
            if ($this->ketDao->get_bap_count($val->id_industri, $year) > 0) {
                // get pelanggaran bap (air, udara, b3 teknis)
                $data_pelanggaran_bap = strtolower($this->ketDao->get_pelanggaran_industri($val->id_industri, $year)->pelanggaran);
                $data_perbaikan_bap = $this->ketDao->get_perbaikan($val->id_industri, $year);

                $val->airadm = 'Taat';
                $val->udaraadm = 'Taat';
                $val->b3adm = 'Taat';

                $val->airteknis = 'Taat';
                $val->udarateknis = 'Taat';
                $val->b3teknis = 'Taat';

                $val->akumulasi = 'Taat';
                $val->keterangan = ' - ';
                
                if (strpos($data_pelanggaran_bap,'air') !== false) {
                    $val->airteknis = '<font color="red">Tidak Taat</font>';
                    // $val->akumulasi = '<font color="red">Tidak Taat</font>';
                }
                if (strpos($data_pelanggaran_bap,'udara') !== false) {
                    $val->udarateknis = '<font color="red">Tidak Taat</font>';
                    // $val->akumulasi = '<font color="red">Tidak Taat</font>';
                }
                if (strpos($data_pelanggaran_bap,'b3') !== false || strpos($data_pelanggaran_bap,'padat') !== false) {
                    $val->b3teknis = '<font color="red">Tidak Taat</font>';
                    // $val->akumulasi = '<font color="red">Tidak Taat</font>';
                }

                if (isset($data_perbaikan_bap)) {
                    $ket = "Item yang sudah diperbaiki :";

                    foreach ($data_perbaikan_bap as $v) {
                        $ket = $ket."<li>".$v->ket_parameter_bap."</li>";
                    }

                    $val->keterangan = $ket;
                }

                $ket = '';

                # administarsi  

                # --------------------------- syarat adm air ---------------------------------------------------------

                $last_id_bap = $this->indDao->get_data_bap($year, $val->id_industri);
                if(count($last_id_bap) > 0) {
                    $bap = $this->get_bap($last_id_bap[0]->bap_terakhir);                    

                    ## dokumen lingkungan
                    $dok_ling = ($bap['bap']->dok_lingk == 0) ? '<font color="red">Tidak Taat</font>' : 'Taat';
                    ## izin pembuangan air limbah
                    $izin_limbah = ($bap['izin_limbah'] != NULL) ? 'Taat' : '<font color="red">Tidak Taat</font>';

                }

                # jumlah dokumen air limbah                
                $where = 'id_industri = '.$val->id_industri.' and jenis_lhu = "LHU Air Limbah" and substring(tgl_pembuatan, 4, 4) = "'.date('Y').'"';
                $sum_air = (count($this->m_lhu_air_limbah->get_detail($where)) > 0) ? count($this->m_lhu_air_limbah->get_detail($where)) : 0;  

                $where = 'id_industri = '.$val->id_industri.' and substring(laporan_bulan, 4, 4) = "'.date('Y').'"';
                $sum_catatan = (count($this->m_debit_air_limbah->get_debit_air_limbah($where)) > 0) ? count($this->m_debit_air_limbah->get_debit_air_limbah($where)) : 0;
                
                # syarat adm
                if($dok_ling == 'Taat' and $izin_limbah == 'Taat' and ($sum_air >= 9 and $sum_catatan >= 9)){
                    $val->airadm = 'Taat';
                }else{
                    $val->airadm = '<font color="red">Tidak Taat</font>';
                }                

                # ------------------------------------------------------------------------------------------------------

                # --------------------------- syarat adm udara ---------------------------------------------------------

                $where = 'id_industri = '.$val->id_industri.' and jenis_lhu = "LHU Udara Emisi" and substring(tgl_pembuatan, 4, 4) = "'.date('Y').'"';
                $sum_emisi = (count($this->m_lhu_air_limbah->get_detail($where)) > 0) ? count($this->m_lhu_air_limbah->get_detail($where)) : 0;

                if($dok_ling == 'Taat' and ($sum_emisi >= 2)) {
                    $val->udaraadm = 'Taat';
                }else{
                    $val->udaraadm = '<font color="red">Tidak Taat</font>';
                }

                # ------------------------------------------------------------------------------------------------------

                # --------------------------- syarat b3 ---------------------------------------------------------

                $where = 'id_penghasil_limbah = '.$val->id_industri.' and substring(tgl, 1, 4) = "'.date('Y').'"';
                $sum_manifest = (count($this->m_manifest->get_detail($where)) > 0) ? count($this->m_manifest->get_detail($where)) : 0;

                $adm_manifest = ($sum_manifest >= 3) ? 'Taat' : '{\cf6 Tidak Taat}';


                $where = 'id_industri = '.$val->id_industri.' and substring(periode_waktu, 1, 4) = "'.date('Y').'"';
                $sum_neraca = (count($this->m_neraca_b3->get_detail($where)) > 0) ? count($this->m_neraca_b3->get_detail($where)) : 0;

                if($dok_ling == 'Taat' and ($sum_neraca == 3 or $sum_neraca == 4) and ($sum_manifest >= 3)) {
                    $val->b3adm = 'Taat';
                }else{
                    $val->b3adm = '<font color="red">Tidak Taat</font>';
                }

                # ------------------------------------------------------------------------------------------------------               

                # akumulasi
                if(strpos($data_pelanggaran_bap,'air') !== false or 
                    strpos($data_pelanggaran_bap,'udara') !== false or
                    strpos($data_pelanggaran_bap,'b3') !== false || strpos($data_pelanggaran_bap,'padat') !== false or
                    $val->airadm == '<font color="red">Tidak Taat</font>' or
                    $val->udaraadm == '<font color="red">Tidak Taat</font>' or
                    $val->b3adm == '<font color="red">Tidak Taat</font>') {
                    $val->akumulasi = '<font color="red">Tidak Taat</font>';
                }

            } else {
                $val->airadm = ' - ';
                $val->udaraadm = ' - ';
                $val->b3adm = ' - ';

                $val->airteknis = ' - ';
                $val->udarateknis = ' - ';
                $val->b3teknis = ' - ';
                
                $val->akumulasi = ' - ';
                $val->keterangan = ' - ';
            }

        }
        
        return $rekap;
    }

    public function generate_apresiasi() {
        date_default_timezone_set("Asia/Jakarta");

        $id_industri = $this->input->post('id_industri');
        $year = $this->input->post('year');

        // read file
        $file_location = "assets/form/Surat_Apresiasi.rtf";
        $handle = fopen ($file_location, 'r');
        $content = fread($handle, filesize($file_location));
        
        fclose($handle);

        $industri = $this->ketDao->get_obj_by_id('industri', 'id_industri', $id_industri);
        
        $param = array (
            '<date-now>' => ' \tab \tab \tab '.date("Y"),
            '<nama_usaha_kegiatan>' => $industri->nama_industri,
            '<alamat_usaha_kegiatan>' => $industri->alamat,
            '<year>' => $year
        );

        // replace content
        foreach($param as $from => $to) {
           $content = str_replace($from, $to, $content);
        }
        
        // throw content 
        header('Content-type: application/rtf');
        header('Content-Disposition: attachment; filename="Surat_Apresiasi_'.$industri->nama_industri.'_'.$year.'.rtf"');

        echo $content;
    }

    private function get_bap($id_bap) {

        $bap = $this->bapDao->get_header_raport_bap($id_bap);

        $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

        $izin_limbah = $this->bapDao->get_obj_by_id("izin_pembuangan_limbah", "id_industri", $bap->id_industri);

        $izin_air_limbah = $this->bapDao->get_obj_by_id("izin_pembuangan_limbah", "id_industri", $bap->id_industri);

        $izin_tps_limbah = $this->bapDao->get_obj_by_id("izin_tps_limbah", "id_industri", $bap->id_industri);        

        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

        $data_cerobong = $this->bapDao->get_list_by_id("data_cerobong", "id_penc_udara", $pencemaran_udara->id_penc_udara);

        $uji_emisi = $this->bapDao->get_list_by_id("uji_emisi", "id_penc_udara", $pencemaran_udara->id_penc_udara);

        $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

        $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);

        // $jenis_limbah_b3 = $this->bapDao->get_list_by_id("jenis_limbah_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

        $tps_b3 = $this->bapDao->get_list_by_id("tps_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

        $data['bap'] = $bap;
        $data['pencemaran_air'] = $pencemaran_air;
        $data['izin_limbah'] = $izin_limbah;
        $data['izin_air_limbah'] = $izin_air_limbah;
        $data['izin_tps_limbah'] = $izin_tps_limbah;
        $data['data_cerobong'] = $data_cerobong;
        $data['uji_emisi'] = $uji_emisi;
        $data['uji_ambien'] = $uji_ambien;
        $data['pencemaran_pb3'] = $pencemaran_pb3;
        // $data['jenis_limbah_b3'] = $jenis_limbah_b3;
        $data['tps_b3'] = $tps_b3;

        return $data;
    }
}
