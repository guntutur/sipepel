<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">

            <!-- Begin page heading -->
            <h1 class="page-heading">Dashboard</h1>
            <!-- End page heading -->			

            <?php echo $this->session->flashdata('msg'); ?>

            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#persebaran" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;PERSEBARAN INDUSTRI</a></li>
                    <li ><a href="#trend-ketaatan" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;TRENDLINE INDUSTRI</a></li>
                    <li ><a href="#lhu5" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;TOP 5 ITEM LHU</a></li>
                    <li ><a href="#bap5" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;TOP 5 ITEM BAP</a></li>
                    <li ><a href="#trend-dokumen" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;TRENDLINE DOKUMEN</a></li>
                    <li style="display: none"><a href="#presentase" role="tab" data-toggle="tab"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;PRESENTASE KETAATAN</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- new chart begin from here -->
                    <div class="tab-pane active" id="persebaran">
                        <h3>
                            Persebaran Industri Berdasarkan Jenis Usaha Kegiatan per Kecamatan
                        </h3>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <select class="form-control input-sm chosen-select" name="select-kecamatan-bar[]" data-placeholder="Pilih Kecamatan" id="select-kecamatan-bar" multiple tabindex="4">
                                            <option value="Empty">&nbsp;</option>
                                            <?php foreach($kecamatan as $ttk): ?>
                                                <option value="<?php echo $ttk->id_kecamatan;?>"><?php echo $ttk->ket; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <button id="gen-persebaran" class="btn btn-success">Generate Chart</button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="persebaran-bar-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="trend-ketaatan">
                        <h3>
                            Trendline Ketidaktaatan Industri (5 Tahun terakhir)
                        </h3>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <select class="form-control input-sm" name="tahun-ketaatan-line" id="tahun-ketaatan-line">
                                                    <!--<option value="2017">2017</option>-->
                                                    <?php $idx = 0; foreach($tahun_trend_ketaatan as $ttk): ?>
                                                        <option value="<?php echo $ttk->tahun;?>" <?php echo $idx == 0 ? 'selected' : ''?>><?php echo $ttk->tahun; ?></option>
                                                    <?php $idx++; endforeach;?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="ketaatan-dokumen-line-chart" style="margin-left: 0px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="lhu5">
                        <h3>
                            5 Item Ketaatan LHU yang sering dilanggar
                        </h3>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <select class="form-control input-sm" name="tahun_lhu" id="tahun_lhu">
                                            <option value="" selected disabled>-- Pilih Tahun LHU --</option>
                                            <?php foreach($tahun5_lhu as $thlhu => $vthlhu): ?>
                                                <option value="<?php echo $vthlhu->tahun;?>"><?php echo $vthlhu->tahun; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control input-sm" name="jenis_lhu" id="jenis_lhu">
                                            <option value="" selected disabled>-- Pilih Jenis LHU --</option>
                                            <option value="LHU Air Limbah">LHU Air Limbah</option>
                                            <option value="LHU Udara Emisi">LHU Udara Emisi</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="form-control input-sm" name="usaha_kegiatan_lhu" id="usaha_kegiatan_lhu">
                                            <option value="" selected disabled>-- Pilih Usaha Kegiatan --</option>
                                            <?php foreach($usaha_kegiatan as $uk): ?>
                                                <option value="<?php echo $uk->id_usaha_kegiatan;?>"><?php echo $uk->ket; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <button id="gen-5-lhu" class="btn btn-success">Generate Chart</button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="lhu-bar-chart" style="margin-left: 0px; height:750px;"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="tab-pane" id="bap5">
                        <h3>
                            5 Item Ketaatan BAP yang sering dilanggar
                        </h3>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <select class="form-control input-sm" name="tahun_bap" id="tahun_bap">
                                            <option value="" selected disabled>-- Pilih Tahun BAP --</option>
                                            <?php foreach($tahun5_bap as $thbap => $vthbap): ?>
                                                <option value="<?php echo $vthbap->tahun;?>"><?php echo $vthbap->tahun; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <select class="form-control input-sm" name="usaha_kegiatan_bap" id="usaha_kegiatan_bap">
                                            <option value="" selected disabled>-- Pilih Usaha Kegiatan --</option>
                                            <?php foreach($usaha_kegiatan as $uk): ?>
                                                <option value="<?php echo $uk->id_usaha_kegiatan;?>"><?php echo $uk->ket; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <button id="gen-5-bap" class="btn btn-success">Generate Chart</button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="bap-bar-chart" style="margin-left: 0px; height:750px;"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="tab-pane" id="trend-dokumen">
                        <h3>
                            Trendline Dokumen yang masuk ke BPLH (5 Tahun terakhir)
                        </h3>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <select class="form-control input-sm" name="tahun-dokumen" id="tahun-dokumen">
                                            <!--<option value="2017">2017</option>-->
                                            <?php foreach($tahun_trend_dokumen as $trd): ?>
                                                <option value="<?php echo $trd->tahun;?>"><?php echo $trd->tahun; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <select class="form-control input-sm" name="usaha_kegiatan_dokumen" id="usaha_kegiatan_dokumen">
                                            <option value="" selected disabled>-- Pilih Usaha Kegiatan --</option>
                                            <?php foreach($usaha_kegiatan as $uk): ?>
                                                <option value="<?php echo $uk->id_usaha_kegiatan;?>"><?php echo $uk->ket; ?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <button id="gen-trend-dokumen" class="btn btn-success">Generate Chart</button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="ketaatan-dokumen-bplh-line-chart" style="margin-left: 0px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div><!-- /.container-fluid -->			


    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->


<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    /*begin persebaran industri*/
    function sebaranBarChart(id_kecamatan, kecamatan) {
        var title = "Menampilkan data untuk Kecamatan : ";
        $.each(kecamatan, function (k, v) {
            title += k==kecamatan.length-1 ? v : v+", ";
        });
        $.post('dashboard/persebaran_industri',
            {id_kecamatan: id_kecamatan},
            function (result) {
                $('#persebaran-bar-chart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {text: title},
                    xAxis: {
                        categories: kecamatan,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Jumlah'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: result
                });
            }, "json");
    }
    /*end persebaran industri*/

    /*begin top 5 lhu*/
    function top5ParamLhu(tahun_lhu, jenis_lhu, usaha_kegiatan_lhu, text) {
        var title = "Menampilkan 5 parameter LHU Tahun "+tahun_lhu+", untuk "+jenis_lhu+", pada Usaha Kegiatan "+text;

        $.post('dashboard/top5param_lhu',
            {
                tahun: tahun_lhu,
                jenis_lhu: jenis_lhu,
                usaha_kegiatan: usaha_kegiatan_lhu
            },
            function (result) {
                $('#lhu-bar-chart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {text: title},
                    xAxis: {
                        categories: result.param_name,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Jumlah'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: "Jumlah",
                        data: result.param_value
                    }]
                });
            }, "json");
    }
    /*end top 5 lhu*/

    /*begin top 5 bap*/
    function top5ParamBap(tahun_bap, usaha_kegiatan_bap, text) {
        var title = "Menampilkan 5 parameter BAP Tahun "+tahun_bap+", pada Usaha Kegiatan "+text;

        $.post('dashboard/top5param_bap',
            {
                tahun: tahun_bap,
                usaha_kegiatan: usaha_kegiatan_bap
            },
            function (result) {
                $('#bap-bar-chart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {text: title},
                    xAxis: {
                        categories: result.param_name,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Jumlah'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y} </b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: "Jumlah",
                        data: result.param_value
                    }]
                });
            }, "json");
    }
    /*begin top 5 bap*/

    /*begin trend ketaatan*/
    function ketaatan_industri(tahun) {
        $.post('dashboard/trendline_ketaatan_industri', {tahun: tahun},
            function (result) {
                $("#ketaatan-dokumen-line-chart").highcharts({
                    title: {text: "Tahun "+(tahun-4)+" - "+(tahun)},
                    xAxis: {
                        categories: result.category
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Item'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: result.data,
                    tooltip: {
                      valueSuffix: ' Item Pelanggaran'
                    }
                });
            }, "json");
    }
    /*end trend ketaatan*/

    /*begin trend ketaatan*/
    function ketaatan_dokumen(tahun, usaha_kegiatan_dokumen, text) {
        $.post('dashboard/trendline_dokumen_bplh', {
                tahun: tahun,
                usaha_kegiatan: usaha_kegiatan_dokumen
            },
            function (result) {
                $("#ketaatan-dokumen-bplh-line-chart").highcharts({
                    title: {text: "Tahun "+(tahun-4)+" - "+(tahun)+", pada Usaha Kegiatan "+text},
                    xAxis: {
                        categories: result.category
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Dokumen'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: result.data
                });
            }, "json");
    }
    /*end trend ketaatan*/

    $(document).ready(function () {
        /*begin new chart*/

        /*begin bar chart*/
        $("#gen-persebaran").click(function () {
            var options = $("#select-kecamatan-bar option:selected");

            var categories = $.map(options, function (option) {
                return option.text;
            });
            sebaranBarChart($("#select-kecamatan-bar").val(), categories);
        });
        /*end bar chart*/

        /*begin top 5 param lhu*/
        $("#gen-5-lhu").click(function () {
            if(!$("#tahun_lhu").val()) { alert("Pilih Tahun LHU!"); return false }
            if(!$("#jenis_lhu").val()) { alert("Pilih Jenis LHU!"); return false }
            if(!$("#usaha_kegiatan_lhu").val()) { alert("Pilih Usaha Kegiatan!"); return false }

            var tahun_lhu = $("#tahun_lhu option:selected").val();
            var jenis_lhu  = $("#jenis_lhu option:selected").val();
            var usaha_kegiatan_lhu = $("#usaha_kegiatan_lhu option:selected").val();
            var usaha_kegiatan_lhu_text = $("#usaha_kegiatan_lhu option:selected").text();

            top5ParamLhu(tahun_lhu, jenis_lhu, usaha_kegiatan_lhu, usaha_kegiatan_lhu_text);
        });
        /*end top 5 param lhu*/

        /*begin top 5 param bap*/
        $("#gen-5-bap").click(function () {
            if(!$("#tahun_bap").val()) { alert("Pilih Tahun BAP!"); return false }
            if(!$("#usaha_kegiatan_bap").val()) { alert("Pilih Usaha Kegiatan!"); return false }

            var tahun_bap = $("#tahun_bap option:selected").val();
            var usaha_kegiatan_bap = $("#usaha_kegiatan_bap option:selected").val();
            var usaha_kegiatan_bap_text = $("#usaha_kegiatan_bap option:selected").text();

            top5ParamBap(tahun_bap, usaha_kegiatan_bap, usaha_kegiatan_bap_text);
        });
        /*end top 5 param bap*/

        /*begin trend ketaatan*/
        $("#tahun-ketaatan-line").on('change', function () {
            $(this).val();
            ketaatan_industri($(this).val());
        });
        // initiate chart on dom ready, because there is only one filter
        ketaatan_industri($("#tahun-ketaatan-line").val());
        /*end trend ketaatan*/

        /*begin trend dokumen*/
        $("#gen-trend-dokumen").click(function () {
            if(!$("#tahun-dokumen").val()) { alert("Pilih Tahun Dokumen!"); return false }
            if(!$("#usaha_kegiatan_dokumen").val()) { alert("Pilih Usaha Kegiatan!"); return false }

            var tahun_dok = $("#tahun-dokumen option:selected").val();
            var usaha_kegiatan_dok = $("#usaha_kegiatan_dokumen option:selected").val();
            var usaha_kegiatan_dok_text = $("#usaha_kegiatan_dokumen option:selected").text();

            ketaatan_dokumen(tahun_dok, usaha_kegiatan_dok, usaha_kegiatan_dok_text);
        });
        /*end trend dokumen*/

        /*end new chart*/
        //------------ end ------------//
        
    });

</script>
<script src="<?php echo base_url()?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/js/exporting.js"></script>
