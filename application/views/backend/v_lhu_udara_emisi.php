<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">LHU UDARA EMISI&nbsp;&nbsp;<small>mengelola seluruh LHU Udara Emisi</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Laporan Hasil Uji</li>
                <li class="active">Udara Emisi</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">
              
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#udara_emisi" onclick="$(this).clear_koor('udara_emisi', true, 'koordinat_cerobong');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat LHU</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>
                
                <?php echo $this->session->flashdata('msg'); ?>
                <?php 
                    if($this->session->flashdata('error')) {
                        $msg = '';
                        foreach ($this->session->flashdata('error') as $key => $value) {
                            $msg .= $value[2]." (".$value[0].") melebihi baku mutu, seharusnya ".$value[1]."<br/>";                            
                        }
                        echo $this->functions->build_message('danger', $msg);
                    } 
                ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%" class="text-area">#</th>
                                <th>No LHU</th>
                                <th>Nama Usaha Kegiatan</th>
                                <th>Laboratorium</th>
                                <th>Laporan Bulan</th>    
                                <th>Jumlah Parameter</th>
                                <th width="15%">Jml Parameter Tidak Memenuhi Baku Mutu</th>            
                                <th class="text-right" width="5%"></th>                                 
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/lhu_udara_emisi/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                                            
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        t += '<td>'+dt.no_lhu+'</td>';
                        t += '<td>'+dt.nama_industri+'</td>';
                        t += '<td>'+dt.nama_lab+'</td>';

                        var tmp = dt.laporan_bulan_tahun.split(".");
                        
                        t += '<td>'+$(this).get_month(parseInt(tmp[0]))+' '+ tmp[1] +'</td>';
                        t += '<td>'+dt.detail+'</td>';
                        t += '<td>'+dt.teguran+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a href="<?php echo base_url("backend/lhu_udara_emisi/detail/'+dt.id_laporan_hasil_uji+'"); ?>">Detail LHU</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_laporan_hasil_uji+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_laporan_hasil_uji+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                            
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

    $(document).ready(function () {
        
        $.validator.setDefaults({ ignore: ":hidden:not(select)" });
        $("#form_add").validate({
            rules: {
                no_lhu: {required: true},
                lap_bln: {required: true},
                jenis_bakumutu: { required: true},
                industri: {required: true},
                jenis_bahan_bakar: {required: true},
                labs: {required: true}
            },
            messages: {     
                no_lhu: { required: "Masukkan nomor laporan hasil uji"},
                lap_bln: { required: "Masukkan bulan laporan"},
                industri: {required: "Masukkan nama industri"},                                
                jenis_bakumutu: { required: "Pilih data"},                
                jenis_bahan_bakar: {required: "Pilih data"},
                labs: {required: "Pilih data"}
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) { 
            // validator.resetForm();              
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/lhu_udara_emisi/get'); ?>",
                data: 'id_laporan_hasil_uji=' + id,
                success: function (response) {
                    console.log(response);

                    $(this).reset_form('udara_emisi');

                    $('input[id=edit_id]').val(response.id_laporan_hasil_uji);                    
                    $('input[name=no_lhu]').val(response.no_lhu);
                    $('input[name=lap_bln]').val(response.laporan_bulan_tahun);
                    $('select[name=jenis_bakumutu]').val(response.jenis_baku_mutu);

                    // $('input[name=nama_industri]').val(response.nama_industri);
                    // $('input[name=industri]').val(response.id_industri);
                    $('select[name=industri]').chosen();
                    $('select[name=industri]').val(response.id_industri);
                    $('select[name=industri]').trigger("chosen:updated");

                    $('select[name=labs]').val(response.id_lab);
                    $('input[name=tgl_pengambilan]').val($(this).format_date(response.tgl_pengambilan_sample));
                    $('input[name=waktu_pengambilan]').val(response.waktu_pengambilan_sample);                    
                    $('select[name=jenis_bahan_bakar]').val(response.jenis_bahan_bakar);
                    
                    // # koordinat outlet ipal
                    var t = '';
                    if(response.koord_cerobong.length > 0) {
                        $(this).koordinat_template('koordinat_cerobong', response.koord_cerobong);
                    }     
                                                        
                    $('#udara_emisi form').attr('action', '<?php echo base_url('/backend/lhu_udara_emisi/edit') ?>'); //this fails silently
                    $('#udara_emisi form').get(0).setAttribute('action', '<?php echo base_url('/backend/lhu_udara_emisi/edit') ?>'); //this works
                    
                    $('#udara_emisi').modal('show');
                }
            })
        }        
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        } 

        // var site = "<?php echo site_url(); ?>";
        // $('#nama_industri').autocomplete({
        //     minChars: 2,
        //     type: 'POST',
        //     noCache: true,
        //     serviceUrl: site + 'backend/industri/search',
        //     onSearchStart: function (query) {
        //         $('#industri').val(null);              
        //     },
        //     onSelect: function (suggestion) {
        //         $('#industri').val(suggestion.data);
        //     }
        // });

        Document.search();
    });
</script>

<!-- Modal -->
<div class="modal fade" id="udara_emisi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Formulir LHU Udara Emisi</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/lhu_udara_emisi/register') ?>" method="post" id="form_add">
                <div class="modal-body">

                                             
                    <div class="form-group param_internal">
                        <label>No. LHU</label>
                        <input type="text" name="no_lhu" id="no_lhu" class="form-control input-sm has-feedback" placeholder="" autofocus />                        
                    </div> 
            

                    <div class="row">                        
                        <div class="col-lg-6 form-group value_parameter">
                            <label>Laporan Bulan dan Tahun</label>
                            <input class="form-control input-sm datepicker" type="text" name="lap_bln" id="lap_bln" value="" placeholder="mm.yyyy" data-date-format="mm.yyyy" data-date-start-view="2" data-date-min-view-mode="1">
                        </div>              
                        <div class="col-lg-6 form-group param_internal">
                            <label>Jenis Baku Mutu</label>
                            <select name="jenis_bakumutu" id="jenis_bakumutu" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Baku Mutu --</option>
                                <?php 
                                    foreach ($this->functions->get_jenis_baku_mutu() as $key => $value) {
                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                    }
                                ?>
                            </select>   
                        </div>
                    </div>  

                    <!-- <div class="form-group status">                        
                        <label>Nama Usaha/Kegiatan</label>
                        <input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                        <input type="hidden" id="industri" name="industri" />                       
                    </div>  -->              
                    <div class="form-group tentang">
                        <label>Nama Usaha/Kegiatan</label>                        
                        <select name="industri" id="industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                            <option value="Empty">&nbsp;</option>
                            <?php
                                foreach ($industri as $key => $value) {
                                    echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                }
                            ?>
                        </select>
                    </div>

                    <div class="form-group status">
                        <label>Nama Laboratorium</label>
                        <select name="labs" id="labs" class="form-control input-sm">
                            <option value="" disabled selected>-- Pilih Laboratorium --</option>
                            <?php 
                                foreach ($labs as $key => $value) {
                                    echo '<option value="'.$value->id_lab.'">'.$value->nama_lab.'</option>';
                                }
                            ?>
                        </select>     
                    </div> 

                    <div class="row">                        
                        <div class="col-lg-6 form-group ">
                            <label>Tanggal dan Waktu Pengambilan Sample</label>
                            <div class="row">                               
                                <div class="col-lg-6" style="padding-right: 5px;">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="tgl_pengambilan" class="form-control datepicker" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    </div>
                                </div>                         
                                <div class="col-lg-6" style="padding-left: 5px;">
                                    <div class="input-group input-group-sm">
                                        <input type="text" name="waktu_pengambilan" class="form-control timepicker" value="">
                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                    </div>
                                </div>                         
                            </div>                 
                        </div>
                        <div class="col-lg-6 form-group param_internal">                           
                            <label>Jenis Bahan Bakar</label>
                            <select name="jenis_bahan_bakar" id="jenis_bahan_bakar" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Bahan Bakar --</option>
                                <?php 
                                    $status = $this->functions->get_bahan_bakar();
                                    foreach($status as $k => $s) {
                                        echo '<option value="'.$k.'">'.$s.'</option>';
                                    }
                                ?>    
                            </select>              
                        </div>
                    </div>

                    <label>Cerobong</label>
                    <div id="koordinat_cerobong"></div>
                    <div class="form-group row" data-duplicate="koordinat" data-duplicate-min="1">
                        <div class="col-lg-5">
                            <label>Koordinat S</label>
                            <div class="row">                               
                                <div class="col-lg-4" style="padding-right: 0;">
                                    <input type="text" name="s_deg[]" class="form-control input-sm derajat" value="" required="required" placeholder="00&deg;">  
                                </div>
                                <div class="col-lg-4" style="padding-right: 0;">
                                    <input type="text" name="s_squo[]" class="form-control input-sm" value="" required="required" placeholder="00&#39;">     
                                </div>
                                <div class="col-lg-4" style="padding-right: 0;">
                                    <input type="text" name="s_quo[]" class="form-control input-sm" value="" required="required" placeholder="00&quot;">  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <label>Koordinat E</label>
                            <div class="row">
                                <div class="col-lg-4" style="padding-right: 0;">
                                    <input type="text" name="e_deg[]" class="form-control input-sm" value="" required="required" placeholder="00&deg;">  
                                </div>
                                <div class="col-lg-4" style="padding-right: 0;">
                                    <input type="text" name="e_squo[]" class="form-control input-sm" value="" required="required" placeholder="00&#39;">     
                                </div>
                                <div class="col-lg-4" style="padding-right: 0;">
                                    <input type="text" name="e_quo[]" class="form-control input-sm" value="" required="required" placeholder="00&quot;">  
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2" style="padding-left: 9px;">
                            <label>&nbsp;</label>
                            <div class="text-right">
                                <div class="btn-group" style="padding-right: 0;">
                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="koordinat"><i class="fa fa-plus-circle"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="koordinat"><i class="fa fa-times-circle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>             
                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Industry Type</h4>
            </div>
            <form action="<?php echo base_url('/backend/lhu_udara_emisi/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal 