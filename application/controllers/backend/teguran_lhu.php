<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class teguran_lhu extends CI_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        $this->load->model('m_teguran_lhu', '', TRUE);
        $this->load->model('m_industry', '', TRUE);
        $this->load->model('m_lhu_air_limbah', '', TRUE);
        $this->load->model('m_lhu_udara_emisi', '', TRUE);
		
		$this->load->config('tank_auth', TRUE);
	}

	public function index() {
		if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

		# load view
        $this->load->view('include/header');        
        $this->load->view('backend/v_teguran_lhu');
        $this->load->view('include/footer');
	}

	public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'i.nama_industri' => array('SEARCH', 'OR', $query),
				'b.nama_lab' => array('SEARCH', 'OR', $query),
				'l.laporan_bulan_tahun' => array('SEARCH', 'OR', $query),
			);

     	$total = $this->m_teguran_lhu->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_teguran_lhu->get_all($where, $dataperpage, $start);
	    // print_r($types);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }

	public function generate_laporan() {

		$param = explode('-', $this->uri->segment(4));

		$industri = $this->m_industry->get_industry_by_id($param[0]);

		// replace #periode berdasarkan bulan saat di generate
		$count = 0;

		if($param[1] != 0) {
			$air_limbah = $this->m_teguran_lhu->get_history($param[1]);
			$lhu_air_limbah = $this->m_lhu_air_limbah->get_detail_by_id($param[1]);
			$bulan_pengujian_air = $this->functions->get_month((integer)substr($lhu_air_limbah->laporan_bulan_tahun, 0, 2));
			$count += 1;
		}

		if($param[2] != 0) {
			$udara_emisi = $this->m_teguran_lhu->get_history($param[2]);
			$lhu_udara_emisi = $this->m_lhu_udara_emisi->get_detail_by_id($param[2]);
			$bulan_pengujian_emisi = $this->functions->get_month((integer)substr($lhu_udara_emisi->laporan_bulan_tahun, 0, 2));
			$count += 1;
		}

		$bulan = $this->functions->get_month((integer)date('m'));

		# get file
		if($count == 2) {

			$file = file_get_contents(site_url('assets/form/surat_teguran_lhu.rtf'));			

			$file = str_replace("#bulan_pengujian_air", $bulan_pengujian_air.' '.date('Y'), $file);
			$file = str_replace("#parameter_air", str_replace(';', ', ', $air_limbah->parameter), $file);
			$file = str_replace("#dasar_hukum_air", str_replace(';', ', ', $air_limbah->dasar_hukum), $file);

			$file = str_replace("#bulan_pengujian_emisi", $bulan_pengujian_emisi.' '.date('Y'), $file);
			$file = str_replace("#parameter_emisi", str_replace(';', ', ', $udara_emisi->parameter), $file);
			$file = str_replace("#dasar_hukum_emisi", str_replace(';', ', ', $udara_emisi->dasar_hukum), $file);

		}else{

			$file = file_get_contents(site_url('assets/form/surat_teguran_lhu_single.rtf'));

			# air limbah
			if($param[1] != 0) {

				$content = 'Hasil pengujian kualitas air limbah perusahaan Saudara pada bulan {\rtlch\fcs1 \af0\afs24 \ltrch\fcs0 \b\f0\fs24\insrsid9585389\charrsid5203079 #bulan_pengujian_air} menunjukkan parameter #parameter_air tidak memenuhi baku mutu air limbah sesuai ketentuan ({\rtlch\fcs1 \af0\afs24 \ltrch\fcs0 \b\f0\fs24\insrsid9585389\charrsid5203079 #dasar_hukum_air})';

				$content = str_replace("#bulan_pengujian_air", $bulan_pengujian_air.' '.date('Y'), $content);
				$content = str_replace("#parameter_air", str_replace(';', ', ', $air_limbah->parameter), $content);
				$content = str_replace("#dasar_hukum_air", str_replace(';', ', ', $air_limbah->dasar_hukum), $content);

				$optimal = 'Optimalisasi pengolahan air imbah pada IPAL sehingga air limbah yang dibuang memenuhi baku mutu;';
			}

			# udara emisi
			if($param[2] != 0) {
				$content = 'Hasil pengujian kualitas udara emisi sumber tidak bergerak perusahaan Saudara pada bulan {\rtlch\fcs1 \af0\afs24 \ltrch\fcs0 \b\f0\fs24\insrsid9585389\charrsid5203079 #bulan_pengujian_emisi} menunjukkan parameter #parameter_emisi tidak memenuhi baku mutu udara emisi sesuai ketentuan ({\rtlch\fcs1 \af0\afs24 \ltrch\fcs0 \b\f0\fs24\insrsid9585389\charrsid5203079 #dasar_hukum_emisi})';

				$content = str_replace("#bulan_pengujian_emisi", $bulan_pengujian_emisi.' '.date('Y'), $content);
				$content = str_replace("#parameter_emisi", str_replace(';', ', ', $udara_emisi->parameter), $content);
				$content = str_replace("#dasar_hukum_emisi", str_replace(';', ', ', $udara_emisi->dasar_hukum), $content);


				$optimal = 'Optimalisasi upaya pengendalian pencemaran udara pada sumber emisi tidak bergerak sehingga kualitas udara emisi yang dibuang memenuhi baku mutu;';
				
			}

			$file = str_replace("#hasil_pengujian", $content, $file);
			$file = str_replace("#optimal", $optimal, $file);

		}	

		$periode = $this->functions->get_periode((integer)date('m'));

		# header
		$file = str_replace("#nomor_surat", "", $file);
		$file = str_replace("#tanggal_hari_ini", date("d").' '.$bulan.' '.date('Y'), $file);
		$file = str_replace("#tahun", date("Y"), $file);
		$file = str_replace("#nama_industri", $industri->nama_industri, $file);
		$file = str_replace("#tempat", $industri->alamat, $file);
		$file = str_replace("#periode_lhu", $periode, $file);	

		header("Content-type: application/msword");
		header("Content-disposition: inline; filename=teguran_lhu_".str_replace(" ", '_', strtolower($industri->nama_industri)).".doc");
		header("Content-length: ".strlen($file));

		echo $file;
	}

	public function konfirmasi() {

		$id = explode('-', $this->input->post('id_lhu'));

		if($id != '') {

			// $i = $this->m_industry->get_industry_by_id($id[0]);

			$temp = pathinfo($_FILES["userfile"]["name"]);
			$path = "assets/lhu/";
			$newfilename = strtolower($temp['filename'].".".$temp['extension']);

			$config['upload_path'] = realpath($path);
			$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png';
			$config['max_size'] = '8000';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $newfilename;
			
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload()) {				
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', $this->upload->display_errors()));
			}else{								

				$param = array(
					'pengirim' => $this->input->post('nama_pengirim'),
					'via' => $this->input->post('via'),
					'tgl_kirim' => date('Y-m-d'),
					'barbuk' => $newfilename
					// 'status' => 1
				);

				if($id[1] != 0) $this->m_teguran_lhu->update_lhu($param, $id[1]);
				if($id[2] != 0) $this->m_teguran_lhu->update_lhu($param, $id[2]);				

				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Berkas berhasil diunggah'));
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}	

	public function tutup_teguran() {

		$id = explode('-', $this->input->post('id_lhu'));		
		$data['status'] = 1;
		$status_lhu_air_limbah = false;
		$status_udara_emisi = false;

		if($id[1] != 0) $status_lhu_air_limbah = $this->m_teguran_lhu->update_lhu($data, $id[1]);
		if($id[2] != 0) $status_udara_emisi = $this->m_teguran_lhu->update_lhu($data, $id[2]);					

		if($status_lhu_air_limbah and $status_udara_emisi) {
			echo json_encode(array('success' => 1, 'msg' => 'Transaksi berhasil ditutup', 'value' => 2));			
		}else {
			echo json_encode(array('success' => 0, 'msg' => 'Transaksi gagal ditutup', 'value' => 1));
		}
		exit();
	}

	public function get_notif_lhu() {

        $notif = $this->m_teguran_lhu->get_notif_lhu();
        $data['notif_lhu'] = $notif;        

        echo json_encode($data);
        exit();	
	}

	public function count_notif_lhu() {
		$jml_notif = $this->m_teguran_lhu->count_notif_lhu();
		$data['jml_notif_lhu'] = $jml_notif;
		echo json_encode($data);
		exit();
	}
}
?>