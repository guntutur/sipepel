<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 // Document   : bap_golf.php 
 // Created on : November 13, 2014 17:04 
 // Author     : huda.jtk09@gmail.com 
 // Description: Controller for BAP Golf 

class bap_golf extends CI_Controller {

	private $day_id = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
	private $id_usaha_kegiatan = 12; #id_usaha_kegiatan golf

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_bap_golf', 'golfDao', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);
        $this->load->model('m_employee', 'empDao', TRUE);
        $this->load->model('m_laporan_bap', 'lapDao', TRUE);
        $this->load->model('m_data_master', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);

		$this->load->library('PHPExcel');
    }

    # default view
	public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all bap golf
        $bap_golf = $this->golfDao->get_all();
		
		# get all industri
		// $industri = $this->indDao->get_all_industri($this->id_usaha_kegiatan);
		$industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

		# get all industri
		$pegawai = $this->empDao->get_all_employee();

		# get all lab
		$data['lab'] = $this->bapDao->select_table('laboratorium');

		# get from data acuan
		$data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
		$data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
		$data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
		$data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");
		
		# response paramter
        $data['bap_golf'] = $bap_golf;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;        
		
        # parameter for view
        $data['tab_list'] = 'active';
		$data['tab_view'] = '';

		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_golf', $data);
    }
	
	public function view() {
    	# get id bap from view
		$id_bap = $this->input->post('id_bap');
		
		// redirect if id_bap is null
		if (empty($id_bap)) {
	        redirect('backend/bap_golf');
		};

		/*data umum*/
    	$bap = $this->bapDao->get_obj_by_id("bap", "id_bap", $id_bap);
    	$bap_golf = $this->bapDao->get_obj_by_id("bap_golf", "id_bap", $id_bap);
    	$industri =	$this->bapDao->get_obj_by_id("industri", "id_industri", $bap->id_industri);
    	$pegawai = $this->bapDao->get_obj_by_id("pegawai", "id_pegawai", $bap->id_pegawai);
    	$petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

        $data['bap'] = $bap;
        $data['bap_golf'] = $bap_golf;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;
        $data['petugas_bap'] = $petugas_bap;

        /*pencemaran air*/
    	$pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);
        if ($pencemaran_air) {
            $data['pencemaran_air'] = $pencemaran_air;
        }

        /*pencemaran udara*/
    	$pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
        if ($pencemaran_udara) {
            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);
            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
        }

        /*pencemaran pb3*/
    	$pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
        if ($pencemaran_pb3) {
            $data['pencemaran_pb3'] = $pencemaran_pb3;
        }

        # parameter for view
        $data['tab_list'] = '';
		$data['tab_view'] = 'active';

		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_golf', $data);
	}

	public function newInsert() {
		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();

		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			// 'url' => $this->input->post(''),
			// 'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => date("Y-m-d", strtotime($this->input->post('bap_tgl'))),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->input->post('dok_lingk'),
			'dok_lingk_jenis' => $this->input->post('dok_lingk_jenis'),
			'dok_lingk_tahun' => $this->input->post('dok_lingk_tahun'),
			'izin_lingk' => $this->input->post('izin_lingk'),
			'izin_lingk_tahun' => $this->input->post('izin_lingk_tahun'),
			'tgl_pembuatan' => $datenow,
			'dibuat_oleh' => $user,
			'catatan' => $this->input->post('catatan')
			);

		# insert bap
		$id_bap = $this->bapDao->insert_getID('bap',$bap);

		$bap['id_bap'] = $id_bap;

		# Petugas BAP Add procedur
		$id_petugas_bap_list = $this->input->post('id_petugas_bap_list');

		if ($id_petugas_bap_list[0]!=null) {
			foreach ($id_petugas_bap_list as $idpbap) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $idpbap,
					// 'ket' => '',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);

				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
			}
		}
		
		# array for table bap_golf
		$bap_golf = array(
			'id_bap' => $id_bap,
			'jumlah_karyawan' => $this->input->post('jumlah_karyawan'),
			'jumlah_hole' => $this->input->post('jumlah_hole'),
			'rata_pengunjung' => $this->input->post('rata_pengunjung'),
			'jml_membership' => $this->input->post('jml_membership'),
			'pakai_pupuk' => $this->input->post('pakai_pupuk'),
			'pakai_pestisida' => $this->input->post('pakai_pestisida'),
			'pakai_oli' => $this->input->post('pakai_oli'),
			'simpan_oli_bekas' => $this->input->post('simpan_oli_bekas')
			);

		#insert bap_golf
		$this->bapDao->insert_getStat('bap_golf', $bap_golf);

		$limb_sumber_array = $this->input->post('limb_sumber');
		$limb_sumber = '';
		foreach ($limb_sumber_array as $key) {
			$limb_sumber .= $key.", ";
		}

		$limb_sumber = rtrim($limb_sumber, ', ');

		# array for pencemaran_air
		$pencemaran_air = array(
			'id_bap' => $id_bap,
			'ambil_air_tanah' => $this->input->post('ambil_air_tanah'),
			'ambil_air_tanah_izin' => $this->input->post('ambil_air_tanah_izin'),
			'ambil_air_permukaan' => $this->input->post('ambil_air_permukaan'),
			'ambil_air_permukaan_izin' => $this->input->post('ambil_air_permukaan_izin'),
			'ambil_air_pdam' => $this->input->post('ambil_air_pdam'),
			'ambil_air_lain' => $this->input->post('ambil_air_lain'),
			'limb_sumber' => $limb_sumber,
			'bdn_terima' => $this->input->post('bdn_terima'),
			'sarana_olah_limbah' => $this->input->post('sarana_olah_limbah'),
			'sarana_jenis' => $this->input->post('sarana_jenis'),
			'sarana_kapasitas' => $this->input->post('sarana_kapasitas'),
			'sarana_koord_derajat_s' => $this->input->post('sarana_koord_derajat_s'),
			'sarana_koord_jam_s' => $this->input->post('sarana_koord_jam_s'),
			'sarana_koord_menit_s' => $this->input->post('sarana_koord_menit_s'),
			'sarana_koord_derajat_e' => $this->input->post('sarana_koord_derajat_e'),
			'sarana_koord_jam_e' => $this->input->post('sarana_koord_jam_e'),
			'sarana_koord_menit_e' => $this->input->post('sarana_koord_menit_e')
			);

			if ($this->input->post('sarana_olah_limbah') == 0) {
				$pencemaran_air['sarana_jenis'] = null;
				$pencemaran_air['sarana_kapasitas'] = null;
				$pencemaran_air['sarana_koord_derajat_s'] = null;
				$pencemaran_air['sarana_koord_jam_s'] = null;
				$pencemaran_air['sarana_koord_menit_s'] = null;
				$pencemaran_air['sarana_koord_derajat_e'] = null;
				$pencemaran_air['sarana_koord_jam_e'] = null;
				$pencemaran_air['sarana_koord_menit_e'] = null;
			}
		
		#insert pencemaran_air
		$id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);
		
		# array for pencemaran_udara
		$pencemaran_udara = array(
			'id_bap' => $id_bap,
			'pelaporan_ua_ue' => $this->input->post('pelaporan_ua_ue'),
			'lain_lain' => $this->input->post('lain_lain_pu')
			);

		#insert pencemaran_udara
		$id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);
		
		# collecting data for uji_ambien
		$cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
		$uji_ambien_list = array();
		
		foreach ($cols as $i => $val) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $val,
				'uji_kualitas' => $this->input->post('uji_kualitas'.$i),
				'period' => $this->input->post('period'.$i),
				'lab' => $this->input->post('lab'.$i),
				'bm_pemenuhan' => $this->input->post('bm_pemenuhan'.$i),
				'bm_param' => $this->input->post('bm_param'.$i),
				'bm_period' => $this->input->post('bm_period'.$i)
				);
			
			$uji_ambien_list[] = $uji_ambien;

			#insert uji_ambien
			$this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
		}

		$padat_jenis_tmp = $this->input->post('padat_jenis');
		$empty = '';
		foreach ($padat_jenis_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jenis = rtrim($empty, ', ');

		$padat_jumlah_tmp = $this->input->post('padat_jumlah');
		$empty = '';
		foreach ($padat_jumlah_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jumlah = rtrim($empty, ', ');

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'padat_jenis' => $padat_jenis,
			'padat_jumlah' => $padat_jumlah,
			'padat_sarana_tong' => $this->input->post('padat_sarana_tong'),
			'padat_sarana_tps' => $this->input->post('padat_sarana_tps'),
			
			'lain_lain' => $this->input->post('lain_lain_pb3')
			);

		if ($this->input->post('padat_kelola1') != 'Lainnya') {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola1');
		} else {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola2');
		}

		#insert pencemaran_pb3
		$id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

        $this->bapDao->collectStatus($id_bap, "bap_golf");

		# notification message
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diunggah'));
			
		redirect('backend/bap_golf');
	}

	public function edit() {
		# get id bap from view
        $last_insert = false;
		$id_bap = $this->input->post('id_bap');
		
		// redirect if id_bap is null
		if (empty($id_bap)) {
	        redirect('backend/bap_golf');
		};

        $check = $this->bapDao->collectStatus($id_bap, "bap_golf");

        if ($check['status']) {

            $bap = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);

            $petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

            # get all industri
            $industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

            # get all pegawai
            $pegawai = $this->empDao->get_all_employee();

            $bap_golf = $this->bapDao->get_obj_by_id("bap_golf", "id_bap", $id_bap);

            $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

            $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);


            # get all lab
            $data['lab'] = $this->bapDao->select_table('laboratorium');

            # get from data acuan
            $data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
            $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
            $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
            $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");

            $data['bap'] = $bap;
            $data['bap_golf'] = $bap_golf;
            $data['industri'] = $industri;
            $data['pegawai'] = $pegawai;
            $data['petugas_bap'] = $petugas_bap;
            $data['pencemaran_air'] = $pencemaran_air;
            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
            $data['pencemaran_pb3'] = $pencemaran_pb3;

            # load view
            $this->load->view('include/header');
            $this->load->view('include/footer');
            $this->load->view('backend/v_bap_golf_edit', $data);
        } else {
            $this->partialEdit($id_bap, $check, $last_insert);
        }
	}

	// by guntur
	public function tipe_edit() {
        $last_insert = false;
		if($this->input->post('id_last_input') != null) {

			$id_last_input = $this->input->post('id_last_input');
			$jenis_bap = 'bap_golf';
			$id_bap = $this->indDao->get_last_input_id_bap($id_last_input, $jenis_bap);
            $last_insert = true;
			if($id_bap == null){

				$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Data BAP Golf pada Nama Usaha/Kegiatan yang anda pilih tidak ditemukan!, silahkan masukkan data baru, atau pilih Nama Usaha/Kegiatan yang lain'));
			
				redirect('backend/bap_golf');
			}
		} else {

			$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Silahkan pilih Nama Usaha/Kegiatan terlebih dahulu!'));
			
			redirect('backend/bap_golf');
		}

        $check = $this->bapDao->collectStatus($id_bap, "bap_golf");

        if ($check['status']) {

            // $bap = $this->bapDao->get_obj_by_id("bap", "id_bap", $id_bap);
            $bap = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);

            $petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

            # get all industri
            $industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

            # get all pegawai
            $pegawai = $this->empDao->get_all_employee();

            $bap_golf = $this->bapDao->get_obj_by_id("bap_golf", "id_bap", $id_bap);

            $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

            $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);


            # get all lab
            $data['lab'] = $this->bapDao->select_table('laboratorium');

            # get from data acuan
            $data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
            $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
            $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
            $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");

            $data['bap'] = $bap;
            $data['bap_golf'] = $bap_golf;
            $data['industri'] = $industri;
            $data['pegawai'] = $pegawai;
            $data['petugas_bap'] = $petugas_bap;
            $data['pencemaran_air'] = $pencemaran_air;
            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
            $data['pencemaran_pb3'] = $pencemaran_pb3;

            # load view
            $this->load->view('include/header');
            $this->load->view('include/footer');
            $this->load->view('backend/v_bap_golf_last_insert', $data);
        } else {
            $this->partialEdit($id_bap, $check, $last_insert);
        }
	}

	public function newSaveEdit() {
		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();

		# Get ID
		$id_bap = $this->input->post('id_bap');

		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			// 'url' => $this->input->post(''),
			// 'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => date("Y-m-d", strtotime($this->input->post('bap_tgl'))),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->input->post('dok_lingk'),
			'dok_lingk_jenis' => $this->input->post('dok_lingk_jenis'),
			'dok_lingk_tahun' => $this->input->post('dok_lingk_tahun'),
			'izin_lingk' => $this->input->post('izin_lingk'),
			'izin_lingk_tahun' => $this->input->post('izin_lingk_tahun'),
			'catatan' => $this->input->post('catatan'),
			'tgl_update' => $datenow,
			'diupdate_oleh' => $user
			);

		# edit bap
		$this->bapDao->edit('bap', 'id_bap', $id_bap, $bap);

		$bap['id_bap'] = $id_bap;

		# Petugas BAP update procedur
		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);

		$id_petugas_bap_list = $this->input->post('id_petugas_bap_list');

		if ($id_petugas_bap_list[0]!=null) {
			foreach ($id_petugas_bap_list as $idpbap) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $idpbap,
					'ket' => 'update',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);

				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
			}
		}

		# array for table bap_golf
		$bap_golf = array(
			'id_bap' => $id_bap,
			'jumlah_karyawan' => $this->input->post('jumlah_karyawan'),
			'jumlah_hole' => $this->input->post('jumlah_hole'),
			'rata_pengunjung' => $this->input->post('rata_pengunjung'),
			'jml_membership' => $this->input->post('jml_membership'),
			'pakai_pupuk' => $this->input->post('pakai_pupuk'),
			'pakai_pestisida' => $this->input->post('pakai_pestisida'),
			'pakai_oli' => $this->input->post('pakai_oli'),
			'simpan_oli_bekas' => $this->input->post('simpan_oli_bekas')
			);
		
		$id_bap_golf = $this->input->post('id_bap_golf');

		# edit bap_golf
		$this->bapDao->edit('bap_golf', 'id_bap_golf', $id_bap_golf, $bap_golf);

		$limb_sumber_array = $this->input->post('limb_sumber');
		$limb_sumber = '';
		foreach ($limb_sumber_array as $key) {
			$limb_sumber .= $key.", ";
		}

		$limb_sumber = rtrim($limb_sumber, ', ');

		# array for pencemaran_air
		$pencemaran_air = array(
			'id_bap' => $id_bap,
			'ambil_air_tanah' => $this->input->post('ambil_air_tanah'),
			'ambil_air_tanah_izin' => $this->input->post('ambil_air_tanah_izin'),
			'ambil_air_permukaan' => $this->input->post('ambil_air_permukaan'),
			'ambil_air_permukaan_izin' => $this->input->post('ambil_air_permukaan_izin'),
			'ambil_air_pdam' => $this->input->post('ambil_air_pdam'),
			'ambil_air_lain' => $this->input->post('ambil_air_lain'),
			'limb_sumber' => $limb_sumber,
			'bdn_terima' => $this->input->post('bdn_terima'),
			'sarana_olah_limbah' => $this->input->post('sarana_olah_limbah'),
			'sarana_jenis' => $this->input->post('sarana_jenis'),
			'sarana_kapasitas' => $this->input->post('sarana_kapasitas'),
			'sarana_koord_derajat_s' => $this->input->post('sarana_koord_derajat_s'),
			'sarana_koord_jam_s' => $this->input->post('sarana_koord_jam_s'),
			'sarana_koord_menit_s' => $this->input->post('sarana_koord_menit_s'),
			'sarana_koord_derajat_e' => $this->input->post('sarana_koord_derajat_e'),
			'sarana_koord_jam_e' => $this->input->post('sarana_koord_jam_e'),
			'sarana_koord_menit_e' => $this->input->post('sarana_koord_menit_e')
			);

			if ($this->input->post('sarana_olah_limbah') == 0) {
				$pencemaran_air['sarana_jenis'] = null;
				$pencemaran_air['sarana_kapasitas'] = null;
				$pencemaran_air['sarana_koord_derajat_s'] = null;
				$pencemaran_air['sarana_koord_jam_s'] = null;
				$pencemaran_air['sarana_koord_menit_s'] = null;
				$pencemaran_air['sarana_koord_derajat_e'] = null;
				$pencemaran_air['sarana_koord_jam_e'] = null;
				$pencemaran_air['sarana_koord_menit_e'] = null;
			}
		
		$id_penc_air = $this->input->post('id_penc_air');

		# edit pencemaran_air
		$this->bapDao->edit('pencemaran_air', 'id_penc_air', $id_penc_air, $pencemaran_air);

		# array for pencemaran_udara
		$pencemaran_udara = array(
			'id_bap' => $id_bap,
			'pelaporan_ua_ue' => $this->input->post('pelaporan_ua_ue'),
			'lain_lain' => $this->input->post('lain_lain_pu')
			);

		$id_penc_udara = $this->input->post('id_penc_udara');

		# edit pencemaran_udara
		$this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $id_penc_udara, $pencemaran_udara);
		
		# collecting data for uji_ambien
		$cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
		$uji_ambien_list = array();
		
		foreach ($cols as $i => $val) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $val,
				'uji_kualitas' => $this->input->post('uji_kualitas'.$i),
				'period' => $this->input->post('period'.$i),
				'lab' => $this->input->post('lab'.$i),
				'bm_pemenuhan' => $this->input->post('bm_pemenuhan'.$i),
				'bm_param' => $this->input->post('bm_param'.$i),
				'bm_period' => $this->input->post('bm_period'.$i)
				);
			
			$id_uji_ambien = $this->input->post('id_uji_ambien'.$i);
			
			# edit uji_ambien
			$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien, $uji_ambien);

			$uji_ambien_list[] = $uji_ambien;
		}

		$padat_jenis_tmp = $this->input->post('padat_jenis');
		$empty = '';
		foreach ($padat_jenis_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jenis = rtrim($empty, ', ');

		$padat_jumlah_tmp = $this->input->post('padat_jumlah');
		$empty = '';
		foreach ($padat_jumlah_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jumlah = rtrim($empty, ', ');

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'padat_jenis' => $padat_jenis,
			'padat_jumlah' => $padat_jumlah,
			'padat_sarana_tong' => $this->input->post('padat_sarana_tong'),
			'padat_sarana_tps' => $this->input->post('padat_sarana_tps'),
			
			'lain_lain' => $this->input->post('lain_lain_pb3')
			);

		if ($this->input->post('padat_kelola1') != 'Lainnya') {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola1');
		} else {
			$pencemaran_pb3['padat_kelola'] = $this->input->post('padat_kelola2');
		}
		
		$id_penc_pb3 = $this->input->post('id_penc_pb3');

		# edit pencemaran_pb3
		$this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $id_penc_pb3, $pencemaran_pb3);

		# Compare BAP
		$data['bap'] = $bap;
		$data['pencemaran_air'] = $pencemaran_air;
		$data['uji_ambien'] = $uji_ambien_list;
		$data['pencemaran_pb3'] = $pencemaran_pb3;
		$data['datenow'] = $datenow;
		$data['user'] = $user;

		$total_error = $this->compareBap($data, 'update');


		# error message
		if ($total_error > 0) {
			$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP: '.$total_error.' item'));
		}

		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data BAP berhasil diperbaharui'));
			
		redirect('backend/bap_golf');
	}

	# delete bap
	public function delete() {
		$id_bap = $this->input->post('id');

		$this->bapDao->delete('bap_golf', 'id_bap', $id_bap);

		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('surat_teguran_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);

		$this->bapDao->delete_penc_air($id_bap);
		$this->bapDao->delete_penc_udara($id_bap);
		$this->bapDao->delete_penc_pb3($id_bap);

		$this->bapDao->delete('bap', 'id_bap', $id_bap);
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/bap_golf');
	}

	# function to compare bap value with item ketaatan bap
	public function compareBap($data, $action) {
		$item_teguran = array();
		$parameter_bap = $this->lapDao->get_ketaatan_bap('bap_golf');

		foreach ($parameter_bap as $p) {
			$param[$p->ket] = $p->nilai_parameter_bap;
			$stat[$p->ket] = $p->status;
			$parent[$p->ket] = new StdClass;
			$parent[$p->ket]->one = $p->id_parent_1;
			$parent[$p->ket]->two = $p->id_parent_2;
		}

		$bap = $data['bap'];
		$pencemaran_air = $data['pencemaran_air'];
		$uji_ambien = $data['uji_ambien'];
		$pencemaran_pb3 = $data['pencemaran_pb3'];
		$datenow = $data['datenow'];
		$user = $data['user'];

		$id_bap = $bap['id_bap'];

		if ($stat['Dokumen Lingkungan']) {
			if ($this->valChecker($this->adaConv($bap['dok_lingk']),$param['Dokumen Lingkungan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Dokumen Lingkungan'],
					'nilai_parameter' => $this->adaConv($bap['dok_lingk']),
					'ket_parameter_bap' => 'Dokumen Lingkungan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Dokumen Lingkungan']->one,
					'id_parent_2' => $parent['Dokumen Lingkungan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Izin Lingkungan']) {
			if ($this->valChecker($this->adaConv($bap['izin_lingk']),$param['Izin Lingkungan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Izin Lingkungan'],
					'nilai_parameter' => $this->adaConv($bap['izin_lingk']),
					'ket_parameter_bap' => 'Izin Lingkungan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Izin Lingkungan']->one,
					'id_parent_2' => $parent['Izin Lingkungan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Sarana Pengolahan Air Limbah']) {
			if ($this->valChecker($this->adaConv($pencemaran_air['sarana_olah_limbah']),$param['Sarana Pengolahan Air Limbah'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Sarana Pengolahan Air Limbah'],
					'nilai_parameter' => $this->adaConv($pencemaran_air['sarana_olah_limbah']),
					'ket_parameter_bap' => 'Sarana Pengolahan Air Limbah',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Sarana Pengolahan Air Limbah']->one,
					'id_parent_2' => $parent['Sarana Pengolahan Air Limbah']->two
					);
				$item_teguran[] = $item;
			}
		}

		foreach ($uji_ambien as $ua) {
			if ($stat['Pengujian Kualitas']) {
				if ($this->valChecker($this->adaConv($ua['uji_kualitas']),$param['Pengujian Kualitas'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pengujian Kualitas'],
						'nilai_parameter' => $this->adaConv($ua['uji_kualitas']),
						'ket_parameter_bap' => 'Pengujian Kualitas '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pengujian Kualitas']->one,
						'id_parent_2' => $parent['Pengujian Kualitas']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Periode pengujian (per 6 bulan)']) {
				if ($this->valChecker($ua['period'],$param['Periode pengujian (per 6 bulan)'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Periode pengujian (per 6 bulan)'],
						'nilai_parameter' => $ua['period'],
						'ket_parameter_bap' => 'Periode pengujian (per 6 bulan) '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Periode pengujian (per 6 bulan)']->one,
						'id_parent_2' => $parent['Periode pengujian (per 6 bulan)']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Pemenuhan BM']) {
				if ($this->valChecker($this->yaConv($ua['bm_pemenuhan']),$param['Pemenuhan BM'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pemenuhan BM'],
						'nilai_parameter' => $this->yaConv($ua['bm_pemenuhan']),
						'ket_parameter_bap' => 'Pemenuhan BM '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pemenuhan BM']->one,
						'id_parent_2' => $parent['Pemenuhan BM']->two
						);
					$item_teguran[] = $item;
				}
			}
		}

		if ($stat['Pengelolaan']) {
			if (strcasecmp($pencemaran_pb3['padat_kelola'],$param['Pengelolaan'])==0) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Pengelolaan'],
					'nilai_parameter' => $pencemaran_pb3['padat_kelola'],
					'ket_parameter_bap' => 'Pengelolaan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Pengelolaan']->one,
					'id_parent_2' => $parent['Pengelolaan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Lain - Lain']) {
			if (!empty($bap['catatan']) ) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => ' - ',
					'nilai_parameter' => $bap['catatan'],
					'ket_parameter_bap' => 'Lain - Lain',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Lain - Lain']->one,
					'id_parent_2' => $parent['Lain - Lain']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($action=='update') {
			$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);
		}

		foreach ($item_teguran as $item) {
			#insert history_item_teguran_bap
			$this->bapDao->insert_getStat('history_item_teguran_bap', $item);
		}

        /*update status is_compared to 1*/
        $this->bapDao->updateIsCompared($id_bap);

		return count($item_teguran);
	}

	public function getPelanggaran() {
		$id_bap = $this->input->post('id_bap');

		$data['industri'] = $this->input->post('industri');
		$data['tgl'] = $this->input->post('tgl');
		$data['teguran'] = $this->bapDao->get_list_by_id("history_item_teguran_bap", "id_bap", $id_bap);

	    echo json_encode($data);
		exit();
	}

	# function to check equality of value (string) (case-insensitive)
	# return FALSE if value equals
	public function valChecker($str1, $str2) {
		if (strcasecmp($str1, $str2) == 0) {
			return false;
		} else {
			return true;
		}
	}
	
	# function to convert string to boolean
	private function boolConverter($value) {
		if (strcasecmp($value, "ada") == 0 || strcasecmp($value, "ya") == 0) {
			return true;
		} else {
			return false;
		}
	}

    # function to convert boolean to string (Ada / Tidak Ada)
	private function adaConv($value) {
        if ($value == true) {
            return "Ada";
        } else {
            return "Tidak Ada";
        }
    }

    # function to convert boolean to string (Ya / Tidak)
    private function yaConv($value) {
        if ($value == true) {
            return "Ya";
        } else {
            return "Tidak";
        }
    }

    public function exit_check_status($id_bap) {
        $this->bapDao->collectStatus($id_bap, "bap_golf");
        redirect("backend/bap_golf");
    }

    public function save_part() {
        $stat['status'] = false;

        $posted = $this->input->post('data');
        $ids = $this->input->post('ids');
        parse_str($posted, $data);

        if(!empty($ids)) {
            if ($this->input->post('dest') == "datum") {
                $stat['id'] = $this->upsert_bap($data, $ids);
                $stat['status'] = true;
            } elseif ($this->input->post('dest') == "pencemaran_air") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_air($data, $ids);
                    $stat['status'] = true;
                }
            } elseif ($this->input->post('dest') == "pencemaran_udara") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_udara($data, $ids);
                    $stat['status'] = true;
                }
            } elseif ($this->input->post('dest') == "pencemaran_b3") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_b3($data, $ids);
                    $stat['status'] = true;
                }
            }
        }

        echo json_encode($stat);
    }

    public function upsert_bap($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # data bap
        $bap = array(
            'id_industri' => $data['id_industri'],
            'id_pegawai' => $data['id_pegawai'],
            'bap_tgl' => date("Y-m-d", strtotime($data['bap_tgl'])),
            'bap_jam' => $data['bap_jam'],
            'p2_nama' => $data['p2_nama'],
            'p2_jabatan' => $data['p2_jabatan'],
            'p2_telp' => $data['p2_telp'],
            'dok_lingk' => $data['dok_lingk'],
            'dok_lingk_jenis' => $data['dok_lingk_jenis'],
            'dok_lingk_tahun' => $data['dok_lingk_tahun'],
            'izin_lingk' => $data['izin_lingk'],
            'izin_lingk_tahun' => $data['izin_lingk_tahun'],
            'tgl_pembuatan' => $datenow,
            'dibuat_oleh' => $user,
            'catatan' => $data['catatan']
        );

        # data bap_golf
        $bap_golf = array(
            /*'id_bap' => $ids['id_bap'],*/
            'jumlah_karyawan' => $data['jumlah_karyawan'],
            'jumlah_hole' => $data['jumlah_hole'],
            'rata_pengunjung' => $data['rata_pengunjung'],
            'jml_membership' => $data['jml_membership'],
            'pakai_pupuk' => $data['pakai_pupuk'],
            'pakai_pestisida' => $data['pakai_pestisida'],
            'pakai_oli' => $data['pakai_oli'],
            'simpan_oli_bekas' => $data['simpan_oli_bekas']
        );

        # data petugas bap
        $id_petugas_bap = isset($data['petugas_pengawas']) ? $data['petugas_pengawas'] : null;

        if($ids['id_bap']=="") {
            $id_bap = $this->bapDao->insert_getID('bap',$bap);

            $bap_golf['id_bap'] = $id_bap;
            $id_bap_golf = $this->bapDao->insert_getID('bap_golf', $bap_golf);

            if ($id_petugas_bap[0] != null) {
                $datenow = date('Y-m-d');

                foreach ($id_petugas_bap as $idpbap) {
                    $petugas_bap = array(
                        'id_bap' => $id_bap,
                        'id_pegawai' => $idpbap,
                        'tgl_pembuatan' => $datenow,
                        'dibuat_oleh' => $user
                    );

                    #insert petugas_bap
                    $this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
                }
            }

            $ret_ids['id_bap'] = $id_bap;
            $ret_ids['id_bap_golf'] = $id_bap_golf;
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;
            $this->bapDao->edit('bap', 'id_bap', $ids['id_bap'], $bap);
            $this->bapDao->edit('bap_golf', 'id_bap_golf', $ids['id_bap_golf'], $bap_golf);

            $this->bapDao->delete('petugas_bap', 'id_bap', $ids['id_bap']);

            if ($id_petugas_bap[0] != null) {
                $datenow = date('Y-m-d');

                foreach ($id_petugas_bap as $idpbap) {
                    $petugas_bap = array(
                        'id_bap' => $ids['id_bap'],
                        'id_pegawai' => $idpbap,
                        'tgl_pembuatan' => $datenow,
                        'dibuat_oleh' => $user
                    );

                    #insert petugas_bap
                    $this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
                }
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_air($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # sumber limbah
        $limb_sumber = isset($data['limb_sumber']) ? $data['limb_sumber'] : "";
        if (!empty($limb_sumber )) {
            $temp = '';
            foreach ($limb_sumber as $key) {
                $temp .= $key . ", ";
            }

            $finlim_sumber = rtrim($temp, ', ');
        } else { $finlim_sumber = ""; }

        # array for pencemaran_air
        $pencemaran_air = array(
            'id_bap' => $ids['id_bap'],
            'ambil_air_tanah' => @$data['ambil_air_tanah'],
            'ambil_air_tanah_izin' => @$data['ambil_air_tanah_izin'],
            'ambil_air_permukaan' => @$data['ambil_air_permukaan'],
            'ambil_air_permukaan_izin' => @$data['ambil_air_permukaan_izin'],
            'ambil_air_pdam' => @$data['ambil_air_pdam'],
            'ambil_air_lain' => @$data['ambil_air_lain'],
            'limb_sumber' => $finlim_sumber,
            'bdn_terima' => @$data['bdn_terima'],
            'sarana_olah_limbah' => @$data['sarana_olah_limbah'],
            'sarana_jenis' => @$data['sarana_jenis'],
            'sarana_kapasitas' => @$data['sarana_kapasitas'],
            'sarana_koord_derajat_s' => @$data['sarana_koord_derajat_s'],
            'sarana_koord_jam_s' => @$data['sarana_koord_jam_s'],
            'sarana_koord_menit_s' => @$data['sarana_koord_menit_s'],
            'sarana_koord_derajat_e' => @$data['sarana_koord_derajat_e'],
            'sarana_koord_jam_e' => @$data['sarana_koord_jam_e'],
            'sarana_koord_menit_e' => @$data['sarana_koord_menit_e']
        );

        if (@$data['sarana_olah_limbah'] == 0) {
            $pencemaran_air['sarana_jenis'] = null;
            $pencemaran_air['sarana_kapasitas'] = null;
            $pencemaran_air['sarana_koord_derajat_s'] = null;
            $pencemaran_air['sarana_koord_jam_s'] = null;
            $pencemaran_air['sarana_koord_menit_s'] = null;
            $pencemaran_air['sarana_koord_derajat_e'] = null;
            $pencemaran_air['sarana_koord_jam_e'] = null;
            $pencemaran_air['sarana_koord_menit_e'] = null;
        }

        if($ids['id_pencemaran_air']=="") {
            $id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = $id_penc_air;
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            $this->bapDao->edit('pencemaran_air', 'id_penc_air', $ids['id_pencemaran_air'], $pencemaran_air);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = $ids['id_pencemaran_air'];
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_udara($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # array for pencemaran_udara
        $pencemaran_udara = array(
            'id_bap' => $ids['id_bap'],
            'pelaporan_ua_ue' => @$data['pelaporan_ua_ue'],
            'lain_lain' => @$data['lain_lain_pu']
        );

        if($ids['id_pencemaran_udara']=="") {
            # insert pencemaran_udara
            $id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);

            # collecting data for uji_ambien
            $cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
            $uji_ambien_list = array();

            foreach ($cols as $i => $val) {
                $uji_ambien = array(
                    'id_penc_udara' => $id_penc_udara,
                    'lokasi' => $val,
                    'uji_kualitas' => $data['uji_kualitas'.$i],
                    'period' => $data['period'.$i],
                    'lab' => $data['lab'.$i],
                    'bm_pemenuhan' => $data['bm_pemenuhan'.$i],
                    'bm_param' => $data['bm_param'.$i],
                    'bm_period' => $data['bm_period'.$i]
                );

                $uji_ambien_list[] = $uji_ambien;

                #insert uji_ambien
                $this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = $id_penc_udara;
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            # edit pencemaran_udara
            $this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $ids['id_pencemaran_udara'], $pencemaran_udara);

            # collecting data for uji_ambien
            $cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
            $uji_ambien_list = array();

            for($i=1; $i<=3; $i++){
                $id_uji_ambien[$i] = @$data['id_uji_ambien'.$i];
            }

            $this->bapDao->delete('uji_ambien', 'id_penc_udara', $ids['id_pencemaran_udara']);

            $i = 1;
            foreach ($cols as $i => $val) {
                $uji_ambien = array(
                    'id_penc_udara' => $ids['id_pencemaran_udara'],
                    'lokasi' => $val,
                    'uji_kualitas' => @$data['uji_kualitas'.$i],
                    'period' => @$data['period'.$i],
                    'lab' => @$data['lab'.$i],
                    'bm_pemenuhan' => @$data['bm_pemenuhan'.$i],
                    'bm_param' => @$data['bm_param'.$i],
                    'bm_period' => @$data['bm_period'.$i]
                );

                $uji_ambien_list[] = $uji_ambien;

                #insert uji_ambien
                //$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien[$i], $uji_ambien);
                $this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
                $i++;
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = $ids['id_pencemaran_udara'];
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_b3($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        $padat_jenis_tmp = $data['padat_jenis'];
        $empty = '';
        foreach ($padat_jenis_tmp as $key) {
            $empty .= $key.', ';
        }
        $padat_jenis = rtrim($empty, ', ');

        $padat_jumlah_tmp = $data['padat_jumlah'];
        $empty = '';
        foreach ($padat_jumlah_tmp as $key) {
            $empty .= $key.', ';
        }
        $padat_jumlah = rtrim($empty, ', ');

        # array for pencemaran_pb3
        $pencemaran_pb3 = array(
            'id_bap' => $ids['id_bap'],
            'padat_jenis' => $padat_jenis,
            'padat_jumlah' => $padat_jumlah,
            'padat_sarana_tong' => @$data['padat_sarana_tong'],
            'padat_sarana_tps' => @$data['padat_sarana_tps'],

            'lain_lain' => $data['lain_lain_pb3']
        );

        if ($data['padat_kelola1'] != 'Lainnya') {
            $pencemaran_pb3['padat_kelola'] = $data['padat_kelola1'];
        } else {
            $pencemaran_pb3['padat_kelola'] = $data['padat_kelola2'];
        }

        if ($ids['id_plb3']=="") {

            # insert pencemaran_pb3
            $id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = $id_penc_pb3;
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            #insert pencemaran_pb3
            $this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $ids['id_plb3'], $pencemaran_pb3);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_golf'] = $ids['id_bap_golf'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = $ids['id_plb3'];
        }

        return $ret_ids;
    }

    public function getComparableStatus() {
        $data = $this->bapDao->collectStatus($this->input->post("id_bap"), "bap_golf");
        echo json_encode($data);
    }

    public function newCompare() {
        date_default_timezone_set("Asia/Jakarta");

        /* ----------------------------------------------- */
        /* BEGIN COMPARE BAP */
        /* ----------------------------------------------- */
        /*data umum & bap */
        $data['bap'] = $this->bapDao->select_table_array("bap", "id_bap", $this->input->post("id"), FALSE);

        /*pencemaran air*/
        $data['pencemaran_air'] = $this->bapDao->select_table_array("pencemaran_air", "id_bap", $this->input->post("id"), FALSE);

        /*pencemaran_udara*/
        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $this->input->post("id"));
        $data['uji_ambien'] = $this->bapDao->select_table_array("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara, TRUE);

        /*pencemaran pb3*/
        $data['pencemaran_pb3'] = $this->bapDao->select_table_array("pencemaran_pb3", "id_bap", $this->input->post("id"), FALSE);

        /*additional data*/
        $data['datenow'] = date("Y-m-d H:i:s:");
        $data['user'] = $this->tank_auth->get_personname();
        //echo "<pre>"; print_r($data); die();

        $total_error = $this->compareBap($data, 'update');

        if ($total_error > 0) {
            $this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP: '.$total_error.' item'));
        } else {
            $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Tidak ditemukan pelanggaran'));
        }

        /* ----------------------------------------------- */
        /* END COMPARE BAP */
        /* ----------------------------------------------- */

        redirect('backend/bap_golf');
    }

    public function partialEdit($id_bap, $status, $last_insert) {
        // ---------------------------------- DATA ACUAN TJOY ---------------------------------- //

        /*param for nullify all the ids in view*/
        if ($last_insert) {
            $data['last_insert'] = $last_insert;
        }

        $data['day_id'] = $this->day_id;
        $data['lab'] = $this->bapDao->select_table('laboratorium');
        $data['jenis_dok_lingkungan'] = $this->m_data_master->get_dropdown("jenis_dok_lingkungan");
        $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
        $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
        $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");

        // ---------------------------------- DATA ACUAN TJOY ---------------------------------- //

        if(!empty($status['bap']) AND !empty($status['bap_golf'])) { // data umum and bap

            $bap_golf = $this->bapDao->get_obj_by_id("bap_golf", "id_bap", $id_bap);
            $data['bap'] = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);
            $data['bap_golf'] = $bap_golf;
            $data['industri'] = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);
            $data['pegawai'] = $this->empDao->get_all_employee();
            $data['petugas_bap'] = $this->bapDao->get_petugas_bap($id_bap);
        }

        if(!empty($status['pencemaran_air'])) { // pencemaran air
            $data['pencemaran_air'] = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);
        }

        if(!empty($status['pencemaran_udara'])) { // pencemaran udara
            $data['pencemaran_udara'] = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
            $data['uji_ambien'] = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $data['pencemaran_udara']->id_penc_udara);
        }

        if(!empty($status['pencemaran_pb3'])) { // pencemaran b3
            $data['pencemaran_pb3'] = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
        }

        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/bap_golf_partial/v_bap_golf_partial', $data);
    }
}
