<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAP BPO&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Bahan Perusak Ozon</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen BAP</li>
                <li class="active">BAP BPO</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#list" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;&nbsp;Daftar BAP</a></li>
                    <li><a href="#add-new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Formulir BAP</a></li>                   
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="list">
                         <div class="table-responsive">
                            <table class="table table-striped table-hover" id="datatable-example">
                                <thead class="the-box dark full">
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Nama Perusahaan</th>
                                        <th>Tanggal </th>
                                        <th class="text-right"></th>                                    
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div>
                    <div class="tab-pane" id="add-new">
                        <form role="form" action="<?php echo base_url('bap_bpo/insert') ?>" method="post" id="form_add">
                            <div class="modal-body">                                                                            
                                <div class="form-group ">
                                    <label>Hari</label>
                                    <input type="text" name="" id="" class="form-control input-sm has-feedback" autofocus />
                                </div>
                                <div class="form-group ">
                                    <label>Tanggal</label>
                                    <input type="text" class="form-control input-sm datepicker" data-date-format="DD-MM-YYYY" placeholder="DD-MM-YYYY">                  
                                </div>
                                <div class="form-group ">
                                    <label>No. Surat Tugas</label>
                                    <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                </div>
                                <div class="form-group ">
                                    <label>Tanggal Surat Tugas</label>
                                    <input type="text" class="form-control input-sm datepicker" data-date-format="DD-MM-YYYY" placeholder="DD-MM-YYYY">                  
                                </div>
                                <fieldset>
                                    <legend>Pihak II</legend>
                                    <div class="form-group ">
                                        <label>Nama</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Jabatan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Alamat</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div> 
                                </fieldset>
                                <fieldset>
                                    <legend>Data Umum Perusahaan</legend>
                                    <div class="form-group ">
                                        <label>Nama Perusahaan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Jenis Usaha</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Alamat</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Telp</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Fax</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Desa/Kelurahan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Kecamatan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Penanggung Jawab Usaha</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Jumlah Karyawan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Hari dan Jam Kerja / Bulan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Dokumen / Perijinan yang dimiliki</legend>
                                    <div class="form-group ">
                                        <label>1. Jenis Dokumen</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Instansi yang mengesahkan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Nomor / Tanggal Pengesahan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>2. Jenis Dokumen</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Instansi yang mengesahkan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Nomor / Tanggal Pengesahan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Data Hasil Pengawasan</legend>
                                    <label>Data Tabung Refrigeran</label>
                                    <div class="form-group ">
                                        <label>a. Warna</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>b. Label</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>c. Merk</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>d. Manufaktur</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>e. Berat Tabung (kapasitas)</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Jenis Refrigeran</label>
                                        <select class="form-control input-sm">
                                            <option value="CFC">CFC</option>
                                            <option value="HCFC">HCFC</option>
                                            <option value="HFC">HFC</option>
                                            <option value="Hidrokarbon">Hidrokarbon</option>
                                        </select>
                                    </div>
                                    <label>Harga Refrigeran</label>
                                    <div class="form-group ">
                                        <label>a. CFC</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>b. HCFC</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>c. HFC</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>d. Hidrokarbon</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>e. Lain-lain</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Sumber Pembelian Refrigeran (Nama dan Alamat)</label>
                                        <textarea class="form-control input-sm no-resize bold-border"></textarea>
                                    </div>
                                    <div class="form-group ">
                                        <label>Jumlah Penggunaan / Penjualan BPO Rata-rata/bulan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <label>Pengamatan Umum Kondisi Bengkel</label>
                                    <div class="form-group ">
                                        <label>a. Bahan Kimia yang digunakan dalam proses flushing sistem pendingin</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>b. Peralatan recovery / recycling / recharging refrigeran</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="pr4a" value="ada">Ada
                                            </label>
                                            <label>
                                                <input type="radio" name="pr4a" value="tidak" checked>Tidak Ada
                                            </label>
                                            <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                            <label>
                                                <input type="radio" name="pr4b" value="dioperasikan">Dioperasikan
                                            </label>
                                            <label>
                                                <input type="radio" name="pr4b" value="tidak" checked>Tidak
                                            </label>
                                            <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label>c. Pengetahuan Penanggung Jawab / Teknisi tentang Isu Kerusakan Ozon</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <label>Pengelolaan Oli Bekas</label>
                                    <div class="form-group ">
                                        <label>a. Sumber</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>b. Jumlah oli bekas / bulan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>c. Pembuangan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>d. Jenis lain-lain</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label>Laboratorium Pengujian</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Hasil Uji Kualitas Air Limbah</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Catatan Tambahan Pelaksana Pengawas BPO</legend>
                                    <div class="form-group ">
                                        <textarea class="form-control input-sm no-resize bold-border"></textarea>
                                    </div>
                                </fieldset>

                                <!-- <fieldset>
                                    <legend></legend>
                                    <div class="form-group ">
                                        <label></label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                </fieldset>  -->                                                                    
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="edit_id" name="id" value="" />
                                <button type="submit" name="register" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>                    
                </div>

                
              <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                <br/><br/> -->

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
	$(document).ready(function () {
		
		$("#form_add").validate({
            rules: {
                personname: {
                    required: true,
					minlength: 3,
                    maxlength: 100
                },
                username: {
					required: true,
					minlength: 3,
                    maxlength: 100
				},
				password: {
					required: true,
					minlength: 6,
                    maxlength: 100
				},
				confirm_password: {
					required: true,
					equalTo: "#password",
					minlength: 6,
                    maxlength: 100
				},
				email: {
					required: true,
					email: true
				},
				user_level: {
					required: true
				}
            },
            messages: {        
                personname: {
                    required: "Please enter a valid name.",
                    minlength: "Name must be at least 3 characters in length.",
                    maxlength: "Name must be less than 100 characters in length."
                },
				username: {
                    required: "Please enter a valid name.",
                    minlength: "Name must be at least 3 characters in length.",
                    maxlength: "Name must be less than 100 characters in length."
                },
				password: {
                    required: "Please enter a valid name.",
                    minlength: "Name must be at least 6 characters in length.",
                    maxlength: "Name must be less than 100 characters in length."
                },
				confirm_password: {
                    required: "Please enter a valid name.",
                    minlength: "Name must be at least 6 characters in length.",
                    maxlength: "Name must be less than 100 characters in length."
                },
				email: {
					required: "Please enter a valid email address",
				},
				user_level: {
                    required: "Please select data."
                },
            }
        });
		
		$('.edit-row').click(function (e) {
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: '<?php echo base_url('/backend/user/get'); ?>',
				data: 'id=' + $(this).data('id'),
				success: function (response) {
					console.log(response);
					$('input[id=edit_id]').val(response.id);
					$('input[name=personname]').val(response.name);
					$('input[name=username]').val(response.username);
					$('input[name=email]').val(response.email);
					$('select[name=user_level]').val(response.group_id);

					$('.password').hide();
					$('input[name=password]').prop('disabled', true);
					$('.confirm_password').hide();
					$('input[name=confirm_password]').prop('disabled', true);

					$('#myModal form').attr('action', '<?php echo base_url('/backend/user/edit') ?>'); //this fails silently
					$('#myModal form').get(0).setAttribute('action', '<?php echo base_url('/backend/user/edit') ?>'); //this works

					$('#myModal').modal('show');
				}
			})
		});
		$('.reset-row').click(function (e) {

			$('input[id=edit_id]').val($(this).data('id'));

			$('.personname').hide();
			$('input[name=personname]').prop('disabled', true);
			$('.username').hide();
			$('input[name=username]').prop('disabled', true);
			$('.email').hide();
			$('input[name=email]').prop('disabled', true);
			$('.user_level').hide();
			$('select[name=user_level]').prop('disabled', true);
			$('.confirm_password').hide();
			$('input[name=confirm_password]').prop('disabled', true);

			$('#myModal form').attr('action', '<?php echo base_url('/backend/user/force_reset') ?>'); //this fails silently
			$('#myModal form').get(0).setAttribute('action', '<?php echo base_url('/backend/user/force_reset') ?>'); //this works

			$('#myModal').modal('show');
		});

		$('.delete-row').click(function (e) {
			$('input[id=deleted_id]').val($(this).data('id'));
			$('#delete').modal('show');
		});
		
        $('#cdh1').click(function (e) {
            $('#cdhrata').show();
        });
        $('#cdh2').click(function (e) {
            $('#cdhrata').hide();
        });
		 window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

	});
</script>


<!-- Modal -->


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete User</h4>
            </div>
            <form action="<?php echo base_url('/backend/user/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Are you sure you want to delete this data?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Delete</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal 