<?php

 // Document   : bap_rm.php 
 // Last edited: November 18, 2014
 // Author     : guntutur@gmail.com 
 // Description: Controller for BAP Rumah Makan 


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class bap_rm extends CI_Controller {

	private $day_id = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');
	private $dok_lingk_jenis = array('Amdal', 'UKL-UPL', 'DPLH', 'SPPL');
	private $padat_kelola = array('Dibakar', 'Diangkut oleh Pihak Ketiga', 'Diangkut oleh Dinas Pertasih', 'Dijual', 'Lainnya');
	private $lapor_ua_ue = array('Rutin', 'Tidak Rutin', 'Tidak Melaporkan');
	private $bool = array('0', '1');
	private $id_usaha_kegiatan = 19;

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_bap_rm', 'bapInDao', TRUE);
        $this->load->model('m_bap_parameter', 'bapPar', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);
        $this->load->model('m_employee', 'empDao', TRUE);
        $this->load->model('m_data_master', '', TRUE);
        $this->load->model('m_laporan_bap', 'lapDao', TRUE);
        // $this->load->model('m_bap_parameter');

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);

		$this->load->library('PHPExcel');
		// $this->load->helper('url');
    }

    # default view

    public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        # get all industri
        $bap_rm = $this->bapInDao->get_all();
		
		# get all industri
		// $id_usaha_kegiatan = 8;
		// $industri = $this->indDao->get_all_industri($id_usaha_kegiatan);
		$industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);

		# get all pegawai
		$pegawai = $this->empDao->get_all_employee();

		# get all lab
		$lab = $this->bapDao->select_table('laboratorium');

		$data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
		$data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
		$data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");

		# response paramter
		$data['day_id'] = $this->day_id;
        $data['bap_rm'] = $bap_rm;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;
        $data['lab'] = $lab;

        # parameter for view
        $data['tab_list'] = 'active';
		$data['tab_view'] = '';
		$data['tab_edit'] = '';    

		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_rm', $data);
    }

    public function download_form() {
    	# $this->load->helper('download');	
		
    	$data = "assets/form/BAPHotel.xls"; // Read the file's contents
		$name = 'BAPHotelform.xls';

		# force_download($name, $data);	
		
			
		$this->functions->download_file($data, $name, 'application/vnd.ms-excel');
    }

	public function view() {
    	# get id bap from view
		$id_bap = $this->input->post("id_bap");
    	$bap = $this->bapDao->get_obj_by_id("bap", "id_bap", $id_bap);
    	$bap_rm = $this->bapDao->get_obj_by_id("bap_rm", "id_bap", $id_bap);
    	$industri =	$this->bapDao->get_obj_by_id("industri", "id_industri", $bap->id_industri);
    	$pegawai = $this->bapDao->get_obj_by_id("pegawai", "id_pegawai", $bap->id_pegawai);
    	$petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

        $data['bap'] = $bap;
        $data['bap_rm'] = $bap_rm;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;
        $data['petugas_bap'] = $petugas_bap;

        /*pencemaran air*/
        $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);
        if ($pencemaran_air) {
            $data['pencemaran_air'] = $pencemaran_air;
        }

        /*pencemaran udara*/
        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
        if ($pencemaran_udara) {
            $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
        }

        /*pencemaran pb3*/
        $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
        if ($pencemaran_pb3) {
            $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
            $data['pencemaran_pb3'] = $pencemaran_pb3;
        }

    	# parameter for view
        $data['tab_list'] = '';
		$data['tab_view'] = 'active';
		$data['tab_edit'] = '';

        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_rm', $data);
	}

	public function insert_from_form() {

		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();

		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => $this->input->post('bap_tgl'),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->boolConverter($this->input->post('dl')),
			'dok_lingk_jenis' => $this->input->post('jdl_name'),
			'dok_lingk_tahun' => $this->input->post('dlt'),
			'izin_lingk' => $this->boolConverter($this->input->post('il')),
			'izin_lingk_tahun' => $this->input->post('il_tahun'),
			'tgl_pembuatan' => $datenow,
			'dibuat_oleh' => $user,
			'catatan' => $this->input->post('lain_lain_bap')
			);

		# insert bap
		$id_bap = $this->bapDao->insert_getID('bap',$bap);

		$bap['id_bap'] = $id_bap;

		# Petugas BAP Add procedur
		$id_petugas_bap = $this->input->post('petugas_pengawas');
		$datenow = date('Y-m-d');

		if($id_petugas_bap[0]!=null){
			foreach ($id_petugas_bap as $idpbap) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $idpbap,
					// 'ket' => '',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);

				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
			}
		}

		# array for table bap_rm
		$bap_rm = array(
			'id_bap' => $id_bap,
			'jml_meja' => $this->input->post('jml_meja'),
			'izin_usaha' => $this->boolConverter($this->input->post('iu')),
			'izin_usaha_tahun' => $this->input->post('iu_tahun'),
			'jumlah_karyawan' => $this->input->post('jml_karyawan')
			);

		#insert bap_rm
		$this->bapDao->insert_getStat('bap_rm', $bap_rm);

		$limb_sumber = $this->input->post('limb_sumber');
		$temp = '';
		foreach ($limb_sumber as $key) {
			$temp .= $key.", ";
		}

		$finlim_sumber = rtrim($temp, ', ');

		# array for pencemaran_air
		$pencemaran_air = array(
			'id_bap' => $id_bap,
			'ambil_air_tanah' => $this->input->post('air_tanah'),
			'ambil_air_tanah_izin' => $this->boolConverter($this->input->post('izin_air_tanah')),
			'ambil_air_permukaan' => $this->input->post('air_permukaan'),
			'ambil_air_permukaan_izin' => $this->boolConverter($this->input->post('izin_air_permukaan')),
			'ambil_air_pdam' => $this->input->post('air_pdam'),
			'ambil_air_lain' => $this->input->post('air_lain'),
			'limb_sumber' => $finlim_sumber,
			'bdn_terima' => $this->input->post('bdn_terima'),
			'sarana_olah_limbah' => $this->boolConverter($this->input->post('sarolim')),
			'sarana_jenis' => $this->input->post('sarana_jenis'),
			'sarana_kapasitas' => $this->input->post('kapasitas'),
			'sarana_koord_derajat_s' => $this->input->post('xkoord_outlet_derajat_s'),
			'sarana_koord_jam_s' => $this->input->post('xkoord_outlet_jam_s'),
			'sarana_koord_menit_s' => $this->input->post('xkoord_outlet_menit_s'),
			'sarana_koord_derajat_e' => $this->input->post('xkoord_outlet_derajat_e'),
			'sarana_koord_jam_e' => $this->input->post('xkoord_outlet_jam_e'),
			'sarana_koord_menit_e' => $this->input->post('xkoord_outlet_menit_e')
			);

		#insert pencemaran_air
		$id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);

		# get data pencamaran_udara
		$pencemaran_udara = array(
				'id_bap' => $id_bap,
				'pelaporan_ua_ue' => $this->input->post('pelaporan_ua_ue'),
				'lain_lain' => $this->input->post('lain_lain_pu')
			);

  		#insert to table pencemaran_udara
		$id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);

		# collecting data for uji_ambien
		$cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
		$uji_ambien_list = array();
		
		foreach ($cols as $i => $val) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $val,
				'uji_kualitas' => $this->boolConverter($this->input->post('uji_kualitas'.$i)),
				'period' => $this->input->post('period'.$i),
				'lab' => $this->input->post('lab'.$i),
				'bm_pemenuhan' => $this->boolConverter($this->input->post('bm_pemenuhan'.$i)),
				'bm_param' => $this->input->post('bm_param'.$i),
				'bm_period' => $this->input->post('bm_period'.$i)
				);
			
			$uji_ambien_list[] = $uji_ambien;

			#insert uji_ambien
			$this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
		}

		$padat_kelola = $this->input->post('padat_kelola');

		if ($padat_kelola == 'other') {
       		$padat_kelola = $this->input->post('padat_kelola_lain');
        } else {
        	$padat_kelola = $this->input->post('padat_kelola');
        }

        $padat_jenis_tmp = $this->input->post('padat_jenis');
		$empty = '';
		foreach ($padat_jenis_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jenis = rtrim($empty, ', ');

		$padat_jumlah_tmp = $this->input->post('padat_jumlah');
		$empty = '';
		foreach ($padat_jumlah_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jumlah = rtrim($empty, ', ');

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'padat_jenis' => $padat_jenis,
			'padat_jumlah' => $padat_jumlah,
			'padat_sarana_tong' => $this->input->post('padat_sarana_tong'),
			'padat_sarana_tps' => $this->input->post('padat_sarana_tps'), 
			'padat_kelola' => $padat_kelola,
			'lain_lain' => $this->input->post('lain_lain_padat')
			);

		#insert pencemaran_pb3
		$id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

        $this->bapDao->collectStatus($id_bap, "bap_rm");

		# notification message
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diunggah'));
			
		redirect('backend/bap_rm');
	}

	public function edit_from_form() {

		$id_bap = $this->input->post('id');

		// $total_error = $this->compare_item_ketaatan($id_bap, 'update');

		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();

		$id_bap_rm = $this->input->post('id_bap_rm');
		$id_penc_air = $this->input->post('id_penc_air');
		$id_penc_udara = $this->input->post('id_penc_udara');
		$id_penc_pb3 = $this->input->post('id_penc_pb3');

		$jdl = $this->input->post('jdl_name');

		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => $this->input->post('bap_tgl'),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->boolConverter($this->input->post('dl')),
			'dok_lingk_jenis' => $this->input->post('jdl_name'),
			'dok_lingk_tahun' => $this->input->post('dlt'),
			'izin_lingk' => $this->boolConverter($this->input->post('il')),
			'izin_lingk_tahun' => $this->input->post('il_tahun'),
			'tgl_pembuatan' => $datenow,
			'tgl_update' => $datenow,
			'dibuat_oleh' => $user,
			'diupdate_oleh' => $user,
			'catatan' => $this->input->post('lain_lain_bap')
			);

		# insert bap
		$this->bapDao->edit('bap', 'id_bap', $id_bap, $bap);
		// $datenow = date('Y-m-d');

		$bap['id_bap'] = $id_bap;

		# Petugas BAP update procedur
		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);

		# Petugas BAP Add procedur
		$id_petugas_bap = $this->input->post('petugas_pengawas');
		$datenow = date('Y-m-d');

		if($id_petugas_bap[0]!=null){
			foreach ($id_petugas_bap as $idpbap) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $idpbap,
					// 'ket' => '',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);

				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
			}
		}

		# array for table bap_rm
		$bap_rm = array(
			'id_bap' => $id_bap,
			'jml_meja' => $this->input->post('jml_meja'),
			'izin_usaha' => $this->boolConverter($this->input->post('iu')),
			'izin_usaha_tahun' => $this->input->post('iu_tahun'),
			'jumlah_karyawan' => $this->input->post('jml_karyawan')
			);

		#insert bap_rm
		$this->bapDao->edit('bap_rm', 'id_bap_rm', $id_bap_rm, $bap_rm);

		$limb_sumber = $this->input->post('limb_sumber');
		$temp = '';
		foreach ($limb_sumber as $key) {
			$temp .= $key.", ";
		}

		$finlim_sumber = rtrim($temp, ', ');

		# array for pencemaran_air
		$pencemaran_air = array(
			'id_bap' => $id_bap,
			'ambil_air_tanah' => $this->input->post('air_tanah'),
			'ambil_air_tanah_izin' => $this->boolConverter($this->input->post('izin_air_tanah')),
			'ambil_air_permukaan' => $this->input->post('air_permukaan'),
			'ambil_air_permukaan_izin' => $this->boolConverter($this->input->post('izin_air_permukaan')),
			'ambil_air_pdam' => $this->input->post('air_pdam'),
			'ambil_air_lain' => $this->input->post('air_lain'),
			'limb_sumber' => $finlim_sumber,
			'bdn_terima' => $this->input->post('bdn_terima'),
			'sarana_olah_limbah' => $this->boolConverter($this->input->post('sarolim')),
			'sarana_jenis' => $this->input->post('sarana_jenis'),
			'sarana_kapasitas' => $this->input->post('kapasitas'),
			'sarana_koord_derajat_s' => $this->input->post('xkoord_outlet_derajat_s'),
			'sarana_koord_jam_s' => $this->input->post('xkoord_outlet_jam_s'),
			'sarana_koord_menit_s' => $this->input->post('xkoord_outlet_menit_s'),
			'sarana_koord_derajat_e' => $this->input->post('xkoord_outlet_derajat_e'),
			'sarana_koord_jam_e' => $this->input->post('xkoord_outlet_jam_e'),
			'sarana_koord_menit_e' => $this->input->post('xkoord_outlet_menit_e')
			);

		#insert pencemaran_air
		$this->bapDao->edit('pencemaran_air', 'id_penc_air', $id_penc_air, $pencemaran_air);

		# array for pencemaran_udara
				$pencemaran_udara = array(
					'id_bap' => $id_bap,
					'pelaporan_ua_ue' => $this->input->post('pelaporan_ua_ue'),
					'lain_lain' => $this->input->post('lain_lain_pu')
					);

				# edit pencemaran_udara
				$this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $id_penc_udara, $pencemaran_udara);
				
				# collecting data for uji_ambien
		$cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
		$uji_ambien_list = array();

		for($i=1; $i<=3; $i++){
			$id_uji_ambien[$i] = $this->input->post('id_uji_ambien'.$i);
		}
		
		$i = 1;
		foreach ($cols as $i => $val) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $val,
				'uji_kualitas' => $this->boolConverter($this->input->post('uji_kualitas'.$i)),
				'period' => $this->input->post('period'.$i),
				'lab' => $this->input->post('lab'.$i),
				'bm_pemenuhan' => $this->boolConverter($this->input->post('bm_pemenuhan'.$i)),
				'bm_param' => $this->input->post('bm_param'.$i),
				'bm_period' => $this->input->post('bm_period'.$i)
				);
			
			$uji_ambien_list[] = $uji_ambien;

			#insert uji_ambien
			$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien[$i], $uji_ambien);
			$i++;
		}

		if ($this->input->post('padat_kelola') == "Lainnya") {
			$padat_kelola = $this->input->post('padat_kelola_lain');
		} else {
			$padat_kelola = $this->input->post('padat_kelola');
		}

		$padat_jenis_tmp = $this->input->post('padat_jenis');
		$empty = '';
		foreach ($padat_jenis_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jenis = rtrim($empty, ', ');

		$padat_jumlah_tmp = $this->input->post('padat_jumlah');
		$empty = '';
		foreach ($padat_jumlah_tmp as $key) {
			$empty .= $key.', ';
		}
		$padat_jumlah = rtrim($empty, ', ');

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'padat_jenis' => $padat_jenis,
			'padat_jumlah' => $padat_jumlah,
			'padat_sarana_tong' => $this->input->post('padat_sarana_tong'),
			'padat_sarana_tps' => $this->input->post('padat_sarana_tps'), 
			'padat_kelola' => $padat_kelola,
			'lain_lain' => $this->input->post('lain_lain_padat')
			);

		#insert pencemaran_pb3
		$this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $id_penc_pb3, $pencemaran_pb3);

		# Compare BAP
		$data['bap'] = $bap;
		$data['pencemaran_air'] = $pencemaran_air;
		$data['uji_ambien'] = $uji_ambien_list;
		$data['pencemaran_pb3'] = $pencemaran_pb3;
		$data['datenow'] = $datenow;
		$data['user'] = $user;

		$total_error = $this->compareBap($data, 'update');

		if ($total_error > 0) {
			$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP: '.$total_error.' item'));
		} else {
			$status_pelanggaran = ', Tidak ditemukan pelanggaran';
		}

		# notification message
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diunggah'.(($status_pelanggaran!=null) ? $status_pelanggaran : '').''));


		// $status_pelanggaran = $this->compare_item_ketaatan($id_bap, "update");

		// if ($status_pelanggaran!=null){
		// 	$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diunggah, '.$status_pelanggaran.''));
		// } else {
		// 	$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diunggah'));
		// }

		redirect('backend/bap_rm');

	}

	public function tipe_edit($type) {
        $last_insert = false;
		switch ($type) {
			case 1:
			case 2:
				$id_bap = $this->input->post('id_bap');
				break;
			case 3:
				if($this->input->post('id_last_input') != null) {

					$id_last_input = $this->input->post('id_last_input');
					$jenis_bap = 'bap_rm';
					$id_bap = $this->indDao->get_last_input_id_bap($id_last_input, $jenis_bap);
                    $last_insert = true;
					if($id_bap == null){

						$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Data BAP Rumah Makan pada Nama Usaha/Kegiatan yang anda pilih tidak ditemukan! silahkan masukkan data baru, atau pilih Nama Usaha/Kegiatan yang lain'));
					
						redirect('backend/bap_rm');
					}
				} else {

					$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Silahkan pilih Nama Usaha/Kegiatan terlebih dahulu!'));
					
					redirect('backend/bap_rm');
				}
				break;	
			// There is no default, you choose, or you die
		}

        # get all lab
        $lab = $this->bapDao->select_table('laboratorium');
        $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
        $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
        $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");
        $data['lab'] = $lab;
        $data['day_id'] = $this->day_id;
        $data['dok_lingk_jenis'] = $this->dok_lingk_jenis;
        $data['arr_padat_kelola'] = $this->padat_kelola;
        $data['bool'] = $this->bool;

        $check = $this->bapDao->collectStatus($id_bap, "bap_rm");

        if ($check['status']) {

            $bap = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);
            $petugas_bap = $this->bapDao->get_petugas_bap($id_bap);
            $industri = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);
            $pegawai = $this->empDao->get_all_employee();
            $bap_rm = $this->bapDao->get_obj_by_id("bap_rm", "id_bap", $id_bap);

            $data['bap'] = $bap;
            $data['petugas_bap'] = $petugas_bap;
            $data['industri'] = $industri;
            $data['pegawai'] = $pegawai;
            $data['bap_rm'] = $bap_rm;

            $pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

            $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
            $uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

            $pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
            $history_item_teguran_bap = $this->bapDao->get_list_by_id("history_item_teguran_bap", "id_bap", $id_bap);

            $data['pencemaran_air'] = $pencemaran_air;
            $data['pencemaran_udara'] = $pencemaran_udara;
            $data['uji_ambien'] = $uji_ambien;
            $data['pencemaran_pb3'] = $pencemaran_pb3;

            # parameter for view
            $data['tab_list'] = '';
            $data['tab_view'] = '';
            if ($type == 1) {
                $data['tab_edit'] = '';
                $data['tab_edit_new'] = 'active';
                $data['tab_last_input'] = '';

                # load view
                $this->load->view('include/header');
                $this->load->view('include/footer');
                $this->load->view('backend/v_bap_rm_edit', $data);

            } else if ($type == 2) {
                $data['tab_edit'] = 'active';
                $data['tab_edit_new'] = '';
                $data['tab_last_input'] = '';
                # load view
                $this->load->view('include/header');
                $this->load->view('include/footer');
                $this->load->view('backend/v_bap_rm_edit', $data);

            } else if ($type == 3) {
                $this->load->view('include/header');
                $this->load->view('include/footer');
                $this->load->view('backend/v_bap_rm_last_insert', $data);
            }
        } else {
            $this->partialEdit($id_bap, $check, $data, $last_insert);
        }
	}
	
	public function delete() {
		$id_bap = $this->input->post("id");

		$this->bapDao->delete('bap_rm', 'id_bap', $id_bap);

		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('surat_teguran_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);

		$this->bapDao->delete_penc_air($id_bap);
		$this->bapDao->delete_penc_udara($id_bap);
		$this->bapDao->delete_penc_pb3($id_bap);

		$this->bapDao->delete('bap', 'id_bap', $id_bap);
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/bap_rm');
	}

	public function ajax_get_id() {

		$data['list'] = $this->indDao->get_ajax_industri();
		$data['detail_tps'] = $this->indDao->get_detail_tps();
		$data['detail_air_limbah'] = $this->indDao->get_detail_air_limbah();
		$id_industri = $this->input->post('id_industri');
		$query = $this->db->query("SELECT 
			industri.nama_industri
			FROM izin_tps_limbah
			INNER JOIN tps_limbah ON izin_tps_limbah.id_izin_tps = tps_limbah.id_izin_tps
			INNER JOIN industri ON industri.id_industri = izin_tps_limbah.id_industri
			WHERE izin_tps_limbah.id_industri ='$id_industri'");
		$data['jml_tps'] = $query->num_rows();
		echo json_encode($data);	
		// $this->load->view('backend/v_alamat_industri', $data);
		exit();
	}

	public function get_pelanggaran() {
		header('Content-type: application/json');

		$id_bap = $this->input->post('id_bap');

		$data['industri'] = $this->input->post('industri');
		$data['tgl'] = $this->input->post('tgl');
		$data['teguran'] = $this->bapDao->get_list_by_id("history_item_teguran_bap", "id_bap", $id_bap);

		echo json_encode($data);
		exit();
	}


	# function to convert excel date format to raedable php format
	# author : guntutur@gmail.com
	public function dateConv($col){

		$date = PHPExcel_Shared_Date::ExcelToPHP($col); // 1007596800 (Unix time)
		$date1 = date('Y-m-d', $date);

		return $date1;
	}

	# function to convert string to boolean
	# add "sudah" ass value occured in BAP Industri
	private function boolConverter($value) {
		if (strcasecmp($value, "ada") == 0 || strcasecmp($value, "ya") == 0 || strcasecmp($value, "sudah") == 0) {
			return true;
		} else {
			return false;
		}
	}

	# function to convert boolean to string (Ada / Tidak Ada)
	private function adaConv($value) {
        if ($value == NULL) {
            return "";
        } else if ($value == 1) {
            return "Ada";
        } else {
            return "Tidak Ada";
        }
    }

    private function adaConv_tps($value) {
        if ($value == true) {
            return "Ada";
        } else {
            return "Tidak Ada";
        }
    }

    # function to convert boolean to string (Ya / Tidak)
    private function yaConv($value) {
        if ($value == true) {
            return "Ya";
        } else {
            return "Tidak";
        }
    }

    public function compare_item_ketaatan($id_bap, $action) {

    	if($action == 'update') {
    		$this->bapInDao->delete_item_ketaatan($id_bap);
    	}

    	$where = ("a.jenis_bap = 'bap_rm' AND a.status = '1' AND a.id_parent != 'top' AND a.nilai_parameter_bap != '-'");
		$standard_detail = $this->bapDao->get_parameter_bap($where);

		if ($standard_detail > 0) {
		
			$standard = array();
			$i = 0;

			foreach ($standard_detail as $key => $value) {
				$standard[$i] = array(
						'id_parameter_bap' => $value->id_parameter_bap,
						'nama_parameter' => $value->nama_parameter,
						'nilai_parameter_bap' => $value->nilai_parameter_bap,
						'dasar_hukum' => $value->dasar_hukum,
						'id_parent_1' => $value->id_parent_1,
						'id_parent_2' => $value->id_parent_2
					);
				echo "Item ketaatan ke ".($i+1)." : <br>";
				print_r($standard[$i]);
				echo "<br>";
				$i++;
			} 
			print_r($standard);
			die();

			for ($f=1;$f<4;$f++){
				$uji_kualitas[$f] = $this->input->post('uji_kualitas'.$f);
				$period[$f] = $this->input->post('period'.$f);
				$bm_pemenuhan[$f] = $this->input->post('bm_pemenuhan'.$f);
			}

			# collect data to compare
			$compare = array(
				'dok_lingk' => $this->input->post('dl'),
				'izin_lingk' => $this->input->post('il'),
				'sarana_olah_limbah' => strtolower($this->input->post('sarolim')),
				'uji_kualitas' => $uji_kualitas,
				'period' => $period,
				'bm_pemenuhan' => $bm_pemenuhan,
				'padat_kelola' => $this->input->post('padat_kelola'),
				'lain-lain' => $this->input->post('lain_lain_bap')
			);

			$pelangaran = array();
			$i = 0;
			$s = 0;
			$no = 1;

			$value = array();
			foreach ($compare as $key => $value) {
	            $nama_parameter = $standard[$i]['nama_parameter'];
				$nilai_standard = $standard[$i]['nilai_parameter_bap'];
	            $dasar_hukum = $standard[$i]['dasar_hukum'];
	            $id_parent_1 = $standard[$i]['id_parent_1'];
	            $id_parent_2 = $standard[$i]['id_parent_2'];
	            
	            // echo "Nama Item Bakumutu => ".$nama_parameter." Nilai Standard => ".$nilai_standard."  => nilai pada BAP => ".$value."<br>";

				if(($key == 'uji_kualitas') || ($key == 'period') || ($key == 'bm_pemenuhan')) {
					$e=1;
					foreach ($value as $k ){
						if (strcasecmp($nilai_standard, $k) != 0){
							$pesan_error = "Tidak Ada!";
							$value = $k;
							$pelanggaran[$s] = array($value, $nilai_standard, $nama_parameter." ".$e, $dasar_hukum, $pesan_error, $id_parent_1, $id_parent_2);
						}
						$s++;
					$e++;
					}
				} else if($key == 'padat_kelola') {
					if (strcasecmp($value, "dibakar") == 0) {
						$pesan_error = "Nilai valid adalah selain dibakar!";
						$pelanggaran[$s] = array($value, $nilai_standard, $nama_parameter, $dasar_hukum, $pesan_error, $id_parent_1, $id_parent_2);
					}
					$s++;
				} else {
					if ((strcasecmp($nilai_standard, $value) != 0)){
						$pesan_error = "Tidak Ada!";
						$pelanggaran[$s] = array($value, $nilai_standard, $nama_parameter, $dasar_hukum, $pesan_error, $id_parent_1, $id_parent_2);
					}
					$s++;
				}
				$i++;
				$no++;
			}
			$total_error = count($pelanggaran);
	    	if ($total_error > 0) {

				// $this->bapInDao->delete_item_ketaatan($id_bap);

				foreach ($pelanggaran as $key => $value) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $value[1],
						'nilai_parameter' => $value[0],
						'ket_parameter_bap' => $value[2],
						'id_parent_1' => $value[5],
						'id_parent_2' => $value[6],
						'tgl_pembuatan' => date('Y-m-d'),
						'tgl_update' => date('Y-m-d'),
						'dibuat_oleh' => $this->tank_auth->get_personname(),
						'diupdate_oleh' => $this->tank_auth->get_personname()
					);
					$this->bapDao->insert_getStat('history_item_teguran_bap', $item);
				}
				$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP : '.$total_error.' item'));
			} else {
				// $this->bapInDao->delete_item_ketaatan($id_bap);
				$status_pelanggaran = "Tidak terdapat pelanggaran";
			}

			return $status_pelanggaran;
    	} else {

			$this->session->set_flashdata('error', $this->functions->build_message('danger', 'Sistem tidak dapat melakukan perbandingan Item Ketaatan BAP! Pastikan data Item Ketaatan BAP Rumah Makan pada Manajemen Data -> Item Ketaatan BAP telah terisi sesuai dengan ketentuan!'));
			return null;
		}
    }

    // dari a huda
    public function compareBap($data, $action) {
		$item_teguran = array();
		$parameter_bap = $this->lapDao->get_ketaatan_bap('bap_rm');

		foreach ($parameter_bap as $p) {
			$param[$p->ket] = $p->nilai_parameter_bap;
			$stat[$p->ket] = $p->status;
			$parent[$p->ket] = new StdClass;
			$parent[$p->ket]->one = $p->id_parent_1;
			$parent[$p->ket]->two = $p->id_parent_2;
		}

		$bap = $data['bap'];
		$pencemaran_air = $data['pencemaran_air'];
		$uji_ambien = $data['uji_ambien'];
		$pencemaran_pb3 = $data['pencemaran_pb3'];
		$datenow = $data['datenow'];
		$user = $data['user'];

		$id_bap = $bap['id_bap'];

		if ($stat['Dokumen Lingkungan']) {
			if ($this->valChecker($this->adaConv($bap['dok_lingk']),$param['Dokumen Lingkungan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Dokumen Lingkungan'],
					'nilai_parameter' => $this->adaConv_tps($bap['dok_lingk']),
					'ket_parameter_bap' => 'Dokumen Lingkungan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Dokumen Lingkungan']->one,
					'id_parent_2' => $parent['Dokumen Lingkungan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Izin Lingkungan']) {
			if ($this->valChecker($this->adaConv($bap['izin_lingk']),$param['Izin Lingkungan'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Izin Lingkungan'],
					'nilai_parameter' => $this->adaConv_tps($bap['izin_lingk']),
					'ket_parameter_bap' => 'Izin Lingkungan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Izin Lingkungan']->one,
					'id_parent_2' => $parent['Izin Lingkungan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Sarana Pengolahan Air Limbah']) {
			if ($this->valChecker($this->adaConv($pencemaran_air['sarana_olah_limbah']),$param['Sarana Pengolahan Air Limbah'])) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Sarana Pengolahan Air Limbah'],
					'nilai_parameter' => $this->adaConv_tps($pencemaran_air['sarana_olah_limbah']),
					'ket_parameter_bap' => 'Sarana Pengolahan Air Limbah',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Sarana Pengolahan Air Limbah']->one,
					'id_parent_2' => $parent['Sarana Pengolahan Air Limbah']->two
					);
				$item_teguran[] = $item;
			}
		}

		foreach ($uji_ambien as $ua) {
			if ($stat['Pengujian Kualitas']) {
				if ($this->valChecker($this->adaConv($ua['uji_kualitas']),$param['Pengujian Kualitas'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pengujian Kualitas'],
						'nilai_parameter' => $this->adaConv_tps($ua['uji_kualitas']),
						'ket_parameter_bap' => 'Pengujian Kualitas '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pengujian Kualitas']->one,
						'id_parent_2' => $parent['Pengujian Kualitas']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Periode pengujian (per 6 bulan)']) {
				if ($this->valChecker($ua['period'],$param['Periode pengujian (per 6 bulan)'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Periode pengujian (per 6 bulan)'],
						'nilai_parameter' => $ua['period'],
						'ket_parameter_bap' => 'Periode pengujian (per 6 bulan) '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Periode pengujian (per 6 bulan)']->one,
						'id_parent_2' => $parent['Periode pengujian (per 6 bulan)']->two
						);
					$item_teguran[] = $item;
				}
			}

			if ($stat['Pemenuhan BM']) {
				if ($this->valChecker($this->yaConv($ua['bm_pemenuhan']),$param['Pemenuhan BM'])) {
					$item = array(
						'id_bap' => $id_bap,
						'nilai_parameter_bap' => $param['Pemenuhan BM'],
						'nilai_parameter' => $this->yaConv($ua['bm_pemenuhan']),
						'ket_parameter_bap' => 'Pemenuhan BM '.$ua['lokasi'],
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user,
						'id_parent_1' => $parent['Pemenuhan BM']->one,
						'id_parent_2' => $parent['Pemenuhan BM']->two
						);
					$item_teguran[] = $item;
				}
			}
		}

		if ($stat['Pengelolaan']) {
			if (strcasecmp($pencemaran_pb3['padat_kelola'],$param['Pengelolaan'])==0) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => $param['Pengelolaan'],
					'nilai_parameter' => $pencemaran_pb3['padat_kelola'],
					'ket_parameter_bap' => 'Pengelolaan',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Pengelolaan']->one,
					'id_parent_2' => $parent['Pengelolaan']->two
					);
				$item_teguran[] = $item;
			}
		}

		if ($stat['Lain - Lain']) {
			if (!empty($bap['catatan']) ) {
				$item = array(
					'id_bap' => $id_bap,
					'nilai_parameter_bap' => ' - ',
					'nilai_parameter' => $bap['catatan'],
					'ket_parameter_bap' => 'Lain - Lain',
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user,
					'id_parent_1' => $parent['Lain - Lain']->one,
					'id_parent_2' => $parent['Lain - Lain']->two
					);
				$item_teguran[] = $item;
			}
		}
		
		if ($action=='update') {
			# history_item_teguran_bap update procedur
			$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);
		}

		foreach ($item_teguran as $item) {
			#insert history_item_teguran_bap
			$this->bapDao->insert_getStat('history_item_teguran_bap', $item);
		}

        /*update status is_compared to 1*/
        $this->bapDao->updateIsCompared($id_bap);

		return count($item_teguran);
	}

	public function valChecker($str1, $str2) {
		if (strcasecmp($str1, $str2) == 0) {
			return false;
		} else {
			return true;
		}
	}

    public function exit_check_status($id_bap) {
        $this->bapDao->collectStatus($id_bap, "bap_rm");
        redirect("backend/bap_rm");
    }

    public function save_part() {
        $stat['status'] = false;

        $posted = $this->input->post('data');
        $ids = $this->input->post('ids');
        parse_str($posted, $data);

        if(!empty($ids)) {
            if ($this->input->post('dest') == "datum") {
                $stat['id'] = $this->upsert_bap($data, $ids);
                $stat['status'] = true;
            } elseif ($this->input->post('dest') == "pencemaran_air") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_air($data, $ids);
                    $stat['status'] = true;
                }
            } elseif ($this->input->post('dest') == "pencemaran_udara") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_udara($data, $ids);
                    $stat['status'] = true;
                }
            } elseif ($this->input->post('dest') == "pencemaran_b3") {
                if ($ids['id_bap']) {
                    $stat['id'] = $this->upsert_pencemaran_b3($data, $ids);
                    $stat['status'] = true;
                }
            }
        }

        echo json_encode($stat);
    }

    public function upsert_bap($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # data bap
        $bap = array(
            'id_industri' => $data['id_industri'],
            'id_pegawai' => $data['id_pegawai'],
            'bap_tgl' => $data['bap_tgl'],
            'bap_jam' => $data['bap_jam'],
            'p2_nama' => $data['p2_nama'],
            'p2_jabatan' => $data['p2_jabatan'],
            'p2_telp' => $data['p2_telp'],
            'dok_lingk' => $this->boolConverter($data['dl']),
            'dok_lingk_jenis' => $data['jdl_name'],
            'dok_lingk_tahun' => $data['dlt'],
            'izin_lingk' => $this->boolConverter($data['il']),
            'izin_lingk_tahun' => $data['il_tahun'],
            'tgl_pembuatan' => $datenow,
            'dibuat_oleh' => $user,
            'catatan' => $data['lain_lain_bap']
        );

        # data bap_rm
        $bap_rm = array(
            'id_bap' => $ids['id_bap'],
            'jml_meja' => @$data['jml_meja'],
            'izin_usaha' => $this->boolConverter(@$data['iu']),
            'izin_usaha_tahun' => @$data['iu_tahun'],
            'jumlah_karyawan' => @$data['jml_karyawan']
        );

        # data petugas bap
        $id_petugas_bap = isset($data['petugas_pengawas']) ? $data['petugas_pengawas'] : null;

        if($ids['id_bap']=="") {
            $id_bap = $this->bapDao->insert_getID('bap',$bap);

            $bap_rm['id_bap'] = $id_bap;
            $id_bap_rm = $this->bapDao->insert_getID('bap_rm', $bap_rm);

            if ($id_petugas_bap[0] != null) {
                $datenow = date('Y-m-d');

                foreach ($id_petugas_bap as $idpbap) {
                    $petugas_bap = array(
                        'id_bap' => $id_bap,
                        'id_pegawai' => $idpbap,
                        'tgl_pembuatan' => $datenow,
                        'dibuat_oleh' => $user
                    );

                    #insert petugas_bap
                    $this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
                }
            }

            $ret_ids['id_bap'] = $id_bap;
            $ret_ids['id_bap_rm'] = $id_bap_rm;
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;
            $this->bapDao->edit('bap', 'id_bap', $ids['id_bap'], $bap);
            $this->bapDao->edit('bap_rm', 'id_bap_rm', $ids['id_bap_rm'], $bap_rm);

            $this->bapDao->delete('petugas_bap', 'id_bap', $ids['id_bap']);

            if ($id_petugas_bap[0] != null) {
                $datenow = date('Y-m-d');

                foreach ($id_petugas_bap as $idpbap) {
                    $petugas_bap = array(
                        'id_bap' => $ids['id_bap'],
                        'id_pegawai' => $idpbap,
                        'tgl_pembuatan' => $datenow,
                        'dibuat_oleh' => $user
                    );

                    #insert petugas_bap
                    $this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
                }
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_air($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # sumber limbah
        $limb_sumber = isset($data['limb_sumber']) ? $data['limb_sumber'] : "";
        if (!empty($limb_sumber )) {
            $temp = '';
            foreach ($limb_sumber as $key) {
                $temp .= $key . ", ";
            }

            $finlim_sumber = rtrim($temp, ', ');
        } else { $finlim_sumber = ""; }

        # array for pencemaran_air
        $pencemaran_air = array(
            'id_bap' => $ids['id_bap'],
            'ambil_air_tanah' => @$data['air_tanah'],
            'ambil_air_tanah_izin' => $this->boolConverter(@$data['izin_air_tanah']),
            'ambil_air_permukaan' => @$data['air_permukaan'],
            'ambil_air_permukaan_izin' => $this->boolConverter($this->input->post('izin_air_permukaan')),
            'ambil_air_pdam' => @$data['air_pdam'],
            'ambil_air_lain' => @$data['air_lain'],
            'limb_sumber' => $finlim_sumber,
            'bdn_terima' => @$data['bdn_terima'],
            'sarana_olah_limbah' => $this->boolConverter(@$data['sarolim']),
            'sarana_jenis' => @$data['sarana_jenis'],
            'sarana_kapasitas' => @$data['kapasitas'],
            'sarana_koord_derajat_s' => @$data['xkoord_outlet_derajat_s'],
            'sarana_koord_jam_s' => @$data['xkoord_outlet_jam_s'],
            'sarana_koord_menit_s' => @$data['xkoord_outlet_menit_s'],
            'sarana_koord_derajat_e' => @$data['xkoord_outlet_derajat_e'],
            'sarana_koord_jam_e' => @$data['xkoord_outlet_jam_e'],
            'sarana_koord_menit_e' => @$data['xkoord_outlet_menit_e']
        );

        if($ids['id_pencemaran_air']=="") {
            $id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = $id_penc_air;
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            $this->bapDao->edit('pencemaran_air', 'id_penc_air', $ids['id_pencemaran_air'], $pencemaran_air);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = $ids['id_pencemaran_air'];
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_udara($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        # array for pencemaran_udara
        $pencemaran_udara = array(
            'id_bap' => $ids['id_bap'],
            'pelaporan_ua_ue' => @$data['pelaporan_ua_ue'],
            'lain_lain' => @$data['lain_lain_pu']
        );

        if($ids['id_pencemaran_udara']=="") {
            # insert pencemaran_udara
            $id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);

            # collecting data for uji_ambien
            $cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
            $uji_ambien_list = array();

            foreach ($cols as $i => $val) {
                $uji_ambien = array(
                    'id_penc_udara' => $id_penc_udara,
                    'lokasi' => $val,
                    'uji_kualitas' => $this->boolConverter(@$data['uji_kualitas'.$i]),
                    'period' => @$data['period'.$i],
                    'lab' => @$data['lab'.$i],
                    'bm_pemenuhan' => $this->boolConverter(@$data['bm_pemenuhan'.$i]),
                    'bm_param' => @$data['bm_param'.$i],
                    'bm_period' => @$data['bm_period'.$i]
                );

                $uji_ambien_list[] = $uji_ambien;

                #insert uji_ambien
                $this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = $id_penc_udara;
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            # edit pencemaran_udara
            $this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $ids['id_pencemaran_udara'], $pencemaran_udara);

            # collecting data for uji_ambien
            $cols = array('1'=>'Upwind', '2'=>'Site', '3'=>'Downwind');
            $uji_ambien_list = array();

            for($i=1; $i<=3; $i++){
                $id_uji_ambien[$i] = @$data['id_uji_ambien'.$i];
            }

            $this->bapDao->delete('uji_ambien', 'id_penc_udara', $ids['id_pencemaran_udara']);

            $i = 1;
            foreach ($cols as $i => $val) {
                $uji_ambien = array(
                    'id_penc_udara' => $ids['id_penc_udara'],
                    'lokasi' => $val,
                    'uji_kualitas' => $this->boolConverter(@$data['uji_kualitas'.$i]),
                    'period' => @$data['period'.$i],
                    'lab' => @$data['lab'.$i],
                    'bm_pemenuhan' => $this->boolConverter(@$data['bm_pemenuhan'.$i]),
                    'bm_param' => @$data['bm_param'.$i],
                    'bm_period' => @$data['bm_period'.$i]
                );

                $uji_ambien_list[] = $uji_ambien;

                #insert uji_ambien
                //$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien[$i], $uji_ambien);
                $this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
                $i++;
            }

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = $ids['id_pencemaran_udara'];
            $ret_ids['id_plb3'] = isset($ids['id_plb3']) ? $ids['id_plb3'] : "";
        }

        return $ret_ids;
    }

    public function upsert_pencemaran_b3($data = array(), $ids) {
        date_default_timezone_set("Asia/Jakarta");
        $datenow = date("Y-m-d H:i:s:");
        $user = $this->tank_auth->get_personname();

        $padat_jenis_tmp = @$data['padat_jenis'];
        $empty = '';
        foreach ($padat_jenis_tmp as $key) {
            $empty .= $key.', ';
        }
        $padat_jenis = rtrim($empty, ', ');

        $padat_jumlah_tmp = @$data['padat_jumlah'];
        $empty = '';
        foreach ($padat_jumlah_tmp as $key) {
            $empty .= $key.', ';
        }
        $padat_jumlah = rtrim($empty, ', ');

        $padat_kelola = @$data['padat_kelola'];

        if ($padat_kelola == 'other') {
            $padat_kelola = @$data['padat_kelola_lain'];
        } else {
            $padat_kelola = @$data['padat_kelola'];
        }

        # array for pencemaran_pb3
        $pencemaran_pb3 = array(
            'id_bap' => $ids['id_bap'],
            'padat_jenis' => $padat_jenis,
            'padat_jumlah' => $padat_jumlah,
            'padat_sarana_tong' => @$data['padat_sarana_tong'],
            'padat_sarana_tps' => @$data['padat_sarana_tps'],
            'padat_kelola' => $padat_kelola,
            'lain_lain' => @$data['lain_lain_padat']
        );

        if ($ids['id_plb3']=="") {

            # insert pencemaran_pb3
            $id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = $id_penc_pb3;
        } else {
            $bap['tgl_update'] = $datenow;
            $bap['diupdate_oleh'] = $user;

            #insert pencemaran_pb3
            $this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $ids['id_plb3'], $pencemaran_pb3);

            $ret_ids['id_bap'] = $ids['id_bap'];
            $ret_ids['id_bap_rm'] = $ids['id_bap_rm'];
            $ret_ids['id_pencemaran_air'] = isset($ids['id_pencemaran_air']) ? $ids['id_pencemaran_air'] : "";
            $ret_ids['id_pencemaran_udara'] = isset($ids['id_pencemaran_udara']) ? $ids['id_pencemaran_udara'] : "";
            $ret_ids['id_plb3'] = $ids['id_plb3'];
        }

        return $ret_ids;
    }

    public function getComparableStatus() {
        $data = $this->bapDao->collectStatus($this->input->post("id_bap"), "bap_rm");
        echo json_encode($data);
    }

    public function newCompare() {
        date_default_timezone_set("Asia/Jakarta");

        /*data umum & bap */
        $data['bap'] = $this->bapDao->select_table_array("bap", "id_bap", $this->input->post("id"), FALSE);

        /*pencemaran air*/
        $data['pencemaran_air'] = $this->bapDao->select_table_array("pencemaran_air", "id_bap", $this->input->post("id"), FALSE);

        /*pencemaran_udara*/
        $pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $this->input->post("id"));
        $data['uji_ambien'] = $this->bapDao->select_table_array("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara, TRUE);

        /*pencemaran pb3*/
        $data['pencemaran_pb3'] = $this->bapDao->select_table_array("pencemaran_pb3", "id_bap", $this->input->post("id"), FALSE);

        /*additional data*/
        $data['datenow'] = date("Y-m-d H:i:s:");
        $data['user'] = $this->tank_auth->get_personname();
        //echo "<pre>"; print_r($data); die();

        $total_error = $this->compareBap($data, 'update');

        if ($total_error > 0) {
            $this->session->set_flashdata('error', $this->functions->build_message('danger', 'Total pelanggaran pada BAP: '.$total_error.' item'));
        } else {
            $this->session->set_flashdata('msg', $this->functions->build_message('success', 'Tidak ditemukan pelanggaran'));
        }

        /* ----------------------------------------------- */
        /* END COMPARE BAP */
        /* ----------------------------------------------- */

        redirect('backend/bap_rm');
    }

    public function partialEdit($id_bap, $status, $last_insert) {
        // ---------------------------------- DATA ACUAN TJOY ---------------------------------- //

        /*param for nullify all the ids in view*/
        if ($last_insert) {
            $data['last_insert'] = $last_insert;
        }

        $data['day_id'] = $this->day_id;
        $data['arr_padat_kelola'] = $this->padat_kelola;
        $data['bool'] = $this->bool;
        $data['dok_lingk_jenis'] = $this->dok_lingk_jenis;
        $data['lab'] = $this->bapDao->select_table('laboratorium');
        $data['limb_sumber'] = $this->m_data_master->get_dropdown("sumber_air_limbah");
        $data['bdn_terima'] = $this->m_data_master->get_dropdown("badan_air_penerima");
        $data['pengelolaan'] = $this->m_data_master->get_dropdown("pengelolaan");

        // ---------------------------------- DATA ACUAN TJOY ---------------------------------- //

        if(!empty($status['bap']) AND !empty($status['bap_rm'])) { // data umum and bap

            $bap_rm = $this->bapDao->get_obj_by_id("bap_rm", "id_bap", $id_bap);
            $data['bap'] = $this->bapDao->get_obj_by_id_edit("bap", "id_bap", $id_bap);
            $data['bap_rm'] = $bap_rm;
            $data['industri'] = $this->indDao->get_dropdown_bap($this->id_usaha_kegiatan);
            $data['pegawai'] = $this->empDao->get_all_employee();
            $data['petugas_bap'] = $this->bapDao->get_petugas_bap($id_bap);
        }

        if(!empty($status['pencemaran_air'])) { // pencemaran air
            $data['pencemaran_air'] = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);
        }

        if(!empty($status['pencemaran_udara'])) { // pencemaran udara
            $data['pencemaran_udara'] = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);
            $data['uji_ambien'] = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $data['pencemaran_udara']->id_penc_udara);
        }

        if(!empty($status['pencemaran_pb3'])) { // pencemaran b3
            $data['pencemaran_pb3'] = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);
        }

        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/bap_rm_partial/v_bap_rm_partial', $data);
    }

}
