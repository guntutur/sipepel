<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document   : perbaikan_item_ketaatan_bap.php 
 // Created on : Januari 5, 2015
 // Author     : guntutur@gmail.com 
 // Description: Controller for menu perbaikan item ketaatan bap

class perbaikan_item_ketaatan_bap extends CI_Controller {

	private $bool = array('0', '1');

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);
        $this->load->model('m_employee', 'empDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');
		
		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_perbaikan_bap');
        // $this->load->view('backend/v_perbaikan_bap', $data);
    }

    public function get_list_bap() {
    	# get parameter pagination
        $input = array('dataperpage', 'query', 'curpage');
        foreach ($input as $val)
            $$val = $this->input->post($val); 

        $where = "c.nama_industri LIKE '%".$query."%'";

        $total = $this->bapDao->get_all_bap_num_row($where);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $bap_data = $this->bapDao->get_all_bap($where, $dataperpage, $start);

        $data = array(
            'bap_data' => $bap_data,
            'pagination' => $this->functions->create_links($npage, $curpage, 3),
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['bap_data'] = $bap_data;

        echo json_encode($data);
        exit();
    }

    public function perbaikan() {

    	if(!empty($_POST['check'])) {
    		$check = $this->input->post('check');
            $id_bap = $this->input->post('id_bap');
    		$this->bapDao->perbaiki_item_teguran_bap($id_bap, $check);
    		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Item Teguran BAP berhasil diperbaiki'));
    	}

    	redirect($_SERVER['HTTP_REFERER']);
    }

	public function get_pelanggaran() {
		header('Content-type: application/json');

		$id_bap = $this->input->post('id_bap');

		$data['industri'] = $this->input->post('industri');
		$data['tgl'] = $this->input->post('tgl');
		$data['teguran'] = $this->bapDao->get_list_by_id("history_item_teguran_bap", "id_bap", $id_bap);

		echo json_encode($data);
		exit();
	}

	# function to convert string to boolean
	# add "sudah" as value occured in BAP Industri
	private function boolConverter($value) {
		if (strcasecmp($value, "ada") == 0 || strcasecmp($value, "ya") == 0 || strcasecmp($value, "sudah") == 0) {
			return true;
		} else if (strcasecmp($value, "tidak ada") == 0 || strcasecmp($value, "tidak") == 0 || strcasecmp($value, "belum") == 0) {
			return false;
		} else if (strcasecmp($value, "") == 0) {
			return NULL;
		} else {}
	}

}
