<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading"><?php echo $title; ?>&nbsp;&nbsp;<small>mengelola seluruh parameter baku mutu yang digunakan oleh LHU</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li>Baku Mutu LHU</li>
				<li class="active"><?php echo $sub_title; ?></li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
						<button type="button" onclick="history.back();" class="btn btn-sm btn-info"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</button>
                        <button type="button" data-toggle="modal" data-target="#input_detail" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>
                
                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%" class="text-area">#</th>						
								<th>Parameter</th>
                                <th>Baku Mutu</th>
								<th>Satuan</th>								
                                <th width="5%">Status</th>                               
                                <th class="text-right" width="5%"></th>									
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="6">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
	
	var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0,
            id: '<?php echo $this->uri->segment(4); ?>'
        },
        url: '<?php echo base_url("backend/baku_mutu_lhu/get_detail_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);                    
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];                        
                        var hukum_internal = (dt.dasar_hukum_internal != '') ? 'Ada': 'Tidak Ada';
                        var hukum_eksternal = (dt.dasar_hukum_eksternal != '') ? 'Ada': 'Tidak Ada';
						var jenis_industri = (dt.nama_jenis_kegiatan != null ) ? dt.nama_jenis_kegiatan : ' ';
						var status = (dt.status == 1) ? 'Aktif': 'Tidak Aktif';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';						
                        t += '<td>'+dt.ket+'</td>';
                        t += '<td><b>Umum</b>: '+$(this).return_value(dt.nilai_parameter_lhu_internal, true)+'<br/><b>Khusus</b>: '+$(this).return_value(dt.nilai_parameter_lhu_eksternal, true)+'</td>';                                    
                        t += '<td>'+$(this).return_value(dt.satuan)+'</td>';
						t += '<td>'+status+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
						t += '<li><a onclick="$(this).edit('+dt.id_parameter_lhu+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_parameter_lhu+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

	$(document).ready(function () {

        $("#btn_input_detail").click(function(event) {
            $('#input_detail form')[0].reset();
            $('#input_detail form input[type=hidden]').val('');

            $("label.error").hide();
            $(".error").removeClass("error");

            $("#input_detail").modal("show");
        });
		
		$("#form_add").validate({
            rules: {
                ket: {
                    required: true,                    
                    maxlength: 250
                },            
				nilai_internal: {
                    required: true					
                },
				nilai_eksternal: {
                    required: false
                },					
				status: {
					required: true
				},
				satuan: {
					required: true,					
                    maxlength: 10
				}
            },
            messages: {        
                ket: {
                    required: "Masukkan nama parameter",
                    maxlength: "Nama parameter tidak boleh lebih dari 250 karakter"
                },
				nilai_internal: {
                    required: "Masukkan nilai internal"                    
                },
				nilai_eksternal: {
                    required: "Masukkan nilai eksternal"                   
                },			
				status: {
                    required: "Silahkan pilih data"
                },				
				satuan: {
                    required: "Masukkan satuan parameter",
                    maxlength: "Satuan parameter tidak boleh lebih dari 10 karakter"
                }				
            }
        });

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    
		
		$.fn.edit = function(id) {
            $.ajax({
                url: "<?php echo base_url('/backend/baku_mutu_lhu/get_detail'); ?>",
                type: 'POST',
                dataType: 'json',
				data: $.param({id_parameter_lhu: id}),
                success: function (response) {

                    // console.log(response);

                    $('#input_detail form')[0].reset();
                    $('#input_detail form input[type=hidden]').val('');

                    $("label.error").hide();
                    $(".error").removeClass("error");
                    
                    $('input[id=detail_id]').val(response.id_parameter_lhu);                    
                    $('input[name=ket]').val(response.ket);
                    $('input[name=nilai_internal]').val(response.nilai_parameter_lhu_internal);
                    $('input[name=nilai_eksternal]').val(response.nilai_parameter_lhu_eksternal);
					$('input[name=satuan]').val(response.satuan);                    
                    $('select[name=status]').val(response.status);
					                    
					$('#input_detail form').attr('action', '<?php echo base_url('/backend/baku_mutu_lhu/edit_detail') ?>'); //this fails silently
                    $('#input_detail form').get(0).setAttribute('action', '<?php echo base_url('/backend/baku_mutu_lhu/edit_detail') ?>'); //this works
					
                    $('#input_detail').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            });          
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        

        Document.search();
	});
</script>

<!-- Modal -->
<div class="modal fade" id="input_detail" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Parameter Baku Mutu LHU</h4>
            </div>
            <form action="<?php echo base_url('/backend/baku_mutu_lhu/insert_detail') ?>" method="post" id="form_add">
                <div class="modal-body">
					<div class="form-group param_internal">
						<label>Parameter</label>
						<input type="text" name="ket" id="ket" class="form-control has-feedback" autofocus />                        
					</div>
                    <div class="row">
                        <div class="col-lg-6 form-group value_parameter">
                            <label>Baku Mutu Umum</label>
                            <input type="text" name="nilai_internal" id="nilai_internal" class="form-control input-sm has-feedback" autofocus />                        
                        </div>
    					<div class="col-lg-6 form-group value_parameter">
                            <label>Baku Mutu Khusus</label>
                            <input type="text" name="nilai_eksternal" id="nilai_eksternal" class="form-control input-sm has-feedback" autofocus />                        
                        </div>
                    </div>                   
					<div class="row">
						<div class="col-lg-6 form-group satuan">
							<label>Satuan</label>
							<input type="text" name="satuan" id="satuan" class="form-control input-sm has-feedback" autofocus />                        
						</div>
						<div class="col-lg-6 form-group status">
							<label>Status</label>
							<select name="status" id="status" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Status --</option>
								<?php 
									$status = $this->functions->get_status();
									foreach($status as $k => $s) {
										echo '<option value="'.$k.'">'.$s.'</option>';
									}
								?>                      
							</select>                        
						</div>  													  						
					</div>
                </div>
				<div class="modal-footer">
                    <input type="hidden" id="detail_id" name="id" value="<?php echo $this->uri->segment(4)?>" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="do_upload" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Parameter Baku Mutu LHU</h4>
            </div>
            <form action="<?php echo base_url('/backend/baku_mutu_lhu/delete_detail'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->