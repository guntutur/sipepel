<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_management extends CI_Model {

	private $table_name			= 'users';			// user accounts
	private $profile_table_name	= 'user_profiles';	// user profiles

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all() {
		$data = array();

		$query = $this->db->query("select * from users u order by u.created desc");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_user_by_id($id) {
		$this->db->where('id', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_users_profile($id) {
		$this->db->where('user_id', $id);
		
		$query = $this->db->get($this->profile_table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_username($username) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('username', $username);
        $query = $this->db->get();

        return $query->row();
    }

    function get_email($email) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->row();
    }
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_user($id) {
		$data = $this->get_user_by_id($id);
		$this->db->where('id', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	bool
	 */
	function change_user($data, $id) {

		$this->db->where("id", $id);
		$update = $this->db->update($this->table_name, $data['users']);

		if (array_key_exists('user_profile', $data)) {
            $data_userprofile = $data['user_profile'];
            $this->db->where('user_id', $id);
            $this->db->update($this->profile_table_name, $data_userprofile);
        }

        if (array_key_exists('group_id', $data)) {
            $group_id = array('group_id' => $data['group_id']);
            $this->db->where('id', $id);
            $this->db->update($this->table_name, $group_id);
        }	
		
		// if ($this->db->affected_rows() > 0) return TRUE;
		if ($update) return TRUE;
		return false;
	}
}
?>