<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Teguran Administrasi&nbsp;&nbsp;<small>teguran administrasi seluruh laporan industri</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Teguran</li>
                <li class="active">Administrasi</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

               <div class="row">                    
                    <div class="col-lg-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Tahun</label>
                                <div class="col-sm-10">
                                    <select name="tahun" id="tahun" class="form-control input-sm">                                        
                                        <option value="2015" selected="selected">2015</option>
                                        <option value="2014">2014</option>
                                        <option value="2013">2013</option>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />                            
                        </div>
                    </div>                    
                </div>
                <br/>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">                                                        
                            <tr>
                                <th width="4%">No</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th width="10%">LHU Air Limbah</th>
                                <th width="10%">Catatan Debit Air</th>
                                <th width="10%">LHU Udara Emisi</th>
                                <th width="10%">LHU Udara Ambien</th>
                                <th width="10%">Manifest</th>
                                <th width="10%">Nercara B3</th>
                                <th width="10%">Surat Teguran</th>
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="9" class="text-center">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
var Document = {
    param: {
        dataperpage: 10,
        query: '',
        curpage: 0,
        numpage: 0
    },
    url: '<?php echo base_url("backend/teguran_adm/get_list"); ?>',
    search: function() {
        this.param.query = $('#query').val();
        this.param.tahun = $('#tahun').val();
        this.param.curpage = 0;
        this.load_data();
        return false;
    },
    search_field: function(e) {

    },
    set_page: function(n) {
        this.param.curpage = n;
        this.load_data();
        return false;
    },
    prev_page: function() {
        if(this.param.curpage > 0) {
            this.param.curpage--;
            this.load_data();
        }
        return false;
    },
    next_page: function() {
        if(this.param.curpage < this.param.numpage) {
            this.param.curpage++;
            this.load_data();
        }
        return false;
    },
    load_data: function() {        
        $.ajax({
            url: Document.url,
            type: 'POST',
            dataType: 'json',
            data: $.param(Document.param),
            success: function(d) {                
                $('#pagination').html(d.pagination);
                if(d.total < 10){ $('#start').text(d.total); }                    
                $('#nums').text(d.total);
                Document.param.numpage = d.numpage;
                var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;                

                $.each(d.data, function(i, v) {
                      
                    t += '<tr>';
                    t += '<td>'+(no+=1)+'</td>';
                    t += '<td>'+v.nama_industri+'</td>';
                    t += '<td>'+$(this).extract(v.kualitas_air)+'</td>';
                    t += '<td>'+$(this).extract(v.catatan_debit)+'</td>';
                    t += '<td>'+$(this).extract(v.kualitas_emisi)+'</td>';
                    t += '<td>'+$(this).extract(v.kualitas_ambien)+'</td>';
                    t += '<td>'+$(this).extract(v.manifest)+'</td>';
                    t += '<td>'+$(this).extract(v.neraca_b3)+'</td>';  
                    t += '<td>';
                    t += '<a class="btn btn-xs btn-warning" href="'+'<?php echo site_url("backend/teguran_adm/generate_teguran/'+v.id_industri+'"); ?>'+'" data-toggle="tooltip" data-placement="bottom" title="Unduh"><i class="fa fa-download"></i>&nbsp;&nbsp;Unduh</a>';
                    t += '</td>';                    
                    t += '</tr>';              
                });
                
                $('#document-data').html(t);
            }
        })
        .fail(function(e) {
            console.log(e);
        });            
    }
} 

$(document).ready(function () {  

    Document.search();  

    $('#tahun').change(function() {
        Document.search();
    });

    $.fn.search = function(e, v) {
        if(e.keyCode == 13 || v.value == '') {
            Document.search();                
        }
    }

    $.fn.addteguran = function() {
        $.ajax({
            url: '',
            type: 'POST',
            dataType: '',
            data: {param1: 'value1'},
            success: function(d) {

            }
        })
        .fail(function() {
            console.log("error");
        });
        
    }

    $.fn.extract = function(months) {

        var tipe = months.split(":");        
        var html = '';   
        
        if(tipe[0] != '') {
            var t = tipe[0].split(",");            
            $.each(t, function(i, v) {
                html += '<i class="fa fa-check-circle" style="color: #8cc152;"></i>&nbsp;&nbsp;' + $(this).get_month(parseInt(v)) + '<br/>';
            });
        }

        if(tipe[1] != '') {

            var t = '';           

            if(tipe[1].indexOf(",") >= 0){ t = tipe[1].split(",");}
            else{ t = [tipe[1]]; }
                     
            $.each(t, function(i, v) {
                if (typeof v != 'undefined') {
                    html += '<i class="fa fa-times-circle" style="color: #e9573f;"></i>&nbsp;&nbsp;' + $(this).get_month(parseInt(v)) + '<br/>';
                }else{
                    html += '<i class="fa fa-times-circle" style="color: #e9573f;"></i>&nbsp;&nbsp;'+$(this).get_month(i+1);
                }
            });
        }
    
        return html;
    }
});
</script>