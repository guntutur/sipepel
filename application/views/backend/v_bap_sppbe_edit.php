    <!-- 
    ===========================================================
    BEGIN PAGE
    ===========================================================
    -->
    <div class="wrapper">

        <?php $this->load->view('include/top_nav'); ?>
        <?php $this->load->view('include/sidebar'); ?>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- Begin page heading -->
                <h1 class="page-heading">BAP SPPBE&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan SPPBE</small></h1>
                <!-- End page heading -->

                <!-- Begin breadcrumb -->
                <ol class="breadcrumb default square rsaquo sm">
                    <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                    <li>Manajemen BAP</li>
                    <li class="active">BAP SPPBE</li>
                </ol>
                <!-- End breadcrumb -->

                <!-- BEGIN DATA TABLE -->
                <div class="the-box no-padding">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="<?php echo (($tab_edit!= '') ? 'active' : ''); ?>" style="<?php echo (($tab_edit== '') ? 'display:none' : ''); ?>"><a href="#edit" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                        <li class="<?php echo (($tab_edit_new!= '') ? 'active' : ''); ?>" style="<?php echo (($tab_edit_new== '') ? 'display:none' : ''); ?>"><a href="#edit_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane <?php echo (($tab_edit!= '') ? 'active' : ''); ?>" id="edit" style="<?php echo (($tab_edit== '') ? 'display:none' : ''); ?>">

                            <div class="row">
                                <div class="modal-body">  
                                    <div class="col-sm-12">
                                        <div class="col-sm-6 text-right">
                                            <h4 class="light">Form BAP SPPBE</h4>
                                        </div>
                                        <div class="col-sm-6 text-left">
                                            <form action="<?php echo base_url('/backend/bap_sppbe/getEditForm'); ?>" method="post">
                                                <input type="hidden" name="id_bap" value="<?php echo $bap->id_bap; ?>"/>
                                                <button type="submit" class="btn btn-primary btn-rounded-lg">Unduh Form BAP</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-body">
                                <form role="form" action="<?php echo base_url('/backend/bap_sppbe/saveEdit') ?>" method="post" id="form_add" enctype="multipart/form-data">
                                    <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label>Tanggal</label>
                                        <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD" value="<?php echo $bap->bap_tgl;?>" >                  
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                            <input id="timepick1" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label>Petugas Pengawas</label>
                                            <select class="form-control input-sm" name="id_pegawai">
                                                <option value=""></option>
                                                <?php 
                                                    foreach ($pegawai as $p) {
                                                        echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                    }?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Beserta Anggota Pengawas:</label>
                                            <select class="form-control input-sm" name="petugas_pengawas" id="id_petugas_bap1" multiple="multiple" style="border: 0!important;">
                                                <?php 
                                                    foreach ($pegawai as $p) {
                                                        echo "<option value='$p->id_pegawai' ";
                                                        for ($i=0; $i < count($petugas_bap); $i++) { 
                                                            if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                                echo "selected";
                                                            }
                                                        }
                                                        echo ">$p->nama_pegawai</option>";
                                                    }?>
                                            </select>
                                        </div>
                                    </div>
                                    <fieldset>
                                        <legend>Lokasi Pengawasan / Pembinaan</legend>    
                                        <div class="form-group ">
                                            <label>Nama Usaha / Kegiatan</label>
                                            <select class="form-control input-sm" onchange="$(this).get_detail_industri(this, '#container1');" name="id_industri">
                                                <?php 
                                                    foreach ($industri as $i) {
                                                        echo "<option value='$i->id_industri' ".(($bap->id_industri==$i->id_industri) ? 'selected' : '').">$i->nama_industri</option>";
                                                    }?>
                                            </select>
                                        </div>
                                        <div id="container1"></div>
                                    </fieldset> 
                                    <fieldset>
                                        <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                        <div class="form-group ">
                                            <label>Nama</label>
                                            <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama;?>"/>
                                        </div>
                                        <div class="form-group ">
                                            <label>Telepon</label>
                                            <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp;?>"/>
                                        </div>
                                        <div class="form-group ">
                                            <label>Jabatan</label>
                                            <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan;?>"/>
                                        </div> 
                                    </fieldset>
                                            <div class="form-group">
                                                <h4 class="light">Upload Form BAP Industri</h4>
                                                <div class="input-group">
                                                    <input type="text" class="form-control input-sm" readonly="">
                                                    <span class="input-group-btn">
                                                        <span class="btn btn-default btn-file">
                                                            Browse… <input type="file" name="file" accept=".xlsx">
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>

                                    <div class="form-group ">    
                                        <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_sppbe'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                       
                                        <input type="hidden" name="id_bap" value="<?php echo $bap->id_bap;?>" />

                                        <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i>Simpan</button>
                                    </div>

                                </form> 
                            </div>                                        
                        </div>

                        <div class="tab-pane <?php echo (($tab_edit_new!= '') ? 'active' : ''); ?>" id="edit_new" style="<?php echo (($tab_edit_new== '') ? 'display:none' : ''); ?>">
                            <div class="row" style="margin-top: 10px;">       
                                <div class="col-xs-3" id="vertical_tab"> <!-- required for floating -->
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a href="#data_umum" id="Data Umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                    <li><a href="#pencemaran_air" id="Pencemaran Air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                    <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                    <li><a href="#pencemaran_b3" id="Limbah Padat dan B3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                    <li><a href="#lain_lain" id="Keterangan Tambahan" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Keterangan Tambahan</a></li>
                                  </ul>
                                </div>
                                <div class="col-xs-9">
                              <!-- Tab panes -->
                                <form role="form" action="<?php echo base_url('/backend/bap_sppbe/edit_from_form') ?>" method="post" id="add_form" enctype="multipart/form-data">
                                  <div class="tab-content">
                                    <div class="tab-pane active form-validated" id="data_umum">
                                       <h4 class="page-tab"><i class="fa fa-newspaper-o icon-sidebar"></i>Data Umum Usaha/Kegiatan</h4>
                                        <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label>Tanggal Pengawasan</label>
                                            <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD" value="<?php echo $bap->bap_tgl;?>">
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Pukul</label>
                                            <div class="bfh-timepicker">
                                            <input id="timepick2" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Petugas Pengawas</label>
                                                    <select class="form-control input-sm" name="id_pegawai">
                                                        <option value=""></option>
                                                        <?php 
                                                        foreach ($pegawai as $p) {
                                                            echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                        }?>
                                                    </select>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label>Beserta Anggota Pengawas:</label>
                                                    <select class="form-control input-sm multiselect" name="petugas_pengawas[]" id="ini_id_petugas_bap2" multiple="multiple" style="border: 0!important;">
                                                    <?php 
                                                        foreach ($pegawai as $p) {
                                                            echo "<option value='$p->id_pegawai' ";
                                                            for ($i=0; $i < count($petugas_bap); $i++) { 
                                                                if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                                    echo "selected";
                                                                }
                                                            }
                                                            echo ">$p->nama_pegawai</option>";
                                                        }?>
                                                </select>
                                            </div>
                                        </div>
                                    <fieldset>
                                        <legend>Lokasi Pengawasan / Pembinaan</legend>    
                                        <div class="form-group status">                        
                                        <label>Nama Usaha/Kegiatan</label>
                                        <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                            <option value="Empty">&nbsp;</option>
                                                            <?php
                                                                foreach ($industri as $value) {
                                                                    echo '<option value="'.$value->id_industri.'"';
                                                                        if($bap->id_industri==$value->id_industri){
                                                                            echo 'selected';
                                                                        }
                                                                    echo '>'.$value->nama_industri.'</option>';
                                                                }
                                                            ?>
                                                    </select>
                                            <!-- <input type="text" value="<?php echo $bap->nama_industri?>" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                            <input type="hidden" id="id_industri" name="id_industri" value="<?php echo $bap->id_industri?>"/> -->
                                        </div>
                                        <div id="container2" style="display:none"></div>
                                    </fieldset>
                                    <div class="form-group ">
                                        <label>Kapasitas Pengisian</label>
                                        <input type="text" name="kapasitas_pengisian" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->kapasitas_pengisian?> "/>
                                    </div>
                                    <div class="form-group ">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label>Koordinat SPPBE S</label>
                                                    <div class="form-group status">
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_derajat_s?>" name="koord_sppbe_derajat_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_jam_s?>" name="koord_sppbe_jam_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">'</span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_menit_s?>" name="koord_sppbe_menit_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">"</span>
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Koordinat SPPBE E</label>
                                                    <div class="form-group status">
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_derajat_e?>" name="koord_sppbe_derajat_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_jam_e?>" name="koord_sppbe_jam_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">'</span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_menit_e?>" name="koord_sppbe_menit_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">"</span>
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                        <div class="table-responsive">
                                        <table class="table table-striped table-hover" id="datatable-example">
                                        <thead class="the-box dark full">
                                            <tr>
                                            <!-- <div class="form-group "> -->
                                                <th>Dokumen Lingkungan</th>
                                                <th>Izin Lingkungan</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tr>
                                            <td>
                                                    <!-- <div class="radio"> -->
                                                <label>
                                                    <input class="dl" type="radio" name="dl" id="dl1" value="ada" <?php echo (($bap->dok_lingk==1) ? 'checked' : '')?>>Ada
                                                </label>
                                                <label>
                                                    <input class="dl" type="radio" name="dl" id="dl2" value="tidak ada" <?php echo (($bap->dok_lingk==0) ? 'checked' : '')?>>Tidak Ada
                                                </label>
                                                    <!-- </div> -->
                                            </td>
                                                <!-- </div> -->
                                                <!-- <div class="form-group "> -->
                                            <td>
                                                    <!-- <div class="radio"> -->
                                                <label>
                                                    <input type="radio" name="il" id="il1" value="ada" <?php echo (($bap->izin_lingk==1) ? 'checked' : '')?>>Ada
                                                </label>
                                                <label>
                                                    <input type="radio" name="il" id="il2" value="tidak ada" <?php echo (($bap->izin_lingk==0) ? 'checked' : '')?>>Tidak Ada
                                                </label>
                                                    <!-- </div> -->
                                            </td>
                                            
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group " id="jdl" name="jdl" style="display:none">
                                                            <!-- <div class="form-group "> -->
                                                        <label>Jenis Dokumen</label>
                                                        <select class="form-control input-sm" name="jdl_name" id="jdl_name">
                                                            <option value="">-- Pilih salah satu --</option>>
                                                            <?php 
                                                                foreach ($dok_lingk_jenis as $j) {
                                                                    echo "<option value='$j' ".(($bap->dok_lingk_jenis == $j) ? 'selected' : '').">$j</option>";
                                                            }?>
                                                        </select>
                                                            <!-- </div> -->
                                                            <!-- <div class="form-group "> -->
                                                        <label>Tahun</label>
                                                        <input type="text" name="dlt" id="dlt" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun; ?>"/>
                                                            <!-- </div> -->
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group " id="il_tahun" style="display:none">
                                                        <label>Tahun</label>
                                                        <input type="text" name="il_tahun" id="ilt" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun; ?>"/>
                                                    </div>
                                                </td>
                                                
                                            </tr>
                                            <tr>
                                                <div class="form-group ">
                                                    <label>Jumlah Karyawan</label>
                                                    <input type="text" name="jml_karyawan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->jumlah_karyawan; ?>"/>
                                                </div>
                                            </tr>
                                            <!-- </div> -->
                                        </tbody>
                                        </table>
                                        </div>
                                        <fieldset>
                                            <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                            <div class="form-group ">
                                                <label>Nama</label>
                                                <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama; ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Telepon</label>
                                                <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp; ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jabatan</label>
                                                <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan; ?>"/>
                                            </div> 
                                        </fieldset>
                                        <div class="col-sm-6">
                                    </div>
                                        <div class="col-sm-6 text-right">
                                                <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                            </div>
                                    </div>
                                    <div class="tab-pane" id="pencemaran_air">          
                                        <h4 class="page-tab">Pengendalian Pencemaran Air</h4>
                                        <fieldset>
                                        <div class="form-group">
                                            <label>a. Sumber Air dan Penggunaan</label>
                                        <!-- </div> -->
                                        <div class="form-group">
                                        <div class="form-group col-lg-6">
                                            <label>Air Tanah</label>
                                            <input type="text" name="air_tanah" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_tanah; ?>"/>
                                            <div class="radio">
                                            <label>Perizinan    : </label>
                                                    <label>
                                                        <input type="radio" name="izin_air_tanah" value="ada" <?php echo (($pencemaran_air->ambil_air_tanah_izin==1) ? 'checked' : '')?>>Ada
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="izin_air_tanah" value="tidak ada" <?php echo (($pencemaran_air->ambil_air_tanah_izin==0) ? 'checked' : '')?>>Tidak Ada
                                                    </label>
                                                </div>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Air Permukaan</label>
                                            <input type="text" name="air_permukaan" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_permukaan; ?>"/>
                                            <div class="radio">
                                                </div>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Air PDAM</label>
                                            <input type="text" name="air_pdam" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_pdam; ?>"/>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Air Lain-lain</label>
                                            <input type="text" name="air_lain" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_lain; ?>"/>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="form-group ">
                                            <label>b. Penggunaan Air</label>
                                            <input type="text" name="air_guna" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->penggunaan_air; ?>"/>
                                        </div>
                                        <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>b. Sumber Air Limbah</label>
                                                    <!-- <select name="limb_sumber" id="" class="form-control input-sm"> -->
                                                    <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                                    <!-- <option value=''></option> -->
                                                        <?php
                                                            $arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);
                                                            foreach ($limb_sumber as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                for ($i=0; $i < count($arr_limb_sumber) ; $i++) { 
                                                                
                                                                    if ($arr_limb_sumber[$i]==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>c. Badan Air Penerima</label>
                                                    <select name="bdn_terima" id="" class="form-control input-sm">
                                                    <option value=''></option>
                                                        <?php
                                                            foreach ($bdn_terima as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                    if ($pencemaran_air->bdn_terima==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <div class="form-group ">
                                            <div class="form-group ">
                                                <label>e. Sarana Pengolahan Air Limbah</label>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="sarolim" id="sarolim1" value="ada" <?php echo (($pencemaran_air->sarana_olah_limbah==1) ? 'checked' : '')?>>Ada
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="sarolim" id="sarolim2" value="tidak ada" <?php echo (($pencemaran_air->sarana_olah_limbah==0) ? 'checked' : '')?>>Tidak Ada
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group " id="sarolim" name="sarolim" style="display:none">
                                            <div class="form-group">
                                                <label>Jenis Sarana</label>
                                                <input type="text" name="sarana_jenis" id="sarana_jenis" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->sarana_jenis; ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Kapasitas</label>
                                                <input type="text" name="sarana_kapasitas" id="kapasitas" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->sarana_kapasitas; ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet S</label>
                                                        <div class="form-group status">
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" value="<?php echo $pencemaran_air->sarana_koord_derajat_s?>" name="xkoord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                                                        <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" value="<?php echo $pencemaran_air->sarana_koord_jam_s?>" name="xkoord_outlet_jam_s" id="jam_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">'</span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" value="<?php echo $pencemaran_air->sarana_koord_menit_s?>" name="xkoord_outlet_menit_s" id="menit_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">"</span>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet E</label>
                                                        <div class="form-group status">
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" value="<?php echo $pencemaran_air->sarana_koord_derajat_e?>" name="xkoord_outlet_derajat_e" id="derajat_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" value="<?php echo $pencemaran_air->sarana_koord_jam_e?>" name="xkoord_outlet_jam_e" id="jam_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">'</span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" value="<?php echo $pencemaran_air->sarana_koord_menit_e?>" name="xkoord_outlet_menit_e" id="menit_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">"</span>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                            <div class="col-sm-6">
                                                <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                            </div><!-- /.col-sm-6 -->
                                            <div class="col-sm-6 text-right">
                                                <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                            </div><!-- /.col-sm-6 -->
                                    </div>

                                    <div class="tab-pane" id="pencemaran_udara">
                                                <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                                
                                                <div class="form-group ">
                                                    <label>a. Data Kualitas Udara Ambien</label>
                                                    <table class="table table-th-block">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <?php 
                                                                    // $n=1;
                                                                    foreach($uji_ambien as $site) {
                                                                        echo "<th class='text-center'>$site->lokasi</th>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><label>Pengujian Kualitas</label></td>
                                                                <?php
                                                                    $c = 1;
                                                                    foreach ($uji_ambien as $dt) {
                                                                        $id_uji_ambien[$c] = $dt->id_uji_ambien;
                                                                        $tuk[$c] = adaConv($dt->uji_kualitas);
                                                                        $per[$c] = $dt->period;
                                                                        $lab_uk[$c] = $dt->lab;
                                                                        $bm[$c] = $dt->bm_pemenuhan;
                                                                        $bm_param[$c] = $dt->bm_param;
                                                                        $bm_period[$c] = $dt->bm_period;
                                                                        $c++;
                                                                    }

                                                                    for($p=1; $p<=3; $p++) {
                                                                        echo "<input type='hidden' name='id_uji_ambien$p' value='".$id_uji_ambien[$p]."' />";
                                                                        echo "<td><select class='form-control input-sm' name='uji_kualitas$p'>";
                                                                        $n=1;
                                                                        foreach ($bool as $f) {
                                                                            echo "<option value='".adaConv($f)."' " ;
                                                                                if (strcasecmp($tuk[$p], adaConv($f)) == 0){
                                                                                    echo "selected='true'";
                                                                                }
                                                                            echo ">".adaConv($f)."</option>";
                                                                            $n++;
                                                                        }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                                <?php
                                                                for($p=1; $p<=3; $p++) {
                                                                        echo "<td><select class='form-control input-sm' name='period$p'>";
                                                                        $n=1;
                                                                        foreach ($bool as $f) {
                                                                            echo "<option value='".rutinConv($f)."' " ;
                                                                                if (strcasecmp($per[$p], rutinConv($f)) == 0){
                                                                                    echo "selected='true'";
                                                                                }
                                                                            echo ">".rutinConv($f)."</option>";
                                                                            $n++;
                                                                        }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Laboratorium Penguji</label></td>
                                                                <?php
                                                                    for($n=1; $n<=count($lab_uk); $n++){
                                                                        echo "<td><select name='lab$n' class='form-control input-sm '>";
                                                                            foreach ($lab as $l) {
                                                                                echo "<option value='$l->nama_lab' ".(($lab_uk[$n] == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                                            }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Pemenuhan BM</label></td>
                                                                <?php
                                                                for($p=1; $p<=3; $p++) {
                                                                        echo "<td><select class='form-control input-sm' name='bm_pemenuhan$p' id='bmp$p'>";
                                                                        $n=1;
                                                                        foreach ($bool as $f) {
                                                                            echo "<option value='".yaConv($f)."' " ;
                                                                                if (strcasecmp(yaConv($bm[$p]), yaConv($f)) == 0){
                                                                                    echo "selected='true'";
                                                                                }
                                                                            echo ">".yaConv($f)."</option>";
                                                                            $n++;
                                                                        }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                                <?php
                                                                    for($n=1; $n<=count($bm_param); $n++){
                                                                        echo "<td><input type='text' name='bm_param$n' class='form-control input-sm has-feedback ' value='$bm_param[$n]'/></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Periode Tidak Memenuhi BM</label></td>
                                                                <?php
                                                                    for($n=1; $n<=count($bm_param); $n++){
                                                                        echo "<td><input type='text' name='bm_period$n' class='form-control input-sm has-feedback ' value='$bm_period[$n]'/></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php
                                                    $lapor = $bool;
                                                    $lapor[] = '2';
                                                ?>
                                                <div class="form-group ">
                                                    <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                    <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                    <?php 
                                                        for($d=0; $d<3; $d++){
                                                            echo "<option value='".rutinConv($lapor[$d])."'";
                                                                if (strcasecmp($pencemaran_udara->pelaporan_ua_ue, rutinConv($lapor[$d])) == 0){
                                                                    echo "selected";
                                                                }
                                                            echo ">".rutinConv($lapor[$d])."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </div>

                                                <div class="form-group ">
                                                    <label>c. Lain-lain</label>
                                                    <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_udara->lain_lain; ?>"/>
                                                </div>

                                                <div class="row"> <!-- navigation -->
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                    </div><!-- /.col-sm-6 -->
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                    </div><!-- /.col-sm-6 -->
                                                </div>
                                            </div>

                                    <div class="tab-pane" id="pencemaran_b3">
                                        <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
                                        <!-- <div class="row"> -->
                                            <?php 
                                                $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                                $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                                $new = array();
                                                for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                                    $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                                }
                                            ?>

                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">a. Jenis</th>
                                                        <th class="text-center">b. Jumlah (ton/hari)</th>
                                                        <th class="text-center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($new as $key => $value) { ?>
                                                    <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                        <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                                                        <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                                                        <td class="col-lg-2">
                                                            <div class=" text-right">
                                                                <div class="btn-group" style="padding-right: 17px;">
                                                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="form-group ">
                                                <label>c. Sarana Pengelolaan</label>
                                                <div class="form-group">
                                                    <div class="form-group col-sm-6">
                                                        <label>1. Tong/Bak Sampah</label>
                                                        <input type="text" name="padat_sarana_tong" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_sarana_tong; ?>" placeholder="unit" />
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label>2. TPS</label>
                                                        <input type="text" name="padat_sarana_tps" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_sarana_tong; ?>"placeholder="unit" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>d. Pengelolaan</label>
                                                <select class="form-control input-sm" id="pjpl" name="padat_kelola">
                                                    <?php 
                                                        $padat_kelola = $pencemaran_pb3->padat_kelola;
                                                        echo $padat_kelola;
                                                            if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola_lain' id='pjpl2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                        } else {
                                                            $temp = "Lainnya";
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola_lain' id='pjpl2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
                                                        }
                                                ?>
                                            </div>
                                            <fieldset>
                                                <legend>e. Lain-lain</legend>
                                                <div class="form-group ">
                                                    <textarea class="form-control input-sm no-resize bold-border" name="lain_lain_padat" ><?php echo $pencemaran_pb3->lain_lain;?></textarea>
                                                </div>
                                            </fieldset>
                                            <div class="col-sm-6">
                                                <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                            </div><!-- /.col-sm-6 -->
                                            <div class="col-sm-6 text-right">
                                                <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                            </div><!-- /.col-sm-6 -->
                                        <!-- </div> -->
                                    </div>
                                    <div class="tab-pane" id="lain_lain">
                                        <h4 class="page-tab">Keterangan Tambahan</h4>
                                        <!-- <div class="row"> -->
                                        <fieldset>
                                        <div class="form-group">
                                        <div class="form-group">
                                            <label>a. Sarana Pemadam Kebakaran</label>
                                        </div>
                                        <div class="form-group">
                                            <label>Kolam Pemadam Kebakaran</label>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Kolam Pemadam Kebakaran</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="jumlah_kolam_pemadam" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_kolam_pemadam?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Ukuran</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="ukuran_kolam_pemadam" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->ukuran_kolam_pemadam?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>APAR</label>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Jumlah</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="jumlah_apar" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_apar?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Kapasitas</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="kapasitas_apar" id="" class="form-control input-sm has-feedback" placeholder="kg" value="<?php echo $bap_sppbe->kapasitas_apar?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>b. Jumlah Agen yang dilayani</label>
                                        <input type="text" name="agen_dilayani" id="" class="form-control input-sm has-feedback" placeholder="Agen" value="<?php echo $bap_sppbe->agen_dilayani?>"/>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>c. Sarana Pemanenan Air Hujan</label>
                                        </div>
                                        <div class="form-group">
                                            <label>Sumur Resapan</label>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Jumlah</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="jumlah_sumur_resapan" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_sumur_resapan?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Ukuran</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="ukuran_sumur_resapan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->ukuran_sumur_resapan?>"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Lubang resapan Biopori</label>
                                        </div>
                                        <div class="form-group row">
                                            <div class="form-group col-lg-6">
                                                <label> -- Jumlah</label>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <input type="text" name="jumlah_biopori" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_biopori?>"/>
                                            </div>
                                        </div>
                                    </div>
                                                <legend>Lain-lain</legend>
                                                <div class="form-group ">
                                                    <textarea class="form-control input-sm no-resize bold-border" name="lain_lain_bap" style="height: 180px;"><?php echo $bap->catatan; ?></textarea>
                                                </div>
                                            </fieldset>
                                            <div class="col-sm-6">
                                                <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                            </div><!-- /.col-sm-6 -->
                                            <div class="col-sm-6 text-right">
                                                <input type="hidden" id="edit_id" name="id" value="<?php echo $bap->id_bap; ?>" />
                                                <input type="hidden" id="id_bap_sppbe" name="id_bap_sppbe" value="<?php echo $bap_sppbe->id_bap_sppbe; ?>" />
                                                <input type="hidden" id="id_penc_air" name="id_penc_air" value="<?php echo $pencemaran_air->id_penc_air; ?>" />
                                                <input type="hidden" id="id_penc_udara" name="id_penc_udara" value="<?php echo $pencemaran_udara->id_penc_udara; ?>" />
                                                <input type="hidden" id="id_penc_pb3" name="id_penc_pb3" value="<?php echo $pencemaran_pb3->id_penc_pb3; ?>" />
                                                <!-- <a class="btn btn-warning"> Finish</a> -->
                                                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_sppbe'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                                <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i>Simpan</button>
                                            </div>
                                        </div><!-- /.col-sm-6 -->
                                        <!-- </div> -->
                                    </div>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                    
                  <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                    <br/><br/> -->

                </div><!-- /.the-box .default -->
                <!-- END DATA TABLE -->

                <?php
                    function adaConv($value) {
                                    if ($value == true) {
                                        return "Ada";
                                    } else {
                                        return "Tidak Ada";
                                    }
                                } 

                                # function to convert boolean to string (Ya / Tidak)
                                function yaConv($value) {
                                    if ($value == true) {
                                        return "Ya";
                                    } else {
                                        return "Tidak";
                                    }
                                }

                                function rutinConv($value) {
                                    if ($value == '1') {
                                        return "Rutin";
                                    } else if ($value == '0'){
                                        return "Tidak Rutin";
                                    } else {
                                        return "Tidak Melaporkan";
                                    }
                                }
                ?>
    			
            </div><!-- /.container-fluid -->				
        </div><!-- /.page-content -->
    </div><!-- /.wrapper -->
    <!-- END PAGE CONTENT -->
    <script type="text/javascript">
    $("#timepick1").timepicker({
            showMeridian: false     
    });

    $("#timepick2").timepicker({
            showMeridian: false     
    });
</script>
<script type="text/javascript">
    // $(document).ready(function(){

    //     $("#form_edit").validate({

    //         highlight : function(element) {
    //             // alert(input);
    //             $(element).closest('.form-group').addClass('has-error');
    //             var div = $(element).parent().parent().parent().parent();
    //             if ($(div).find("div.tab-pane.active:has(div.has-error)").length == 0) {
    //                  $(div).find("div.tab-pane:has(div.has-error)").each(function (index, tab) {
    //                     var id = $(tab).attr("id");
    //                 $('a[href="#' + id + '"]').tab('show');
    //                  });
                    
    //             }
                
    //         },

    //         unhighlight : function(element) {
    //             // alert(input);
    //             $(element).closest('.form-group').removeClass('has-error');
    //             var div = $(element).parent().parent().parent().parent();
    //             if ($(div).find("div.tab-pane.active:has(div.has-error)").length == 0) {
    //                  $(div).find("div.tab-pane:has(div.has-error)").each(function (index, tab) {
    //                     var id = $(tab).attr("id");
    //                 $('a[href="#' + id + '"]').tab('show');
    //                  });
                    
    //             }
                
    //         },
            
    //         debug: true,
    //         ignore : [],
    //         rules : {
    //             bap_tgl : {
    //                 required : true
    //             },
    //             id_pegawai : {
    //                 required : true
    //             },
    //             kapasitas_pengisian : {
    //                 required : true
    //             },
    //             koord_sppbe_derajat_s : {
    //                 required : true
    //             },
    //             koord_sppbe_jam_s : {
    //                 required : true
    //             },
    //             koord_sppbe_menit_s : {
    //                 required : true
    //             },
    //             koord_sppbe_derajat_e : {
    //                 required : true
    //             },
    //             koord_sppbe_jam_e : {
    //                 required : true
    //             },
    //             koord_sppbe_menit_e : {
    //                 required : true
    //             },
    //             jml_karyawan : {
    //                 required : true
    //             },
    //             petugas_pengawas : {
    //                 required : true
    //             },
    //             id_industri : {
    //                 required : true
    //             },
    //             p2_nama : {
    //                 required : true
    //             },
    //             p2_telp : {
    //                 required : true
    //             },
    //             p2_jabatan : {
    //                 required : true
    //             },
    //             air_tanah : {
    //                 required : true
    //             },
    //             air_permukaan : {
    //                 required : true
    //             },
    //             air_guna : {
    //                 required : true
    //             },
    //             air_pdam : {
    //                 required : true
    //             },
    //             air_lain : {
    //                 required : true
    //             },
    //             limbah_sumber : {
    //                 required : true
    //             },
    //             limbah_penerima : {
    //                 required : true
    //             },
    //             lab1 : {
    //                 required : true
    //             },
    //             lab2 : {
    //                 required : true
    //             },
    //             lab3 : {
    //                 required : true
    //             },
    //             jumlah_kolam_pemadam : {
    //                 required : true
    //             },
    //             ukuran_kolam_pemadam : {
    //                 required : true
    //             },
    //             jumlah_apar : {
    //                 required : true
    //             },
    //             kapasitas_apar : {
    //                 required : true
    //             },
    //             agen_dilayani : {
    //                 required : true
    //             },
    //             jumlah_sumur_resapan : {
    //                 required : true
    //             },
    //             ukuran_sumur_resapan : {
    //                 required : true
    //             },
    //             jumlah_biopori : {
    //                 required : true
    //             },
    //             bm_param1 : {
    //                 required : function(element) {
    //                             return $("#bmp1").val() == 'Tidak'
    //                         }
    //             },
    //             bm_param2 : {
    //                 required : function(element) {
    //                             return $("#bmp2").val() == 'Tidak'
    //                         }
    //             },
    //             bm_param3 : {
    //                 required : function(element) {
    //                             return $("#bmp3").val() == 'Tidak'
    //                         }
    //             },
    //             bm_period1 : {
    //                 required : function(element) {
    //                             return $("#bmp1").val() == 'Tidak'
    //                         }
    //             },
    //             bm_period2 : {
    //                 required : function(element) {
    //                             return $("#bmp2").val() == 'Tidak'
    //                         }
    //             },
    //             bm_period3 : {
    //                 required : function(element) {
    //                             return $("#bmp3").val() == 'Tidak'
    //                         }
    //             },
    //             limbah_jenis : {
    //                 required : true
    //             },
    //             limbah_jml : {
    //                 required : true
    //             },
    //             padat_sarana_tong : {
    //                 required : true
    //             },
    //             padat_sarana_tps : {
    //                 required : true
    //             },
    //             padat_kelola_lain : {
    //                 required : function(element) {
    //                             return $("#pjpl").val() == 'Lainnya'
    //                         }
    //             },
    //             jdl_name : {
    //                 required : '#dl1:checked'
    //             },
    //             dlt : {
    //                 required : '#dl1:checked'
    //             },
    //             il_tahun : {
    //                 required : '#il1:checked'
    //             },
    //             iu_tahun : {
    //                 required : '#iu1:checked'
    //             },
    //             kapasitas : {
    //                 required : '#sarolim1:checked'
    //             },
    //             sarana_jenis : {
    //                 required : '#sarolim1:checked'
    //             },
    //             koord_outlet_derajat_s : {
    //                 required : '#sarolim1:checked'
    //             },
    //             koord_outlet_jam_s : {
    //                 required : '#sarolim1:checked'
    //             },
    //             koord_outlet_menit_s : {
    //                 required : '#sarolim1:checked'
    //             },
    //             koord_outlet_derajat_e : {
    //                 required : '#sarolim1:checked'
    //             },
    //             koord_outlet_jam_e : {
    //                 required : '#sarolim1:checked'
    //             },
    //             koord_outlet_menit_e : {
    //                 required : '#sarolim1:checked'
    //             }
    //         },

    //         submitHandler: function (form) {
    //             if($(form).valid())
    //                 form.submit();
    //             return false;
    //         }
        
    //     });
    // });
</script>
    <script type="text/javascript">
    $(document).ready(function() {

        var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search_industri_bap/21',
            onSearchStart: function (query) {
                $('#id_industri').val(null);              
            },
            onSelect: function (suggestion) {
                $('#id_industri').val(suggestion.data).change();
                var id_last_input = $('#id_industri').val();
                $('#id_last_input').val(id_last_input);
                console.log($('#id_last_input').val());
            }
        });

        $("select[name=id_industri]").change(function(){
                $(this).get_detail_industri(this, '#container2');
            });

        $('#id_petugas_bap1').multiselect({
            checkboxName: 'petugas_bap[]'
        });

        $('#id_petugas_bap2').multiselect({
            checkboxName: 'petugas_bap[]'
        });

        var dl = $('input[name=dl]:checked').val();
        if (dl == "ada") {
                $('#jdl').show();
        } else {
                $('#jdl').hide();
        }

        var il = $('input[name=il]:checked').val();
        if (il == "ada") {
                $('#il_tahun').show();
        } else {
                $('#il_tahun').hide();
                $('#il_tahun').val(null);
        }

        var iu = $('input[name=iu]:checked').val();
        if (iu == "ada") {
                $('#iu_tahun').show();
        } else {
                $('#iu_tahun').hide();
                $('#iu_tahun').val(null);
        }

        var sarolim = $('input[name=sarolim]:checked').val();
        if (sarolim == "ada") {
            $('#sarolim').show();
        } else {
            $('#sarolim').hide();
            $('#sarana_jenis').val(null);
            $('#kapasitas').val(null);
            $('#derajat_s').val(null);
            $('#jam_s').val(null);
            $('#menit_s').val(null);
            $('#derajat_e').val(null);
            $('#jam_e').val(null);
            $('#menit_e').val(null);
        }

        $('#sarolim1').click(function (e) {
            // alert(e);
            $('#sarolim').show();
        });
        $('#sarolim2').click(function (e) {
            // alert(e);
            $('#sarolim').hide();
            $('#sarana_jenis').val(null);
            $('#kapasitas').val(null);
            $('#derajat_s').val(null);
            $('#jam_s').val(null);
            $('#menit_s').val(null);
            $('#derajat_e').val(null);
            $('#jam_e').val(null);
            $('#menit_e').val(null);
        });

        $('#dl1').click(function (e) {
            // alert('#dl1');
            $('#jdl').show();
        });
        $('#dl2').click(function (e) {
            // alert('#dl2');
            $('#jdl').hide();
            $('#jdl_name').val(null);
            $('#dlt').val(null);
        });

        $('#il1').click(function (e) {
            $('#il_tahun').show();
        });
        $('#il2').click(function (e) {
            $('#il_tahun').hide();
            $('#ilt').val(null);
        });

        $('#pjpl').change(function() {
            if($(this).val()  == 'Lainnya') {
                $('#pjpl2').show();
                $('#pjpl2').focus();
            } else {
                $('#pjpl2').hide();
                $('#pjpl2').val(null);
            }
        });

    });
    </script>

    <!-- BEGIN BACK TO TOP BUTTON -->
    <div id="back-top">
        <a href="#top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <!-- END BACK TO TOP -->		
