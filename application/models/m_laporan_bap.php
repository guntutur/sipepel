<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Document   : m_laporan_bap.php 
// Created on : Desember 21, 2014 19:45 
// Author     : huda.jtk09@gmail.com 
// Description: Model for Laporan BAP

class m_laporan_bap extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * huda : get item ketaatan bap
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_ketaatan_bap($jenis_bap) {
		$data = array();

		$query = $this->db->query("SELECT a.nilai_parameter_bap, a.status, a.ket, a.id_parent as id_parent_1, b.id_parent as id_parent_2
									FROM parameter_bap a
									LEFT JOIN parameter_bap b ON a.id_parent = b.id_parameter_bap
									WHERE a.jenis_bap = '".$jenis_bap."'
									AND a.id_parent != 'top'");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function get_ketaatan_bap_order_urutan($jenis_bap) {
		$data = array();

		$query = $this->db->query("SELECT a.nilai_parameter_bap, a.status, a.ket, a.id_parent as id_parent_1, b.id_parent as id_parent_2
									FROM parameter_bap a
									LEFT JOIN parameter_bap b ON a.id_parent = b.id_parameter_bap
									WHERE a.jenis_bap = '".$jenis_bap."'
									AND a.id_parent != 'top'
									ORDER BY a.urutan");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	// huda : need improvement asap (8 Jan 2014 15:22)
	/** 
	 * huda : get judul item ketaatan bap
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_top($id_bap) {
		$query = $this->db->query("SELECT * 
									FROM parameter_bap WHERE id_parameter_bap IN 
									(SELECT id_parent_1
									 FROM history_item_teguran_bap
									 WHERE id_bap = ".$id_bap." and id_parent_2 = 'top'
									UNION
									SELECT id_parent_2
									 FROM history_item_teguran_bap
									 WHERE id_bap = ".$id_bap." and id_parent_2 != 'top')
									ORDER BY urutan ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	/**
	 * huda : get pelanggaran bap
	 *
	 * @param	int, int 
	 * @return	Array of Object
	 */
	function get_pelanggaran($id_bap, $id_top) {
		$query = $this->db->query("SELECT * 
									FROM history_item_teguran_bap
									WHERE id_bap = ".$id_bap."
									AND id_parent_1 = ".$id_top."
									AND id_parent_2 = 'top'
									UNION
									SELECT * 
									FROM history_item_teguran_bap
									WHERE id_bap = ".$id_bap."
									AND id_parent_2 = ".$id_top."
									AND id_parent_2 != 'top'");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	// huda : add -> method to get all bap with data umum (9 Dec 2014, 17:28)
	// huda : edit -> add limit and offset to create page with pagination (2 Jan 2015, 15:27)
	function get_all_data_umum($bap_type, $limit=0, $offset=0) {
		$data = array();

		if ($bap_type != 'all') {
			$query = 'SELECT a.id_bap, c.nama_industri, d.ket as jenis_industri, c.alamat, b.bap_tgl
						FROM '.$bap_type.' a
						LEFT JOIN bap b ON a.id_bap = b.id_bap
						INNER JOIN industri c ON b.id_industri = c.id_industri
						INNER JOIN jenis_industri d ON c.id_jenis_industri = d.id_jenis_industri
						ORDER BY a.id_'.$bap_type.' ASC
						LIMIT '.$offset.', '.$limit;
		} else {
			$query = 'SELECT a.id_bap, c.nama_industri, d.ket as jenis_industri, c.alamat, a.bap_tgl
						FROM bap a 
						LEFT JOIN industri c ON a.id_industri = c.id_industri
						INNER JOIN jenis_industri d ON c.id_jenis_industri = d.id_jenis_industri
						ORDER BY d.ket, a.id_bap ASC 
						LIMIT '.$offset.', '.$limit;
		}
		
		$results = $this->db->query($query);


		if ($results->num_rows() > 0) {
			foreach ($results->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	// huda : add -> method to get size bap with data umum to create pagination (2 Jan 2015, 15:41)
	// huda : edit -> add limit and offset to create page with pagination (2 Jan 2015, 15:27)
	function get_all_data_umum_size($bap_type) {

		if ($bap_type != 'all') {
			$query = $this->db->query('SELECT a.id_bap
										FROM '.$bap_type.' a');
		} else {
			$query = $this->db->query('SELECT a.id_bap
										FROM bap a');
		}		

		return ($query->num_rows() > 0) ? $query->num_rows() : 0;
	}

	// huda : add -> method to get all usaha kegiatan who have a pelanggaran (9 Dec 2014, 17:28)
	function get_usaha_teguran($bap_type, $limit=0, $offset=0) {
		$data = array();

		if ($bap_type != 'all') {
			$query = 'SELECT a.id_bap, c.nama_industri, d.ket as jenis_industri, c.alamat, b.bap_tgl, b.conf_pengirim, b.conf_status, b.status
						FROM '.$bap_type.' a
						LEFT JOIN bap b ON a.id_bap = b.id_bap
						INNER JOIN industri c ON b.id_industri = c.id_industri
						INNER JOIN jenis_industri d ON c.id_jenis_industri = d.id_jenis_industri
						WHERE a.id_bap IN (SELECT DISTINCT e.id_bap
											FROM history_item_teguran_bap e
											GROUP BY e.id_bap)
						ORDER BY d.ket, a.id_'.$bap_type.' ASC
						LIMIT '.$offset.', '.$limit;
		} else {
			$query = 'SELECT a.id_bap, c.nama_industri, d.ket as jenis_industri, c.alamat, a.bap_tgl, a.conf_pengirim, a.conf_status, a.status
						FROM bap a 
						LEFT JOIN industri c ON a.id_industri = c.id_industri
						INNER JOIN jenis_industri d ON c.id_jenis_industri = d.id_jenis_industri
						WHERE a.id_bap IN (SELECT DISTINCT e.id_bap
											FROM history_item_teguran_bap e
											GROUP BY e.id_bap)
						ORDER BY d.ket, a.id_bap ASC
						LIMIT '.$offset.', '.$limit;
		}
		
		$results = $this->db->query($query);

		if ($results->num_rows() > 0) {
			foreach ($results->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	// huda : add -> method to get size usaha kegiatan who have a pelanggaran to create pagination (2 Jan 2015, 17:27)
	function get_usaha_teguran_size($bap_type) {

		if ($bap_type != 'all') {
			$query = $this->db->query('SELECT a.id_bap
										FROM '.$bap_type.' a
										WHERE a.id_bap IN (SELECT DISTINCT e.id_bap
															FROM history_item_teguran_bap e
															GROUP BY e.id_bap)');
		} else {
			$query = $this->db->query('SELECT a.id_bap
										FROM bap a 
										WHERE a.id_bap IN (SELECT DISTINCT e.id_bap
															FROM history_item_teguran_bap e
															GROUP BY e.id_bap)');
		}		

		return ($query->num_rows() > 0) ? $query->num_rows() : 0;
	}

	// huda : add -> method to get bap with data umum (23 Dec 2014, 23:14)
	function get_data_umum($id_bap) {
		$query = $this->db->query('SELECT c.nama_industri, c.alamat, a.bap_tgl
									FROM bap a 
									LEFT JOIN industri c ON a.id_industri = c.id_industri
									INNER JOIN jenis_industri d ON c.id_jenis_industri = d.id_jenis_industri
									WHERE a.id_bap = '.$id_bap);
		
		
		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return NULL;
		}
	}

}
?>