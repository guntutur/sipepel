<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 19/12/16
 * Time: 21:41
 */
?>

<div class="tab-pane active" id="data_umum">
    <h4 class="page-tab">Data Umum</h4>

    <div class="row">
        <div class="form-group col-lg-6">
            <label>Tanggal</label>
            <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>" >
        </div>
        <div class="form-group col-lg-6">
            <label>Pukul</label>
            <div class="bfh-timepicker">
                <input id="timepick1" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>Petugas Pengawas</label>
            <select class="form-control input-sm" name="id_pegawai">
                <option value=""></option>
                <?php
                foreach ($pegawai as $p) {
                    echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                }?>
            </select>
        </div>
        <div class="form-group col-lg-6">
            <label>Beserta Anggota Pengawas:</label>
            <select data-placeholder="Pilih Pengawas..." class="form-control input-sm multiselect" multiple="multiple" name="id_petugas_bap_list[]" style="border: 0!important;">
                <?php
                foreach ($pegawai as $p) {
                    echo "<option value='$p->id_pegawai' ";
                    for ($i=0; $i < count($petugas_bap); $i++) {
                        if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                            echo "selected";
                        }
                    }
                    echo ">$p->nama_pegawai</option>";
                }?>
            </select>
        </div>
    </div> <br>
    <div class="form-group">
        <h4>Lokasi Pengawasan / Pembinaan</h4>
        <div class="form-group ">
            <label>Nama Usaha / Kegiatan</label>
            <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                <option value="Empty">&nbsp;</option>
                <?php
                foreach ($industri as $value) {
                    echo '<option value="'.$value->id_industri.'"';
                    if($bap->id_industri==$value->id_industri){
                        echo 'selected';
                    }
                    echo '>'.$value->nama_industri.'</option>';
                }
                ?>
            </select>
        </div>
        <div id="container2" style="display:none"></div>
    </div>
    <div class="form-group">
        <h4>Penanggung Jawab Usaha / Kegiatan</h4>
        <div class="form-group ">
            <label>Nama</label>
            <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama;?>"/>
        </div>
        <div class="form-group ">
            <label>Telepon</label>
            <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp;?>"/>
        </div>
        <div class="form-group ">
            <label>Jabatan</label>
            <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan;?>"/>
        </div>
    </div>

    <h4 class="page-tab">Data Umum Usaha/Kegiatan</h4>
    <div class="form-group ">
        <label>Jumlah Karyawan</label>
        <input type="text" name="jumlah_karyawan" class="form-control input-sm has-feedback" value="<?php echo $bap_rph->jumlah_karyawan;?>"/>
    </div>
    <div class="form-group ">
        <label>Jumlah Ternak</label>
        <input type="text" name="jumlah_ternak" class="form-control input-sm has-feedback" value="<?php echo $bap_rph->jumlah_ternak;?>"/>
    </div>
    <div class="form-group ">
        <div class="row">
            <div class="col-lg-6">
                <label>Koordinat Lokasi S</label>
                <div class="row">
                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo $bap_rph->koord_rph_derajat_s?>" name="koord_rph_derajat_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding: 0; margin:0;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo $bap_rph->koord_rph_jam_s?>" name="koord_rph_jam_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">'</span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo $bap_rph->koord_rph_menit_s?>" name="koord_rph_menit_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">"</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label>Koordinat Lokasi E</label>
                <div class="row">
                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo $bap_rph->koord_rph_derajat_e?>" name="koord_rph_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding: 0; margin:0;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo $bap_rph->koord_rph_jam_e?>" name="koord_rph_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">'</span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo $bap_rph->koord_rph_menit_e?>" name="koord_rph_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                            <span class="input-group-addon">"</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <div class="row">
            <div class="col-sm-6">
                <label>Dokumen Lingkungan</label>
                <select class="form-control input-sm" name="dok_lingk">
                    <option value="1" <?php echo (($bap->dok_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                    <option value="0" <?php echo (($bap->dok_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                </select>
            </div>
            <div class="col-sm-6">
                <label>Tahun</label>
                <input type="text" name="dok_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun;?>" />
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label>Jenis</label>
        <select class="form-control input-sm" name="dok_lingk_jenis" id="jdl_name">
            <option value="">-- Pilih salah satu --</option>
            <?php
            foreach ($jenis_dok_lingkungan as $j) {
                echo "<option value='$j->ket' ".(($bap->dok_lingk_jenis == $j->ket) ? 'selected' : '').">$j->ket</option>";
            }?>
        </select>
    </div>
    <div class="form-group ">
        <div class="row">
            <div class="col-sm-6">
                <label>Izin Lingkungan</label>
                <select class="form-control input-sm" name="izin_lingk">
                    <option value="1" <?php echo (($bap->izin_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                    <option value="0" <?php echo (($bap->izin_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                </select>
            </div>
            <div class="col-sm-6">
                <label>Tahun</label>
                <input type="text" name="izin_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun;?>"/>
            </div>
        </div>
    </div>

    <div class="form-group ">
        <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"><?php echo $bap->catatan;?></textarea>
    </div>

    <div class="row"> <!-- navigation -->
        <div class="col-sm-6 text-left">
            <input id="save-datum" class="btn btn-md btn-success save-partially" value="Simpan Data Umum"/>
        </div>
        <div class="col-sm-6 text-right">
            <a class="btn btn-warning NextStep">Pencemaran Air<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>
