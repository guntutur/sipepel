<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class bap_parameter extends CI_Controller {

	private $jenis_bap = array('bap_agro','bap_hotel','bap_industri','bap_rm','bap_rph','bap_rs','bap_sppbe');

    function __construct() {
        parent::__construct();

        $this->load->model('m_bap_parameter', '', TRUE);
		$this->load->model('m_business_activity', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types        
		$types = $this->m_bap_parameter->get_all_jenis_bap();
		
		# response paramter        
		$data['usaha_kegiatan'] = $this->m_business_activity->get_dropdown();        
		
		# load view
        $this->load->view('include/header');        
		$this->load->view('backend/v_item_ketaatan_bap', $data);
        $this->load->view('include/footer');
    }

	public function get_list_usaha_kegiatan() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'ket' => array('SEARCH', 'OR', $query)
			);

     	$total = $this->m_bap_parameter->get_num_rows_jenis_bap($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_bap_parameter->get_all_jenis_bap($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	
	/* ----------------- detail --------------------------*/
	
	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $par_bap = $this->m_bap_parameter->get_usaha_kegiatan_by_id($id);
		$par = $this->m_bap_parameter->get_parameter($id);

        $data['title'] = "ITEM KETAATAN ".$par_bap->ket;
		$data['parameter'] = $par;
        //$data['jenis_bap'] = $this->jenis_bap;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_bap_parameter', $data);
        $this->load->view('include/footer');
	}
	
    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		foreach ($input as $val)
     		$$val = $this->input->post($val);  

     	/*$where = array(
				'u.ket' => array('SEARCH', 'OR', $query),
				'p.ket' => array('SEARCH', 'OR', $query),				
				'p.status' => array('', 'AND', 1)
			);*/
		
		$where = "p.ket LIKE '%". $query ."%' AND p.nilai_parameter_bap != '-' AND p.nilai_parameter_bap != '' AND p.id_jenis_bap = ".$id;

     	$total = $this->m_bap_parameter->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_bap_parameter->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_bap_parameter->get_bap_parameter_by_id($this->input->post('id_parameter_bap')));
		exit();
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'ket' => $this->input->post('description'),
			'id_parent' => $this->input->post('parent'),
			'urutan' => $this->input->post('urutan'),
			'dasar_hukum' => $this->input->post('dasar_hukum'),
			'status' => $this->input->post('status'),
			'nilai_parameter_bap' => $this->input->post('value_parameter'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_bap_parameter->change_bap_parameter($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function register() {
		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$jenis_bap = $this->m_bap_parameter->get_usaha_kegiatan_by_id($this->input->post('id'));	

			$param = array(
				'id_jenis_bap' => $this->input->post('id'),
				'urutan' => $this->input->post('urutan'),
				'id_parent' => $this->input->post('parent'),
				'ket' => $this->input->post('description'),
				'dasar_hukum' => $this->input->post('dasar_hukum'),
				'status' => $this->input->post('status'),
				'jenis_bap' => $jenis_bap->value,
				'nilai_parameter_bap' => $this->input->post('value_parameter'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
		
			if($this->m_bap_parameter->insert_bap_parameter($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function delete() {
		$this->m_bap_parameter->delete_bap_parameter($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function validate() {
		# check data yang duplicate
		if($this->m_bap_parameter->is_duplicate($this->input->post('id'), $this->input->post('description'), $this->input->post('parent'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}

}
