<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 18/12/16
 * Time: 12:48
 */
?>

<div class="tab-pane" id="pencemaran_udara">
    <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>

    <div class="form-group ">
        <label>a. Data Kualitas Udara Ambien</label>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th></th>
                <th class="text-center">Upwind</th>
                <th class="text-center">Site</th>
                <th class="text-center">Downwind</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><label>Pengujian Kualitas</label></td>
                <td><select class="form-control input-sm" name="uji_kualitas1"><option value="1" <?php echo isset($uji_ambien) ? (($uji_ambien[0]->uji_kualitas == true) ? 'selected' : '') : ""; ?> >Ada</option><option value="0" <?php echo isset($uji_ambien) ? (($uji_ambien[0]->uji_kualitas == false) ? 'selected' : '') : ""; ?> >Tidak Ada</option></select></td>
                <td><select class="form-control input-sm" name="uji_kualitas2"><option value="1" <?php echo isset($uji_ambien) ? (($uji_ambien[1]->uji_kualitas == true) ? 'selected' : '') : ""; ?> >Ada</option><option value="0" <?php echo isset($uji_ambien) ? (($uji_ambien[1]->uji_kualitas == false) ? 'selected' : '') : ""; ?> >Tidak Ada</option></select></td>
                <td><select class="form-control input-sm" name="uji_kualitas3"><option value="1" <?php echo isset($uji_ambien) ? (($uji_ambien[2]->uji_kualitas == true) ? 'selected' : '') : ""; ?> >Ada</option><option value="0" <?php echo isset($uji_ambien) ? (($uji_ambien[2]->uji_kualitas == false) ? 'selected' : '') : ""; ?> >Tidak Ada</option></select></td>
            </tr>
            <tr>
                <td><label>Periode Pengujian (per 6 bulan)</label></td>
                <td><select class="form-control input-sm" name="period1"><option value="Rutin" <?php echo isset($uji_ambien) ? (($uji_ambien[0]->period == 'Rutin') ? 'selected' : '') : "";?> >Rutin</option><option value="Tidak Rutin" <?php echo isset($uji_ambien) ? (($uji_ambien[0]->period == 'Tidak Rutin') ? 'selected' : '') : ""; ?> >Tidak Rutin</option></select></td>
                <td><select class="form-control input-sm" name="period2"><option value="Rutin" <?php echo isset($uji_ambien) ? (($uji_ambien[1]->period == 'Rutin') ? 'selected' : '') : "";?> >Rutin</option><option value="Tidak Rutin" <?php echo isset($uji_ambien) ? (($uji_ambien[1]->period == 'Tidak Rutin') ? 'selected' : '') : ""; ?> >Tidak Rutin</option></select></td>
                <td><select class="form-control input-sm" name="period3"><option value="Rutin" <?php echo isset($uji_ambien) ? (($uji_ambien[2]->period == 'Rutin') ? 'selected' : '') : "";?> >Rutin</option><option value="Tidak Rutin" <?php echo isset($uji_ambien) ? (($uji_ambien[2]->period == 'Tidak Rutin') ? 'selected' : '') : ""; ?> >Tidak Rutin</option></select></td>
            </tr>
            <tr>
                <td><label>Laboratorium Penguji</label></td>
                <?php
                if (isset($uji_ambien)) {
                    for($i=0; $i<count($uji_ambien); $i++) {
                        echo '<input type="hidden" name="id_uji_ambien' . ($i + 1) . '" value="' . $uji_ambien[$i]->id_uji_ambien . '"/>';
                    }
                }
                ?>
                <?php if (isset($uji_ambien)) {
                    for ($i = 0; $i < count($uji_ambien); $i++) {
                        echo "<td><select name='lab" . ($i + 1) . "' class='form-control input-sm '>";
                        foreach ($lab as $l) {
                            echo "<option value='$l->nama_lab' " . (($uji_ambien[$i]->lab == $l->nama_lab) ? 'selected' : '') . ">$l->nama_lab</option>";
                        }
                        echo "</select></td>";
                    }
                } else {
                    for ($i=1; $i<4; $i++) {
                        echo "<td><select name='lab".$i."' class='form-control input-sm'>";
                        echo "<option value=''></option>";
                        foreach ($lab as $key) {
                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                        }
                        echo '</select></td>';
                    }
                }
                ?>
            </tr>
            <tr>
                <td><label>Pemenuhan BM</label></td>
                <td><select class="form-control input-sm" id="bmp1" name="bm_pemenuhan1"><option value="1" <?php echo isset($uji_ambien) ? (($uji_ambien[0]->bm_pemenuhan == true) ? 'selected' : '') : ""; ?> >Ya</option><option value="0" <?php echo isset($uji_ambien) ? (($uji_ambien[0]->bm_pemenuhan == false) ? 'selected' : '') : ""; ?> >Tidak</option></select></td>
                <td><select class="form-control input-sm" id="bmp2" name="bm_pemenuhan2"><option value="1" <?php echo isset($uji_ambien) ? (($uji_ambien[1]->bm_pemenuhan == true) ? 'selected' : '') : ""; ?> >Ya</option><option value="0" <?php echo isset($uji_ambien) ? (($uji_ambien[1]->bm_pemenuhan == false) ? 'selected' : '') : ""; ?> >Tidak</option></select></td>
                <td><select class="form-control input-sm" id="bmp3" name="bm_pemenuhan3"><option value="1" <?php echo isset($uji_ambien) ? (($uji_ambien[2]->bm_pemenuhan == true) ? 'selected' : '') : ""; ?> >Ya</option><option value="0" <?php echo isset($uji_ambien) ? (($uji_ambien[2]->bm_pemenuhan == false) ? 'selected' : '') : ""; ?> >Tidak</option></select></td>
            </tr>
            <tr>
                <td><label>Parameter Tidak Memenuhi BM</label></td>
                <td><input type="text" id="bm_param1" name="bm_param1" class="form-control input-sm has-feedback " value="<?php echo isset($uji_ambien) ? $uji_ambien[0]->bm_param : "";?>"/></td>
                <td><input type="text" id="bm_param2" name="bm_param2" class="form-control input-sm has-feedback " value="<?php echo isset($uji_ambien) ? $uji_ambien[1]->bm_param : "";?>"/></td>
                <td><input type="text" id="bm_param3" name="bm_param3" class="form-control input-sm has-feedback " value="<?php echo isset($uji_ambien) ? $uji_ambien[2]->bm_param : "";?>"/></td>
            </tr>
            <tr>
                <td><label>Periode Tidak Memenuhi BM</label></td>
                <td><input type="text" id="bm_period1" name="bm_period1" class="form-control input-sm has-feedback " value="<?php echo isset($uji_ambien) ? $uji_ambien[0]->bm_period : "";?>"/></td>
                <td><input type="text" id="bm_period2" name="bm_period2" class="form-control input-sm has-feedback " value="<?php echo isset($uji_ambien) ? $uji_ambien[1]->bm_period : "";?>"/></td>
                <td><input type="text" id="bm_period3" name="bm_period3" class="form-control input-sm has-feedback " value="<?php echo isset($uji_ambien) ? $uji_ambien[2]->bm_period : "";?>"/></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="form-group ">
        <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
        <select class="form-control input-sm" name="pelaporan_ua_ue">
            <option value="Rutin" <?php echo isset($pencemaran_udara) ? (($pencemaran_udara->pelaporan_ua_ue == 'Rutin') ? 'selected' : '') : ""; ?> >Rutin</option>
            <option value="Tidak Rutin" <?php echo isset($pencemaran_udara) ? (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Rutin') ? 'selected' : '') : ""; ?> >Tidak Rutin</option>
            <option value="Tidak Melaporkan" <?php echo isset($pencemaran_udara) ? (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Melaporkan') ? 'selected' : '') : ""; ?> >Tidak Melaporkan</option>
        </select>
    </div>

    <div class="form-group ">
        <label>c. Lain-lain</label>
        <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_udara) ? $pencemaran_udara->lain_lain : "";?>"/>
    </div>

    <div class="row"> <!-- navigation -->
        <div class="col-sm-4 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Air</a>
        </div>
        <div class="col-sm-4 text-center">
            <input id="save-pencemaran_udara" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Udara"/>
        </div>
        <div class="col-sm-4 text-right">
            <a class="btn btn-warning NextStep">Pencemaran PB3<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>