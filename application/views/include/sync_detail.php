<!-- 
	Description : Javascript for ajax request to retrieve Industri information, izin air limbah and izin tps, may be developed
					to retrieve another information.
	Author 		: guntutur@gmail.com
	note 		: this section has been moved from header.php, now this should be pretty neat
-->

<script type="text/javascript">

$(document).ready(function(){

	// timmepicker format 24 hour
	$("#timepick1").timepicker({
        showMeridian: false     
    });

    $("#timepick2").timepicker({
        showMeridian: false     
    });
    // timmepicker format 24 hour

    $.fn.get_detail_industri = function(source, target) {
        var base_url = '<?php echo site_url(); //you have to load the "url_helper" to use this function ?>';
        var id = $(source).val();
        console.log(id);

        for(var z=1; z<5; z++){
        	$('select[name=tb_izin'+z+']').val(null).prop('disabled', false);
			$('input[name=tb_izin_no'+z+']').val(null);
			$('input[name=tb_izin_no'+z+']').prop('readonly', false);
			$('input[name=tb_izin_no'+z+']').prop('disabled', false);
			$('input[name=tb_izin_tgl'+z+']').val(null);
			$('input[name=tb_izin_tgl'+z+']').prop('readonly', false);
			$('input[name=tb_izin_tgl'+z+']').prop('disabled', false);
			$('input[name=tb_jenis'+z+']').val(null).prop('readonly', false);
			$('input[name=tb_jenis'+z+']').prop('disabled', false);
			$('input[name=koord_derajat_s'+z+']').prop('readonly', false);
			$('input[name=koord_derajat_s'+z+']').val(null).prop('disabled', false);
			$('input[name=koord_jam_s'+z+']').prop('readonly', false);
			$('input[name=koord_jam_s'+z+']').val(null).prop('disabled', false);
			$('input[name=koord_menit_s'+z+']').prop('readonly', false);
			$('input[name=koord_menit_s'+z+']').val(null).prop('disabled', false);
			$('input[name=koord_derajat_e'+z+']').prop('readonly', false);
			$('input[name=koord_derajat_e'+z+']').val(null).prop('disabled', false);
			$('input[name=koord_jam_e'+z+']').prop('readonly', false);
			$('input[name=koord_jam_e'+z+']').val(null).prop('disabled', false);
			$('input[name=koord_menit_e'+z+']').prop('readonly', false);
			$('input[name=koord_menit_e'+z+']').val(null).prop('disabled', false);
			$('input[name=tb_ukuran'+z+']').val(null).prop('disabled', false);
			$('select[name=tb_ppn_nama_koord'+z+']').prop('disabled', false);
			$('select[name=tb_simbol_label'+z+']').prop('disabled', false);
			$('select[name=tb_saluran_cecer'+z+']').prop('disabled', false);
			$('select[name=tb_bak_cecer'+z+']').prop('disabled', false);
			$('select[name=tb_kemiringan'+z+']').prop('disabled', false);
			$('select[name=tb_sop_darurat'+z+']').prop('disabled', false);
			$('select[name=tb_log_book'+z+']').prop('disabled', false);
			$('select[name=tb_apar_p3k'+z+']').prop('disabled', false);
			$('select[name=tb_p3k'+z+']').prop('disabled', false);
		}

		$('input[name=izin_no]').val(null).prop('readonly', false);
		$('input[name=izin_tgl]').val(null).prop('readonly', false);
		$('input[name=limb_sumber]').val(null).prop('readonly', false);
		$('input[name=bdn_terima]').val(null).prop('readonly', false);
		$('input[name=limbah_sumber]').val(null).prop('readonly', false);
		$('input[name=limbah_penerima]').val(null).prop('readonly', false);
		$('input[name=koord_outlet_derajat_s]').val(null).prop('readonly', false);
		$('input[name=koord_outlet_jam_s]').val(null).prop('readonly', false);
		$('input[name=koord_outlet_menit_s]').val(null).prop('readonly', false);
		$('input[name=koord_outlet_derajat_e]').val(null).prop('readonly', false);
		$('input[name=koord_outlet_jam_e]').val(null).prop('readonly', false);
		$('input[name=koord_outlet_menit_e]').val(null).prop('readonly', false);
		$('input[name=xkoord_outlet_derajat_s]').val(null).prop('readonly', false);
		$('input[name=xkoord_outlet_jam_s]').val(null).prop('readonly', false);
		$('input[name=xkoord_outlet_menit_s]').val(null).prop('readonly', false);
		$('input[name=xkoord_outlet_derajat_e]').val(null).prop('readonly', false);
		$('input[name=xkoord_outlet_jam_e]').val(null).prop('readonly', false);
		$('input[name=xkoord_outlet_menit_e]').val(null).prop('readonly', false);
        		          	
      	$(target).slideUp();

      	$.ajax({
            'type' : 'POST',
            'url' : base_url + 'backend/industri/ajax_get_id',
            'data' : {'id_industri' : id},
            'dataType' : 'json',
            'success' : function(data){ 
            	var container = $(target);
            	var t = '';
                
                if(data.list != null){
                	d = {}, no = 0;
                    for (var i = 0; i < data.list.length; i++) {
                        d = data.list[i];
                        // console.log(d.jenis_limbah);

	                    t += '<div class="panel panel-default">';
        				t += '<div class="panel-body">';
            			t += '<div>';
                		t += '<label>Alamat           : </label>'+d.alamat+'<br>';
                		t += '<label>Desa / Kelurahan : </label>'+d.kelurahan+'<br>';
                		t += '<label>Kecamatan        : </label>'+d.kecamatan+'<br>';
                		t += '<label>Telp             : </label>'+d.tlp+'<br>';
                		t += '<label>Fax              : </label>'+d.fax+'<br>';
                		t += '</div>';
        				t += '</div>';
    					t += '</div>';
	                }
	            } else {
	            		t += '<div class="panel panel-default">';
						t += '<div class="panel-body bg-danger">';
						t += '<div ><label>Data Usaha / Kegiatan tidak ditemukan</label></div>';
							t += '</div>';
							t += '</div>';
	            }

	            container.html(t);
				var jml_tps = data.jml_tps; 
				console.log(jml_tps);
				$(target).slideDown().delay(800);

				if(data.detail_air_limbah != null){
					for(var s=0; s< data.detail_air_limbah.length; s++){
						d = data.detail_air_limbah[s];
						$('input[name=izin_no]').val(d.izin_limbah).prop('readonly', false);
						$('input[name=izin_tgl]').val(d.tgl_perizinan).prop('readonly', false);
						$('select[id=limb_sumber]').val(d.sumber_limbah).multiselect("refresh");
						$('select[name=bdn_terima]').val(d.penerima_limbah);
						$('select[name=limbah_sumber]').val(d.sumber_limbah);
						$('select[name=limbah_penerima]').val(d.penerima_limbah);
						$('input[name=izin_debit]').val(d.debit_perhari).prop('readonly', false);
						$('input[name=koord_outlet_derajat_s]').val(d.derajat_s).prop('readonly', false);
						$('input[name=koord_outlet_jam_s]').val(d.jam_s).prop('readonly', false);
						$('input[name=koord_outlet_menit_s]').val(d.menit_s).prop('readonly', false);
						$('input[name=koord_outlet_derajat_e]').val(d.derajat_e).prop('readonly', false);
						$('input[name=koord_outlet_jam_e]').val(d.jam_e).prop('readonly', false);
						$('input[name=koord_outlet_menit_e]').val(d.menit_e).prop('readonly', false);
						$('input[name=xkoord_outlet_derajat_s]').val(d.derajat_s).prop('readonly', false);
						$('input[name=xkoord_outlet_jam_s]').val(d.jam_s).prop('readonly', false);
						$('input[name=xkoord_outlet_menit_s]').val(d.menit_s).prop('readonly', false);
						$('input[name=xkoord_outlet_derajat_e]').val(d.derajat_e).prop('readonly', false);
						$('input[name=xkoord_outlet_jam_e]').val(d.jam_e).prop('readonly', false);
						$('input[name=xkoord_outlet_menit_e]').val(d.menit_e).prop('readonly', false);
					}
				}

				if(data.detail_tps != null) {

					if(jml_tps == 1){
						for(var x=2; x<5; x++){
							$('select[name=tb_izin'+x+']').val('3').prop('disabled', false);
							$('input[name=tb_izin_no'+x+']').val('');
							$('input[name=tb_izin_no'+x+']').prop('disabled', true);
							$('input[name=tb_izin_tgl'+x+']').val('');
							$('input[name=tb_izin_tgl'+x+']').prop('disabled', true);
							$('input[name=tb_jenis'+x+']').val('').prop('disabled', true);
							$('input[name=koord_derajat_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_jam_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_menit_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_derajat_e'+x+']').val('').prop('disabled', true);
							$('input[name=koord_jam_e'+x+']').val('').prop('disabled', true);
							$('input[name=koord_menit_e'+x+']').val('').prop('disabled', true);
							$('input[name=tb_ukuran'+x+']').val('').prop('disabled', true);
							$('select[name=tb_ppn_nama_koord'+x+']').val('').prop('disabled', true);
							$('select[name=tb_simbol_label'+x+']').val('').prop('disabled', true);
							$('select[name=tb_saluran_cecer'+x+']').val('').prop('disabled', true);
							$('select[name=tb_bak_cecer'+x+']').val('').prop('disabled', true);
							$('select[name=tb_kemiringan'+x+']').val('').prop('disabled', true);
							$('select[name=tb_sop_darurat'+x+']').val('').prop('disabled', true);
							$('select[name=tb_log_book'+x+']').val('').prop('disabled', true);
							$('select[name=tb_apar_p3k'+x+']').val('').prop('disabled', true);
							$('select[name=tb_p3k'+x+']').val('').prop('disabled', true);
						}

						var y = 1;
						for(var s=0; s< data.detail_tps.length; s++){
							d = data.detail_tps[s];
							$('select[name=tb_izin'+y+']').val('Ada');
							$('input[name=tb_izin_no'+y+']').val(d.izin_tps).prop('readonly', false);
							$('input[name=tb_izin_tgl'+y+']').val(d.tgl_perizinan).prop('readonly', false);
							$('input[name=tb_jenis'+y+']').val(d.jenis_limbah).prop('readonly', false);
							$('input[name=koord_derajat_s'+y+']').val(d.derajat_s).prop('readonly', false);
							$('input[name=koord_jam_s'+y+']').val(d.jam_s).prop('readonly', false);
							$('input[name=koord_menit_s'+y+']').val(d.menit_s).prop('readonly', false);
							$('input[name=koord_derajat_e'+y+']').val(d.derajat_e).prop('readonly', false);
							$('input[name=koord_jam_e'+y+']').val(d.jam_e).prop('readonly', false);
							$('input[name=koord_menit_e'+y+']').val(d.menit_e).prop('readonly', false);
							$('input[name=tb_ukuran'+y+']').val(d.ukuran);
							y+=1;
						}
					} else if (jml_tps == 2){

						for(var x=3; x<5; x++){
							$('select[name=tb_izin'+x+']').val('3').prop('disabled', false);
							$('input[name=tb_izin_no'+x+']').val('');
							$('input[name=tb_izin_no'+x+']').prop('disabled', true);
							$('input[name=tb_izin_tgl'+x+']').val('');
							$('input[name=tb_izin_tgl'+x+']').prop('disabled', true);
							$('input[name=tb_jenis'+x+']').val('').prop('disabled', true);
							$('input[name=koord_derajat_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_jam_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_menit_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_derajat_e'+x+']').val('').prop('disabled', true);
							$('input[name=koord_jam_e'+x+']').val('').prop('disabled', true);
							$('input[name=koord_menit_e'+x+']').val('').prop('disabled', true);
							$('input[name=tb_ukuran'+x+']').val('').prop('disabled', true);
							$('select[name=tb_ppn_nama_koord'+x+']').val('').prop('disabled', true);
							$('select[name=tb_simbol_label'+x+']').val('').prop('disabled', true);
							$('select[name=tb_saluran_cecer'+x+']').val('').prop('disabled', true);
							$('select[name=tb_bak_cecer'+x+']').val('').prop('disabled', true);
							$('select[name=tb_kemiringan'+x+']').val('').prop('disabled', true);
							$('select[name=tb_sop_darurat'+x+']').val('').prop('disabled', true);
							$('select[name=tb_log_book'+x+']').val('').prop('disabled', true);
							$('select[name=tb_apar_p3k'+x+']').val('').prop('disabled', true);
							$('select[name=tb_p3k'+x+']').val('').prop('disabled', true);
						}

						var y = 1;
						for(var s=0; s< data.detail_tps.length; s++){
							d = data.detail_tps[s];
							$('select[name=tb_izin'+y+']').val('Ada');
							$('input[name=tb_izin_no'+y+']').val(d.izin_tps).prop('readonly', false);
							$('input[name=tb_izin_tgl'+y+']').val(d.tgl_perizinan).prop('readonly', false);
							$('input[name=tb_jenis'+y+']').val(d.jenis_limbah).prop('readonly', false);
							$('input[name=koord_derajat_s'+y+']').val(d.derajat_s).prop('readonly', false);
							$('input[name=koord_jam_s'+y+']').val(d.jam_s).prop('readonly', false);
							$('input[name=koord_menit_s'+y+']').val(d.menit_s).prop('readonly', false);
							$('input[name=koord_derajat_e'+y+']').val(d.derajat_e).prop('readonly', false);
							$('input[name=koord_jam_e'+y+']').val(d.jam_e).prop('readonly', false);
							$('input[name=koord_menit_e'+y+']').val(d.menit_e).prop('readonly', false);
							$('input[name=tb_ukuran'+y+']').val(d.ukuran);
							y+=1;
						}
					} else if (jml_tps == 3){
						for(var x=4; x<5; x++){
							$('select[name=tb_izin'+x+']').val('3').prop('disabled', false);
							$('input[name=tb_izin_no'+x+']').val('');
							$('input[name=tb_izin_no'+x+']').prop('disabled', true);
							$('input[name=tb_izin_tgl'+x+']').val('');
							$('input[name=tb_izin_tgl'+x+']').prop('disabled', true);
							$('input[name=tb_jenis'+x+']').val('').prop('disabled', true);
							$('input[name=koord_derajat_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_jam_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_menit_s'+x+']').val('').prop('disabled', true);
							$('input[name=koord_derajat_e'+x+']').val('').prop('disabled', true);
							$('input[name=koord_jam_e'+x+']').val('').prop('disabled', true);
							$('input[name=koord_menit_e'+x+']').val('').prop('disabled', true);
							$('input[name=tb_ukuran'+x+']').val('').prop('disabled', true);
							$('select[name=tb_ppn_nama_koord'+x+']').val('').prop('disabled', true);
							$('select[name=tb_simbol_label'+x+']').val('').prop('disabled', true);
							$('select[name=tb_saluran_cecer'+x+']').val('').prop('disabled', true);
							$('select[name=tb_bak_cecer'+x+']').val('').prop('disabled', true);
							$('select[name=tb_kemiringan'+x+']').val('').prop('disabled', true);
							$('select[name=tb_sop_darurat'+x+']').val('').prop('disabled', true);
							$('select[name=tb_log_book'+x+']').val('').prop('disabled', true);
							$('select[name=tb_apar_p3k'+x+']').val('').prop('disabled', true);
							$('select[name=tb_p3k'+x+']').val('').prop('disabled', true);
						}

						var y = 1;
						for(var s=0; s< data.detail_tps.length; s++){
							d = data.detail_tps[s];
							$('select[name=tb_izin'+y+']').val('Ada');
							$('input[name=tb_izin_no'+y+']').val(d.izin_tps).prop('readonly', false);
							$('input[name=tb_izin_tgl'+y+']').val(d.tgl_perizinan).prop('readonly', false);
							$('input[name=tb_jenis'+y+']').val(d.jenis_limbah).prop('readonly', false);
							$('input[name=koord_derajat_s'+y+']').val(d.derajat_s).prop('readonly', false);
							$('input[name=koord_jam_s'+y+']').val(d.jam_s).prop('readonly', false);
							$('input[name=koord_menit_s'+y+']').val(d.menit_s).prop('readonly', false);
							$('input[name=koord_derajat_e'+y+']').val(d.derajat_e).prop('readonly', false);
							$('input[name=koord_jam_e'+y+']').val(d.jam_e).prop('readonly', false);
							$('input[name=koord_menit_e'+y+']').val(d.menit_e).prop('readonly', false);
							$('input[name=tb_ukuran'+y+']').val(d.ukuran);
							y+=1;
						}
					} else if (jml_tps == 4){

						var y = 1;
						for(var s=0; s< data.detail_tps.length; s++){
							d = data.detail_tps[s];
							$('select[name=tb_izin'+y+']').val('Ada');
							$('input[name=tb_izin_no'+y+']').val(d.izin_tps).prop('readonly', false);
							$('input[name=tb_izin_tgl'+y+']').val(d.tgl_perizinan).prop('readonly', false);
							$('input[name=tb_jenis'+y+']').val(d.jenis_limbah).prop('readonly', false);
							$('input[name=koord_derajat_s'+y+']').val(d.derajat_s).prop('readonly', false);
							$('input[name=koord_jam_s'+y+']').val(d.jam_s).prop('readonly', false);
							$('input[name=koord_menit_s'+y+']').val(d.menit_s).prop('readonly', false);
							$('input[name=koord_derajat_e'+y+']').val(d.derajat_e).prop('readonly', false);
							$('input[name=koord_jam_e'+y+']').val(d.jam_e).prop('readonly', false);
							$('input[name=koord_menit_e'+y+']').val(d.menit_e).prop('readonly', false);
							$('input[name=tb_ukuran'+y+']').val(d.ukuran);
							y+=1;
						}
					}
				} else {
					for(var z=1; z<5; z++){
    					$('select[name=tb_izin'+z+']').val('3');
						$('input[name=tb_izin_no'+z+']').prop('disabled', true);
						$('input[name=tb_izin_tgl'+z+']').prop('disabled', true);
						$('input[name=tb_jenis'+z+']').prop('disabled', true);
						$('input[name=koord_derajat_s'+z+']').prop('disabled', true);
						$('input[name=koord_jam_s'+z+']').prop('disabled', true);
						$('input[name=koord_menit_s'+z+']').prop('disabled', true);
						$('input[name=koord_derajat_e'+z+']').prop('disabled', true);
						$('input[name=koord_jam_e'+z+']').prop('disabled', true);
						$('input[name=koord_menit_e'+z+']').prop('disabled', true);
						$('input[name=tb_ukuran'+z+']').prop('disabled', true);
						$('select[name=tb_ppn_nama_koord'+z+']').prop('disabled', true);
						$('select[name=tb_simbol_label'+z+']').prop('disabled', true);
						$('select[name=tb_saluran_cecer'+z+']').prop('disabled', true);
						$('select[name=tb_bak_cecer'+z+']').prop('disabled', true);
						$('select[name=tb_kemiringan'+z+']').prop('disabled', true);
						$('select[name=tb_sop_darurat'+z+']').prop('disabled', true);
						$('select[name=tb_log_book'+z+']').prop('disabled', true);
						$('select[name=tb_apar_p3k'+z+']').prop('disabled', true);
						$('select[name=tb_p3k'+z+']').prop('disabled', true);
					}
				}
            }
        });
    }

    $('.izin_tps').change(function() {
    	// alert(message);
        var num = $(this).prop('name').match(/\d+$/)[0];
       // console.log(num);
        if(($(this).val()  == 'Ada') || ($(this).val() == 1)) {
        	$('input[name=tb_izin_no'+num+']').val('');
            $('input[name=tb_izin_no'+num+']').prop('disabled', false);
            $('input[name=tb_izin_tgl'+num+']').val('');
            $('input[name=tb_izin_tgl'+num+']').prop('disabled', false);
            $('input[name=tb_jenis'+num+']').val('').prop('disabled', false);
            $('input[name=koord_derajat_s'+num+']').val('').prop('disabled', false);
            $('input[name=koord_jam_s'+num+']').val('').prop('disabled', false);
            $('input[name=koord_menit_s'+num+']').val('').prop('disabled', false);
            $('input[name=koord_derajat_e'+num+']').val('').prop('disabled', false);
            $('input[name=koord_jam_e'+num+']').val('').prop('disabled', false);
            $('input[name=koord_menit_e'+num+']').val('').prop('disabled', false);
            $('input[name=tb_ukuran'+num+']').val('').prop('disabled', false);
            $('select[name=tb_ppn_nama_koord'+num+']').val('').prop('disabled', false);
            $('select[name=tb_simbol_label'+num+']').val('').prop('disabled', false);
            $('select[name=tb_saluran_cecer'+num+']').val('').prop('disabled', false);
            $('select[name=tb_bak_cecer'+num+']').val('').prop('disabled', false);
            $('select[name=tb_kemiringan'+num+']').val('').prop('disabled', false);
            $('select[name=tb_sop_darurat'+num+']').val('').prop('disabled', false);
            $('select[name=tb_log_book'+num+']').val('').prop('disabled', false);
            $('select[name=tb_apar_p3k'+num+']').val('').prop('disabled', false);
            $('select[name=tb_p3k'+num+']').val('').prop('disabled', false);
        } else if(($(this).val()  == 'Tidak Ada') || ($(this).val() == 0)) {
        	$('input[name=tb_izin_no'+num+']').val('');
            $('input[name=tb_izin_no'+num+']').prop('disabled', true);
            $('input[name=tb_izin_tgl'+num+']').val('');
            $('input[name=tb_izin_tgl'+num+']').prop('disabled', true);
            $('input[name=tb_jenis'+num+']').val('').prop('disabled', false);
            $('input[name=koord_derajat_s'+num+']').val('').prop('disabled', false);
            $('input[name=koord_jam_s'+num+']').val('').prop('disabled', false);
            $('input[name=koord_menit_s'+num+']').val('').prop('disabled', false);
            $('input[name=koord_derajat_e'+num+']').val('').prop('disabled', false);
            $('input[name=koord_jam_e'+num+']').val('').prop('disabled', false);
            $('input[name=koord_menit_e'+num+']').val('').prop('disabled', false);
            $('input[name=tb_ukuran'+num+']').val('').prop('disabled', false);
            $('select[name=tb_ppn_nama_koord'+num+']').val('').prop('disabled', false);
            $('select[name=tb_simbol_label'+num+']').val('').prop('disabled', false);
            $('select[name=tb_saluran_cecer'+num+']').val('').prop('disabled', false);
            $('select[name=tb_bak_cecer'+num+']').val('').prop('disabled', false);
            $('select[name=tb_kemiringan'+num+']').val('').prop('disabled', false);
            $('select[name=tb_sop_darurat'+num+']').val('').prop('disabled', false);
            $('select[name=tb_log_book'+num+']').val('').prop('disabled', false);
            $('select[name=tb_apar_p3k'+num+']').val('').prop('disabled', false);
            $('select[name=tb_p3k'+num+']').val('').prop('disabled', false);
        } else {
        	$('input[name=tb_izin_no'+num+']').val('').prop('disabled', true);
            $('input[name=tb_izin_tgl'+num+']').val('').prop('disabled', true);
            $('input[name=tb_jenis'+num+']').val('').prop('disabled', true);
            $('input[name=koord_derajat_s'+num+']').val('').prop('disabled', true);
            $('input[name=koord_jam_s'+num+']').val('').prop('disabled', true);
            $('input[name=koord_menit_s'+num+']').val('').prop('disabled', true);
            $('input[name=koord_derajat_e'+num+']').val('').prop('disabled', true);
            $('input[name=koord_jam_e'+num+']').val('').prop('disabled', true);
            $('input[name=koord_menit_e'+num+']').val('').prop('disabled', true);
            $('input[name=tb_ukuran'+num+']').val('').prop('disabled', true);
            $('select[name=tb_ppn_nama_koord'+num+']').val('').prop('disabled', true);
            $('select[name=tb_simbol_label'+num+']').val('').prop('disabled', true);
            $('select[name=tb_saluran_cecer'+num+']').val('').prop('disabled', true);
            $('select[name=tb_bak_cecer'+num+']').val('').prop('disabled', true);
            $('select[name=tb_kemiringan'+num+']').val('').prop('disabled', true);
            $('select[name=tb_sop_darurat'+num+']').val('').prop('disabled', true);
            $('select[name=tb_log_book'+num+']').val('').prop('disabled', true);
            $('select[name=tb_apar_p3k'+num+']').val('').prop('disabled', true);
            $('select[name=tb_p3k'+num+']').val('').prop('disabled', true);	
        }
    });

});
</script>
<script type="text/javascript">
// below is the old ajax request, above is the implemented one, just in case anyone need
// this one is used to call another file to embbed to view, the file is v_alamat_industri.php
// $.ajax({
//     'type' : 'POST',
//     'url' : base_url + 'backend/' + controller + '/ajax_get_id',
//     'data' : {'id_industri' : id},
//     // 'dataType' : 'json',
//     'success' : function(data){ 
//     	var container = $(target);
//         if(data){
//         	container.html(data);
//         	// $('#tb_izin').val('d.no_izin');
//         	if(data.list != null){
//         		d = {}, no = 0;
//                 for (var i = 0; i < data.list.length; i++) {
//                     d = data.list[i];
//                     console.log(d.izin_tps);
//                     $('#tb_izin').val(d.izin_tps);
//                 }
//             }
//         	$(target).slideDown().delay(800);
//         }
//     }
// });
</script>