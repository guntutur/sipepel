<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_business_activity extends CI_Model {

	private $table_name			= 'usaha_kegiatan';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();
		
		$this->db->select('p.*, (select count(*) from jenis_industri dd where dd.id_usaha_kegiatan = p.id_usaha_kegiatan) as jumlah, (select count(*) from industri d where d.id_usaha_kegiatan = p.id_usaha_kegiatan) as jumlah_industri')->from($this->table_name .' as p');
									
		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[1]));
				else $this->db->like($key, $this->db->escape_like_str($val[2]));
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[1]));
				else $this->db->where($key, $this->db->escape_like_str($val[2]));
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("p.ket", "asc"); 
		$this->db->order_by("p.tgl_pembuatan", "desc"); 

		$query = $this->db->get();

		// $query = $this->db->query("select * from jenis_industri u order by u.tgl_pembuatan desc");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		$this->db->select('p.*, (select count(*) from jenis_industri dd where dd.id_usaha_kegiatan = p.id_usaha_kegiatan) as jumlah, (select count(*) from industri d where d.id_usaha_kegiatan = p.id_usaha_kegiatan) as jumlah_industri')->from($this->table_name .' as p');


		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[1]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[1]));
				else $this->db->where($key, $val[2]);
			}
		}

		return $this->db->count_all_results();
	}

	function get_dropdown() {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("ket", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_business_activity_by_id($id) {
		$this->db->where('id_usaha_kegiatan', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_business_activity($id) {
		$data = $this->get_business_activity_by_id($id);
		$this->db->where('id_usaha_kegiatan', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_business_activity($data, $id) {

		$this->db->where("id_usaha_kegiatan", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_business_activity($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate($name) {
		$this->db->where('ket', $name);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
}
?>