<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document   : bap_agro.php 
 // Created on : October x, 2014 xx:xx 
 // Author     : huda.jtk09@gmail.com 
 // Description: Controller for BAP Agro 

class bap_agro extends CI_Controller {

	private $day_id = array('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu');

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_bap_agro', 'agrDao', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);
        $this->load->model('m_employee', 'empDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);

		$this->load->library('PHPExcel');
    }

    # default view

    public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all agro
        $bap_agro = $this->agrDao->get_all();
		
		# get all industri
		$industri = $this->indDao->get_all_industri();

		# get all pegawai
		$pegawai = $this->empDao->get_all_employee();

		# response paramter
		$data['day_id'] = $this->day_id;
        $data['bap_agro'] = $bap_agro;
        $data['industri'] = $industri;
        $data['pegawai'] = $pegawai;

        # parameter for view
        $data['tab_list'] = 'active';
		$data['tab_view'] = '';
		$data['tab_edit'] = '';    
		
		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_agro', $data);
    }
	
	
	public function insert() {
		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();
		$filename = "bapagro_".date("Ymd_his");
		$upload_loc = "../assets/tmp/";

		# upload BAP Form
		if (isset($_FILES['file']['name']) and ($_FILES['file']['name'] != '')) {
			$this->load->library('upload');
			$this->upload->initialize(array(
				"file_name" => $filename,
				"allowed_types" => "xlsx|XLSX",
	            "upload_path"   => realpath(APPPATH . $upload_loc),
	            "is_image" => false,
	            "encrypt_name" => false
	    	));

	    	if($this->upload->do_multi_upload("file")){
			} else {
				echo "error : ". $this->upload->display_errors();
			}
		}

		# Excel Reader (read BAP Form)
		$input_path = realpath(APPPATH . $upload_loc)."/".$filename.".xlsx";
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($input_path);
		
		# array for table bap
		$bap = array(
			'id_industri' => $this->input->post('id_industri'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			// 'url' => $this->input->post(''),
			'bap_hari' => $this->input->post('bap_hari'),
			'bap_tgl' => $this->input->post('bap_tgl'),
			'bap_jam' => $this->input->post('bap_jam'),
			'p2_nama' => $this->input->post('p2_nama'),
			'p2_jabatan' => $this->input->post('p2_jabatan'),
			'p2_telp' => $this->input->post('p2_telp'),
			'dok_lingk' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C14')->getValue()),
			'dok_lingk_jenis' => $objPHPExcel->getActiveSheet()->getCell('C15')->getValue(),
			'dok_lingk_tahun' => $objPHPExcel->getActiveSheet()->getCell('E14')->getValue(),
			'izin_lingk' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C16')->getValue()),
			'izin_lingk_tahun' => $objPHPExcel->getActiveSheet()->getCell('E16')->getValue(),
			'tgl_pembuatan' => $datenow,
			'dibuat_oleh' => $user,
			'catatan' => $objPHPExcel->getActiveSheet()->getCell('B167')->getValue()
			);

		# insert bap
		$id_bap = $this->bapDao->insert_getID('bap',$bap);

			# array for table petugas_bap
			$datenow = date('Y-m-d');
			$petugas1 = $this->input->post('id_petugas_1');
			$petugas2 = $this->input->post('id_petugas_2');
			$petugas3 = $this->input->post('id_petugas_3');


			if ($petugas1 != NULL) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $petugas1,
					// 'ket'
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);
	
				#insert petugas_bap
				$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);

				if ($petugas2 != NULL) {
				$petugas_bap = array(
					'id_bap' => $id_bap,
					'id_pegawai' => $petugas2,
					// 'ket'
					'tgl_pembuatan' => $datenow,
					'dibuat_oleh' => $user
					);
	
					#insert petugas_bap
					$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);

					if ($petugas3 != NULL) {
						$petugas_bap = array(
						'id_bap' => $id_bap,
						'id_pegawai' => $petugas3,
						// 'ket'
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user
						);
	
						#insert petugas_bap
						$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
					}
				}
			}
		
		# array for table bap_agro
		$bap_agro = array(
			'id_bap' => $id_bap,
			'status_perusahaan' => $objPHPExcel->getActiveSheet()->getCell('C9')->getValue(),
			'kapasitas_produksi' => $objPHPExcel->getActiveSheet()->getCell('C10')->getValue(),
			'jenis_produk' => $objPHPExcel->getActiveSheet()->getCell('C11')->getValue(),
			'pakai_pupuk' => $objPHPExcel->getActiveSheet()->getCell('C12')->getValue(),
			'jumlah_karyawan' => $objPHPExcel->getActiveSheet()->getCell('C13')->getValue()
			);

		#insert bap_agro
		$this->bapDao->insert_getStat('bap_agro', $bap_agro);

		# array for pencemaran_air
		$pencemaran_air = array(
			'id_bap' => $id_bap,
			'ambil_air_tanah' => $objPHPExcel->getActiveSheet()->getCell('C21')->getValue(),
			'ambil_air_tanah_izin' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C22')->getValue()),
			'ambil_air_permukaan' => $objPHPExcel->getActiveSheet()->getCell('C23')->getValue(),
			'ambil_air_permukaan_izin' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C24')->getValue()),
			'ambil_air_pdam' => $objPHPExcel->getActiveSheet()->getCell('C25')->getValue(),
			'ambil_air_lain' => $objPHPExcel->getActiveSheet()->getCell('C26')->getValue(),
			'limb_sumber' => $objPHPExcel->getActiveSheet()->getCell('C27')->getValue(),
			'bdn_terima' => $objPHPExcel->getActiveSheet()->getCell('C28')->getValue(),
			'ipal' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C29')->getValue()),
			'ipal_sistem' => $objPHPExcel->getActiveSheet()->getCell('C30')->getValue(),
			'ipal_unit' => $objPHPExcel->getActiveSheet()->getCell('C31')->getValue(),
			'ipal_kapasitas' => $objPHPExcel->getActiveSheet()->getCell('C32')->getValue(),
			'izin' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C33')->getValue()),
			'izin_no' => $objPHPExcel->getActiveSheet()->getCell('C34')->getValue(),
			'izin_tgl' => $this->dateConv($objPHPExcel->getActiveSheet()->getCell('E34')->getValue()),
			'izin_debit' => $objPHPExcel->getActiveSheet()->getCell('C35')->getValue(),
			'koord_outlet_s' => $objPHPExcel->getActiveSheet()->getCell('D36')->getValue(),
			'koord_outlet_e' => $objPHPExcel->getActiveSheet()->getCell('D37')->getValue(),
			'au' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C38')->getValue()),
			'au_jenis' => $objPHPExcel->getActiveSheet()->getCell('C39')->getValue(),
			'au_ukuran' => $objPHPExcel->getActiveSheet()->getCell('C40')->getValue(),
			'au_kondisi' => $objPHPExcel->getActiveSheet()->getCell('C41')->getValue(),
			'cat_deb_hari' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C42')->getValue()),
			'daur_ulang' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C43')->getValue()),
			'daur_ulang_debit' => $objPHPExcel->getActiveSheet()->getCell('C44')->getValue(),
			'uji_limbah' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C45')->getValue()),
			'uji_period' => $objPHPExcel->getActiveSheet()->getCell('C46')->getValue(),
			'uji_lab' => $objPHPExcel->getActiveSheet()->getCell('C47')->getValue(),
			'uji_hasil' => $objPHPExcel->getActiveSheet()->getCell('C48')->getValue(),
			'uji_tidak_bulan' => $objPHPExcel->getActiveSheet()->getCell('C49')->getValue(),
			'uji_tidak_param' => $objPHPExcel->getActiveSheet()->getCell('C50')->getValue(),
			'uji_lapor' => $objPHPExcel->getActiveSheet()->getCell('C51')->getValue(),
			'bocor' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C59')->getValue()),
			'bocor_lokasi' => $objPHPExcel->getActiveSheet()->getCell('C60')->getValue(),
			'lain_lain' => $objPHPExcel->getActiveSheet()->getCell('B62')->getValue()
			);

		#insert pencemaran_air
		$id_penc_air = $this->bapDao->insert_getID('pencemaran_air', $pencemaran_air);
		
		# collecting data for bahan_kimia_ipal
		for ($i=54; $i < 58; $i++) { 
			$bahan_kimia_ipal = array(
	 			'id_penc_air' => $id_penc_air,
	 			'nama' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue(),
	 			'jml_pemakaian' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue()
	 			);

			#insert bahan_kimia_ipal
			$this->bapDao->insert_getStat('bahan_kimia_ipal', $bahan_kimia_ipal);
		}

		# array for pencemaran_udara
		$pencemaran_udara = array(
			'id_bap' => $id_bap,
			'sumber_emisi' => $objPHPExcel->getActiveSheet()->getCell('C66')->getValue(),
			'jml_cerobong' => $objPHPExcel->getActiveSheet()->getCell('C76')->getValue(),
			'pelaporan_ua_ue' => $objPHPExcel->getActiveSheet()->getCell('C101')->getValue(),
			'lain_lain' => $objPHPExcel->getActiveSheet()->getCell('B103')->getValue()
			);

		#insert pencemaran_udara
		$id_penc_udara = $this->bapDao->insert_getID('pencemaran_udara', $pencemaran_udara);

		# collecting data for emisi_boiler
		$cols = array('C', 'D', 'E');

		foreach ($cols as $col) {
			$emisi_boiler = array(
				'id_penc_udara' => $id_penc_udara,
				'id_jenis_emisi_boiler' => $objPHPExcel->getActiveSheet()->getCell($col.'74')->getValue(),
				'jml_boiler' => $objPHPExcel->getActiveSheet()->getCell($col.'68')->getValue(),
				'merk' => $objPHPExcel->getActiveSheet()->getCell($col.'69')->getValue(),
				'type' => $objPHPExcel->getActiveSheet()->getCell($col.'70')->getValue(),
				'kapasitas' => $objPHPExcel->getActiveSheet()->getCell($col.'71')->getValue(),
				'bahan_bakar' => $objPHPExcel->getActiveSheet()->getCell($col.'72')->getValue(),
				'jml_bahan_bakar' => $objPHPExcel->getActiveSheet()->getCell($col.'73')->getValue()
				);
			
			#insert emisi_boiler
			$this->bapDao->insert_getStat('emisi_boiler', $emisi_boiler);
		}

		#assign jml_cerobong
		$jml_cerobong = $objPHPExcel->getActiveSheet()->getCell('C76')->getValue();
		if ($jml_cerobong == 1){
			$cols = array('C');
		} else if ($jml_cerobong == 2){
			$cols = array('C', 'D');
		} else if ($jml_cerobong == 3){
			$cols = array('C', 'D', 'E');
		} else if ($jml_cerobong == 4){
			$cols = array('C', 'D', 'E', 'F');
		} else {
			die();
		} 

		# collecting data for data_cerobong

		foreach ($cols as $col) {
			$data_cerobong = array(
				'id_penc_udara' => $id_penc_udara,
				'h_cerobong' => $objPHPExcel->getActiveSheet()->getCell($col.'78')->getValue(),
				'd_cerobong' => $objPHPExcel->getActiveSheet()->getCell($col.'79')->getValue(),
				'sampling_hole' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'80')->getValue()),
				'tangga' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'81')->getValue()),
				'pengaman_tangga' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'82')->getValue()),
				'lantai' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'83')->getValue())
				);
			
			#insert data_cerobong
			$this->bapDao->insert_getStat('data_cerobong', $data_cerobong);
		}

		#assign numer of column filled to insert from excel to table uji_emisi
		$jml_cerobong = $objPHPExcel->getActiveSheet()->getCell('C76')->getValue();
		if ($jml_cerobong == 1){
			$cols = array('C');
		} else if ($jml_cerobong == 2){
			$cols = array('C', 'D');
		} else if ($jml_cerobong == 3){
			$cols = array('C', 'D', 'E');
		} else if ($jml_cerobong == 4){
			$cols = array('C', 'D', 'E', 'F');
		} else {
			die();
		}

		# collecting data for uji_emisi

		foreach ($cols as $col) {
			$uji_emisi = array(
				'id_penc_udara' => $id_penc_udara,
				'uji_kualitas' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'84')->getValue()),
				'period' => $objPHPExcel->getActiveSheet()->getCell($col.'85')->getValue(),
				'lab' => $objPHPExcel->getActiveSheet()->getCell($col.'86')->getValue(),
				'bme_pemenuhan' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'87')->getValue()),
				'bme_param' => $objPHPExcel->getActiveSheet()->getCell($col.'88')->getValue(),
				'bme_period' => $objPHPExcel->getActiveSheet()->getCell($col.'89')->getValue(),
				'pengendali_emisi' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'90')->getValue()),
				'jenis' => $objPHPExcel->getActiveSheet()->getCell($col.'91')->getValue()
				);
			
			#insert uji_emisi
			$this->bapDao->insert_getStat('uji_emisi', $uji_emisi);
		}
		
		# collecting data for uji_ambien
		$cols = array('C', 'D', 'E');

		foreach ($cols as $col) {
			$uji_ambien = array(
				'id_penc_udara' => $id_penc_udara,
				'lokasi' => $objPHPExcel->getActiveSheet()->getCell($col.'93')->getValue(),
				'uji_kualitas' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'94')->getValue()),
				'period' => $objPHPExcel->getActiveSheet()->getCell($col.'95')->getValue(),
				'lab' => $objPHPExcel->getActiveSheet()->getCell($col.'96')->getValue(),
				'bm_pemenuhan' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'97')->getValue()),
				'bm_param' => $objPHPExcel->getActiveSheet()->getCell($col.'98')->getValue(),
				'bm_period' => $objPHPExcel->getActiveSheet()->getCell($col.'99')->getValue()
				);
			
			#insert uji_ambien
			$this->bapDao->insert_getStat('uji_ambien', $uji_ambien);
		}

		# array for pencemaran_pb3
		$pencemaran_pb3 = array(
			'id_bap' => $id_bap,
			'b3_penyimpanan' => $objPHPExcel->getActiveSheet()->getCell('C120')->getValue(),
			'b3_uji' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C150')->getValue()), 
			'b3_uji_jenis' => $objPHPExcel->getActiveSheet()->getCell('C151')->getValue(), 
			'b3_uji_param' => $objPHPExcel->getActiveSheet()->getCell('C152')->getValue(),
			'b3_uji_lab' => $objPHPExcel->getActiveSheet()->getCell('C153')->getValue(),
			'b3_uji_waktu' => $objPHPExcel->getActiveSheet()->getCell('C154')->getValue(),
			'b3_neraca' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C155')->getValue()),
			'b3_manifest' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C156')->getValue()), 
			'b3_lapor_nm' => $objPHPExcel->getActiveSheet()->getCell('C157')->getValue(),
			'padat_jenis' => $objPHPExcel->getActiveSheet()->getCell('C159')->getValue(), 
			'padat_jumlah' => $objPHPExcel->getActiveSheet()->getCell('C160')->getValue(),
			'padat_kelola' => $objPHPExcel->getActiveSheet()->getCell('C161')->getValue(),
			'lain_lain' => $objPHPExcel->getActiveSheet()->getCell('B164')->getValue()
			);

		#insert pencemaran_pb3
		$id_penc_pb3 = $this->bapDao->insert_getID('pencemaran_pb3', $pencemaran_pb3);

 		# array for jenis_limbah_b3
		for ($i=108; $i < 119; $i++) {
			$jenis_limbah_b3 = array(
				'id_penc_pb3' => $id_penc_pb3,
				'jenis' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue(),
				'jumlah' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue(),
				'pengelolaan' => $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue(),
				'pihak_ke3' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue()
				);

			#insert jenis_limbah_b3
			$this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
		}

		# collecting data for tps_b3
		$cols = array('D', 'E', 'F');

		foreach ($cols as $col) {
			$tps_b3 = array(
				'id_penc_pb3' => $id_penc_pb3,
				'izin' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'123')->getValue()),
				'izin_no' => $objPHPExcel->getActiveSheet()->getCell($col.'124')->getValue(),
				'izin_tgl' => $this->dateConv($objPHPExcel->getActiveSheet()->getCell($col.'125')->getValue()),
				'jenis' => $objPHPExcel->getActiveSheet()->getCell($col.'126')->getValue(),
				'lama_spn_lumpur' => $objPHPExcel->getActiveSheet()->getCell($col.'127')->getValue(),
				'lama_spn_batu_bara' => $objPHPExcel->getActiveSheet()->getCell($col.'128')->getValue(),
				'lama_spn_pelumas' => $objPHPExcel->getActiveSheet()->getCell($col.'129')->getValue(),
				'lama_spn_aki_bekas' => $objPHPExcel->getActiveSheet()->getCell($col.'130')->getValue(),
				'lama_spn_ltl' => $objPHPExcel->getActiveSheet()->getCell($col.'131')->getValue(),
				'lama_spn_kmsn_b3' => $objPHPExcel->getActiveSheet()->getCell($col.'132')->getValue(),
				'lama_spn_majun_b3' => $objPHPExcel->getActiveSheet()->getCell($col.'133')->getValue(),
				'lama_spn_limbah_elektro' => $objPHPExcel->getActiveSheet()->getCell($col.'134')->getValue(),
				'lama_spn_limbah_kimia' => $objPHPExcel->getActiveSheet()->getCell($col.'135')->getValue(),
				'lama_spn_lain_nama' => $objPHPExcel->getActiveSheet()->getCell('C136')->getValue(),
				'lama_spn_lain_ket' => $objPHPExcel->getActiveSheet()->getCell($col.'136')->getValue(),
				'koord_s' => $objPHPExcel->getActiveSheet()->getCell($col.'137')->getValue(),
				'koord_e' => $objPHPExcel->getActiveSheet()->getCell($col.'138')->getValue(),
				'ukuran' => $objPHPExcel->getActiveSheet()->getCell($col.'139')->getValue(),
				'ppn_nama_koord' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'141')->getValue()),
				'simbol_label' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'142')->getValue()),
				'saluran_cecer' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'143')->getValue()), 
				'bak_cecer' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'144')->getValue()),
				'kemiringan' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'145')->getValue()),
				'sop_darurat' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'146')->getValue()),
				'log_book' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'147')->getValue()), 
				'apar_p3k' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'148')->getValue())
				);
			#insert jenis_limbah_b3
			$this->bapDao->insert_getStat('tps_b3', $tps_b3);
		}

		# delete temp file (BAP form)
		unlink($input_path);

		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data successfully Uploaded'));
			
		redirect('backend/bap_agro');
	}

	public function view() {
    	# get id bap from view
		$id_bap = $this->input->post("id_bap");

    	$bap = $this->bapDao->get_obj_by_id("bap", "id_bap", $id_bap);

    	$bap_agro = $this->bapDao->get_obj_by_id("bap_agro", "id_bap", $id_bap);

    	$industri =	$this->bapDao->get_obj_by_id("industri", "id_industri", $bap->id_industri);

    	$pegawai = $this->bapDao->get_obj_by_id("pegawai", "id_pegawai", $bap->id_pegawai);

    	$petugas_bap = $this->bapDao->get_petugas_bap($id_bap);

    	$pencemaran_air = $this->bapDao->get_obj_by_id("pencemaran_air", "id_bap", $id_bap);

    	$bahan_kimia_ipal = $this->bapDao->get_list_by_id("bahan_kimia_ipal", "id_penc_air", $pencemaran_air->id_penc_air);

    	$pencemaran_udara = $this->bapDao->get_obj_by_id("pencemaran_udara", "id_bap", $id_bap);

    	$emisi_boiler = $this->bapDao->get_list_by_id("emisi_boiler", "id_penc_udara", $pencemaran_udara->id_penc_udara);

    	$data_cerobong = $this->bapDao->get_list_by_id("data_cerobong", "id_penc_udara", $pencemaran_udara->id_penc_udara);

    	$uji_emisi = $this->bapDao->get_list_by_id("uji_emisi", "id_penc_udara", $pencemaran_udara->id_penc_udara);

		$uji_ambien = $this->bapDao->get_list_by_id("uji_ambien", "id_penc_udara", $pencemaran_udara->id_penc_udara);

    	$pencemaran_pb3 = $this->bapDao->get_obj_by_id("pencemaran_pb3", "id_bap", $id_bap);

    	$jenis_limbah_b3 = $this->bapDao->get_list_by_id("jenis_limbah_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

    	$tps_b3 = $this->bapDao->get_list_by_id("tps_b3", "id_penc_pb3", $pencemaran_pb3->id_penc_pb3);

    	$data['bap'] = $bap;
    	$data['bap_agro'] = $bap_agro;
    	$data['industri'] = $industri;
    	$data['pegawai'] = $pegawai;
    	$data['petugas_bap'] = $petugas_bap;
    	$data['pencemaran_air'] = $pencemaran_air;
    	$data['bahan_kimia_ipal'] = $bahan_kimia_ipal;
    	$data['pencemaran_udara'] = $pencemaran_udara;
    	$data['emisi_boiler'] = $emisi_boiler;
    	$data['data_cerobong'] = $data_cerobong;
    	$data['uji_emisi'] = $uji_emisi;
    	$data['uji_ambien'] = $uji_ambien;
    	$data['pencemaran_pb3'] = $pencemaran_pb3;
    	$data['jenis_limbah_b3'] = $jenis_limbah_b3;
    	$data['tps_b3'] = $tps_b3;

    	# parameter for view
        $data['tab_list'] = '';
		$data['tab_view'] = 'active';
		$data['tab_edit'] = '';

        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_agro', $data);
	}

	public function edit() {
		# get id bap from view
		$id_bap = $this->input->post('id_bap');
		
    	$bap = $this->bapDao->get_obj_by_id("bap", "id_bap", $id_bap);

    	$petugas_bap = $this->bapDao->get_petugas_bap($id_bap);
    	
		# get all industri
		$industri = $this->indDao->get_all_industri();

		# get all pegawai
		$pegawai = $this->empDao->get_all_employee();

    	$data['day_id'] = $this->day_id;
    	$data['bap'] = $bap;
    	$data['industri'] = $industri;
    	$data['pegawai'] = $pegawai;
    	$data['petugas_bap'] = $petugas_bap;

        # parameter for view
        $data['tab_list'] = '';
		$data['tab_view'] = '';
		$data['tab_edit'] = 'active';

		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_agro_edit', $data);
	}

	public function saveEdit() {
		date_default_timezone_set("Asia/Jakarta");
		$datenow = date("Y-m-d H:i:s:");
		$user = $this->tank_auth->get_personname();
		$filename = "bapindustri_".date("dmyHis");
		$upload_loc = "../assets/tmp/";

		# upload BAP Form
		if (isset($_FILES['file']['name']) and ($_FILES['file']['name'] != '')) {
			$this->load->library('upload');
			$this->upload->initialize(array(
				"file_name" => $filename,
				"allowed_types" => "xlsx|XLSX",
	            "upload_path"   => realpath(APPPATH . $upload_loc),
	            "is_image" => false,
	            "encrypt_name" => false
	    	));

	    	if($this->upload->do_multi_upload("file")){

	    		# Excel Reader (read BAP Form)
				$input_path = realpath(APPPATH . $upload_loc)."/".$filename.".xlsx";
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($input_path);
				
				# Get ID
				$id_bap = $this->input->post('id_bap');

				$id_bap_hotel = $objPHPExcel->getActiveSheet()->getCell('A179')->getValue();
				$id_penc_air = $objPHPExcel->getActiveSheet()->getCell('A180')->getValue();
				
				$i = 0;
				for ($ip=181; $ip < 185; $ip++) { 
					$val = $objPHPExcel->getActiveSheet()->getCell('A'.$ip)->getValue();
					if ($val != "") {
						$id_bahan_kimia_ipal[$i] = $val;
						$i = $i + 1;
					}
				}

				$id_penc_udara = $objPHPExcel->getActiveSheet()->getCell('A185')->getValue();
				
				$i = 0;
				for ($ip=186; $ip < 189; $ip++) { 
					$val = $objPHPExcel->getActiveSheet()->getCell('A'.$ip)->getValue();
					if ($val != "") {
						$id_uji_ambien[$i] = $val;
						$i = $i + 1;
					}
				}

				$id_penc_pb3 = $objPHPExcel->getActiveSheet()->getCell('A189')->getValue();

				$i = 0;
				for ($ip=190; $ip < 194; $ip++) { 
					$val = $objPHPExcel->getActiveSheet()->getCell('A'.$ip)->getValue();
					if ($val != "") {
						$id_jlb3[$i] = $val;
						$i = $i + 1;
					}
				}
				
				$i = 0;
				for ($ip=194; $ip < 196; $ip++) { 
					$val = $objPHPExcel->getActiveSheet()->getCell('A'.$ip)->getValue();
					if ($val != "") {
						$id_tps_b3[$i] = $val;
						$i = $i + 1;
					}
				}

				# array for table bap
				$bap = array(
					'id_industri' => $this->input->post('id_industri'),
					'id_pegawai' => $this->input->post('id_pegawai'),
					// 'url' => $this->input->post(''),
					'bap_hari' => $this->input->post('bap_hari'),
					'bap_tgl' => date("Y-m-d", strtotime($this->input->post('bap_tgl'))),
					'bap_jam' => $this->input->post('bap_jam'),
					'p2_nama' => $this->input->post('p2_nama'),
					'p2_jabatan' => $this->input->post('p2_jabatan'),
					'p2_telp' => $this->input->post('p2_telp'),
					'dok_lingk' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C13')->getValue()),
					'dok_lingk_jenis' => $objPHPExcel->getActiveSheet()->getCell('C14')->getValue(),
					'dok_lingk_tahun' => $objPHPExcel->getActiveSheet()->getCell('E13')->getValue(),
					'izin_lingk' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C15')->getValue()),
					'izin_lingk_tahun' => $objPHPExcel->getActiveSheet()->getCell('E15')->getValue(),
					'catatan' => $objPHPExcel->getActiveSheet()->getCell('B124')->getValue(),
					'tgl_update' => $datenow,
					'diupdate_oleh' => $user
					);

				# edit bap
				$this->bapDao->edit('bap', 'id_bap', $id_bap, $bap);

				# Petugas BAP update procedur
				$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);
				for ($i=1; $i < 4 ; $i++) { 
					# array for table petugas_bap
					$petugas = $this->input->post('id_petugas_'.$i);

					if ($petugas != '') {
						$petugas_bap = array(
							'id_bap' => $id_bap,
							'id_pegawai' => $petugas,
							'ket' => 'update',
							'tgl_pembuatan' => $datenow,
							'dibuat_oleh' => $user
							);

						#insert petugas_bap
						$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
					}
				}
				
				# array for table bap_hotel
				$bap_hotel = array(
					'id_bap' => $id_bap,
					'skala_kegiatan' => $objPHPExcel->getActiveSheet()->getCell('C10')->getValue() ." ". $objPHPExcel->getActiveSheet()->getCell('D10')->getValue(),
					'jml_kamar' => $objPHPExcel->getActiveSheet()->getCell('C11')->getValue(),
					'tingkat_hunian' => $objPHPExcel->getActiveSheet()->getCell('C12')->getValue()
					);

				# edit bap_hotel
				$this->bapDao->edit('bap_hotel', 'id_bap_hotel', $id_bap_hotel, $bap_hotel);

				# array for pencemaran_air
				$pencemaran_air = array(
					'id_bap' => $id_bap,
					'ambil_air_tanah' => $objPHPExcel->getActiveSheet()->getCell('C20')->getValue(),
					'ambil_air_permukaan' => $objPHPExcel->getActiveSheet()->getCell('C21')->getValue(),
					'ambil_air_pdam' => $objPHPExcel->getActiveSheet()->getCell('C22')->getValue(),
					'ambil_air_lain' => $objPHPExcel->getActiveSheet()->getCell('C23')->getValue(),
					'limb_sumber' => $objPHPExcel->getActiveSheet()->getCell('C24')->getValue(),
					'bdn_terima' => $objPHPExcel->getActiveSheet()->getCell('C25')->getValue(),
					'ipal' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C26')->getValue()),
					'ipal_sistem' => $objPHPExcel->getActiveSheet()->getCell('C27')->getValue(),
					'ipal_unit' => $objPHPExcel->getActiveSheet()->getCell('C28')->getValue(),
					'ipal_kapasitas' => $objPHPExcel->getActiveSheet()->getCell('C29')->getValue(),
					'izin' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C30')->getValue()),
					'izin_no' => $objPHPExcel->getActiveSheet()->getCell('C31')->getValue(),
					'izin_tgl' => date("Y-m-d", strtotime($objPHPExcel->getActiveSheet()->getCell('E31')->getValue())),
					'izin_debit' => $objPHPExcel->getActiveSheet()->getCell('C32')->getValue(),
					'koord_outlet_s' => $objPHPExcel->getActiveSheet()->getCell('D33')->getValue(),
					'koord_outlet_e' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('D34')->getValue()),
					'au' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C35')->getValue()),
					'au_jenis' => $objPHPExcel->getActiveSheet()->getCell('C36')->getValue(),
					'au_ukuran' => $objPHPExcel->getActiveSheet()->getCell('C37')->getValue(),
					'au_kondisi' => $objPHPExcel->getActiveSheet()->getCell('C38')->getValue(),
					'cat_deb_hari' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C39')->getValue()),
					'daur_ulang' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C40')->getValue()),
					'daur_ulang_debit' => $objPHPExcel->getActiveSheet()->getCell('C41')->getValue(),
					'uji_limbah' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C42')->getValue()),
					'uji_period' => $objPHPExcel->getActiveSheet()->getCell('C43')->getValue(),
					'uji_lab' => $objPHPExcel->getActiveSheet()->getCell('C44')->getValue(),
					'uji_hasil' => $objPHPExcel->getActiveSheet()->getCell('C45')->getValue(),
					'uji_tidak_bulan' => $objPHPExcel->getActiveSheet()->getCell('C46')->getValue(),
					'uji_tidak_param' => $objPHPExcel->getActiveSheet()->getCell('C47')->getValue(),
					'uji_lapor' => $objPHPExcel->getActiveSheet()->getCell('C48')->getValue(),
					'bocor' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C56')->getValue()),
					'bocor_lokasi' => $objPHPExcel->getActiveSheet()->getCell('C57')->getValue(),
					'lain_lain' => $objPHPExcel->getActiveSheet()->getCell('B59')->getValue()
					);

				# edit pencemaran_air
				$this->bapDao->edit('pencemaran_air', 'id_penc_air', $id_penc_air, $pencemaran_air);
				
				# collecting data for bahan_kimia_ipal
				for ($i=51; $i < 55; $i++) { 
					$bahan_kimia_ipal = array(
			 			'id_penc_air' => $id_penc_air,
			 			'nama' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue(),
			 			'jml_pemakaian' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue()
			 			);

					# edit / insert bahan_kimia_ipal
					$x = $i - 51;
					$idx = $id_bahan_kimia_ipal[$x];

					if ($idx != "") {
						$this->bapDao->edit('bahan_kimia_ipal', 'id_bahan_kimia_ipal', $idx, $bahan_kimia_ipal);
					} else {
						$this->bapDao->insert_getStat('bahan_kimia_ipal', $bahan_kimia_ipal);
					}
				}

				# array for pencemaran_udara
				$pencemaran_udara = array(
					'id_bap' => $id_bap,
					'pelaporan_ua_ue' => $objPHPExcel->getActiveSheet()->getCell('C70')->getValue(),
					'lain_lain' => $objPHPExcel->getActiveSheet()->getCell('B72')->getValue()
					);

				# edit pencemaran_udara
				$this->bapDao->edit('pencemaran_udara', 'id_penc_udara', $id_penc_udara, $pencemaran_udara);
				
				# collecting data for uji_ambien
				$cols = array('C', 'D', 'E');

				$i = 0;
				foreach ($cols as $col) {
					$uji_ambien = array(
						'id_penc_udara' => $id_penc_udara,
						'lokasi' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'62')->getValue()),
						'uji_kualitas' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'63')->getValue()),
						'period' => $objPHPExcel->getActiveSheet()->getCell($col.'64')->getValue(),
						'lab' => $objPHPExcel->getActiveSheet()->getCell($col.'65')->getValue(),
						'bm_pemenuhan' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'66')->getValue()),
						'bm_param' => $objPHPExcel->getActiveSheet()->getCell($col.'67')->getValue(),
						'bm_period' => $objPHPExcel->getActiveSheet()->getCell($col.'68')->getValue()
						);
					
					# edit uji_ambien
					$this->bapDao->edit('uji_ambien', 'id_uji_ambien', $id_uji_ambien[$i], $uji_ambien);
					$i = $i + 1;
				}

				# array for pencemaran_pb3
				$pencemaran_pb3 = array(
					'id_bap' => $id_bap,
					'b3_penyimpanan' => $objPHPExcel->getActiveSheet()->getCell('C82')->getValue(),
					'b3_uji' => $objPHPExcel->getActiveSheet()->getCell('C106')->getValue(), 
					'b3_uji_jenis' => $objPHPExcel->getActiveSheet()->getCell('C107')->getValue(), 
					'b3_uji_param' => $objPHPExcel->getActiveSheet()->getCell('C108')->getValue(),
					'b3_uji_lab' => $objPHPExcel->getActiveSheet()->getCell('C109')->getValue(),
					'b3_uji_waktu' => $objPHPExcel->getActiveSheet()->getCell('C110')->getValue(),
					'b3_neraca' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C111')->getValue()),
					'b3_manifest' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell('C112')->getValue()), 
					'b3_lapor_nm' => $objPHPExcel->getActiveSheet()->getCell('C113')->getValue(),
					'padat_jenis' => $objPHPExcel->getActiveSheet()->getCell('C115')->getValue(), 
					'padat_jumlah' => $objPHPExcel->getActiveSheet()->getCell('C116')->getValue(),
					'padat_sarana_tong' => $objPHPExcel->getActiveSheet()->getCell('D117')->getValue(),
					'padat_sarana_tps' => $objPHPExcel->getActiveSheet()->getCell('D118')->getValue(), 
					'padat_kelola' => $objPHPExcel->getActiveSheet()->getCell('C119')->getValue(),
					'lain_lain' => $objPHPExcel->getActiveSheet()->getCell('B121')->getValue()
					);

				# edit pencemaran_pb3
					$this->bapDao->edit('pencemaran_pb3', 'id_penc_pb3', $id_penc_pb3, $pencemaran_pb3);

					# array for jenis_limbah_b3
				for ($i=77; $i < 81; $i++) {
					$jenis_limbah_b3 = array(
						'id_penc_pb3' => $id_penc_pb3,
						'jenis' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue(),
						'jumlah' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue(),
						'pengelolaan' => $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue(),
						'pihak_ke3' => $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue()
						);

					# edit / insert bahan_kimia_ipal
					$x = $i - 77;
					$idx = $id_jlb3[$x];

					if ($idx != "") {
						$this->bapDao->edit('jenis_limbah_b3', 'id_jlb3', $idx, $jenis_limbah_b3);
					} else {
						$this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
					}

					#insert jenis_limbah_b3
					$this->bapDao->insert_getStat('jenis_limbah_b3', $jenis_limbah_b3);
				}

				# collecting data for tps_b3
				$cols = array('D', 'E');
				$i = 0;
				foreach ($cols as $col) {
					$tps_b3 = array(
						'id_penc_pb3' => $id_penc_pb3,
						'izin' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'85')->getValue()),
						'izin_no' => $objPHPExcel->getActiveSheet()->getCell($col.'86')->getValue(),
						'izin_tgl' => date("Y-m-d", strtotime($objPHPExcel->getActiveSheet()->getCell($col.'87')->getValue())),
						'jenis' => $objPHPExcel->getActiveSheet()->getCell($col.'88')->getValue(),
						'lama_spn_lumpur' => $objPHPExcel->getActiveSheet()->getCell($col.'89')->getValue(),
						'lama_spn_pelumas' => $objPHPExcel->getActiveSheet()->getCell($col.'90')->getValue(),
						'lama_spn_ltl' => $objPHPExcel->getActiveSheet()->getCell($col.'91')->getValue(),
						'lama_spn_lain_nama' => $objPHPExcel->getActiveSheet()->getCell('C92')->getValue(),
						'lama_spn_lain_ket' => $objPHPExcel->getActiveSheet()->getCell($col.'92')->getValue(),
						'koord_s' => $objPHPExcel->getActiveSheet()->getCell($col.'93')->getValue(),
						'koord_e' => $objPHPExcel->getActiveSheet()->getCell($col.'94')->getValue(),
						'ukuran' => $objPHPExcel->getActiveSheet()->getCell($col.'95')->getValue(),
						'ppn_nama_koord' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'97')->getValue()),
						'simbol_label' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'98')->getValue()),
						'saluran_cecer' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'99')->getValue()), 
						'bak_cecer' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'100')->getValue()),
						'kemiringan' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'101')->getValue()),
						'sop_darurat' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'102')->getValue()),
						'log_book' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'103')->getValue()), 
						'apar_p3k' => $this->boolConverter($objPHPExcel->getActiveSheet()->getCell($col.'104')->getValue())
						);

					# edit / insert tps_b3
					$idx = $id_jlb3[$i];

					$this->bapDao->edit('tps_b3', 'id_tps_b3', $idx, $tps_b3);
					$i = $i + 1;
				}

				# delete temp file (BAP form)
				unlink($input_path);

			} else {
				echo "FILE UPLOAD ERROR : ". $this->upload->display_errors();
			}

		} else {

			# Get ID
			$id_bap = $this->input->post('id_bap');
			
			# array for table bap
			$bap = array(
				'id_industri' => $this->input->post('id_industri'),
				'id_pegawai' => $this->input->post('id_pegawai'),
				// 'url' => $this->input->post(''),
				'bap_hari' => $this->input->post('bap_hari'),
				'bap_tgl' => date("Y-m-d", strtotime($this->input->post('bap_tgl'))),
				'bap_jam' => $this->input->post('bap_jam'),
				'p2_nama' => $this->input->post('p2_nama'),
				'p2_jabatan' => $this->input->post('p2_jabatan'),
				'p2_telp' => $this->input->post('p2_telp'),
				'tgl_update' => $datenow,
				'diupdate_oleh' => $user
				);

			# edit bap
			$this->bapDao->edit('bap', 'id_bap', $id_bap, $bap);

			# Petugas BAP update procedur
			$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);
			for ($i=1; $i < 4 ; $i++) { 
				# array for table petugas_bap
				$petugas = $this->input->post('id_petugas_'.$i);

				if ($petugas != '') {
					$petugas_bap = array(
						'id_bap' => $id_bap,
						'id_pegawai' => $petugas,
						'ket' => 'update',
						'tgl_pembuatan' => $datenow,
						'dibuat_oleh' => $user
						);

					#insert petugas_bap
					$this->bapDao->insert_getStat('petugas_bap', $petugas_bap);
				}
			}
		}

		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data successfully Updated'));
			
		redirect('backend/bap_agro');
	}
	
	public function delete() {
		$id_bap = $this->input->post("id");

		$this->bapDao->delete('bap_agro', 'id_bap', $id_bap);

		$this->bapDao->delete('petugas_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('surat_teguran_bap', 'id_bap', $id_bap);
		$this->bapDao->delete('history_item_teguran_bap', 'id_bap', $id_bap);

		$this->bapDao->delete_penc_air($id_bap);
		$this->bapDao->delete_penc_udara($id_bap);
		$this->bapDao->delete_penc_pb3($id_bap);

		$this->bapDao->delete('bap', 'id_bap', $id_bap);
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data successfully deleted'));
		redirect('backend/bap_agro');
	}

	public function dateConv($col){

		if ($col != "") {
			$date = PHPExcel_Shared_Date::ExcelToPHP($col); // 1007596800 (Unix time)
			$date1 = date('Y-m-d', $date);
		
			return $date1;
		} else {
			return NULL;
		}
	}

	# function to convert string to boolean
	# add "sudah" as value occured in BAP Industri
	private function boolConverter($value) {
		if (strcasecmp($value, "ada") == 0 || strcasecmp($value, "ya") == 0 || strcasecmp($value, "sudah") == 0) {
			return true;
		} else if (strcasecmp($value, "tidak ada") == 0 || strcasecmp($value, "tidak") == 0 || strcasecmp($value, "belum") == 0) {
			return false;
		} else if (strcasecmp($value, "") == 0) {
			return NULL;
		} else {}
	}

}
