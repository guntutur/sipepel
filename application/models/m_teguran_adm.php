<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_teguran_adm extends CI_Model {	
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	function get_all($where='', $limit=0, $offset=0, $tahun='') {

		$tahun = ($tahun == '') ? date('Y') : $tahun;

		$data = array();		
		$sql = 'select i.id_industri, i.nama_industri, i.alamat, (select group_concat(laporan_bulan_tahun separator ", ") from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah" and id_industri = i.id_industri and substring(laporan_bulan_tahun, 4, 4) = "#tahun") as kualitas_air, (select group_concat(d.laporan_bulan separator ", ") as jumlah from debit_air_limbah d where d.id_industri = i.id_industri and substring(laporan_bulan, 4, 4) = "#tahun") as catatan_debit, (select group_concat(laporan_bulan_tahun separator ", ") from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi" and id_industri = i.id_industri and substring(laporan_bulan_tahun, 4, 4) = "#tahun") as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ", ") from laporan_hasil_uji where jenis_lhu = "LHU Udara Ambien" and id_industri = i.id_industri and substring(laporan_bulan_tahun, 4, 4) = "#tahun") as kualitas_ambien, (select group_concat(date_format(tanggal, "%m") separator ", ") from rekap_manifest where id_penghasil_limbah = i.id_industri and year(tanggal) = "#tahun") as manifest, (select group_concat(date_format(periode_waktu, "%m") separator ", ") from neraca_b3 where id_industri = i.id_industri) as neraca_b3 from industri i '.$where.' order by i.nama_industri limit '.$offset.', '.$limit;

		$new_sql = str_replace("#tahun", $tahun, $sql);
		
		$query = $this->db->query($new_sql);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {				

				$temp = $this->check_missing_month($this->parse_month($row->kualitas_air));
				$row->kualitas_air = join(':', $temp);				

				$temp = $this->check_missing_month($this->parse_month($row->catatan_debit, "catatan_debit"));
				$row->catatan_debit = join(':', $temp);

				$temp = $this->check_missing_month($this->parse_month($row->kualitas_emisi));
				$row->kualitas_emisi = join(':', $temp);

				$temp = $this->check_missing_month($this->parse_month($row->kualitas_ambien));
				$row->kualitas_ambien = join(':', $temp);

				$temp = $this->check_missing_month($this->parse_month($row->manifest));
				$row->manifest = join(':', $temp);

				$temp = $this->check_missing_month($this->parse_month($row->neraca_b3));
				$row->neraca_b3 = join(':', $temp);

				$data[] = $row;
			}
		}
		return $data;

	}

	function check_missing_month($data) {	
	
		$bulan = array();
		$bulan_laporan = array();
		$c = (int)date('m');
		for($i=1;$i<=$c;$i++) { $bulan[] = $i; }	
			
		if(count($data) == 0){ return array(join(', ', $bulan_laporan), join(', ', $bulan)); }
		foreach($data as $k => $v) {		
			if(($key = array_search($v, $bulan)) !== false) {
				$bulan_laporan[] = $bulan[$key];
				unset($bulan[$key]);
			}
		}		

		return array(join(', ', $bulan_laporan), join(', ', $bulan));
	}

	function parse_month($data, $jenis="") {

		$result = array();
		$month = explode(',', $data);
		
		if($data == '') return $result;	
		if($jenis == '') {
			foreach($month as $k => $v) {
				$m = explode('.', trim($v));
				$result[] = (int)$m[0];
			}	
		}else{		
			foreach($month as $k => $v) {
				$c = explode(':', trim($v));
				// if((int)$c[1] >= 10) {				
				$m = explode('.', trim($c[0]));
				$result[] = (int)$m[0];
				// }
			}	
		}
		return $result;	
	}


	function get_num_rows($where='', $tahun='') {

		$tahun = ($tahun == '') ? date('Y') : $tahun;

		$sql = 'select i.id_industri, i.nama_industri, (select group_concat(laporan_bulan_tahun separator ", ") from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah" and id_industri = i.id_industri and substring(laporan_bulan_tahun, 4, 4) = "#tahun") as kualitas_air, (select group_concat(d.laporan_bulan separator ", ") as jumlah from debit_air_limbah d where d.id_industri = i.id_industri and substring(laporan_bulan, 4, 4) = "#tahun") as catatan_debit, (select group_concat(laporan_bulan_tahun separator ", ") from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi" and id_industri = i.id_industri and substring(laporan_bulan_tahun, 4, 4) = "#tahun") as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ", ") from laporan_hasil_uji where jenis_lhu = "LHU Udara Ambien" and id_industri = i.id_industri and substring(laporan_bulan_tahun, 4, 4) = "#tahun") as kualitas_ambien, (select group_concat(date_format(tanggal, "%m") separator ", ") from rekap_manifest where id_penghasil_limbah = i.id_industri and year(tanggal) = "#tahun") as manifest, (select group_concat(date_format(periode_waktu, "%m") separator ", ") from neraca_b3 where id_industri = i.id_industri) as neraca_b3 from industri i order by i.nama_industri';

		$new_sql = str_replace("#tahun", $tahun, $sql);
		$query = $this->db->query($new_sql);
		return $query->num_rows();
	}
}
?>