<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        # load model
        $this->load->model('m_charts', 'chrDao', TRUE);
        $this->load->model('m_bap', '', TRUE);

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');		
    }

    # default view
    public function index() {
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        /*filter data*/
        $data['jenis_bap'] = $this->m_bap->select_table('jenis_bap');
        $data['usaha_kegiatan'] = $this->m_bap->select_table('usaha_kegiatan');
        $data['tahun_trend_ketaatan'] = $this->chrDao->select_tahun_trend_ketaatan();
        $data['tahun_trend_dokumen'] = $this->chrDao->select_tahun_trend_dokumen();
        $data['tahun5_bap'] = $this->chrDao->select_tahun5('bap');
        $data['tahun5_lhu'] = $this->chrDao->select_tahun5('laporan_hasil_uji');

        $data['kecamatan'] = $this->m_bap->select_table('kecamatan');

        $this->load->view('include/header');
		// $this->load->view('include/box-themes');		
        $this->load->view('backend/dashboard', $data);
        $this->load->view('include/footer');
    }

    public function load_data() {

      /*filter data*/
      $data['jenis_bap'] = $this->m_bap->select_table('jenis_bap');
      $data['usaha_kegiatan'] = $this->m_bap->select_table('usaha_kegiatan');
      $data['tahun_trend_ketaatan'] = $this->chrDao->select_tahun_trend_ketaatan();
      $data['tahun_trend_dokumen'] = $this->chrDao->select_tahun_trend_dokumen();
      $data['tahun5_bap'] = $this->chrDao->select_tahun5('bap');
      $data['tahun5_lhu'] = $this->chrDao->select_tahun5('laporan_hasil_uji');
      $data['kecamatan'] = $this->m_bap->select_table('kecamatan');

      echo json_encode($data, JSON_NUMERIC_CHECK);
      exit();

    }

    
    public function get_year_langgar_bap() {
        // charts function to-do here
        $langgar = $this->chrDao->get_year_langgar_bap();

        foreach ($langgar as $v)
        {
            $year[] = $v->year;
            $total[] = $v->total;
            // echo $v->year.'-'.$v->total;
        }

        $data['year'] = $year;
        $data['total'] = $total;

        echo json_encode($data, JSON_NUMERIC_CHECK);
        exit();
    }

    public function get_year_langgar_lhu() {
        // charts function to-do here
        $langgar = $this->chrDao->get_year_langgar_lhu();

        foreach ($langgar as $v)
        {
            $year[] = $v->year;
            $total[] = $v->total;
            // echo $v->year.'-'.$v->total;
        }

        $data['year'] = $year;
        $data['total'] = $total;

        echo json_encode($data, JSON_NUMERIC_CHECK);
        exit();
    }

    public function get_item_langgar_lhu() {
        // charts function to-do here
        $langgar = $this->chrDao->get_item_langgar_lhu_2();

        foreach ($langgar as $v)
        {
            $parameter_lhu[] = $v->parameter_lhu;
            $total[] = $v->total;
        }

        $data['parameter_lhu'] = $parameter_lhu;
        $data['total'] = $total;

        echo json_encode($data, JSON_NUMERIC_CHECK);
        exit();
    }
    
    public function get_year_industri_langgar_bap() {
        // charts function to-do here
        $langgar = $this->chrDao->get_year_industri_langgar_bap();

        foreach ($langgar as $v)
        {
            $year[] = $v->year;
            $nama_industri[] = $v->nama_industri;
            $total[] = $v->total;
            // $year[$v->year] = ;
        }

        $data['year'] = $year;
        $data['nama_industri'] = $nama_industri;
        $data['total'] = $total;


        echo json_encode($datax, JSON_NUMERIC_CHECK);
        exit();
    }

    public function get_year_industri_langgar_lhu() {
        // charts function to-do here
        $langgar = $this->chrDao->get_year_industri_langgar_lhu();

        foreach ($langgar as $v)
        {
            $year[] = $v->year;
            $nama_industri[] = $v->nama_industri;
            $total[] = $v->total;
        }

        $data['year'] = $year;
        $data['nama_industri'] = $nama_industri;
        $data['total'] = $total;

        echo json_encode($data, JSON_NUMERIC_CHECK);
        exit();
    }

    public function persebaran_industri() {
        $data = $this->chrDao->persebaran_industri($this->input->post("id_kecamatan"));
        echo json_encode($data);
        exit();
    }

    public function top5param_lhu() {
        $data = $this->chrDao->top5param_lhu(
            $this->input->post("tahun"),
            $this->input->post("jenis_lhu"),
            $this->input->post("usaha_kegiatan")
        );
        echo json_encode($data);
        exit();
    }

    public function top5param_bap() {
        $data = $this->chrDao->top5param_bap(
            $this->input->post("tahun"),
            $this->input->post("usaha_kegiatan")
        );
        echo json_encode($data);
        exit();
    }

    public function trendline_ketaatan_industri() {
        $data = $this->chrDao->trendline_ketaatan_industri($this->input->post("tahun"));
        echo json_encode($data);
        exit();
    }

    public function trendline_dokumen_bplh() {
        $data = $this->chrDao->trendline_dokumen_bplh(
            $this->input->post("tahun"),
            $this->input->post("usaha_kegiatan")
        );
        echo json_encode($data);
        exit();
    }
    
}
