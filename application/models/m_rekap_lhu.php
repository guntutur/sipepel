<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_rekap_lhu extends CI_Model {

	private $table_name			= 'laporan_hasil_uji';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	// function get_all($where=array(), $limit=0, $offset=0) {

	// 	$data = array();

	// 	$this->db->select('l.no_lhu, l.id_laporan_hasil_uji, l.laporan_bulan_tahun, l.status, l.jenis_lhu, i.nama_industri, b.nama_lab, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = l.id_laporan_hasil_uji group by l.id_industri) as parameter_teguran');
	// 	$this->db->from($this->table_name .' as l');
	// 	$this->db->join('industri i', 'i.id_industri = l.id_industri', 'inner');
	// 	$this->db->join('laboratorium b', 'b.id_lab = l.id_lab', 'inner');

	// 	foreach($where as $key => $val) {
	// 		if($val[0] == 'SEARCH') {
	// 			if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
	// 			else $this->db->like($key, $this->db->escape_like_str($val[2]));
	// 		}else{
	// 			if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
	// 			else $this->db->where($key, $this->db->escape_like_str($val[2]));
	// 		}
	// 	}

	// 	$this->db->limit($limit, $offset);		
	// 	$this->db->order_by("l.laporan_bulan_tahun", "desc"); 
	// 	// $this->db->order_by("l.tgl_pembuatan", "desc"); 
	// 	// $this->db->group_by("title"); 

	// 	$query = $this->db->get();
			
	// 	if ($query->num_rows() > 0) {
	// 		foreach ($query->result() as $row) {
	// 			$data[] = $row;
	// 		}
	// 	}

	// 	return $data;
	// }

	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();		
		$sql = 'select i.id_industri, i.nama_industri, l.laporan_bulan_tahun, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = air.id_laporan_hasil_uji) as teguran_air, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = emisi.id_laporan_hasil_uji) as teguran_emisi, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = ambien.id_laporan_hasil_uji) as teguran_ambien from industri i left join laporan_hasil_uji l on l.id_industri = i.id_industri left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah") air on air.id_industri = i.id_industri and air.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi") emisi on emisi.id_industri = i.id_industri and emisi.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Ambien") ambien on ambien.id_industri = i.id_industri and ambien.laporan_bulan_tahun = l.laporan_bulan_tahun group by i.nama_industri, l.laporan_bulan_tahun order by l.laporan_bulan_tahun desc limit '.$offset.''.$limit;

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;

	}

	function get_num_rows($where) {

		$sql = 'select i.id_industri, i.nama_industri, l.laporan_bulan_tahun, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = air.id_laporan_hasil_uji) as teguran_air, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = emisi.id_laporan_hasil_uji) as teguran_emisi, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = ambien.id_laporan_hasil_uji) as teguran_ambien from industri i left join laporan_hasil_uji l on l.id_industri = i.id_industri left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah") air on air.id_industri = i.id_industri and air.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi") emisi on emisi.id_industri = i.id_industri and emisi.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Ambien") ambien on ambien.id_industri = i.id_industri and ambien.laporan_bulan_tahun = l.laporan_bulan_tahun group by i.nama_industri, l.laporan_bulan_tahun order by l.laporan_bulan_tahun desc';

		$query = $this->db->query($sql);

		return $this->db->count_all_results();
	}


	/**
	 * huda : add -> get all history_item_teguran_lhu to be used in laporan lhu (24 Oct 2014, 13:23)
	 *
	 * Get all history_item_teguran_lhu
	 *
	 * @return array
	 */
	function get_all_teguran_lhu() {
		$data = array();

		$query = $this->db->query("SELECT b.id_industri, a.ket_paramter_lhu, b.jenis_lhu
									FROM history_item_teguran_lhu a
									LEFT JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
									ORDER BY b.id_industri ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

}
?>