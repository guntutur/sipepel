<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAKU MUTU LHU&nbsp;&nbsp;<small>mengelola seluruh baku mutu yang digunakan oleh LHU</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Baku Mutu LHU</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>
                
                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
								<th width="10%">Jenis LHU</th>
								<th width="5%" class="text-center">#</th>
								<th>Jenis Usaha/Kegiatan</th>
								<th>Jenis/Kelas/Tipe</th>
                                <th>Dasar Hukum</th>
								<th width="15%">Jumlah Parameter</th>								                   
                                <th class="text-right" width="5%"></th>									
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="7">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start"></span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/baku_mutu_lhu/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);                    
                    $('#start').text(d.data.length);                  
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
					var lhu;
					var nomer = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                        var hukum_internal = (dt.dasar_hukum_internal != '') ? 'Ada': 'Tidak Ada';
                        var hukum_eksternal = (dt.dasar_hukum_eksternal != '') ? 'Ada': 'Tidak Ada';
						var jenis_industri = (dt.nama_jenis_kegiatan != null ) ? dt.nama_jenis_kegiatan : dt.data_acuan;
						var jenis_lhu = (dt.jenis_lhu != lhu ) ? dt.jenis_lhu : ' ';
						var no_lhu = (dt.jenis_lhu == lhu ) ? (nomer+=1) : nomer = 1;
                        t += '<tr>';
						t += '<td>'+jenis_lhu+'</td>';
						t += '<td class="text-center">'+no_lhu+'</td>';
						t += '<td>'+dt.usaha_kegiatan+'</td>';
                        t += '<td>'+jenis_industri+'</td>';
						t += '<td><b>Umum</b>: '+hukum_internal+'<br/><b>Khusus</b>: '+hukum_eksternal+'</td>';
						t += '<td>'+ ((dt.jumlah) ? dt.jumlah : 0)+'</td>';                       
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
						t += '<li><a href="' + '<?php echo base_url("backend/baku_mutu_lhu/detail/'+ dt.id_parameter_lhu + '"); ?>' + '">Tambah Parameter</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_parameter_lhu+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_parameter_lhu+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
						lhu = dt.jenis_lhu;
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

	$(document).ready(function () {
		
		$("#form_add").validate({
            rules: {
               jenis_lhu:  { required: true },
               usaha_kegiatan: { required: true },
               jenis_industri: { required: true },
               hukum_internal: { required: true, minlength: 4 }               
            },
            messages: {        
                jenis_lhu:  { required: "Silahkan pilih data" },
                usaha_kegiatan: { required: "Silahkan pilih data" },
                jenis_industri: { required: "Silahkan pilih data" },
                hukum_internal: { 
                    required: "Masukkan dasar hukum internal",
                    minlength: "Dasar hukum internal harus lebih dari 4 karakter"
                }                
            }
        });

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) { 
            // validator.resetForm();              
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/baku_mutu_lhu/get'); ?>",
                data: 'id_parameter_lhu=' + id,
                success: function (response) {
                    // console.log(response);
                    $('input[id=edit_id]').val(response.id_parameter_lhu);
                    $('select[name=usaha_kegiatan]').val(response.id_usaha_kegiatan);
                    $(this).load_jenis_industri(response.id_usaha_kegiatan, response.id_jenis_industri);                   
                    $('textarea[name=hukum_internal]').val(response.dasar_hukum_internal);
                    $('textarea[name=hukum_eksternal]').val(response.dasar_hukum_eksternal);                    
					$('select[name=jenis_lhu]').val(response.jenis_lhu);
                    
                    $('#myModal').modal('show');
                }
            })
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        } 		
		
        $('select[name=jenis_industri]').prop('disabled', true);        
        $('#usaha_kegiatan').change(function() {
            var id_usaha_kegiatan = $(this).val();
            $(this).load_jenis_industri(id_usaha_kegiatan);
        });

        $.fn.load_jenis_industri = function(id_usaha_kegiatan, id_jenis_industri) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/jenis_industri/get_by_usaha_kegiatan_merge_data_acuan'); ?>",
                data: 'id_usaha_kegiatan=' + id_usaha_kegiatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {
                            opt += '<option value="'+v.id_jenis_industri+'" '+ ((v.id_jenis_industri == id_jenis_industri) ? "selected" : "") +'>'+v.ket+'</option>';
                        });
                        $('select[name=jenis_industri]').prop('disabled', false);
                    }else{
                        $('select[name=jenis_industri]').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#jenis_industri").html(opt);
                }
            });
        }
		
        Document.search();
	});
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Baku Mutu LHU</h4>
            </div>
            <form role="form" data-action="<?php echo base_url('/backend/baku_mutu_lhu') ?>" method="post" id="form_add">
                <div class="modal-body">
					<!-- <div class="row"> -->
					<div class="form-group jenis_lhu">
						<label>Jenis LHU</label>
						<select name="jenis_lhu" id="jenis_lhu" class="form-control input-sm">
							<option value="" disabled selected>-- Pilih Jenis LHU --</option>
							<?php 
								$jenis = $this->functions->get_jenis_lhu();
								foreach($jenis as $k => $s) {
									echo '<option value="'.$k.'">'.$s.'</option>';
								}
							?>                      
						</select>                        
					</div>
					 <div class="row">
						<div class="col-lg-6 form-group usaha_kegiatan">
							<label>Jenis Usaha/Kegiatan</label>                        
							<select name="usaha_kegiatan" id="usaha_kegiatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Jenis Usaha/Kegiatan --</option>
                                <?php foreach($usaha_kegiatan as $uk): ?>
                                    <option value="<?php echo $uk->id_usaha_kegiatan;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>                        
						</div>
						<div class="col-lg-6 form-group jenis_industri">
							<label>Jenis/Kelas/Tipe</label>                        
							<select name="jenis_industri" id="jenis_industri" class="form-control input-sm">
                                <option value="" selected>-- Pilih Jenis/Kelas/Tipe --</option>                                                   
                            </select>                        
						</div>		
					</div>					
                    <div class="row">
                        <div class="col-lg-6 form-group hukum_parameter">
                            <label>Dasar Hukum Umum</label>                                     
                            <textarea rows="5" name="hukum_internal" id="hukum_internal" class="form-control input-sm has-feedback"></textarea> 
                        </div>
                        <div class="col-lg-6 form-group value_parameter">
                            <label>Dasar Hukum Khusus</label>
                            <textarea rows="5" name="hukum_eksternal" id="hukum_eksternal" class="form-control input-sm has-feedback"></textarea>                                                
                        </div>
                    </div>             											  						
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Baku Mutu LHU dan Parameter</h4>
            </div>
            <form action="<?php echo base_url('/backend/baku_mutu_lhu/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini beserta semua parameter yang dimiliki jenis usaha/kegiatan yang akan dihapus?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->