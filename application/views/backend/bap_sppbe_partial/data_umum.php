<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 20/12/16
 * Time: 20:53
 */
?>

<div class="tab-pane active form-validated" id="data_umum">
    <h4 class="page-tab"><i class="fa fa-newspaper-o icon-sidebar"></i>Data Umum Usaha/Kegiatan</h4>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>Tanggal Pengawasan</label>
            <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD" value="<?php echo $bap->bap_tgl;?>">
        </div>
        <div class="form-group col-lg-6">
            <label>Pukul</label>
            <div class="bfh-timepicker">
                <input id="timepick2" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-6">
            <label>Petugas Pengawas</label>
            <select class="form-control input-sm" name="id_pegawai">
                <option value=""></option>
                <?php
                foreach ($pegawai as $p) {
                    echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                }?>
            </select>
        </div>
        <div class="form-group col-lg-6">
            <label>Beserta Anggota Pengawas:</label>
            <select class="form-control input-sm multiselect" name="petugas_pengawas[]" id="ini_id_petugas_bap2" multiple="multiple" style="border: 0!important;">
                <?php
                foreach ($pegawai as $p) {
                    echo "<option value='$p->id_pegawai' ";
                    for ($i=0; $i < count($petugas_bap); $i++) {
                        if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                            echo "selected";
                        }
                    }
                    echo ">$p->nama_pegawai</option>";
                }?>
            </select>
        </div>
    </div>
    <fieldset>
        <legend>Lokasi Pengawasan / Pembinaan</legend>
        <div class="form-group status">
            <label>Nama Usaha/Kegiatan</label>
            <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                <option value="Empty">&nbsp;</option>
                <?php
                foreach ($industri as $value) {
                    echo '<option value="'.$value->id_industri.'"';
                    if($bap->id_industri==$value->id_industri){
                        echo 'selected';
                    }
                    echo '>'.$value->nama_industri.'</option>';
                }
                ?>
            </select>
            <!-- <input type="text" value="<?php echo $bap->nama_industri?>" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                            <input type="hidden" id="id_industri" name="id_industri" value="<?php echo $bap->id_industri?>"/> -->
        </div>
        <div id="container2" style="display:none"></div>
    </fieldset>
    <div class="form-group ">
        <label>Kapasitas Pengisian</label>
        <input type="text" name="kapasitas_pengisian" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->kapasitas_pengisian?> "/>
    </div>
    <div class="form-group ">
        <div class="row">
            <div class="col-lg-6">
                <label>Koordinat SPPBE S</label>
                <div class="form-group status">
                    <div class="row">
                        <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                            <div class="input-group input-group-sm">
                                <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_derajat_s?>" name="koord_sppbe_derajat_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                            </div>
                        </div>
                        <div class="col-lg-4" style="padding: 0; margin:0;">
                            <div class="input-group input-group-sm">
                                <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_jam_s?>" name="koord_sppbe_jam_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                <span class="input-group-addon">'</span>
                            </div>
                        </div>
                        <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                            <div class="input-group input-group-sm">
                                <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_menit_s?>" name="koord_sppbe_menit_s" class="form-control input-sm has-feedback" placeholder="0"  />
                                <span class="input-group-addon">"</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label>Koordinat SPPBE E</label>
                <div class="form-group status">
                    <div class="row">
                        <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                            <div class="input-group input-group-sm">
                                <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_derajat_e?>" name="koord_sppbe_derajat_e" class="form-control input-sm has-feedback" placeholder="0"  />
                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                            </div>
                        </div>
                        <div class="col-lg-4" style="padding: 0; margin:0;">
                            <div class="input-group input-group-sm">
                                <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_jam_e?>" name="koord_sppbe_jam_e" class="form-control input-sm has-feedback" placeholder="0"  />
                                <span class="input-group-addon">'</span>
                            </div>
                        </div>
                        <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                            <div class="input-group input-group-sm">
                                <input type="text" value="<?php echo $bap_sppbe->koord_sppbe_menit_e?>" name="koord_sppbe_menit_e" class="form-control input-sm has-feedback" placeholder="0"  />
                                <span class="input-group-addon">"</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover" id="datatable-example">
            <thead class="the-box dark full">
            <tr>
                <!-- <div class="form-group "> -->
                <th>Dokumen Lingkungan</th>
                <th>Izin Lingkungan</th>

            </tr>
            </thead>
            <tbody>
            </tr>
            <td>
                <!-- <div class="radio"> -->
                <label>
                    <input class="dl" type="radio" name="dl" id="dl1" value="ada" <?php echo (($bap->dok_lingk==1) ? 'checked' : '')?>>Ada
                </label>
                <label>
                    <input class="dl" type="radio" name="dl" id="dl2" value="tidak ada" <?php echo (($bap->dok_lingk==0) ? 'checked' : '')?>>Tidak Ada
                </label>
                <!-- </div> -->
            </td>
            <!-- </div> -->
            <!-- <div class="form-group "> -->
            <td>
                <!-- <div class="radio"> -->
                <label>
                    <input type="radio" name="il" id="il1" value="ada" <?php echo (($bap->izin_lingk==1) ? 'checked' : '')?>>Ada
                </label>
                <label>
                    <input type="radio" name="il" id="il2" value="tidak ada" <?php echo (($bap->izin_lingk==0) ? 'checked' : '')?>>Tidak Ada
                </label>
                <!-- </div> -->
            </td>

            </tr>
            <tr>
                <td>
                    <div class="form-group " id="jdl" name="jdl" style="display:none">
                        <!-- <div class="form-group "> -->
                        <label>Jenis Dokumen</label>
                        <select class="form-control input-sm" name="jdl_name" id="jdl_name">
                            <option value="">-- Pilih salah satu --</option>>
                            <?php
                            foreach ($dok_lingk_jenis as $j) {
                                echo "<option value='$j' ".(($bap->dok_lingk_jenis == $j) ? 'selected' : '').">$j</option>";
                            }?>
                        </select>
                        <!-- </div> -->
                        <!-- <div class="form-group "> -->
                        <label>Tahun</label>
                        <input type="text" name="dlt" id="dlt" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun; ?>"/>
                        <!-- </div> -->
                    </div>
                </td>
                <td>
                    <div class="form-group " id="il_tahun" style="display:none">
                        <label>Tahun</label>
                        <input type="text" name="il_tahun" id="ilt" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun; ?>"/>
                    </div>
                </td>

            </tr>
            <tr>
                <div class="form-group ">
                    <label>Jumlah Karyawan</label>
                    <input type="text" name="jml_karyawan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->jumlah_karyawan; ?>"/>
                </div>
            </tr>
            <!-- </div> -->
            </tbody>
        </table>
    </div>
    <fieldset>
        <legend>Penanggung Jawab Usaha / Kegiatan</legend>
        <div class="form-group ">
            <label>Nama</label>
            <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama; ?>"/>
        </div>
        <div class="form-group ">
            <label>Telepon</label>
            <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp; ?>"/>
        </div>
        <div class="form-group ">
            <label>Jabatan</label>
            <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan; ?>"/>
        </div>
    </fieldset>

    <fieldset>
        <div class="form-group">
            <div class="form-group">
                <label>a. Sarana Pemadam Kebakaran</label>
            </div>
            <div class="form-group">
                <label>Kolam Pemadam Kebakaran</label>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Kolam Pemadam Kebakaran</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="jumlah_kolam_pemadam" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_kolam_pemadam?>"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Ukuran</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="ukuran_kolam_pemadam" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->ukuran_kolam_pemadam?>"/>
                </div>
            </div>
            <div class="form-group">
                <label>APAR</label>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Jumlah</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="jumlah_apar" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_apar?>"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Kapasitas</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="kapasitas_apar" id="" class="form-control input-sm has-feedback" placeholder="kg" value="<?php echo $bap_sppbe->kapasitas_apar?>"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>b. Jumlah Agen yang dilayani</label>
            <input type="text" name="agen_dilayani" id="" class="form-control input-sm has-feedback" placeholder="Agen" value="<?php echo $bap_sppbe->agen_dilayani?>"/>
        </div>
        <div class="form-group">
            <div class="form-group">
                <label>c. Sarana Pemanenan Air Hujan</label>
            </div>
            <div class="form-group">
                <label>Sumur Resapan</label>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Jumlah</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="jumlah_sumur_resapan" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_sumur_resapan?>"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Ukuran</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="ukuran_sumur_resapan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_sppbe->ukuran_sumur_resapan?>"/>
                </div>
            </div>
            <div class="form-group">
                <label>Lubang resapan Biopori</label>
            </div>
            <div class="form-group row">
                <div class="form-group col-lg-6">
                    <label> -- Jumlah</label>
                </div>
                <div class="form-group col-lg-6">
                    <input type="text" name="jumlah_biopori" id="" class="form-control input-sm has-feedback" placeholder="unit" value="<?php echo $bap_sppbe->jumlah_biopori?>"/>
                </div>
            </div>
        </div>
        <legend>Lain-lain</legend>
        <div class="form-group ">
            <textarea class="form-control input-sm no-resize bold-border" name="lain_lain_bap" style="height: 180px;"><?php echo $bap->catatan; ?></textarea>
        </div>
    </fieldset>

    <div class="row"> <!-- navigation -->
        <div class="col-sm-6 text-left">
            <input id="save-datum" class="btn btn-md btn-success save-partially" value="Simpan Data Umum"/>
        </div>
        <div class="col-sm-6 text-right">
            <a class="btn btn-warning NextStep">Pencemaran Air<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>
