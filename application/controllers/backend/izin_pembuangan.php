<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class izin_pembuangan extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model
        $this->load->model('m_izin_pembuangan');
        $this->load->model('m_industry');
		$this->load->model('m_data_master', '', TRUE);
        		
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $data['industri'] = $this->m_industry->get_dropdown();        
		$sumber_air_limbah = "sumber_air_limbah";
		$data['data_acuan_proses'] = $this->m_data_master->get_dropdown($sumber_air_limbah);
		$badan_air_penerima = "badan_air_penerima";
		$data['data_acuan_badan_air'] = $this->m_data_master->get_dropdown($badan_air_penerima);

    	$this->load->view('include/header');
        $this->load->view('backend/v_izin_pembuangan', $data);
        $this->load->view('include/footer');
    }

   public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'i.id_industri' => array('SEARCH', 'OR', $query),
				'j.ket' => array('SEARCH', 'OR', $query),
				'k.ket' => array('SEARCH', 'OR', $query),			
				'i.nama_industri' => array('SEARCH', 'OR', $query),
				'i.badan_hukum' => array('SEARCH', 'OR', $query),
				'al.no_izin' => array('SEARCH', 'OR', $query),
				'al.badan_air_penerima' => array('SEARCH', 'OR', $query),
				'al.proses' => array('SEARCH', 'OR', $query)
			);

     	$total = $this->m_izin_pembuangan->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_izin_pembuangan->get_all($where, $dataperpage, $start);
	
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		$data = $this->m_izin_pembuangan->get_by_id($this->input->post('id_izin_pembuangan'));
		
		# proses
        if($data->proses) {
            $proses_produksi = ($data->proses) ? unserialize($data->proses) : array();
            $data->proses = $proses_produksi;
        }
		
		echo json_encode($data);
        exit();
	}

    public function register() {       

        date_default_timezone_set("Asia/Jakarta");
        $tgl_pembuatan = date("Y-m-d H:i:s");
		
		//$koor_outlet = serialize($this->input->post('koor_outlet'));
		$koor_s = $this->input->post('derajat_s'). ':' .$this->input->post('jam_s'). ':' .$this->input->post('menit_s'); 
		$koor_e = $this->input->post('derajat_e'). ':' .$this->input->post('jam_e'). ':' .$this->input->post('menit_e'); 
		$koor_outlet = $koor_s. ',' .$koor_e;
        $param = array(         
				'status' => $this->input->post('status'),
                'id_industri' => $this->input->post('industri'),
                'no_izin' => $this->input->post('no_izin'),
                'tgl_izin' => date("Y-m-d", strtotime($this->input->post('tgl_izin'))),
				'tentang' => $this->input->post('tentang'),
                'debit_perhari' => $this->input->post('debit_perhari'),
				'debit_perbulan' => $this->input->post('debit_perbulan'),
                'proses' => serialize($this->input->post('proses')),
                //'koordinat_outlet' => $koor_outlet,
				'badan_air_penerima' => $this->input->post('badan_air_penerima'),
				'lama_pembuangan' => $this->input->post('lama_pembuangan'),
				'baku_mutu_pembuangan' => $this->input->post('baku_mutu_pembuangan'),
				'derajat_s' => $this->input->post('derajat_s'),
				'jam_s' => $this->input->post('jam_s'),
				'menit_s' => $this->input->post('menit_s'),
				'derajat_e' => $this->input->post('derajat_e'),
				'jam_e' => $this->input->post('jam_e'),
				'menit_e' => $this->input->post('menit_e'),
				'dasar_hukum' => $this->input->post('dasar_hukum'),
                'tgl_pembuatan' => $tgl_pembuatan,
                'tgl_update' => $tgl_pembuatan,
                'dibuat_oleh' =>$this->tank_auth->get_personname(),
                'diupdate_oleh' => $this->tank_auth->get_personname()
            );
        
        if($this->m_izin_pembuangan->insert($param))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
			
		redirect('backend/izin_pembuangan');

    }
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		
		$koor_s = $this->input->post('derajat_s'). ':' .$this->input->post('jam_s'). ':' .$this->input->post('menit_s'); 
		$koor_e = $this->input->post('derajat_e'). ':' .$this->input->post('jam_e'). ':' .$this->input->post('menit_e'); 
		$koor_outlet = $koor_s. ',' .$koor_e;
		
		$param = array(
			'status' => $this->input->post('status'),
			'id_industri' => $this->input->post('industri'),
			'no_izin' => $this->input->post('no_izin'),
			'tgl_izin' => date('Y-m-d', strtotime($this->input->post('tgl_izin'))), 
			'tentang' => $this->input->post('tentang'),
			'debit_perhari' => $this->input->post('debit_perhari'),
			'debit_perbulan' => $this->input->post('debit_perbulan'),
			'proses' => serialize($this->input->post('proses')),
			//'koordinat_outlet' => $koor_outlet,
			'badan_air_penerima' => $this->input->post('badan_air_penerima'),
			'lama_pembuangan' => $this->input->post('lama_pembuangan'),
			'baku_mutu_pembuangan' => $this->input->post('baku_mutu_pembuangan'),
			'derajat_s' => $this->input->post('derajat_s'),
			'jam_s' => $this->input->post('jam_s'),
			'menit_s' => $this->input->post('menit_s'),
			'derajat_e' => $this->input->post('derajat_e'),
			'jam_e' => $this->input->post('jam_e'),
			'menit_e' => $this->input->post('menit_e'),
			'dasar_hukum' => $this->input->post('dasar_hukum'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_izin_pembuangan->change($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/izin_pembuangan');
	}
	
	public function delete() {
		$this->m_izin_pembuangan->delete($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/izin_pembuangan');
	}
	
	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $izin = $this->m_izin_pembuangan->get_izin_detail_by_id($id);

        $badan_hukum = $izin->badan_hukum;
		$industri = $izin->nama_industri;

        $data['title'] = $badan_hukum." ".$industri;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_baku_mutu_izin', $data);
        $this->load->view('include/footer');
	}
	
	public function get_detail() {		
		$data = $this->m_izin_pembuangan->get_detail_by_id($this->input->post('id_baku_mutu_izin'));
		
		echo json_encode($data);
        exit();
	}

	public function get_detail_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = "id_pembuangan = ".$id ;
    
     	$total = $this->m_izin_pembuangan->get_detail_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_izin_pembuangan->get_detail_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }

	public function insert_detail() {
        $izin = $this->m_izin_pembuangan->get_izin_detail_by_id($this->input->post('id'));
		if($this->validate_detail()) {
			$param = array(
					'id_pembuangan' => $this->input->post('id'),
					'id_industri' => $izin->id_industri,
					'nilai_baku_mutu' => $this->input->post('nilai_baku_mutu'),
					'jenis_baku_mutu' => $this->input->post('jenis_baku_mutu'),
					'ket' => $this->input->post('ket'),
					'satuan' => $this->input->post('satuan'),
					'status' => $this->input->post('status')
				);

			if($this->m_izin_pembuangan->insert_detail($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function edit_detail() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		
		$param = array(			
				'nilai_baku_mutu' => $this->input->post('nilai_baku_mutu'),
				'ket' => $this->input->post('ket'),
				'jenis_baku_mutu' => $this->input->post('jenis_baku_mutu'),
				'satuan' => $this->input->post('satuan'),
				'status' => $this->input->post('status')
			);
		
		if($this->m_izin_pembuangan->change_detail($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_detail() {
		$this->m_izin_pembuangan->delete_detail($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function validate_detail() {
		# check data yang duplicate
		if($this->m_izin_pembuangan->is_duplicate_detail($this->input->post('jenis_baku_mutu'), $this->input->post('id'), $this->input->post('ket'), $this->input->post('nilai_baku_mutu'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}
}
?>