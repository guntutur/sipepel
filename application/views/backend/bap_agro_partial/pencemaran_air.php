<?php
/**
 * Created by PhpStorm.
 * User: zer0
 * Date: 11/12/16
 * Time: 2:16
 */

?>

<div class="tab-pane" id="pencemaran_air">
    <h4 class="page-tab">Pengendalian Pencemaran Air</h4>
    <div class="form_group">
        <label>a. Sumber Air dan Penggunaan</label>

        <div class="form-group">
            <div class="form-group col-lg-6">
                <label>Air Tanah</label>
                <input type="text" name="air_tanah" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_tanah : ''; ?>"/>
                <div class="radio">
                    <label>Perizinan    : </label>
                    <label>
                        <input type="radio" name="izin_air_tanah" value="ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_tanah_izin==1) ? 'checked' : '') : '';?>>Ada
                    </label>
                    <label>
                        <input type="radio" name="izin_air_tanah" value="tidak ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_tanah_izin==0) ? 'checked' : '') : '';?>>Tidak Ada
                    </label>
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label>Air Permukaan</label>
                <input type="text" name="air_permukaan" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_permukaan : ''; ?>"/>
                <div class="radio">
                    <label>Perizinan    : </label>
                    <label>
                        <input type="radio" name="izin_air_permukaan" value="ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_permukaan_izin==1) ? 'checked' : '') : '';?>>Ada
                    </label>
                    <label>
                        <input type="radio" name="izin_air_permukaan" value="tidak ada" <?php echo isset($pencemaran_air) ? (($pencemaran_air->ambil_air_permukaan_izin==0) ? 'checked' : '') : '';?>>Tidak Ada
                    </label>
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label>Air PDAM</label>
                <input type="text" name="air_pdam" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ?  $pencemaran_air->ambil_air_pdam : ''; ?>"/>
            </div>
            <div class="form-group col-lg-6">
                <label>Air Lain-lain</label>
                <input type="text" name="air_lain" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ambil_air_lain : ''; ?>"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-lg-6">
            <label>b. Sumber Air Limbah</label>
            <!-- <select name="limb_sumber" id="" class="form-control input-sm"> -->
            <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                <!-- <option value=''></option> -->
                <?php
                if (isset($pencemaran_air)) {$arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);}
                foreach ($limb_sumber as $l) {
                    echo "<option value='$l->ket' ";
                    if (isset($arr_limb_sumber)) {
                        for ($i = 0; $i < count($arr_limb_sumber); $i++) {

                            if ($arr_limb_sumber[$i] == $l->ket) {
                                echo 'selected';
                            }
                        }
                    }
                    echo ">$l->ket</option>";
                }
                ?>
            </select>
        </div>
        <div class="form-group col-lg-6">
            <label>c. Badan Air Penerima</label>
            <select name="bdn_terima" id="" class="form-control input-sm">
                <option value=''></option>
                <?php
                foreach ($bdn_terima as $l) {
                    echo "<option value='$l->ket' ";
                    if(isset($pencemaran_air)) {
                        if ($pencemaran_air->bdn_terima == $l->ket) {
                            echo 'selected';
                        }
                    }
                    echo ">$l->ket</option>";
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label>d. IPAL</label>
        <select class="form-control input-sm" name="ipal" id="ipal">
            <?php
            foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if(isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->ipal), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
                //$n++;
            }
            ?>
        </select>
    </div>

    <div class="form-group" id="detail_ipal" style="dsiplay:none">
        <div class="form-group row">
            <div class="col-lg-4">
                <label>Sistem IPAL</label>
                <select class="form-control input-sm" id="si" name="ipal_sistem">
                    <?php
                    if(isset($pencemaran_air)) {
                        $si = $pencemaran_air->ipal_sistem;
                        if (($si === "F") || ($si === "F-B") || ($si === "F-B-K") || ($si === "F-K") || ($si === "F-K-B")) {
                            foreach ($ipal as $p) {
                                echo "<option value='$p->ket' " . (($si == $p->ket) ? 'selected' : '') . ">$p->ket</option>";
                            }
                            echo "<option value='other'>lainnya</option>";
                            echo "</select>";
                            echo "<input type='text' name='ipal_sistem2' id='si2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                        } else {

                            foreach ($ipal as $p) {
                                echo "<option value='$p->ket'>$p->ket</option>";
                            }
                            echo "<option value='other' selected>lainnya</option>";
                            echo "</select>";
                            echo "<input type='text' name='ipal_sistem2' id='si2' value='$si' class='form-control input-sm has-feedback'/>";
                        }
                    } else {
                        foreach ($ipal as $val) {
                            echo "<option value='$val->ket'>$val->ket</option>";
                        } ?>
                    <option value="other">lainnya</option>
                </select>
                <input type="text" name="ipal_sistem2" id="si2" class="form-control input-sm has-feedback" style="display:none"/>
                    <?php } ?>
            </div>
            <!-- </div> -->
            <div class="col-lg-4">
                <label>Unit IPAL</label>
                <select name="ipal_unit[]" id="ipal_unit" class="form-control input-sm has-feedback multiselect" multiple="multiple">
                    <?php
                    if(isset($pencemaran_air)) {$arr_ipal_unit = explode(', ', $pencemaran_air->ipal_unit);}
                    foreach ($ipal_unit as $iu) {
                        echo "<option value='$iu->ket' " ;
                        if(isset($pencemaran_air)) {
                            for ($i = 0; $i < count($arr_ipal_unit); $i++) {

                                if ($arr_ipal_unit[$i] == $iu->ket) {
                                    echo "selected='true'";
                                }
                            }
                        }
                        echo ">$iu->ket</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-4">
                <label>Kapasitas IPAL</label>
                <input type="text" name="ipal_kapasitas" id="ipal_kapasitas" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->ipal_kapasitas : ''; ?>"/>
            </div>
        </div>
    </div>

    <div class="form-group ">
        <label>e. Perizinan</label>
        <select class="form-control input-sm" name="izin" id="izin">
            <?php
            foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if(isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->izin), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
                //$n++;
            }
            ?>
        </select>
    </div>

    <div class="form-group" name="detail_izin" id="detail_izin">

        <div class="row">
            <div class="col-sm-6">
                <label>Nomor</label>
                <input type="text" name="izin_no" id="izin_no" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->izin_no : ''; ?>"/>
            </div>
            <div class="col-sm-6">
                <label>Tanggal</label>
                <input type="text" name="izin_tgl" id="izin_tgl" class="form-control input-sm datepicker" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->izin_tgl : '';?>" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
            </div>
        </div>

    </div>

    <div class="form-group ">
        <label>Debit Izin (m3/hari)</label>
        <input type="text" name="izin_debit" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->izin_debit : '' ?>"/>
    </div>

    <div class="form-group ">
        <div class="row">
            <div class="col-lg-6">
                <label>Koordinat Outlet S</label>
                <div class="row">
                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_derajat_s : ''?>" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback" />
                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding: 0; margin:0;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_jam_s : ''?>" name="koord_outlet_jam_s" id="derajat_s" class="form-control input-sm has-feedback" />
                            <span class="input-group-addon">'</span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_menit_s : ''?>" name="koord_outlet_menit_s" id="derajat_s" class="form-control input-sm has-feedback" />
                            <span class="input-group-addon">"</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <label>Koordinat Outlet E</label>
                <div class="row">
                    <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_derajat_e : ''?>" name="koord_outlet_derajat_e" id="derajat_s" class="form-control input-sm has-feedback" />
                            <span class="input-group-addon"><?php echo '&deg;' ?></span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding: 0; margin:0;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_jam_e : ''?>" name="koord_outlet_jam_e" id="derajat_s" class="form-control input-sm has-feedback" />
                            <span class="input-group-addon">'</span>
                        </div>
                    </div>
                    <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                        <div class="input-group input-group-sm">
                            <input type="text" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->koord_outlet_menit_e : ''?>" name="koord_outlet_menit_e" id="derajat_s" class="form-control input-sm has-feedback" />
                            <span class="input-group-addon">"</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group ">
        <label>f. Alat Ukur</label>
        <select class="form-control input-sm" name="au" id="au">
            <?php
            foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if(isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->au), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
                //$n++;
            }
            ?>
        </select>
    </div>

    <div class="form-group" id="detail_au" name="detail_au">
        <div class="form-group ">
            <label>Jenis</label>
            <select class="form-control input-sm" id="je" name="au_jenis">
                <?php
                if(isset($pencemaran_air)) {
                    $auj = $pencemaran_air->au_jenis;
                    echo $auj;
                    if (($auj == "Kumulatif") || ($auj == "V-notch") || ($auj == "Digital")) {
                        foreach ($jenis_alat_ukur as $p) {
                            echo "<option value='$p' " . (($auj == $p) ? 'selected' : '') . ">$p</option>";
                        }
                        echo "</select>";
                        echo "<input type='text' name='au_jenis2' id='je2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                    } else {
                        $temp = "other";
                        foreach ($jenis_alat_ukur as $p) {
                            echo "<option value='$p' " . (($temp == $p) ? 'selected' : '') . ">$p</option>";
                        }
                        echo "</select>";
                        echo "<input type='text' name='au_jenis2' id='je2' value='$auj' class='form-control input-sm has-feedback'/>";
                    }
                } else { ?>
                <option value="Kumulatif">Kumulatif</option>
                <option value="V-notch">V-notch</option>
                <option value="Digital">Digital</option>
                <option value="other">lainnya</option>
            </select>
            <input type="text" name="au_jenis2" id="je2" class="form-control input-sm has-feedback" style="display:none"/>
                <?php } ?>
        </div>
        <div class="form-group ">
            <label>Ukuran (inchi)</label>
            <input type="text" name="au_ukuran" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->au_ukuran : ''?>"/>
        </div>
        <div class="form-group ">
            <label>Kondisi (apabila ada)</label>
            <select class="form-control input-sm" name="au_kondisi">
                <option value="Berfungsi" <?php echo isset($pencemaran_air) ? (($pencemaran_air->au_kondisi=="Berfungsi") ? 'selected' : ''):'';?>>Berfungsi</option>
                <option value="Tidak Berfungsi" <?php echo isset($pencemaran_air) ? (($pencemaran_air->au_kondisi=="Tidak Berfungsi") ? 'selected' : ''):'';?>>Tidak Berfungsi</option>
            </select>
        </div>
    </div>

    <div class="form-group ">
        <label>g. Catatan debit harian</label>
        <select class="form-control input-sm" name="cat_deb_hari">
            <?php foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if (isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->cat_deb_hari), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
            } ?>
        </select>
    </div>

    <div class="form-group ">
        <label>h. Daur Ulang</label>
        <select class="form-control input-sm" name="daur_ulang">
            <?php
            foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if (isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->daur_ulang), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
            }
            ?>
        </select>
    </div>

    <div class="form-group ">
        <label>Debit (m3/hari)</label>
        <input type="text" name="daur_ulang_debit" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->daur_ulang_debit:''?>" />
    </div>

    <div class="form-group ">
        <label>i. Pengujian Kualitas Air Limbah</label>
        <select class="form-control input-sm" name="uji_limbah" id="uji_limbah">
            <?php
            foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if (isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->uji_limbah), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
            }
            ?>
        </select>
    </div>

    <div class="form-group" name="detail_uji" id="detail_uji">
        <div class="form-group ">
            <label>Periode Pengujian</label>
            <select class="form-control input-sm" name="uji_period" id="pp">
                <?php
                if (isset($pencemaran_air)) {
                    $pu = $pencemaran_air->uji_period;
                    if (($pu == "Setiap Bulan")) {
                        foreach ($periode_uji as $p) {
                            echo "<option value='$p' " . (($pu == $p) ? 'selected' : '') . ">$p</option>";
                        }
                        echo "</select>";
                        echo "<input type='text' name='uji_period2' id='pp2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                    } else {
                        $temp = "other";
                        foreach ($periode_uji as $p) {
                            echo "<option value='$p' " . (($temp == $p) ? 'selected' : '') . ">$p</option>";
                        }
                        echo "</select>";
                        echo "<input type='text' name='uji_period2' id='pp2' value='$pu' class='form-control input-sm has-feedback'/>";
                    }
                } else { ?>
                <option value="Setiap Bulan" selected="true">Setiap Bulan</option>
                <option value="other">lainnya</option>
            </select>
            <input type="text" name="uji_period2" id="pp2" class="form-control input-sm has-feedback" style="display:none"/>
                <?php } ?>
        </div>

        <div class="form-group ">
            <label>Laboratorium Pengujian</label>
            <select name="uji_lab" class="form-control input-sm">
                <?php
                foreach ($lab as $l) {
                    echo "<option value='$l->nama_lab'";
                    if (isset($pencemaran_air)) {
                        if ($pencemaran_air->uji_lab == $l->nama_lab ) {
                            echo "selected";
                        }
                    }
                    echo ">".$l->nama_lab."</option>";
                }
                ?>
            </select>
        </div>

        <div class="form-group ">
            <label>Hasil Pengujian</label>
            <select class="form-control input-sm" name="uji_hasil" id="uji_hasil">
                <?php
                foreach ($bool as $f) {
                    echo "<option value='".penuhConv($f)."' " ;
                    if (isset($pencemaran_air)) {
                        if (strcasecmp($pencemaran_air->uji_hasil, penuhConv($f)) == 0) {
                            echo "selected='true'";
                        }
                    }
                    echo ">".penuhConv($f)."</option>";
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group ">
        <label>Apabila tidak memenuhi baku mutu, terjadi pada bulan</label>
        <input type="text" name="uji_tidak_bulan" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->uji_tidak_bulan:''?>" />
    </div>

    <div class="form-group ">
        <label>Parameter yang tidak memenuhi baku mutu</label>
        <input type="text" name="uji_tidak_param" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->uji_tidak_param:''?>"/>
    </div>

    <?php
    $lapor = $bool;
    $lapor[] = '2';
    ?>

    <div class="form-group ">
        <label>Pelaporan</label>
        <select class="form-control input-sm" name="uji_lapor">
            <?php
            for($d=0; $d<3; $d++){
                echo "<option value='".rutinConv($lapor[$d])."'";
                if (isset($pencemaran_air)) {
                    if (strcasecmp($pencemaran_air->uji_lapor, rutinConv($lapor[$d])) == 0) {
                        echo "selected";
                    }
                }
                echo ">".rutinConv($lapor[$d])."</option>";
            }
            ?>
        </select>
    </div>

    <div class="form-group ">
        <label>j. Bahan Kimia Pembantu IPAL</label>
        <table class="table table-th-block">
            <thead>
            <tr>
                <th class="text-center">Nama Bahan Kimia</th>
                <th class="text-center">Jumlah Pemakaian (ton/hari)</th>
                <th class="text-center"></th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($bahan_kimia_ipal)) { $x=1; foreach($bahan_kimia_ipal as $bki){ ?>
                <tr data-duplicate="kimia-ipal" data-duplicate-min="1">
                    <td class="col-lg-5"><input type="text" name="bkpi_nama[]" value="<?php echo $bki->nama?>" class="form-control input-sm has-feedback" /></td></td>
                    <td class="col-lg-5"><input type="text" name="bkpi_jml[]" value="<?php echo $bki->jml_pemakaian?>" class="form-control input-sm has-feedback" /></td>
                    <td class="col-lg-2">
                        <div class=" text-right">
                            <div class="btn-group" style="padding-right: 17px;">
                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="kimia-ipal"><i class="fa fa-plus-circle"></i></button>
                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="kimia-ipal"><i class="fa fa-times-circle"></i></button>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php $x++;
            } } else { ?>
                <tr data-duplicate="kimia-ipal" data-duplicate-min="1">
                    <td class="col-lg-5"><input type="text" name="bkpi_nama[]" class="form-control input-sm has-feedback" /></td></td>
                    <td class="col-lg-5"><input type="text" name="bkpi_jml[]" class="form-control input-sm has-feedback" /></td>
                    <td class="col-lg-2">
                        <div class=" text-right">
                            <div class="btn-group" style="padding-right: 17px;">
                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="kimia-ipal"><i class="fa fa-plus-circle"></i></button>
                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="kimia-ipal"><i class="fa fa-times-circle"></i></button>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php }
            ?>
            </tbody>
        </table>
    </div>

    <div class="form-group ">
        <label>k. Kebocoran/Rembesan/Tumpahan/Bypass</label>
        <select class="form-control input-sm" name="bocor" id="bocor">
            <?php
            foreach ($bool as $f) {
                echo "<option value='".adaConv($f)."' " ;
                if (isset($pencemaran_air)) {
                    if (strcasecmp(adaConv($pencemaran_air->bocor), adaConv($f)) == 0) {
                        echo "selected='true'";
                    }
                }
                echo ">".adaConv($f)."</option>";
            }
            ?>
        </select>
    </div>

    <div class="form-group " id="lok" >
        <label>Lokasi</label>
        <input type="text" name="bocor_lokasi" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->bocor_lokasi:''?>" />
    </div>

    <div class="form-group ">
        <label>l. Lain-lain</label>
        <input type="text" name="lain_lain_pa" id="" class="form-control input-sm has-feedback" value="<?php echo isset($pencemaran_air) ? $pencemaran_air->lain_lain:''?>"/>
    </div>

    <div class="row">
        <div class="col-sm-4 text-left">
            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Data Umum</a>
        </div>
        <div class="col-sm-4 text-center">
            <input id="save-pencemaran_air" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Air"/>
        </div>
        <div class="col-sm-4 text-right">
            <a class="btn btn-warning NextStep">Pencemaran Udara<i class="fa fa-angle-right"></i></a>
        </div>
    </div>
</div>