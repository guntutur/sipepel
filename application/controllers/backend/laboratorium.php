<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class laboratorium extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_laboratory', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_laboratory');
        $this->load->view('include/footer');
    }
	
	public function get_list() {

		$input = array('dataperpage', 'query', 'curpage');

		foreach ($input as $val)
     		$$val = $this->input->post($val);

		$where = array(
				'kategori_lab' => array('SEARCH', 'OR', $query),
				'no_lab' => array('SEARCH', 'OR', $query),
				'nama_lab' => array('SEARCH', 'OR', $query),
				'alamat' => array('SEARCH', 'OR', $query),
				'tlp' => array('SEARCH', 'OR', $query),
				'email' => array('SEARCH', 'OR', $query),
				'fax' => array('SEARCH', 'OR', $query)
			);

	
    	$total = $this->m_laboratory->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $labs = $this->m_laboratory->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $labs,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

		$data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

		echo json_encode($data);
		exit();

	}

	public function get() {			
		echo json_encode($this->m_laboratory->get_laboratory_by_id($this->input->post('id_lab')));
		exit();
	}
	
	public function edit() {

		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'nama_lab' => $this->input->post('lab_name'),
			'no_lab' => $this->input->post('no_lab'),
			'no_akreditasi' => $this->input->post('no_akreditasi'),
			'tlp' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'alamat' => $this->input->post('address'),
			'fax' => $this->input->post('fax'),
			'kategori_lab' => $this->input->post('kategori_lab'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_laboratory->change_laboratory($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
				
		redirect('backend/laboratorium');
	}
	
	public function register() {

		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'nama_lab' => $this->input->post('lab_name'),
				'no_lab' => $this->input->post('no_lab'),
				'no_akreditasi' => $this->input->post('no_akreditasi'),
				'tlp' => $this->input->post('phone'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('address'),
				'fax' => $this->input->post('fax'),
				'kategori_lab' => $this->input->post('kategori_lab'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_laboratory->insert_laboratory($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		redirect('backend/laboratorium');
	}
	
	public function delete() {		
		$this->m_laboratory->delete_laboratory($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/laboratorium');
	}

	public function validate() {

		$data = array(
			'no_lab' => $this->input->post('no_lab')
		);

		# check data yang duplicate
		if($this->m_laboratory->is_duplicate($data)) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah ada'));
			redirect('backend/laboratorium');
		}

		return true;
	}
}
