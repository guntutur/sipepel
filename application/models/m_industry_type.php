<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_industry_type extends CI_Model {

	private $table_name			= 'jenis_industri';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('p.id_jenis_industri, p.ket, p.status, p.tgl_pembuatan, u.ket as nm_usaha_kegiatan, (select count(*) from industri d where d.id_jenis_industri = p.id_jenis_industri) as jumlah_industri')->from($this->table_name .' as p');
		$this->db->join('usaha_kegiatan u', 'u.id_usaha_kegiatan = p.id_usaha_kegiatan', 'left');

		/*foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}*/
		
		if($where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("u.ket", "asc");
		$this->db->order_by("p.ket", "asc"); 
		$this->db->order_by("p.tgl_pembuatan", "desc"); 

		$query = $this->db->get();

		// $query = $this->db->query("select * from jenis_industri u order by u.tgl_pembuatan desc");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		//$this->db->select('*')->from($this->table_name);
		
		$this->db->select('p.id_jenis_industri, p.ket, p.status, p.tgl_pembuatan, u.ket as nm_usaha_kegiatan, (select count(*) from industri d where d.id_jenis_industri = p.id_jenis_industri) as jumlah_industri')->from($this->table_name .' as p');
		$this->db->join('usaha_kegiatan u', 'u.id_usaha_kegiatan = p.id_usaha_kegiatan', 'left');
		
		/*foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}*/
		
		if($where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}

	function get_dropdown() {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("ket", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_industry_type_by_id($id) {
		$this->db->where('id_jenis_industri', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_industry_type($id) {
		$data = $this->get_industry_type_by_id($id);
		$this->db->where('id_jenis_industri', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_industry_type($data, $id) {

		$this->db->where("id_jenis_industri", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_industry_type($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate($name, $id_uk) {
		$this->db->where('ket', $name);
		$this->db->where('id_usaha_kegiatan', $id_uk);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	function get_it_by_uk($id) {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("ket", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 
		if($id) {
			$this->db->where("id_usaha_kegiatan", $id);
		}

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	function get_id_by_usaha_kegiatan($id) {
		$query = $this->db->query('select id_jenis_industri, ket 
									from jenis_industri
									where status = 1 AND id_usaha_kegiatan = "'.$id.'"
									union 
									select ket as id_jenis_industri, ket
									from data_master_detail 
									where jenis_data_master = "jenis_bahan_bakar"
									order by ket asc');
        $num = $query->num_rows();
        if($num>0){
            return $query->result();
        }
        else{
            return 0;
        }
	}
}
?>