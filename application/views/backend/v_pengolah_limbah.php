<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">PERUSAHAAN PENGOLAH LIMBAH B3&nbsp;&nbsp;<small>mengelola seluruh data perusahaan pengolah limbah B3</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Data Pengolah Limbah B3</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#myModal" onclick="$(this).reset_form('myModal');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Perusahaan</th>
                                <th>Nomer Pendaftaran KLH</th>
                                <th>Kontak Kami</th>                                
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/pengolah_limbah/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        var nama = (dt.badan_hukum) ? dt.badan_hukum+' ' : '';
                        t += '<td><b>'+nama+dt.nama_industri+'</b><br/>';
						t += '<small>' + dt.alamat + '</small><br/>';
						t += '<small>Kec. ' + dt.nama_kecamatan + ' Kel. '+ dt.nama_kelurahan +'</small><br/>';
						t += '</td>';
						t += '<td>'+dt.nomer_klh+'</td>';
                        t += '<td>';					
                        t += '<i class="fa fa-phone-square icon-sidebar"></i>&nbsp;&nbsp;&nbsp; '+dt.tlp+'<br/> ';
                        t += '<i class="fa fa-fax icon-sidebar"></i>&nbsp;&nbsp;&nbsp; '+dt.fax+' <br/> ';
                        t += '<i class="fa fa-envelope icon-sidebar"></i>&nbsp;&nbsp;&nbsp;'+dt.email+'';  
                        t += '</td>';

                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_industri+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_industri+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        
        $("#form_add").validate({
            rules: {
                nama_industri: {
                    required: true,
                    maxlength: 250
                },
                tlp:{
                    required: true,
                    maxlength: 90
                },
                email: {
                    required: true,
                    maxlength: 90
                },
                alamat: {
                    required: true,
                    maxlength: 90
                },
                fax: {
                    required: true,
                    maxlength: 90
                },				
                usaha_kegiatan: {
                    required: true,
                },
				nama_kecamatan: {
                    required: true,
                },
				nama_kelurahan: {
                    required: true,
                }
            },
            messages: {        
                nama_industri: {
                    required: "Field tidak boleh kosong.",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 250 karakter"
                },
                tlp: {
                    required: "Field tidak boleh kosong.",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 250 karakter"
                },
                email: {
                    required: "Field tidak boleh kosong.",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 250 karakter"
                },
                alamat: {
                    required: "Field tidak boleh kosong.",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 250 karakter"
                },
                fax: {
                    required: "Field tidak boleh kosong.",
                    maxlength: "Nama usaha/kegiatan minimal memiliki 250 karakter"
                },               
                usaha_kegiatan: {
                    required: "Field belum dipilih."
                },
				nama_kecamatan: {
                    required: "Field belum dipilih."
                },
				nama_kelurahan: {
                    required: "Field belum dipilih."
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
       
        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/pengolah_limbah/get'); ?>",
                data: 'id_industri=' + id,
                success: function (response) {
                    console.log(response);
                    $('input[id=edit_id]').val(response.id_industri);

                    $('input[name=nama_industri]').val(response.nama_industri);
                    //$('select[name=nama_industri]').chosen();
                    //$('select[name=nama_industri]').val(response.id_industri);
                    //$('select[name=nama_industri]').trigger("chosen:updated");

					$('select[name=badan_hukum]').val(response.badan_hukum);
                    $('input[name=tlp]').val(response.tlp);
                    $('input[name=email]').val(response.email);
                    $('textarea[name=alamat]').val(response.alamat);
                    $('input[name=fax]').val(response.fax);
					$('input[name=nomer_klh]').val(response.fax);              
					$('select[name=nama_kecamatan]').val(response.id_kecamatan);
                    $(this).load_kelurahan(response.id_kecamatan, response.id_kelurahan);

                    $('#myModal').modal('show');
                }
            })
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }
		
		$('select[name=nama_kelurahan]').prop('disabled', true);		
		$('#nama_kecamatan').change(function() {
			var id_kecamatan = $(this).val();
            $(this).load_kelurahan(id_kecamatan);			
		});

        $.fn.load_kelurahan = function(id_kecamatan, id_kel) {                        
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/kelurahan/get_by_kecamatan'); ?>",
                data: 'id_kecamatan=' + id_kecamatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {                            
                            opt += '<option value="'+v.id_kelurahan+'" '+ ((v.id_kelurahan == id_kel) ? "selected" : "") +'>'+v.ket+'</option>';
                        });
                        $('select[name=nama_kelurahan]').prop('disabled', false);
                    }else{
                        $('select[name=nama_kelurahan]').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#nama_kelurahan").html(opt);
                }
            });
        }
		
		$('select[name=jenis_industri]').prop('disabled', true);		
		$('#usaha_kegiatan').change(function() {
			var id_usaha_kegiatan = $(this).val();
			$(this).load_jenis_industri(id_usaha_kegiatan);
		});

        $.fn.load_jenis_industri = function(id_usaha_kegiatan, id_jenis_industri) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/jenis_industri/get_by_usaha_kegiatan'); ?>",
                data: 'id_usaha_kegiatan=' + id_usaha_kegiatan,
                success: function(resp) {
                    var opt = '';
                    if(resp.length > 0) {
                        $.each(resp, function(k, v) {
                            opt += '<option value="'+v.id_jenis_industri+'" '+ ((v.id_jenis_industri == id_jenis_industri) ? "selected" : "") +'>'+v.ket+'</option>';
                        });
                        $('select[name=jenis_industri]').prop('disabled', false);
                    }else{
                        $('select[name=jenis_industri]').prop('disabled', true);
                        opt += '<option value="" selected disabled>- no data -</option>';
                    }
                    $("#jenis_industri").html(opt);
                }
            });
        }

        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Perusahaan Pengolah Limbah B3</h4>
            </div>
            <form role="form" data-action="<?php echo base_url('/backend/pengolah_limbah') ?>" method="post" id="form_add">
                <div class="modal-body">                    
					<div class="row">
						<!-- <div class="col-lg-3 form-group badan_hukum">
							<label>Badan Usaha</label>
							<input type="text" name="badan_hukum" id="badan_hukum" class="form-control input-sm has-feedback" autofocus />							             
						</div> -->
                        <div class="col-lg-3 form-group badan_hukum">
                            <label>Badan Usaha</label>
                            <!-- <input type="text" name="badan_hukum" id="badan_hukum" class="form-control input-sm has-feedback" autofocus />                                    -->
                            <select name="badan_hukum" id="badan_hukum" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Badan Usaha --</option>
                                <?php foreach($badan_usaha as $uk): ?>
                                    <option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>     
                        </div>
						<div class="col-lg-9 form-group nama_industri">
							<label>Nama Perusahaan</label>
							<input type="text" name="nama_industri" id="nama_industri" class="form-control input-sm has-feedback" autofocus />							   
						</div>                        
					</div>
                    <div class="row">
                        <div class="col-lg-6 form-group tlp">
                            <label>Telepon</label>
                            <input type="text" name="tlp" id="tlp" class="form-control has-feedback input-sm numberonly" autofocus />                                         
                        </div>
                        <div class="col-lg-6 form-group fax">
                            <label>Fax</label>
                            <input type="text" name="fax" id="fax" class="form-control has-feedback input-sm numberonly" autofocus />                                         
                        </div>
                    </div>
                    <div class="form-group email">
                        <label>Email</label>
                        <input type="text" name="email" id="email" class="form-control input-sm has-feedback" autofocus />                                     
                    </div>
					<div class="form-group nomer_klh">
                        <label>Nomer Pendaftaran KLH</label>
                        <input type="text" name="nomer_klh" id="nomer_klh" class="form-control input-sm has-feedback" autofocus />                                     
                    </div>
					<div class="row">
						<div class="col-lg-6 form-group nama_kecamatan">
                            <label>Kecamatan</label>
                            <select name="nama_kecamatan" id="nama_kecamatan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Kecamatan --</option>
                                <?php foreach($kecamatan as $uk): ?>
                                    <option value="<?php echo $uk->id_kecamatan;?>"><?php echo $uk->ket; ?></option>
                                <?php endforeach;?>                             
                            </select>                        
                        </div>
                        <div class="col-lg-6 form-group nama_kelurahan">
                            <label>Kelurahan</label>
                            <select name="nama_kelurahan" id="nama_kelurahan" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Kelurahan --</option>                                                       
                            </select>                            
                        </div>  
                    </div>
                    <div class="form-group alamat">
                        <label>Alamat</label>
                        <textarea rows="4" name="alamat" id="alamat" class="form-control input-sm has-feedback" autofocus ></textarea>                                     
                    </div>                                                                  
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data Perusahaan Pengolah Limbah B3</h4>
            </div>
            <form action="<?php echo base_url('/backend/pengolah_limbah/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->