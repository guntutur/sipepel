<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class kelurahan extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_kelurahan', '', TRUE);
		$this->load->model('m_kecamatan', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        
		
		# response paramter        
		$data['kecamatan'] = $this->m_kecamatan->get_dropdown();
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_kelurahan', $data);
        $this->load->view('include/footer');
    }

	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $kec = $this->m_kecamatan->get_kecamatan_by_id($id);	

        $data['title'] = "KELURAHAN/DESA DI KECAMATAN ".$kec->ket;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_kelurahan', $data);
        $this->load->view('include/footer');
	}
	
    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 
     
		$where = "kel.ket LIKE '%". $query ."%' AND kel.id_kecamatan = ".$id;

     	$total = $this->m_kelurahan->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_kelurahan->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_kelurahan->get_kelurahan_by_id($this->input->post('id_kelurahan')));
		exit();
	}
	
	public function get_by_kecamatan() {
		echo json_encode($this->m_kelurahan->get_kelurahan_by_kecamatan($this->input->post('id_kecamatan')));
		exit();
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			//'id_kecamatan' => $this->input->post('kecamatan'),
			'ket' => $this->input->post('description'),
			'status' => $this->input->post('status'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_kelurahan->change_kelurahan($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function register() {

		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");		
			$param = array(
				'id_kecamatan' => $this->input->post('id'),
				'ket' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => '', # $this->tank_auth->get_personname(),
				'tgl_update' => '' # $tgl_update
			);
			
			if($this->m_kelurahan->insert_kelurahan($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function delete() {
		$this->m_kelurahan->delete_kelurahan($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function validate() {

		$data = array(
			'ket' => $this->input->post('description'),
			'id_kecamatan' => $this->input->post('id')
		);

		# check data yang duplicate
		if($this->m_kelurahan->is_duplicate($data)) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah ada'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}
}
