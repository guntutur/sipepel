<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class data_master extends CI_Controller {

	function __construct() {
        parent::__construct();      

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        # load model
        $this->load->model('m_data_master');
		
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');


    	$this->load->view('include/header');
        $this->load->view('backend/v_data_master');
        $this->load->view('include/footer');
    }

   public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				
			);

     	$total = $this->m_data_master->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_data_master->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_data_master->get_by_id($this->input->post('id_data_master')));
		exit();
	}

    public function register() {       

        date_default_timezone_set("Asia/Jakarta");
        $tgl_pembuatan = date("Y-m-d H:i:s");
		
		if($this->validate()) {
			$param = array(                
					'value' => $this->input->post('value'),
					'ket' => $this->input->post('ket')
					/*'tgl_pembuatan' => $tgl_pembuatan,
					'tgl_update' => $tgl_pembuatan,
					'dibuat_oleh' =>$this->tank_auth->get_personname(),
					'diupdate_oleh' => $this->tank_auth->get_personname()*/
				);
			
			
			if($this->m_data_master->insert($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
			
		}
		
		redirect('backend/data_master');

    }
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		
		if($this->validate()) {
						$param = array(
				'value' => $this->input->post('value'),
				'ket' => $this->input->post('ket')
				/*'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update*/
			);
			
			if($this->m_data_master->change($param, $this->input->post('id')))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		}
		
		redirect('backend/data_master');
	}
	
	public function delete() {
		$this->m_data_master->delete($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/data_master');
	}
	
	public function validate() {
		# check data yang duplicate
		if($this->m_data_master->is_duplicate($this->input->post('value'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah ada'));
			redirect('backend/data_master');
		}

		return true;
	}
	
	
	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $izin = $this->m_data_master->get_data_master_detail_by_id($id);

        $jenis = $izin->ket;

        $data['title'] = "DATA ACUAN ".$jenis;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_data_master_detail', $data);
        $this->load->view('include/footer');
	}
	
	public function get_detail() {		
		echo json_encode($this->m_data_master->get_detail_item_by_id($this->input->post('id_data_master_detail')));
		exit();
	}

	public function get_detail_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		/*$id_query = $id;*/
        /*$baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($id_query);*/

		
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = "jenis_data_master = '".$id."'" ;
    
     	$total = $this->m_data_master->get_detail_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_data_master->get_detail_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }

	public function insert_detail() {
		if($this->validate_detail()) {
			$param = array(
					'status' => $this->input->post('status'),
					'ket' => $this->input->post('ket'),
					'jenis_data_master' => $this->input->post('id')
				);

			if($this->m_data_master->insert_detail($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		// redirect('backend/debit_air_limbah/detail/'.$this->input->post('id')."/".);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function edit_detail() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		if($this->validate_detail()) {
			$param = array(			
					'status' => $this->input->post('status'),
					'ket' => $this->input->post('ket')
				);
			
			if($this->m_data_master->change_detail_data_master($param, $this->input->post('id')))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_detail() {
		$this->m_data_master->delete_detail_data_master($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	
	public function validate_detail() {
		# check data yang duplicate
		if($this->m_data_master->is_duplicate_detail($this->input->post('id'), $this->input->post('ket'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah ada'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}
	
	
}
?>