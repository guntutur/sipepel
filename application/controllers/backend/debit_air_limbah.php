<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class debit_air_limbah extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_debit_air_limbah', '', TRUE);
		$this->load->model('m_industry', '', TRUE);
        
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types        
		$industri = $this->m_industry->get_dropdown();		
		
		# response paramter        
		$data['industri'] = $industri;
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_debit_air_limbah', $data);
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'd.laporan_bulan' => array('SEARCH', 'OR', $query),
				'i.nama_industri' => array('SEARCH', 'OR', $query),
				'd.metode_sampling' => array('SEARCH', 'OR', $query)
			);

     	$total = $this->m_debit_air_limbah->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_debit_air_limbah->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_debit_air_limbah->get_debit_air_limbah_by_id($this->input->post('id_debit_air_limbah')));
		exit();
	}
	
	public function edit() {
		
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");

		// $o = $this->m_debit_air_limbah->is_duplicate($this->input->post('industri'), $this->input->post('lap_bln'));    
  //       if($o and ($o->laporan_bulan == $this->input->post('lap_bln'))) {
		$param = array(
			'id_industri' => $this->input->post('industri'),			
			'metode_sampling' => $this->input->post('metode_sampling'),
			'laporan_bulan' => $this->input->post('lap_bln'),
			'min' => $this->input->post('min'),
			'max' => $this->input->post('max'),
			'rerata' => $this->input->post('rerata'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_debit_air_limbah->change_debit_air_limbah($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		// }else{
  //           $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia.<br/>Silahkan ulangi proses input data.'));
  //       }
			
		redirect('backend/debit_air_limbah');
	}
	
	public function register() {		

		date_default_timezone_set("Asia/Jakarta");
		$tgl_pembuatan = date("Y-m-d H:i:s:");
		$tgl_update = date("Y-m-d H:i:s:");

		$o = $this->m_debit_air_limbah->is_duplicate($this->input->post('industri'), $this->input->post('lap_bln'));    
        if($o) {
			$param = array(
				'id_industri' => $this->input->post('industri'),			
				'metode_sampling' => $this->input->post('metode_sampling'),
				'laporan_bulan' => $this->input->post('lap_bln'),
				'min' => $this->input->post('min'),
				'max' => $this->input->post('max'),
				'rerata' => $this->input->post('rerata'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_debit_air_limbah->insert_debit_air_limbah($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}else{
            $this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia.<br/>Silahkan ulangi proses input data.'));
        }

		redirect('backend/debit_air_limbah');
	}
	
	public function delete() {
		$this->m_debit_air_limbah->delete_debit_air_limbah($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/debit_air_limbah');
	}

	public function upload() {
		$id = $this->input->post('id');

		$d = $this->m_debit_air_limbah->get_debit_air_limbah_by_id($id);
		$bln = str_replace('.', '', $d->laporan_bulan);
		$i = $this->m_industry->get_industry_by_id($d->id_industri);		

		if($d) {

			$temp = pathinfo($_FILES["userfile"]["name"]);
			$path = "assets/data_debit_air_limbah/";
			$newfilename = str_replace(' ', '_', strtolower(trim($i->nama_industri))).'_'.$bln.".".$temp['extension'];

			$config['upload_path'] = realpath($path);
			$config['allowed_types'] = 'jpg|JPG|jpeg|JPEG|png';
			$config['max_size'] = '8000';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $newfilename;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload()) {				
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', $this->upload->display_errors()));
			}else{
				$param = array('nama_berkas' => $newfilename);
				$this->m_debit_air_limbah->change_debit_air_limbah($param, $id);
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Berkas berhasil diunggah'));
			}
		}

		redirect($_SERVER['HTTP_REFERER']);
	}

	/* ------------------------------ detail ------------------------------ */

	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $debit_air_limbah = $this->m_debit_air_limbah->get_debit_air_limbah_by_id($id);


        $tgl = explode('.', $debit_air_limbah->laporan_bulan);
        $bulan = $this->functions->get_month((int)$tgl[0]);
        $tahun = $tgl[1];

        $data['title'] = "DEBIT DETAIL BULAN ".strtoupper($bulan)." ".$tahun;
        $data['date'] = ucfirst($bulan)." ".$tahun;

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_detail_debit', $data);
        $this->load->view('include/footer');
	}

	public function get_detail() {		
		echo json_encode($this->m_debit_air_limbah->get_detail_debit_air_limbah_by_id($this->input->post('id_debit')));
		exit();
	}

	public function get_detail_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = "tgl_debit LIKE '%". $query ."%' AND id_debit_air_limbah = ".$id;
    
     	$total = $this->m_debit_air_limbah->get_detail_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_debit_air_limbah->get_detail_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }

	public function insert_detail() {
		$param = array(
				'id_debit_air_limbah' => $this->input->post('id'),
				'debit' => $this->input->post('debit'),
				'tgl_debit' => date('Y-m-d', strtotime($this->input->post('tgl_debit')))
			);

		
		$valid = $this->m_debit_air_limbah->is_duplicate_detail($param);
		if(count($valid) == 0) {
			if($this->m_debit_air_limbah->insert_detail($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}else{
			$this->session->set_flashdata('msg', $this->functions->build_message('warning', 'Data yang dimasukkan sudah ada'));
		}		
			
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function edit_detail() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(			
				'id_debit' => $this->input->post('id'),
				'id_debit_air_limbah' => $this->input->post('id_debit'),
				'debit' => $this->input->post('debit'),
				'tgl_debit' => date('Y-m-d', strtotime($this->input->post('tgl_debit')))
			);

		$valid = $this->m_debit_air_limbah->is_duplicate_detail($param);
		if(count($valid) == 0) {
			if($this->m_debit_air_limbah->change_detail_debit_air_limbah($param, $this->input->post('id')))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		}else{
			$this->session->set_flashdata('msg', $this->functions->build_message('warning', 'Data yang dimasukkan sudah ada'));
		}
			
		redirect($_SERVER['HTTP_REFERER']);		
	}

	public function delete_detail() {
		$this->m_debit_air_limbah->delete_detail_debit_air_limbah($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	/* ------------------------------ / detail ------------------------------ */
}
