<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
 // Document   : m_bap.php 
 // Created on : November 12, 2014 
 // Author     : Faridzi 
 // Description: Model for BAP SPPBE 

class m_bap_sppbe extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * Insert to table
	 *
	 * @param	String, object
	 * @return	boolean
	 */
	function insert($table_name, $data) {
		$this->db->insert($table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function get_all() {
		$data = array();

		$query = $this->db->query("SELECT a.id_bap, c.nama_industri, b.bap_tgl, b.compare_status, b.is_compared, COUNT(d.id_bap) AS jml_teguran
									FROM bap_sppbe a
									LEFT JOIN bap b ON a.id_bap = b.id_bap
									LEFT JOIN history_item_teguran_bap d ON b.id_bap = d.id_bap
									INNER JOIN industri c ON b.id_industri = c.id_industri
									GROUP BY b.id_bap
									ORDER BY a.id_bap_sppbe ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function delete_item_ketaatan($id_bap) {
		$query = $this->db->query("DELETE FROM history_item_teguran_bap where id_bap = '$id_bap'");

		if ($query > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}


?>