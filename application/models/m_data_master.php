<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_data_master extends CI_Model {

	private $table_name			= 'data_master';			// industry type
	private $table_detail		= 'data_master_detail';
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('al.*, (select count(*) from data_master_detail dd where dd.jenis_data_master = al.value) as jumlah');
		$this->db->from($this->table_name .' as al');
	

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("al.ket", "desc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select('al.*, (select count(*) from data_master_detail dd where dd.jenis_data_master = al.value) as jumlah');
		$this->db->from($this->table_name .' as al');
		

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	function get_detail_data_master($where=array()) {
		$data = array();

		$this->db->select('*');
		$this->db->from($this->table_detail);
		$this->db->where($where);		

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_by_id($id) {
		$this->db->where('id_data_master', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete($id) {
		$data = $this->get_by_id($id);
		$this->db->where('id_data_master', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change($data, $id) {

		$this->db->where("id_data_master", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function is_duplicate($value) {
		$this->db->where('value', $value);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	
	function get_data_master_detail_by_id($id) {
		$whr1 = 'p.value = "'.$id.'"';
		$query = $this->db->query('select p.*
									from data_master as p 
									where '.$whr1.'  AND 1=1' );
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}	
	
	function get_detail_all($where='', $limit=0, $offset=0) {

		$data = array();

		$this->db->select('*');
		$this->db->from('data_master_detail');		

		if($where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		 
		
		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	function get_detail_num_rows($where) {

		$this->db->select('*');
		$this->db->from('data_master_detail');		

		if($where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}
	
	function get_detail_item_by_id($id) {
		$this->db->where('id_data_master_detail', $id);
		
		$query = $this->db->get('data_master_detail');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function delete_detail_data_master($id) {
		$data = $this->get_detail_item_by_id($id);
		$this->db->where('id_data_master_detail', $id);
		$this->db->delete('data_master_detail');
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	function change_detail_data_master($data, $id) {

		$this->db->where("id_data_master_detail", $id);
		$this->db->update('data_master_detail', $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_detail($data){
		$this->db->insert('data_master_detail', $data);
		
		return $this->db->affected_rows() > 0;
	}
	
	function is_duplicate_detail($id, $ket) {
		$this->db->where('jenis_data_master', $id);
		$this->db->where('ket', $ket);
		$this->db->from('data_master_detail');
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	function get_dropdown($jenis_data_master) {
		$data = array();
		$this->db->select('*')->from('data_master_detail');
		$this->db->where('jenis_data_master', $jenis_data_master);
		$this->db->where('status', 1);
		$this->db->order_by("ket", "asc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

}
?>