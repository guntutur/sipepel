<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">DEBIT AIR LIMBAH&nbsp;&nbsp;<small>controling permission</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>SWAPANTAU</li>
                <li class="active">Debit Air Limbah</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">         

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#debit_air_limbah" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Industri</th>
                                <th>Laporan Bulan</th>                                
                                <th>Metode Sampling</th>						
                                <th>Minimum</th>
                                <th>Maksimum</th>
                                <th>Rata-Rata</th>
                                <!-- <th>Jumlah Data</th> -->
                                <th>Berkas</th>
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm" style="margin: 0;">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/debit_air_limbah/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];                        
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        t += '<td>'+dt.nama_industri+'</td>';

                        var tmp = dt.laporan_bulan.split(".");                        

                        t += '<td>'+$(this).get_month(parseInt(tmp[0]))+' '+ tmp[1] +'</td>';
                        t += '<td>'+dt.metode_sampling.charAt(0).toUpperCase() + dt.metode_sampling.slice(1)+'</td>';
                        t += '<td class="text-center">'+$(this).return_value(dt.min, true)+'</td>';
                        t += '<td class="text-center">'+$(this).return_value(dt.max, true)+'</td>';                        
                        t += '<td class="text-center">'+$(this).return_value(dt.rerata, true)+'</td>';                                              
                        var link = '<?php echo base_url("assets/data_debit_air_limbah/'+dt.nama_berkas+'"); ?>';
                        t += '<td><a href="'+link+'">'+ $(this).return_value(dt.nama_berkas) +'</a></td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        // t += '<li><a href="' + '<?php echo base_url("backend/debit_air_limbah/detail/'+ dt.id_debit_air_limbah + '/' + parseInt(tmp[0]) + '/' + parseInt(tmp[1]) +'"); ?>' + '">Tambah Detail</a></li>';
                        t += '<li><a onclick="$(this).upload_berkas('+dt.id_debit_air_limbah+')">Unggah Berkas</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_debit_air_limbah+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_debit_air_limbah+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }    

    $(document).ready(function () {

        $.fn.upload_berkas = function(id) {
            $('#upload_id').val(id);
            $('#upload').modal('show');
        }

        $.fn.edit = function(id) {
            $.ajax({
                url: "<?php echo base_url('/backend/debit_air_limbah/get'); ?>",
                type: 'POST',
                dataType: 'json',
                data: $.param({id_debit_air_limbah: id}),
                success: function(resp) {

                    $(this).reset_form('debit_air_limbah');                    
                 
                    $('input[id=edit_id]').val(resp.id_debit_air_limbah);
                    $('input[name=lap_bln]').val(resp.laporan_bulan);
                    $('input[name=min]').val(resp.min);
                    $('input[name=max]').val(resp.max);
                    $('input[name=rerata]').val(resp.rerata);

                    // $('select[name=industri]').val(resp.id_industri);
                    $('select[name=industri]').chosen();
                    $('select[name=industri]').val(resp.id_industri);
                    $('select[name=industri]').trigger("chosen:updated");

                    $('select[name=metode_sampling]').val(resp.metode_sampling);                    

                    $('#debit_air_limbah form').attr('action', '<?php echo base_url('/backend/debit_air_limbah/edit') ?>'); //this fails silently
                    $('#debit_air_limbah form').get(0).setAttribute('action', '<?php echo base_url('/backend/debit_air_limbah/edit') ?>'); //this works

                    $('#debit_air_limbah').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            });            
        }

        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }

        $.validator.setDefaults({ ignore: ":hidden:not(select)" });
        $("#form_add").validate({
            rules: {                            
                metode_sampling: { required: true },
                lap_bln: { required: true },
                min: { required: true },
                max: { required: true },
                industri: { required: true },
                rerata: { required: true }
            },
            messages: {                               
                metode_sampling: { required: "Field tidak boleh kosong" },
                lap_bln: { required: "Field tidak boleh kosong" },
                min: { required: "Field tidak boleh kosong" },
                max: { required: "Field tidak boleh kosong" },
                industri: { required: "Field tidak boleh kosong" },
                rerata: { required: "Field tidak boleh kosong" }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        
        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="debit_air_limbah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Registrasi Debit Air Limbah</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/debit_air_limbah/register') ?>" method="post" id="form_add">
                <div class="modal-body">                    
                   
                    <div class="row">                        
                        <div class="col-lg-6 form-group value_parameter">
                            <label>Laporan Bulan dan Tahun</label>
                            <input class="form-control input-sm datepicker" type="text" name="lap_bln" id="lap_bln" value="" placeholder="mm.yyyy" data-date-format="mm.yyyy" data-date-start-view="2" data-date-min-view-mode="1">
                        </div>             
                        <div class="col-lg-6 form-group param_internal">
                            <label>Metode Sampling</label>
                            <select name="metode_sampling" id="metode_sampling" class="form-control input-sm">
                                <option value="" disabled selected>-- Pilih Metode Sampling --</option>
                                <?php 
                                    foreach ($this->functions->get_metode_sampling() as $key => $value) {
                                        echo '<option value="'.$key.'">'.$value.'</option>';
                                    }
                                ?>
                            </select>                                                   
                        </div>                      
                    </div>   

                    <div class="form-group tentang">
                        <label>Nama Usaha/Kegiatan</label>                        
                        <select name="industri" id="industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                            <option value="Empty">&nbsp;</option>
                            <?php
                                foreach ($industri as $key => $value) {
                                    echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                }
                            ?>
                        </select>
                    </div>


                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">                      
                            <label>Min.</label>
                            <input type="text" name="min" id="min" class="form-control input-sm" value="" placeholder="0" />
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">                      
                            <label>Max.</label>
                            <input type="text" name="max" id="max" class="form-control input-sm" value="" placeholder="0" />
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">                      
                            <label>Rata-Rata</label>
                            <input type="text" name="rerata" id="rerata" class="form-control input-sm" value="" placeholder="0" />
                        </div>
                    </div>                				
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="upload" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="uploadLabel">Unggah Berkas Laporan Bulanan Pemantauan Air Limbah</h4>
            </div>
            <form action="<?php echo base_url('/backend/debit_air_limbah/upload'); ?>" method="post" enctype="multipart/form-data">
				    <div class="modal-body">   
                        <div class="form-group">
                        <label>Upload Berkas</label>
                            <div class="input-group input-group-sm">
                                <input type="text" readonly="" class="form-control">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Browse… <input type="file" name="userfile" />
                                    </span>
                                </span>
                            </div><!-- /.input-group -->
                        </div>
                    </div>
					<div class="modal-footer">
						<input type="hidden" id="upload_id" name="id" value="" />
						<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
						<button type="submit" name="do_upload" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Unggah</button>
					</div>
				</div>
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Debit Air Limbah</h4>
            </div>
            <form action="<?php echo base_url('/backend/debit_air_limbah/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal 