<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Document   : m_ketaatan_industri.php 
// Created on : January 05, 2015 17:57 
// Author     : huda.jtk09@gmail.com 
// Description: Model for Rekapitulasi Ketaatan Industri 

class m_ketaatan_industri extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	/**
	 * huda : get all industri
	 *
	 * @param	Date 
	 * @return	Array of Object
	 */
	function get_all_industri($limit=0, $offset=0) {
		$data = array();

		$query = $this->db->query("SELECT b.id_industri, b.nama_industri, b.alamat, e.ket as kecamatan, f.ket as kelurahan -- , c.ket as usaha_kegiatan, d.ket as jenis_industri
									FROM  industri b
									-- LEFT JOIN usaha_kegiatan c ON b.id_usaha_kegiatan = c.id_usaha_kegiatan
									-- LEFT JOIN jenis_industri d ON b.id_jenis_industri = d.id_jenis_industri
									LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kecamatan
									LEFT JOIN kelurahan f ON b.id_kelurahan = f.id_kelurahan
									ORDER BY b.nama_industri ASC
									LIMIT ".$offset.", ".$limit);
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * huda : get all industri size
	 *
	 * @return	int
	 */
	function get_all_industri_size() {
		
		$query = $this->db->query("SELECT a.id_industri
									FROM industri a");
		
		return ($query->num_rows() > 0) ? $query->num_rows() : 0;
	}

	function get_pelanggaran_industri($id_industri, $year) {
		
		$query = $this->db->query("SELECT GROUP_CONCAT(ket) as pelanggaran
									FROM parameter_bap
									WHERE id_parameter_bap in 
										(SELECT id_parent_1 
											FROM history_item_teguran_bap 
											WHERE id_parent_2 = 'top'
											AND perbaikan = false 
											AND id_bap = 
												(SELECT max(id_bap) 
													FROM bap 
													WHERE id_industri = ".$id_industri." 
													AND YEAR(bap_tgl) = ".$year.")
										UNION
										SELECT id_parent_2 
											FROM history_item_teguran_bap 
											WHERE id_parent_2 != 'top' 
											AND perbaikan = false 
											AND id_bap = 
												(SELECT max(id_bap) 
													FROM bap 
													WHERE id_industri = ".$id_industri." 
													AND YEAR(bap_tgl) = ".$year."))");
		
		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return NULL;
		}
	}

	function get_bap_count($id_industri, $year) {

		$query = $this->db->query("SELECT count(id_bap) as total
									FROM bap 
									WHERE id_industri = ".$id_industri." 
									AND YEAR(bap_tgl) = ".$year);
		
		if ($query->num_rows() == 1) {
			return $query->row()->total;
		} else {
			return 0;
		}
	}

	/**
	 * Get data by Id
	 *
	 * @param	String, String, int	 
	 * @return	object
	 */
	function get_obj_by_id($table_name, $column_name, $id) {

		$this->db->where($column_name, $id);
		
		$query = $this->db->get($table_name);
		
		if ($query->num_rows() == 1) {
			return $query->row();
		} else {
			return NULL;
		}
	}

	/**
	 * huda : get all pelanggaran who had been corrected
	 *
	 * @param	int, int 
	 * @return	Array of Object
	 */
	function get_perbaikan($id_industri, $year) {
		$data = array();

		$query = $this->db->query("SELECT ket_parameter_bap 
									FROM history_item_teguran_bap 
									WHERE perbaikan = true 
									AND id_bap = 
										(SELECT max(id_bap) 
											FROM bap 
											WHERE id_industri = ".$id_industri." 
											AND YEAR(bap_tgl) = ".$year.")");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>