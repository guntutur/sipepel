<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_employee extends CI_Model {

	private $table_name			= 'pegawai';			
	# private $user_table			= 'users';
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_list($where=array(), $start=0, $limit=0) {

		$this->db->select("*");
		$this->db->from($this->table_name);
		# $this->db->join($this->user_table, $this->user_table.'.id = '.$this->table_name.'.user_id', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->order_by($this->table_name.".nama_pegawai", 'asc');
		$this->db->order_by($this->table_name.".tgl_pembuatan", 'desc');

		if($limit > 0) {
			$this->db->limit($limit, $start);
		}

		$query = $this->db->get();

		$data = array();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select("*");
		$this->db->from($this->table_name);
		// $this->db->join($this->user_table, $this->user_table.'.id = '.$this->table_name.'.user_id', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		return $this->db->count_all_results();
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_employee_by_id($id) {
		$this->db->where('id_pegawai', $id);
		
		// $query = $this->db->get($this->table_name);
		$query = $this->db->query("select u.*, p.* from pegawai p left join users u on p.user_id = u.id where p.id_pegawai = ".$id." order by u.name asc");
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_employee($id) {
		$data = $this->get_employee_by_id($id);

		# delete employee
		$this->db->where('id_pegawai', $data->id_pegawai);
		$this->db->delete($this->table_name);

		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_employee($data, $id) {

		$this->db->where("id_pegawai", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_employee($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate($name) {
		$this->db->where('nip', $name);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}


	/**
	 * huda : add -> get all employee to be used as input in bap form (30 Oct 2014, 14:54)
	 *
	 * Get all employee
	 *
	 * @return array
	 */
	function get_all_employee() {
		$data = array();

		$query = $this->db->query("SELECT * FROM pegawai ORDER BY nama_pegawai ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}
}
?>