 <!-- Document   : v_perbaikan_bap.php  -->
 <!-- Created on : December 5, 2015 xx:xx  -->
 <!-- Author     : guntutur@gmail.com  -->
 <!-- Description: View for Perbaikan Item Ketaatan BAP  -->

<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<!-- <script type="text/javascript" src="../assets/js/bootstrap-multiselect.js"></script> -->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Perbaikan Item Ketaatan BAP&nbsp;&nbsp;<small>Memperbaiki Item Ketaatan berdasarkan data BAP</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Teguran</li>
                <li class="active">Perbaikan Item Ketaatan BAP</li>
            </ol>
            <!-- End breadcrumb -->

            <?php 
                echo $this->session->flashdata('msg'); 
                echo $this->session->flashdata('error');
            ?>

            <ol class="breadcrumb default square rsaquo sm">

                <div class="row">                    
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">
                        <form action="<?php echo base_url('/backend/export_excel/teguran_perbaikan_bap');?>" method="post">
                            <button class="btn-sm btn-primary"><i class="fa fa-download"></i>&nbsp;Export to Excel</button>
                        <form>
                    </div>
                </div>
            </ol>

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <!-- Tab panes -->
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">No.</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th>Jenis Industri</th>
                                <th>Alamat Usaha/Kegiatan</th>
                                <th>Tanggal Pemeriksaan</th>
                                <th>Jumlah pelanggaran</th>
                                <th>Sudah diperbaiki</th>
                                <th>Status</th>
                                <th>action</th>
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->
<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/perbaikan_item_ketaatan_bap/get_list_bap"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                beforeSend: function() { $('#loading').show(); },
                complete: function() { $('#loading').hide(); },
                success: function(result) {
                    console.log(result);
                    $('#pagination').html(result.pagination);
                    $('#start').text(result.total);                 
                    $('#nums').text(result.total);
                    Document.param.numpage = result.numpage;

                    var h = '', ind = {}, no = Document.param.curpage * Document.param.dataperpage;
                     
                    if (result.bap_data.length < 1) {
                        h += '<tr><td colspan=\'7\'>Data Tidak Ditemukan</td></tr>';
                    } else {
                        for (var i = 0; i < result.bap_data.length; i++) {
                            ind = result.bap_data[i];

                            var tgl_buat = ind.tgl.split('-');
                            var new_tgl = [tgl_buat[2], $(this).get_month(parseInt(tgl_buat[1])), tgl_buat[0]].join(' ');

                            h += '<tr>';
                            h += '<td class="text-center">'+(no+=1)+'</td>';
                            h += '<td>'+ ind.nama_industri +'</td>';
                            h += '<td>'+ ind.jenis_usaha +'</td>';
                            h += '<td>'+ ind.alamat +'</td>';
                            h += '<td>'+ new_tgl +'</td>';
                            h += '<td class="text-center">'+ ind.jml_teguran +'</td>';
                            if(ind.teguran_diperbaiki!=null){
                                h += '<td class="text-center">'+ ind.teguran_diperbaiki +'</td>';
                            } else{
                                h += '<td class="text-center">Tidak Melanggar</td>';
                            }
                            if((ind.teguran_diperbaiki==ind.jml_teguran) || (ind.teguran_diperbaiki==null)){
                                h += '<td class="text-center"><i class="fa fa-check"></i></td>';
                            } else {
                                h += '<td class="text-center"><i class="fa fa-remove"></i></td>';    
                            }
                           
                            h += '<td class="text-center">';
                            h += '<div class="btn-group">';
                            h += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">';
                            h += '<i class="fa fa-cogs"></i>';
                            h += '</button>';
                            h += '<ul class="dropdown-menu pull-right" role="menu">';
                            h += '<li><a onclick="$(this).perbaiki(\''+ind.id_bap+'\',\''+ind.nama_industri+'\',\''+new_tgl+'\')">Perbaiki Pelanggaran</a></li>';
                            h += '</ul></div></td>';

                            h += '<tr>'
                        }
                    }

                    $('#document-data').html(h); 
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }

    $(document).ready(function () {

        $.fn.perbaiki = function(id_bap,industri,tgl) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/teguran_bap/get_pelanggaran'); ?>",
                data: 'id_bap='+id_bap+'&industri='+industri+'&tgl='+tgl,
                success: function(d) {
                    console.log(d);
                    var header = '<tr><td>Nama Usaha / Kegiatan</td><td>'+d.industri+'</td></tr>';
                        header += '<tr><td>Tanggal Pengawasan</td><td>'+d.tgl+'</td></tr></tr>';
                    var h = '';

                    h += '<div class="table-responsive"> ';
                    h += '<table class="table table-striped table-hover" id="datatable-example">';
                    h += '<thead class="the-box dark full">';
                    h += '<tr><th>No</th>';
                    h += '<th>Item Pelanggaran</th>';
                    h += '<th>Nilai Item</th>';
                    h += '<th>Nilai Standard Baku Mutu</th>';
                    h += '<th>Perbaiki Item</th>';
                    h += '</tr>';
                    h += '</thead>';
                    h += '<tbody>';
                    h += '<input type="hidden" name="id_bap" value="'+d.id_bap+'">';

                    if (d.teguran != null) {
                        data = {}, no = 0;
                        for (var i = 0; i < d.teguran.length; i++) {
                            data = d.teguran[i];

                            if ((data.nilai_parameter == 'Dibakar') || data.nilai_parameter == 'dibakar'){
                                data.nilai_parameter_bap = 'Selain dibakar';
                            }

                            h += '<tr>';
                            h += '<td class="text-center">'+(no+=1)+'</td>';
                            h += '<td>'+data.ket_parameter_bap+'</td>';
                            h += '<td>'+((data.perbaikan==1) ? data.nilai_parameter_bap : data.nilai_parameter)+'</td>';
                            h += '<td>'+data.nilai_parameter_bap+'</td>';
                            h += '<td><input type="checkbox" name="check[]" value="'+data.id_history_teguran_bap+'" '+((data.perbaikan==1) ? "checked" : "")+'><td>';
                            h += '</tr>';             
                        }

                        h += '</tbody>';
                        h += '</table>';
                        h += '</div>';

                        $('#bap-pelanggaran').html(h);
                        $('#bap-ket').html('');
                    } else {
                        $('#bap-ket').html('Tidak Ditemukan Pelanggaran');
                    }

                    $('#bap-header').html(header);
                    $('#DetailModal').modal('show');
                }            
            })
        }

        $("form").submit(function () {

            var this_master = $(this);

            this_master.find('input[type="checkbox"]').each( function () {
                var checkbox_this = $(this);


                if( checkbox_this.is(":checked") == true ) {
                } else {
                    checkbox_this.prop('checked',true);
                    checkbox_this.attr('value','0');
                }
            })
        })

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }

        Document.search();

        window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

    });
</script>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Bap</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rm/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apa anda yakin ingin menghapus data BAP ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="DetailModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Perbaikan Pelanggaran BAP </h4>
            </div>
            <form action="<?php echo base_url('/backend/perbaikan_item_ketaatan_bap/perbaikan'); ?>" method="post">

                <div class="modal-body">
                    <table class="table table-th-block">
                         <tbody id="bap-header"></tbody>
                    </table>
                    <div id="bap-pelanggaran"></div>
                    <p id="bap-ket"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;&nbsp;Perbaiki</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="EditModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Ubah Status Pelanggaran BAP </h4>
            </div>
            <form action="<?php echo base_url('/backend/perbaikan_item_ketaatan_bap/perbaikan'); ?>" method="post">
                <div class="modal-body">
                    <table class="table table-th-block">
                         <tbody id="edit-header"></tbody>
                    </table>
                    <div id="edit-pelanggaran"></div>
                    <p id="edit-ket"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;&nbsp;Perbaiki</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->