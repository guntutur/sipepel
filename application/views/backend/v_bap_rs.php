<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAP Rumah Sakit&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Rumah Sakit</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen BAP</li>
                <li class="active">BAP Rumah Sakit</li>
            </ol>
            <!-- End breadcrumb -->

            <?php echo $this->session->flashdata('msg'); ?>
                        <?php echo $this->session->flashdata('error'); ?>

            <!-- BEGIN DATA TABLE -->
            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li id="bap-list" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>" class="<?php echo (($tab_list!= '') ? 'active' : ''); ?>"><a href="#" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;&nbsp;Daftar BAP</a></li>
                    <li id="bap-form" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>"><a href="#add_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Tambah BAP</a></li>
                    <li style="<?php echo (($tab_view== '') ? 'display:none' : ''); ?>" class="<?php echo (($tab_view!= '') ? 'active' : ''); ?>"><a href="#view" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Lihat BAP</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane <?php echo (($tab_list!= '') ? 'active' : ''); ?>" id="list" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>">
                         
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="datatable-example">
                                <thead class="the-box dark full">
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Nama Rumah Sakit</th>
                                        <th>Tanggal </th>
                                        <th>Kelengkapan Data</th>
                                        <th>Status Periksa (by system)</th>
                                        <th>Jumlah Item Tidak Taat</th>
                                        <th class="text-right"></th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($bap_rs as $b) {
                                    ?>
                                    <tr class="gradeA">
                                        <td class="text-center"><?php echo ++$no; ?></td>
                                        <td><?php echo $b->nama_industri; ?></td>
                                        <td><?php echo $b->bap_tgl; ?></td>
                                        <td><?php echo $b->compare_status==0 ? "<span class='label label-danger'>Tidak Lengkap</span>" : "<span class='label label-success'>Lengkap</span>"; ?></td>
                                        <td><?php echo $b->is_compared==0 ? "<span class='label label-danger'>Belum diperiksa</span>" : "<span class='label label-success'>Sudah diperiksa</span>"; ?></td>
                                        <td><?php echo $b->is_compared==0 ? "-" : $b->jml_teguran; ?></td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-cogs"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li><a href="#" class="compare-row" data-id="<?php echo $b->id_bap; ?>">Periksa BAP</a></li>
                                                    <li>
                                                        <form action="<?php echo base_url('/backend/bap_rs/view'); ?>" method="post">
                                                            <a href="#" onclick="parentNode.submit();" style="padding: 5px 15px;color: #333;display: block;">Lihat Data</a>
                                                            <input type="hidden" name="id_bap" value="<?php echo $b->id_bap; ?>"/>
                                                        </form>
                                                    </li>
                                                    <li><a onclick="$(this).pelanggaran(<?php echo $b->id_bap.',\''.$b->nama_industri.'\',\''.$b->bap_tgl.'\''; ?>)">Lihat Pelanggaran</a></li> 
                                                    <li class="divider"></li>
                                                    <li>
                                                        <form action="<?php echo base_url('/backend/bap_rs/edit'); ?>" method="post">
                                                            <a href="#" onclick="parentNode.submit();" style="padding: 5px 15px;color: #333;display: block;">Ubah Data</a>
                                                            <input type="hidden" name="id_bap" value="<?php echo $b->id_bap; ?>"/>
                                                        </form>
                                                    </li>
                                                    <li><a href="#" class="delete-row" data-id="<?php echo $b->id_bap; ?>">Hapus Data</a></li>
                                                </ul>
                                            </div>
                                        </td>                                   
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div><!-- /.table-responsive -->
                    </div>

                    <div class="tab-pane" id="add_new" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>">
                        <div class="row" style="margin-top: 10px;">       
                            <div class="col-xs-3" id="vertical_tab" style="display: none"> <!-- required for floating -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#data_umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                <li><a href="#pencemaran_air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                <li><a href="#pencemaran_b3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                <li><a href="#lain_lain" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Lain-lain</a></li>
                              </ul>
                            </div>
                            <div class="col-xs-12">
                            <!-- Tab panes -->
                                <form role="form" id="add_form" action="<?php echo base_url('backend/bap_rs/newInsert') ?>" method="post">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="data_umum">
                                            <h4 class="page-tab">Data Umum</h4>
                                            <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Tanggal Pengawasan</label>
                                                <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">                  
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label>Pukul</label>
                                                <div class="bfh-timepicker">
                                                  <input id="timepick1" type="text" name="bap_jam" class="form-control input-sm bfh-timepicker">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>Petugas Pengawas</label>
                                                    <select class="form-control input-sm" name="id_pegawai">
                                                        <option value=""></option>
                                                        <?php 
                                                            foreach ($pegawai as $p) {
                                                                echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                            }?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Beserta Anggota Pengawas</label>
                                                    <select class="form-control input-sm multiselect" multiple="multiple" name="id_petugas_bap_list[]" style="border: 0!important;">
                                                        <?php 
                                                            foreach ($pegawai as $p) {                                                    
                                                                echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                            }?>
                                                    </select>
                                                </div>
                                            </div> <br>
                                            <div class="form-group">
                                                <h4>Lokasi Pengawasan / Pembinaan</h4>    
                                                <div class="form-group ">
                                                    <label>Nama Usaha / Kegiatan</label>
                                                    <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                        <option value="Empty">&nbsp;</option>
                                                        <?php
                                                            foreach ($industri as $value) {
                                                                echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                    <!-- <input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                                    <input type="hidden" id="id_industri" name="id_industri" value="0"/> -->
                                                </div>
                                                <!-- <div id="industri-detail2"></div> -->
                                                <div id="container2" style="display:none"></div>
                                            </div>
                                            <div class="form-group">
                                                <h4>Penanggung Jawab Usaha / Kegiatan</h4>
                                                <div class="form-group ">
                                                    <label>Nama</label>
                                                    <input type="text" name="p2_nama" class="form-control input-sm has-feedback" />
                                                </div>
                                                <div class="form-group ">
                                                    <label>Telepon</label>
                                                    <input type="text" name="p2_telp" class="form-control input-sm has-feedback" />
                                                </div>
                                                <div class="form-group ">
                                                    <label>Jabatan</label>
                                                    <input type="text" name="p2_jabatan" class="form-control input-sm has-feedback" />
                                                </div>
                                            </div>
                                            <br>

                                            <h4 class="page-tab">Data Umum Usaha/Kegiatan</h4>
                                            <div class="form-group ">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" name="jumlah_karyawan" class="form-control input-sm has-feedback" />
                                            </div>
                                            <div class="form-group ">
                                                <label>Jumlah Pasien</label>
                                                <input type="text" name="jml_pasien" class="form-control input-sm has-feedback" />
                                            </div>
                                            <div class="form-group ">
                                                <label>Jumlah Tempat Tidur</label>
                                                <input type="text" name="jml_tempat_tidur" class="form-control input-sm has-feedback" />
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Dokumen Lingkungan</label>
                                                        <select class="form-control input-sm" name="dok_lingk" id="dok_lingk">
                                                            <option value="1">Ada</option>
                                                            <option value="0">Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tahun</label>
                                                        <input type="text" name="dok_lingk_tahun" class="form-control input-sm has-feedback" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jenis</label>
                                                <!-- <select class="form-control input-sm" name="dok_lingk_jenis">
                                                    <option value=""></option>
                                                    <option value="Amdal">Amdal</option>
                                                    <option value="UKL-UPL">UKL-UPL</option>
                                                    <option value="DELH">DELH</option>
                                                    <option value="DPLH">DPLH</option>
                                                    <option value="SPPL">SPPL</option>
                                                </select> -->
                                                <select class="form-control input-sm" name="dok_lingk_jenis" id="jdl_name">
                                                        <option value='' selected="true">--  Pilih Salah Satu --</option>
                                                        <?php
                                                            foreach ($jenis_dok_lingkungan as $key) {
                                                                echo "<option value='$key->ket'>$key->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Izin Lingkungan</label>
                                                        <select class="form-control input-sm" name="izin_lingk" id="izin_lingk">
                                                            <option value="1">Ada</option>
                                                            <option value="0">Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tahun</label>
                                                        <input type="text" name="izin_lingk_tahun" class="form-control input-sm has-feedback" />
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="page-tab">Lain - Lain</h4>
                                            <div class="form-group ">
                                                <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"></textarea>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6 text-left">
                                                    <input id="save-datum" class="btn btn-md btn-success save-partially" value="Simpan Data Umum"/>
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <a id="confirm" class="btn btn-warning NextStep">Pencemaran Air<i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="pencemaran_air">          
                                            <h4 class="page-tab">Pengendalian Pencemaran Air</h4>

                                            <label>a. Sumber Air dan Penggunaan</label>
                                            <div class="form-group ">
                                                <label>Air Tanah</label>
                                                <input type="text" name="ambil_air_tanah" class="form-control input-sm has-feedback" placeholder="m3/hari"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Air Permukaan</label>
                                                <input type="text" name="ambil_air_permukaan" class="form-control input-sm has-feedback" placeholder="m3/hari"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>PDAM</label>
                                                <input type="text" name="ambil_air_pdam" class="form-control input-sm has-feedback" placeholder="m3/hari"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Lain-lain</label>
                                                <input type="text" name="ambil_air_lain" class="form-control input-sm has-feedback" placeholder="m3/hari"/>
                                            </div>

                                            <!-- <div class="form-group ">
                                                <label>b. Sumber Air Limbah</label>
                                                <select class="form-control input-sm" name="limb_sumber">
                                                    <option value="Domestik">Domestik</option>
                                                    <option value="Ruang Operasi">Ruang Operasi</option>
                                                    <option value="Laboratorium">Laboratorium</option>
                                                    <option value="Radiologi">Radiologi</option>
                                                    <option value="Dapur">Dapur</option>
                                                    <option value="Laundry">Laundry</option>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label>c. Badan Air Penerima</label>
                                                <input type="text" name="bdn_terima" class="form-control input-sm has-feedback" />
                                            </div> -->

                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>b. Sumber Air Limbah</label>
                                                    <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                                    <!-- <option value=''></option> -->
                                                        <?php
                                                            foreach ($limb_sumber as $l) {
                                                                echo "<option value='$l->ket'>$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>c. Badan Air Penerima</label>
                                                    <select name="bdn_terima" class="form-control input-sm ">
                                                    <option value=''></option>
                                                        <?php
                                                            foreach ($bdn_terima as $bdn) {
                                                                echo "<option value='$bdn->ket'>$bdn->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>d. IPAL</label>
                                                <select class="form-control input-sm" name="ipal" id="ipal">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="row" id="detail_ipal">
                                                <div class="col-lg-4">
                                                    <div class="form-group ">
                                                        <label>Sistem IPAL</label>
                                                        <select class="form-control input-sm" name="ipal_sistem" id="si">
                                                            <?php
                                                                foreach ($sistem_ipal as $val) {
                                                                    echo '<option value='.$val->ket.'>'.$val->ket.'</option>';
                                                                }
                                                            ?>
                                                            <option value="other">lainnya</option>
                                                        </select>
                                                        <input type="text" name="ipal_sistem2" id="si2" class="form-control input-sm has-feedback" style="display:none"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group ">
                                                        <label>Unit IPAL</label>
                                                        <select name="ipal_unit[]" id="ipal_unit" class="form-control input-sm multiselect" multiple="multiple">
                                                            <?php
                                                                foreach ($ipal_unit as $iu) {
                                                                    echo '<option value='.$iu->ket.'>'.$iu->ket.'</option>';
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group ">
                                                        <label>Kapasitas IPAL</label>
                                                        <input type="text" name="ipal_kapasitas" id="ipal_kapasitas" class="form-control input-sm has-feedback" />
                                                    </div>  
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>e. Perizinan</label>
                                                <select class="form-control input-sm" name="izin">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Nomor</label>
                                                        <input type="text" name="izin_no" class="form-control input-sm has-feedback" />
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tanggal</label>
                                                        <input type="text" name="izin_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">    
                                                    </div>
                                                </div>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Debit Izin (m3/hari)</label>
                                                <input type="text" name="izin_debit" class="form-control input-sm has-feedback" />
                                            </div>
                                            <div class="form-group ">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet S</label>
                                                        <div class="form-group status">
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" name="koord_outlet_jam_s" id="jam_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">'</span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" name="koord_outlet_menit_s" id="menit_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">"</span>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet E</label>
                                                        <div class="form-group status">
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" name="koord_outlet_derajat_e" id="derajat_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" name="koord_outlet_jam_e" id="jam_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">'</span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                    <div class="input-group input-group-sm">                                
                                                                        <input type="text" name="koord_outlet_menit_e" id="menit_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                        <span class="input-group-addon">"</span>
                                                                    </div> 
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>f. Alat Ukur</label>
                                                <select class="form-control input-sm" name="au" id="au">
                                                    <option value="1" selected="true">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>
                                            <div class="form-group" id="detail_au" name="detail_au">
                                                <div class="form-group ">
                                                    <label>Jenis</label>
                                                    <select class="form-control input-sm" name="au_jenis" id="je">
                                                        <option value="Kumulatif">Kumulatif</option>
                                                        <option value="V-notch">V-notch</option>
                                                        <option value="Digital">Digital</option>
                                                        <option value="other">lainnya</option>
                                                    </select>
                                                    <input type="text" name="au_jenis2" id="je2" class="form-control input-sm has-feedback" style="display:none"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Ukuran (inchi)</label>
                                                    <input type="text" name="au_ukuran" class="form-control input-sm has-feedback" />
                                                </div>    
                                                <div class="form-group ">
                                                    <label>Kondisi (apabila ada)</label>
                                                    <select class="form-control input-sm" name="au_kondisi">
                                                        <option value="Berfungsi">Berfungsi</option>
                                                        <option value="Tidak Berfungsi">Tidak Berfungsi</option>
                                                    </select>
                                                </div>    
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>g. Catatan debit harian</label>
                                                <select class="form-control input-sm" name="cat_deb_hari">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>h. Daur Ulang</label>
                                                <select class="form-control input-sm" name="daur_ulang">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Debit (m3/hari)</label>
                                                <input type="text" name="daur_ulang_debit" class="form-control input-sm has-feedback" />
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>i. Pengujian Kualitas Air Limbah</label>
                                                <select class="form-control input-sm" name="uji_limbah" id="uji_limbah">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>    
                                            <div class="form-group" name="detail_uji" id="detail_uji">
                                                <div class="form-group ">
                                                    <label>Periode Pengujian</label>
                                                    <select class="form-control input-sm" name="uji_period" id="pp">
                                                        <option value="Setiap Bulan" selected="true">Setiap Bulan</option>
                                                        <option value="other">lainnya</option>
                                                    </select>
                                                    <input type="text" name="uji_period2" id="pp2" class="form-control input-sm has-feedback" style="display:none"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Laboratorium Pengujian</label>
                                                    <select id="uji_lab" name="uji_lab" class="form-control input-sm">
                                                        <?php
                                                            foreach ($lab as $key) {
                                                                echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                                                            }
                                                            
                                                        ?>
                                                    </select>
                                                </div>    
                                                <div class="form-group ">
                                                    <label>Hasil Pengujian</label>
                                                    <select class="form-control input-sm" name="uji_hasil" id="uji_hasil">
                                                        <option value="Memenuhi">Memenuhi</option>
                                                        <option value="Tidak Memenuhi Baku Mutu">Tidak Memenuhi Baku Mutu</option>
                                                    </select>
                                                </div>   
                                            </div>
                                            <div class="form-group ">
                                                <label>Apabila tidak memenuhi baku mutu, terjadi pada bulan</label>
                                                <input type="text" name="uji_tidak_bulan" class="form-control input-sm has-feedback" />
                                            </div>    
                                            <div class="form-group ">
                                                <label>Parameter yang tidak memenuhi baku mutu</label>
                                                <input type="text" name="uji_tidak_param" class="form-control input-sm has-feedback" />
                                            </div>    

                                            <div class="form-group ">
                                                <label>Pelaporan</label>
                                                <select class="form-control input-sm" name="uji_lapor">
                                                    <option value="Rutin">Rutin</option>
                                                    <option value="Tidak Rutin">Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan">Tidak Melaporkan</option>
                                                </select>
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>j. Bahan Kimia Pembantu IPAL</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Nama Bahan Kimia</th>
                                                            <th class="text-center">Jumlah Pemakaian (ton/hari)</th>
                                                            <th class="text-center"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-duplicate="kimia-ipal" data-duplicate-min="1">
                                                            <td class="col-lg-5"><input type="text" name="bkpi_nama[]" class="form-control input-sm has-feedback" /></td></td>
                                                            <td class="col-lg-5"><input type="text" name="bkpi_jml[]" class="form-control input-sm has-feedback" /></td>
                                                            <td class="col-lg-2">
                                                                <div class=" text-right">
                                                                    <div class="btn-group" style="padding-right: 17px;">
                                                                        <button type="button" class="btn btn-sm btn-success" data-duplicate-add="kimia-ipal"><i class="fa fa-plus-circle"></i></button>
                                                                        <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="kimia-ipal"><i class="fa fa-times-circle"></i></button>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <!-- <tbody>
                                                        <tr><td><input type="text" name="bkpi_nama1" class="form-control input-sm has-feedback " /></td><td><input type="text" name="bkpi_jml1" class="form-control input-sm has-feedback " /></td></tr>
                                                        <tr><td><input type="text" name="bkpi_nama2" class="form-control input-sm has-feedback " /></td><td><input type="text" name="bkpi_jml2" class="form-control input-sm has-feedback " /></td></tr>
                                                        <tr><td><input type="text" name="bkpi_nama3" class="form-control input-sm has-feedback " /></td><td><input type="text" name="bkpi_jml3" class="form-control input-sm has-feedback " /></td></tr>
                                                        <tr><td><input type="text" name="bkpi_nama4" class="form-control input-sm has-feedback " /></td><td><input type="text" name="bkpi_jml4" class="form-control input-sm has-feedback " /></td></tr>
                                                        <tr><td><input type="text" name="bkpi_nama5" class="form-control input-sm has-feedback " /></td><td><input type="text" name="bkpi_jml5" class="form-control input-sm has-feedback " /></td></tr>
                                                        <tr><td><input type="text" name="bkpi_nama6" class="form-control input-sm has-feedback " /></td><td><input type="text" name="bkpi_jml6" class="form-control input-sm has-feedback " /></td></tr>
                                                    </tbody> -->
                                                </table>
                                            </div>

                                            <div class="form-group ">
                                                <label>k. Kebocoran/Rembesan/Tumpahan/Bypass</label>
                                                <select class="form-control input-sm" name="bocor" id="bocor">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>    
                                            <div class="form-group " id="lok" >
                                                <label>Lokasi</label>
                                                <input type="text" name="bocor_lokasi" class="form-control input-sm has-feedback" />
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>l. Lain-lain</label>
                                                <input type="text" name="lain_lain_pa" id="" class="form-control input-sm has-feedback" />
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4 text-left">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Data Umum</a>
                                                </div>
                                                <div class="col-sm-4 text-center">
                                                    <input id="save-pencemaran_air" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Air"/>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <a class="btn btn-warning NextStep">Pencemaran Udara<i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_udara">
                                            <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                            
                                            <div class="form-group ">
                                                <label>a. Data Kualitas Udara Ambien</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th class="text-center">Upwind</th>
                                                            <th class="text-center">Site</th>
                                                            <th class="text-center">Downwind</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>Pengujian Kualitas</label></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas1"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas2"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas3"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                            <td><select class="form-control input-sm" name="period1"><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period2"><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period3"><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Laboratorium Penguji</label></td>
                                                            <?php
                                                                for ($i=1; $i<4; $i++) {
                                                                    echo "<td><select name='lab".$i."' class='form-control input-sm'>";
                                                                    echo "<option value=''></option>";
                                                                        foreach ($lab as $key) {
                                                                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                                                                        }
                                                                    echo '</select></td>';
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Pemenuhan BM</label></td>
                                                            <td><select class="form-control input-sm bmp" id="bmp1" name="bm_pemenuhan1"><option value="0">Tidak</option><option value="1">Ya</option></select></td>
                                                            <td><select class="form-control input-sm bmp" id="bmp2" name="bm_pemenuhan2"><option value="0">Tidak</option><option value="1">Ya</option></select></td>
                                                            <td><select class="form-control input-sm bmp" id="bmp3" name="bm_pemenuhan3"><option value="0">Tidak</option><option value="1">Ya</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" name="bm_param1" id="bm_param1" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" name="bm_param2" id="bm_param2" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" name="bm_param3" id="bm_param3" class="form-control input-sm has-feedback " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" name="bm_period1" id="bm_period1" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" name="bm_period2" id="bm_period2" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" name="bm_period3" id="bm_period3" class="form-control input-sm has-feedback " /></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
   
                                            <div class="form-group ">
                                                <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                    <option value="Rutin">Rutin</option>
                                                    <option value="Tidak Rutin">Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan">Tidak Melaporkan</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. Lain-lain</label>
                                                <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" />
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4 text-left">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Air</a>
                                                </div>
                                                <div class="col-sm-4 text-center">
                                                    <input id="save-pencemaran_udara" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Udara"/>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <a class="btn btn-warning NextStep">Pencemaran PB3<i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_b3">
                                            <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
                                            <div class="form-group ">
                                                <label>a. Jenis Limbah B3 yang ditimbulkan</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Jenis Limbah B3</th>
                                                            <th class="text-center" style="width:15%">Jumlah (ton/hari)</th>
                                                            <th class="text-center">Pengelolaan</th>
                                                            <th class="text-left" style="width:30%">Pihak Ketiga</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr data-duplicate="jenis_limbah" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3 --</option>
                                                                    <?php foreach($jenis_limbah_b3 as $uk): ?>
                                                                        <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                                                                    <?php endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="jumlah[]" class="form-control input-sm has-feedback" /></td>
                                                            <td><select class="form-control input-sm" name="pengelolaan[]"><option value=""></option><option value="Dikelola">Dikelola</option><option value="Tidak Dikelola">Tidak Dikelola</option></select></td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:63%">
                                                                        <select name="pihak_ke3[]" class="form-control input-sm has-feedback">
                                                                            <option value=""></option>
                                                                                <?php
                                                                                    foreach ($pihak_ke3 as $p3) {
                                                                                        echo "<option value='".$p3->badan_hukum." ".$p3->nama_industri."'>".$p3->badan_hukum." ".$p3->nama_industri."</option>";
                                                                                    }
                                                                                ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="co-lg-6" style="padding-right: 0;">
                                                                        <div class=" text-right">
                                                                            <div class="btn-group" style="padding-right: 17px;">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="jenis_limbah"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="jenis_limbah"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group ">
                                                <label>b. Penyimpanan</label>
                                                <select class="form-control input-sm" name="b3_penyimpanan">
                                                    <option value="Pada TPS Limbah B3">Pada TPS Limbah B3</option>
                                                    <option value="Di luar TPS Limbah B3">Di luar TPS Limbah B3</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. TPS Limbah B3</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">TPS Limbah B3</th>
                                                            <th class="text-center">1</th>
                                                            <th class="text-center">2</th>
                                                            <th class="text-center">3</th>
                                                            <th class="text-center">4</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Perizinan</td>
                                                            <td><select name="tb_izin1" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin2" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin3" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                                                            <td><select name="tb_izin4" class="form-control input-sm izin_tps"><option value="3" selected> </option><option value="Ada">Ada</option><option value="Tidak Ada">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nomor</td>
                                                            <td><input type="text" name="tb_izin_no1" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_izin_no2" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_izin_no3" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_izin_no4" class="form-control input-sm has-feedback" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal</td>
                                                            <td><input type="text" name="tb_izin_tgl1" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                                                            <td><input type="text" name="tb_izin_tgl2" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                                                            <td><input type="text" name="tb_izin_tgl3" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                                                            <td><input type="text" name="tb_izin_tgl4" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenis Limbah B3</td>
                                                            <td><input type="text" name="tb_jenis1" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_jenis2" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_jenis3" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_jenis4" class="form-control input-sm has-feedback" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"><label>Lama Penyimpanan</label></td>
                                                        </tr>
                                                        <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                                                    <?php foreach($jenis_limbah_b3 as $uk): ?>
                                                                        <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                                                                    <?php endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" /></td>
                                                            <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" /></td>
                                                            <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" /></td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:60%">
                                                                        <div class="text-left">
                                                                            <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-lg-6">
                                                                        <div class=" text-right" >
                                                                            <div class="btn-group">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>Koordinat</td>
                                                            <td>
                                                                <div class="row" style="padding-right: 14px;">
                                                                    <div class="col-lg-12">
                                                                        <label>S</label>
                                                                        <div class="row">                               
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_s1" class="form-control input-sm derajat" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_s1" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_s1" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <label>E</label>
                                                                        <div class="row">
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_e1" class="form-control input-sm" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_e1" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_e1" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="row" style="padding-right: 14px;">
                                                                    <div class="col-lg-12">
                                                                        <label>S</label>
                                                                        <div class="row">                               
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_s2" class="form-control input-sm derajat" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_s2" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_s2" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <label>E</label>
                                                                        <div class="row">
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_e2" class="form-control input-sm" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_e2" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_e2" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="row" style="padding-right: 14px;">
                                                                    <div class="col-lg-12">
                                                                        <label>S</label>
                                                                        <div class="row">                               
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_s3" class="form-control input-sm derajat" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_s3" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_s3" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <label>E</label>
                                                                        <div class="row">
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_e3" class="form-control input-sm" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_e3" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_e3" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="row" style="padding-right: 14px;">
                                                                    <div class="col-lg-12">
                                                                        <label>S</label>
                                                                        <div class="row">                               
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_s4" class="form-control input-sm derajat" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_s4" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_s4" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <label>E</label>
                                                                        <div class="row">
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_derajat_e4" class="form-control input-sm" value="" placeholder="00&deg;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_jam_e4" class="form-control input-sm" value="" placeholder="00&#39;">
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-right: 0;">
                                                                                <input type="text" name="koord_menit_e4" class="form-control input-sm" value="" placeholder="00&quot;">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ukuran (m2)</td>
                                                            <td><input type="text" name="tb_ukuran1" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_ukuran2" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_ukuran3" class="form-control input-sm has-feedback" /></td>
                                                            <td><input type="text" name="tb_ukuran4" class="form-control input-sm has-feedback" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4"><label>Kelengkapan</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Papan Nama dan Koordinat</td>
                                                            <td><select name="tb_ppn_nama_koord1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_ppn_nama_koord4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Simbol dan Label</td>
                                                            <td><select name="tb_simbol_label1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_simbol_label4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Saluran Ceceran Air Limbah</td>
                                                            <td><select name="tb_saluran_cecer1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_saluran_cecer4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bak Penampung Ceceran Air Limbah</td>
                                                            <td><select name="tb_bak_cecer1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_bak_cecer4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kemiringan</td>
                                                            <td><select name="tb_kemiringan1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_kemiringan4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>SOP Pengelolaan dan Tanggap Darurat</td>
                                                            <td><select name="tb_sop_darurat1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_sop_darurat4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Log Book</td>
                                                            <td><select name="tb_log_book1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_log_book4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>APAR</td>
                                                            <td><select name="tb_apar_p3k1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_apar_p3k4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kotak P3K</td>
                                                            <td><select name="tb_p3k1" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k2" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k3" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                            <td><select name="tb_p3k4" class="form-control input-sm"><option value="1">Ada</option><option value="0">Tidak Ada</option></select></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>e. Pengujian Kualitas Limbah B3</label>
                                                    <select class="form-control input-sm" name="b3_uji">
                                                        <option value="Sudah">Sudah</option>
                                                        <option value="Belum">Belum</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Jenis Pengujian</label>
                                                    <select class="form-control input-sm multiselect" name="b3_uji_jenis[]" id="b3_uji_jenis" multiple="multiple"  style="border: 0!important">
                                                        <!-- <option value=""></option> -->
                                                        <?php
                                                            foreach ($jenis_pengujian as $jp) {
                                                            echo "<option value='$jp->ket'>$jp->ket</option>";
                                                        }
                                                        ?>
                                                        <!-- <option value="Total Logam">Total Logam</option>
                                                        <option value="TCLP">TCLP</option> -->
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>Parameter Uji</label>
                                                <input type="text" name="b3_uji_param" class="form-control input-sm has-feedback" />
                                            </div>
                                            <div class="form-group ">
                                                <label>Laboratorium Penguji</label>
                                                <select name="b3_uji_lab" class="form-control input-sm">
                                                    <?php
                                                        foreach ($lab as $key) {
                                                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                                                        }
                                                        
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label>Waktu Pengujian</label>
                                                <input type="text" name="b3_uji_waktu" class="form-control input-sm has-feedback" />
                                            </div>
                                            <div class="form-group ">
                                                <label>f. Neraca Limbah B3</label>
                                                <select class="form-control input-sm" name="b3_neraca">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>g. Manifest</label>
                                                <select class="form-control input-sm" name="b3_manifest">
                                                    <option value="1">Ada</option>
                                                    <option value="0">Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>h. Pelaporan Neraca dan Manifest</label>
                                                <select class="form-control input-sm" name="b3_lapor_nm">
                                                    <option value="Rutin">Rutin</option>
                                                    <option value="Tidak Rutin">Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan">Tidak Melaporkan</option>
                                                </select>
                                            </div>
                                            
                                            <label>i. Limbah Padat Lain</label>
                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Jenis</th>
                                                        <th class="text-center">Jumlah (ton/hari)</th>
                                                        <th class="text-center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                        <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" /></td></td>
                                                        <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" /></td>
                                                        <td class="col-lg-2">
                                                            <div class=" text-right">
                                                                <div class="btn-group" style="padding-right: 17px;">
                                                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="form-group ">
                                                <label> Pengelolaan</label>
                                                <!-- <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                    <option value="Dibakar">Dibakar</option>
                                                    <option value="Diangkut oleh Pihak Ketiga">Diangkut oleh Pihak Ketiga</option>
                                                    <option value="Diangkut oleh Dinas Pertasih">Diangkut oleh Dinas Pertasih</option>
                                                    <option value="Dikelola oleh Masyarakat Sekitar">Dikelola oleh Masyarakat Sekitar</option>
                                                    <option value="Dijual">Dijual</option>
                                                    <option value="other">lainnya</option>
                                                </select>
                                                <input type="text" name="padat_kelola2" id="pe2" class="form-control input-sm has-feedback" style="display:none"/> -->
                                                <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                <?php
                                                            foreach ($pengelolaan as $pk) {
                                                            echo "<option value='$pk->ket'>$pk->ket</option>";
                                                        }
                                                        ?>
                                                </select>
                                                <input type="text" name="padat_kelola2" id="pe2" class="form-control input-sm has-feedback" style="display:none"/>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4 text-left">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Udara</a>
                                                </div>
                                                <div class="col-sm-4 text-center">
                                                    <input id="save-pencemaran_b3" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran LB3"/>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <!--<a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>-->
                                                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="add" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>">

                        <div class="row">
                            <div class="modal-body">  
                                <div class="col-sm-12">
                                    <!-- <div class="the-box rounded"> -->
                                        <div class="col-sm-6 text-right">
                                            <h4 class="light">Form BAP Rumah Sakit</h4>
                                        </div>
                                        <div class="col-sm-6 text-left">
                                            <button type="button" class="btn btn-primary btn-rounded-lg" onclick="window.location='<?php echo base_url('assets/form/BAPRS.xlsx'); ?>'">Unduh Form</button>
                                        </div>
                                    <!-- </div>   -->
                                </div>
                            </div>
                        </div>

                        <div class="modal-body">
                            <form role="form" action="<?php echo base_url('/backend/bap_rs/insert') ?>" method="post" id="form_add" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label>Tanggal Pengawasan</label>
                                        <input type="text" name="" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                          <input id="timepick2" type="text" name="bap_jam" class="form-control input-sm bfh-timepicker">
                                        </div>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label>Petugas Pengawas</label>
                                        <select class="form-control input-sm" name="">
                                            <option value=""></option>
                                            <?php 
                                                foreach ($pegawai as $p) {
                                                    echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Beserta Anggota Pengawas:</label>
                                        <select class="form-control input-sm multiselect" multiple="multiple" name="" style="border: 0!important;">
                                            <?php 
                                                foreach ($pegawai as $p) {                                                    
                                                    echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                </div>
                                <fieldset>
                                    <legend>Lokasi Pengawasan / Pembinaan</legend>    
                                    <div class="form-group ">
                                        <label>Nama Usaha / Kegiatan</label>
                                        <select class="form-control input-sm" name="" onchange="$(this).get_detail_industri(this, '#industri-detail1');">
                                            <option value=''></option>
                                            <?php 
                                                foreach ($industri as $i) {
                                                    echo "<option value='$i->id_industri'>$i->nama_industri</option>";
                                                }?>
                                        </select>
                                    </div>
                                    <div id="industri-detail1"></div>
                                </fieldset> 
                                <fieldset>
                                    <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                    <div class="form-group ">
                                        <label>Nama</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Telepon</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Jabatan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div> 
                                </fieldset>
                                    <!-- <div class="the-box rounded"> -->
                                        <div class="form-group">
                                            <h4 class="light">Upload Form BAP Rumah Sakit</h4>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" readonly="">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file">
                                                        Browse… <input type="file" name="file" accept=".xlsx">
                                                    </span>
                                                </span>
                                            </div><!-- /.input-group -->
                                        </div>
                                    <!-- </div>   -->

                                <div class="form-group ">
                                    <input type="hidden" id="edit_id" name="id" value="" />
                                    <button type="submit" name="register" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>                                         
                    </div>

                    <div class="tab-pane <?php echo (($tab_view!= '') ? 'active' : ''); ?>" id="view" style="<?php echo (($tab_view== '') ? 'display:none' : ''); ?>">
                        <div class="modal-body">
                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>Tanggal Input Data</td><td><?php echo $bap->tgl_pembuatan; ?></td></tr>
                                    <tr><td>Tanggal Pengawasan</td><td><?php echo date('l', strtotime($bap->bap_tgl)).", ".$bap->bap_tgl; ?></td></tr>
                                    <tr><td>Pukul</td><td><?php echo date('H:i' ,strtotime($bap->bap_jam)); ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>Pengawas</tr>
                                </thead>
                                <tbody>
                                    <tr><td>Nama</td><td><?php echo $pegawai->nama_pegawai; ?></td></tr>
                                    <tr><td>Instansi</td><td><?php echo $pegawai->instansi; ?></td></tr>
                                    <tr><td>NIP.</td><td><?php echo $pegawai->nip; ?></td></tr>
                                    <tr><td>Pangkat/Gol</td><td><?php echo $pegawai->pangkat_gol; ?></td></tr>
                                    <tr><td>Jabatan</td><td><?php echo $pegawai->jabatan; ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>Anggota pengawas</tr>
                                    <tr><td>Nama</td><td>NIP</td><td>Jabatan</td></tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($petugas_bap as $pb) { ?>
                                            <tr><td><?php echo $pb->nama_pegawai; ?></td><td><?php echo $pb->nip; ?></td><td><?php echo $pb->jabatan; ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <br >
                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>Nama Usaha / Kegiatan</td><td><?php echo $industri->nama_industri; ?></td></tr>
                                    <tr><td>Alamat</td><td><?php echo $industri->alamat; ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>Penanggung Jawab Usaha/Kegiatan</tr>
                                </thead>
                                <tbody>
                                    <tr><td>Nama</td><td><?php echo $bap->p2_nama; ?></td></tr>
                                    <tr><td>Telepon</td><td><?php echo $bap->p2_telp; ?></td></tr>
                                    <tr><td>Jabatan</td><td><?php echo $bap->p2_jabatan; ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>DATA UMUM</tr>
                                </thead>
                                <tbody>
                                    <tr><td>a.</td><td>Jenis Usaha/Kegiatan</td><td colspan="2">Rumah Sakit</td></tr>
                                    <tr><td>b.</td><td>Pimpinan Usaha/Kegiatan</td><td colspan="2"><?php echo $industri->pimpinan; ?></td></tr>
                                    <tr><td>c.</td><td>Jumlah Karyawan</td><td colspan="2"><?php echo $bap_rs->jumlah_karyawan; ?></td></tr>
                                    <tr><td>d.</td><td>Hari Kerja</td><td colspan="2"><?php echo $industri->hari_kerja_bulan; ?> Hari/Bulan</td></tr>
                                    <tr><td>e.</td><td>Jam Kerja </td><td colspan="2"><?php echo $industri->jam_kerja_hari; ?> Jam/Hari</td></tr>
                                    <tr><td>f.</td><td>Jumlah Pasien</td><td colspan="2"><?php echo $bap_rs->jml_pasien; ?></td></tr>
                                    <tr><td>g.</td><td>Jumlah Tempat Tidur</td><td colspan="2"><?php echo $bap_rs->jml_tempat_tidur; ?></td></tr>
                                    <tr><td>h.</td><td>Dokumen Lingkungan </td><td colspan="2"><?php echo adaConv($bap->dok_lingk) ?></td></tr>
                                    <tr><td> </td><td>Jenis:</td><td><?php echo $bap->dok_lingk_jenis; ?></td><td>Tahun: <?php echo $bap->dok_lingk_tahun; ?></td></tr>
                                    <tr><td>i.</td><td>Izin Lingkungan</td><td><?php echo adaConv($bap->izin_lingk); ?></td><td>Tahun: <?php echo $bap->izin_lingk_tahun; ?></td></tr>
                                </tbody>
                            </table>
                            <br>

                            <h4>RINGKASAN TEMUAN LAPANGAN:</h4>

                            <!--pencemaran air-->
                            <?php if(isset($pencemaran_air)) {?>
                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>I.   PENGENDALIAN PENCEMARAN AIR</tr>
                                </thead>
                                <tbody>
                                    <tr><td>a.</td><td colspan="2">Sumber Air dan Penggunaan</td></tr>
                                    <tr><td> </td><td>Air Tanah</td><td><?php echo $pencemaran_air->ambil_air_tanah; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>Air Permukaan</td><td><?php echo $pencemaran_air->ambil_air_permukaan; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>PDAM</td><td><?php echo $pencemaran_air->ambil_air_pdam; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>Lain-lain</td><td><?php echo $pencemaran_air->ambil_air_pdam; ?> m3/hari</td></tr>
                                    <tr><td>b.</td><td>Sumber Air Limbah</td><td><?php echo $pencemaran_air->limb_sumber; ?></td></tr>
                                    <tr><td>c.</td><td>Badan Air Penerima</td><td><?php echo $pencemaran_air->bdn_terima; ?></td></tr>
                                    <tr><td>d.</td><td>IPAL</td><td><?php echo adaConv($pencemaran_air->ipal); ?></td></tr>
                                    <tr><td> </td><td>Sistem IPAL</td><td><?php echo $pencemaran_air->ipal_sistem; ?></td></tr>
                                    <tr><td> </td><td>Unit IPAL</td><td><?php echo $pencemaran_air->ipal_unit; ?></td></tr>
                                    <tr><td> </td><td>Kapasitas IPAL</td><td><?php echo $pencemaran_air->ipal_kapasitas; ?></td></tr>
                                    <tr><td>e.</td><td>Perizinan</td><td><?php echo adaConv($pencemaran_air->izin); ?></td></tr>
                                    <tr><td> </td><td>Nomor</td><td><?php echo $pencemaran_air->izin_no." Tanggal: ".$pencemaran_air->izin_tgl; ?></td></tr>
                                    <tr><td> </td><td>Debit Izin</td><td><?php echo $pencemaran_air->izin_debit; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>Koordinat Outlet</td><td>S: <?php echo $pencemaran_air->koord_outlet_derajat_s."&deg".$pencemaran_air->koord_outlet_jam_s."'".$pencemaran_air->koord_outlet_menit_s."&quot"; ?></td></tr>
                                    <tr><td> </td><td></td><td>E: <?php echo $pencemaran_air->koord_outlet_derajat_e."&deg".$pencemaran_air->koord_outlet_jam_e."'".$pencemaran_air->koord_outlet_menit_e."&quot"; ?></td></tr>
                                    <tr><td>f.</td><td>Alat Ukur</td><td><?php echo adaConv($pencemaran_air->au); ?></td></tr>
                                    <tr><td> </td><td>Jenis</td><td><?php echo $pencemaran_air->au_jenis; ?></td></tr>
                                    <tr><td> </td><td>Ukuran</td><td><?php echo $pencemaran_air->au_ukuran; ?> inchi</td></tr>
                                    <tr><td> </td><td>Kondisi (apabila ada)</td><td><?php echo $pencemaran_air->au_kondisi; ?></td></tr>
                                    <tr><td>g.</td><td>Catatan debit harian</td><td><?php echo adaConv($pencemaran_air->cat_deb_hari); ?></td></tr>
                                    <tr><td>h.</td><td>Daur Ulang</td><td><?php echo adaConv($pencemaran_air->daur_ulang); ?></td></tr>
                                    <tr><td> </td><td>Debit</td><td><?php echo $pencemaran_air->daur_ulang_debit; ?> m3/hari</td></tr>
                                    <tr><td>i.</td><td>Pengujian Kualitas Air Limbah</td><td><?php echo adaConv($pencemaran_air->uji_limbah); ?></td></tr>
                                    <tr><td> </td><td>Periode Pengujian</td><td><?php echo $pencemaran_air->uji_period; ?></td></tr>
                                    <tr><td> </td><td>Laboratorium Pengujian</td><td><?php echo $pencemaran_air->uji_lab; ?></td></tr>
                                    <tr><td> </td><td>Hasil Pengujian</td><td><?php echo $pencemaran_air->uji_hasil; ?></td></tr>
                                    <tr><td> </td><td>bulan tidak memenuhi baku mutu</td><td><?php echo $pencemaran_air->uji_tidak_bulan; ?></td></tr>
                                    <tr><td> </td><td>parameter tidak memenuhi baku mutu</td><td><?php echo $pencemaran_air->uji_tidak_param; ?></td></tr>
                                    <tr><td> </td><td>Pelaporan</td><td><?php echo $pencemaran_air->uji_lapor; ?></td></tr>
                                    <tr><td>j.</td><td colspan="2">Bahan Kimia Pembantu IPAL</td></tr>
                                    <tr><td> </td><td>Nama Bahan Kimia</td><td>Jumlah Pemakaian (ton/hari)</td></tr>
                                    <?php
                                        if (is_array($bahan_kimia_ipal)) {
                                            foreach ($bahan_kimia_ipal as $bk) { ?>
                                                <tr><td> </td><td><?php echo $bk->nama; ?></td><td><?php echo $bk->jml_pemakaian; ?></td></tr>
                                    <?php } } else { ?>
                                                <tr><td> </td><td><?php print_r($bahan_kimia_ipal);//['0']->nama; ?></td><td><?php echo $bahan_kimia_ipal['1']->jml_pemakaian; ?></td></tr>
                                    <?php } ?>
                                    <tr><td>k.</td><td>Kebocoran/Rembesan/Tumpahan/Bypass</td><td><?php echo adaConv($pencemaran_air->bocor); ?></td></tr>
                                    <tr><td> </td><td>Lokasi (apabila ada)</td><td><?php echo $pencemaran_air->bocor_lokasi; ?></td></tr>
                                    <tr><td>l.</td><td>Lain-lain</td><td><?php echo $pencemaran_air->lain_lain; ?></td></tr>
                                </tbody>
                            </table>
                            <?php } else { ?>
                                <table class="table table-th-block table-bordered">
                                    <thead>
                                    <tr>I.   PENGENDALIAN PENCEMARAN Air</tr>
                                    </thead>
                                    <tbody>
                                    <tr><h3 style="color: red">BELUM ADA DATA</h3></tr>
                                    </tbody>
                                </table>
                            <?php } ?>

                            <!--pencemaran udara-->
                            <?php if(isset($pencemaran_udara)) {?>
                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>II.   PENGENDALIAN PENCEMARAN UDARA</tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 1;
                                        foreach ($uji_ambien as $ua) { 
                                            $ual[$i] = $ua;
                                            $i = $i + 1;
                                        } ?>

                                    <tr><td>a.</td><td>Data Kualitas Udara Ambien</td><td><?php echo $ual[1]->lokasi; ?></td><td><?php echo $ual[2]->lokasi; ?></td><td><?php echo $ual[3]->lokasi; ?></td></tr>
                                    <tr><td> </td><td>Pengujian Kualitas </td><td><?php echo adaConv($ual[1]->uji_kualitas); ?></td><td><?php echo adaConv($ual[2]->uji_kualitas); ?></td><td><?php echo adaConv($ual[3]->uji_kualitas); ?></td></tr>
                                    <tr><td> </td><td>Periode pengujian (per 6 bulan)</td><td><?php echo $ual[1]->period; ?></td><td><?php echo $ual[2]->period; ?></td><td><?php echo $ual[3]->period; ?></td></tr>
                                    <tr><td> </td><td>Laboratorium penguji</td><td><?php echo $ual[1]->lab; ?></td><td><?php echo $ual[2]->lab; ?></td><td><?php echo $ual[3]->lab; ?></td></tr>
                                    <tr><td> </td><td>Pemenuhan BM</td><td><?php echo yaConv($ual[1]->bm_pemenuhan); ?></td><td><?php echo yaConv($ual[2]->bm_pemenuhan); ?></td><td><?php echo yaConv($ual[3]->bm_pemenuhan); ?></td></tr>
                                    <tr><td> </td><td>Parameter tidak memenuhi BM</td><td><?php echo $ual[1]->bm_param; ?></td><td><?php echo $ual[2]->bm_param; ?></td><td><?php echo $ual[3]->bm_param; ?></td></tr>
                                    <tr><td> </td><td>Periode tidak memenuhi BM</td><td><?php echo $ual[1]->bm_period; ?></td><td><?php echo $ual[2]->bm_period; ?></td><td><?php echo $ual[3]->bm_period; ?></td></tr>
                                    <tr><td>b.</td><td>Pelaporan pengujian kualitas udara emisi dan ambien</td><td colspan="3"><?php echo $pencemaran_udara->pelaporan_ua_ue; ?></td></tr>
                                    <tr><td>c.</td><td>Lain-lain:</td><td colspan="3"><?php echo $pencemaran_udara->lain_lain; ?></td></tr>
                                </tbody>
                            </table>
                            <?php } else { ?>
                                <table class="table table-th-block table-bordered">
                                    <thead>
                                    <tr>II.   PENGENDALIAN PENCEMARAN UDARA</tr>
                                    </thead>
                                    <tbody>
                                    <tr><h3 style="color: red">BELUM ADA DATA</h3></tr>
                                    </tbody>
                                </table>
                            <?php } ?>

                            <!--pencemaran pb3-->
                            <?php if(isset($pencemaran_pb3)) {?>
                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>III.   PENGELOLAAN LIMBAH PADAT DAN B3</tr>
                                </thead>
                                <tbody>
                                    <tr><td>a.</td><td colspan="4">Jenis limbah B3 yang ditimbulkan</td></tr>
                                    <tr><td> </td><td>Jenis Limbah B3</td><td>Jumlah (ton/hari)</td><td>Pengelolaan</td><td>Pihak Ketiga</td></tr>
                                    <?php
                                        foreach ($jenis_limbah_b3 as $jlb) { ?>
                                            <tr><td> </td><td><?php echo $jlb->jenis; ?></td><td><?php echo $jlb->jumlah; ?></td><td><?php echo $jlb->pengelolaan; ?></td><td><?php echo $jlb->pihak_ke3; ?></td></tr>
                                        <?php } ?>
                                </tbody>
                            </table>
      
                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <?php
                                        $i = 1;
                                        foreach ($tps_b3 as $tb) { 
                                            $tbl[$i] = $tb;
                                            $i = $i + 1;
                                        } ?>
                                    <tr><td>b.</td><td>Penyimpanan</td><td colspan="4"><?php echo $pencemaran_pb3->b3_penyimpanan; ?></td></tr>
                                    <tr><td>c.</td><td colspan="5">TPS Limbah B3 :</td></tr>
                                    <tr><td> </td><td>TPS Limbah B3</td><td>1</td><td>2</td><td>3</td><td>4</td></tr>
                                    <tr><td> </td><td>Perizinan</td><td><?php echo adaConv($tbl[1]->izin); ?></td><td><?php echo adaConv($tbl[2]->izin); ?></td><td><?php echo adaConv($tbl[3]->izin); ?></td><td><?php echo adaConv($tbl[4]->izin); ?></td></tr>
                                    <tr><td> </td><td>Nomor</td><td><?php echo $tbl[1]->izin_no; ?></td><td><?php echo $tbl[2]->izin_no; ?></td><td><?php echo $tbl[3]->izin_no; ?></td><td><?php echo $tbl[4]->izin_no; ?></td></tr>
                                    <tr><td> </td><td>Tanggal</td><td><?php echo $tbl[1]->izin_tgl; ?></td><td><?php echo $tbl[2]->izin_tgl; ?></td><td><?php echo $tbl[3]->izin_tgl; ?></td><td><?php echo $tbl[4]->izin_tgl; ?></td></tr>
                                    <tr><td> </td><td>Jenis limbah B3</td><td><?php echo $tbl[1]->jenis; ?></td><td><?php echo $tbl[2]->jenis; ?></td><td><?php echo $tbl[3]->jenis; ?></td><td><?php echo $tbl[4]->jenis; ?></td></tr>
                                    <tr><td> </td><td colspan="5">Lama Penyimpanan</td></tr>
                                        
                                    <?php

                                        $i = 1;
                                        foreach ($tps_b3 as $tb3) {
                                            $test = unserialize($tb3->lama_spn);
                                            foreach ($test['tps'] as $v) {
                                                $lama[] = $v;
                                            }
                                            $new['lama_tps_'.$i] = $lama;
                                            unset($lama);
                                            $i++;
                                        }

                                        $test = unserialize($tps_b3[0]->lama_spn);

                                        $n = 0;
                                    if ($test['jenis_limbah']!=null) {
                                        foreach ($test['jenis_limbah'] as $key => $value) {
                                            $f = 1;
                                            $newest['jenis'] = $value;
                                            foreach ($new as $x => $y) {
                                                $newest['tps_' . $f] = $y[$n];
                                                $f++;
                                            }
                                            $all_new[] = $newest;

                                            unset($t);
                                            $n++;
                                        }
                                    }
                                    ?>
                                    
                                    <?php if(isset($all_new)) { $idx = "a"; foreach ($all_new as $key => $value) { ?>
                                        <tr><td> </td><td><?php echo $idx.". ".$value['jenis']?></td><td><?php echo $value['tps_1']; ?></td><td><?php echo $value['tps_2']; ?></td><td><?php echo $value['tps_3']; ?></td><td><?php echo $value['tps_4']; ?></td></tr>
                                    <?php $idx++; } } ?>

                                    <tr><td> </td><td>Koordinat S</td><td><?php echo $tbl[1]->koord_derajat_s."&deg".$tbl[1]->koord_jam_s."'".$tbl[1]->koord_menit_s."&quot"; ?></td><td><?php echo $tbl[2]->koord_derajat_s."&deg".$tbl[2]->koord_jam_s."'".$tbl[2]->koord_menit_s."&quot"; ?><td><?php echo $tbl[3]->koord_derajat_s."&deg".$tbl[3]->koord_jam_s."'".$tbl[3]->koord_menit_s."&quot"; ?></td><td><?php echo $tbl[4]->koord_derajat_s."&deg".$tbl[4]->koord_jam_s."'".$tbl[4]->koord_menit_s."&quot"; ?></td>
                                    <tr><td> </td><td>Koordinat E</td><td><?php echo $tbl[1]->koord_derajat_e."&deg".$tbl[1]->koord_jam_e."'".$tbl[1]->koord_menit_e."&quot"; ?></td><td><?php echo $tbl[2]->koord_derajat_e."&deg".$tbl[2]->koord_jam_e."'".$tbl[2]->koord_menit_e."&quot"; ?><td><?php echo $tbl[3]->koord_derajat_e."&deg".$tbl[3]->koord_jam_e."'".$tbl[3]->koord_menit_e."&quot"; ?></td><td><?php echo $tbl[4]->koord_derajat_e."&deg".$tbl[4]->koord_jam_e."'".$tbl[4]->koord_menit_e."&quot"; ?></td>
                                    <tr><td> </td><td>Ukuran</td><td><?php echo $tbl[1]->ukuran; ?></td><td><?php echo $tbl[2]->ukuran; ?></td><td><?php echo $tbl[3]->ukuran; ?></td><td><?php echo $tbl[4]->ukuran; ?></td></tr>
                                    <tr><td> </td><td colspan="5">Kelengkapan:</td></tr>
                                    <tr><td> </td><td>Papan nama dan koordinat</td><td><?php echo adaConv($tbl[1]->ppn_nama_koord); ?></td><td><?php echo adaConv($tbl[2]->ppn_nama_koord); ?></td><td><?php echo adaConv($tbl[3]->ppn_nama_koord); ?></td><td><?php echo adaConv($tbl[4]->ppn_nama_koord); ?></td></tr>
                                    <tr><td> </td><td>simbol dan label</td><td><?php echo adaConv($tbl[1]->simbol_label); ?></td><td><?php echo adaConv($tbl[2]->simbol_label); ?></td><td><?php echo adaConv($tbl[3]->simbol_label); ?></td><td><?php echo adaConv($tbl[4]->simbol_label); ?></td></tr>
                                    <tr><td> </td><td>saluran ceceran air limbah</td><td><?php echo adaConv($tbl[1]->saluran_cecer); ?></td><td><?php echo adaConv($tbl[2]->saluran_cecer); ?></td><td><?php echo adaConv($tbl[3]->saluran_cecer); ?></td><td><?php echo adaConv($tbl[4]->saluran_cecer); ?></td></tr>
                                    <tr><td> </td><td>bak penampung ceceran air limbah</td><td><?php echo adaConv($tbl[1]->bak_cecer); ?></td><td><?php echo adaConv($tbl[2]->bak_cecer); ?></td><td><?php echo adaConv($tbl[3]->bak_cecer); ?></td><td><?php echo adaConv($tbl[4]->bak_cecer); ?></td></tr>
                                    <tr><td> </td><td>kemiringan</td><td><?php echo adaConv($tbl[1]->kemiringan); ?></td><td><?php echo adaConv($tbl[2]->kemiringan); ?></td><td><?php echo adaConv($tbl[3]->kemiringan); ?></td><td><?php echo adaConv($tbl[4]->kemiringan); ?></td></tr>
                                    <tr><td> </td><td>SOP pengelolaan dan tanggap darurat</td><td><?php echo adaConv($tbl[1]->sop_darurat); ?></td><td><?php echo adaConv($tbl[2]->sop_darurat); ?></td><td><?php echo adaConv($tbl[3]->sop_darurat); ?></td><td><?php echo adaConv($tbl[4]->sop_darurat); ?></td></tr>
                                    <tr><td> </td><td>Log book</td><td><?php echo adaConv($tbl[1]->log_book); ?></td><td><?php echo adaConv($tbl[2]->log_book); ?></td><td><?php echo adaConv($tbl[3]->log_book); ?></td><td><?php echo adaConv($tbl[4]->log_book); ?></td></tr>
                                    <tr><td> </td><td>APAR</td><td><?php echo adaConv($tbl[1]->apar_p3k); ?></td><td><?php echo adaConv($tbl[2]->apar_p3k); ?></td><td><?php echo adaConv($tbl[3]->apar_p3k); ?></td><td><?php echo adaConv($tbl[4]->apar_p3k); ?></td></tr>
                                    <tr><td> </td><td>Kotak P3K</td><td><?php echo adaConv($tbl[1]->p3k); ?></td><td><?php echo adaConv($tbl[2]->p3k); ?></td><td><?php echo adaConv($tbl[3]->p3k); ?></td><td><?php echo adaConv($tbl[4]->p3k); ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>e.</td><td>Pengujian Kualitas Limbah B3</td><td><?php echo $pencemaran_pb3->b3_uji; ?></td></tr>
                                    <tr><td> </td><td>Jenis Pengujian </td><td><?php echo $pencemaran_pb3->b3_uji_jenis; ?></td></tr>
                                    <tr><td> </td><td>Parameter Uji</td><td><?php echo $pencemaran_pb3->b3_uji_param; ?></td></tr>
                                    <tr><td> </td><td>Laboratorium Penguji</td><td><?php echo $pencemaran_pb3->b3_uji_lab; ?></td></tr>
                                    <tr><td> </td><td>Waktu Pengujian</td><td><?php echo $pencemaran_pb3->b3_uji_waktu; ?></td></tr>
                                    <tr><td>f.</td><td>Neraca Limbah B3</td><td><?php echo adaConv($pencemaran_pb3->b3_neraca); ?></td></tr>
                                    <tr><td>g.</td><td>Manifest </td><td><?php echo adaConv($pencemaran_pb3->b3_manifest); ?></td></tr>
                                    <tr><td>h.</td><td>Pelaporan Neraca dan Manifest</td><td><?php echo $pencemaran_pb3->b3_lapor_nm; ?></td></tr>
                                    <tr><td>i.</td><td colspan="2">Limbah Padat Lain</td></tr>
                                    <tr><td> </td><th>Jenis</th><th>Jumlah (ton/hari)</th></tr>
                                    <?php 
                                        $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                        $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                        $new = array();
                                        for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                            $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                        }

                                        foreach ($new as $key => $value) {
                                    ?>
                                    <tr><td> </td><td><?php echo $key; ?></td><td><?php echo $value; ?></td></tr>
                                    <?php } ?>
                                    <tr><td> </td><td colspan="2"></td></tr>
                                    <tr><td> </td><td>Pengelolaan</td><td><?php echo $pencemaran_pb3->padat_kelola; ?></td></tr>
                                </tbody>
                            </table>
                            <?php } else { ?>
                                <table class="table table-th-block table-bordered">
                                    <thead>
                                    <tr>III.   PENGELOLAAN LIMBAH PADAT DAN B3</tr>
                                    </thead>
                                    <tbody>
                                    <tr><h3 style="color: red">BELUM ADA DATA</h3></tr>
                                    </tbody>
                                </table>
                            <?php } ?>

                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>IV LAIN-LAIN</td></tr>
                                    <tr><td><?php echo $bap->catatan; ?></td></tr>
                                </tbody>
                            </table>

                            <?php 
                                # function to convert boolean to string (Ada / Tidak Ada)
                                function adaConv($value) {
                                    if ($value == NULL) {
                                        return "";
                                    }
                                    if ($value == 3) {
                                        return "";
                                    }
                                    if ($value == true) {
                                        return "Ada";
                                    } else {
                                        return "Tidak Ada";
                                    }
                                } 

                                # function to convert boolean to string (Ya / Tidak)
                                function yaConv($value) {
                                    if ($value == true) {
                                        return "Ya";
                                    } else {
                                        return "Tidak";
                                    }
                                }
                                ?>
                        </div>
                        <button type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url('backend/bap_rs'); ?>'">Kembali</button>
                    </div>

                    
                </div>
            </div>

                
              <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                <br/><br/> -->

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
	$(document).ready(function () {
        /*warn user when exited from form input*/
        $("#bap-list").click(function () {
            if (!$(this).hasClass('active')) {
                $("#warn").modal("show");
                $("#back-to-form").click(function () {
                    $("#bap-list").removeClass("active");
                    $("#bap-form").addClass("active");
                });
            }
        });

        /* prev next submit handler */
        $(".save-partially").click(function () {
            /*manual validation for save partially*/
            /*show again head-info*/
            $("#save-part-dismiss").hide();
            $("#head-info").show();
            $("#save-part-button").show();
            /*data umum*/
            if($(this).attr('id')=='save-datum') {
                if (!$("input[name=bap_tgl]").val()) { alert("Simpan Sebagian : Tanggal Pengawasan tidak boleh kosong!"); return false };
                if (!$("select[name=id_pegawai]").val()) { alert("Simpan Sebagian : Petugas Pengawas boleh kosong!"); return false };
                if ($("#id_industri").val() == 'Empty') { alert("Simpan Sebagian : Nama Usaha/Kegiatan boleh kosong!"); return false };
                if (!$("input[name=p2_nama]").val()) { alert("Simpan Sebagian : Nama Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=p2_telp]").val()) { alert("Simpan Sebagian : Telepon Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=p2_jabatan]").val()) { alert("Simpan Sebagian : Jabatan Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=jumlah_karyawan]").val()) { alert("Simpan Sebagian : Jumlah Karyawan tidak boleh kosong!"); return false };
                if (!$("input[name=jml_pasien]").val()) { alert("Simpan Sebagian : Jumlah Pasien tidak boleh kosong!"); return false };
                if (!$("input[name=jml_tempat_tidur]").val()) { alert("Simpan Sebagian : Jumlah Tempat Tidur tidak boleh kosong!"); return false };

                if ($("#dok_lingk").val()=="1") {
                    if (!$("input[name=dok_lingk_tahun]").val()) { alert("Simpan Sebagian : Tahun Dokumen Lingkungan tidak boleh kosong!"); return false };
                }

                if ($("#izin_lingk").val()=="1") {
                    if (!$("input[name=izin_lingk_tahun]").val()) { alert("Simpan Sebagian : Tahun Izin Lingkungan tidak boleh kosong!"); return false };
                }

                var info = $("#save-datum").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#data_umum");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else if($(this).attr('id')=='save-pencemaran_air') { /*pencemaran air*/
                if (!$("select[name=bdn_terima]").val()) { alert("Simpan Sebagian : Badan Air Penerima tidak boleh kosong!"); return false };

                if (!$("input[name=izin_debit]").val()) { alert("Simpan Sebagian : Debit tidak boleh kosong!"); return false };

                if (!$("input[name=daur_ulang_debit]").val()) { alert("Simpan Sebagian : Debit tidak boleh kosong!"); return false };

                var info = $("#save-pencemaran_air").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_air");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else  if ($(this).attr('id')=='save-pencemaran_udara') {
                if($("#bmp1").val()=="0") {
                    if (!$("#bm_param1").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Upwind) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period1").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Upwind) tidak boleh kosong!"); return false };
                }

                if($("#bmp2").val()=="0") {
                    if (!$("#bm_param2").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Site) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period2").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Site) tidak boleh kosong!"); return false };
                }

                if($("#bmp3").val()=="0") {
                    if (!$("#bm_param3").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Downwind) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period3").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Downwind) tidak boleh kosong!"); return false };
                }

                var info = $("#save-pencemaran_udara").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_udara");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else if($(this).attr('id')=='save-pencemaran_b3') {
                /*no validation? ok then*/

                var info = $("#save-pencemaran_b3").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_b3");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            }

            $("#save-part").modal("show");

            $("button#button-save-part").unbind().click(function () {
                /*collect ids*/
                var ids = {
                    'id_bap': $("#id_bap").val(),
                    'id_bap_rs': $("#id_bap_rs").val(),
                    'id_pencemaran_air': $("#id_pencemaran_air").val(),
                    'id_pencemaran_udara': $("#id_pencemaran_udara").val(),
                    'id_plb3': $("#id_plb3").val()
                };
                $('.spinner-loader').show();

                $.post('<?php echo base_url("backend/bap_rs/save_part")?>', {
                    data : serializedData,
                    dest : dest,
                    ids : ids
                }, function (ret) {
                    if(ret.status) {
                        console.log(ret);
                        /*append ids to hidden field*/
                        $("#id_bap").val(ret.id.id_bap);
                        $("#id_bap_rs").val(ret.id.id_bap_rs);
                        $("#id_pencemaran_air").val(ret.id.id_pencemaran_air);
                        $("#id_pencemaran_udara").val(ret.id.id_pencemaran_udara);
                        $("#id_plb3").val(ret.id.id_plb3);

                        //$("#save-part").modal("hide");
                        $("#head-info").hide();
                        $("#save-part-button").hide();
                        $("#save-part-dismiss").show();
                        $("#save-part-info").text("Data "+info[1]+" "+info[2]+" berhasil disimpan");
                        var nextId = $elem.next().attr("id");
                        $('[href=#'+nextId+']').tab('show');
                    } else {
                        /*force to check if datum is not yet saved*/
                        $("#save-part").modal("hide");
                        $("#save-datum-first").modal("show");
                    }
                }, "json");

                $('.spinner-loader').hide();
            });
        });
        /* prev next submit handler */

        $('.compare-row').click(function (e) {
            $('input[id=compare_id]').val($(this).data('id'));

            $.post('<?php echo base_url("backend/bap_rs/getComparableStatus")?>', {
                id_bap : $(this).data('id')
            }, function (ret) {
                var info = "";
                $("#compare-modal-body").html("");
                if(!ret.status) {
                    //console.log(ret);

                    info += '<p>Data Belum lengkap! Silahkan edit data terlebih dahulu</p>';
                    var info_bap = ret.bap.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_bap+"&nbsp; Data Umum BAP</span><br>";
                    var info_bap_rs = ret.bap_rs.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_bap_rs+"&nbsp; Data BAP Rumah Sakit</span><br>";
                    var info_pencemaran_air = ret.pencemaran_air.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_pencemaran_air+"&nbsp; Pencemaran Air</span><br>";
                    var info_pencemaran_udara  = ret.pencemaran_udara.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_pencemaran_udara+"&nbsp; Pencemaran Udara</span><br>";
                    var info_pencemaran_pb3 = ret.pencemaran_pb3.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_pencemaran_pb3+"&nbsp; Pencemaran Limbah B3</span><br>";

                    $("#compare-modal-body").html(info);
                    $("#button-compare").hide();
                } else {
                    info += '<p style="color: green">Data siap diperiksa, click tombol compare</p>';
                    $("#compare-modal-body").html(info);
                    $("#button-compare").show();
                }
            }, "json");

            $('#compare').modal('show');
        });

        $.fn.pelanggaran = function(id_bap,industri,tgl) {                         
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/bap_rs/getPelanggaran'); ?>",
                data: 'id_bap='+id_bap+'&industri='+industri+'&tgl='+tgl,
                success: function (response) {
                    console.log(response);
                    var header = '<tr><td>Nama Usaha / Kegiatan</td><td>'+response.industri+'</td></tr>';
                        header += '<tr><td>Tanggal Pengawasan</td><td>'+response.tgl+'</td></tr></tr>';
                    
                    if (response.teguran != null) {
                        var t = '', data = {}, no = 0;
                        for (var i = 0; i < response.teguran.length; i++) {
                            data = response.teguran[i]; 

                            t += '<tr>';
                            t += '<td class="text-center">'+(no+=1)+'</td>';
                            t += '<td>'+data.ket_parameter_bap+'</td>';
                            t += '<td>'+data.nilai_parameter+'</td>';
                            t += '<td>'+data.nilai_parameter_bap+'</td>';
                            t += '<td>'+((data.perbaikan==1) ? "<font color='green'>Sudah diperbaiki</font>" : "<font color='red'>Belum diperbaiki</font>")+'</td>';
                            t += '</tr>';                        
                        }
                        $('#bap-pelanggaran').html(t);
                        $('#bap-ket').html('');
                    } else {
                        t += '<tr><td colspan="5" class="text-center">Tidak Ditemukan Pelanggaran</td><tr>';
                        $('#bap-pelanggaran').html(t);
                    }

                    $('#bap-header').html(header);
                    $('#pelanggaran').modal('show');
                }
            })
            .fail(function(e) {
                console.log(e);
            });  

        }

        var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search_industri_bap/20',
            onSearchStart: function (query) {
                $('#id_industri').val(null);              
            },
            onSelect: function (suggestion) {
                $('#id_industri').val(suggestion.data).change();
                var id_last_input = $('#id_industri').val();
                $('#id_last_input').val(id_last_input);
                console.log($('#id_last_input').val());
            }
        });

        $("select[name=id_industri]").change(function(){
            $('#id_last_input').val($('#id_industri').val());
            $(this).get_detail_industri(this, '#container2');
        });

        $('#confirm').click(function (e) {
            $('#last_input').modal('show');
        });

        $('#si').change(function() {
            if($(this).val()  == 'other') {
                $('#si2').show();
                $('#si2').focus();
            } else {
                $('#si2').hide();
            }
        });

        $('#je').change(function() {
            if($(this).val()  == 'other') {
                $('#je2').show();
                $('#je2').focus();
            } else {
                $('#je2').hide();
            }
        });

        $('#pp').change(function() {
            if($(this).val()  == 'other') {
                $('#pp2').show();
                $('#pp2').focus();
            } else {
                $('#pp2').hide();
            }
        });

        $('#bocor').change(function() {
            if($(this).val()  == true) {
                $('#lok').show();
                $('#lok').focus();
            } else {
                $('#lok').hide();
            }
        });

        $('#pe').change(function() {
            if($(this).val()  == 'Lainnya') {
                $('#pe2').show();
                $('#pe2').focus();
            } else {
                $('#pe2').hide();
            }
        });

        $('.bmp').change(function(event) {
            var num = $(this).prop('name').match(/\d+$/)[0];
            if($(this).val()  == '1') {
                $('#bm_param'+num).val('-');
                $('#bm_period'+num).val('-');
            } else {
                $('#bm_param'+num).val('');
                $('#bm_period'+num).val('');
            }
        });

        $('#uji_limbah').change(function() {
            if($(this).val() == '0') {
                $('#detail_uji').hide();
                $('#pp').val("other");
                $('#pp2').val(null);
                $('#uji_hasil').val("Tidak Memenuhi Baku Mutu");
                $('#uji_lab').val(null);
                $('input[name=uji_tidak_bulan]').val('-');
                $('input[name=uji_tidak_param]').val('-');
            } else {
                $('#pp2').show();
                $('#detail_uji').show();
            }
        });

        $('#ipal').change(function() {
            if($(this).val() == '1') {
                $('#detail_ipal').show();
            } else {
                $('#detail_ipal').hide();
                $('#si').val(null);
                $('#si2').val(null);
                $('#ipal_unit').val(null);
                $('#ipal_kapasitas').val(null);
            }
        });

        $('#izin').change(function() {
            if($(this).val() == '1') {
                $('#detail_izin').show();
            } else {
                $('#detail_izin').hide();
                $('#izin_no').val(null);
                $('#izin_tgl').val(null);
            }
        });

        $('#au').change(function() {
            if($(this).val() == '1') {
                $('#detail_au').show();
            } else {
                $('#detail_au').hide();
                $('#je').val(null);
                $('#je2').val(null);
            }
        });

		$('.delete-row').click(function (e) {
			$('input[id=deleted_id]').val($(this).data('id'));
			$('#delete').modal('show');
            //alert('e: '+e+', id: '+ $(this).data('id'));
		});

		 window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

	});
</script>

<!-- Modal -->
<div class="modal fade" id="pelanggaran" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Pelanggaran BAP </h4>
            </div>
            <div class="modal-body">
                <table class="table table-th-block">
                     <tbody id="bap-header"></tbody>
                </table>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%" class="text-area">No.</th>
                                <th>Nama Item</th>
                                <th>Nilai Item</th>
                                <th>Nilai Item Ketaatan</th>
                                <th>Keterangan</th>                             
                            </tr>
                        </thead>
                        <tbody id="bap-pelanggaran"></tbody>
                    </table>
                    <p id="bap-ket"></p>
                </div><!-- /.table-responsive -->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Bap</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rs/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apa anda yakin ingin menghapus data BAP ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="last_input" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rs/tipe_edit/'); ?>" method="post">
                <div class="modal-body">
                <p>Silahkan kofirmasikan aksi anda dengan memilih salah satu opsi dibawah</p>
                    <p>Jika Anda memilih Ambil data terakhir, data BAP Rumah Sakit terakhir dengan Nama Usaha/Kegiatan yang sudah dipilih akan ditampilkan</p>
                    <input type="hidden" name="id_last_input" id="id_last_input" />
                </div>
                <div class="modal-footer">
                <div class="col-xs-2">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Input Baru</button>
                    </div>
                    <div class="col-xs-10">
                    <button type="submit" class="btn btn-primary" >Ambil data terakhir</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="save-part" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Simpan data BAP Sebagian</h4>
            </div>
            <div class="modal-body">
                <p id="head-info">Data berikut akan disimpan: </p>
                <p id="save-part-info"></p>
                <input type="hidden" name="id" id="id_bap" value="" />
                <input type="hidden" name="id" id="id_bap_rs" value="" />
                <input type="hidden" name="id" id="id_pencemaran_air" value="" />
                <input type="hidden" name="id" id="id_pencemaran_udara" value="" />
                <input type="hidden" name="id" id="id_plb3" value="" />
                <center><img class="spinner-loader" style="display: none;" src="<?php echo base_url('assets/img/spinner.gif') ?>" class="img-responsive" alt="memuat"></center>
            </div>
            <div class="modal-footer">
                <div id="save-part-button">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" id="button-save-part" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;Simpan</button>
                </div>
                <div id="save-part-dismiss" style="display: none">
                    <button type="button" class="btn btn-success" data-dismiss="modal"><i class="glyphicon glyphicon-check"></i>&nbsp;&nbsp;Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="compare" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Status Kelengkapan Data BAP Rumah Sakit</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rs/newCompare'); ?>" method="post">
                <div class="modal-body">
                    <div id="compare-modal-body"></div>
                    <input type="hidden" name="id" id="compare_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" id="button-compare" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;Compare</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="warn" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rs/'); ?>" method="post">
                <div class="modal-body">
                    <p>Anda akan keluar dari form pengisian data! </p>
                    <p>Apakah anda yakin? </p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="back-to-form" class="btn btn-default" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-remove"></i>&nbsp;&nbsp;Keluar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="save-datum-first" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
            </div>
            <div class="modal-body">
                <p>Silahkan simpan Data Umum BAP terlebih dahulu! </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="button-save-datum" class="btn btn-default" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;&nbsp;Ok</button>
            </div>
        </div>
    </div>
</div>