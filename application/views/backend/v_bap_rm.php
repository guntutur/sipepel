 <!-- Document   : v_bap_rm.php  -->
 <!-- Created on : November 11, 2014 xx:xx  -->
 <!-- Author     : guntutur@gmail.com  -->
 <!-- Description: View for BAP Rumah Makan  -->

<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<!-- <script type="text/javascript" src="../assets/js/bootstrap-multiselect.js"></script> -->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">BAP Rumah Makan&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Rumah Makan</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen BAP</li>
                <li class="active">BAP Rumah Makan</li>
            </ol>
            <!-- End breadcrumb -->

            <?php 
                echo $this->session->flashdata('msg'); 
                echo $this->session->flashdata('error');
            ?>

            <!-- BEGIN DATA TABLE -->
            <div class="the-box no-padding">

                <ul class="nav nav-tabs" role="tablist">
                    <li id="bap-list" class="<?php echo (($tab_list!= '') ? 'active' : ''); ?>" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>"><a href="#" role="tab" data-toggle="tab"><i class="fa fa-list"></i>&nbsp;&nbsp;Daftar BAP</a></li>
                    <li id="bap-form" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>"><a href="#add_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Tambah BAP</a></li>
                    <li class="<?php echo (($tab_view!= '') ? 'active' : ''); ?>" style="<?php echo (($tab_view== '') ? 'display:none' : ''); ?>"><a href="#view" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Lihat BAP</a></li>
                    <li class="<?php echo (($tab_edit!= '') ? 'active' : ''); ?>" style="<?php echo (($tab_edit== '') ? 'display:none' : ''); ?>"><a href="#edit" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane <?php echo (($tab_list!= '') ? 'active' : ''); ?>" id="list" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>">
                         <div class="table-responsive">
                            <table class="table table-striped table-hover" id="datatable-example">
                                <thead class="the-box dark full">
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Nama Rumah Makan</th>
                                        <th>Tanggal Pengawasan</th>
                                        <th>Kelengkapan Data</th>
                                        <th>Status Periksa (by system)</th>
                                        <th>Jumlah Item Tidak Taat</th>
                                        <th class="text-right"></th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($bap_rm as $b) {
                                    ?>
                                    <tr class="gradeA">
                                        <td class="text-center"><?php echo ++$no; ?></td>
                                        <td><?php echo $b->nama_industri; ?></td>
                                        <td><?php echo $b->bap_tgl; ?></td>
                                        <td><?php echo $b->compare_status==0 ? "<span class='label label-danger'>Tidak Lengkap</span>" : "<span class='label label-success'>Lengkap</span>"; ?></td>
                                        <td><?php echo $b->is_compared==0 ? "<span class='label label-danger'>Belum diperiksa</span>" : "<span class='label label-success'>Sudah diperiksa</span>"; ?></td>
                                        <td><?php echo $b->is_compared==0 ? "-" : $b->jml_teguran; ?></td>
                                        <td class="text-right">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    <i class="fa fa-cogs"></i>
                                                </button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li><a href="#" class="compare-row" data-id="<?php echo $b->id_bap; ?>">Periksa BAP</a></li>
                                                    <li>
                                                        <form action="<?php echo base_url('/backend/bap_rm/view'); ?>" method="post">
                                                            <a href="#" onclick="parentNode.submit();" style="padding: 5px 15px;color: #333;display: block;">Lihat Data</a>
                                                            <input type="hidden" name="id_bap" value="<?php echo $b->id_bap; ?>"/>
                                                        </form>
                                                    </li>
                                                    <li><a onclick="$(this).edit(<?php echo $b->id_bap.',\''.$b->nama_industri.'\',\''.$b->bap_tgl.'\''; ?>)">Lihat Pelanggaran</a></li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <form action="<?php echo base_url('/backend/bap_rm/tipe_edit/'. 1); ?>" method="post">
                                                            <a href="#" onclick="parentNode.submit();" style="padding: 5px 15px;color: #333;display: block;">Ubah Data</a>
                                                            <input type="hidden" name="id_bap" value="<?php echo $b->id_bap; ?>"/>
                                                        </form>
                                                    </li>
                                                    <li><a href="#" class="delete-row" data-id="<?php echo $b->id_bap; ?>">Hapus Data</a></li>
                                                </ul>
                                            </div>
                                        </td>                                   
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="add-new" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>">
                        <div class="row">
                            <div class="modal-body">  
                                <div class="col-sm-12">
                                        <div class="col-sm-6 text-right">
                                            <h4 class="light">Form BAP Rumah Makan</h4>
                                        </div>
                                        <div class="col-sm-6 text-left">
                                            <button type="button" class="btn btn-primary btn-rounded-lg" onclick="window.location='<?php echo base_url('assets/form/BAPRumahMakan.xlsx'); ?>'">Unduh Form</button>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <form role="form" action="<?php echo base_url('/backend/bap_rm/insert') ?>" method="post" id="form_add" enctype="multipart/form-data">
                            <div class="modal-body">                                                                            
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>Tanggal Pengawasan</label>
                                    <input type="text" name="" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>Pukul</label>
                                    <div class="bfh-timepicker">
                                        <input id="timepick1" type="text" name="bap_jam" class="form-control input-sm bfh-timepicker">
                                    </div>
                                    <!-- <div class="input-group input-append bootstrap-timepicker">
                                        <input id="timepick" type="text" name="bap_jam" class="form-control input-sm timepicker">
                                        <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                    </div> -->
                                </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label>Petugas Pengawas</label>
                                        <select class="form-control input-sm" name="">
                                            <option value=""></option>
                                            <?php 
                                                foreach ($pegawai as $p) {
                                                    echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Beserta Anggota Pengawas:</label>
                                        <select class="form-control input-sm multiselect" name="petugas_pengawas[]" id="ini_id_petugas_bap1" multiple="multiple" style="border: 0!important;">
                                            <?php 
                                                foreach ($pegawai as $p) {                                                    
                                                    echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                </div>
                                <fieldset>
                                <legend>Lokasi Pengawasan / Pembinaan</legend>    
                                    <div class="form-group ">
                                        <label>Nama Usaha / Kegiatan</label>
                                        <select class="form-control input-sm" onchange="$(this).get_detail_industri(this, '#container1');" name="">
                                        <option value=""></option>
                                            <?php 
                                                 foreach ($industri as $i) {
                                                     echo "<option value='$i->id_industri'>$i->nama_industri</option>";
                                                 }?>
                                        </select>
                                    </div>
                                    <!-- dari sini sampe tutup div tempat alamat di hardcode -->
                                    <div id="container1"></div>

                                </fieldset> 
                                <fieldset>
                                    <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                    <div class="form-group ">
                                        <label>Nama</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Telepon</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div>
                                    <div class="form-group ">
                                        <label>Jabatan</label>
                                        <input type="text" name="" id="" class="form-control input-sm has-feedback" />
                                    </div> 
                                </fieldset>
                                    
                                        <div class="form-group">
                                            <h4 class="light">Upload Form BAP Rumah Makan</h4>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" readonly="">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-default btn-file">
                                                        Browse… <input type="file" name="file" accept=".xlsx">
                                                    </span>
                                                </span>
                                            </div><!-- /.input-group -->
                                        </div>
                                    
                            </div>
                    
                            
                                <div class="form-group ">
                                    <input type="hidden" id="edit_id" name="id" value="" />
                                    <button type="submit" name="register" class="btn btn-primary">Simpan</button>
                                </div>
                            
                        </form>
                    </div>

                    <div class="tab-pane" id="add_new" style="<?php echo (($tab_list== '') ? 'display:none' : ''); ?>">
                        <div class="row" style="margin-top: 10px;">       
                            <div class="col-xs-3" id="vertical_tab" style="display: none"> <!-- required for floating -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#data_umum" id="Data Umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                <li><a href="#pencemaran_air" id="Pencemaran Air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                <li><a href="#pencemaran_b3" id="Limbah Padat dan B3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                <li><a href="#lain_lain" id="Keterangan Tambahan" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Lain-lain</a></li>
                              </ul>
                            </div>
                            <div class="col-xs-12">
                              <!-- Tab panes -->
                            <form role="form" action="<?php echo base_url('/backend/bap_rm/insert_from_form') ?>" method="post" id="add_form" enctype="multipart/form-data">
                              <div class="tab-content">
                                <div class="tab-pane active form-validated" id="data_umum">
                                   <h4 class="page-tab"><i class="fa fa-newspaper-o icon-sidebar"></i>Data Umum Usaha/Kegiatan</h4>
                                   <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label>Tanggal Pengawasan</label>
                                        <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="YYYY-MM-DD">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                            <input id="timepick2" type="text" name="bap_jam" class="form-control input-sm bfh-timepicker">
                                        </div>
                                            <!-- <div class="input-group input-append bootstrap-timepicker">
                                                <input type="text" name="bap_jam" class="form-control input-sm timepicker">
                                                <span class="input-group-addon add-on"><i class="fa fa-clock-o"></i></span>
                                            </div> -->
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label>Petugas Pengawas</label>
                                                <select class="form-control input-sm" name="id_pegawai">
                                                    <option value=""></option>
                                                    <?php 
                                                        foreach ($pegawai as $p) {
                                                            echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                        }?>
                                                </select>
                                        </div>
                                        <div class="form-group col-lg-6">
                                        <label>Beserta Anggota Pengawas:</label>
                                        <select class="form-control input-sm multiselect" name="petugas_pengawas[]" id="ini_id_petugas_bap2" multiple="multiple"  style="border: 0!important">
                                            <?php 
                                                foreach ($pegawai as $p) {                                                    
                                                    echo "<option value='$p->id_pegawai'>$p->nama_pegawai</option>";
                                                }?>
                                        </select>
                                    </div>
                                    </div>
                                <fieldset>
                                    <legend>Lokasi Pengawasan / Pembinaan</legend>    
                                    <h4>Lokasi Pengawasan / Pembinaan</h4>
                                        <div class="form-group status">                        
                                        <label>Nama Usaha/Kegiatan</label>
                                        <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                        <option value="Empty">&nbsp;</option>
                                                        <?php
                                                            foreach ($industri as $value) {
                                                                echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                            <!-- <input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                            <input type="hidden" id="id_industri" name="id_industri" value="0"/> -->
                                        </div>
                                    <div id="container2"></div>
                                </fieldset>
                                    <div class="form-group ">
                                        <label>Jumlah Meja/Kursi</label>
                                        <input type="text" value="" name="jml_meja" id="jml_meja" class="form-control input-sm has-feedback"/>
                                    </div>
                                    <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="datatable-example">
                                    <thead class="the-box dark full">
                                        <tr>
                                        <!-- <div class="form-group "> -->
                                            <th>Dokumen Lingkungan</th>
                                            <th>Izin Lingkungan</th>
                                            <th>Izin Usaha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tr>
                                        <td>
                                                <!-- <div class="radio"> -->
                                            <label>
                                                <input type="radio" name="dl" id="dl1" value="ada">Ada
                                            </label>
                                            <label>
                                                <input type="radio" name="dl" id="dl2" value="tidak ada" checked>Tidak Ada
                                            </label>
                                                <!-- </div> -->
                                        </td>
                                            <!-- </div> -->
                                            <!-- <div class="form-group "> -->
                                        <td>
                                                <!-- <div class="radio"> -->
                                            <label>
                                                <input type="radio" name="il" id="il1" value="Ada">Ada
                                            </label>
                                            <label>
                                                <input type="radio" name="il" id="il2" value="Tidak ada" checked>Tidak Ada
                                            </label>
                                                <!-- </div> -->
                                        </td>
                                        <td>
                                                <!-- <div class="radio"> -->
                                            <label>
                                                <input type="radio" name="iu" id="iu1" value="ada">Ada
                                            </label>
                                            <label>
                                                <input type="radio" name="iu" id="iu2" value="tidak ada" checked>Tidak Ada
                                            </label>
                                                <!-- </div> -->
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="form-group " id="jdl" name="jdl" style="display:none">
                                                        <!-- <div class="form-group "> -->
                                                    <label>Jenis Dokumen</label>
                                                    <select class="form-control input-sm" name="jdl_name" id="jdl_name">
                                                        <option value='' selected="true">--  Pilih Salah Satu --</option>
                                                        <option value='Amdal'>Amdal</option>
                                                        <option value='UKL-UPL'>UKL-UPL</option>
                                                        <option value='DPLH'>DPLH</option>
                                                        <option value='SPPL'>SPPL</option>
                                                    </select>
                                                        <!-- </div> -->
                                                        <!-- <div class="form-group "> -->
                                                    <label>Tahun</label>
                                                    <input type="text" name="dlt" id="dlt" class="form-control input-sm has-feedback" />
                                                        <!-- </div> -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group " id="il_tahun" style="display:none">
                                                    <label>Tahun</label>
                                                    <input type="text" name="il_tahun" id="ilt" class="form-control input-sm has-feedback" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group " id="iu_tahun" style="display:none">
                                                    <label>Tahun</label>
                                                    <input type="text" name="iu_tahun" id="iut" class="form-control input-sm has-feedback" />
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <div class="form-group ">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" name="jml_karyawan" id="" class="form-control input-sm has-feedback" />
                                            </div>
                                        </tr>
                                        <!-- </div> -->
                                    </tbody>
                                    </table>
                                    </div>
                                    <fieldset>
                                        <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                        <div class="form-group ">
                                            <label>Nama</label>
                                            <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" />
                                        </div>
                                        <div class="form-group ">
                                            <label>Telepon</label>
                                            <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" />
                                        </div>
                                        <div class="form-group ">
                                            <label>Jabatan</label>
                                            <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" />
                                        </div> 
                                    </fieldset>

                                    <fieldset>
                                        <legend>Lain-lain</legend>
                                        <div class="form-group ">
                                            <textarea class="form-control input-sm no-resize bold-border" name="lain_lain_bap" style="height: 180px;"></textarea>
                                        </div>
                                    </fieldset>

                                    <div class="row"> <!-- navigation -->
                                        <div class="col-sm-6 text-left">
                                            <input id="save-datum" class="btn btn-md btn-success save-partially" value="Simpan Data Umum"/>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <a id="confirm" class="btn btn-warning NextStep">Pencemaran Air<i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pencemaran_air">          
                                    <h4 class="page-tab">Pengendalian Pencemaran Air</h4>
                                    <fieldset>
                                    <div class="form-group">
                                        <label>a. Sumber Air dan Penggunaan</label>
                                    <!-- </div> -->
                                    <div class="form-group">
                                    <div class="form-group col-lg-6">
                                        <label>Air Tanah</label>
                                        <input type="text" name="air_tanah" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" />
                                        <div class="radio">
                                        <label>Perizinan    : </label>
                                                <label>
                                                    <input type="radio" name="izin_air_tanah" value="ada">Ada
                                                </label>
                                                <label>
                                                    <input type="radio" name="izin_air_tanah" value="tidak ada" checked>Tidak Ada
                                                </label>
                                            </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Air Permukaan</label>
                                        <input type="text" name="air_permukaan" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" />
                                        <div class="radio">
                                        <label>Perizinan    : </label>
                                                <label>
                                                    <input type="radio" name="izin_air_permukaan" value="ada">Ada
                                                </label>
                                                <label>
                                                    <input type="radio" name="izin_air_permukaan" value="tidak ada" checked>Tidak Ada
                                                </label>
                                            </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Air PDAM</label>
                                        <input type="text" name="air_pdam" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" />
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Air Lain-lain</label>
                                        <input type="text" name="air_lain" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" />
                                    </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6">
                                            <label>b. Sumber Air Limbah</label>
                                            <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                            <!-- <option value=''></option> -->
                                                <?php
                                                    foreach ($limb_sumber as $l) {
                                                        echo '<option value='.$l->ket.'>'.$l->ket.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>c. Badan Air Penerima</label>
                                            <select name="bdn_terima" class="form-control input-sm ">
                                            <option value=''></option>
                                                <?php
                                                    foreach ($bdn_terima as $bdn) {
                                                        echo "<option value='$bdn->ket'>$bdn->ket</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="form-group ">
                                            <label>d. Sarana Pengolahan Air Limbah</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sarolim" id="sarolim1" value="Ada" checked>Ada
                                                </label>
                                                <label>
                                                    <input type="radio" name="sarolim" id="sarolim2" value="Tidak ada">Tidak Ada
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group " id="sarolim">
                                        <div class="form-group">
                                            <label>Jenis Sarana</label>
                                            <input type="text" name="sarana_jenis" id="sarana_jenis" class="form-control input-sm has-feedback" />
                                        </div>
                                        <div class="form-group ">
                                            <label>Kapasitas</label>
                                            <input type="text" name="kapasitas" id="kapasitas" class="form-control input-sm has-feedback" />
                                        </div>
                                        <div class="form-group ">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label>Koordinat Outlet S</label>
                                                    <div class="form-group status">
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" name="xkoord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" name="xkoord_outlet_jam_s" id="jam_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">'</span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" name="xkoord_outlet_menit_s" id="menit_s" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">"</span>
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Koordinat Outlet E</label>
                                                    <div class="form-group status">
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" name="xkoord_outlet_derajat_e" id="derajat_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" name="xkoord_outlet_jam_e" id="jam_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">'</span>
                                                                </div> 
                                                            </div> 
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
                                                                <div class="input-group input-group-sm">                                
                                                                    <input type="text" name="xkoord_outlet_menit_e" id="menit_e" class="form-control input-sm has-feedback" placeholder="0"  />                                
                                                                    <span class="input-group-addon">"</span>
                                                                </div> 
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </fieldset>
                                    <div class="row"> <!-- navigation -->
                                        <div class="col-sm-4 text-left">
                                            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Data Umum</a>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                            <input id="save-pencemaran_air" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Air"/>
                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <a class="btn btn-warning NextStep">Pencemaran Udara<i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pencemaran_udara">
                                            <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                            
                                            <div class="form-group ">
                                                <label>a. Data Kualitas Udara Ambien</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th class="text-center">Upwind</th>
                                                            <th class="text-center">Site</th>
                                                            <th class="text-center">Downwind</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>Pengujian Kualitas</label></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas1"><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas2"><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                                                            <td><select class="form-control input-sm" name="uji_kualitas3"><option value="ada">Ada</option><option value="tidak ada">Tidak Ada</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                            <td><select class="form-control input-sm" name="period1"><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period2"><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                                                            <td><select class="form-control input-sm" name="period3"><option value="Rutin">Rutin</option><option value="Tidak Rutin">Tidak Rutin</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Laboratorium Pengujian</label></td>
                                                            <?php
                                                                for ($i=1; $i<4; $i++) {
                                                                    echo "<td><select name='lab".$i."' class='form-control input-sm'>";
                                                                    echo "<option value=''></option>";
                                                                        foreach ($lab as $key) {
                                                                            echo "<option value=".$key->nama_lab.">".$key->nama_lab."</option>";
                                                                        }
                                                                    echo '</select></td>';
                                                                }
                                                            ?>
                                                        </tr>
                                                        <!-- <tr>
                                                            <td><label>Laboratorium Penguji</label></td>
                                                            <td><input type="text" name="lab1" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" name="lab2" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" name="lab3" class="form-control input-sm has-feedback " /></td>
                                                        </tr> -->
                                                        <tr>
                                                            <td><label>Pemenuhan BM</label></td>
                                                            <td><select class="form-control input-sm bmp" id="bmp1" name="bm_pemenuhan1"><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                                                            <td><select class="form-control input-sm bmp" id="bmp2" name="bm_pemenuhan2"><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                                                            <td><select class="form-control input-sm bmp" id="bmp3" name="bm_pemenuhan3"><option value="Tidak">Tidak</option><option value="Ya">Ya</option></select></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" id="bm_param1" name="bm_param1" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" id="bm_param2" name="bm_param2" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" id="bm_param3" name="bm_param3" class="form-control input-sm has-feedback " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Periode Tidak Memenuhi BM</label></td>
                                                            <td><input type="text" id="bm_period1" name="bm_period1" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" id="bm_period2" name="bm_period2" class="form-control input-sm has-feedback " /></td>
                                                            <td><input type="text" id="bm_period3" name="bm_period3" class="form-control input-sm has-feedback " /></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
   
                                            <div class="form-group ">
                                                <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                    <option value="Rutin">Rutin</option>
                                                    <option value="Tidak Rutin">Tidak Rutin</option>
                                                    <option value="Tidak Melaporkan">Tidak Melaporkan</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. Lain-lain</label>
                                                <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" />
                                            </div>

                                    <div class="row"> <!-- navigation -->
                                        <div class="col-sm-4 text-left">
                                            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Air</a>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                            <input id="save-pencemaran_udara" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran Udara"/>
                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <a class="btn btn-warning NextStep">Pencemaran PB3<i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="pencemaran_b3">
                                    <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
                                    <!-- <div class="row"> -->
                                    <table class="table table-th-block">
                                        <thead>
                                            <tr>
                                                <th class="text-center">a. Jenis</th>
                                                <th class="text-center">b. Jumlah (ton/hari)</th>
                                                <th class="text-center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" /></td></td>
                                                <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" /></td>
                                                <td class="col-lg-2">
                                                    <div class=" text-right">
                                                        <div class="btn-group" style="padding-right: 17px;">
                                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                        <!-- <div class="form-group">
                                            <label>a. Jenis</label>
                                            <input type="text" name="limbah_jenis" id="" class="form-control input-sm has-feedback" />
                                        </div>
                                        <div class="form-group">
                                            <label>b. Jumlah</label>
                                            <input type="text" name="limbah_jml" id="" class="form-control input-sm has-feedback" />
                                        </div> -->
                                        <div class="form-group ">
                                            <label>c. Sarana Pengelolaan</label>
                                            <div class="form-group">
                                                <div class="form-group col-sm-6">
                                                    <label>1. Tong/Bak Sampah</label>
                                                    <input type="text" name="padat_sarana_tong" id="" class="form-control input-sm has-feedback" placeholder="unit" />
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label>2. TPS</label>
                                                    <input type="text" name="padat_sarana_tps" id="" class="form-control input-sm has-feedback" placeholder="unit" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label> Pengelolaan</label>
                                                <select class="form-control input-sm" id="pjpl" name="padat_kelola">
                                                <?php
                                                            foreach ($pengelolaan as $pk) {
                                                            echo "<option value='$pk->ket'>$pk->ket</option>";
                                                        }
                                                        ?>
                                                </select>
                                                <input type="text" name="padat_kelola_lain" id="pjpl2" class="form-control input-sm has-feedback" style="display:none"/>
                                            </div>
                                        <fieldset>
                                            <legend>e. Lain-lain</legend>
                                            <div class="form-group ">
                                                <textarea class="form-control input-sm no-resize bold-border" name="lain_lain_padat"></textarea>
                                            </div>
                                        </fieldset>
                                    <div class="row">
                                        <div class="col-sm-4 text-left">
                                            <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Pencemaran Udara</a>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                            <input id="save-pencemaran_b3" class="btn btn-md btn-success save-partially" value="Simpan Pencemaran LB3"/>
                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <!--<a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>-->
                                            <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>
                                </div>
                              </div>
                            </form>
                        </div>
                        </div>
                    </div>

                    <div class="tab-pane <?php echo (($tab_view!= '') ? 'active' : ''); ?>" id="view" style="<?php echo (($tab_view== '') ? 'display:none' : ''); ?>">
                         <div class="modal-body">
                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>Tanggal Input Data</td><td><?php echo $bap->tgl_pembuatan; ?></td></tr>
                                    <tr><td>Tanggal Pengawasan</td><td><?php echo date('l', strtotime($bap->bap_tgl)).", ".$bap->bap_tgl; ?></td></tr>
                                    <tr><td>Pukul</td><td><?php echo date('H:i' ,strtotime($bap->bap_jam)); ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>Pengawas</tr>
                                </thead>
                                <tbody>
                                    <tr><td>Nama</td><td><?php echo $pegawai->nama_pegawai; ?></td></tr>
                                    <tr><td>Instansi</td><td><?php echo $pegawai->instansi; ?></td></tr>
                                    <tr><td>NIP.</td><td><?php echo $pegawai->nip; ?></td></tr>
                                    <tr><td>Pangkat/Gol</td><td><?php echo $pegawai->pangkat_gol; ?></td></tr>
                                    <tr><td>Jabatan</td><td><?php echo $pegawai->jabatan; ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>Anggota pengawas</tr>
                                    <tr><td>Nama</td><td>NIP</td><td>Jabatan</td></tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($petugas_bap as $pb) { ?>
                                            <tr><td><?php echo $pb->nama_pegawai; ?></td><td><?php echo $pb->nip; ?></td><td><?php echo $pb->jabatan; ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            
                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>Nama Usaha / Kegiatan</td><td><?php echo $industri->nama_industri; ?></td></tr>
                                    <tr><td>Alamat</td><td><?php echo $industri->alamat; ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>Penanggung Jawab Usaha/Kegiatan</tr>
                                </thead>
                                <tbody>
                                    <tr><td>Nama</td><td><?php echo $bap->p2_nama; ?></td></tr>
                                    <tr><td>Telepon</td><td><?php echo $bap->p2_telp; ?></td></tr>
                                    <tr><td>Jabatan</td><td><?php echo $bap->p2_jabatan; ?></td></tr>
                                </tbody>
                            </table>

                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>DATA UMUM</tr>
                                </thead>
                                <tbody>
                                    <tr><td>a.</td><td>Jenis Usaha/Kegiatan</td><td colspan="2">Rumah Makan</td></tr>
                                    <tr><td>b.</td><td>Pimpinan Usaha/Kegiatan</td><td colspan="2"><?php echo $industri->pimpinan; ?></td></tr>
                                    <tr><td>c.</td><td>Jumlah Karyawan</td><td colspan="2"><?php echo $bap_rm->jumlah_karyawan; ?></td></tr>
                                    <tr><td>d.</td><td>Hari Kerja </td><td colspan="2"><?php echo $industri->hari_kerja_bulan; ?> Hari/Bulan</td></tr>
                                    <tr><td>e.</td><td>Jam Kerja</td><td colspan="2"><?php echo $industri->jam_kerja_hari; ?> Jam/Hari</td></tr>
                                    <tr><td>f.</td><td>Jumlah Meja/Kursi</td><td colspan="2"><?php echo $bap_rm->jml_meja; ?></td></tr>
                                    <tr><td>g.</td><td>Izin Usaha</td><td><?php echo adaConv($bap_rm->izin_usaha) ?></td><td>Tahun: <?php echo $bap_rm->izin_usaha_tahun; ?></td></tr>
                                    <tr><td>h.</td><td>Luas Area / Bangunan</td><td colspan="2"><?php echo $industri->luas_area." / ".$industri->luas_bangunan; ?> Ha</td></tr>
                                    <?php 
                                    // $class = ($bap->dok_lingk == '0') ? 'class="danger"' : ''; 
                                    ?>
                                    <tr <?php // echo $class; ?>><td>i.</td><td>Dokumen Lingkungan </td><td colspan="2"><?php echo adaConv($bap->dok_lingk) ?></td></tr>
                                    <tr><td> </td><td>Jenis:</td><td><?php echo $bap->dok_lingk_jenis; ?></td><td>Tahun: <?php echo $bap->dok_lingk_tahun; ?></td></tr>
                                    <?php 
                                    // $class = ($bap->izin_lingk == '0') ? 'class="danger"' : ''; 
                                    ?>
                                    <tr <?php // echo $class; ?>><td>j.</td><td>Izin Lingkungan</td><td><?php echo adaConv($bap->izin_lingk) ?></td><td>Tahun: <?php echo $bap->izin_lingk_tahun; ?></td></tr>
                                </tbody>
                            </table>

                            <h4>RINGKASAN TEMUAN LAPANGAN:</h4>

                             <!--pencemaran air-->
                             <?php if(isset($pencemaran_air)) {?>
                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>I.   PENGENDALIAN PENCEMARAN AIR</tr>
                                </thead>
                                <tbody>
                                    
                                    <tr><td>a.</td><td colspan="2">Sumber Air dan Penggunaan</td></tr>
                                    <tr><td> </td><td>Air Tanah</td><td><?php echo $pencemaran_air->ambil_air_tanah; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>Perizinan</td><td><?php echo adaConv($pencemaran_air->ambil_air_tanah_izin) ?></td></tr>
                                    <tr><td> </td><td>Air Permukaan</td><td><?php echo $pencemaran_air->ambil_air_permukaan; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>Perizinan</td><td><?php echo adaConv($pencemaran_air->ambil_air_permukaan_izin) ?></td></tr>
                                    <tr><td> </td><td>PDAM</td><td><?php echo $pencemaran_air->ambil_air_pdam; ?> m3/hari</td></tr>
                                    <tr><td> </td><td>Lain-lain</td><td><?php echo $pencemaran_air->ambil_air_lain; ?> m3/hari</td></tr>
                                    <tr><td>b.</td><td>Sumber Air Limbah</td><td><?php echo $pencemaran_air->limb_sumber; ?></td></tr>
                                    <tr><td>c.</td><td>Badan Air Penerima</td><td><?php echo $pencemaran_air->bdn_terima; ?></td></tr>
                                    <?php 
                                    // $class = ($pencemaran_air->sarana_olah_limbah == '0') ? 'class="danger"' : ''; 
                                    ?>
                                    <tr <?php // echo $class; ?>><td>d.</td><td>Sarana Pengolahan Air Limbah</td><td><?php echo adaConv($pencemaran_air->sarana_olah_limbah) ?></td></tr>
                                    <tr><td> </td><td>Jenis Sarana</td><td><?php echo $pencemaran_air->sarana_jenis; ?></td></tr>
                                    <tr><td> </td><td>Kapasitas</td><td><?php echo $pencemaran_air->sarana_kapasitas; ?></td></tr>
                                    <tr><td> </td><td>Koordinat Outlet</td><td>S: <?php echo $pencemaran_air->sarana_koord_derajat_s."&deg".$pencemaran_air->sarana_koord_jam_s."'".$pencemaran_air->sarana_koord_menit_s."&quot"; ?></td></tr>
                                    <tr><td> </td><td></td><td>E: <?php echo $pencemaran_air->sarana_koord_derajat_e."&deg".$pencemaran_air->sarana_koord_jam_e."'".$pencemaran_air->sarana_koord_menit_e."&quot"; ?></td></tr>
                                    <tr><td>e.</td><td>Lain-lain</td><td><?php echo $pencemaran_air->lain_lain; ?></td></tr>
                                </tbody>
                            </table>
                             <?php } else { ?>
                                 <table class="table table-th-block table-bordered">
                                     <thead>
                                     <tr>I.   PENGENDALIAN PENCEMARAN Air</tr>
                                     </thead>
                                     <tbody>
                                     <tr><h3 style="color: red">BELUM ADA DATA</h3></tr>
                                     </tbody>
                                 </table>
                             <?php } ?>

                             <!--pencemaran udara-->
                             <?php if(isset($pencemaran_udara)) {?>
                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>II.   PENGENDALIAN PENCEMARAN UDARA</tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i = 1;
                                        foreach ($uji_ambien as $ua) { 
                                            $ual[$i] = $ua;
                                            $i = $i + 1;
                                        } ?>

                                    <tr><td>a.</td><td>Data Kualitas Udara Ambien</td><td><?php echo $ual[1]->lokasi; ?></td><td><?php echo $ual[2]->lokasi; ?></td><td><?php echo $ual[3]->lokasi; ?></td></tr>
                                    <tr><td> </td><td>Pengujian Kualitas </td><td><?php echo adaConv($ual[1]->uji_kualitas) ?></td><td><?php echo adaConv($ual[2]->uji_kualitas) ?></td><td><?php echo adaConv($ual[3]->uji_kualitas) ?></td></tr>
                                    <tr><td> </td><td>Periode pengujian (per 6 bulan)</td><td><?php echo $ual[1]->period; ?></td><td><?php echo $ual[2]->period; ?></td><td><?php echo $ual[3]->period; ?></td></tr>
                                    <tr><td> </td><td>Laboratorium penguji</td><td><?php echo $ual[1]->lab; ?></td><td><?php echo $ual[2]->lab; ?></td><td><?php echo $ual[3]->lab; ?></td></tr>
                                    <tr><td> </td><td>Pemenuhan BM</td><td><?php echo yaConv($ual[1]->bm_pemenuhan) ?></td><td><?php echo yaConv($ual[2]->bm_pemenuhan) ?></td><td><?php echo yaConv($ual[3]->bm_pemenuhan) ?></td></tr>
                                    <tr><td> </td><td>Parameter tidak memenuhi BM</td><td><?php echo $ual[1]->bm_param; ?></td><td><?php echo $ual[2]->bm_param; ?></td><td><?php echo $ual[3]->bm_param; ?></td></tr>
                                    <tr><td> </td><td>Periode tidak memenuhi BM</td><td><?php echo $ual[1]->bm_period; ?></td><td><?php echo $ual[2]->bm_period; ?></td><td><?php echo $ual[3]->bm_period; ?></td></tr>
                                    <tr><td>b.</td><td>Pelaporan pengujian kualitas udara emisi dan ambien</td><td colspan="3"><?php echo $pencemaran_udara->pelaporan_ua_ue; ?></td></tr>
                                    <tr><td>c.</td><td>Lain-lain:</td><td colspan="3"><?php echo $pencemaran_udara->lain_lain; ?></td></tr>
                                </tbody>
                            </table>
                             <?php } else { ?>
                                 <table class="table table-th-block table-bordered">
                                     <thead>
                                     <tr>II.   PENGENDALIAN PENCEMARAN UDARA</tr>
                                     </thead>
                                     <tbody>
                                     <tr><h3 style="color: red">BELUM ADA DATA</h3></tr>
                                     </tbody>
                                 </table>
                             <?php } ?>

                             <!--pencemaran pb3-->
                             <?php if(isset($pencemaran_pb3)) {?>
                            <table class="table table-th-block table-bordered">
                                <thead>
                                    <tr>III.   PENGELOLAAN LIMBAH PADAT</tr>
                                </thead>
                                <tbody>
                                    <tr><td>&nbsp;</td><th>a. Jenis</th><th>b. Jumlah (ton/hari)</th></tr>
                                    <?php 
                                        $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                        $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                        $new = array();
                                        for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                            $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                        }

                                        foreach ($new as $key => $value) {
                                    ?>
                                    <tr><td>&nbsp;</td><td><?php echo $key; ?></td><td><?php echo $value; ?></td></tr>
                                    <?php } ?>
                                    <tr><td></td><td colspan="3"></td></tr>
                                    <tr><td>c.</td><td>Sarana Pengelolaan</td><td>1. Tong/Bak Sampah</td><td><?php echo $pencemaran_pb3->padat_sarana_tong; ?> unit</td></tr>
                                    <tr><td> </td><td></td><td>2. TPS </td><td><?php echo $pencemaran_pb3->padat_sarana_tps; ?> unit</td></tr>
                                    <tr><td>d.</td><td>Pengelolaan</td><td colspan="2"><?php echo $pencemaran_pb3->padat_kelola; ?></td></tr>
                                    <tr><td>e.</td><td>Lain-lain :</td><td colspan="2"><?php echo $pencemaran_pb3->lain_lain; ?></td></tr>
                                </tbody>
                            </table>
                             <?php } else { ?>
                                 <table class="table table-th-block table-bordered">
                                     <thead>
                                     <tr>III.   PENGELOLAAN LIMBAH PADAT DAN B3</tr>
                                     </thead>
                                     <tbody>
                                     <tr><h3 style="color: red">BELUM ADA DATA</h3></tr>
                                     </tbody>
                                 </table>
                             <?php } ?>

                            <table class="table table-th-block table-bordered">
                                <tbody>
                                    <tr><td>IV LAIN-LAIN</td></tr>
                                    <tr><td><?php echo $bap->catatan; ?></td></tr>
                                </tbody>
                            </table>

                            <?php 
                                # function to convert boolean to string (Ada / Tidak Ada)
                                function adaConv($value) {
                                    if ($value == true) {
                                        return "Ada";
                                    } else {
                                        return "Tidak Ada";
                                    }
                                } 

                                # function to convert boolean to string (Ya / Tidak)
                                function yaConv($value) {
                                    if ($value == true) {
                                        return "Ya";
                                    } else {
                                        return "Tidak";
                                    }
                                }
                                ?>
                        </div>
                        <button type="button" class="btn btn-primary" onclick="window.location='<?php echo base_url('backend/bap_rm'); ?>'">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->
<script type="text/javascript">
    $("#timepick1").timepicker({
            showMeridian: false     
    });

    $("#timepick2").timepicker({
            showMeridian: false     
    });
</script>
<script type="text/javascript">
        $(document).ready(function () {

        var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search_industri_bap/19',
            onSearchStart: function (query) {
                $('#id_industri').val(null);              
            },
            onSelect: function (suggestion) {
                $('#id_industri').val(suggestion.data).change();
                var id_last_input = $('#id_industri').val();
                $('#id_last_input').val(id_last_input);
                console.log($('#id_last_input').val());
            }
        });

        $("select[name=id_industri]").change(function(){
            $('#id_last_input').val($('#id_industri').val());
            $(this).get_detail_industri(this, '#container2');
        });

        $('#confirm').click(function (e) {
            $('#last_input').modal('show');
        });

        $('#id_petugas_bap1').multiselect({
            checkboxName: 'petugas_bap[]'
        });

        $('#id_petugas_bap2').multiselect({
            checkboxName: 'petugas_bap[]'
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        /*warn user when exited from form input*/
        $("#bap-list").click(function () {
            if (!$(this).hasClass('active')) {
                $("#warn").modal("show");
                $("#back-to-form").click(function () {
                    $("#bap-list").removeClass("active");
                    $("#bap-form").addClass("active");
                });
            }
        });

        /* prev next submit handler */
        $(".save-partially").click(function () {
            /*manual validation for save partially*/
            /*show again head-info*/
            $("#save-part-dismiss").hide();
            $("#head-info").show();
            $("#save-part-button").show();
            /*data umum*/
            if($(this).attr('id')=='save-datum') {
                if (!$("input[name=bap_tgl]").val()) { alert("Simpan Sebagian : Tanggal Pengawasan tidak boleh kosong!"); return false };
                if (!$("select[name=id_pegawai]").val()) { alert("Simpan Sebagian : Petugas Pengawas boleh kosong!"); return false };
                if ($("#id_industri").val() == 'Empty') { alert("Simpan Sebagian : Nama Usaha/Kegiatan boleh kosong!"); return false };
                if (!$("input[name=p2_nama]").val()) { alert("Simpan Sebagian : Nama Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=p2_telp]").val()) { alert("Simpan Sebagian : Telepon Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=p2_jabatan]").val()) { alert("Simpan Sebagian : Jabatan Penanggung Jawab tidak boleh kosong!"); return false };
                if (!$("input[name=jml_meja]").val()) { alert("Simpan Sebagian : Jumlah Meja/Kursi tidak boleh kosong!"); return false };

                if($("#dl1").is(":checked")) {
                    if (!$("select[name=jdl_name").val()) { alert("Simpan Sebagian : Pilih Jenis Dokumen Lingkungan!"); return false };
                    if (!$("input[name=dlt").val()) { alert("Simpan Sebagian : Tahun Dokumen Lingkungan tidak boleh kosong!"); return false };
                }

                if($("#il1").is(":checked")) {
                    if (!$("input[name=il_tahun").val()) { alert("Simpan Sebagian : Tahun Izin Lingkungan tidak boleh kosong!"); return false };
                }

                if($("#iu1").is(":checked")) {
                    if (!$("input[name=iu_tahun").val()) { alert("Simpan Sebagian : Tahun Izin Usaha tidak boleh kosong!"); return false };
                }

                var info = $("#save-datum").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#data_umum");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else if($(this).attr('id')=='save-pencemaran_air') { /*pencemaran air*/
                if (!$("select[name=bdn_terima]").val()) { alert("Simpan Sebagian : Badan Air Penerima tidak boleh kosong!"); return false };

                if($("#sarolim1").is(":checked")) {
                    if (!$("input[name=sarana_jenis]").val()) { alert("Simpan Sebagian : Jenis Sarana tidak boleh kosong!"); return false };
                    if (!$("input[name=kapasitas]").val()) { alert("Simpan Sebagian : Kapasitas Sarana tidak boleh kosong!"); return false };
                }

                var info = $("#save-pencemaran_air").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_air");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else  if ($(this).attr('id')=='save-pencemaran_udara') {
                if($("#bmp1").val()=="0") {
                    if (!$("#bm_param1").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Upwind) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period1").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Upwind) tidak boleh kosong!"); return false };
                }

                if($("#bmp2").val()=="0") {
                    if (!$("#bm_param2").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Site) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period2").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Site) tidak boleh kosong!"); return false };
                }

                if($("#bmp3").val()=="0") {
                    if (!$("#bm_param3").val()) { alert("Simpan Sebagian : Parameter Tidak Memenuhi BM (Downwind) tidak  boleh kosong!"); return false };
                    if (!$("#bm_period3").val()) { alert("Simpan Sebagian : Periode Tidak Memenuhi BM (Downwind) tidak boleh kosong!"); return false };
                }

                var info = $("#save-pencemaran_udara").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_udara");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            } else if($(this).attr('id')=='save-pencemaran_b3') {
                if (!$("input[name=padat_sarana_tong]").val()) { alert("Simpan Sebagian : Tong/Bak Sampah tidak boleh kosong!"); return false };
                if (!$("input[name=padat_sarana_tps]").val()) { alert("Simpan Sebagian : TPS tidak boleh kosong!"); return false };

                var info = $("#save-pencemaran_b3").val().split(" ");
                $("#save-part-info").text(info[1]+" "+info[2]);

                var $elem = $("#pencemaran_b3");
                var serializedData = $elem.find("input, select, button, textarea").serialize();
                var dest = $(this).attr('id').split("-")[1];
            }

            $("#save-part").modal("show");

            $("button#button-save-part").unbind().click(function () {
                /*collect ids*/
                var ids = {
                    'id_bap': $("#id_bap").val(),
                    'id_bap_rm': $("#id_bap_rm").val(),
                    'id_pencemaran_air': $("#id_pencemaran_air").val(),
                    'id_pencemaran_udara': $("#id_pencemaran_udara").val(),
                    'id_plb3': $("#id_plb3").val()
                };
                $('.spinner-loader').show();

                $.post('<?php echo base_url("backend/bap_rm/save_part")?>', {
                    data : serializedData,
                    dest : dest,
                    ids : ids
                }, function (ret) {
                    if(ret.status) {
                        console.log(ret);
                        /*append ids to hidden field*/
                        $("#id_bap").val(ret.id.id_bap);
                        $("#id_bap_rm").val(ret.id.id_bap_rm);
                        $("#id_pencemaran_air").val(ret.id.id_pencemaran_air);
                        $("#id_pencemaran_udara").val(ret.id.id_pencemaran_udara);
                        $("#id_plb3").val(ret.id.id_plb3);

                        //$("#save-part").modal("hide");
                        $("#head-info").hide();
                        $("#save-part-button").hide();
                        $("#save-part-dismiss").show();
                        $("#save-part-info").text("Data "+info[1]+" "+info[2]+" berhasil disimpan");
                        var nextId = $elem.next().attr("id");
                        $('[href=#'+nextId+']').tab('show');
                    } else {
                        /*force to check if datum is not yet saved*/
                        $("#save-part").modal("hide");
                        $("#save-datum-first").modal("show");
                    }
                }, "json");

                $('.spinner-loader').hide();
            });
        });
        /* prev next submit handler */

        $('.compare-row').click(function (e) {
            $('input[id=compare_id]').val($(this).data('id'));

            $.post('<?php echo base_url("backend/bap_rm/getComparableStatus")?>', {
                id_bap : $(this).data('id')
            }, function (ret) {
                var info = "";
                $("#compare-modal-body").html("");
                if(!ret.status) {
                    //console.log(ret);

                    info += '<p>Data Belum lengkap! Silahkan edit data terlebih dahulu</p>';
                    var info_bap = ret.bap.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_bap+"&nbsp; Data Umum BAP</span><br>";
                    var info_bap_rm = ret.bap_rm.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_bap_rm+"&nbsp; Data BAP RM</span><br>";
                    var info_pencemaran_air = ret.pencemaran_air.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_pencemaran_air+"&nbsp; Pencemaran Air</span><br>";
                    var info_pencemaran_udara  = ret.pencemaran_udara.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_pencemaran_udara+"&nbsp; Pencemaran Udara</span><br>";
                    var info_pencemaran_pb3 = ret.pencemaran_pb3.length===0 ? "<span class='label label-danger'><i class='glyphicon glyphicon-remove-circle'></i>" : "<span class='label label-success'><i class='glyphicon glyphicon-check'></i>";
                    info += info_pencemaran_pb3+"&nbsp; Pencemaran Limbah B3</span><br>";

                    $("#compare-modal-body").html(info);
                    $("#button-compare").hide();
                } else {
                    info += '<p style="color: green">Data siap diperiksa, click tombol compare</p>';
                    $("#compare-modal-body").html(info);
                    $("#button-compare").show();
                }
            }, "json");

            $('#compare').modal('show');
        });
        
        $('.edit-row').click(function (e) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '<?php echo base_url('/backend/user/get'); ?>',
                data: 'id=' + $(this).data('id'),
                success: function (response) {
                    console.log(response);
                    $('input[id=edit_id]').val(response.id);
                    $('input[name=personname]').val(response.name);
                    $('input[name=username]').val(response.username);
                    $('input[name=email]').val(response.email);
                    $('select[name=user_level]').val(response.group_id);

                    $('.password').hide();
                    $('input[name=password]').prop('disabled', true);
                    $('.confirm_password').hide();
                    $('input[name=confirm_password]').prop('disabled', true);

                    $('#myModal form').attr('action', '<?php echo base_url('/backend/user/edit') ?>'); //this fails silently
                    $('#myModal form').get(0).setAttribute('action', '<?php echo base_url('/backend/user/edit') ?>'); //this works

                    $('#myModal').modal('show');
                }
            })
        });
        $('.reset-row').click(function (e) {

            $('input[id=edit_id]').val($(this).data('id'));

            $('.personname').hide();
            $('input[name=personname]').prop('disabled', true);
            $('.username').hide();
            $('input[name=username]').prop('disabled', true);
            $('.email').hide();
            $('input[name=email]').prop('disabled', true);
            $('.user_level').hide();
            $('select[name=user_level]').prop('disabled', true);
            $('.confirm_password').hide();
            $('input[name=confirm_password]').prop('disabled', true);

            $('#myModal form').attr('action', '<?php echo base_url('/backend/user/force_reset') ?>'); //this fails silently
            $('#myModal form').get(0).setAttribute('action', '<?php echo base_url('/backend/user/force_reset') ?>'); //this works

            $('#myModal').modal('show');
        });

        $('.delete-row').click(function (e) {
            $('input[id=deleted_id]').val($(this).data('id'));
            $('#delete').modal('show');
        });

        $.fn.edit = function(id_bap,industri,tgl) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/bap_rm/get_pelanggaran'); ?>",
                data: 'id_bap='+id_bap+'&industri='+industri+'&tgl='+tgl,
                success: function(d) {
                    console.log(d);
                    var header = '<tr><td>Nama Usaha / Kegiatan</td><td>'+d.industri+'</td></tr>';
                        header += '<tr><td>Tanggal Pengawasan</td><td>'+d.tgl+'</td></tr></tr>';
                    var h = '';

                    if (d.teguran != null) {
                        data = {}, no = 0;
                        for (var i = 0; i < d.teguran.length; i++) {
                            data = d.teguran[i];

                            if ((data.nilai_parameter == 'Dibakar') || data.nilai_parameter == 'dibakar'){
                                data.nilai_parameter_bap = 'Selain dibakar';
                            }

                            h += '<tr>';
                            h += '<td class="text-center">'+(no+=1)+'</td>';
                            h += '<td>'+data.ket_parameter_bap+'</td>';
                            h += '<td>'+data.nilai_parameter+'</td>';
                            h += '<td>'+data.nilai_parameter_bap+'</td>';
                            h += '<td>'+((data.perbaikan==1) ? "<font color='green'>Sudah diperbaiki</font>" : "<font color='red'>Belum diperbaiki</font>")+'</td>';
                            h += '</tr>';             
                        }

                        $('#bap-pelanggaran').html(h);
                    } else {
                        h += '<tr><td colspan="5" class="text-center">Tidak Ditemukan Pelanggaran</td><tr>';
                        $('#bap-pelanggaran').html(h);
                    }

                    $('#bap-header').html(header);
                    $('#DetailModal').modal('show');
                }            
            })
        }
        
        $('#dl1').click(function (e) {
            // alert('#dl1');
            $('#jdl').show();
        });
        $('#dl2').click(function (e) {
            // alert('#dl2');
            $('#jdl').hide();
            $('#jdl_name').val(null);
            $('#dlt').val(null);
        });

        $('#sarolim1').click(function (e) {
            // alert(e);
            $('#sarolim').show();
        });
        $('#sarolim2').click(function (e) {
            // alert('#sarolim2');
            $('#sarolim').hide();
            $('#sarana_jenis').val(null);
            $('#kapasitas').val(null);
            $('#derajat_s').val(null).prop('readonly', false);
            $('#jam_s').val(null).prop('readonly', false);
            $('#menit_s').val(null).prop('readonly', false);
            $('#derajat_e').val(null).prop('readonly', false);
            $('#jam_e').val(null).prop('readonly', false);
            $('#menit_e').val(null).prop('readonly', false);
        });

        $('#il1').click(function (e) {
            $('#il_tahun').show();
        });
        $('#il2').click(function (e) {
            $('#il_tahun').hide();
            $('#ilt').val(null);
        });

        $('#iu1').click(function (e) {
            $('#iu_tahun').show();
        });
        $('#iu2').click(function (e) {
            $('#iu_tahun').hide();
            $('#iut').val(null);
        });
        
        $('#jenis-au').change(function() {
            if($(this).val()  == 'other') {
                $('#jenis-au2').show();
                $('#jenis-au2').focus();
            } else {
                $('#jenis-au2').hide();
            }
        });

        $('.hp').change(function() {
            if ($(this).val() == 'tidak'){
                $('#tmbm').show();
                $('#tpb').focus();
            } else {
                $('#tmbm').hide();
            }
        });

        $('#sh1').click(function (e) {
            $('#dsh').show();
        });
        $('#sh2').click(function (e) {
            $('#dsh').hide();
        });

        $('#hpue1').click(function (e) {
            $('#tmbmue').hide();
        });
        $('#hpue2').click(function (e) {
            $('#tmbmue').show();
        });

        $('#hpua1').click(function (e) {
            $('#tmbmua').hide();
        });
        $('#hpua2').click(function (e) {
            $('#tmbmua').show();
        });

        $('#jape').change(function() {
            if($(this).val()  == 'other') {
                $('#jape2').show();
                $('#jape2').focus();
            } else {
                $('#jape2').hide();
            }
        });

        $('#jlbyd').change(function() {
            if($(this).val()  == 'other') {
                $('#jlbyd2').show();
                $('#jlbyd2').focus();
            } else {
                $('#jlbyd2').hide();
            }
        });

        $('#pjpl').change(function() {
            if($(this).val()  == 'Lainnya') {
                $('#pjpl2').show();
                $('#pjpl2').focus();
            } else {
                $('#pjpl2').hide();
                $('#pjpl2').val(null);
            }
        });

        $('.bmp').change(function(event) {
            var num = $(this).prop('name').match(/\d+$/)[0];
            if($(this).val()  == 'Ya') {
                $('#bm_param'+num).val('-');
                $('#bm_period'+num).val('-');
            } else {
                $('#bm_param'+num).val('');
                $('#bm_period'+num).val('');
            }
        });

         window.setTimeout(function () { $(".alert").alert('close'); }, <?php echo $this->config->item('timeout_message'); ?>);

    });
</script>


<!-- Modal -->
<div class="modal fade" id="last_input" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rm/tipe_edit/'. 3); ?>" method="post">
                <div class="modal-body">
                <p>Silahkan kofirmasikan aksi anda dengan memilih salah satu opsi dibawah</p>
                    <p>Jika Anda memilih Ambil data terakhir, data BAP Rumah Makan terakhir dengan Nama Usaha/Kegiatan yang sudah dipilih akan ditampilkan</p>
                    <input type="hidden" name="id_last_input" id="id_last_input" />
                </div>
                <div class="modal-footer">
                <div class="col-xs-2">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Input Baru</button>
                    </div>
                    <div class="col-xs-10">
                    <button type="submit" class="btn btn-primary" >Ambil data terakhir</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Bap</h4>
            </div>
            <form action="<?php echo base_url('/backend/bap_rm/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apa anda yakin ingin menghapus data BAP ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="DetailModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Pelanggaran BAP </h4>
            </div>
            <div class="modal-body">
                <table class="table table-th-block">
                     <tbody id="bap-header"></tbody>
                </table>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%" class="text-area">No.</th>
                                <th>Nama Item</th>
                                <th>Nilai Item</th>
                                <th>Nilai Item Ketaatan</th>                             
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody id="bap-pelanggaran"></tbody>
                    </table>
                    <p id="bap-ket"></p>
                </div><!-- /.table-responsive -->
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

 <div class="modal fade" id="warn" role="dialog">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header bg-primary no-border">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
             </div>
             <form action="<?php echo base_url('/backend/bap_rm/'); ?>" method="post">
                 <div class="modal-body">
                     <p>Anda akan keluar dari form pengisian data! </p>
                     <p>Apakah anda yakin? </p>
                 </div>
                 <div class="modal-footer">
                     <button type="button" id="back-to-form" class="btn btn-default" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;&nbsp;Batal</button>
                     <button type="submit" class="btn btn-danger"><i class="fa fa-remove"></i>&nbsp;&nbsp;Keluar</button>
                 </div>
             </form>
         </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
 </div>

 <div class="modal fade" id="save-datum-first" role="dialog">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header bg-primary no-border">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Peringatan!</h4>
             </div>
             <div class="modal-body">
                 <p>Silahkan simpan Data Umum BAP terlebih dahulu! </p>
             </div>
             <div class="modal-footer">
                 <button type="button" id="button-save-datum" class="btn btn-default" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;&nbsp;Ok</button>
             </div>
         </div>
     </div>
 </div>

 <div class="modal fade" id="save-part" role="dialog">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header bg-primary no-border">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Simpan data BAP Sebagian</h4>
             </div>
             <div class="modal-body">
                 <p id="head-info">Data berikut akan disimpan: </p>
                 <p id="save-part-info"></p>
                 <input type="hidden" name="id" id="id_bap" value="" />
                 <input type="hidden" name="id" id="id_bap_rm" value="" />
                 <input type="hidden" name="id" id="id_pencemaran_air" value="" />
                 <input type="hidden" name="id" id="id_pencemaran_udara" value="" />
                 <input type="hidden" name="id" id="id_plb3" value="" />
                 <center><img class="spinner-loader" style="display: none;" src="<?php echo base_url('assets/img/spinner.gif') ?>" class="img-responsive" alt="memuat"></center>
             </div>
             <div class="modal-footer">
                 <div id="save-part-button">
                     <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                     <button type="submit" id="button-save-part" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;Simpan</button>
                 </div>
                 <div id="save-part-dismiss" style="display: none">
                     <button type="button" class="btn btn-success" data-dismiss="modal"><i class="glyphicon glyphicon-check"></i>&nbsp;&nbsp;Tutup</button>
                 </div>
             </div>
         </div>
     </div>
 </div>

 <div class="modal fade" id="compare" role="dialog">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header bg-primary no-border">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="myModalLabel">Status Kelengkapan Data BAP Rumah Makan</h4>
             </div>
             <form action="<?php echo base_url('/backend/bap_rm/newCompare'); ?>" method="post">
                 <div class="modal-body">
                     <div id="compare-modal-body"></div>
                     <input type="hidden" name="id" id="compare_id" value="" />
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                     <button type="submit" id="button-compare" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;Compare</button>
                 </div>
             </form>
         </div>
     </div>
 </div>