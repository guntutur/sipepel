<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_debit_air_limbah extends CI_Model {

	private $table_name = 'debit_air_limbah';
	private $table_detail = 'detail_debit_air_limbah';
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('d.*, i.nama_industri');
		$this->db->from($this->table_name .' as d');
		$this->db->join('industri i', 'd.id_industri = i.id_industri', 'inner');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);		
		$this->db->order_by("d.laporan_bulan", "asc"); 

		$query = $this->db->get();	
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {

		$this->db->select('d.*, i.nama_industri');
		$this->db->from($this->table_name .' as d');
		$this->db->join('industri i', 'd.id_industri = i.id_industri', 'inner');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	function get_detail_all($where='', $limit=0, $offset=0) {

		$data = array();

		$this->db->select('*');
		$this->db->from($this->table_detail);		

		if($where != '') {
			$this->db->where($where);
		}

		$this->db->limit($limit, $offset);		
		$this->db->order_by("tgl_debit", "asc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_detail_num_rows($where) {

		$this->db->select('*');
		$this->db->from($this->table_detail);		

		if($where != '') {
			$this->db->where($where);
		}

		return $this->db->count_all_results();
	}
	
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_debit_air_limbah_by_id($id) {		
		$this->db->where('id_debit_air_limbah', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_debit_air_limbah($where) {		
		$this->db->where($where);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_detail_debit_air_limbah_by_id($id) {
		$this->db->where('id_debit', $id);
		
		$query = $this->db->get($this->table_detail);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_debit_air_limbah($id) {
		$data = $this->get_debit_air_limbah_by_id($id);
		$this->db->where('id_debit_air_limbah', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}

	function delete_detail_debit_air_limbah($id) {
		$data = $this->get_detail_debit_air_limbah_by_id($id);
		$this->db->where('id_debit', $id);
		$this->db->delete($this->table_detail);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_debit_air_limbah($data, $id) {

		$this->db->where("id_debit_air_limbah", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}

	function change_detail_debit_air_limbah($data, $id) {

		$this->db->where("id_debit", $id);
		$this->db->update($this->table_detail, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_debit_air_limbah($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function insert_detail($data){
		$this->db->insert($this->table_detail, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate_detail($data) {
		$this->db->where('id_debit_air_limbah', $data['id_debit_air_limbah']);
		$this->db->where('tgl_debit', $data['tgl_debit']);

		$query = $this->db->get($this->table_detail);
		if ($query->num_rows() == 1) return $query->row();
		return array();
	}

	function is_duplicate($id_industri, $laporan_bulan) {
		$this->db->where('id_industri = '.$id_industri." and laporan_bulan = '".$laporan_bulan."'");
		$query = $this->db->get('debit_air_limbah');
		return ($query->num_rows() > 0) ? FALSE : TRUE;
	}
}
?>