<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">PEGAWAI&nbsp;&nbsp;<small>mengelola seluruh data pegawai</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Pegawai</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">
			
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
								<th width="15%">NIP</th>
                                <th>Nama Lengkap</th>                                
                                <th>Gol./Jabatan</th>
                                <th>Kontak</th>
                                <th class="text-right"></th>									
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="6">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start"></span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/pegawai/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                       
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
                        t += '<td>'+dt.nip+'</td>';
                        t += '<td>'+dt.nama_pegawai+'</td>';                        
                        t += '<td>'+dt.pangkat_gol+'/'+dt.jabatan+'</td>';                        
                        t += '<td>';
                        t += '<i class="fa fa-phone-square icon-sidebar"></i>'+dt.tlp+'<br/>';                                      
                        t += '<i class="fa fa-envelope icon-sidebar"></i>'+dt.email;
                        t += '</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_pegawai+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_pegawai+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }  

	$(document).ready(function () {       
		
        $("#form_add").validate({
            rules: {
                personname: { required: true, minlength: 3, maxlength: 100 },
                nip:{ required: true, maxlength: 20 },
                classs: { required: true, maxlength: 20 },
                position: { required: true, maxlength: 90 },
                phone: { required: true, maxlength: 20 },
                address: { required: true },
                username: { required: true, minlength: 3, maxlength: 100 },
                password: { required: true, minlength: 6, maxlength: 100 },
                confirm_password: { required: true, equalTo: "#password", minlength: 6, maxlength: 100 },
                email: { required: true, email: true, minlength: 5 },
                user_level: { required: true }
            },
            messages: {        
                personname: {
                    required: "Masukkan nama lengkap pegawai",
                    minlength: "Nama lengkap pegawai harus lebih dari 3 karakter.",
                    maxlength: "Nama lengkap tidak boleh lebih dari 100 karakter."
                },
                nip: {
                    required: "Masukkan nomor induk pegawai",
                    minlength: "NIP harus lebih dari 3 karakter.",
                    maxlength: "NIP tidak boleh lebih dari 20 karakter."
                },
                classs: {
                    required: "Masukkan golongan pegawai",
                    minlength: "Golongan pegawai harus lebih dari 3 karakter.",
                    maxlength: "Golongan pegawai tidak boleh lebih dari 20 karakter."
                },
                position: {
                    required: "Masukkan jabatan pegawai",
                    minlength: "Jabatan pegawai harus lebih dari 3 karakter.",
                    maxlength: "Jabatan pegawai tidak boleh lebih dari 90 karakter."
                },
                phone: {
                    required: "Masukkan nomor telepon",
                    minlength: "Nomor telepon harus lebih dari 3 karakter.",
                    maxlength: "Nomor telepon tidak boleh lebih dari 20 karakter."
                },
                address: {
                    required: "Masukkan alamat lengkap",
                    minlength: "Alamat lengkap harus lebih dari 3 karakter."                    
                },
                username: {
                    required: "Masukkan username pegawai",
                    minlength: "Username harus lebih dari 3 karakter",
                    maxlength: "Username tidak boleh lebih dari 100 karakter"
                },
                password: {
                    required: "Masukkan sandi pegawai",
                    minlength: "Sandi harus lebih dari 3 karakter",
                    maxlength: "Sandi tidak boleh lebih dari 100 karakter"
                },
                confirm_password: {
                    required: "Masukkan konfirmasi sandi",
                    minlength: "Konfirmasi sandi harus lebih dari 3 karakter",
                    maxlength: "Konfirmasi sandi tidak boleh lebih dari 100 karakter"
                },
                email: {
                    required: "Masukkan alamat email",
                    minlength: "Alamat email harus lebih dari 5 karakter"
                },
                user_level: {
                    required: "Silahkan pilih data"
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });       

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) { 
            // validator.resetForm();              
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/pegawai/get'); ?>",
                data: 'id_pegawai=' + id,
                success: function (response) {
                    console.log(response);
                    $('input[id=edit_id]').val(response.id_pegawai);                    
                    $('input[name=personname]').val(response.nama_pegawai);
                    $('input[name=nip]').val(response.nip);
                    $('input[name=agency]').val(response.instansi);
                    $('input[name=classs]').val(response.pangkat_gol);
                    $('input[name=position]').val(response.jabatan);
                    $('input[name=phone]').val(response.tlp);
                    $('textarea[name=address]').val(response.alamat);

                    $('input[name=email]').val(response.email);
                    $('input[name=username]').val(response.username);
                    $('select[name=user_level]').val(response.group_id);

                    $('#myModal').modal('show');
                }
            })
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        

        Document.search();

	});
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Pegawai</h4>
            </div>
            <form role="form" method="post" data-action="<?php echo base_url('backend/pegawai') ?>" id="form_add" enctype="multipart/form-data">
                <div class="modal-body">
                    
                    <div class="form-group nip">  
                        <label>Nomor Induk Pegawai</label>
                        <input type="text" name="nip" id="nip" class="form-control has-feedback input-sm numberonly" autofocus placeholder="Nomor Induk Pegawai" />
                    </div> 
                    <div class="form-group personname">
                        <label>Nama Pegawai</label>
                        <input type="text" name="personname" id="personname" class="form-control input-sm has-feedback" autofocus placeholder="Nama Pegawai"/>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 form-group classs">
                            <label>Golongan</label>
                            <input type="text" name="classs" id="classs" class="form-control input-sm has-feedback" autofocus placeholder="Golongan" />
                        </div>
                        <div class="col-lg-6 form-group position">
                            <label>Jabatan</label>
                            <input type="text" name="position" id="position" class="form-control input-sm has-feedback" autofocus placeholder="Jabatan"/>
                        </div>
                    </div>
                    <div class="form-group phone">
                        <label>Telepon</label>
                        <input type="text" name="phone" id="phone" class="form-control has-feedback input-sm numberonly" autofocus placeholder="Telepon" />
                    </div>                          
                    <div class="form-group phone">
                        <label>Email</label>
                        <input type="email" name="email" id="email" class="form-control has-feedback input-sm" autofocus placeholder="Email" />
                    </div>   
                    <div class="form-group address">
                        <label>Alamat</label>
                        <textarea name="address" id="address" class="form-control input-sm has-feedback" autofocus rows="4" placeholder="Alamat"></textarea>
                    </div>                              
                                                                               
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Pegawai</h4>
            </div>
            <form action="<?php echo base_url('/backend/pegawai/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda yakin ingin menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->