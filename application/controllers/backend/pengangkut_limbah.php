<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pengangkut_limbah extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_pengangkut_limbah', '', TRUE);
		$this->load->model('m_kecamatan', '', TRUE);
		$this->load->model('m_kelurahan', '', TRUE);
		$this->load->model('m_industry', '', TRUE);
		$this->load->model('m_data_master', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types
        // $industries = $this->m_pengangkut_limbah->get_all();
		$kecamatan = $this->m_kecamatan->get_dropdown();		
		$kelurahan = $this->m_kelurahan->get_dropdown();		
		$badan_usaha = $this->m_data_master->get_detail_data_master(array('jenis_data_master' => 'badan_usaha', 'status' => 1));	
		
		# response paramter
        // $data['industry'] = $industries;  
		$data['kecamatan'] = $kecamatan;
		$data['kelurahan'] = $kelurahan;
		$data['industri'] = $this->m_industry->get_dropdown(); 		
		$data['badan_usaha'] = $badan_usaha;
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_pengangkut_limbah', $data);
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = array(
				'i.id_industri' => array('SEARCH', 'OR', $query),
				'i.nama_industri' => array('SEARCH', 'OR', $query),
				'i.badan_hukum' => array('SEARCH', 'OR', $query),
				'i.alamat' => array('SEARCH', 'OR', $query),
				'i.nomer_klh' => array('SEARCH', 'OR', $query),
				'k.ket' => array('SEARCH', 'OR', $query),
				'l.ket' => array('SEARCH', 'OR', $query)
			);

     	$total = $this->m_pengangkut_limbah->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_pengangkut_limbah->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_pengangkut_limbah->get_industry_by_id($this->input->post('id_industri')));
		exit();
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'badan_hukum' => $this->input->post('badan_hukum'),
			'nama_industri' => $this->input->post('nama_industri'),
			'tlp' => $this->input->post('tlp'),
			'no_truck' => $this->input->post('no_truck'),
			'email' => $this->input->post('email'),
			'alamat' => $this->input->post('alamat'),
			'fax' => $this->input->post('fax'),
			'nomer_klh' => $this->input->post('nomer_klh'),
			'id_kecamatan' => $this->input->post('nama_kecamatan'),
			'id_kelurahan' => $this->input->post('nama_kelurahan'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_pengangkut_limbah->change_industry($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/pengangkut_limbah');
	}
	
	public function register() {
		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'badan_hukum' => $this->input->post('badan_hukum'),
				'nama_industri' => $this->input->post('nama_industri'),
				'no_truck' => $this->input->post('no_truck'),
				'tlp' => $this->input->post('tlp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'fax' => $this->input->post('fax'),
				'nomer_klh' => $this->input->post('nomer_klh'),
				'id_kecamatan' => $this->input->post('nama_kecamatan'),
				'id_kelurahan' => $this->input->post('nama_kelurahan'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_pengangkut_limbah->insert_industry($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		redirect('backend/pengangkut_limbah');
	}
	
	public function delete() {
		$this->m_pengangkut_limbah->delete_industry($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/pengangkut_limbah');
	}
	
	public function validate() {
		# check data yang duplicate
		if($this->m_pengangkut_limbah->is_duplicate($this->input->post('badan_hukum'), $this->input->post('nama_industri'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah ada'));
			redirect('backend/pengangkut_limbah');
		}

		return true;
	}
	
	public function search() {
        $keyword = $_POST['query'];
        
        $data = $this->m_pengangkut_limbah->search_industry($keyword);
        echo json_encode($data);
    }
	
}
