<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document   : rekap_izin.php 
 // Created on : January 05, 2015 17:57 
 // Author     : huda.jtk09@gmail.com 
 // Description: Controller for Rekapitulasi Masa Kontrak Izin 

class rekap_izin extends CI_Controller {

    function __construct() {
        parent::__construct();

        # load model
        // $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_rekap_izin', 'rekDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view
	public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

        # load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_rekap_izin');
    }

    public function get_rekap() {
        $year = 5;
        $month_line = ($year * 12) - 3;
        $month_dead = ($year * 12);

        date_default_timezone_set("Asia/Jakarta");
        $date_line = date('Y-m-d', strtotime("-".$month_line." months", strtotime(date("Y-m-d"))));

        # get parameter pagination
        $input = array('dataperpage', 'rekap_type', 'curpage');
        foreach ($input as $val) {
            $$val = $this->input->post($val); 
        }

        if ($rekap_type == 'izin_pembuangan') {
            $total = $this->rekDao->get_izin_pembuangan_size($date_line);
        } else {
            $total = $this->rekDao->get_izin_tps_size($date_line);
        }

        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        if ($rekap_type == 'izin_pembuangan') {
            $rekap = $this->rekDao->get_izin_pembuangan($date_line, $dataperpage, $start);
        } else {
            $rekap = $this->rekDao->get_izin_tps($date_line, $dataperpage, $start);
        }

        foreach ($rekap as $value) {

            $value->tgl_habis = date('Y-m-d', strtotime("+".$month_dead." months", strtotime($value->tgl_izin)));

            if ($value->tgl_habis < date("Y-m-d")) {
                $value->status = 'Expired';
            } else {
                $value->status = ' - ';
            }
        }

        $data = array(
            'rekap' => $rekap,
            'pagination' => $this->functions->create_links($npage, $curpage, 3),
            'numpage' => $npage - 1,
            'total' => $total
        );

        echo json_encode($data);
        exit();
    }
}
