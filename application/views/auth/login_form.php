	<style>
	body{ margin-top: 70px; }
	.container { width: 900px; }
	.panel{ border-radius: 0; }
	.panel-footer{ background-color: #428bca; border: none; border-radius: 0; padding-left: 20px; padding-right: 20px;}
	.panel-default{ border: none;}
	.panel-body{ padding: 0; margin: 0; }
	.page-header{ margin-top: 0;}
	.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child > .btn, .input-group-btn:first-child > .dropdown-toggle, .input-group-btn:last-child > .btn:not(:last-child):not(.dropdown-toggle) { border-radius: 0; }
	.form-control, .btn{ border-radius: 0;}
	h1, .h1{ font-size: 28px;}
	.panel-footer{ background-color: #37bc9b; }
	.panel-body{ width: 100%; height: 276px; overflow: hidden;}
	</style>

	<div class="container">
		<div class="row">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">							
				<div class="panel panel-default">
					<div class="panel-body flexslider">						
						<ul class="slides">
							<li>
								<img src="<?php echo base_url('assets/img/bg.jpg'); ?>" class="img-responsive" alt="Image">
							</li>							
							<li>
								<img src="<?php echo base_url('assets/img/bg-2.jpg'); ?>" class="img-responsive" alt="Image">
							</li>
							<li>
								<img src="<?php echo base_url('assets/img/bg-3.jpg'); ?>" class="img-responsive" alt="Image">
							</li>
						</ul>					
					</div>
					<div class="panel-footer">
						<span style="font-size: 19pt; color: white;">SISTEM INFORMASI LINGKUNGAN</span>
						<br/>
						<p style="color: white; line-height: 13pt; margin-top: 8px;">Menampung, mengelola dan memberikan rujukan/ rekomendasi otomasi untuk mendukung kegiatan BPLH Kabupaten Bandung</p>
					</div>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
				<h1 class="page-header">SIL Sign in</h1>
				<p>Login dengan account Anda sendiri !</p>
				<?php echo form_open($this->uri->uri_string()); ?>									
					<div class="form-group has-feedback lg left-feedback no-label">
						<input type="text" name="login" class="form-control input-lg" placeholder="Username" autofocus>
						<span class="fa fa-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback lg left-feedback no-label">
						<input type="password" name="password" class="form-control input-lg" placeholder="Password">
						<span class="fa fa-unlock-alt form-control-feedback"></span>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label class="login-label">
								<input type="checkbox" name="remember" value="1" class="i-grey-flat" tabindex="3" /><ins class="iCheck-helper"></ins> Ingatkan Saya
							</label>
						</div>
					</div>
					<button type="submit" class="btn btn-primary btn-lg pull-right">Sign in</button>
				<?php echo form_close(); ?>
				<p style="margin-top: 130px;">© 2014 <a href="http://www.bandungkab.go.id/">BPLH Kabupaten Bandung</a></p>
			</div>
		</div>
	</div><!-- /.container -->

	<script type="text/javascript">
		$(document).ready(function(){
		  $('.flexslider').flexslider({
			animation	: "slide",
			controlNav	: false,
			start		: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	</script>