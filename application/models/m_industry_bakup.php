<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_industry extends CI_Model {

	private $table_name			= 'industri';			// industry type
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}
	
	/**
	 * Get all user
	 *
	 * @return object
	 */
	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('i.*, j.id_jenis_industri, j.ket as nama_jenis_industri, u.id_usaha_kegiatan, u.ket as nama_usaha_kegiatan, k.ket as nama_kecamatan, l.ket as nama_kelurahan');
		$this->db->from($this->table_name .' as i');
		$this->db->join('usaha_kegiatan u', 'i.id_usaha_kegiatan = u.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri j', 'i.id_jenis_industri = j.id_jenis_industri', 'left');
		$this->db->join('kecamatan k', 'k.id_kecamatan = i.id_kecamatan', 'left');
		$this->db->join('kelurahan l', 'l.id_kelurahan = i.id_kelurahan', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("u.ket", "asc"); 
		$this->db->order_by("j.ket", "asc"); 
		$this->db->order_by("i.badan_hukum", "asc"); 
		$this->db->order_by("i.nama_industri", "asc"); 
		
	

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	function get_industri(){
		$this->db->select('i.*, j.id_jenis_industri, j.ket as nama_jenis_industri, u.id_usaha_kegiatan, u.ket as nama_usaha_kegiatan, k.ket as nama_kecamatan, l.ket as nama_kelurahan');
		$this->db->from($this->table_name .' as i');
		$this->db->join('usaha_kegiatan u', 'i.id_usaha_kegiatan = u.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri j', 'i.id_jenis_industri = j.id_jenis_industri', 'left');
		$this->db->join('kecamatan k', 'k.id_kecamatan = i.id_kecamatan', 'left');
		$this->db->join('kelurahan l', 'l.id_kelurahan = i.id_kelurahan', 'left');

		$this->db->order_by("u.ket", "asc"); 
		$this->db->order_by("j.ket", "asc"); 
		$this->db->order_by("i.badan_hukum", "asc"); 
		$this->db->order_by("i.nama_industri", "asc"); 
		
	

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_num_rows($where) {
		
		$this->db->select('i.*, j.id_jenis_industri, j.ket as nama_jenis_industri, u.id_usaha_kegiatan, u.ket as nama_usaha_kegiatan, k.ket as nama_kecamatan, l.ket as nama_kelurahan');
		$this->db->from($this->table_name .' as i');
		$this->db->join('usaha_kegiatan u', 'i.id_usaha_kegiatan = u.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri j', 'i.id_jenis_industri = j.id_jenis_industri', 'left');
		$this->db->join('kecamatan k', 'k.id_kecamatan = i.id_kecamatan', 'left');
		$this->db->join('kelurahan l', 'l.id_kelurahan = i.id_kelurahan', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}
		return $this->db->count_all_results();
	}

	function get_dropdown() {
		$data = array();

		$this->db->select('*')->from($this->table_name);
		$this->db->order_by("nama_industri", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	/**
	 * Another attempt to get nama usaha/kegiatan in form input bap, so far we've been using 3 different method, this should be the final
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_dropdown_bap($id) {
		$data = array();

		$this->db->select('*')->from($this->table_name)->where('id_usaha_kegiatan', $id);
		$this->db->order_by("nama_industri", "asc"); 
		$this->db->order_by("tgl_pembuatan", "desc"); 

		$query = $this->db->get();	
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}
	
	/**
	 * Get user record by Id
	 *
	 * @param	int	 
	 * @return	object
	 */
	function get_industry_by_id($id) {
		$this->db->where('id_industri', $id);
		
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_industry($id) {
		$data = $this->get_industry_by_id($id);
		$this->db->where('id_industri', $id);
		$this->db->delete($this->table_name);
		
		if ($this->db->affected_rows() > 0) return TRUE;
		return FALSE;
	}
	
	/**
	 * Create edit user record
	 *
	 * @param	array
	 * @param 	int 
	 * @return	array
	 */
	function change_industry($data, $id) {

		$this->db->where("id_industri", $id);
		$this->db->update($this->table_name, $data);		
		
		return $this->db->affected_rows() > 0;
	}
	
	function insert_industry($data){
		$this->db->insert($this->table_name, $data);
		
		return $this->db->affected_rows() > 0;
	}

	function is_duplicate($badan_hukum, $nama_industri) {
		$this->db->where('badan_hukum', $badan_hukum);
		$this->db->where('nama_industri', $nama_industri);
		$this->db->from($this->table_name);
		$res = $this->db->count_all_results();

		if($res > 0) return true;
		else return false;
	}
	
	/**
	 * huda : add -> get all industri to be used as input in bap form (16 Oct 2014, 14:31)
	 * guntur : edit -> where based on id usaha kegiatan
	 * Get all industri
	 *
	 * @return array
	 */
	function get_all_industri($id_usaha_kegiatan) {
		$data = array();

		$query = $this->db->query("SELECT * FROM industri WHERE id_usaha_kegiatan = '$id_usaha_kegiatan' ORDER BY nama_industri ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	
	/**
	 * Guntur : add -> get industri information for ajax view (12 Nov 2014, 15:12)
	 *
	 * Get industri information
	 *
	 * @return array
	 */
	function get_ajax_industri() {

		if(isset($_POST['id_industri'])) {

			$id_industri = $this->input->post('id_industri');

			if ($id_industri != ''){

				$query = $this->db->query("SELECT 
										industri.nama_industri, 
										industri.tlp as tlp, 
										industri.fax as fax, 
										industri.alamat as alamat, 
										kecamatan.ket as kecamatan, 
										kelurahan.ket as kelurahan
										FROM industri 
										INNER JOIN kecamatan on kecamatan.id_kecamatan = industri.id_kecamatan
										INNER JOIN kelurahan on kelurahan.id_kelurahan = industri.id_kelurahan
										WHERE industri.id_industri = '$id_industri'");
			
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$data[] = $row;
					}
				}
				return $data;
			} else {
				$data = NULL;
				return $data;
			}
		}
	}

	/**
	 * Guntur : add -> get detail tps for synchronize data within ajax request information for ajax view (21 Dec 2014)
	 *
	 * Get industri information
	 *
	 * @return array
	 */
	function get_detail_tps() {

		if(isset($_POST['id_industri'])) {

			$id_industri = $this->input->post('id_industri');

			if ($id_industri != ''){

				$query = $this->db->query("SELECT 
					industri.nama_industri, izin_tps_limbah.no_izin AS izin_tps, 
					izin_tps_limbah.tgl_izin AS tgl_perizinan, 
					tps_limbah.jenis_limbah_b3 AS jenis_limbah, 
					tps_limbah.derajat_s AS derajat_s, tps_limbah.jam_s AS jam_s, tps_limbah.menit_s AS menit_s, 
					tps_limbah.derajat_e AS derajat_e, tps_limbah.jam_e AS jam_e, tps_limbah.menit_e AS menit_e,
					tps_limbah.luas_tps as ukuran
					FROM industri
					INNER JOIN izin_tps_limbah ON izin_tps_limbah.id_industri = industri.id_industri
					INNER JOIN tps_limbah ON tps_limbah.id_izin_tps = izin_tps_limbah.id_izin_tps
					WHERE industri.id_industri = '$id_industri' AND izin_tps_limbah.status=1");
			
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$data[] = $row;
					}
				} else {
					$data = NULL;	
				}

				return $data;

			} else {
				$data = NULL;
				return $data;
			}
		}
	}

	/**
	 * Guntur : add -> get detail air limbah for synchronize data within ajax request information for ajax view (21 Dec 2014)
	 *
	 * Get industri information
	 *
	 * @return array
	 */
	function get_detail_air_limbah() {

		if(isset($_POST['id_industri'])) {

			$id_industri = $this->input->post('id_industri');

			if ($id_industri != ''){

				$query = $this->db->query("SELECT 
					industri.nama_industri, 
					izin_pembuangan_limbah.no_izin AS izin_limbah, 
					izin_pembuangan_limbah.tgl_izin AS tgl_perizinan, 
					izin_pembuangan_limbah.proses AS sumber_limbah, 
					izin_pembuangan_limbah.badan_air_penerima AS penerima_limbah,
					izin_pembuangan_limbah.debit_perbulan AS debit_perbulan, 
					izin_pembuangan_limbah.derajat_s AS derajat_s, izin_pembuangan_limbah.jam_s AS jam_s, izin_pembuangan_limbah.menit_s AS menit_s, 
					izin_pembuangan_limbah.derajat_e AS derajat_e, izin_pembuangan_limbah.jam_e AS jam_e, izin_pembuangan_limbah.menit_e AS menit_e
					FROM industri
					INNER JOIN izin_pembuangan_limbah ON izin_pembuangan_limbah.id_industri = industri.id_industri
					WHERE industri.id_industri = '$id_industri' AND izin_pembuangan_limbah.status=1");
			
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$data[] = $row;
					}
				} else {
					$data = NULL;	
				}

				return $data;

			} else {
				$data = NULL;
				return $data;
			}
		}
	}

	/**
	 * huda : add -> get all industri to be used in laporan lhu (21 Oct 2014, 17:53)
	 *
	 * Get all industri
	 *
	 * @return array
	 */
	function get_all_industry_detail() {
		$data = array();

		$query = $this->db->query("SELECT a.id_industri, a.nama_industri, a.alamat, b.ket AS jenis_usaha_kegiatan, d.ket AS nama_kecamatan, e.ket AS nama_kelurahan
									FROM industri a
									LEFT JOIN usaha_kegiatan b ON a.id_usaha_kegiatan = b.id_usaha_kegiatan
									LEFT JOIN jenis_industri c ON a.id_jenis_industri = c.id_jenis_industri
									LEFT JOIN kecamatan d ON a.id_kecamatan = d.id_kecamatan
									LEFT JOIN kelurahan e ON a.id_kelurahan = e.id_kelurahan
									-- LEFT JOIN laporan_hasil_uji f ON a.id_industri = f.id_industri
									ORDER BY a.nama_industri ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function get_all_industry_detail_2($where=array(), $limit=0, $offset=0) {

		$data = array();

		$this->db->select('*');
		$this->db->from('industri a');
		$this->db->join('usaha_kegiatan b', 'a.id_usaha_kegiatan = b.id_usaha_kegiatan', 'left');
		$this->db->join('jenis_industri c', 'a.id_jenis_industri = c.id_jenis_industri', 'left');
		$this->db->join('kecamatan d', 'a.id_kecamatan = d.id_kecamatan', 'left');
		$this->db->join('kelurahan e', 'a.id_kelurahan = e.id_kelurahan', 'left');
		$this->db->join('laporan_hasil_uji f', 'a.id_industri = f.id_industri', 'left');

		foreach($where as $key => $val) {
			if($val[0] == 'SEARCH') {
				if($val[1] == "OR") $this->db->or_like($key, $this->db->escape_like_str($val[2]));
				else $this->db->like($key, $val[2]);
			}else{
				if($val[1] == "OR") $this->db->or_where($key, $this->db->escape_like_str($val[2]));
				else $this->db->where($key, $val[2]);
			}
		}

		$this->db->limit($limit, $offset);
		$this->db->order_by("a.nama_industri", "asc"); 
		$this->db->order_by("c.ket", "asc"); 
		$this->db->order_by("a.tgl_pembuatan", "desc"); 

		$query = $this->db->get();
		
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function search_industry($keyword, $limit=10) {
        $arr = array();
        $this->db->select('id_industri, nama_industri');
        $this->db->from($this->table_name);
        $this->db->like('nama_industri', $keyword);
        $this->db->limit($limit, 0);
        $query = $this->db->get();
        
        $data = $query->result();
        if(count($data) > 0) {
            foreach ($data as $row) {
                $arr['query'] = $keyword;
                $arr['suggestions'][] = array(
                    'value' => $row->nama_industri,
                    'data' => $row->id_industri
                );
            }
        }else {
            $arr['query'] = $keyword;
            $arr['suggestions'] = array();
        }
        
        return $arr;
    }

    function search_industry_bap($keyword, $id_usaha_kegiatan, $limit=10) {
        $arr = array();
        $this->db->select('id_industri, nama_industri');
        $this->db->from($this->table_name);
        $this->db->like('nama_industri', $keyword);
        $this->db->where('id_usaha_kegiatan', $id_usaha_kegiatan);
        $this->db->limit($limit, 0);
        $query = $this->db->get();
        
        $data = $query->result();
        if(count($data) > 0) {
            foreach ($data as $row) {
                $arr['query'] = $keyword;
                $arr['suggestions'][] = array(
                    'value' => $row->nama_industri,
                    'data' => $row->id_industri
                );
            }
        }else {
            $arr['query'] = $keyword;
            $arr['suggestions'] = array();
        }
        
        return $arr;
    }

    function get_last_input_id_bap($id_industri, $jenis_bap){
    	
    	$this->db->select('a.id_bap');
    	$this->db->from('bap a');
    	$this->db->join($jenis_bap.' b', 'a.id_bap = b.id_bap', 'inner');
    	$this->db->where('a.id_industri', $id_industri);
    	$this->db->order_by('b.id_bap', 'desc');
    	$this->db->limit(1);

    	$query = $this->db->get();

    	return $query->row()->id_bap;
    	// return $this->db->get()->result()->row();
    }

    // author : guntutur@gmail.com
    // Menu Raport Industri
    function count_raport_bap($tahun) {
    	if($tahun != 'all') {
	    	$query = $this->db->query("SELECT YEAR(a.bap_tgl) as tahun, a.id_industri, b.nama_industri, a.id_bap AS bap_terakhir
									FROM bap a
									INNER JOIN industri b ON a.id_industri = b.id_industri
									WHERE YEAR(a.bap_tgl) = '$tahun'
									GROUP BY a.id_industri DESC
									ORDER BY a.id_bap DESC");
	    } else {
	    	$query = $this->db->query("SELECT YEAR(a.bap_tgl) as tahun, a.id_industri, b.nama_industri, a.id_bap AS bap_terakhir
									FROM bap a
									INNER JOIN industri b ON a.id_industri = b.id_industri
									GROUP BY a.id_industri DESC
									ORDER BY a.id_bap DESC");
	    }

	    return ($query->num_rows() > 0) ? $query->num_rows() : 0;
    }

    function get_raport_bap($tahun, $limit=0, $offset=0) {
		$data = array();

		if ($tahun != 'all') {
			$query = "SELECT YEAR(a.bap_tgl) as tahun, a.id_industri, b.nama_industri, a.id_bap AS bap_terakhir
									FROM bap a
									INNER JOIN industri b ON a.id_industri = b.id_industri
									WHERE YEAR(a.bap_tgl) = '$tahun'
									GROUP BY a.id_industri DESC
									ORDER BY a.id_bap DESC
									LIMIT $offset, $limit";
		} else {
			$query = "SELECT YEAR(a.bap_tgl) as tahun, a.id_industri, b.nama_industri, a.id_bap AS bap_terakhir
									FROM bap a
									INNER JOIN industri b ON a.id_industri = b.id_industri
									GROUP BY a.id_industri DESC
									ORDER BY a.id_bap DESC
									LIMIT $offset, $limit";
		}
		
		$results = $this->db->query($query);

		if ($results->num_rows() > 0) {
			foreach ($results->result() as $row) {
				$data[] = $row;
			}
		}

		return $data;
	}

	function get_data_bap($tahun, $id_industri) {

		$data = array();		
		$query = "SELECT YEAR(a.bap_tgl) as tahun, a.id_industri, b.nama_industri, a.id_bap AS bap_terakhir
								FROM bap a
								INNER JOIN industri b ON a.id_industri = b.id_industri
								WHERE YEAR(a.bap_tgl) = '$tahun' AND a.id_industri = '$id_industri'
								GROUP BY a.id_industri DESC
								ORDER BY a.id_bap DESC";
		
		
		$results = $this->db->query($query);
		if ($results->num_rows() > 0) {
			return $results->result();
		}

		return NULL;
	}

	function count_jml_tps_industri($id_industri) {
		$query = $this->db->query("SELECT 
			industri.nama_industri
			FROM izin_tps_limbah
			INNER JOIN tps_limbah ON izin_tps_limbah.id_izin_tps = tps_limbah.id_izin_tps
			INNER JOIN industri ON industri.id_industri = izin_tps_limbah.id_industri
			WHERE izin_tps_limbah.id_industri ='$id_industri'");

		return $query->num_rows();
	}

}
?>