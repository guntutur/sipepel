<!-- 
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">Laporan Teguran LHU&nbsp;&nbsp;<small>rekapitulasi kegiatan swapantau untuk seluruh parameter yang diuji</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li class="active">Laporan Teguran LHU</li>
            </ol>
            <!-- End breadcrumb -->
            <!-- BEGIN DATA TABLE -->
            <div class="the-box">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%" rowspan="2">#</th>
                                <th rowspan="2">Nama Industri</th>
                                <th rowspan="2">Kecamatan</th>
                                <th rowspan="2">Jenis Industri</th>
                                <th colspan="2">Penaatan Minimal</th>
                                <th colspan="3">Penaatan</th>
                                <th colspan="2">Pengujian oleh UPT Labling</th>
                                <th rowspan="2"></th>                                    
                            </tr>
                            <tr>                               
                                <th>Adm.</th>
                                <th>Teknis</th>
                                <th>Adm.</th>
                                <th>Teknis</th>
                                <th>Status</th>                                    
                                <th>Memenuhi</th>
                                <th>Tidak Memenuhi</th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->

        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        url: '<?php echo base_url("backend/laporan_lhu/get_list"); ?>',
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                success: function(d) { 
                    console.log(d); 
                    var t = '', dt = {};
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                        t += '<tr>';
                        t += '<td class="text-center">'+i+'</td>';
                        
                        t += '<td><b>'+dt.nama_industri+'</b><br/>';
                        t += '<small>' + dt.alamat + '</small><br/>';
                        t += '<small>Kec. ' + dt.nama_kecamatan + ' Kel. '+ dt.nama_kelurahan +'</small><br/>';
                        t += '</td>';
                        
                        t += '<td>'+dt.jenis_usaha_kegiatan+'</td>';
                        t += '<td>'+'</td>'; 
                        t += '<td>'+'</td>'; 
                        t += '<td>'+'</td>';
                        t += '</tr>';                    
                    }
                
                $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 
</script>
