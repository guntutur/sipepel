<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_rekap_adm extends CI_Model {
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	// public function get_all() {

	// 	$data = array();

	// 	$sql = " select i.id_industri, i.nama_industri, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Air Limbah' and id_industri = i.id_industri) as kualitas_air, (select group_concat(concat(d.laporan_bulan, ':', (select count(*) from detail_debit_air_limbah where id_debit_air_limbah = d.id_debit_air_limbah)) separator ';') as jumlah from debit_air_limbah d where d.id_industri = i.id_industri) as catatan_debit, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Emisi' and id_industri = i.id_industri) as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Ambien' and id_industri = i.id_industri) as kualitas_ambien from industri i order by i.nama_industri";
		

	// 	$query = $this->db->query($sql);

	// 	if ($query->num_rows() > 0) {
	// 		foreach ($query->result() as $row) {
	// 			$data[] = $row;
	// 		}
	// 	}
	// 	return $data;
	// }

	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();

		$sql = "select i.id_industri, i.nama_industri, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Air Limbah' and id_industri = i.id_industri) as kualitas_air, (select group_concat(d.laporan_bulan separator ';') as jumlah from debit_air_limbah d where d.id_industri = i.id_industri) as catatan_debit, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Emisi' and id_industri = i.id_industri) as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Ambien' and id_industri = i.id_industri) as kualitas_ambien, (select date_format(max(tgl), '%m.%Y') from manifest where id_penghasil_limbah = i.id_industri) as manifest, (select group_concat(date_format(periode_waktu, '%m.%Y')) from neraca_b3 where id_industri = i.id_industri) as neraca_b3 from industri i order by i.nama_industri limit ".$offset.", ".$limit;

		// $sql = "select i.id_industri, i.nama_industri, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Air Limbah' and id_industri = i.id_industri) as kualitas_air, (select group_concat(concat(d.laporan_bulan, ':', (select count(*) from detail_debit_air_limbah where id_debit_air_limbah = d.id_debit_air_limbah)) separator ';') as jumlah from debit_air_limbah d where d.id_industri = i.id_industri) as catatan_debit, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Emisi' and id_industri = i.id_industri) as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Ambien' and id_industri = i.id_industri) as kualitas_ambien, (select date_format(max(tgl), '%m.%Y') from manifest where id_penghasil_limbah = i.id_industri) as manifest, (select group_concat(date_format(periode_waktu, '%m.%Y')) from neraca_b3 where id_industri = i.id_industri) as neraca_b3 from industri i order by i.nama_industri limit ".$offset.", ".$limit;
	
		$query = $this->db->query($sql);
			
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}	
		return $data;
	}

	function get_num_rows($where) {

		$sql = "select i.id_industri, i.nama_industri, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Air Limbah' and id_industri = i.id_industri) as kualitas_air, (select group_concat(d.laporan_bulan separator ';') as jumlah from debit_air_limbah d where d.id_industri = i.id_industri) as catatan_debit, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Emisi' and id_industri = i.id_industri) as kualitas_emisi, (select group_concat(laporan_bulan_tahun separator ';') from laporan_hasil_uji where jenis_lhu = 'LHU Udara Ambien' and id_industri = i.id_industri) as kualitas_ambien, (select date_format(max(tgl), '%m.%Y') from manifest where id_penghasil_limbah = i.id_industri) as manifest, (select group_concat(date_format(periode_waktu, '%m.%Y')) from neraca_b3 where id_industri = i.id_industri) as neraca_b3 from industri i order by i.nama_industri";
		$query = $this->db->query($sql);
		return $this->db->count_all_results();
	}

}
?>