<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading"><?php echo $title; ?>&nbsp;&nbsp;<small>mengelola seluruh data TPS limbah B3</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
				<li>Izin TPS Limbah B3</li>
                <li class="active">Data TPS Limbah B3</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <!-- button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/>   -->

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" onclick="document.location='<?php echo site_url('backend/izin_tps'); ?>';" class="btn btn-sm btn-info"><i class="fa fa-mail-reply"></i>&nbsp;Kembali</button>
                        <button type="button" data-toggle="modal" data-target="#input_detail" onclick="$(this).reset_form_exclude_add('input_detail', true, 'koor_tps');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>                
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Jenis Limbah B3</th>
                                <th>Lama Penyimpanan</th>
								<th>Luas TPS</th>                                
								<th>Koordinat TPS</th>
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0,
			id: '<?php echo $this->uri->segment(4); ?>'
        },
        url: '<?php echo base_url("backend/izin_tps/get_detail_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : ' ';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
						//t += '<td>'+dt.jenis_limbah_b3+'</td>';
						t += '<td><ol style="margin-left:-13px !important;">';
						if(dt.jenis_limbah_b3.length > 0) {
							$.each(dt.jenis_limbah_b3, function(k, v) {  
								t += '<li style="margin-left:-13px !important;">' + v + '</li>';
							});
						}
						t += '</ol>';
						t += '</td>';
						t += '<td>'+dt.lama_penyimpanan+' Hari</td>';
						t += '<td>'+dt.luas_tps+' m2</td>';
						t += '<td>S : '+dt.derajat_s+'&deg;'+dt.jam_s+'&#39;'+dt.menit_s+'&quot;<br>';
						t += 'E : '+dt.derajat_e+'&deg;'+dt.jam_e+'&#39;'+dt.menit_e+'&quot;';
						t += '</td>'
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_tps_limbah+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_tps_limbah+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        
        $("#form_add").validate({
            rules: {
                nama_industri: {
                    required: true
                },
                no_izin:{
                    required: true,
                    maxlength: 100
                },
                tentang: {
                    required: true
                },
				luas_tps: {
                    required: true
                },
                jenis_limbah_b3: {
                    required: true,
                    maxlength: 255
                },
				lama_penyimpanan: {
                    required: true
                },
				jumlah_tps: {
                    required: true
                },
				koordinat_tps_s: {
                    required: true,
                    maxlength: 100
                },
				koordinat_tps_e: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {        
                nama_industri: {
                    required: "Field belum dipilih."
                },
                no_izin: {
                    required: "Masukan nomor izin.",
                    maxlength: "Nama nomer izi maksimal memiliki 100 karakter"
                },
                tentang: {
                    required: "Masukan tentang."
                },
				luas_tps: {
                    required: "Masukan Luas TPS."
                },
                jenis_limbah_b3: {
                    required: "Masukan jenis limbah b3.",
                    maxlength: "Nama usaha/kegiatan maksimal memiliki 250 karakter"
                },
				lama_penyimpanan: {
                    required: "Masukan lama penyimpanan."
                },
				jumlah_tps: {
                    required: "Masukan jumlah tps."
                },
				koordinat_tps_s: {
                    required: "Masukan koordinat tps S.",
                    maxlength: "Koordinat TPS S maksimal memiliki 100 karakter"
                },
				koordinat_tps_e: {
                    required: "Masukan koordinat tps E.",
                    maxlength: "Koordinat TPS E maksimal memiliki 100 karakter"
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }
	});

	$(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }
		
        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/izin_tps/get_detail'); ?>",
                data: $.param({id_tps_limbah: id}),
                success: function (response) {
                    
					console.log(response);

                    $(this).reset_form('input_detail');
					
                   $('input[id=detail_id]').val(response.id_tps_limbah);
					
                    $('input[name=lama_penyimpanan]').val(response.lama_penyimpanan);
					$('input[name=luas_tps]').val(response.luas_tps);
					$('input[name=derajat_s]').val(response.derajat_s);
					$('input[name=jam_s]').val(response.jam_s);
					$('input[name=menit_s]').val(response.menit_s);
					$('input[name=derajat_e]').val(response.derajat_e);
					$('input[name=jam_e]').val(response.jam_e);
					$('input[name=menit_e]').val(response.menit_e);
					
					var t = '';
                    if(response.jenis_limbah_b3.length > 0) {
                        $(this).jenis_limbah_tps_template('jenis_limbah_tps', response.jenis_limbah_b3);
                    }
					
                    $('#input_detail form').attr('action', '<?php echo base_url('/backend/izin_tps/edit_detail') ?>'); //this fails silently
                    $('#input_detail form').get(0).setAttribute('action', '<?php echo base_url('/backend/izin_tps/edit_detail') ?>'); //this works
					
                    $('#input_detail').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        

        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="input_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Izin TPS Limbah B3</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/izin_tps/insert_detail') ?>" method="post" id="form_add">
                <div class="modal-body"> 
					<label>Jenis Limbah</label>
					<div id="jenis_limbah_tps"></div>
					<div id="box_jenis_limbah">
						<div class="form-group row" data-duplicate="koordinat" data-duplicate-min="1">
							<div class="col-lg-10">
								<div class="row">  
									<div class="col-lg-12">
										<select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm" required="required">
											<option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
											<?php foreach($data_acuan as $uk): ?>
												<option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
											<?php endforeach;?>                             
										</select>
									</div>
								</div>
							</div>
							<div class="col-lg-2" style="padding-left: 9px;">
								<div class="text-right">
									<div class="btn-group" style="padding-right: 0;">
										<button type="button" class="btn btn-sm btn-success" data-duplicate-add="koordinat"><i class="fa fa-plus-circle"></i></button>
										<button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="koordinat"><i class="fa fa-times-circle"></i></button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 form-group lama_penyimpanan">
							<label>Lama Penyimpanan</label>
							<div class="input-group input-group-sm"> 
								<input type="text" name="lama_penyimpanan" id="lama_penyimpanan" class="form-control input-sm has-feedback numberonly" autofocus />
								<span class="input-group-addon">hari</span>
							</div>
							<!-- <span class="fa fa-male form-control-feedback"></span> -->             
						</div>
						<div class="col-lg-6 form-group luas_tps">
							<label>Luas TPS</label>
							<div class="input-group input-group-sm"> 
								<input type="text" name="luas_tps" id="luas_tps" class="form-control input-sm has-feedback" autofocus />
								<span class="input-group-addon">m2</span>
							</div>
							<!-- <span class="fa fa-male form-control-feedback"></span> -->             
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<label>Koordinat Outlet S</label>
							<div class="form-group status">
								<div class="row">
									<div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="derajat_s" id="derajat_s" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon"><?php echo '&deg;' ?></span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding: 0; margin:0;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="jam_s" id="jam_s" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">'</span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="menit_s" id="menit_s" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">"</span>
										</div> 
									</div> 
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<label>Koordinat Outlet E</label>
							<div class="form-group status">
								<div class="row">
									<div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="derajat_e" id="derajat_e" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon"><?php echo '&deg;' ?></span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding: 0; margin:0;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="jam_e" id="jam_e" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">'</span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="menit_e" id="menit_e" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">"</span>
										</div> 
									</div> 
								</div>
							</div>
						</div>
					</div>                                                        
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="detail_id" name="id" value="<?php echo $this->uri->segment(4)?>" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus TPS Limbah B3</h4>
            </div>
            <form action="<?php echo base_url('/backend/izin_tps/delete_detail'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->