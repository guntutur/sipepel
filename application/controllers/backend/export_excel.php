<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

 // Document   : perbaikan_item_ketaatan_bap.php 
 // Created on : Januari 5, 2015
 // Author     : guntutur@gmail.com 
 // Description: Controller for menu perbaikan item ketaatan bap

class export_excel extends CI_Controller {

    function __construct() {
        parent::__construct();

        # load model
        $this->load->model('m_bap', 'bapDao', TRUE);
        $this->load->model('m_industry', 'indDao', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);

        $this->load->library('PHPExcel');
    }

    public function teguran_bap() {

        $bap_data = $this->bapDao->get_teguran_bap_excel();

        $form_location = "assets/form/export.xlsx";
         
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($form_location);

        // -------------------------- style --------------------------- //

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->setCellValue('D2', "LAPORAN TEGURAN BAP");
        $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(28);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getStyle('F4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getStyle('G4')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->applyFromArray($style);

        // -------------------------- style --------------------------- //

        // -------------------------- header --------------------------- //

        $objPHPExcel->getActiveSheet()
        ->setCellValue('A4', "No")
        ->setCellValue('B4', "Nama Usaha/Kegiatan")
        ->setCellValue('C4', "Jenis industri")
        ->setCellValue('D4', "Alamat Usaha/Kegiatan")
        ->setCellValue('E4', "Tanggal Pemeriksaan BAP");
        if($this->tank_auth->is_manager() or $this->tank_auth->is_admin()){
            $objPHPExcel->getActiveSheet()->setCellValue('F4', "Pengunggah");
            $objPHPExcel->getActiveSheet()->setCellValue('G4', "Konfirmasi");
        }

        // -------------------------- header --------------------------- //

        // -------------------------- data ----------------------------- //

        $index = count($bap_data);
        if ($index > 0) {
            $base = 5;
            for ($i=0; $i < $index; $i++) { 
                $no = $base+$i;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$no, $i+1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$no, $bap_data[$i]->nama_industri);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$no, $bap_data[$i]->jenis_industri);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$no, $bap_data[$i]->alamat);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$no, $bap_data[$i]->tgl);
                if($this->tank_auth->is_manager() or $this->tank_auth->is_admin()){
                    $objPHPExcel->getActiveSheet()->setCellValue('F'.$no, $bap_data[$i]->conf_pengirim);
                    if(($bap_data[$i]->conf_status==true) && ($bap_data[$i]->status==false)){
                        $approval = "Belum di approve";
                    } else if($bap_data[$i]->status==true) {
                        $approval = "Sudah di approve";
                    } else {
                        $approval = "-";
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue('G'.$no, $approval);
                }
            }
        }

        // -------------------------- data ----------------------------- //

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="daftar_teguran_bap.xlsx"');

        // this shit works like magic
        // ob_end_clean(); 
        // Write file to the browser
        $objWriter->save('php://output');
    }

	public function teguran_perbaikan_bap() {

        $bap_data = $this->bapDao->get_teguran_perbaikan_bap_excel();

        $form_location = "assets/form/export.xlsx";
         
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($form_location);

        // -------------------------- style --------------------------- //

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->setCellValue('D2', "PERBAIKAN ITEM KETAATAN BAP");
        $objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('D2')->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(28);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(38);
        $objPHPExcel->getActiveSheet()->getStyle('F4')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A4:F4')->applyFromArray($style);

        // -------------------------- style --------------------------- //

        // -------------------------- header --------------------------- //

        $objPHPExcel->getActiveSheet()
        ->setCellValue('A4', "No")
        ->setCellValue('B4', "Nama Usaha/Kegiatan")
        ->setCellValue('C4', "Jenis industri")
        ->setCellValue('D4', "Alamat Usaha/Kegiatan")
        ->setCellValue('E4', "Tanggal Pemeriksaan BAP")
        ->setCellValue('F4', "Jumlah Pelanggaran Belum Diperbaiki");

        // -------------------------- header --------------------------- //

        // -------------------------- data ----------------------------- //

        $index = count($bap_data);
        if ($index > 0) {
            $base = 5;
            for ($i=0; $i < $index; $i++) { 
                $no = $base+$i;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$no, $i+1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$no, $bap_data[$i]->nama_industri);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$no, $bap_data[$i]->jenis_usaha);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$no, $bap_data[$i]->alamat);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$no, $bap_data[$i]->tgl);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$no, $bap_data[$i]->jml_teguran);
            }
        }

        // -------------------------- data ----------------------------- //

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="daftar_teguran_perbaikan_bap.xlsx"');

        // this shit works like magic
        // ob_end_clean(); 
        // Write file to the browser
        $objWriter->save('php://output');
    }
	
	public function industri() {
		$industri_data = $this->indDao->get_industri();

        $form_location = "assets/form/export.xlsx";
         
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($form_location);

        // -------------------------- style --------------------------- //

        $style = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $objPHPExcel->getActiveSheet()->setCellValue('B2', "LIST USAHA/KEGIATAN");
        $objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('B2')->applyFromArray($style);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('B4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('C4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('D4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(38);
        $objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('F4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('G4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('H4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('I4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('J4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('K4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('L4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('M4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('N4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('O4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('P4')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(26);
        $objPHPExcel->getActiveSheet()->getStyle('Q4')->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A4:Q4')->applyFromArray($style);

        // -------------------------- style --------------------------- //

        // -------------------------- header --------------------------- //

        $objPHPExcel->getActiveSheet()
        ->setCellValue('A4', "No")
        ->setCellValue('B4', "Nama Usaha/Kegiatan")
        ->setCellValue('D4', "Jenis industri")
        ->setCellValue('C4', "Jenis Usaha/Kegiatan")
        ->setCellValue('E4', "Alamat")
        ->setCellValue('F4', "Kelurahan")
		->setCellValue('G4', "Kecamatan")
		->setCellValue('H4', "Telp")
		->setCellValue('I4', "Fax")
		->setCellValue('J4', "Email")
		->setCellValue('K4', "PJ Industri")
		->setCellValue('L4', "Jumlah Pegawai")
		->setCellValue('M4', "Jumlah Hari Kerja (Bulan)")
		->setCellValue('N4', "Jumlah Jam Kerja (Hari)")
		->setCellValue('O4', "Luas Area")
		->setCellValue('P4', "Luas Bangunan")
		->setCellValue('Q4', "Pimpinan");

        // -------------------------- header --------------------------- //

        // -------------------------- data ----------------------------- //

        $index = count($industri_data);
        if ($index > 0) {
            $base = 5;
            for ($i=0; $i < $index; $i++) { 
                $no = $base+$i;
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$no, $i+1);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$no, $industri_data[$i]->nama_industri);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$no, $industri_data[$i]->nama_jenis_industri);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$no, $industri_data[$i]->nama_usaha_kegiatan);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$no, $industri_data[$i]->alamat);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$no, $industri_data[$i]->nama_kelurahan);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$no, $industri_data[$i]->nama_kecamatan);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$no, $industri_data[$i]->tlp);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$no, $industri_data[$i]->fax);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$no, $industri_data[$i]->email);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$no, $industri_data[$i]->pj_industri);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$no, $industri_data[$i]->jml_pegawai);$objPHPExcel->getActiveSheet()->setCellValue('M'.$no, $industri_data[$i]->hari_kerja_bulan);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$no, $industri_data[$i]->jam_kerja_hari);
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$no, $industri_data[$i]->luas_area);
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$no, $industri_data[$i]->luas_bangunan);
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$no, $industri_data[$i]->pimpinan);
            }
        }

        // -------------------------- data ----------------------------- //

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        // We'll be outputting an excel file
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="daftar_industri.xlsx"');

        // this shit works like magic
        // ob_end_clean(); 
        // Write file to the browser
        $objWriter->save('php://output');
	}

}
