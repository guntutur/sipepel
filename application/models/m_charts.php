<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Document   : m_charts.php 
// Created on : January 11, 2015. 16:54 
// Author     : huda.jtk09@gmail.com 
// Description: Model for Charts 

class m_charts extends CI_Model {

	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');

        $this->load->model("m_bap");
	}

	/**
	 * huda : get total pelanggaran bap per-year
	 *
	 * @param	Int 
	 * @return	Array of Object
	 */
	function get_year_langgar_bap() {
		$data = array();

		$query = $this->db->query("SELECT YEAR(b.bap_tgl) as year, COUNT(DISTINCT(b.id_industri)) as total
									FROM `history_item_teguran_bap` a
									LEFT JOIN bap b ON a.id_bap = b.id_bap
									GROUP BY YEAR(b.bap_tgl)
									ORDER BY YEAR(b.bap_tgl) ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * huda : get total pelanggaran lhu per-year
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_year_langgar_lhu() {
		$data = array();

		$query = $this->db->query("SELECT SUBSTRING(b.laporan_bulan_tahun,4) as year, COUNT(DISTINCT(b.id_industri)) as total
									FROM history_item_teguran_lhu a
									LEFT JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
									GROUP BY SUBSTRING(b.laporan_bulan_tahun,4)
									ORDER BY SUBSTRING(b.laporan_bulan_tahun,4) ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}
	
	/**
	 * huda : get pelanggaran lhu per-item & per-year
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_item_langgar_lhu($year) {
		$data = array();

		$query = $this->db->query("SELECT a.parameter_lhu, COUNT(a.parameter_lhu) as total
									FROM history_item_teguran_lhu a
									LEFT JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
									WHERE SUBSTRING(b.laporan_bulan_tahun,4) = ".$year." 
									GROUP BY a.parameter_lhu");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}
	/**
	 * huda : get pelanggaran lhu per-item
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_item_langgar_lhu_2() {
		$data = array();

		$query = $this->db->query("SELECT a.parameter_lhu, COUNT(a.parameter_lhu) as total
									FROM history_item_teguran_lhu a
									LEFT JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
									GROUP BY a.parameter_lhu");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * huda : get total industri melanggar bap per-year
	 *
	 * @param	Int 
	 * @return	Array of Object
	 */
	function get_year_industri_langgar_bap() {
		$data = array();

		$query = $this->db->query("SELECT YEAR(b.bap_tgl) as year, c.nama_industri, COUNT(a.`id_history_teguran_bap`) as total
									FROM `history_item_teguran_bap` a
									LEFT JOIN bap b ON a.id_bap = b.id_bap
									LEFT JOIN industri c ON b.id_industri = c.id_industri
									GROUP BY YEAR(b.bap_tgl), c.id_industri
									ORDER BY c.nama_industri ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * huda : get total industri melanggar lhu per-year
	 *
	 * @param	String 
	 * @return	Array of Object
	 */
	function get_year_industri_langgar_lhu() {
		$data = array();

		$query = $this->db->query("SELECT SUBSTRING(b.laporan_bulan_tahun,4) as year, c.nama_industri, COUNT(a.id_laporan_hasil_uji) as total
									FROM history_item_teguran_lhu a
									LEFT JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
									LEFT JOIN industri c ON b.id_industri = c.id_industri
									GROUP BY SUBSTRING(b.laporan_bulan_tahun,4), c.id_industri
									ORDER BY c.nama_industri ASC");
		
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;
	}

	function select_tahun5($type) {
        $data = array();

        //$query = $this->db->query("SELECT DISTINCT (YEAR(tgl_pembuatan)) as tahun FROM $type WHERE tgl_pembuatan");
        if ($type=="bap") {
            $query = $this->db->query("SELECT DISTINCT (YEAR(bap_tgl)) as tahun FROM bap WHERE bap_tgl");
        } elseif ($type=="laporan_hasil_uji") {
            $query = $this->db->query("SELECT DISTINCT SUBSTRING_INDEX(laporan_bulan_tahun,'.',-1) as tahun FROM laporan_hasil_uji WHERE laporan_bulan_tahun");
        }

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    function select_tahun_trend_ketaatan() {
        $data = array();

        /*$query = $this->db->query("select DISTINCT YEAR(tgl_pembuatan) as tahun FROM bap WHERE tgl_pembuatan
                            UNION
                            SELECT DISTINCT year(tgl_pembuatan) as tahun FROM laporan_hasil_uji WHERE tgl_pembuatan
                            ORDER BY tahun DESC");*/

        $query = $this->db->query("select DISTINCT YEAR(bap_tgl) as tahun FROM bap WHERE bap_tgl
                            UNION
                            SELECT DISTINCT SUBSTRING_INDEX(laporan_bulan_tahun,'.',-1) as tahun FROM laporan_hasil_uji WHERE laporan_bulan_tahun
                            ORDER BY tahun DESC");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    function select_tahun_trend_dokumen() {
        $data = array();

        /*$query = $this->db->query("
SELECT DISTINCT YEAR(tgl_pembuatan) as tahun FROM bap WHERE tgl_pembuatan
UNION
SELECT DISTINCT year(tgl_pembuatan) as tahun FROM laporan_hasil_uji WHERE tgl_pembuatan
UNION
SELECT DISTINCT year(tgl_pembuatan) AS tahun FROM debit_air_limbah WHERE tgl_pembuatan
UNION
SELECT DISTINCT year(tgl_pembuatan) AS tahun FROM manifest WHERE tgl_pembuatan
UNION
SELECT DISTINCT year(tgl_pembuatan) AS tahun FROM neraca_b3 WHERE tgl_pembuatan
ORDER BY tahun DESC");*/

        $query = $this->db->query("
SELECT DISTINCT YEAR(bap_tgl) as tahun FROM bap WHERE bap_tgl
UNION
SELECT DISTINCT SUBSTRING_INDEX(laporan_bulan_tahun,'.',-1) as tahun FROM laporan_hasil_uji WHERE laporan_bulan_tahun
UNION
SELECT DISTINCT SUBSTRING_INDEX(laporan_bulan,'.',-1) AS tahun FROM debit_air_limbah WHERE laporan_bulan
UNION
SELECT DISTINCT year(tgl_angkut) AS tahun FROM manifest WHERE tgl_angkut
UNION
SELECT DISTINCT year(periode_waktu) AS tahun FROM neraca_b3 WHERE periode_waktu
ORDER BY tahun DESC");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    function persebaran_industri(array $id_kecamatan = array()) {
        $tmp_data = array();

        $usaha_kegiatan = $this->m_bap->select_table("usaha_kegiatan");

        foreach ($usaha_kegiatan as $u => $ka) {
            foreach ($id_kecamatan as $k => $v) {
                $query = $this->db->query("SELECT COUNT(id_usaha_kegiatan) as sum_industri 
                        FROM industri 
                    WHERE id_usaha_kegiatan = $ka->id_usaha_kegiatan AND id_kecamatan = $v")->row();

                $rows[$ka->ket][] = (int)$query->sum_industri;
            }
        }

        $idx = 0;
        foreach ($rows as $key => $value) {
            $data[$idx]['name'] = $key;
            $data[$idx]['data'] = $value;
            $idx++;
        }

        return $data;
    }

    function top5param_lhu($tahun, $jenis_lhu, $usaha_kegiatan) {
        $data = array();

        /*$query = $this->db->query("select a.parameter_lhu, count(a.parameter_lhu) as sum_item, d.ket, YEAR(b.tgl_pembuatan) as tahun
                            FROM history_item_teguran_lhu a
                              JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
                              JOIN industri c ON b.id_industri = c.id_industri
                              JOIN usaha_kegiatan d ON c.id_usaha_kegiatan = d.id_usaha_kegiatan
                              WHERE YEAR(b.tgl_pembuatan) = $tahun AND b.jenis_lhu = \"$jenis_lhu\" AND c.id_usaha_kegiatan = $usaha_kegiatan
                            GROUP BY a.parameter_lhu, tahun
                            ORDER BY sum_item DESC
                            LIMIT 5");*/

        $query = $this->db->query("select a.parameter_lhu, count(a.parameter_lhu) as sum_item, d.ket, YEAR(b.tgl_pembuatan) as tahun
                            FROM history_item_teguran_lhu a
                              JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
                              JOIN industri c ON b.id_industri = c.id_industri
                              JOIN usaha_kegiatan d ON c.id_usaha_kegiatan = d.id_usaha_kegiatan
                              WHERE SUBSTRING_INDEX(laporan_bulan_tahun,'.',-1) = $tahun AND b.jenis_lhu = \"$jenis_lhu\" AND c.id_usaha_kegiatan = $usaha_kegiatan 
                            GROUP BY a.parameter_lhu, tahun
                            ORDER BY sum_item DESC
                            LIMIT 5");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data['param_name'][] = $row->parameter_lhu;
                $data['param_value'][] = (int) $row->sum_item;
            }
        }

        return $data;

    }

    function top5param_bap($tahun, $usaha_kegiatan) {
        $data = array();

        /*WHERE YEAR(b.tgl_pembuatan) = $tahun AND d.id_usaha_kegiatan = $usaha_kegiatan*/
        $query = $this->db->query("select a.ket_parameter_bap, count(a.ket_parameter_bap) sum_item, d.ket, YEAR(b.tgl_pembuatan) as tahun
                            FROM history_item_teguran_bap a
                              JOIN bap b ON a.id_bap = b.id_bap
                              JOIN industri c ON b.id_industri = c.id_industri
                              JOIN usaha_kegiatan d ON c.id_usaha_kegiatan = d.id_usaha_kegiatan
                              WHERE YEAR(b.bap_tgl) = $tahun AND d.id_usaha_kegiatan = $usaha_kegiatan
                            GROUP BY a.ket_parameter_bap, tahun
                              ORDER BY sum_item DESC
                            LIMIT 5");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data['param_name'][] = $row->ket_parameter_bap;
                $data['param_value'][] = (int) $row->sum_item;
            }
        }

        return $data;

    }

    function trendline_ketaatan_industri($tahun) {
        $data = array();

        /*ketaatan lhu udara emisi*/
        $query = $this->db->query("select YEAR(b.tgl_pembuatan) as tahun, count(a.parameter_lhu) as sum_pelanggaran_lhu_udara
                            FROM history_item_teguran_lhu a
                              JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
                              WHERE b.jenis_lhu = 'LHU Udara Emisi' 
                            GROUP BY tahun");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_lhu_udara[$row->tahun] = $row->sum_pelanggaran_lhu_udara;
            }
        }

        /*ketaatan lhu air limbah*/
        $query = $this->db->query("select YEAR(b.tgl_pembuatan) as tahun, count(a.parameter_lhu) as sum_pelanggaran_lhu_air
                            FROM history_item_teguran_lhu a
                              JOIN laporan_hasil_uji b ON a.id_laporan_hasil_uji = b.id_laporan_hasil_uji
                              WHERE b.jenis_lhu = 'LHU Air Limbah' 
                            GROUP BY tahun");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_lhu_air[$row->tahun] = $row->sum_pelanggaran_lhu_air;
            }
        }

        /*ketaatan bap*/
        $query = $this->db->query("SELECT YEAR(tgl_pembuatan) as tahun, count(id_history_teguran_bap) as sum_pelanggaran_bap FROM history_item_teguran_bap GROUP BY tahun");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_bap[$row->tahun] = $row->sum_pelanggaran_bap;
            }
        }

        $bap["name"] = "BAP";
        $lhu_udara["name"] = "LHU Udara Emisi";
        $lhu_air["name"] = "LHU Air Limbah";
        for ($diff = $tahun - 4; $diff <= $tahun; $diff++) {
            $array_tahun[] = (int) $diff;
            $bap["data"][] = isset($tmp_bap[$diff]) ? (int) $tmp_bap[$diff] : 0;
            $lhu_udara["data"][] = isset($tmp_lhu_udara[$diff]) ? (int) $tmp_lhu_udara[$diff] : 0;
            $lhu_air["data"][] = isset($tmp_lhu_air[$diff]) ? (int) $tmp_lhu_air[$diff] : 0;
        }

        $data['category'] = $array_tahun;
        $data['data'][] = $bap;
        $data['data'][] = $lhu_udara;
        $data['data'][] = $lhu_air;

        return $data;
    }

    function trendline_dokumen_bplh($tahun, $id_usaha_kegiatan) {
        $data = array();

        /*bap*/
        /*$query = $this->db->query("SELECT YEAR(a.tgl_pembuatan) as tahun, count(a.id_bap) as dok FROM bap a*/
        $query = $this->db->query("SELECT YEAR(a.bap_tgl) as tahun, count(a.id_bap) as dok FROM bap a
JOIN industri b ON a.id_industri = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_bap[$row->tahun] = $row->dok;
            }
        }

        /*lhu udara ambien*/
        /*$query = $this->db->query("SELECT YEAR(a.tgl_pembuatan) as tahun, count(a.id_laporan_hasil_uji) as dok FROM laporan_hasil_uji a*/
        $query = $this->db->query("SELECT SUBSTRING_INDEX(a.laporan_bulan_tahun,'.',-1) as tahun, count(a.id_laporan_hasil_uji) as dok FROM laporan_hasil_uji a
JOIN industri b ON a.id_industri = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan AND jenis_lhu = 'LHU Udara Ambien'
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_lhu_udara_ambien[$row->tahun] = $row->dok;
            }
        }

        /*lhu udara emisi*/
        /*$query = $this->db->query("SELECT YEAR(a.tgl_pembuatan) as tahun, count(a.id_laporan_hasil_uji) as dok FROM laporan_hasil_uji a*/
        $query = $this->db->query("SELECT SUBSTRING_INDEX(a.laporan_bulan_tahun,'.',-1) as tahun, count(a.id_laporan_hasil_uji) as dok FROM laporan_hasil_uji a
JOIN industri b ON a.id_industri = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan AND jenis_lhu = 'LHU Udara Emisi'
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_lhu_udara_emisi[$row->tahun] = $row->dok;
            }
        }

        /*lhu air limbah*/
        /*$query = $this->db->query("SELECT YEAR(a.tgl_pembuatan) as tahun, count(a.id_laporan_hasil_uji) as dok FROM laporan_hasil_uji a*/
        $query = $this->db->query("SELECT SUBSTRING_INDEX(a.laporan_bulan_tahun,'.',-1) as tahun, count(a.id_laporan_hasil_uji) as dok FROM laporan_hasil_uji a
JOIN industri b ON a.id_industri = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan AND jenis_lhu = 'LHU Air Limbah'
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_lhu_air_limbah[$row->tahun] = $row->dok;
            }
        }

        /*debit air limbah*/
        $query = $this->db->query("SELECT SUBSTRING_INDEX(a.laporan_bulan,'.',-1) as tahun, count(a.id_debit_air_limbah) as dok FROM debit_air_limbah a
JOIN industri b ON a.id_industri = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_debit_air_limbah[$row->tahun] = $row->dok;
            }
        }

        /*neraca b3*/
        $query = $this->db->query("SELECT year(a.periode_waktu) as tahun, count(a.id_neraca) as dok FROM neraca_b3 a
JOIN industri b ON a.id_industri = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_manifest[$row->tahun] = $row->dok;
            }
        }

        /*manifest*/
        $query = $this->db->query("SELECT year(a.tgl_angkut) as tahun, count(a.id_manifest) as dok FROM manifest a
JOIN industri b ON a.id_penghasil_limbah = b.id_industri
  WHERE b.id_usaha_kegiatan = $id_usaha_kegiatan
GROUP BY tahun");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tmp_neraca[$row->tahun] = $row->dok;
            }
        }


        $bap["name"] = "Dok BAP";
        $lhu_udara_emisi["name"] = "Dok LHU Udara Emisi";
        $lhu_udara_ambien["name"] = "Dok LHU Udara Ambien";
        $lhu_air_limbah["name"] = "Dok LHU Air Limbah";
        $debit_air_limbah["name"] = "Dok Debit Air Limbah";
        $manifest["name"] = "Dok Manifest";
        $neraca["name"] = "Dok Neraca B3";
        for ($diff = $tahun+1 - 5; $diff < $tahun+1; $diff++) {
            $array_tahun[] = (int) $diff;
            $bap["data"][] = isset($tmp_bap[$diff]) ? (int) $tmp_bap[$diff] : 0;
            $lhu_udara_ambien["data"][] = isset($tmp_lhu_udara_ambien[$diff]) ? (int) $tmp_lhu_udara_ambien[$diff] : 0;
            $lhu_udara_emisi["data"][] = isset($tmp_lhu_udara_emisi[$diff]) ? (int) $tmp_lhu_udara_emisi[$diff] : 0;
            $lhu_air_limbah["data"][] = isset($tmp_lhu_air_limbah[$diff]) ? (int) $tmp_lhu_air_limbah[$diff] : 0;
            $debit_air_limbah["data"][] = isset($tmp_debit_air_limbah[$diff]) ? (int) $tmp_debit_air_limbah[$diff] : 0;
            $manifest["data"][] = isset($tmp_manifest[$diff]) ? (int) $tmp_manifest[$diff] : 0;
            $neraca["data"][] = isset($tmp_neraca[$diff]) ? (int) $tmp_neraca[$diff] : 0;
        }

        $data['category'] = $array_tahun;
        $data['data'][] = $bap;
        $data['data'][] = $lhu_udara_ambien;
        $data['data'][] = $lhu_udara_emisi;
        $data['data'][] = $lhu_air_limbah;
        $data['data'][] = $debit_air_limbah;
        $data['data'][] = $manifest;
        $data['data'][] = $neraca;

        return $data;
    }
}