			<!-- BEGIN SIDEBAR LEFT -->
			<div class="sidebar-left sidebar-nicescroller dark-color">
				<ul class="sidebar-menu">
					<li>
						<a href="<?php echo base_url('/backend'); ?>">
							<i class="fa fa-dashboard icon-sidebar"></i>
							Dashboard						
						</a>
					</li>
					<?php if($this->tank_auth->is_admin() or $this->tank_auth->is_super_admin()) { ?>
					<li class="<?php echo ($this->uri->segment(2) == 'user') ? 'active selected' : ''; ?>" >
						<a href="<?php echo base_url('/backend/user'); ?>">
							<i class="fa fa-users icon-sidebar"></i>
							Manajemen User
						</a>
					</li>
					<?php } ?>										
					<li class="static">MENU OPERATOR</li>
					<li id="manajemen_data">
						<a href="">
							<i class="fa fa-database icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Manajemen Data
						</a>
						<ul class="submenu">
							<li class="<?php echo ($this->uri->segment(2) == 'data_master' OR $this->uri->segment(2) == 'data_master' ) ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/data_master'); ?>">Data Acuan</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'kecamatan' OR $this->uri->segment(2) == 'kelurahan' ) ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/kecamatan'); ?>">Kecamatan</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'pegawai') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/pegawai'); ?>">Pegawai</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'laboratorium') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/laboratorium'); ?>">Laboratorium</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'usaha_kegiatan' OR $this->uri->segment(2) == 'jenis_industri' ) ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/usaha_kegiatan'); ?>">Jenis Usaha/Kegiatan</a></li>							
							<li class="<?php echo ($this->uri->segment(2) == 'industri') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/industri'); ?>">Nama Usaha/Kegiatan</a></li>							
							<li class="<?php echo ($this->uri->segment(2) == 'bap_parameter') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/bap_parameter'); ?>">Item Ketaatan BAP</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'baku_mutu_lhu') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/baku_mutu_lhu'); ?>">Baku Mutu LHU</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'jenis_dokumen') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/jenis_dokumen'); ?>">Periode Adm Dokumen</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'izin_pembuangan') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/izin_pembuangan'); ?>">Izin Pembuangan Air Limbah</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'izin_tps') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/izin_tps'); ?>">Izin TPS Limbah B3</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'pengangkut_limbah') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/pengangkut_limbah'); ?>">Pengangkut Limbah B3</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'pengolah_limbah') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/pengolah_limbah'); ?>">Pengolah Limbah B3</a></li>							
																					
						</ul>
					</li>
					<li id="manajemen_bap">
						<a href="#fakelink">
							<i class="fa fa-binoculars icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Berita Acara Pengawasan		
						</a>
						<ul class="submenu">							
							<li class="<?php echo ($this->uri->segment(2) == 'bap_agro') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/bap_agro'); ?>">BAP Agro</a></li>							
							<li class="<?php echo ($this->uri->segment(2) == 'bap_golf') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_golf'); ?>">BAP Golf</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_hotel') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_hotel'); ?>">BAP Hotel</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_industri') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_industri'); ?>">BAP Industri</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_lab') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_lab'); ?>">BAP Laboratorium</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_plb3') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_plb3'); ?>">BAP Pengelola Limbah B3</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_tambang') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_tambang'); ?>">BAP Pertambangan</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_rph') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_rph'); ?>">BAP RPH</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_rm') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_rm'); ?>">BAP Rumah Makan</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_rs') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_rs'); ?>">BAP Rumah Sakit</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'bap_sppbe') ? 'active selected' : ''; ?>"><a href="<?php echo base_url('/backend/bap_sppbe'); ?>">BAP SPPBE</a></li>
						</ul>
					</li>							
					<li id="lhu">
						<a href="#fakelink">
							<i class="fa fa-flask icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Laporan Hasil Uji
						</a>
						<ul class="submenu">
							<li class="<?php echo ($this->uri->segment(2) == 'lhu_air_limbah') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/lhu_air_limbah'); ?>">Air Limbah</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'lhu_udara_emisi') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/lhu_udara_emisi'); ?>">Udara Emisi</a></li>
							<li class="<?php echo ($this->uri->segment(2) == 'lhu_udara_ambien') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/lhu_udara_ambien'); ?>">Udara Ambien</a></li>							
						</ul>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'debit_air_limbah') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/debit_air_limbah'); ?>">
								<i class="fa fa-calendar icon-sidebar"></i>
								Debit Air Limbah
							</a>
						</li>
					</li>
					<li id="lhu">
						<a href="#fakelink">
							<i class="fa fa-suitcase icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Manifest
						</a>
						<ul class="submenu">
							<li class="<?php echo ($this->uri->segment(2) == 'manifest') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/manifest'); ?>">Data Manifest</a></li>							
							<li class="<?php echo ($this->uri->segment(2) == 'rekap_manifest') ? 'active selected' : ''; ?>" ><a href="<?php echo base_url('/backend/rekap_manifest'); ?>">Rekap Manifest</a></li>												
						</ul>
					</li>
					<!-- <li>
						<li class="<?php echo ($this->uri->segment(2) == 'rekap_manifest') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/rekap_manifest'); ?>">
								<i class="fa fa-suitcase icon-sidebar"></i>
								Rekap Manifest
							</a>
						</li>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'manifest') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/manifest'); ?>">
								<i class="fa fa-suitcase icon-sidebar"></i>
								Manifest
							</a>
						</li>
					</li> -->
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'neraca_b3') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/neraca_b3'); ?>">
								<i class="fa fa-fire icon-sidebar"></i>
								Neraca Limbah B3
							</a>
						</li>
					</li>	
					<li class="static">TEGURAN</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'teguran_bap' && $this->uri->segment(3) == '') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/teguran_bap'); ?>">
								<i class="fa fa-warning icon-sidebar"></i>
								Berita Acara Pengawasan
							</a>
						</li>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(3) == 'manager') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/teguran_bap/manager'); ?>">
								<i class="fa fa-warning icon-sidebar"></i>
								Berita Acara Pengawasan Manager
							</a>
						</li>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'teguran_lhu') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/teguran_lhu'); ?>">
								<i class="fa fa-warning icon-sidebar"></i>
								Laporan Hasil Uji
							</a>
						</li>
					</li>
					<!-- <li>
						<li class="<?php echo ($this->uri->segment(2) == 'rekap_adm_swapantau') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/rekap_adm_swapantau'); ?>">
								<i class="fa fa-warning icon-sidebar"></i>
								Administrasi
							</a>
						</li>
					</li> -->
					<li class="static">REKAPITULASI</li>
					<li class="<?php echo ($this->uri->segment(2) == 'rekap_bap') ? 'active selected' : ''; ?>">
						<a href="<?php echo base_url('/backend/rekap_bap'); ?>">
							<i class="fa fa-bar-chart-o icon-sidebar"></i>
							Berita Acara Pengawasan
						</a>
					</li>
					<li class="<?php echo ($this->uri->segment(2) == 'rekap_lhu') ? 'active selected' : ''; ?>">
						<a href="<?php echo base_url('/backend/rekap_lhu'); ?>">
							<i class="fa fa-bar-chart-o icon-sidebar"></i>
							Laporan Hasil Uji
						</a>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'rekap_adm_swapantau') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/rekap_adm_swapantau'); ?>">
								<i class="fa fa-bar-chart-o icon-sidebar"></i>
								Periode Administrasi
							</a>
						</li>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'ketaatan_industri') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/ketaatan_industri'); ?>">
								<i class="fa fa-bar-chart-o icon-sidebar"></i>
								Ketaatan Industri
							</a>
						</li>
					</li>
					<li>
						<li class="<?php echo ($this->uri->segment(2) == 'raport_industri') ? 'active selected' : ''; ?>" >
							<a href="<?php echo base_url('/backend/raport_industri'); ?>">
								<i class="fa fa-bar-chart-o icon-sidebar"></i>
								Raport Industri
							</a>
						</li>
					</li>
					<li class="<?php echo ($this->uri->segment(2) == 'rekap_izin') ? 'active selected' : ''; ?>">
						<a href="<?php echo base_url('/backend/rekap_izin'); ?>">
							<i class="fa fa-bar-chart-o icon-sidebar"></i>
							Masa Kontrak Izin
						</a>
					</li>
				</ul>
			</div><!-- /.sidebar-left -->
			<!-- END SIDEBAR LEFT -->
			
			<script type="text/javascript">
				$('#manajemen_data').removeClass('active selected');
				$('#manajemen_data').find('.submenu').removeClass('visible');

				if ( $("#manajemen_data").find('li.active').length != 0 ) {
					$('#manajemen_data').addClass('active selected');
					$('#manajemen_data').find('.submenu').addClass('visible');
				}

				$('#manajemen_bap').removeClass('active selected');
				$('#manajemen_bap').find('.submenu').removeClass('visible');

				if ( $("#manajemen_bap").find('li.active').length != 0 ) {
					$('#manajemen_bap').addClass('active selected');
					$('#manajemen_bap').find('.submenu').addClass('visible');
				}

				$('#lhu').removeClass('active selected');
				$('#lhu').find('.submenu').removeClass('visible');

				if ( $("#lhu").find('li.active').length != 0 ) {
					$('#lhu').addClass('active selected');
					$('#lhu').find('.submenu').addClass('visible');
				}
			</script>