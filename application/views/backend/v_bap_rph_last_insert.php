    <!-- 
    ===========================================================
    BEGIN PAGE
    ===========================================================
    -->
    <div class="wrapper">

        <?php $this->load->view('include/top_nav'); ?>
        <?php $this->load->view('include/sidebar'); ?>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- Begin page heading -->
                <h1 class="page-heading">BAP RPH&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan RPH</small></h1>
                <!-- End page heading -->

                <!-- Begin breadcrumb -->
                <ol class="breadcrumb default square rsaquo sm">
                    <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                    <li>Manajemen BAP</li>
                    <li class="active">BAP RPH</li>
                </ol>
                <!-- End breadcrumb -->

                <!-- BEGIN DATA TABLE -->
                <div class="the-box no-padding">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#edit_new" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Ubah BAP</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="edit_new">
                            <div class="row" style="margin-top: 10px;">       
                                <div class="col-xs-3" id="vertical_tab"> <!-- required for floating -->
                                  <!-- Nav tabs -->
                                  <ul class="nav nav-tabs tabs-left">
                                    <li class="active"><a href="#data_umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                    <li><a href="#pencemaran_air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                    <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                    <li><a href="#pencemaran_b3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                    <li><a href="#lain_lain" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Lain-lain</a></li>
                                  </ul>
                                </div>
                                <div class="col-xs-9">
                                <!-- Tab panes -->
                                    <form role="form" id="add_form" action="<?php echo base_url('backend/bap_rph/newInsert') ?>" method="post">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="data_umum">
                                                <h4 class="page-tab">Data Umum</h4>
                                                
                                                <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>Tanggal</label>
                                                    <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>" >                  
                                                </div>
                                                <div class="form-group col-lg-6">
                                        <label>Pukul</label>
                                        <div class="bfh-timepicker">
                                            <input id="timepick1" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                        </div>
                                    </div>
                                    </div>
                                                <div class="row">
                                                    <div class="form-group col-lg-6">
                                                        <label>Petugas Pengawas</label>
                                                        <select class="form-control input-sm" name="id_pegawai">
                                                            <option value=""></option>
                                                            <?php 
                                                                foreach ($pegawai as $p) {
                                                                    echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                                }?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Beserta Anggota Pengawas:</label>
                                                        <select data-placeholder="Pilih Pengawas..." class="form-control input-sm multiselect" multiple="multiple" name="id_petugas_bap_list[]" style="border: 0!important;">
                                                            <?php 
                                                                foreach ($pegawai as $p) {
                                                                    echo "<option value='$p->id_pegawai' ";
                                                                    for ($i=0; $i < count($petugas_bap); $i++) { 
                                                                        if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                                            echo "selected";
                                                                        }
                                                                    }
                                                                    echo ">$p->nama_pegawai</option>";
                                                                }?>
                                                        </select>
                                                    </div>
                                                </div> <br>
                                                <div class="form-group">
                                                    <h4>Lokasi Pengawasan / Pembinaan</h4>    
                                                    <div class="form-group ">
                                                        <label>Nama Usaha / Kegiatan</label>
                                                        <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                            <option value="Empty">&nbsp;</option>
                                                            <?php
                                                                foreach ($industri as $value) {
                                                                    echo '<option value="'.$value->id_industri.'"';
                                                                        if($bap->id_industri==$value->id_industri){
                                                                            echo 'selected';
                                                                        }
                                                                    echo '>'.$value->nama_industri.'</option>';
                                                                }
                                                            ?>
                                                    </select>
                                                        <!-- <input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                                    <input type="hidden" id="id_industri" name="id_industri" value="0"/> -->
                                                    </div>
                                                    <div id="container2" style="display:none"></div>
                                                </div> 
                                                <div class="form-group">
                                                    <h4>Penanggung Jawab Usaha / Kegiatan</h4>
                                                    <div class="form-group ">
                                                        <label>Nama</label>
                                                        <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama;?>"/>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label>Telepon</label>
                                                        <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp;?>"/>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label>Jabatan</label>
                                                        <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan;?>"/>
                                                    </div> 
                                                </div>

                                                <h4 class="page-tab">Data Umum Usaha/Kegiatan</h4>
                                                <div class="form-group ">
                                                    <label>Jumlah Karyawan</label>
                                                    <input type="text" name="jumlah_karyawan" class="form-control input-sm has-feedback" value="<?php echo $bap_rph->jumlah_karyawan;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Jumlah Ternak</label>
                                                    <input type="text" name="jumlah_ternak" class="form-control input-sm has-feedback" value="<?php echo $bap_rph->jumlah_ternak;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <label>Koordinat Lokasi S</label>
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $bap_rph->koord_rph_derajat_s?>" name="koord_rph_derajat_s" id="derajat_s" class="form-control input-sm has-feedback" />
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $bap_rph->koord_rph_jam_s?>" name="koord_rph_jam_s" id="derajat_s" class="form-control input-sm has-feedback" />
                                                                    <span class="input-group-addon">'</span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $bap_rph->koord_rph_menit_s?>" name="koord_rph_menit_s" id="derajat_s" class="form-control input-sm has-feedback" />
                                                                    <span class="input-group-addon">"</span>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label>Koordinat Lokasi E</label>
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $bap_rph->koord_rph_derajat_e?>" name="koord_rph_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $bap_rph->koord_rph_jam_e?>" name="koord_rph_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon">'</span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $bap_rph->koord_rph_menit_e?>" name="koord_rph_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon">"</span>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Dokumen Lingkungan</label>
                                                            <select class="form-control input-sm" name="dok_lingk">
                                                                <option value="1" <?php echo (($bap->dok_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                                                                <option value="0" <?php echo (($bap->dok_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Tahun</label>
                                                            <input type="text" name="dok_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun;?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Jenis</label>
                                                    <!-- <select class="form-control input-sm" name="dok_lingk_jenis"> -->
                                                        <!-- <option value="" <?php //echo (($bap->dok_lingk_jenis== '') ? 'selected' : ''); ?> ></option> -->
                                                        <!-- <option value="Amdal" <?php //echo (($bap->dok_lingk_jenis== 'Amdal') ? 'selected' : ''); ?> >Amdal</option> -->
                                                        <!-- <option value="UKL-UPL" <?php //echo (($bap->dok_lingk_jenis== 'UKL-UPL') ? 'selected' : ''); ?> >UKL-UPL</option> -->
                                                        <!-- <option value="DELH" <?php //echo (($bap->dok_lingk_jenis== 'DELH') ? 'selected' : ''); ?> >DELH</option> -->
                                                        <!-- <option value="DPLH" <?php //echo (($bap->dok_lingk_jenis== 'DPLH') ? 'selected' : ''); ?> >DPLH</option> -->
                                                        <!-- <option value="SPPL" <?php //echo (($bap->dok_lingk_jenis== 'SPPL') ? 'selected' : ''); ?> >SPPL</option> -->
                                                    <!-- </select> -->
                                                    <select class="form-control input-sm" name="dok_lingk_jenis" id="jdl_name">
                                                        <option value="">-- Pilih salah satu --</option>
                                                        <?php 
                                                            foreach ($jenis_dok_lingkungan as $j) {
                                                                echo "<option value='$j->ket' ".(($bap->dok_lingk_jenis == $j->ket) ? 'selected' : '').">$j->ket</option>";
                                                        }?>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Izin Lingkungan</label>
                                                            <select class="form-control input-sm" name="izin_lingk">
                                                                <option value="1" <?php echo (($bap->izin_lingk== true) ? 'selected' : ''); ?> >Ada</option>
                                                                <option value="0" <?php echo (($bap->izin_lingk== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Tahun</label>
                                                            <input type="text" name="izin_lingk_tahun" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun;?>"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row"> <!-- navigation -->
                                                    <div class="col-sm-6">
                                                    </div>
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="pencemaran_air">
                                                <h4 class="page-tab">Pengendalian Pencemaran Air</h4>

                                                <label>a. Sumber Air dan Penggunaan</label>
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <label>Air Tanah</label>
                                                            <input type="text" name="ambil_air_tanah" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_tanah;?>"/>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label>Perizinan</label>
                                                            <select class="form-control input-sm" name="ambil_air_tanah_izin">
                                                                <option value="1" <?php echo (($pencemaran_air->ambil_air_tanah_izin== true) ? 'selected' : ''); ?>>Ada</option>
                                                                <option value="0" <?php echo (($pencemaran_air->ambil_air_tanah_izin== false) ? 'selected' : ''); ?>>Tidak Ada</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Air Permukaan</label>
                                                    <input type="text" name="ambil_air_permukaan" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_permukaan;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>PDAM</label>
                                                    <input type="text" name="ambil_air_pdam" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_pdam;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Lain-lain</label>
                                                    <input type="text" name="ambil_air_lain" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ambil_air_lain;?>"/>
                                                </div>


                                                <div class="form-group ">
                                                    <label>b. Penggunaan Air</label>
                                                    <input type="text" name="penggunaan_air" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->penggunaan_air;?>"/>
                                                </div>
                                                
                                                <!-- <div class="form-group ">
                                                    <label>c. Sumber Air Limbah</label>
                                                    <input type="text" name="limb_sumber" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->limb_sumber;?>"/>
                                                </div>

                                                <div class="form-group ">
                                                    <label>d. Badan Air Penerima</label>
                                                    <input type="text" name="bdn_terima" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->bdn_terima;?>"/>
                                                </div> -->

                                                <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>c. Sumber Air Limbah</label>
                                                    <!-- <select name="limb_sumber" id="" class="form-control input-sm"> -->
                                                    <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                                    <!-- <option value=''></option> -->
                                                        <?php
                                                            $arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);
                                                            foreach ($limb_sumber as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                for ($i=0; $i < count($arr_limb_sumber) ; $i++) { 
                                                                
                                                                    if ($arr_limb_sumber[$i]==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>d. Badan Air Penerima</label>
                                                    <select name="bdn_terima" id="" class="form-control input-sm">
                                                    <option value=''></option>
                                                        <?php
                                                            foreach ($bdn_terima as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                    if ($pencemaran_air->bdn_terima==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>

                                                <div class="form-group ">
                                                    <label>e. Sarana Pengolahan Air Limbah</label>
                                                    <select class="form-control input-sm" name="sarana_olah_limbah">
                                                        <option value="1" <?php echo (($pencemaran_air->sarana_olah_limbah== true) ? 'selected' : ''); ?> >Ada</option>
                                                        <option value="0" <?php echo (($pencemaran_air->sarana_olah_limbah== false) ? 'selected' : ''); ?> >Tidak Ada</option>
                                                    </select>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Jenis Sarana</label>
                                                    <input type="text" name="sarana_jenis" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->sarana_jenis;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <label>Kapasitas</label>
                                                    <input type="text" name="sarana_kapasitas" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->sarana_kapasitas;?>"/>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <label>Koordinat Outlet S</label>
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_s?>" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_s?>" name="koord_outlet_jam_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon">'</span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_s?>" name="koord_outlet_menit_s" id="derajat_s" class="form-control input-sm has-feedback" />
                                                                    <span class="input-group-addon">"</span>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label>Koordinat Outlet E</label>
                                                            <div class="row">
                                                                <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_e?>" name="koord_outlet_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding: 0; margin:0;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_e?>" name="koord_outlet_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon">'</span>
                                                                </div>
                                                                </div>
                                                                <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                                <div class="input-group input-group-sm">
                                                                    <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_e?>" name="koord_outlet_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                    <span class="input-group-addon">"</span>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row"> <!-- navigation -->
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                    </div><!-- /.col-sm-6 -->
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                    </div><!-- /.col-sm-6 -->
                                                </div>
                                            </div>
                                            
                                            <div class="tab-pane" id="pencemaran_udara">
                                                <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                                
                                                <div class="form-group ">
                                                    <label>a. Data Kualitas Udara Ambien</label>
                                                    <table class="table table-th-block">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th class="text-center">Upwind</th>
                                                                <th class="text-center">Site</th>
                                                                <th class="text-center">Downwind</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><label>Pengujian Kualitas</label></td>
                                                                <td><select class="form-control input-sm" name="uji_kualitas1"><option value="1" <?php echo (($uji_ambien[0]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[0]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                                <td><select class="form-control input-sm" name="uji_kualitas2"><option value="1" <?php echo (($uji_ambien[1]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[1]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                                <td><select class="form-control input-sm" name="uji_kualitas3"><option value="1" <?php echo (($uji_ambien[2]->uji_kualitas == true) ? 'selected' : ''); ?> >Ada</option><option value="0" <?php echo (($uji_ambien[2]->uji_kualitas == false) ? 'selected' : ''); ?> >Tidak Ada</option></select></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                                <td><select class="form-control input-sm" name="period1"><option value="Rutin" <?php echo (($uji_ambien[0]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[0]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                                <td><select class="form-control input-sm" name="period2"><option value="Rutin" <?php echo (($uji_ambien[1]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[1]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                                <td><select class="form-control input-sm" name="period3"><option value="Rutin" <?php echo (($uji_ambien[2]->period == 'Rutin') ? 'selected' : ''); ?> >Rutin</option><option value="Tidak Rutin" <?php echo (($uji_ambien[2]->period == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option></select></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Laboratorium Penguji</label></td>
                                                                <?php
                                                                    for($i=0; $i<count($uji_ambien); $i++){
                                                                        echo "<td><select name='lab".($i+1)."' class='form-control input-sm '>";
                                                                            foreach ($lab as $l) {
                                                                                echo "<option value='$l->nama_lab' ".(($uji_ambien[$i]->lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                                            }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Pemenuhan BM</label></td>
                                                                <td><select class="form-control input-sm" name="bm_pemenuhan1"><option value="1" <?php echo (($uji_ambien[0]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[0]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                                <td><select class="form-control input-sm" name="bm_pemenuhan2"><option value="1" <?php echo (($uji_ambien[1]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[1]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                                <td><select class="form-control input-sm" name="bm_pemenuhan3"><option value="1" <?php echo (($uji_ambien[2]->bm_pemenuhan == true) ? 'selected' : ''); ?> >Ya</option><option value="0" <?php echo (($uji_ambien[2]->bm_pemenuhan == false) ? 'selected' : ''); ?> >Tidak</option></select></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                                <td><input type="text" name="bm_param1" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[0]->bm_param;?>"/></td>
                                                                <td><input type="text" name="bm_param2" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[1]->bm_param;?>"/></td>
                                                                <td><input type="text" name="bm_param3" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[2]->bm_param;?>"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Periode Tidak Memenuhi BM</label></td>
                                                                <td><input type="text" name="bm_period1" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[0]->bm_period;?>"/></td>
                                                                <td><input type="text" name="bm_period2" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[1]->bm_period;?>"/></td>
                                                                <td><input type="text" name="bm_period3" class="form-control input-sm has-feedback " value="<?php echo $uji_ambien[2]->bm_period;?>"/></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                        
                                                <div class="form-group ">
                                                    <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                    <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                        <option value="Rutin" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Rutin') ? 'selected' : ''); ?> >Rutin</option>
                                                        <option value="Tidak Rutin" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Rutin') ? 'selected' : ''); ?> >Tidak Rutin</option>
                                                        <option value="Tidak Melaporkan" <?php echo (($pencemaran_udara->pelaporan_ua_ue == 'Tidak Melaporkan') ? 'selected' : ''); ?> >Tidak Melaporkan</option>
                                                    </select>
                                                </div>

                                                <div class="form-group ">
                                                    <label>c. Lain-lain</label>
                                                    <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_udara->lain_lain;?>"/>
                                                </div>

                                                <div class="row"> <!-- navigation -->
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                    </div><!-- /.col-sm-6 -->
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                    </div><!-- /.col-sm-6 -->
                                                </div>
                                            </div>
                                            
                                            <div class="tab-pane" id="pencemaran_b3">
                                                <h4 class="page-tab">Pengendalian Limbah Padat</h4>
                                                
                                            <?php 
                                                $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                                $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                                $new = array();
                                                for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                                    $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                                }
                                            ?>

                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">a. Jenis</th>
                                                        <th class="text-center">b. Jumlah (ton/hari)</th>
                                                        <th class="text-center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($new as $key => $value) { ?>
                                                    <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                        <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                                                        <td class="col-lg-5"><input type="text" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                                                        <td class="col-lg-2">
                                                            <div class=" text-right">
                                                                <div class="btn-group" style="padding-right: 17px;">
                                                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                                <div class="form-group ">
                                                    <label>c. Sarana Pengelolaan</label>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label>1. Tong / Bak Sampah</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="padat_sarana_tong" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_sarana_tong;?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <label>2. TPS</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="padat_sarana_tps" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_sarana_tps;?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label>d. Pengelolaan</label>
                                                    <!-- <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                    <option value="Dibakar" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dibakar') ? 'selected' : ''); ?> >Dibakar</option>
                                                    <option value="Diangkut oleh Pihak Ketiga" <?php //echo (($pencemaran_pb3->padat_kelola == 'Diangkut oleh Pihak Ketiga') ? 'selected' : ''); ?> >Diangkut oleh Pihak Ketiga</option>
                                                    <option value="Diangkut oleh Dinas Pertasih" <?php //echo (($pencemaran_pb3->padat_kelola == 'Diangkut oleh Dinas Pertasih') ? 'selected' : ''); ?> >Diangkut oleh Dinas Pertasih</option>
                                                    <option value="Dikelola oleh Masyarakat Sekitar" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dikelola oleh Masyarakat Sekitar') ? 'selected' : ''); ?> >Dikelola oleh Masyarakat Sekitar</option>
                                                    <option value="Dijual" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dijual') ? 'selected' : ''); ?> >Dijual</option>
                                                    <option value="other" <?php //echo (($pencemaran_pb3->padat_kelola == 'Dibakar') || ($pencemaran_pb3->padat_kelola == 'Diangkut oleh Pihak Ketiga') || 
                                                                                       ////($pencemaran_pb3->padat_kelola == 'Diangkut oleh Dinas Pertasih') || ($pencemaran_pb3->padat_kelola == 'Dijual') || 
                                                                                      //// ($pencemaran_pb3->padat_kelola == 'Dikelola oleh Masyarakat Sekitar') ? '' : 'selected'); ?> >lainnya</option>
                                                </select>
                                                <input type="text" name="padat_kelola2" id="pe2" class="form-control input-sm has-feedback" style="display:none" value="<?php //echo $pencemaran_pb3->padat_kelola;?>"/> -->
                                                <select class="form-control input-sm" id="pe" name="padat_kelola1">
                                                    <?php 
                                                        $padat_kelola = $pencemaran_pb3->padat_kelola;
                                                        echo $padat_kelola;
                                                            if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola2' id='pe2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                        } else {
                                                            $temp = "Lainnya";
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola2' id='pe2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
                                                        }
                                                ?>
                                                </div>

                                                <div class="form-group ">
                                                    <label>e. Lain-lain</label>
                                                    <input type="text" name="lain_lain_pb3" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->lain_lain;?>"/>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                    </div><!-- /.col-sm-6 -->
                                                    <div class="col-sm-6 text-right">
                                                        <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                    </div><!-- /.col-sm-6 -->
                                                </div>
                                            </div>
                                            
                                            <div class="tab-pane" id="lain_lain">
                                                <h4 class="page-tab">Lain - Lain</h4>
                                                <div class="form-group ">
                                                    <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"><?php echo $bap->catatan;?></textarea>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                    </div><!-- /.col-sm-6 -->
                                                    <div class="col-sm-6 text-right">
                                                        <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_rph'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                                    </div><!-- /.col-sm-6 -->
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                            
                            </div>
                            <!-- /.row -->
                        </div>

                        <div class="modal-footer">
                        </div>
                    </div>
                </div>

                    
                  <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                    <br/><br/> -->

                </div><!-- /.the-box .default -->
                <!-- END DATA TABLE -->
    			
            </div><!-- /.container-fluid -->				
        </div><!-- /.page-content -->
    </div><!-- /.wrapper -->
    <!-- END PAGE CONTENT -->



    <!-- BEGIN BACK TO TOP BUTTON -->
    <div id="back-top">
        <a href="#top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <!-- END BACK TO TOP -->

    <script type="text/javascript">
        $(document).ready(function () {
            var site = "<?php echo site_url(); ?>";
            $('#nama_industri').autocomplete({
                minChars: 2,
                type: 'POST',
                noCache: true,
                serviceUrl: site + 'backend/industri/search_industri_bap/18',
                onSearchStart: function (query) {
                    $('#id_industri').val(null);              
                },
                onSelect: function (suggestion) {
                    $('#id_industri').val(suggestion.data).change();
                    var id_last_input = $('#id_industri').val();
                    $('#id_last_input').val(id_last_input);
                    console.log($('#id_last_input').val());
                }
            });

            $("select[name=id_industri]").change(function(){
                $(this).get_detail_industri(this, '#container2');
            });
        });
    </script>