 <!-- Document   : bap_bpo.php  -->
 <!-- Created on : October x, 2014 xx:xx  -->
 <!-- Author     : huda.jtk09@gmail.com  -->
 <!-- Description: Controller for BAP Bahan Perusak Ozon (BPO)  -->

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class bap_bpo extends CI_Controller {

    function __construct() {
        parent::__construct();

        # load model
        // $this->load->model('', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# load view
        $this->load->view('include/header');
        $this->load->view('include/footer');
        $this->load->view('backend/v_bap_bpo');
    }
	
	public function get() {		
		
	}
	

	public function insert() {
	
	}

	public function edit() {
		
	}
	
	public function delete() {
		
	}

}
