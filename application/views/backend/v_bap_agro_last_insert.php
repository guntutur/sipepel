    <!-- 
    ===========================================================
    BEGIN PAGE
    ===========================================================
    -->
    <div class="wrapper">

        <?php $this->load->view('include/top_nav'); ?>
        <?php $this->load->view('include/sidebar'); ?>

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- Begin page heading -->
                <h1 class="page-heading">BAP Agro&nbsp;&nbsp;<small>Berita Acara Pembinaan/Pengawasan Agro</small></h1>
                <!-- End page heading -->

                <!-- Begin breadcrumb -->
                <ol class="breadcrumb default square rsaquo sm">
                    <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                    <li>Manajemen BAP</li>
                    <li class="active">BAP Agro</li>
                </ol>
                <!-- End breadcrumb -->

                <?php echo $this->session->flashdata('error'); ?>

                <!-- BEGIN DATA TABLE -->
                <div class="the-box no-padding">

                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active" style=""><a href="#last_input" role="tab" data-toggle="tab"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Tambah BAP</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        
                        <div class="tab-pane active" id="last_input" style="">
                            <div class="row" style="margin-top: 10px;">       
                            <div class="col-xs-3" id="vertical_tab"> <!-- required for floating -->
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#data_umum" data-toggle="tab"><i class="fa fa-file-text icon-sidebar"></i>Data Umum</a></li>
                                <li><a href="#pencemaran_air" data-toggle="tab"><i class="fa fa-tint icon-sidebar"></i>Pencemaran Air</a></li>
                                <li><a href="#pencemaran_udara" data-toggle="tab"><i class="fa fa-cloud icon-sidebar"></i>Pencemaran Udara</a></li>
                                <li><a href="#pencemaran_b3" data-toggle="tab"><i class="fa fa-flask icon-sidebar"></i>Limbah Padat dan B3</a></li>
                                <li><a href="#lain_lain" data-toggle="tab"><i class="fa fa-comment-o icon-sidebar"></i>Lain-lain</a></li>
                              </ul>
                            </div>
                            <div class="col-xs-9">
                            <!-- Tab panes -->
                                <form role="form" id="add_form" action="<?php echo base_url('backend/bap_agro/insert_from_form') ?>" method="post">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="data_umum">
                                            <h4 class="page-tab"><i class="fa fa-newspaper-o icon-sidebar"></i>Data Umum Usaha/Kegiatan</h4>
                                            <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Tanggal Pengawasan</label>
                                                <input type="text" name="bap_tgl" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo $bap->bap_tgl;?>">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label>Pukul</label>
                                                <div class="bfh-timepicker">
                                                    <input id="timepick2" type="text" name="bap_jam" value="<?php echo $bap->bap_jam;?>" class="form-control input-sm bfh-timepicker">
                                                </div>
                                            </div>
                                            </div>
                                            <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label>Petugas Pengawas</label>
                                                    <select class="form-control input-sm" name="id_pegawai">
                                                        <option value=""></option>
                                                        <?php 
                                                        foreach ($pegawai as $p) {
                                                            echo "<option value='$p->id_pegawai' ".(($bap->id_pegawai== $p->id_pegawai) ? 'selected' : '').">$p->nama_pegawai</option>";
                                                        }?>
                                                    </select>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label>Beserta Anggota Pengawas:</label>
                                                    <select class="form-control input-sm multiselect" name="petugas_pengawas[]" multiple="multiple" style="border: 0!important;">
                                                    <?php 
                                                        foreach ($pegawai as $p) {
                                                            echo "<option value='$p->id_pegawai' ";
                                                            for ($i=0; $i < count($petugas_bap); $i++) { 
                                                                if($petugas_bap[$i]->id_pegawai==$p->id_pegawai) {
                                                                    echo "selected";
                                                                }
                                                            }
                                                            echo ">$p->nama_pegawai</option>";
                                                        }?>
                                                </select>
                                            </div>
                                            </div>
                                            <div class="form-group">
                                                <h4>Lokasi Pengawasan / Pembinaan</h4>    
                                                <div class="form-group status">                        
                                                <label>Nama Usaha/Kegiatan</label>
                                                    <select name="id_industri" id="id_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                                        <option value="Empty">&nbsp;</option>
                                                        <?php
                                                            foreach ($industri as $value) {
                                                                echo '<option value="'.$value->id_industri.'"';
                                                                    if($bap->id_industri==$value->id_industri){
                                                                        echo 'selected';
                                                                    }
                                                                echo '>'.$value->nama_industri.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                    <!-- <input type="text" title="Nama Usaha/Kegiatan" value="<?php echo $bap->nama_industri?>" class="form-control input-sm" id="nama_industri" name="nama_industri">
                                                    <input type="hidden" id="id_industri" name="id_industri" value="<?php echo $bap->id_industri?>"/> -->
                                                </div>
                                                <div id="container2" style="display:none"></div>
                                            </div>
                                            <div class="form-group ">
                                            <label>Jenis Perusahaan</label>
                                                <select class="form-control input-sm" id="peru_stat" name="peru_stat">
                                                    <option value="">-- Pilih Salah Satu --</option>>
                                                    <?php 
                                                        $jp = $bap_agro->status_perusahaan;
                                                        echo $jp;
                                                            if (($jp == "PMA") || ($jp == "PMDN")) {
                                                            foreach ($jenis_perusahaan as $p) {
                                                                echo "<option value='$p' ".(($jp == $p) ? 'selected' : '').">$p</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='peru_stat_lain' id='peru_stat_lain' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                        } else {
                                                            $temp = "other";
                                                            foreach ($jenis_perusahaan as $p) {
                                                                echo "<option value='$p' ".(($temp == $p) ? 'selected' : '').">$p</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='peru_stat_lain' id='peru_stat_lain' value='$jp' class='form-control input-sm has-feedback'/>";
                                                        }
                                                ?>
                                                </div>
                                            <div class="form-group ">
                                                <label>Kapasitas Produksi (ton/bulan)</label>
                                                <input type="text" name="kapasitas_produksi" class="form-control input-sm has-feedback" value="<?php echo $bap_agro->kapasitas_produksi?>" />
                                            </div>
                                            <div class="form-group ">
                                                <label>Jenis Produk</label>
                                                <input type="text" name="jenis_produk" class="form-control input-sm has-feedback" value="<?php echo $bap_agro->jenis_produk?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Pemakaian Pupuk</label>
                                                <select name="pakai_pupuk" class="form-control input-sm has-feedback">
                                                    <option value="Organik" <?php echo (($bap_agro->pakai_pupuk== 'Organik') ? 'selected' : ''); ?>>Organik</option>
                                                    <option value="Anorganik" <?php echo (($bap_agro->pakai_pupuk== 'Anorganik') ? 'selected' : ''); ?>>Anorganik</option>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>Jumlah Karyawan</label>
                                                <input type="text" name="jml_karyawan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap_agro->jumlah_karyawan?>"/>
                                            </div>
                                            <div class="table-responsive">
                                            <table class="table table-striped table-hover" id="datatable-example">
                                                <thead class="the-box dark full">
                                                    <tr>
                                            <!-- <div class="form-group "> -->
                                                        <th>Dokumen Lingkungan</th>
                                                        <th>Izin Lingkungan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                <!-- <div class="radio"> -->
                                                        <label>
                                                            <input class="dl" type="radio" name="dl" id="dl1" value="ada" <?php echo (($bap->dok_lingk==1) ? 'checked' : '')?>>Ada
                                                        </label>
                                                        <label>
                                                            <input class="dl" type="radio" name="dl" id="dl2" value="tidak ada" <?php echo (($bap->dok_lingk==0) ? 'checked' : '')?>>Tidak Ada
                                                        </label>
                                                <!-- </div> -->
                                                    </td>
                                                        <!-- </div> -->
                                                        <!-- <div class="form-group "> -->
                                                    <td>
                                                            <!-- <div class="radio"> -->
                                                        <label>
                                                            <input type="radio" name="il" id="il1" value="ada" <?php echo (($bap->izin_lingk==1) ? 'checked' : '')?>>Ada
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="il" id="il2" value="tidak ada" <?php echo (($bap->izin_lingk==0) ? 'checked' : '')?>>Tidak Ada
                                                        </label>
                                                            <!-- </div> -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-group " id="jdl" name="jdl" style="display:none">
                                                                <!-- <div class="form-group "> -->
                                                            <label>Jenis Dokumen</label>
                                                            <select class="form-control input-sm" name="jdl_name" id="jdl_name">
                                                                <option value="">-- Pilih salah satu --</option>
                                                                <?php 
                                                                    foreach ($jenis_dok_lingkungan as $j) {
                                                                        echo "<option value='$j->ket' ".(($bap->dok_lingk_jenis == $j->ket) ? 'selected' : '').">$j->ket</option>";
                                                                }?>
                                                            </select>
                                                                <!-- </div> -->
                                                                <!-- <div class="form-group "> -->
                                                            <label>Tahun</label>
                                                            <input type="text" name="dlt" id="dlt" class="form-control input-sm has-feedback" value="<?php echo $bap->dok_lingk_tahun; ?>"/>
                                                                <!-- </div> -->
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group " id="il_tahun" style="display:none">
                                                        <label>Tahun</label>
                                                        <input type="text" name="il_tahun" id="ilt" class="form-control input-sm has-feedback" value="<?php echo $bap->izin_lingk_tahun; ?>"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                    </div>
                                    <fieldset>
                                        <legend>Penanggung Jawab Usaha / Kegiatan</legend>
                                        <div class="form-group ">
                                                <label>Nama</label>
                                                <input type="text" name="p2_nama" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_nama; ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Telepon</label>
                                                <input type="text" name="p2_telp" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_telp; ?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label>Jabatan</label>
                                                <input type="text" name="p2_jabatan" id="" class="form-control input-sm has-feedback" value="<?php echo $bap->p2_jabatan; ?>"/>
                                            </div> 
                                    </fieldset>

                                        <div class="row"> <!-- navigation -->
                                            <div class="col-sm-6 text-right">
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="tab-pane" id="pencemaran_air">          
                                            <h4 class="page-tab">Pengendalian Pencemaran Air</h4>
                                            <div class="form_group">
                                            <label>a. Sumber Air dan Penggunaan</label>

                                            <div class="form-group">
                                        <div class="form-group col-lg-6">
                                            <label>Air Tanah</label>
                                            <input type="text" name="air_tanah" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_tanah; ?>"/>
                                            <div class="radio">
                                            <label>Perizinan    : </label>
                                                    <label>
                                                        <input type="radio" name="izin_air_tanah" value="ada" <?php echo (($pencemaran_air->ambil_air_tanah_izin==1) ? 'checked' : '')?>>Ada
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="izin_air_tanah" value="tidak ada" <?php echo (($pencemaran_air->ambil_air_tanah_izin==0) ? 'checked' : '')?>>Tidak Ada
                                                    </label>
                                                </div>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Air Permukaan</label>
                                            <input type="text" name="air_permukaan" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_permukaan; ?>"/>
                                            <div class="radio">
                                            <label>Perizinan    : </label>
                                                    <label>
                                                        <input type="radio" name="izin_air_permukaan" value="ada" <?php echo (($pencemaran_air->ambil_air_permukaan_izin==1) ? 'checked' : '')?>>Ada
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="izin_air_permukaan" value="tidak ada" <?php echo (($pencemaran_air->ambil_air_permukaan_izin==0) ? 'checked' : '')?>>Tidak Ada
                                                    </label>
                                                </div>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Air PDAM</label>
                                            <input type="text" name="air_pdam" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_pdam; ?>"/>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label>Air Lain-lain</label>
                                            <input type="text" name="air_lain" id="" class="form-control input-sm has-feedback" placeholder="m3/hari" value="<?php echo $pencemaran_air->ambil_air_lain; ?>"/>
                                        </div>
                                        </div>
                                            </div>                                            
                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>b. Sumber Air Limbah</label>
                                                    <!-- <select name="limb_sumber" id="" class="form-control input-sm"> -->
                                                    <select name="limb_sumber[]" id="limb_sumber" class="form-control input-sm multiselect" multiple="multiple" style="border: 0!important">
                                                    <!-- <option value=''></option> -->
                                                        <?php
                                                            $arr_limb_sumber = explode(', ', $pencemaran_air->limb_sumber);
                                                            foreach ($limb_sumber as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                for ($i=0; $i < count($arr_limb_sumber) ; $i++) { 
                                                                
                                                                    if ($arr_limb_sumber[$i]==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>c. Badan Air Penerima</label>
                                                    <select name="bdn_terima" id="" class="form-control input-sm">
                                                    <option value=''></option>
                                                        <?php
                                                            foreach ($bdn_terima as $l) {
                                                                echo "<option value='$l->ket' ";
                                                                    if ($pencemaran_air->bdn_terima==$l->ket){
                                                                        echo 'selected';
                                                                    }
                                                                echo ">$l->ket</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row"> -->
                                            <div class="form-group">
                                                <label>c. IPAL</label>
                                                <select class="form-control input-sm" name="ipal" id="ipal">
                                                    <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->ipal), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                    </select>
                                            </div>
                                            <div class="form-group" id="detail_ipal" style="dsiplay:none">
                                                <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Sistem IPAL</label>
                                                    <select class="form-control input-sm" id="si" name="ipal_sistem">
                                                        <?php         
                                                            $si = $pencemaran_air->ipal_sistem;
                                                                if (($si === "F") || ($si === "F-B") || ($si === "F-B-K") || ($si === "F-K") || ($si === "F-K-B")) {
                                                                foreach ($ipal as $p) {
                                                                    echo "<option value='$p->ket' ".(($si == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                                }
                                                                echo "<option value='other'>lainnya</option>";
                                                        echo "</select>";
                                                        echo "<input type='text' name='ipal_sistem2' id='si2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                            } else {
                                                                
                                                                foreach ($ipal as $p) {
                                                                    echo "<option value='$p->ket' >$p->ket</option>";
                                                                }
                                                                echo "<option value='other' selected>lainnya</option>";
                                                        echo "</select>";
                                                        echo "<input type='text' name='ipal_sistem2' id='si2' value='$si' class='form-control input-sm has-feedback'/>";
                                                            }
                                                    ?>
                                                </div>
                                                <!-- </div> -->
                                                <div class="col-lg-4">
                                                    <label>Unit IPAL</label>
                                                    <select name="ipal_unit[]" id="ipal_unit" class="form-control input-sm has-feedback multiselect" multiple="multiple">
                                                    <?php
                                                        $arr_ipal_unit = explode(', ', $pencemaran_air->ipal_unit);
                                                        foreach ($ipal_unit as $iu) {
                                                            echo "<option value='$iu->ket' " ;
                                                            for ($i=0; $i < count($arr_ipal_unit); $i++) { 
                                                            
                                                                if ($arr_ipal_unit[$i]==$iu->ket){
                                                                    echo "selected='true'";
                                                                }
                                                            }
                                                            echo ">$iu->ket</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Kapasitas IPAL</label>
                                                    <input type="text" name="ipal_kapasitas" id="ipal_kapasitas" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->ipal_kapasitas; ?>"/>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>d. Perizinan</label>
                                                <select class="form-control input-sm" name="izin" id="izin">
                                                    <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->izin), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group" name="detail_izin" id="detail_izin">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label>Nomor</label>
                                                        <input type="text" name="izin_no" id="izin_no" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->izin_no?>"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Tanggal</label>
                                                        <input type="text" name="izin_tgl" id="izin_tgl" class="form-control input-sm datepicker" value="<?php echo $pencemaran_air->izin_tgl?>" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>Debit Izin (m3/hari)</label>
                                                <input type="text" name="izin_debit" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->izin_debit?>"/>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet S</label>
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_s?>" name="koord_outlet_derajat_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_s?>" name="koord_outlet_jam_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">'</span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_s?>" name="koord_outlet_menit_s" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">"</span>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Koordinat Outlet E</label>
                                                        <div class="row">
                                                            <div class="col-lg-4" style="padding-right:0px; margin-right:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_derajat_e?>" name="koord_outlet_derajat_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon"><?php echo '&deg;' ?></span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding: 0; margin:0;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_jam_e?>" name="koord_outlet_jam_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">'</span>
                                                            </div>
                                                            </div>
                                                            <div class="col-lg-4" style="padding-left:0px; margin-left:0px;">
                                                            <div class="input-group input-group-sm">
                                                                <input type="text" value="<?php echo $pencemaran_air->koord_outlet_menit_e?>" name="koord_outlet_menit_e" id="derajat_s" class="form-control input-sm has-feedback"  />
                                                                <span class="input-group-addon">"</span>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label>e. Alat Ukur</label>
                                                <select class="form-control input-sm" name="au" id="au">
                                                    <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->au), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group" id="detail_au" name="detail_au">
                                            <div class="form-group ">
                                                <label>Jenis</label>
                                                <select class="form-control input-sm" id="je" name="au_jenis">
                                                        <?php         
                                                            $auj = $pencemaran_air->au_jenis;
                                                            echo $auj;
                                                                if (($auj === "Kumulatif") || ($auj === "V-notch") || ($auj === "Digital")) {
                                                                foreach ($jenis_alat_ukur as $p) {
                                                                    echo "<option value='$p' ".(($auj == $p) ? 'selected' : '').">$p</option>";
                                                                }
                                                                echo "<option value='other'>lainnya</option>";
                                                        echo "</select>";
                                                        echo "<input type='text' name='au_jenis2' id='je2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                            } else {
                                                                $temp = "other";
                                                                foreach ($jenis_alat_ukur as $p) {
                                                                    echo "<option value='$p' ".(($temp == $p) ? 'selected' : '').">$p</option>";
                                                                }
                                                                echo "<option value='other'>lainnya</option>";
                                                        echo "</select>";
                                                        echo "<input type='text' name='au_jenis2' id='je2' value='$auj' class='form-control input-sm has-feedback'/>";
                                                            }
                                                    ?>
                                            </div>
                                            <div class="form-group ">
                                                <label>Ukuran (inchi)</label>
                                                <input type="text" name="au_ukuran" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->au_ukuran?>"/>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Kondisi (apabila ada)</label>
                                                <select class="form-control input-sm" name="au_kondisi">
                                                    <option value="Berfungsi" <?php echo (($pencemaran_air->au_kondisi=="Berfungsi") ? 'selected' : '')?>>Berfungsi</option>
                                                    <option value="Tidak Berfungsi" <?php echo (($pencemaran_air->au_kondisi=="Tidak Berfungsi") ? 'selected' : '')?>>Tidak Berfungsi</option>
                                                </select>
                                            </div>    
                                            </div>    
                                            <div class="form-group ">
                                                <label>f. Catatan debit harian</label>
                                                <select class="form-control input-sm" name="cat_deb_hari">
                                                <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->cat_deb_hari), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>g. Daur Ulang</label>
                                                <select class="form-control input-sm" name="daur_ulang">
                                                <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->daur_ulang), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Debit (m3/hari)</label>
                                                <input type="text" name="daur_ulang_debit" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->daur_ulang_debit?>" />
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>h. Pengujian Kualitas Air Limbah</label>
                                                <select class="form-control input-sm" name="uji_limbah" id="uji_limbah">
                                                <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->uji_limbah), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            <div class="form-group" name="detail_uji" id="detail_uji">
                                            <div class="form-group ">
                                                <label>Periode Pengujian</label>
                                                <select class="form-control input-sm" name="uji_period" id="pp">
                                                <?php         
                                                            $pu = $pencemaran_air->uji_period;
                                                            echo $pu;
                                                                if (($pu == "Setiap Bulan")) {
                                                                foreach ($periode_uji as $p) {
                                                                    echo "<option value='$p' ".(($pu == $p) ? 'selected' : '').">$p</option>";
                                                                }
                                                        echo "</select>";
                                                        echo "<input type='text' name='uji_period2' id='pp2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                            } else {
                                                                $temp = "other";
                                                                foreach ($periode_uji as $p) {
                                                                    echo "<option value='$p' ".(($temp == $p) ? 'selected' : '').">$p</option>";
                                                                }
                                                        echo "</select>";
                                                        echo "<input type='text' name='uji_period2' id='pp2' value='$pu' class='form-control input-sm has-feedback'/>";
                                                            }
                                                    ?>
                                            </div>
                                            <div class="form-group ">
                                                <label>Laboratorium Pengujian</label>
                                                <select name="uji_lab" class="form-control input-sm">
                                                    <?php
                                                        foreach ($lab as $l) {
                                                            echo "<option value='$l->nama_lab' ".(($pencemaran_air->uji_lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            <div class="form-group ">
                                                <label>Hasil Pengujian</label>
                                                <select class="form-control input-sm" name="uji_hasil" id="uji_hasil">
                                                <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".penuhConv($f)."' " ;
                                                            if (strcasecmp($pencemaran_air->uji_hasil, penuhConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".penuhConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>Apabila tidak memenuhi baku mutu, terjadi pada bulan</label>
                                                <input type="text" name="uji_tidak_bulan" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->uji_tidak_bulan?>" />
                                            </div>    
                                            <div class="form-group ">
                                                <label>Parameter yang tidak memenuhi baku mutu</label>
                                                <input type="text" name="uji_tidak_param" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->uji_tidak_param?>"/>
                                            </div>
                                            <?php
                                                    $lapor = $bool;
                                                    $lapor[] = '2';
                                                ?>
                                            <div class="form-group ">
                                                <label>Pelaporan</label>
                                                <select class="form-control input-sm" name="uji_lapor">
                                                <?php 
                                                        for($d=0; $d<3; $d++){
                                                            echo "<option value='".rutinConv($lapor[$d])."'";
                                                                if (strcasecmp($pencemaran_air->uji_lapor, rutinConv($lapor[$d])) == 0){
                                                                    echo "selected";
                                                                }
                                                            echo ">".rutinConv($lapor[$d])."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            
                                            <div class="form-group ">
                                                <label>i. Bahan Kimia Pembantu IPAL</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Nama Bahan Kimia</th>
                                                            <th class="text-center">Jumlah Pemakaian (ton/hari)</th>
                                                            <th class="text-center"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                        $x=1;
                                                        foreach($bahan_kimia_ipal as $bki){ ?>
                                                            <tr data-duplicate="kimia-ipal" data-duplicate-min="1">
                                                                <td class="col-lg-5"><input type="text" name="bkpi_nama[]" value="<?php echo $bki->nama?>" class="form-control input-sm has-feedback" /></td></td>
                                                                <td class="col-lg-5"><input type="text" name="bkpi_jml[]" value="<?php echo $bki->jml_pemakaian?>" class="form-control input-sm has-feedback" /></td>
                                                                <td class="col-lg-2">
                                                                    <div class=" text-right">
                                                                        <div class="btn-group" style="padding-right: 17px;">
                                                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="kimia-ipal"><i class="fa fa-plus-circle"></i></button>
                                                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="kimia-ipal"><i class="fa fa-times-circle"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <!-- echo "<input type='hidden' name='id_bahan_kimia_ipal$x' value='".$bki->id_bahan_kimia_ipal."'>";
                                                            echo "<tr><td><input type='text' name='bkpi_nama$x' class='form-control input-sm has-feedback ' value='".$bki->nama."'/></td><td><input type='text' name='bkpi_jml$x' class='form-control input-sm has-feedback ' value='".$bki->jml_pemakaian."' /></td></tr>"; -->

                                                            <?php $x++;
                                                        }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group ">
                                                <label>j. Kebocoran/Rembesan/Tumpahan/Bypass</label>
                                                <select class="form-control input-sm" name="bocor" id="bocor">
                                                <?php
                                                        foreach ($bool as $f) {
                                                            echo "<option value='".adaConv($f)."' " ;
                                                            if (strcasecmp(adaConv($pencemaran_air->bocor), adaConv($f)) == 0){
                                                                echo "selected='true'";
                                                            }
                                                            echo ">".adaConv($f)."</option>";
                                                        $n++;
                                                        }
                                                    ?>
                                                </select>
                                            </div>    
                                            <div class="form-group " id="lok" >
                                                <label>Lokasi</label>
                                                <input type="text" name="bocor_lokasi" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->bocor_lokasi?>" />
                                            </div>
                                            
                                            <div class="form-group ">
                                                <label>k. Lain-lain</label>
                                                <input type="text" name="lain_lain_pa" id="" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_air->lain_lain?>"/>
                                            </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_udara">
                                            <h4 class="page-tab">Pengendalian Pencemaran Udara</h4>
                                            <div class="form-group ">
                                            <div class="form-group">
                                                <label>a. Data Emisi Sumber Tidak Bergerak</label>
                                                </div>
                                                <div class="form-group">
                                                <div class="col-lg-6">
                                                    <label>Sumber emisi     :</label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <select class="form-control input-sm" name="emboil" id="emboil">
                                                    <?php         
                                                            $se = $pencemaran_udara->sumber_emisi;
                                                            // echo $se;
                                                                if (($se == "Boiler")) {
                                                                foreach ($emboil as $p) {
                                                                    echo "<option value='$p' ".(($se == $p) ? 'selected' : '').">$p</option>";
                                                                }
                                                        echo "</select>";
                                                        echo "<input type='text' name='emisi_lain' id='emisi_lain' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                            } else {
                                                                $temp = "other";
                                                                foreach ($emboil as $p) {
                                                                    echo "<option value='$p' ".(($temp == $p) ? 'selected' : '').">$p</option>";
                                                                }
                                                        echo "</select>";
                                                        echo "<input type='text' name='emisi_lain' id='emisi_lain' value='$se' class='form-control input-sm has-feedback'/>";
                                                            }
                                                    ?>
                                                </div>
                                                </div>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th>Data Boiler</th>
                                                            <th class="text-center">Steam</th>
                                                            <th class="text-center">Steam</th>
                                                            <th class="text-center">Oli</th>
                                                            <th class="text-center">Lain-lain</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><label>Jumlah</label></td>
                                                            <?php
                                                                $x=1;
                                                                foreach($emisi_boiler as $db){
                                                                    echo "<input type='hidden' name='id_emisi_boiler$x' value='".$db->id_emisi_boiler."'>";
                                                                    echo "<input type='hidden' name='id_jenis_emisi_boiler$x' value='".$db->id_jenis_emisi_boiler."'>";
                                                                    echo "<td><input type='text' name='jml_boiler$x' class='form-control input-sm has-feedback ' value='".$db->jml_boiler."'/></td>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Merk</label></td>
                                                            <?php
                                                                $x=1;
                                                                foreach($emisi_boiler as $db){
                                                                    echo "<td><input type='text' name='merk$x' class='form-control input-sm has-feedback ' value='".$db->merk."'/></td>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Type</label></td>
                                                            <?php
                                                                $x=1;
                                                                foreach($emisi_boiler as $db){
                                                                    echo "<td><input type='text' name='type$x' class='form-control input-sm has-feedback ' value='".$db->type."'/></td>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Kapasitas</label></td>
                                                            <?php
                                                                $x=1;
                                                                foreach($emisi_boiler as $db){
                                                                    echo "<td><input type='text' name='kapasitas$x' class='form-control input-sm has-feedback ' value='".$db->kapasitas."'/></td>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Bahan Bakar</label></td>
                                                            <?php
                                                                $x=1;
                                                                foreach($emisi_boiler as $db){
                                                                    echo "<td><select name='bb$x' class='form-control input-sm '/>";
                                                                    echo "<option value=''></option>";
                                                                        foreach ($bb as $b) {
                                                                            echo "<option value='$b->ket' ".(($db->bahan_bakar == $b->ket) ? 'selected' : '').">$b->ket</option>";
                                                                        }
                                                                    echo "</td>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr>
                                                            <td><label>Jumlah Bahan Bakar (ton/hari)</label></td>
                                                            <?php
                                                                $x=1;
                                                                foreach($emisi_boiler as $db){
                                                                    echo "<td><input type='text' name='jbb$x' class='form-control input-sm has-feedback ' value='".$db->jml_bahan_bakar."'/></td>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-group">
                                                <label>b. Data Cerobong/<i>Stackgas</i></label>
                                                </div>
                                                <div class="form-group">
                                                <div class="col-lg-6">
                                                    <label>Jumlah Cerobong      :</label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" name="jml_cerobong" id="jml_cerobong" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_udara->jml_cerobong?>" />
                                                </div>
                                                </div>
                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th>b.1 Data Cerobong</th>
                                                        <th class="text-center">1</th>
                                                        <th class="text-center">2</th>
                                                        <th class="text-center">3</th>
                                                        <th class="text-center">4</th>
                                                    </tr>
                                                </thead>
                                                    <?php
                                                        $c = 1;
                                                        foreach ($data_cerobong as $dc) {
                                                            $sampling_hole[$c] = adaConv($dc->sampling_hole);
                                                            $tangga[$c] = adaConv($dc->tangga);
                                                            $ppt[$c] = adaConv($dc->pengaman_tangga);
                                                            $lantai[$c] = adaConv($dc->lantai);
                                                            $c++;
                                                        }
                                                    ?>
                                                <tbody>
                                                <tr>
                                                    <td><label>Tinggi Cerobong(m)</label></td>
                                                    <?php
                                                        $x=1;
                                                        foreach($data_cerobong as $dc){
                                                            echo "<input type='hidden' name='id_cerobong$x' value='".$dc->id_cerobong."' />";
                                                            echo "<td><input type='text' name='h_crb$x' class='form-control input-sm has-feedback ' value='".$dc->h_cerobong."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Diameter(m)</label></td>
                                                    <?php
                                                        $x=1;
                                                        foreach($data_cerobong as $dc){
                                                            echo "<td><input type='text' name='d_crb$x' class='form-control input-sm has-feedback ' value='".$dc->d_cerobong."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Sampling Hole</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($data_cerobong); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='sampling_hole$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".adaConv($f)."' " ;
                                                                if (strcasecmp($sampling_hole[$p], adaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".adaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Tangga</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($data_cerobong); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='tangga$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".adaConv($f)."' " ;
                                                                if (strcasecmp($tangga[$p], adaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".adaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Pagar pengaman tangga</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($data_cerobong); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='ppt$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".adaConv($f)."' " ;
                                                                if (strcasecmp($ppt[$p], adaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".adaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Lantai kerja</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($data_cerobong); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='lantai$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".adaConv($f)."' " ;
                                                                if (strcasecmp($lantai[$p], adaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".adaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <?php
                                                    $c = 1;
                                                    foreach ($uji_emisi as $ue) {
                                                        $id_uji_emisi[$c] = $ue->id_uji_emisi;
                                                        $uk[$c] = $ue->uji_kualitas;
                                                        $per[$c] = $ue->period;
                                                        $bme[$c] = $ue->bme_pemenuhan;
                                                        $pe[$c] = $ue->pengendali_emisi;

                                                        $c++;
                                                    }
                                                    ?>
                                                <tr>
                                                    <td><label>b.2 Pengujian Kualitas Emisi</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($uji_emisi); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='uji_kualitas_emisi$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".adaConv($f)."' " ;
                                                                if (strcasecmp(adaConv($uk[$p]), adaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".adaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Periode pengujian (per 6 bulan)</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($uji_emisi); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='uemis_period$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".rutinConv($f)."' " ;
                                                                if (strcasecmp($per[$p], rutinConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".rutinConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Laboratorium Pengujian</label></td>
                                                    <?php
                                                        $x=1;
                                                        foreach($uji_emisi as $ue){
                                                            echo "<input type='hidden' name='id_uji_emisi$x' value='".$ue->id_uji_emisi."' />";
                                                            echo "<td><select name='laborator$x' class='form-control input-sm '/>";
                                                                foreach ($lab as $l) {
                                                                    echo "<option value='$l->nama_lab' ".(($ue->lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                                }
                                                            echo "</select></td>";
                                                            $x++;
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Pemenuhan BME</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($uji_emisi); $p++) {
                                                            echo "<td><select class='form-control input-sm' id='bmep$p' name='bme_pemenuhan$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".yaConv($f)."' " ;
                                                                if (strcasecmp(yaConv($bme[$p]), yaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".yaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Parameter tidak memenuhi BME</label></td>
                                                    <?php
                                                        $x=1;
                                                        foreach($uji_emisi as $ue){
                                                            echo "<td><input type='text' id='bme_param$x' name='bme_param$x' class='form-control input-sm has-feedback ' value='".$ue->bme_param."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Periode tidak memenuhi BME</label></td>
                                                    <?php
                                                        $x=1;
                                                        foreach($uji_emisi as $ue){
                                                            echo "<td><input type='text' id='bme_period$x' name='bme_period$x' class='form-control input-sm has-feedback ' value='".$ue->bme_period."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>b.3 Alat Pengendali Emisi</label></td>
                                                    <?php
                                                        for($p=1; $p<=count($uji_emisi); $p++) {
                                                            echo "<td><select class='form-control input-sm' name='pengendali_emisi$p'>";
                                                            $n=1;
                                                            foreach ($bool as $f) {
                                                                echo "<option value='".adaConv($f)."' " ;
                                                                if (strcasecmp(adaConv($pe[$p]), adaConv($f)) == 0){
                                                                    echo "selected='true'";
                                                                }
                                                                echo ">".adaConv($f)."</option>";
                                                                $n++;
                                                            }
                                                            echo "</select></td>";
                                                        }
                                                    ?>
                                                </tr>
                                                <tr>
                                                    <td><label>Jenis</label></td>
                                                    <?php
                                                        $x=1;
                                                        foreach($uji_emisi as $ue){
                                                            echo "<td><input type='text' name='jenis_alat$x' class='form-control input-sm has-feedback ' value='".$ue->jenis."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>
                                                </tr>
                                            </tbody>
                                            </table>
                                            </div>

                                            <div class="form-group ">
                                                    <label>a. Data Kualitas Udara Ambien</label>
                                                    <table class="table table-th-block">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <?php 
                                                                    // $n=1;
                                                                    foreach($uji_ambien as $site) {
                                                                        echo "<th class='text-center'>$site->lokasi</th>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><label>Pengujian Kualitas</label></td>
                                                                <?php
                                                                    $c = 1;
                                                                    foreach ($uji_ambien as $dt) {
                                                                        $id_uji_ambien[$c] = $dt->id_uji_ambien;
                                                                        $tuk[$c] = adaConv($dt->uji_kualitas);
                                                                        $per[$c] = $dt->period;
                                                                        $lab_uk[$c] = $dt->lab;
                                                                        $bm[$c] = $dt->bm_pemenuhan;
                                                                        $bm_param[$c] = $dt->bm_param;
                                                                        $bm_period[$c] = $dt->bm_period;
                                                                        $c++;
                                                                    }

                                                                    for($p=1; $p<=3; $p++) {
                                                                        echo "<input type='hidden' name='id_uji_ambien$p' value='".$id_uji_ambien[$p]."' />";
                                                                        echo "<td><select class='form-control input-sm' name='uji_kualitas$p'>";
                                                                        $n=1;
                                                                        foreach ($bool as $f) {
                                                                            echo "<option value='".adaConv($f)."' " ;
                                                                                if (strcasecmp($tuk[$p], adaConv($f)) == 0){
                                                                                    echo "selected='true'";
                                                                                }
                                                                            echo ">".adaConv($f)."</option>";
                                                                            $n++;
                                                                        }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Periode Pengujian (per 6 bulan)</label></td>
                                                                <?php
                                                                for($p=1; $p<=3; $p++) {
                                                                        echo "<td><select class='form-control input-sm' name='period$p'>";
                                                                        $n=1;
                                                                        foreach ($bool as $f) {
                                                                            echo "<option value='".rutinConv($f)."' " ;
                                                                                if (strcasecmp($per[$p], rutinConv($f)) == 0){
                                                                                    echo "selected='true'";
                                                                                }
                                                                            echo ">".rutinConv($f)."</option>";
                                                                            $n++;
                                                                        }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Laboratorium Penguji</label></td>
                                                                <?php
                                                                    for($n=1; $n<=count($lab_uk); $n++){
                                                                        echo "<td><select name='lab$n' class='form-control input-sm '>";
                                                                            foreach ($lab as $l) {
                                                                                echo "<option value='$l->nama_lab' ".(($lab[$n] == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                                            }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Pemenuhan BM</label></td>
                                                                <?php
                                                                for($p=1; $p<=3; $p++) {
                                                                        echo "<td><select class='form-control input-sm' name='bm_pemenuhan$p' id='bmp$p'>";
                                                                        $n=1;
                                                                        foreach ($bool as $f) {
                                                                            echo "<option value='".yaConv($f)."' " ;
                                                                                if (strcasecmp(yaConv($bm[$p]), yaConv($f)) == 0){
                                                                                    echo "selected='true'";
                                                                                }
                                                                            echo ">".yaConv($f)."</option>";
                                                                            $n++;
                                                                        }
                                                                        echo "</select></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Parameter Tidak Memenuhi BM</label></td>
                                                                <?php
                                                                    for($n=1; $n<=count($bm_param); $n++){
                                                                        echo "<td><input type='text' id='bm_param$n' name='bm_param$n' class='form-control input-sm has-feedback ' value='$bm_param[$n]'/></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                            <tr>
                                                                <td><label>Periode Tidak Memenuhi BM</label></td>
                                                                <?php
                                                                    for($n=1; $n<=count($bm_param); $n++){
                                                                        echo "<td><input type='text' id='bm_period$n' name='bm_period$n' class='form-control input-sm has-feedback ' value='$bm_period[$n]'/></td>";
                                                                    }
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php
                                                    $lapor = $bool;
                                                    $lapor[] = '2';
                                                ?>
                                            <div class="form-group ">
                                                    <label>b. Pelaporan pengujian kualitas udara emisi dan ambien</label>
                                                    <select class="form-control input-sm" name="pelaporan_ua_ue">
                                                    <?php 
                                                        for($d=0; $d<3; $d++){
                                                            echo "<option value='".rutinConv($lapor[$d])."'";
                                                                if (strcasecmp($pencemaran_udara->pelaporan_ua_ue, rutinConv($lapor[$d])) == 0){
                                                                    echo "selected";
                                                                }
                                                            echo ">".rutinConv($lapor[$d])."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </div>

                                                <div class="form-group ">
                                                    <label>c. Lain-lain</label>
                                                    <input type="text" name="lain_lain_pu" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_udara->lain_lain; ?>"/>
                                                </div>

                                            <div class="row"> <!-- navigation -->
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                        
                                        <div class="tab-pane" id="pencemaran_b3">
                                            <h4 class="page-tab">Pengendalian Limbah Padat dan B3</h4>
                                            <div class="form-group ">
                                                <label>a. Jenis Limbah B3 yang ditimbulkan</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">Jenis Limbah B3</th>
                                                            <th class="text-center" style="width:15%">Jumlah (ton/hari)</th>
                                                            <th class="text-center">Pengelolaan</th>
                                                            <th class="text-left" style="width:30%">Pihak Ketiga</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        for($x=0;$x<count($jenis_limbah_b3);$x++){ 
                                                        ?>
                                                        <tr data-duplicate="jenis_limbah" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah[]" id="jenis_limbah" class="form-control input-sm" required="required">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3 --</option>
                                                                    <?php foreach($jenis_limbah_b3_data_master as $uk):
                                                                        echo "<option value='".$uk->ket."' ".(($jenis_limbah_b3[$x]->jenis==$uk->ket) ? 'selected' : '').">".$uk->ket."</option>";
                                                                    endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="jumlah[]" class="form-control input-sm has-feedback" value='<?php echo $jenis_limbah_b3[$x]->jumlah; ?>'/></td>
                                                            <td><select class='form-control input-sm' name='pengelolaan[]'><option value="">
                                                            <?php
                                                                 foreach ($bool as $f) {

                                                                     echo "<option value='".kelolaConv($f)."' " ;
                                                                     if (strcasecmp($jenis_limbah_b3[$x]->pengelolaan, kelolaConv($f)) == 0){
                                                                         echo "selected='true'";
                                                                     }
                                                                     echo ">".kelolaConv($f)."</option>";
                                                                 }
                                                                 echo "</select></td>";
                                                                 ?>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:63%">
                                                                        <select name="pihak_ke3[]" class="form-control input-sm has-feedback">
                                                                            <option value=''></option>";
                                                                            <?php 
                                                                                foreach ($pihak_ke3 as $p3) {
                                                                                    echo "<option value='".$p3->badan_hukum." ".$p3->nama_industri."' ".(($jenis_limbah_b3[$x]->pihak_ke3==($p3->badan_hukum." ".$p3->nama_industri)) ? 'selected' : '').">".$p3->badan_hukum." ".$p3->nama_industri."</option>";
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="co-lg-6" style="padding-right: 0;">
                                                                        <div class=" text-right">
                                                                            <div class="btn-group" style="padding-right: 17px;">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="jenis_limbah"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="jenis_limbah"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                    <!-- <tr>
                                                        <input type='hidden' name='id_jlb311' value="<?php echo $jenis_limbah_b3[$x]->id_jlb3?>"/>
                                                        <td><input class='form-control input-sm has-feedback' type='text' name="<?php echo 'jenis'.($x+1);?>" value="<?php echo $jenis_limbah_b3[$x]->jenis?>"/></td>
                                                        <td><input class='form-control input-sm has-feedback' type='text' name="<?php echo 'jumlah'.($x+1);?>" value="<?php echo $jenis_limbah_b3[$x]->jumlah?>" /></td>
                                                        <td><select class='form-control input-sm' name="<?php echo 'pengelolaan'.($x+1);?>">
                                                        <?php
                                                        // foreach ($bool as $f) {

                                                        //         echo "<option value='".kelolaConv($f)."' " ;
                                                        //         if (strcasecmp($jenis_limbah_b3[$x]->pengelolaan, kelolaConv($f)) == 0){
                                                        //             echo "selected='true'";
                                                        //         }
                                                        //         echo ">".kelolaConv($f)."</option>";
                                                        //         $n++;
                                                        //     }
                                                        ?>
                                                        </select></td>
                                                        <td><select name="<?php echo 'pihak_ke3'.($x+1);?>" class='form-control input-sm has-feedback'>
                                                            <option value=''></option>
                                                            <?        
                                                                // foreach ($pihak_ke3 as $p3) {
                                                                //     echo "<option value=".$p3->nama_industri." ".(($jenis_limbah_b3[$x]->pihak_ke3==$p3->nama_industri) ? 'selected' : '').">".$p3->nama_industri."</option>";
                                                                // }
                                                            ?>     
                                                            </select></td>
                                                    </tr> -->
                                                    </tbody>
                                                </table>
                                            </div>


                                            <div class="form-group ">
                                                <label>b. Penyimpanan</label>
                                                <select class="form-control input-sm" name="b3_penyimpanan">
                                                    <option value="Pada TPS Limbah B3" <?php echo ($pencemaran_pb3->b3_penyimpanan == 'Pada TPS Limbah B3') ? 'selected' : ''; ?>>Pada TPS Limbah B3</option>
                                                    <option value="Di luar TPS Limbah B3" <?php echo ($pencemaran_pb3->b3_penyimpanan == 'Di luar TPS Limbah B3') ? 'selected' : ''; ?>>Di luar TPS Limbah B3</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>c. TPS Limbah B3</label>
                                                <table class="table table-th-block">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">TPS Limbah B3</th>
                                                            <th class="text-center">1</th>
                                                            <th class="text-center">2</th>
                                                            <th class="text-center">3</th>
                                                            <th class="text-center">4</th>
                                                        </tr>
                                                    </thead>
                                                <tbody>
                                                    <tr>
                                                    <td>Perizinan</td>
                                                        <td><select name="tb_izin1" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[0]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[0]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[0]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        <td><select name="tb_izin2" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[1]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[1]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[1]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        <td><select name="tb_izin3" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[2]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[2]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[2]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                        <td><select name="tb_izin4" class="form-control input-sm"><option value="3" <?php echo (($tps_b3[3]->izin == 3) ? 'selected' : ''); ?>> </option><option value="Ada" <?php echo (($tps_b3[3]->izin == 1) ? 'selected' : ''); ?>>Ada</option><option value="Tidak Ada" <?php echo (($tps_b3[3]->izin == 0) ? 'selected' : ''); ?>>Tidak Ada</option></select></td>
                                                    <?php
                                                        // $x=1;
                                                        // foreach($tps_b3 as $tb3){
                                                        //     $izin[$x] = $tb3->izin;
                                                        //     $x++;
                                                        // }

                                                        // $izin_tps = $bool;
                                                        // $izin_tps[] = '2';
                                                        // for($p=1; $p<=4; $p++) {
                                                        //     if($izin[$p] != NULL){
                                                        //         // $jml_tps = 
                                                        //         // echo "<input type='hidden' name='jml_tps$x' value='".$jlb->pihak_ke3."' />";
                                                        //         echo "<td><select class='form-control input-sm' name='tb_izin$p' >";
                                                        //         $n=1;
                                                        //         foreach ($izin_tps as $f) {
                                                        //             echo "<option value='".adaConv($f)."' " ;
                                                        //             if (strcasecmp(adaConv($izin[$p]), adaConv($f)) == 0){
                                                        //                 echo "selected='true'";
                                                        //             }
                                                        //             echo ">".adaConv($f)."</option>";
                                                        //             $n++;
                                                        //         }
                                                        //         echo "</select></td>";
                                                        //     } else {
                                                        //         echo "<td><select class='form-control input-sm' name='tb_izin$p' >";
                                                        //         $n=1;
                                                        //         $izin[$p] = "";
                                                        //         foreach ($izin_tps as $f) {
                                                        //             echo "<option value='".adaConv($f)."' " ;
                                                        //             if (strcasecmp(adaConv($izin[$p]), adaConv($f)) == 0){
                                                        //                 echo "selected='true'";
                                                        //             }
                                                        //             echo ">".adaConv($f)."</option>";
                                                        //             $n++;
                                                        //         }
                                                        //         echo "</select></td>";
                                                        //     }
                                                        // }
                                                    ?>
                                                    </tr>
                                                    <tr>
                                                    <td>Nomor</td>
                                                    <?php
                                                        $x=1;
                                                        foreach ($tps_b3 as $tb3) {
                                                            echo "<input type='hidden' name='id_tps_b3$x' value='".$tb3->id_tps_b3."' />";
                                                            echo "<td><input type='text' name='tb_izin_no$x' class='form-control input-sm has-feedback' value='".$tb3->izin_no."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>  
                                                    </tr>
                                                    <tr>
                                                    <td>Tanggal</td>
                                                    <?php
                                                        $x=1;
                                                        foreach ($tps_b3 as $tb3) {
                                                            echo "<td><input type='text' name='tb_izin_tgl$x' class='form-control input-sm datepicker' data-date-format='yyyy-mm-dd' placeholder='yyyy-mm-dd' value='".$tb3->izin_tgl."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>  
                                                    </tr>
                                                    <tr>
                                                    <td>Jenis Limbah</td>
                                                    <?php
                                                        $x=1;
                                                        foreach ($tps_b3 as $tb3) {
                                                            echo "<td><input type='text' name='tb_jenis$x' class='form-control input-sm has-feedback' value='".$tb3->jenis."'/></td>";
                                                            $x++;
                                                        }
                                                    ?>  
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><label>Lama Penyimpanan</label></td>
                                                    </tr>
                                                    <tr>
                                                    <?php
                                                        if($tps_b3!=null) {
                                                            $i = 1;
                                                            foreach ($tps_b3 as $tb3) {
                                                                $test = unserialize($tb3->lama_spn);
                                                                foreach ($test['tps'] as $v) {
                                                                    $lama[] = $v;
                                                                }
                                                                $new['lama_tps_'.$i] = $lama;
                                                                unset($lama);
                                                                $i++;
                                                            }

                                                            $test = unserialize($tps_b3[0]->lama_spn);

                                                            $n = 0;
                                                            if ($test['jenis_limbah']!=null) {
                                                                foreach ($test['jenis_limbah'] as $key => $value) {
                                                                    $f = 1;
                                                                    $newest['jenis'] = $value;
                                                                    foreach ($new as $x => $y) {
                                                                        $newest['tps_' . $f] = $y[$n];
                                                                        $f++;
                                                                    }
                                                                    $all_new[] = $newest;

                                                                    unset($t);
                                                                    $n++;
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                    <?php if(isset($all_new)) { foreach ($all_new as $key => $value){ ?>
                                                            
                                                    <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                                                        <td>
                                                            <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                                                <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                                                <?php foreach($jenis_limbah_b3_data_master as $uk):
                                                                    echo "<option value='".$uk->ket."'";
                                                                        if ($value['jenis'] == $uk->ket) echo "selected";
                                                                    echo ">$uk->ket</option>";
                                                                endforeach;?>
                                                                <?php // foreach($jenis_limbah_b3 as $uk): ?>
                                                                    <!-- <option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option> -->
                                                                <?php // endforeach;?>
                                                            </select>
                                                        </td>
                                                        <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" value="<?php echo $value['tps_1']?>" /></td>
                                                        <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" value="<?php echo $value['tps_2']?>" /></td>
                                                        <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" value="<?php echo $value['tps_3']?>" /></td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-lg-6" style="width:60%">
                                                                    <div class="text-left">
                                                                        <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" value="<?php echo $value['tps_4']?>" />
                                                                    </div>
                                                                </div>
                                                                <div class="co-lg-6">
                                                                    <div class=" text-right" >
                                                                        <div class="btn-group">
                                                                            <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                                                            <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } } else { ?>
                                                        <tr data-duplicate="lama_simpan" data-duplicate-min="1">
                                                            <td>
                                                                <select name="jenis_limbah_lama_spn[]" id="jenis_limbah_lama_spn" class="form-control input-sm">
                                                                    <option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
                                                                    <?php foreach($jenis_limbah_b3_data_master as $uk): ?>
                                                                        <option value="<?php echo $uk->ket;?>" <?php if ($uk->ket=='Nilai Kosong') echo 'selected';?>><?php echo $uk->ket; ?></option>
                                                                    <?php endforeach;?>
                                                                </select>
                                                            </td>
                                                            <td><input type="text" name="tb_lama_spn_1[]" class="form-control input-sm has-feedback tb_lama_spn_1" /></td>
                                                            <td><input type="text" name="tb_lama_spn_2[]" class="form-control input-sm has-feedback tb_lama_spn_2" /></td>
                                                            <td><input type="text" name="tb_lama_spn_3[]" class="form-control input-sm has-feedback tb_lama_spn_3" /></td>
                                                            <td>
                                                                <div class="row">
                                                                    <div class="col-lg-6" style="width:60%">
                                                                        <div class="text-left">
                                                                            <input type="text" name="tb_lama_spn_4[]" class="form-control input-sm has-feedback tb_lama_spn_4" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-lg-6">
                                                                        <div class=" text-right" >
                                                                            <div class="btn-group">
                                                                                <button type="button" class="btn btn-sm btn-success" data-duplicate-add="lama_simpan"><i class="fa fa-plus-circle"></i></button>
                                                                                <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="lama_simpan"><i class="fa fa-times-circle"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <?php
                                                        $x=1;
                                                        foreach($tps_b3 as $jl){
                                                            $koord_derajat_s[$x] = $jl->koord_derajat_s;
                                                            $koord_jam_s[$x] = $jl->koord_jam_s;
                                                            $koord_menit_s[$x] = $jl->koord_menit_s;
                                                            $koord_derajat_e[$x] = $jl->koord_derajat_e;
                                                            $koord_jam_e[$x] = $jl->koord_jam_e;
                                                            $koord_menit_e[$x] = $jl->koord_menit_e;
                                                            $x++;
                                                        }
                                                    ?>
                                                    <tr>
                                                        <td>Koordinat</td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s1" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[1]?>" placeholder="00&deg;">
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s1" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[1]?>" placeholder="00&#39;">
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s1" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[1]?>" placeholder="00&quot;"> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e1" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[1]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e1" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[1]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e1" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[1]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s2" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[2]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s2" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[2]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s2" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[2]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e2" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[2]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e2" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[2]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e2" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[2]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s3" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[3]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s3" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[3]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s3" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[3]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e3" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[3]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e3" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[3]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e3" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[3]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12 ">
                                                                    <label>S</label>
                                                                    <div class="row">                               
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_s4" id="derajat_s" class="form-control input-sm" value="<?php echo $koord_derajat_s[4]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_s4" id="jam_s" class="form-control input-sm" value="<?php echo $koord_jam_s[4]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_s4" id="menit_s" class="form-control input-sm" value="<?php echo $koord_menit_s[4]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="padding-right: 14px;">
                                                                <div class="col-lg-12">
                                                                    <label>E</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_derajat_e4" id="derajat_e" class="form-control input-sm" value="<?php echo $koord_derajat_e[4]?>" placeholder="00&deg;">  
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_jam_e4" id="jam_e" class="form-control input-sm" value="<?php echo $koord_jam_e[4]?>" placeholder="00&#39;">     
                                                                        </div>
                                                                        <div class="col-lg-4" style="padding-right: 0;">
                                                                            <input type="text" name="koord_menit_e4" id="menit_e" class="form-control input-sm" value="<?php echo $koord_menit_e[4]?>" placeholder="00&quot;">  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td>Ukuran (m2)</td>
                                                    <?php
                                                        $x=1;
                                                        foreach ($tps_b3 as $tb3) {
                                                            echo "<td><input type='text' name='tb_ukuran$x' value='".$tb3->ukuran."' class='form-control input-sm has-feedback' /></td>";
                                                            $x++;
                                                        }
                                                    ?>  
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4"><label>Kelengkapan</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Papan Nama dan Koordinat</td>
                                                        <?php
                                                            $c = 1;
                                                            foreach ($tps_b3 as $dt) {
                                                                $ppn[$c] = adaConv($dt->ppn_nama_koord);
                                                                $simbol[$c] = adaConv($dt->simbol_label);
                                                                $salcer[$c] = adaConv($dt->saluran_cecer);
                                                                $bakcer[$c] = adaConv($dt->bak_cecer);
                                                                $kemiringan[$c] = adaConv($dt->kemiringan);
                                                                $sop[$c] = adaConv($dt->sop_darurat);
                                                                $log[$c] = adaConv($dt->log_book);
                                                                $apar[$c] = adaConv($dt->apar_p3k);
                                                                $p3k[$c] = adaConv($dt->p3k);
                                                                $c++;
                                                            }
                                                                    
                                                            for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_ppn_nama_koord$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($ppn[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Simbol dan Label</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_simbol_label$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($simbol[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Saluran Ceceran Air Limbah</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_saluran_cecer$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($salcer[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Bak Penampung Ceceran Air Limbah</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_bak_cecer$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($bakcer[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Kemiringan</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_kemiringan$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($kemiringan[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>SOP Pengelolaan dan Tanggap Darurat</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_sop_darurat$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($sop[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Log Book</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                                echo "<td><select class='form-control input-sm' name='tb_log_book$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($log[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>APAR</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                            if($apar[$p]!=null) {
                                                                echo "<td><select class='form-control input-sm' name='tb_apar_p3k$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($apar[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            } else {
                                                               echo "<td><select class='form-control input-sm' name='tb_apar_p3k$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                                                                }
                                                                echo "</select></td>"; 
                                                            }
                                                        }
                                                        ?>
                                                    </tr>
                                                    <tr>
                                                        <td>Kotak P3K</td>
                                                        <?php
                                                        for($p=1; $p<=4; $p++) {
                                                            if($p3k[$p]!=null) {
                                                                echo "<td><select class='form-control input-sm' name='tb_p3k$p'>";
                                                                $n=1;
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' " ;
                                                                        if (strcasecmp($p3k[$p], adaConv($f)) == 0){
                                                                            echo "selected='true'";
                                                                        }
                                                                        echo ">".adaConv($f)."</option>";
                                                                    $n++;
                                                                }
                                                                echo "</select></td>";
                                                            } else {
                                                                echo "<td><select class='form-control input-sm' name='tb_p3k$p'>";
                                                                foreach ($bool as $f) {
                                                                    echo "<option value='".adaConv($f)."' >".adaConv($f)."</option>";
                                                                }
                                                                echo "</select></td>";
                                                            }
                                                        }
                                                        ?>
                                                    </tr>
                                                </tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <label>e. Pengujian Kualitas Limbah B3</label>
                                                    <select class="form-control input-sm" name="b3_uji">
                                                        <option value="Sudah" <?php echo ($pencemaran_pb3->b3_uji == '1') ? 'selected' : ''; ?>>Sudah</option>
                                                        <option value="Belum" <?php echo ($pencemaran_pb3->b3_uji == '0') ? 'selected' : ''; ?>>Belum</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-lg-6">
                                                    <label>Jenis Pengujian</label>
                                                    <select class="form-control input-sm" name="b3_uji_jenis" id="b3_uji_jenis" multiple="multiple"  style="border: 0!important">
                                                        <option value=""></option>
                                                        <?php
                                                                $jenis_uji = explode(', ', $pencemaran_pb3->b3_uji_jenis);
                                                                foreach ($jenis_pengujian as $j){
                                                                    echo "<option value='$j->ket' ";
                                                                        for ($i=0; $i < count($jenis_uji); $i++) {
                                                                        if($jenis_uji[$i]==$j->ket) {
                                                                            echo "selected";
                                                                        }
                                                                    }
                                                                    echo ">$j->ket</option>";
                                                                }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label>Parameter Uji</label>
                                                <input type="text" name="b3_uji_param" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->b3_uji_param?>" />
                                            </div>
                                            <div class="form-group ">
                                                <label>Laboratorium Penguji</label>
                                                <select name="b3_uji_lab" class="form-control input-sm has-feedback">
                                                <option value=""></option>>
                                                    <?php
                                                        foreach ($lab as $l) {
                                                            echo "<option value='$l->nama_lab' ".(($pencemaran_pb3->b3_uji_lab == $l->nama_lab) ? 'selected' : '').">$l->nama_lab</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group ">
                                                <label>Waktu Pengujian</label>
                                                <input type="text" name="b3_uji_waktu" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->b3_uji_waktu?>" />
                                            </div>
                                            <div class="form-group ">
                                                <label>f. Neraca Limbah B3</label>
                                                <select class="form-control input-sm" name="b3_neraca">
                                                    <option value="Ada" <?php echo ($pencemaran_pb3->b3_neraca == '1') ? 'selected' : ''; ?>>Ada</option>
                                                    <option value="Tidak ada" <?php echo ($pencemaran_pb3->b3_neraca == '0') ? 'selected' : ''; ?>>Tidak Ada</option>
                                                </select>
                                            </div>

                                            <div class="form-group ">
                                                <label>g. Manifest</label>
                                                <select class="form-control input-sm" name="b3_manifest">
                                                    <option value="Ada" <?php echo ($pencemaran_pb3->b3_manifest == '1') ? 'selected' : ''; ?>>Ada</option>
                                                    <option value="Tidak ada" <?php echo ($pencemaran_pb3->b3_manifest == '0') ? 'selected' : ''; ?>>Tidak Ada</option>
                                                </select>
                                            </div>
                                            <?php
                                                    $lapor = $bool;
                                                    $lapor[] = '2';
                                                ?>
                                            <div class="form-group ">
                                                    <label>h. Pelaporan Neraca dan Manifest</label>
                                                    <select class="form-control input-sm" name="b3_lapor_nm">
                                                    <?php 
                                                        for($d=0; $d<3; $d++){
                                                            echo "<option value='".rutinConv($lapor[$d])."'";
                                                                if (strcasecmp($pencemaran_pb3->b3_lapor_nm, rutinConv($lapor[$d])) == 0){
                                                                    echo "selected";
                                                                }
                                                            echo ">".rutinConv($lapor[$d])."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                            </div>

                                            <?php 
                                                $arr_padat_jenis = explode(', ', $pencemaran_pb3->padat_jenis);
                                                $arr_padat_jumlah = explode(', ', $pencemaran_pb3->padat_jumlah);
                                                $new = array();
                                                for ($i=0; $i < count($arr_padat_jenis); $i++) { 
                                                    $new[$arr_padat_jenis[$i]] = $arr_padat_jumlah[$i];
                                                }
                                            ?>

                                            <label>i. Limbah Padat Lain</label>

                                            <table class="table table-th-block">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Jenis</th>
                                                        <th class="text-center">Jumlah (ton/hari)</th>
                                                        <th class="text-center"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($new as $key => $value) { ?>
                                                    <tr data-duplicate="limbah_padat_lain" data-duplicate-min="1">
                                                        <td class="col-lg-5"><input type="text" name="padat_jenis[]" class="form-control input-sm has-feedback" value="<?php echo $key?>" /></td></td>
                                                        <td class="col-lg-5"><input type="number" name="padat_jumlah[]" class="form-control input-sm has-feedback" value="<?php echo $value?>" /></td>
                                                        <td class="col-lg-2">
                                                            <div class=" text-right">
                                                                <div class="btn-group" style="padding-right: 17px;">
                                                                    <button type="button" class="btn btn-sm btn-success" data-duplicate-add="limbah_padat_lain"><i class="fa fa-plus-circle"></i></button>
                                                                    <button type="button" class="btn btn-sm btn-danger" data-duplicate-remove="limbah_padat_lain"><i class="fa fa-times-circle"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            
                                            <!-- <div class="form-group ">
                                                <label>i. Limbah Padat Lain</label>
                                                <input type="text" name="padat_jenis" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_jenis?>"/>
                                            </div>
                                            <div class="form-group ">
                                                <label> Jumlah</label>
                                                <input type="text" name="padat_jumlah" class="form-control input-sm has-feedback" value="<?php echo $pencemaran_pb3->padat_jumlah?>"/>
                                            </div> -->
                                            <div class="form-group ">
                                                <label>d. Pengelolaan</label>
                                                <select class="form-control input-sm" id="pjpl" name="padat_kelola">
                                                    <?php 
                                                        $padat_kelola = $pencemaran_pb3->padat_kelola;
                                                        echo $padat_kelola;
                                                            if (($padat_kelola == "Dibakar") || ($padat_kelola == "Diangkut oleh Pihak Ketiga") || ($padat_kelola == "Diangkut oleh Dinas Pertasih") || ($padat_kelola == "Dikelola oleh Warga Sekitar") || ($padat_kelola == "Dijual")){
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($padat_kelola == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola_lain' id='pjpl2' class='form-control input-sm has-feedback' value='' style='display:none'/>";
                                                        } else {
                                                            $temp = "Lainnya";
                                                            foreach ($pengelolaan as $p) {
                                                                echo "<option value='$p->ket' ".(($temp == $p->ket) ? 'selected' : '').">$p->ket</option>";
                                                            }
                                                    echo "</select>";
                                                    echo "<input type='text' name='padat_kelola_lain' id='pjpl2' value='$padat_kelola' class='form-control input-sm has-feedback'/>";
                                                        }
                                                ?>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                    <a class="btn btn-warning NextStep">Next step <i class="fa fa-angle-right"></i></a>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="lain_lain">
                                            <h4 class="page-tab">Lain - Lain</h4>
                                            <div class="form-group ">
                                                <textarea class="form-control input-sm no-resize bold-border" name="catatan" style="height: 180px;"><?php echo $bap->catatan; ?></textarea>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-warning PrevStep"><i class="fa fa-angle-left"></i> Prev step</a>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6 text-right">
                                                <button type="button" class="btn btn-danger" onclick="window.location='<?php echo base_url('backend/bap_agro'); ?>'"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                                                    <button type="submit" name="register" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                                </div><!-- /.col-sm-6 -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>

                        
                </div>
            </div>

                <?php
                    function adaConv($value) {
                                    if ($value == '1') {
                                        return "Ada";
                                    } else if ($value == '0') {
                                        return "Tidak Ada";
                                    } else {
                                        return "";
                                    }
                                } 

                                # function to convert boolean to string (Ya / Tidak)
                                function yaConv($value) {
                                    if ($value == true) {
                                        return "Ya";
                                    } else {
                                        return "Tidak";
                                    }
                                }

                                function rutinConv($value) {
                                    if ($value == '1') {
                                        return "Rutin";
                                    } else if ($value == '0'){
                                        return "Tidak Rutin";
                                    } else {
                                        return "Tidak Melaporkan";
                                    }
                                }

                                function penuhConv($value) {
                                    if ($value == '1') {
                                        return "Memenuhi";
                                    } else {
                                        return "Tidak Memenuhi Baku Mutu";
                                    }
                                }

                                function kelolaConv($value) {
                                    if ($value == '1') {
                                        return "Dikelola";
                                    } else {
                                        return "Tidak Dikelola";
                                    }
                                }
                ?>

                    
                  <!--   <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Buat BAP</button>
                    <br/><br/> -->

        </div><!-- /.the-box .default -->
                <!-- END DATA TABLE -->
    			
    </div><!-- /.container-fluid -->
    <!-- END PAGE CONTENT -->

    <script type="text/javascript">
        $(document).ready(function() {

            var site = "<?php echo site_url(); ?>";
            $('#nama_industri').autocomplete({
                minChars: 2,
                type: 'POST',
                noCache: true,
                serviceUrl: site + 'backend/industri/search_industri_bap/11',
                onSearchStart: function (query) {
                    $('#id_industri').val(null);              
                },
                onSelect: function (suggestion) {
                    $('#id_industri').val(suggestion.data).change();
                    var id_last_input = $('#id_industri').val();
                    $('#id_last_input').val(id_last_input);
                    console.log($('#id_last_input').val());
                }
            });

            $("select[name=id_industri]").change(function(){
                $(this).get_detail_industri(this, '#container2');
            });

            $('#id_petugas_bap1').multiselect({
                checkboxName: 'petugas_bap[]'
            });

            $('#id_petugas_bap2').multiselect({
                checkboxName: 'petugas_bap[]'
            });

            $('#b3_uji_jenis').multiselect({
                checkboxName: 'b3_uji_jenis[]'
            });

            var ipal = $("#ipal option:selected").val();
            if (ipal == "Ada") {
                $('#detail_ipal').show();
            } else {
                $('#detail_ipal').hide();
                $('#si').val(null);
            }

            var izin = $("#izin option:selected").val();
            if (izin == "Ada") {
                $('#detail_izin').show();
            } else {
                $('#detail_izin').hide();
            }

            var au = $("#au option:selected").val();
            if (au == "Ada") {
                $('#detail_au').show();
            } else {
                $('#detail_au').hide();
                $('#je').val(null);
            }

            var ul = $("#uji_limbah option:selected").val();
            if (ul == "Ada") {
                $('#detail_uji').show();
            } else {
                $('#detail_uji').hide();
            }

            var emboil = $("#emboil option:selected").val();
            if (emboil == "other") {
                $('#emisi_lain').show();
            } else {
                $('#emisi_lain').hide();
            }            

            var dl = $('input[name=dl]:checked').val();
            if (dl == "ada") {
                    $('#jdl').show();
            } else {
                    $('#jdl').hide();
            }

            var il = $('input[name=il]:checked').val();
            if (il == "ada") {
                    $('#il_tahun').show();
            } else {
                    $('#il_tahun').hide();
                    $('#il_tahun').val(null);
            }

            $('#peru_stat').change(function() {
                if($(this).val()  == 'other') {
                    $('#peru_stat_lain').show();
                    $('#peru_stat_lain').focus();
                } else {
                    $('#peru_stat_lain').hide();
                    $('#peru_stat_lain').val(null);
                }
            });

         $('#dl1').click(function (e) {
            $('#jdl').show();
            $('#jdl_name').val(null);
            $('#dlt').val(null);
        });

        $('#dl2').click(function (e) {
            $('#jdl').hide();
            $('#jdl_name').val(null);
            $('#dlt').val(null);
        });

        $('#il1').click(function (e) {
            $('#il_tahun').show();
            $('#ilt').val(null);
        });

        $('#il2').click(function (e) {
            $('#il_tahun').hide();
            $('#ilt').val(null);
        });

        $('#izin').change(function() {
            if($(this).val() == 'Ada') {
                $('#detail_izin').show();
            } else {
                $('#detail_izin').hide();
                $('#izin_no').val(null);
                $('#izin_tgl').val(null);
            }
        });

        $('#ipal').change(function() {
            if($(this).val() == 'Ada') {
                $('#detail_ipal').show();
            } else {
                $('#detail_ipal').hide();
                $('#si').val(null);
                $('#si2').val(null);
                $('#ipal_unit').val(null);
                $('#ipal_kapasitas').val(null);
            }
        });

        $('#si').change(function() {
            if($(this).val()  == 'other') {
                $('#si2').show();
                $('#si2').focus();
            } else {
                $('#si2').hide();
                $('#si2').val(null);
            }
        });

        $('#au').change(function() {
            if($(this).val() == 'Ada') {
                $('#detail_au').show();
            } else {
                $('#detail_au').hide();
                $('#je').val(null);
                $('#je2').val(null);
            }
        });

        $('#je').change(function() {
            if($(this).val()  == 'other') {
                $('#je2').show();
                $('#je2').focus();
            } else {
                $('#je2').hide();
                $('#je2').val(null);
            }
        });

        $('#uji_limbah').change(function() {
            if($(this).val() == 'Ada') {
                $('#detail_uji').show();
            } else {
                $('#detail_uji').hide();
                $('#pp').val("other");
                $('#pp2').val(null);
                $('#uji_hasil').val("Tidak Memenuhi Baku Mutu");
                $('input[name=uji_tidak_bulan]').val('-');
                $('input[name=uji_tidak_param]').val('-');
            }
        });

        $('#pp').change(function() {
            if($(this).val()  == 'other') {
                $('#pp2').show();
                $('#pp2').focus();
            } else {
                $('#pp2').hide();
                $('#pp2').val(null);
            }
        });

        $('#emboil').change(function() {
            if($(this).val()  == 'other') {
                $('#emisi_lain').show();
                $('#emisi_lain').focus();
            } else {
                $('#emisi_lain').hide();
                $('#emisi_lain').val(null);
            }
        });

        $('#bocor').change(function() {
            if($(this).val()  == 'Ada') {
                $('#lok').show();
                $('#lok').focus();
            } else {
                $('#lok').hide();
                $('input[name=bocor_lokasi]').val(null);
            }
        });
        $('#bmp1').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bm_param1').val('-');
                $('#bm_period1').val('-');
            }
        });
        $('#bmp2').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bm_param2').val('-');
                $('#bm_period2').val('-');
            }
        });
        $('#bmp3').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bm_param3').val('-');
                $('#bm_period3').val('-');
            }
        });
        $('#bmep1').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bme_param1').val('-');
                $('#bme_period1').val('-');
            }
        });
        $('#bmep2').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bme_param2').val('-');
                $('#bme_period2').val('-');
            }
        });
        $('#bmep3').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bme_param3').val('-');
                $('#bme_period3').val('-');
            }
        });
        $('#bmep4').change(function() {
            if($(this).val()  == 'Ya') {
                $('#bme_param4').val('-');
                $('#bme_period4').val('-');
            }
        });

        });
    </script>

    <!-- BEGIN BACK TO TOP BUTTON -->
    <div id="back-top">
        <a href="#top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <!-- END BACK TO TOP -->		
