<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_teguran_lhu extends CI_Model {	
	
	function __construct() {
		parent::__construct();	

        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
	}

	function get_all($where=array(), $limit=0, $offset=0) {

		$data = array();		
		$sql = 'select i.id_industri, i.nama_industri, l.laporan_bulan_tahun, air.id_laporan_hasil_uji as id_air_limbah, emisi.id_laporan_hasil_uji as id_udara_emisi, ambien.id_laporan_hasil_uji as id_udara_ambien, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = air.id_laporan_hasil_uji) as teguran_air, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = emisi.id_laporan_hasil_uji) as teguran_emisi, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = ambien.id_laporan_hasil_uji) as teguran_ambien, (select air.status or emisi.status) as status_lhu from industri i left join laporan_hasil_uji l on l.id_industri = i.id_industri left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun, status from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah") air on air.id_industri = i.id_industri and air.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun, status from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi") emisi on emisi.id_industri = i.id_industri and emisi.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Ambien") ambien on ambien.id_industri = i.id_industri and ambien.laporan_bulan_tahun = l.laporan_bulan_tahun where l.id_laporan_hasil_uji is not null group by i.nama_industri, l.laporan_bulan_tahun order by l.laporan_bulan_tahun desc limit '.$offset.''.$limit;

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
		}
		return $data;

	}

	function get_detail_lhu($id) {
		$this->db->where('id_laporan_hasil_uji', $id);
		
		$query = $this->db->get('laporan_hasil_uji');
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function get_num_rows($where) {

		$sql = 'select i.id_industri, i.nama_industri, l.laporan_bulan_tahun, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = air.id_laporan_hasil_uji) as teguran_air, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = emisi.id_laporan_hasil_uji) as teguran_emisi, (select group_concat(parameter_lhu separator ";") from history_item_teguran_lhu where id_laporan_hasil_uji = ambien.id_laporan_hasil_uji) as teguran_ambien from industri i left join laporan_hasil_uji l on l.id_industri = i.id_industri left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah") air on air.id_industri = i.id_industri and air.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi") emisi on emisi.id_industri = i.id_industri and emisi.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Ambien") ambien on ambien.id_industri = i.id_industri and ambien.laporan_bulan_tahun = l.laporan_bulan_tahun where l.id_laporan_hasil_uji is not null group by i.nama_industri, l.laporan_bulan_tahun order by l.laporan_bulan_tahun desc';

		$query = $this->db->query($sql);

		return $this->db->count_all_results();
	}


	function get_history($id) {
		$query = $this->db->query('select group_concat(parameter_lhu separator ";") as parameter, dasar_hukum from history_item_teguran_lhu where id_laporan_hasil_uji = '.$id);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	function update_lhu($data, $id) {
		$this->db->where("id_laporan_hasil_uji", $id);
		$this->db->update("laporan_hasil_uji", $data);		
		
		return $this->db->affected_rows() > 0;
	}

	function get_notif_lhu() {
		$data = array();

		// $sql = 'SELECT a.id_industri as id_industri, a.id_laporan_hasil_uji as id_lhu, b.nama_industri as nama_industri, b.badan_hukum as bdn_hukum, a.dibuat_oleh as maker, a.tgl_pengambilan_sample as timeago, a.laporan_bulan_tahun as tgl, c.status_pelanggaran as jml_pelanggaran, a.jenis_lhu as jenis_lhu FROM laporan_hasil_uji a inner join industri b on a.id_industri = b.id_industri inner join (select id_laporan_hasil_uji, sum(status_pelanggaran) as status_pelanggaran from detail_lhu where status_pelanggaran=1 group by id_laporan_hasil_uji ) c on c.id_laporan_hasil_uji = a.id_laporan_hasil_uji where a.status != 1 group by a.id_laporan_hasil_uji';
		$sql = 'SELECT l.id_industri as id_industri, l.id_laporan_hasil_uji as id_lhu, b.nama_industri as nama_industri, b.badan_hukum as bdn_hukum, air.id_laporan_hasil_uji as id_air_limbah, emisi.id_laporan_hasil_uji as id_udara_emisi, l.dibuat_oleh as maker, l.tgl_pengambilan_sample as timeago, l.laporan_bulan_tahun as tgl, sum(c.status_pelanggaran=1) as jml_pelanggaran, (select count(id_laporan_hasil_uji) from history_item_teguran_lhu where id_laporan_hasil_uji = air.id_laporan_hasil_uji) as teguran_air, (select count(id_laporan_hasil_uji) from history_item_teguran_lhu where id_laporan_hasil_uji = emisi.id_laporan_hasil_uji) as teguran_emisi FROM laporan_hasil_uji l inner join detail_lhu c on c.id_laporan_hasil_uji = l.id_laporan_hasil_uji inner join industri b on l.id_industri = b.id_industri left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Air Limbah") air on air.id_industri = b.id_industri and air.laporan_bulan_tahun = l.laporan_bulan_tahun left join (select id_laporan_hasil_uji, id_industri, laporan_bulan_tahun from laporan_hasil_uji where jenis_lhu = "LHU Udara Emisi") emisi on emisi.id_industri = b.id_industri and emisi.laporan_bulan_tahun = l.laporan_bulan_tahun where l.status != 1 group by l.id_industri';

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}

		return NULL;
	}

	function count_notif_lhu() {
		// $sum = $this->db->query("SELECT count(distinct id_industri) as total_notif_lhu FROM laporan_hasil_uji where status != 1")->row()->total_notif_lhu;
		$sum = $this->db->query("SELECT count(distinct a.id_industri) as total_notif_lhu FROM laporan_hasil_uji a WHERE a.id_laporan_hasil_uji IN (SELECT DISTINCT e.id_laporan_hasil_uji FROM history_item_teguran_lhu e GROUP BY e.id_laporan_hasil_uji) and a.status != 1")->row()->total_notif_lhu;
		return $sum;
	}
}
?>