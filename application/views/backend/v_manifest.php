<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">MANIFEST&nbsp;&nbsp;<small>mengelola seluruh data manifest</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>SWAPANTAU</li>
                <li class="active">Manifest</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <!-- button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/>   -->

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#manifest_data"  class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th width="30%">Penghasil Limbah B3</th>
                                <th width="30%">Pengangkut Limbah B3</th>                                
								<th width="30%">Pengolah Limbah B3</th>
								<th>Berkas</th>
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 10,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/manifest/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    if(d.total < 10){ $('#start').text(d.total); }                    
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : ' ';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
						t += '<td>';
						t += '<b data-toggle="tooltip" data-placement="right" data-html="true" title="<small>Usaha/Kegaitan: '+dt.usaha_kegiatan+'</small><br/><small>Jenis/Kelas/Tipe: '+jenis_industri+'</small><br/><small>'+ dt.alamat + '</small><br/><small>Kel. '+dt.nama_kelurahan + ' Kec. '+dt.nama_kecamatan+ ' </small><br/>">'+ dt.badan_hukum +' '+dt.nama_industri+'</b><br/>';
                        t += '<small><b>Telp.</b> '+dt.tlp+'</small>, ';
                        t += '<small><b>Fax.</b> '+dt.fax+'</small><br/>';
                        t += '<small>'+dt.email+'</small>';                        
                        t += '</td>';
						t += '<td>';
						t += '<b data-toggle="tooltip" data-placement="right" data-html="true" title="<small>'+dt.alamat_pengangkut + '</small><br/><small>Kel. '+dt.kel_pengangkut + ' Kec. '+dt.kec_pengangkut+ ' </small></br>">'+dt.bh_pengangkut+' '+dt.nm_pengangkut + '</b><br/>';					
						t += '<small><b>Telp.</b> '+dt.tlp_pengangkut+'</small>, ';
                        t += '<small><b>Fax.</b> '+dt.fax_pengangkut+'</small><br/> ';
                        t += '<small>'+dt.email_pengangkut+'</small>'; 					
                        t += '</td>';
						t += '<td>';
						t += '<b data-toggle="tooltip" data-placement="right" data-html="true" title="<small>'+dt.alamat_pengolah + '</small><br/><small>Kel. '+dt.kel_pengolah + ' Kec. '+dt.kec_pengolah+ '</small>">'+dt.bh_pengolah+' '+dt.nm_pengolah + '</b><br/>';
		                t += '<small><b>Telp.</b> '+dt.tlp_pengolah+'</small>, ';
                        t += '<small><b>Fax.</b> '+dt.fax_pengolah+'</small><br/> ';
                        t += '<small>'+dt.email_pengolah+'</small>'; 
                        t += '</td>';
						var link = '<?php echo base_url("assets/manifest/'+dt.nama_berkas+'"); ?>';
                        t += '<td><a href="'+link+'" target="_blank" title="Klik Untuk Download Berkas"><i class="fa fa-file-text icon-sidebar"></i></a></td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
						t += '<li><a onclick="$(this).detail('+dt.id_manifest+')">Lihat Data Pengiriman Limbah</a></li>';
						t += '<li><a onclick="$(this).upload_berkas('+dt.id_manifest+')">Upload Berkas</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_manifest+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_manifest+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }
	});

	$(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }
		
        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/manifest/get'); ?>",
                data: 'id_manifest=' + id,
                success: function (response) {
                    
                    $('#manifest_data form')[0].reset();
                    $('#manifest_data form input[type=hidden]').val('');

                    $('input[id=edit_id]').val(response.id_manifest);
                    //$('select[name=penghasil_limbah]').val(response.id_penghasil_limbah);
					$('select[name=nama_industri]').chosen();
                    $('select[name=nama_industri]').val(response.id_penghasil_limbah);
                    $('select[name=nama_industri]').trigger("chosen:updated");
					//$('select[name=pengangkut_limbah]').val(response.id_pengangkut_limbah);
					$('select[name=pengangkut_limbah]').chosen();
                    $('select[name=pengangkut_limbah]').val(response.id_pengangkut_limbah);
                    $('select[name=pengangkut_limbah]').trigger("chosen:updated");
					//$('select[name=pengolah_limbah]').val(response.id_pengolah_limbah);
					$('select[name=pengolah_limbah]').chosen();
                    $('select[name=pengolah_limbah]').val(response.id_pengolah_limbah);
                    $('select[name=pengolah_limbah]').trigger("chosen:updated");
					$('select[name=jenis_limbah]').val(response.jenis_limbah);
                    $('input[name=tgl]').val($(this).format_date(response.tgl));
					$('input[name=tgl_angkut]').val($(this).format_date(response.tgl_angkut));
                    //$('input[name=nama_teknik]').val(response.nama_teknik);
					$('input[name=karakteristik_limbah]').val(response.karakteristik_limbah);
					$('input[name=kode_limbah]').val(response.kode_limbah);
					$('input[name=kelompok_kemasan]').val(response.kelompok_kemasan);
					$('input[name=satuan_ukuran]').val(response.satuan_ukuran);
					$('input[name=total_kemasan]').val(response.total_kemasan);
					$('textarea[name=ket_tambahan]').val(response.ket_tambahan);
					$('textarea[name=tujuan]').val(response.tujuan);

                    $('#manifest_data form').attr('action', '<?php echo base_url('/backend/manifest/edit') ?>'); //this fails silently
                    $('#manifest_data form').get(0).setAttribute('action', '<?php echo base_url('/backend/manifest/edit') ?>'); //this works
					
                    $('#manifest_data').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
		
		$.validator.setDefaults({ ignore: ":hidden:not(select)" });
        $("#form_add").validate({
            rules: {
                nama_industri: {
                    required: true
                },
				pengangkut_limbah: {
                    required: true
                },
				pengolah_limbah: {
                    required: true
                },
                tgl:{
                    required: true,
                    maxlength: 100
                },
                jenis_limbah: {
                    required: true
                },
                nama_teknik: {
                    required: true,
                    maxlength: 255
                },
				kode_limbah: {
                    required: true
                },
				kelompok_kemasan: {
                    required: true,
                    maxlength: 100
                },
				satuan_ukuran: {
                    required: true,
                    maxlength: 100
                },
				total_kemasan: {
                    required: true,
                    maxlength: 100
                },
				ket_tambahan: {
                    required: true
                },
				tujuan: {
                    required: true
                },
				no_truck: {
                    required: true,
                    maxlength: 100
                },
				tgl_angkut: {
                    required: true
                }
            },
            messages: {        
                nama_industri: {
                    required: "Field belum dipilih."
                },
				pengangkut_limbah: {
                    required: "Field belum dipilih."
                },
				pengolah_limbah: {
                    required: "Field belum dipilih."
                },
                tgl: {
                    required: "Field tidak boleh kosong."
                },
                jenis_limbah: {
                    required: "Field tidak boleh kosong."
                },
                nama_teknik: {
					required: "Field tidak boleh kosong."
                },
				kode_limbah: {
                    required: "Field tidak boleh kosong."
                },
				kelompok_kemasan: {
					required: "Field tidak boleh kosong."
                },
				satuan_ukuran: {
					required: "Field tidak boleh kosong."
                },
				total_kemasan: {
					required: "Field tidak boleh kosong."
                },
				ket_tambahan: {
                    required: "Field tidak boleh kosong."
                },
				tujuan: {
                    required: "Field tidak boleh kosong."
                },
				no_truck: {
                    required: "Field tidak boleh kosong."
                },
				tgl_angkut: {
                    required: "Field tidak boleh kosong."
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
		
		$.fn.detail = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/manifest/get'); ?>",
                data: 'id_manifest=' + id,
                success: function (response) {
                    
                    $('#data_pengiriman form')[0].reset();
                    $('#data_pengiriman form input[type=hidden]').val('');

                    $('input[id=edit_id]').val(response.id_manifest);
                    $('select[name=penghasil_limbah]').val(response.id_penghasil_limbah);
					$('select[name=pengangkut_limbah]').val(response.id_pengangkut_limbah);
					$('select[name=pengolah_limbah]').val(response.id_pengolah_limbah);
					$('select[name=jenis_limbah]').val(response.jenis_limbah);
                    $('input[name=tgl]').val($(this).format_date(response.tgl));
					$('input[name=tgl_angkut]').val($(this).format_date(response.tgl_angkut));
                    //$('input[name=nama_teknik]').val(response.nama_teknik);
					$('input[name=karakteristik_limbah]').val(response.karakteristik_limbah);
					$('input[name=kode_limbah]').val(response.kode_limbah);
					$('input[name=kelompok_kemasan]').val(response.kelompok_kemasan);
					$('input[name=satuan_ukuran]').val(response.satuan_ukuran);
					$('input[name=total_kemasan]').val(response.total_kemasan);
					$('textarea[name=ket_tambahan]').val(response.ket_tambahan);
					$('textarea[name=tujuan]').val(response.tujuan);

                    $('#data_pengiriman form').attr('action', '<?php echo base_url('/backend/manifest/edit') ?>'); //this fails silently
                    $('#data_pengiriman form').get(0).setAttribute('action', '<?php echo base_url('/backend/manifest/edit') ?>'); //this works
					
                    $('#data_pengiriman').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
		
		$.fn.upload_berkas = function(id) {
            $('#upload_id').val(id);
            $('#upload').modal('show');
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        
		
        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="manifest_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Manifest</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/manifest/register') ?>" method="post" id="form_add">
                <div class="modal-body">   
					<label>Dokumen Limbah B3 - Bagian Penghasil Limbah B3</label>
					<div class="row">
						<div class="col-lg-8 form-group nama_industri">
                            <label>Penghasil Limbah</label>                        
                            <select name="nama_industri" id="nama_industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                <option value="Empty">&nbsp;</option>
                                <?php
                                    foreach ($industri as $key => $value) {
                                        echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
						<div class="col-lg-4 form-group tgl">
							<label>Tanggal</label>
							<div class="input-group input-group-sm">
								<input type="text" name="tgl" class="form-control input-sm datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
								<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
							</div>							
						</div>
					</div>
					 <div class="row">
						<div class="col-lg-12 form-group jenis_limbah">
							<label>Jenis Limbah B3</label>
							<select name="jenis_limbah" id="jenis_limbah" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
								<?php foreach($data_acuan as $uk): ?>
									<option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
								<?php endforeach;?>                             
							</select>						             
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 form-group karakteristik_limbah">
							<label>Karakteristik Limbah</label>
							<input type="text" name="karakteristik_limbah" id="karakteristik_limbah" class="form-control input-sm has-feedback" autofocus />							             
						</div>
						<div class="col-lg-6 form-group kode_limbah">
							<label>Kode Limbah B3</label>
							<input type="text" name="kode_limbah" id="kode_limbah" class="form-control input-sm has-feedback" autofocus />							             
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5 form-group kelompok_kemasan">
							<label>Kelompok Kemasan</label>
							<input type="text" name="kelompok_kemasan" id="kelompok_kemasan" class="form-control input-sm has-feedback" autofocus />							             
						</div>
						<div class="col-lg-4 form-group satuan_ukuran">
							<label>Satuan Ukuran</label>
							<input type="text" name="satuan_ukuran" id="satuan_ukuran" class="form-control input-sm has-feedback" autofocus />							             
						</div>
						<div class="col-lg-3 form-group total_kemasan">
							<label>Total Kemasan</label>
							<input type="text" name="total_kemasan" id="total_kemasan" class="form-control input-sm has-feedback" autofocus />							             
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 form-group ket_tambahan">
							<label>Keterangan Tambahan</label>
							<textarea rows="2" name="ket_tambahan" id="ket_tambahan" class="form-control input-sm has-feedback" autofocus ></textarea>							             
						</div>
						<div class="col-lg-6 form-group tujuan">
							<label>Tujuan Pengangkutan</label>
							<textarea rows="2" name="tujuan" id="tujuan" class="form-control input-sm has-feedback" autofocus ></textarea>							             
						</div>
					</div>
					<label>Dokumen Limbah B3 - Bagian Pengangkut Limbah B3</label>
					<div class="row">
						<div class="col-lg-8 form-group pengangkut_limbah">
                            <label>Pengangkut Limbah</label>                        
                            <select name="pengangkut_limbah" id="pengangkut_limbah" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                <option value="Empty">&nbsp;</option>
                                <?php
                                    foreach ($pengangkut as $key => $value) {
                                        echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
						<div class="col-lg-4 form-group tgl_angkut">
							<label>Tanggal Pengangkutan</label>
							<div class="input-group input-group-sm">
								<input type="text" name="tgl_angkut" class="form-control input-sm datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
								<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
							</div>							
						</div>
					</div>
					<label>Dokumen Limbah B3 - Bagian Pengolah Limbah B3</label>
					<div class="form-group pengolah_limbah">
                            <label>Pengolah Limbah</label>                        
                            <select name="pengolah_limbah" id="pengolah_limbah" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                                <option value="Empty">&nbsp;</option>
                                <?php
                                    foreach ($pengolah as $key => $value) {
                                        echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="data_pengiriman" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" style="width:700px !important;">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Pengiriman Limbah</h4>
            </div>
            <form role="form" method="post" id="form_add">
                <div class="modal-body">   
					<div class="row">
						<div class="col-lg-8 form-group penghasil_limbah">
							<label>Penghasil Limbah</label>
							<select disabled name="penghasil_limbah" id="penghasil_limbah" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Penghasil Limbah --</option>
								<?php foreach($industri as $uk): ?>
									<option value="<?php echo $uk->id_industri;?>"><?php echo $uk->badan_hukum; ?>&nbsp;<?php echo $uk->nama_industri; ?></option>
								<?php endforeach;?>                             
							</select>
						</div>
						<div class="col-lg-4 form-group tgl">
							<label>Tanggal</label>
							<div class="input-group input-group-sm">
								<input disabled  type="text" name="tgl" class="form-control input-sm datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
								<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
							</div>							
						</div>
					</div>
					 <div class="row">
						<div class="col-lg-12 form-group jenis_limbah">
							<label>Jenis Limbah B3</label>
							<select disabled name="jenis_limbah" id="jenis_limbah" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Jenis Limbah B3--</option>
								<?php foreach($data_acuan as $uk): ?>
									<option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
								<?php endforeach;?>                             
							</select>						             
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6 form-group karakteristik_limbah">
							<label>Karakteristik Limbah</label>
							<input disabled type="text" name="karakteristik_limbah" id="karakteristik_limbah" class="form-control input-sm has-feedback" autofocus />							             
						</div>
						<div class="col-lg-6 form-group kode_limbah">
							<label>Kode Limbah B3</label>
							<input disabled type="text" name="kode_limbah" id="kode_limbah" class="form-control input-sm has-feedback" autofocus />							             
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5 form-group kelompok_kemasan">
							<label>Kelompok Kemasan</label>
							<input disabled type="text" name="kelompok_kemasan" id="kelompok_kemasan" class="form-control input-sm has-feedback" autofocus />							             
						</div>
						<div class="col-lg-4 form-group satuan_ukuran">
							<label>Satuan Ukuran</label>
							<input disabled type="text" name="satuan_ukuran" id="satuan_ukuran" class="form-control input-sm has-feedback" autofocus />							             
						</div>
						<div class="col-lg-3 form-group total_kemasan">
							<label>Total Kemasan</label>
							<input disabled type="text" name="total_kemasan" id="total_kemasan" class="form-control input-sm has-feedback" autofocus />							             
						</div>
					</div>
					<div class="form-group ket_tambahan">
						<label>Keterangan Tambahan</label>
						<textarea disabled rows="4" name="ket_tambahan" id="ket_tambahan" class="form-control input-sm has-feedback" autofocus ></textarea>						             
					</div>
					<div class="form-group tujuan">
						<label>Tujuan Pengangkutan</label>
						<textarea disabled  rows="4" name="tujuan" id="tujuan" class="form-control input-sm has-feedback" autofocus ></textarea>						             
					</div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="upload" role="dialog" aria-labelledby="uploadLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="uploadLabel">Unggah Berkas Manifest</h4>
            </div>
            <form action="<?php echo base_url('/backend/manifest/upload'); ?>" method="post" enctype="multipart/form-data">
				    <div class="modal-body">   
                        <div class="form-group">
                        <label>Upload Berkas</label>
                            <div class="input-group input-group-sm">
                                <input type="text" readonly="" class="form-control">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        Browse� <input type="file" name="userfile" />
                                    </span>
                                </span>
                            </div><!-- /.input-group -->
                        </div>
                    </div>
					<div class="modal-footer">
						<input type="hidden" id="upload_id" name="id" value="" />
						<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
						<button type="submit" name="do_upload" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Unggah</button>
					</div>
				</div>
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data Manifest</h4>
            </div>
            <form action="<?php echo base_url('/backend/manifest/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->