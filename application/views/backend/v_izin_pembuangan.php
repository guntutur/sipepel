<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">IZIN PEMBUANGAN AIR LIMBAH&nbsp;&nbsp;<small>mengelola seluruh data izin pembuangan air limbah</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Izin Pembuangan Air Limbah</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <!-- button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/>   -->

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#izin_pembuangan" onclick="$(this).reset_form('izin_pembuangan', true, 'koor_outlet');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th>Perizinan</th>                                
								<th>Debit (m3)</th>
								<th>Koordinat Outlet</th>
								<th>Keterangan</th>
								<th>Status</th>
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/izin_pembuangan/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var status = (dt.status == 1) ? 'Aktif': 'Tidak Aktif';
						var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : '-';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
						t += '<td>';
                        var nama = (dt.badan_hukum) ? dt.badan_hukum+' ' : '';
						var dasar = (dt.dasar_hukum) ? ' Ada' : ' Tidak Ada';
						t += '<b>'+nama+dt.nama_industri + '</b><br/> <small>Usaha/Kegiatan: ';
						t += dt.usaha_kegiatan + '<br/> Jenis/Kelas/Tipe : ';
						t += jenis_industri + '</small>';
                        t += '</td>';
                        t += '<td>No Izin: ';
						t += dt.no_izin + '<br/> Tgl Izin: ';
						var nowTemp = $(this).format_date(dt.tgl_izin);                        
						t += nowTemp + '<br/> Tentang: ';
                        t += dt.tentang + '';
                        t += '</td>';
						t += '<td>Perhari: ';
						t += dt.debit_perhari + '<br/> Perbulan: ';
						t += dt.debit_perbulan + '';
                        t += '</td>';
						t += '<td>S : '+dt.derajat_s+'&deg;'+dt.jam_s+'&#39;'+dt.menit_s+'&quot;<br>';
						t += 'E : '+dt.derajat_e+'&deg;'+dt.jam_e+'&#39;'+dt.menit_e+'&quot;';
						t += '</td>'
						t += '<td>Sumber Air Limbah : ';
						if(dt.proses.length > 0) {
							$.each(dt.proses, function(k, v) {  
								t += '' + v + ', ';
							});
						}
						t += '<br/> Badan Air Penerima : ';
						t += dt.badan_air_penerima + '<br/> Baku Mutu Izin : ';
						t += dasar + '<br> Jumlah Baku Mutu :';
						t += dt.jumlah + '<br>';
                        t += '</td>';
						t += '<td>'+status+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
						t += '<li><a href="' + '<?php echo base_url("backend/izin_pembuangan/detail/'+ dt.id_pembuangan + '"); ?>' + '">Tambah Baku Mutu</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_pembuangan+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_pembuangan+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }
	});

	$(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }
		
        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/izin_pembuangan/get'); ?>",
                data: 'id_izin_pembuangan=' + id,
                success: function (response) {

					$('#izin_pembuangan form')[0].reset();
                    $('#izin_pembuangan form input[type=hidden]').val('');
					
					$('input[id=edit_id]').val(response.id_pembuangan);
					$('select[name=status]').val(response.status);

                    // $('input[name=nama_industri]').val(response.nama_industri);
                    $('select[name=industri]').chosen();
                    $('select[name=industri]').val(response.id_industri);
                    $('select[name=industri]').trigger("chosen:updated");

                    $('input[name=no_izin]').val(response.no_izin);
                    $('input[name=tgl_izin]').val($(this).format_date(response.tgl_izin));
                    $('textarea[name=tentang]').val(response.tentang);
                    $('input[name=debit_perhari]').val(response.debit_perhari);
                    $('input[name=debit_perbulan]').val(response.debit_perbulan);
					$('select[id=proses]').val(response.proses);
					$('select[name=badan_air_penerima]').val(response.badan_air_penerima);
					$('input[name=baku_mutu_pembuangan]').val(response.baku_mutu_pembuangan);
					$('input[name=derajat_s]').val(response.derajat_s);
					$('input[name=jam_s]').val(response.jam_s);
					$('input[name=menit_s]').val(response.menit_s);
					$('input[name=derajat_e]').val(response.derajat_e);
					$('input[name=jam_e]').val(response.jam_e);
					$('input[name=menit_e]').val(response.menit_e);
					$('textarea[name=dasar_hukum]').val(response.dasar_hukum);

                    $('#izin_pembuangan form').attr('action', '<?php echo base_url('/backend/izin_pembuangan/edit') ?>'); //this fails silently
                    $('#izin_pembuangan form').get(0).setAttribute('action', '<?php echo base_url('/backend/izin_pembuangan/edit') ?>'); //this works
					
                    $('#izin_pembuangan').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }

		$.validator.setDefaults({ ignore: ":hidden:not(select)" });
        $("#form_add").validate({
            rules: {
				status: {
                    required: true
                },
                industri: {
                    required: true
                },
				tgl_izin: {
                    required: true
                },
                no_izin:{
                    required: true,
                    maxlength: 100
                },
                tentang: {
                    required: true
                },
                debit_perhari: {
                    required: true
                },
				debit_perbulan: {
                    required: true
                },
				proses:{
                    required: true
                },
				koordinat_outlet_s: {
                    required: true,
                    maxlength: 100
                },
				koordinat_outlet_e: {
                    required: true,
                    maxlength: 100
                },
				badan_air_penerima: {
                    required: true
                },
				lama_pembuangan: {
                    required: true
                },
				baku_mutu_pembuangan: {
                    required: true
                }
				
            },
            messages: {     
				status: {
                    required: "Silahkan pilih data"
                },
                industri: {
                    required: "Silahkan pilih data industri"
                },
				 tgl_izin: {
                    required: "Tanggal belum dipilih"
                },
                no_izin: {
                    required: "Masukan nomor izin",
                    maxlength: "Nama nomer izi maksimal memiliki 100 karakter"
                },
                tentang: {
                    required: "Masukan tentang."
                },
                debit_perhari: {
                    required: "Masukan debit perhari."
                },
				debit_perbulan: {
                    required: "Masukan ldebit perbulan."
                },
				proses: {
                    required: "Silahkan pilih data"
                },
				koordinat_outlet_s: {
                    required: "Masukan koordinat outlet S.",
                    maxlength: "Koordinat Outlet S maksimal memiliki 100 karakter"
                },
				koordinat_outlet_e: {
                    required: "Masukan koordinat outlet E.",
                    maxlength: "Koordinat Outlet E maksimal memiliki 100 karakter"
                },
				badan_air_penerima: {
                    required: "Silahkan pilih data"
                },
				lama_pembuangan: {
                    required: "Masukan lama pembuangan."
                },
				baku_mutu_pembuangan: {
                    required: "Masukan baku mutu."
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

		var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search',
            onSearchStart: function (query) {
                $('#industri').val(null);              
            },
            onSelect: function (suggestion) {
                $('#industri').val(suggestion.data);
            }
        });

        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="izin_pembuangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Izin Pembuangan Air Limbah</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/izin_pembuangan/register') ?>" method="post" id="form_add">
                <div class="modal-body">                    
                    <!-- <div class="row">
                        <div class="col-lg-5 form-group no_izin">
                            <label>No. Izin</label>
                            <input type="text" name="no_izin" id="no_izin" class="form-control input-sm has-feedback" placeholder="Nomor Izin" autofocus />                                
                        </div>
						<div class="col-lg-7 form-group nama_industri">
                            <label>Nama Usaha/Kegiatan</label>
							<input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
							<input type="hidden" id="industri" name="industri" />  
                        </div>                       
                    </div> -->
                    <div class="form-group tentang">
                        <label>Nomor Izin</label>
                        <input type="text" name="no_izin" id="no_izin" class="form-control input-sm has-feedback" placeholder="Nomor Izin" autofocus />
                    </div>
                    <div class="form-group tentang">
                        <label>Nama Usaha/Kegiatan</label>                        
                        <select name="industri" id="industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                            <option value="Empty">&nbsp;</option>
                            <?php
                                foreach ($industri as $key => $value) {
                                    echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                }
                            ?>
                        </select>
                    </div>
					<div class="row">
						<div class="col-lg-6" style="padding-right: 5px;">
                            <label>Tanggal Izin</label>
                            <div class="input-group input-group-sm">
                                <input type="text" name="tgl_izin" class="form-control input-sm datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
						<div class="col-lg-6 form-group proses">
							<label>Sumber Air Limbah</label>
							<select name="proses[]" id="proses" multiple="multiple" class="form-control input-sm" size="4">
								<option value="" disabled>-- Pilih Sumber Air Limbah --</option>
                                <?php                                     
                                    foreach($data_acuan_proses as $k => $s) {
                                        echo '<option value="'.$s->ket.'" class="">'.$s->ket.'</option>';
                                    }
                                ?>                              
							</select>							   
						</div>
					</div>
					<div class="form-group tentang">
                        <label>Tentang</label>
                        <textarea rows="2" name="tentang" id="tentang" class="form-control input-sm has-feedback" autofocus placeholder="Tentang"></textarea>                        
                    </div>
					<div class="form-group status">
						<div class="row">
							<div class="col-lg-6">
								<label>Debit Perhari</label>
								<div class="input-group input-group-sm">                                
									<input type="text" name="debit_perhari" id="debit_perhari" class="form-control numberonly" placeholder="0" />
									<span class="input-group-addon">m3</span>
								</div> 
							</div>
							<div class="col-lg-6">
								<label>Debit Perbulan</label>
								<div class="input-group input-group-sm">                                
									<input type="text" name="debit_perbulan" id="debit_perbulan" class="form-control numberonly" placeholder="0" />                                
									<span class="input-group-addon">m3</span>
								</div> 
							</div>
						</div>
					</div>					
                    <div class="form-group status">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Badan Air Penerima</label> 
								<select name="badan_air_penerima" id="badan_air_penerima" class="form-control input-sm">
									<option value="" disabled selected>-- Pilih Badan Air Penerima--</option>
									<?php foreach($data_acuan_badan_air as $uk): ?>
										<option value="<?php echo $uk->ket;?>"><?php echo $uk->ket; ?></option>
									<?php endforeach;?>                             
								</select>
                            </div>
							<div class="col-lg-6" style="">
								<label>Status Izin</label>
								<select name="status" id="status" class="form-control input-sm">
									<option value="" disabled selected>-- Pilih Status --</option>
									<?php 
										$status = $this->functions->get_status();
										foreach($status as $k => $s) {
											echo '<option value="'.$k.'">'.$s.'</option>';
										}
									?>                      
								</select>						
							</div>
                        </div>
                    </div> 
					<div class="row">
						<div class="col-lg-6">
							<label>Koordinat Outlet S</label>
							<div class="form-group status">
								<div class="row">
									<div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="derajat_s" id="derajat_s" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">&deg;</span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding: 0; margin:0;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="jam_s" id="jam_s" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">'</span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="menit_s" id="menit_s" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">"</span>
										</div> 
									</div> 
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<label>Koordinat Outlet E</label>
							<div class="form-group status">
								<div class="row">
									<div class="col-lg-4" style="padding-right:0px; margin-right:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="derajat_e" id="derajat_e" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">&deg;</span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding: 0; margin:0;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="jam_e" id="jam_e" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">'</span>
										</div> 
									</div> 
									<div class="col-lg-4" style="padding-left:0px; margin-left:0px;">                                
										<div class="input-group input-group-sm">                                
											<input type="text" name="menit_e" id="menit_e" class="form-control input-sm has-feedback" placeholder="0" autofocus />                                
											<span class="input-group-addon">"</span>
										</div> 
									</div> 
								</div>
							</div>
						</div>
					</div>
					 <div class="row">
                        <div class="col-lg-12 form-group dasar_hukum">
                            <label>Dasar Hukum Baku Mutu</label>                                     
                            <textarea rows="2" name="dasar_hukum" id="dasar_hukum" class="form-control input-sm has-feedback" placeholder="Dasar Hukum"></textarea> 
                        </div>
					</div>
                    <!--<div class="form-group status">
                        <label>Koordinat Outlet (S), (E) : (12D:12:34,25:23), (10D:11:32,40:13)</label>
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="koor_outlet"></div>                                
                            </div>                            
                        </div>
                    </div>-->                                                                 
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Izin Pembuangan Air Limbah</h4>
            </div>
            <form action="<?php echo base_url('/backend/izin_pembuangan/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->