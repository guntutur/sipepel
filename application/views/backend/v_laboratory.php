<!--
===========================================================
BEGIN PAGE
===========================================================
-->

<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">LABORATORIUM&nbsp;&nbsp;<small>mengelola seluruh data laboratorium</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Laboratorium</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <!-- <th width="5%">#</th> -->
                                <th>Nama Laboratorium</th>
								<th width="20%">Nomor Laboratorium</th>
								<th width="20%">Nomor Akreditasi Lab</th>
								<th>Jenis Laboratorium</th>
								<th  width="20%">Kontak Kami</th>                              
                                <th class="text-right" width="5%"></th>									
                            </tr>
                        </thead>
                        <tbody id="document-data">
                            <tr><td colspan="6">Data tidak ditemukan</td></tr>
                        </tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start"></span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">

    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/laboratorium/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    // console.log(d);
                    $('#pagination').html(d.pagination);
                    $('#start').text(d.data.length);            
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {};
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
                        t += '<tr>';
						t += '<td><b>'+dt.nama_lab+'</b><br/><small>'+dt.alamat+'</small></td>';
                        t += '<td>'+dt.no_lab+'</td>';
						t += '<td>'+dt.no_akreditasi+'</td>';
						t += '<td>'+dt.kategori_lab+'</td>';
                        t += '<td><i class="fa fa-phone-square icon-sidebar"></i>'+dt.tlp+'<br/><i class="fa fa-fax icon-sidebar"></i>'+dt.fax+'<br/><i class="fa fa-envelope icon-sidebar"></i>'+dt.email+'</td>';                        
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
                        t += '<li><a onclick="$(this).edit('+dt.id_lab+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_lab+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';                         
                        t += '</tr>';
                        // t += "<tr><td>" + dt.no_lab + "</td>" + "<td>" + dt.nama_lab + "</td>" + "<td>" + dt.tlp + "</td>";
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    }
	

    $(document).ready(function() {

        var validator = $("#form_add").validate({
            rules: {
                lab_name: {
                    required: true,
                    maxlength: 250
                },
                no_lab: {
                    required: true,
                    maxlength: 100
                },
                phone:{
                    required: true,
                    maxlength: 20
                },
                email: {
                    required: true,
                    maxlength: 90
                },
                address: {
                    required: true,
                    maxlength: 250
                },
                fax: {
                    required: true,
                    maxlength: 9
                },
				kategori_lab: {
                    required: true
                }
            },
            messages: {        
                lab_name: {
                    required: "Nama laboratorium tidak boleh kosong.",
                    minlength: "Nama laboratorium harus lebih dari 3 karakter.",
                    maxlength: "Nama laboratorium tidak boleh lebih dari 250 karakter."
                },
                no_lab: {
                    required: "Nomor laboratorium tidak boleh kosong.",
                    minlength: "Nomor laboratorium harus lebih dari 3 karakter.",
                    maxlength: "Nomor laboratorium tidak boleh lebih dari 100 karakter."
                },
                phone: {
                    required: "Nomor telepon tidak boleh kosong.",
                    minlength: "Nomor telepon harus lebih dari 3 karakter.",
                    maxlength: "Nomor telepon tidak boleh lebih dari 20 karakter."
                },
                email: {
                    required: "Alamat email tidak boleh kosong.",
                    minlength: "Alamat email harus lebih dari 3 karakter.",
                    maxlength: "Alamat email tidak boleh lebih dari 90 karakter."
                },
                address: {
                    required: "Alamat lengkap tidak boleh kosong.",
                    minlength: "Alamat lengkap harus lebih dari 3 karakter.",
                    maxlength: "Alamat lengkap tidak boleh lebih dari 250 karakter."
                },
                fax: {
                    required: "Nomor fax tidak boleh kosong.",
                    minlength: "Nomor fax  harus lebih dari 3 karakter.",
                    maxlength: "Nomor fax  tidak boleh lebih dari 12 karakter."
                },
				kategori_lab: {
                    required: "Silahkan pilih data"
                },
            }
        });

        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }    

        $.fn.edit = function(id) { 
            validator.resetForm();              
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/laboratorium/get'); ?>",
                data: 'id_lab=' + id,
                success: function (response) {
                    console.log(response);
                    $('input[id=edit_id]').val(response.id_lab);
                    $('input[name=lab_name]').val(response.nama_lab);
                    $('input[name=no_lab]').val(response.no_lab);
					$('input[name=no_akreditasi]').val(response.no_akreditasi);
                    $('input[name=phone]').val(response.tlp);
                    $('input[name=email]').val(response.email);
                    $('textarea[name=address]').val(response.alamat);
                    $('input[name=fax]').val(response.fax);
					$('select[name=kategori_lab]').val(response.kategori_lab);

                    $('#myModal').modal('show');
                }
            })
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }        

        Document.search();
    });
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Laboratorium</h4>
            </div>
            <form role="form" data-action="<?php echo base_url('/backend/laboratorium') ?>" method="post" id="form_add">
                <div class="modal-body">																			
                    <div class="form-group lab_name">
                        <label>Nama Laboratorium</label>
                        <input type="text" name="lab_name" id="lab_name" class="form-control input-sm has-feedback" autofocus />                        	
                    </div>
					<div class="row">
						<div class="col-lg-6 form-group no_lab">
							<label>Nomor Laboratorium</label>
							<input type="text" name="no_lab" id="no_lab" class="form-control input-sm has-feedback" autofocus />                        
						</div>
						<div class="col-lg-6 form-group no_akreditasi">
							<label>Nomor Akreditasi</label>
							<input type="text" name="no_akreditasi" id="no_akreditasi" class="form-control input-sm has-feedback" autofocus />                        
						</div>						
					</div>
					<div class="form-group kategori_lab">
						<label>Jenis Laboratorium</label>
						<select name="kategori_lab" id="kategori_lab" class="form-control input-sm">
							<option value="" disabled selected>-- Pilih Jenis Lab --</option>
							<?php 
								$jenis_lab = $this->functions->get_jenis_lab();
								foreach($jenis_lab as $k => $s) {
									echo '<option value="'.$k.'">'.$s.'</option>';
								}
							?>                      
						</select>                        
					</div>
					<div class="row">
						<div class="col-lg-6 form-group phone">
							<label>Telepon</label>
							<input type="text" name="phone" id="phone" class="form-control input-sm has-feedback numberonly" autofocus />							
						</div>
						<div class="col-lg-6 form-group fax">
							<label>Fax</label>
							<input type="text" name="fax" id="fax" class="form-control input-sm has-feedback numberonly" autofocus />							
						</div>			
					</div>
					<div class="form-group email">
                        <label>E-mail</label>
                        <input type="text" name="email" id="email" class="form-control input-sm has-feedback" autofocus />                        
                    </div>
                    <div class="form-group address">
                        <label>Alamat</label>
                        <textarea name="address" id="address" class="form-control input-sm has-feedback" autofocus rows="4"></textarea>                        
                    </div>  									  						
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Laboratorium</h4>
            </div>
            <form action="<?php echo base_url('/backend/laboratorium/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin Ingin Menghapus Data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php # $this->load->view('include/footer'); ?>