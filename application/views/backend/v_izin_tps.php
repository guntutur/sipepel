<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading">IZIN TPS LIMBAH B3&nbsp;&nbsp;<small>mengelola seluruh data izin TPS limbah B3</small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Manajemen Data</li>
                <li class="active">Izin TPS Limbah B3</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">

                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" data-toggle="modal" data-target="#izin_tps" onclick="$(this).reset_form('izin_tps');" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>        
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>          

                <?php echo $this->session->flashdata('msg'); ?>

                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%">#</th>
                                <th>Nama Usaha/Kegiatan</th>
                                <th>Perizinan</th>                                
								<th>Tentang</th>
								<th>Jumlah TPS</th>  
								<th>Status</th>								
                                <th class="text-right" width="5%"></th>                                    
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
            
        </div><!-- /.container-fluid -->                
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->        

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
    var Document = {
        param: {
            dataperpage: 25,
            query: '',
            curpage: 0,
            numpage: 0
        },
        url: '<?php echo base_url("backend/izin_tps/get_list"); ?>',
        search: function() {
            this.param.query = $('#query').val();
            this.param.curpage = 0;
            this.load_data();
            return false;
        },
        search_field: function(e) {

        },
        set_page: function(n) {
            this.param.curpage = n;
            this.load_data();
            return false;
        },
        prev_page: function() {
            if(this.param.curpage > 0) {
                this.param.curpage--;
                this.load_data();
            }
            return false;
        },
        next_page: function() {
            if(this.param.curpage < this.param.numpage) {
                this.param.curpage++;
                this.load_data();
            }
            return false;
        },
        load_data: function() {
            $.ajax({
                url: Document.url,
                type: 'POST',
                dataType: 'json',
                data: $.param(Document.param),
                success: function(d) {
                    console.log(d);
                    $('#pagination').html(d.pagination);
                    $('#start').text(d.data.length);
                    $('#nums').text(d.total);
                    Document.param.numpage = d.numpage;
                    var t = '', dt = {}, no = 0;
                    for (var i = 0; i < d.data.length; i++) {
                        dt = d.data[i];
						var status = (dt.status == 1) ? 'Aktif': 'Tidak Aktif';
						var jenis_industri = (dt.nama_jenis_industri != null ) ? dt.nama_jenis_industri : ' ';
                        t += '<tr>';
                        t += '<td class="text-center">'+(no+=1)+'</td>';
						t += '<td>';
                        var nama = (dt.badan_hukum) ? dt.badan_hukum+' ' : '';
						t += '<b>'+nama+dt.nama_industri + '</b><br/><small>Usaha/Kegiatan: ';
						t += dt.usaha_kegiatan + '<br/> Jenis/Kelas/Tipe: ';
						t += jenis_industri + '</small>';
                        t += '</td>';
                        t += '<td>No Izin: ';
						t += dt.no_izin + '<br/> Tgl Izin: ';
						var nowTemp = $(this).format_date(dt.tgl_izin);                        
						t += nowTemp + '';
                        t += '</td>';
						t += '<td>'+ dt.tentang + '</td>';
						t += '<td>'+ ((dt.jumlah) ? dt.jumlah : 0)+'</td>';
						t += '<td>'+status+'</td>';
                        t += '<td class="text-right">';
                        t += '<div class="btn-group">';
                        t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                        t += '<ul class="dropdown-menu pull-right" role="menu">';
						t += '<li><a href="' + '<?php echo base_url("backend/izin_tps/detail/'+ dt.id_izin_tps + '"); ?>' + '">Tambah Data TPS</a></li>';
                        t += '<li class="divider"></li>';
                        t += '<li><a onclick="$(this).edit('+dt.id_izin_tps+')">Ubah Data</a></li>';
                        t += '<li><a onclick="$(this).delete('+dt.id_izin_tps+')">Hapus Data</a></li>';
                        t += '</ul></div></td>';
                        t += '</tr>';                        
                    }
                    
                    $('#document-data').html(t);
                }
            })
            .fail(function(e) {
                console.log(e);
            });            
        }
    } 

    $(document).ready(function () {
        $.fn.search = function(e, v) {
            if(e.keyCode == 13 || v.value == '') {
                Document.search();                
            }
        }
	});

	$(function() {
        if($('.datepickercustom').length > 0) {

            var month = '<?php echo $this->uri->segment(5); ?>';
            var year = '<?php echo $this->uri->segment(6); ?>';

            var firstDay = new Date(parseInt(year), parseInt(month) - 1, 1);
            var lastDay = new Date(parseInt(year), parseInt(month), 0);

            $('.datepickercustom').datepicker({startDate: firstDay, endDate: lastDay, autoclose: true})
        }
		
        $.fn.edit = function(id) {            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "<?php echo base_url('/backend/izin_tps/get'); ?>",
                data: 'id_izin_tps=' + id,
                success: function (response) {
                    
                    $('#izin_tps form')[0].reset();
                    $('#izin_tps form input[type=hidden]').val('');

                    $('input[id=edit_id]').val(response.id_izin_tps);

                    // $('select[name=nama_industri]').val(response.id_industri);
                    $('select[name=industri]').chosen();
                    $('select[name=industri]').val(response.id_industri);
                    $('select[name=industri]').trigger("chosen:updated");

                    $('input[name=no_izin]').val(response.no_izin);
					$('select[name=status]').val(response.status);
                    $('input[name=tgl_izin]').val($(this).format_date(response.tgl_izin));
                    $('textarea[name=tentang]').val(response.tentang);
                    $('input[name=jenis_limbah_b3]').val(response.jenis_limbah_b3);
                    $('input[name=lama_penyimpanan]').val(response.lama_penyimpanan);
					$('input[name=jumlah_tps]').val(response.jumlah_tps);
					$('input[name=koordinat_tps_s]').val(response.koordinat_tps_s);
					$('input[name=koordinat_tps_e]').val(response.koordinat_tps_e);
					
                    $('#izin_tps form').attr('action', '<?php echo base_url('/backend/izin_tps/edit') ?>'); //this fails silently
                    $('#izin_tps form').get(0).setAttribute('action', '<?php echo base_url('/backend/izin_tps/edit') ?>'); //this works
					
                    $('#izin_tps').modal('show');
                }
            })
            .fail(function() {
                console.log("error");
            }); 
        }
        
        $.fn.delete = function(id) {
            $('input[id=deleted_id]').val(id);
            $('#delete').modal('show');
        }  

		$.validator.setDefaults({ ignore: ":hidden:not(select)" });
        $("#form_add").validate({
            rules: {
				status: {
                    required: true
                },
                industri: {
                    required: true
                },
				tgl_izin: {
                    required: true
                },
                no_izin:{
                    required: true,
                    maxlength: 100
                },
                tentang: {
                    required: true
                },
                jenis_limbah_b3: {
                    required: true,
                    maxlength: 255
                },
				lama_penyimpanan: {
                    required: true
                },
				jumlah_tps: {
                    required: true
                },
				koordinat_tps_s: {
                    required: true,
                    maxlength: 100
                },
				koordinat_tps_e: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {        
				status: {
                    required: "Silahkan pilih data"
                },
                industri: {
                    required: "Silahkan pilih data industri."
                },
				tgl_izin: {
                    required: "Tanggal belum dipilih."
                },
                no_izin: {
                    required: "Masukan nomor izin.",
                    maxlength: "Nama nomer izi maksimal memiliki 100 karakter"
                },
                tentang: {
                    required: "Masukan tentang."
                },
                jenis_limbah_b3: {
                    required: "Masukan jenis limbah b3.",
                    maxlength: "Nama usaha/kegiatan maksimal memiliki 250 karakter"
                },
				lama_penyimpanan: {
                    required: "Masukan lama penyimpanan."
                },
				jumlah_tps: {
                    required: "Masukan jumlah tps."
                },
				koordinat_tps_s: {
                    required: "Masukan koordinat tps S.",
                    maxlength: "Koordinat TPS S maksimal memiliki 100 karakter"
                },
				koordinat_tps_e: {
                    required: "Masukan koordinat tps E.",
                    maxlength: "Koordinat TPS E maksimal memiliki 100 karakter"
                }
            },
            errorElement: 'label',
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
		
		var site = "<?php echo site_url(); ?>";
        $('#nama_industri').autocomplete({
            minChars: 2,
            type: 'POST',
            noCache: true,
            serviceUrl: site + 'backend/industri/search',
            onSearchStart: function (query) {
                $('#industri').val(null);              
            },
            onSelect: function (suggestion) {
                $('#industri').val(suggestion.data);
            }
        });

        Document.search();

    });
</script>

<!-- Modal -->
<div class="modal fade" id="izin_tps" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Keluar</span></button>
                <h4 class="modal-title" id="myModalLabel">Data Izin TPS Limbah B3</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/izin_tps/register') ?>" method="post" id="form_add">
                <div class="modal-body">                    
					<!-- <div class="form-group nama_industri">
						<label>Nama Usaha/Kegiatan</label>
						<input type="text" title="Nama Usaha/Kegiatan" value="" class="form-control input-sm" id="nama_industri" name="nama_industri">
						<input type="hidden" id="industri" name="industri" /> 
					</div> -->
                    <div class="form-group tentang">
                        <label>Nama Usaha/Kegiatan</label>                        
                        <select name="industri" id="industri" data-placeholder="Pilih nama usaha/kegiatan..." class="form-control input-sm chosen-select" tabindex="2">
                            <option value="Empty">&nbsp;</option>
                            <?php
                                foreach ($industri as $key => $value) {
                                    echo '<option value="'.$value->id_industri.'">'.$value->nama_industri.'</option>';
                                }
                            ?>
                        </select>
                    </div>
					<div class="row">
						<div class="col-lg-6 form-group no_izin">
							<label>No. Izin</label>
							<input type="text" name="no_izin" id="no_izin" class="form-control input-sm has-feedback" placeholder="Nomor Izin" autofocus />							
						</div>
						<div class="col-lg-3" style="">
                            <label>Tanggal Izin</label>
                            <div class="input-group input-group-sm">
                                <input type="text" name="tgl_izin" class="form-control input-sm datepickercustom" value="" placeholder="dd.mm.yyyy" data-date-format="dd.mm.yyyy" style="margin: 0;">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
						<div class="col-lg-3" style="">
							<label>Status Izin</label>
							<select name="status" id="status" class="form-control input-sm">
								<option value="" disabled selected>-- Pilih Status --</option>
								<?php 
									$status = $this->functions->get_status();
									foreach($status as $k => $s) {
										echo '<option value="'.$k.'">'.$s.'</option>';
									}
								?>                      
							</select>						
						</div>
					</div>
					<div class="form-group tentang">
                        <label>Tentang</label>
                        <textarea rows="4" name="tentang" id="tentang" class="form-control input-sm has-feedback" autofocus placeholder="Tentang"></textarea>                              
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Izin TPS Limbah B3</h4>
            </div>
            <form action="<?php echo base_url('/backend/izin_tps/delete'); ?>" method="post">
                <div class="modal-body">
                    <p>Apakah Anda Yakin akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="deleted_id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Batal</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->