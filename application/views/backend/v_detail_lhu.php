<!--
===========================================================
BEGIN PAGE
===========================================================
-->
<div class="wrapper">

    <?php $this->load->view('include/top_nav'); ?>
    <?php $this->load->view('include/sidebar'); ?>

    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- Begin page heading -->
            <h1 class="page-heading"><?php echo $title; ?>&nbsp;&nbsp;<small></small></h1>
            <!-- End page heading -->

            <!-- Begin breadcrumb -->
            <ol class="breadcrumb default square rsaquo sm">
                <li><a href="<?php echo base_url('/backend/dashboard'); ?>"><i class="fa fa-home"></i></a></li>
                <li>Laporan Hasil Uji</li>
                <li class="active">Air Limbah</li>
            </ol>
            <!-- End breadcrumb -->

            <!-- BEGIN DATA TABLE -->
            <div class="the-box">              

               <!--  <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Tambah Data</button>
                <br/><br/> -->
                <div class="row">
                    <div class="col-lg-6">
                        <button type="button" class="btn btn-sm btn-info" onclick="history.back();"><i class="fa fa-reply"></i>&nbsp;Kembali</button>        
                        <button id="input_parameter" type="button" class="btn btn-sm btn-success" onclick="$(this).detail_lhu(<?php echo $this->uri->segment(4); ?>)"><i class="fa fa-plus-circle"></i>&nbsp;Input Parameter Uji</button>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group input-group-sm">
                            <input type="text" name="query" id="query" class="form-control" onkeyup="$(this).search(event, this);" placeholder="Masukkan kata kunci pencarian .." />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                        </div>
                    </div>
                </div>
                <br/>
                
                <?php echo $this->session->flashdata('msg'); ?>
                <?php 
                    if($this->session->flashdata('error')) {
                        $msg = '';
                        foreach ($this->session->flashdata('error') as $key => $value) {
                            $msg .= $value[2]." (".$value[0].") melebihi baku mutu, seharusnya ".$value[1].'<br/>';
                        }
                        echo $this->functions->build_message('danger', $msg);
                    } 
                ?>

                <div class="sisa"></div>

                <div class="table-responsive">
                    <table class="table table-hover" id="datatable-example">
                        <thead class="the-box dark full">
                            <tr>
                                <th width="5%" class="text-area">#</th>
								<th>Nama Parameter</th>
                                <!-- <th>Satuan</th> -->
                                <th>Nilai Baku Mutu</th>
                                <th>Nilai Hasil Uji</th>                                
                                <th>Metode</th>                                                    
                                <!-- <th class="text-right" width="5%"></th> -->
                            </tr>
                        </thead>
                        <tbody id="document-data"></tbody>
                    </table>
                </div><!-- /.table-responsive -->

                <div class="row">
                    <div class="col-lg-6 showing">Menampilkan <span id="start">10</span> dari <span id="nums"></span> data.</div>
                    <div class="col-lg-6 text-right">
                        <div id="pagination" style="margin: 0!important; padding: 0!important;">
                            <ul class="pagination pagination-sm">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>                   
                            <li><a href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- /.the-box .default -->
            <!-- END DATA TABLE -->
			
        </div><!-- /.container-fluid -->				
    </div><!-- /.page-content -->
</div><!-- /.wrapper -->
<!-- END PAGE CONTENT -->



<!-- BEGIN BACK TO TOP BUTTON -->
<div id="back-top">
    <a href="#top"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END BACK TO TOP -->		

<!--
===========================================================
END PAGE
===========================================================
-->

<script type="text/javascript">
var Document = {
    param: {
        dataperpage: 25,
        query: '',
        curpage: 0,
        numpage: 0,
        id: <?php echo $this->uri->segment(4); ?>
    },
    url: '<?php echo base_url("backend/detail_lhu/get_detail_lhu"); ?>',
    search: function() {
        this.param.query = $('#query').val();
        this.param.curpage = 0;
        this.load_data();
        return false;
    },
    search_field: function(e) {

    },
    set_page: function(n) {
        this.param.curpage = n;
        this.load_data();
        return false;
    },
    prev_page: function() {
        if(this.param.curpage > 0) {
            this.param.curpage--;
            this.load_data();
        }
        return false;
    },
    next_page: function() {
        if(this.param.curpage < this.param.numpage) {
            this.param.curpage++;
            this.load_data();
        }
        return false;
    },
    load_data: function() {
        $.ajax({
            url: Document.url,
            type: 'POST',
            dataType: 'json',
            data: $.param(Document.param),
            success: function(d) {
                console.log(d);
                $('#pagination').html(d.pagination);
                if(d.total < 10){ $('#start').text(d.total); }                    
                $('#nums').text(d.total);
                Document.param.numpage = d.numpage;                
                    
               if(d.sisa > 0) {
                    var temp = '';
                    temp += '<div class="row"><div class="col-lg-12">';
                    temp += '<div class="well-info well-sm"> ';
                    temp += '<span class="fa fa-info-circle fa-lg"></span>&nbsp;&nbsp;Tersisa '+d.sisa+' parameter belum diisikan ';
                    temp += '</div></div></div> <br/>';
                    $('.sisa').html(temp);                   
                }

                var t = '', dt = {}, no = Document.param.curpage * Document.param.dataperpage;
                for (var i = 0; i < d.data.length; i++) {
                    dt = d.data[i];                    
                    var status = (dt.status_pelanggaran == '1') ? 'class="danger"' : '';
                    
                    t += '<tr '+ status +'>';
                    t += '<td class="text-center">'+(no+=1)+'</td>';
                    t += '<td>'+dt.nama_parameter+'</td>';
                    // t += '<td>'+dt.satuan+'</td>';
                    t += '<td>'+dt.nilai_baku_mutu+'</td>';
                    t += '<td>'+$(this).add_simbol(dt.nilai_hasil_uji, dt.nilai_baku_mutu)+'</td>';                    
                    t += '<td>'+((dt.metode_pengujian) ? dt.metode_pengujian : '-')+'</td>';                
                    // t += '<td class="text-right">';
                    // t += '<div class="btn-group">';
                    // t += '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i></button>';
                    // t += '<ul class="dropdown-menu pull-right" role="menu">';                   
                    // t += '<li><a onclick="$(this).edit('+dt.id_detail_lhu+')">Edit Data</a></li>';
                    // t += '<li><a onclick="$(this).delete('+dt.id_detail_lhu+')">Delete Data</a></li>';
                    // t += '</ul></div></td>';
                    t += '</tr>';                        
                }
                
                $('#document-data').html(t);
            }
        })
        .fail(function(e) {
            console.log(e);
        });            
    }
}    

$(function() {   

    $.fn.detail_lhu = function(id) {
        $.ajax({
            url: "<?php echo base_url('/backend/detail_lhu/get_baku_mutu'); ?>",
            type: 'POST',
            dataType: 'json',
            data: 'id_laporan_hasil_uji=' + id,
            success: function(d) {
                // console.log(d);             
                var h = '';                
                $.each(d, function(k, v) {
                    h += '<div class="col-lg-4" style="margin-bottom: 10px;"> ';
                    h += '<label>'+v.ket+'</label> ';
                    h += '<div class="input-group input-group-sm" style="margin-bottom: 10px;"> ';

                    var nilai_uji = (v.nilai_uji) ? 'value="'+ v.nilai_uji +'"' : '';
                    var metode = (v.metode) ? 'value="'+ v.metode +'"' : '';
                    var id_param = (v.id_baku_mutu_izin) ? v.id_baku_mutu_izin : v.id_parameter_lhu;

                    h += '<input type="text" name="param_lhu['+id_param+']" class="form-control numberonly" '+nilai_uji+' placeholder="0" /> ';
                    h += '<span class="input-group-addon">'+v.satuan+'</span> ';                        
                    h += '</div>';
                    h += '<input type="text" style="font-size: small;" class="form-control" name="metode['+id_param+']" '+metode+' placeholder="Metode pengujian '+v.ket+'..">';
                    h += '</div>';                              
                });                 

                $('#parameter').html(h);
                $('input[id=id_laporan_hasil_uji]').val(id);                 

                if(h != '') { $('#detail_lhu').modal('show'); }
                else{ $('#alert_nothing').modal('show'); }
            }
        })
        .fail(function() {
            console.log("error");
        });        
    }
        
    $.fn.delete = function(id) {
        $('input[id=deleted_id]').val(id);
        $('#delete').modal('show');
    } 

    $.fn.edit = function(id) {
        $.ajax({
            url: "<?php echo base_url('/backend/detail_lhu/get'); ?>",
            type: 'POST',
            dataType: 'json',
            data: 'id_detail_lhu=' + id,
            success: function(d) {
                console.log(d);      
                $('#satuan').html(d.satuan);
                $('#nama_parameter').html('Paremeter ' + d.nama_parameter);
                $('input[id=edit_id]').val(d.id_detail_lhu);
                $('input[name=nilai_hasil_uji]').val(d.nilai_hasil_uji);
                $('input[name=metode_pengujian]').val(d.metode_pengujian);
            }
        })
        .fail(function() {
            console.log("error");
        });
        $('#edit_parameter').modal('show');     
    }

    Document.search();
});
</script>

<!-- Modal -->
<div class="modal fade" id="detail_lhu" role="dialog">
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Parameter Hasil Uji</h4>
            </div>
            <form action="<?php echo base_url('/backend/detail_lhu/'.$action); ?>" method="post">
                <div class="modal-body"> 
                    <div class="row" id="parameter"></div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="id_laporan_hasil_uji" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="alert_nothing" role="dialog">
    <div class="modal-dialog" style="width: 600px;">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Konfirmasi Parameter</h4>
            </div>            
            <div class="modal-body"> 
                <p>Usaha/Kegiatan ini tidak memiliki baku mutu izin</p>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>                
            </div>            
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="edit_parameter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary no-border">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Ubah Nilai Hasil Uji</h4>
            </div>
            <form role="form" action="<?php echo base_url('/backend/detail_lhu/edit') ?>" method="post" id="form_add">
                <div class="modal-body">                     

                    <div class="form-group status">
                        <label id="nama_parameter"></label>
                        <div class="row">
                            <div class="col-lg-6">                                
                                <div class="input-group input-group-sm">                                
                                    <input type="text" name="nilai_hasil_uji" id="nilai_hasil_uji" class="form-control" placeholder="0" />                                
                                    <span class="input-group-addon" id="satuan"></span>
                                </div> 
                            </div>
                            <div class="col-lg-6">                               
                                <input type="text" name="metode_pengujian" id="metode_pengujian" class="form-control" placeholder="metode pengujian" />                                
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="edit_id" name="id" value="" />
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;&nbsp;Tutup</button>
                    <button type="submit" name="register" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;&nbsp;Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>