<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class industri extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('m_industry', '', TRUE);
		$this->load->model('m_industry_type', '', TRUE);
		$this->load->model('m_business_activity', '', TRUE);
		$this->load->model('m_kecamatan', '', TRUE);
		$this->load->model('m_kelurahan', '', TRUE);
		$this->load->model('m_data_master', '', TRUE);

        // $this->load->library('tank_auth');
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    # default view

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types        
		$jenis_industri = $this->m_industry_type->get_dropdown();
		$usaha_kegiatan = $this->m_business_activity->get_dropdown();		
		$kecamatan = $this->m_kecamatan->get_dropdown();		
		$kelurahan = $this->m_kelurahan->get_dropdown();
		$badan_usaha = $this->m_data_master->get_detail_data_master(array('jenis_data_master' => 'badan_usaha', 'status' => 1));	
		
		# response paramter     
		$data['jenis_industri'] = $jenis_industri;
		$data['usaha_kegiatan'] = $usaha_kegiatan;
		$data['kecamatan'] = $kecamatan;
		$data['kelurahan'] = $kelurahan;
		$data['badan_usaha'] = $badan_usaha;
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_industry', $data);
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		// huda : remove 'keyword' (4 Feb 2015, 17:41)
		// $input = array('dataperpage', 'query', 'keyword', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

       	$where = array(
					'i.id_industri' => array('SEARCH', 'OR', $query),
					'u.ket' => array('SEARCH', 'OR', $query),
					'j.ket' => array('SEARCH', 'OR', $query),			
					'k.ket' => array('SEARCH', 'OR', $query),
					'l.ket' => array('SEARCH', 'OR', $query),
					'i.nama_industri' => array('SEARCH', 'OR', $query),
					'i.badan_hukum' => array('SEARCH', 'OR', $query),
					'i.alamat' => array('SEARCH', 'OR', $query)
					// huda: edit-> disable by request (4 Feb 2015, 19:45)
					// 'i.status' => array('', 'AND', 1)
				);     

     	$total = $this->m_industry->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_industry->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_industry->get_industry_by_id($this->input->post('id_industri')));
		exit();
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		if ($this->input->post('badan_hukum') == "") {
			$badan_hukum = '';
		} else {
			$badan_hukum = $this->input->post('badan_hukum');
		}
		
		$param = array(
			'badan_hukum' => $badan_hukum,
			'nama_industri' => $this->input->post('nama_industri'),
			'hari_kerja_bulan' => $this->input->post('hari_kerja'),
			'jam_kerja_hari' => $this->input->post('jam_kerja'),
			'luas_area' => $this->input->post('luas_area'),
			'luas_bangunan' => $this->input->post('luas_bangunan'),
			'pimpinan' => $this->input->post('pimpinan'),
			'tlp' => $this->input->post('tlp'),
			'email' => $this->input->post('email'),
			'alamat' => $this->input->post('alamat'),
			'fax' => $this->input->post('fax'),
			'id_jenis_industri' => $this->input->post('jenis_industri'),
			'id_usaha_kegiatan' => $this->input->post('usaha_kegiatan'),
			'id_kecamatan' => $this->input->post('nama_kecamatan'),
			'id_kelurahan' => $this->input->post('nama_kelurahan'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_industry->change_industry($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect('backend/industri');
	}
	
	public function register() {
		if($this->validate()) {
			if ($this->input->post('badan_hukum') == "") {
				$badan_hukum = '';
			} else {
				$badan_hukum = $this->input->post('badan_hukum');
			}
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'badan_hukum' => $badan_hukum,
				'nama_industri' => $this->input->post('nama_industri'),
				'hari_kerja_bulan' => $this->input->post('hari_kerja'),
				'jam_kerja_hari' => $this->input->post('jam_kerja'),
				'luas_area' => $this->input->post('luas_area'),
				'luas_bangunan' => $this->input->post('luas_bangunan'),
				'pimpinan' => $this->input->post('pimpinan'),
				'tlp' => $this->input->post('tlp'),
				'email' => $this->input->post('email'),
				'alamat' => $this->input->post('alamat'),
				'fax' => $this->input->post('fax'),
				'id_jenis_industri' => $this->input->post('jenis_industri'),
				'id_usaha_kegiatan' => $this->input->post('usaha_kegiatan'),
				'id_kecamatan' => $this->input->post('nama_kecamatan'),
				'id_kelurahan' => $this->input->post('nama_kelurahan'),
				'tgl_pembuatan' => $tgl_pembuatan,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'diupdate_oleh' => $this->tank_auth->get_personname(),
				'tgl_update' => $tgl_update
			);
			
			if($this->m_industry->insert_industry($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		redirect('backend/industri');
	}
	
	public function delete() {
		$this->m_industry->delete_industry($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/industri');
	}
	
	public function validate() {
		# check data yang duplicate
		if($this->m_industry->is_duplicate($this->input->post('badan_hukum'), $this->input->post('nama_industri'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect('backend/industri');
		}

		return true;
	}

	public function search() {
        $keyword = $_POST['query'];
        
        $data = $this->m_industry->search_industry($keyword);
        echo json_encode($data);
    }

    public function search_industri_bap($id_usaha_kegiatan) {
        $keyword = $_POST['query'];
        
        $data = $this->m_industry->search_industry_bap($keyword, $id_usaha_kegiatan);
        echo json_encode($data);
    }

    public function ajax_get_id() {

		$data['list'] = $this->m_industry->get_ajax_industri();
		$data['detail_tps'] = $this->m_industry->get_detail_tps();
		if($data['detail_tps']!=null){
			foreach($data['detail_tps'] as $row){
				$jenis_limbah = ($row->jenis_limbah) ? unserialize($row->jenis_limbah) : array();
				$jl = '';
				foreach ($jenis_limbah as $key) {
					$jl .= $key.' / ';
				}
	        	
	        	$newJl = rtrim($jl, " /");
	        	$row->jenis_limbah = $newJl;
			}
		}
		$data['detail_air_limbah'] = $this->m_industry->get_detail_air_limbah();
		if($data['detail_air_limbah']!=null){
			foreach($data['detail_air_limbah'] as $row){
				$sumber_limbah = ($row->sumber_limbah) ? unserialize($row->sumber_limbah) : array();
	        	$row->sumber_limbah = $sumber_limbah;
			}
		}
		$id_industri = $this->input->post('id_industri');
		$data['jml_tps'] = $this->m_industry->count_jml_tps_industri($id_industri);
		
		echo json_encode($data);
		exit();
	}

	/** 
	 * search industry based on keyword in advance search
	 * 4 Feb 2015, 17:29
	 *
	 * @author huda.jtk09@gmail.com
	 * @return array
	 */
	public function get_list_advanced() {
    	# get parameter pagination
		$input = array('dataperpage', 'keyword', 'curpage');
		
		foreach ($input as $val) {
     		$$val = $this->input->post($val); 
     	}

     	$keyword_list = explode(";", $keyword);
     	
     	if (!empty(trim($keyword_list[0])) && $keyword_list[0]!=='null') {
     		$where['i.nama_industri'] = array('SEARCH', 'AND', $keyword_list[0]);
     	}
     	if (!empty(trim($keyword_list[1])) && $keyword_list[1]!=='null') {
     		$where['i.id_usaha_kegiatan'] = array('SEARCH', 'AND', $keyword_list[1]);
     	}
     	if (!empty(trim($keyword_list[2])) && $keyword_list[2]!=='null') {
     		$where['i.id_jenis_industri'] = array('SEARCH', 'AND', $keyword_list[2]);
     	}
     	if (!empty(trim($keyword_list[3])) && $keyword_list[3]!=='null') {
     		$where['i.pimpinan'] = array('SEARCH', 'AND', $keyword_list[3]);
     	}
     	if (!empty(trim($keyword_list[4])) && $keyword_list[4]!=='null') {
     		$where['i.alamat'] = array('SEARCH', 'AND', $keyword_list[4]);
     	}
     	if (!empty(trim($keyword_list[5])) && $keyword_list[5]!=='null') {
     		$where['i.id_kecamatan'] = array('SEARCH', 'AND', $keyword_list[5]);
     	}
     	if (!empty(trim($keyword_list[6])) && $keyword_list[6]!=='null') {
     		$where['i.id_kelurahan'] = array('SEARCH', 'AND', $keyword_list[6]);
     	}

     	// huda: edit-> disable by request (4 Feb 2015, 19:45)
      	// $where['i.status'] = array('', 'AND', 1);     

     	$total = $this->m_industry->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_industry->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
}
