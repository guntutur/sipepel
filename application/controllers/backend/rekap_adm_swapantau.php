<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class rekap_adm_swapantau extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_rekap_adm');
        $this->load->model('m_jenis_dokumen');
		
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');	
		$this->load->config('tank_auth', TRUE);
    }

    public function index() {
    	# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_rekap_adm');
        $this->load->view('include/footer');
    } 

    public function get_list() {
        # get parameter pagination
        $input = array('dataperpage', 'query', 'curpage');
        foreach ($input as $val)
            $$val = $this->input->post($val); 

        $where = '';
        // $where = array(
        //         'i.nama_industri' => array('SEARCH', 'OR', $query),
        //         'b.nama_lab' => array('SEARCH', 'OR', $query),
        //         'l.laporan_bulan_tahun' => array('SEARCH', 'OR', $query),
        //     );

        $total = $this->m_rekap_adm->get_num_rows($where);
        $npage = ceil($total / $dataperpage);

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $types = $this->rekapitulasi_adm($this->m_rekap_adm->get_all($where, $dataperpage, $start));
        $data = array(
            'data' => $types,
            'pagination' => '',
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

        echo json_encode($data);
        exit();
        
    }

    public function rekapitulasi_adm($periode_adm) {
        
        $data = array();
                
        foreach ($periode_adm as $k => $v) {

            $data[$v->id_industri]['nama_industri'] = $v->nama_industri;
            
            # extract kualitas air            
            $temp_catatan = array();               
            $laporan = explode(';', $v->kualitas_air);
            foreach ($laporan as $key => $value) {                    
                $temp = explode('.', $value);                
                $temp_catatan[] = $temp[0];                
            } 
            // $detail_dok = $this->m_jenis_dokumen->get_jenis_dokumen_by_id(1);
            $data[$v->id_industri]['kualitas_air'] = $this->functions->check_periode($temp_catatan, 'triwulan');                    
            // print_r($data[$v->id_industri]['kualitas_air']);

            # extract catatan debit           
            $temp_catatan = array();
            $laporan = explode(';', $v->catatan_debit);
            foreach ($laporan as $key => $value) {                    
                // $temp = explode(':', $value);
                // $temp_catatan[] = (isset($temp[1]) and ($temp[1] > 0)) ? (substr($temp[0], 0, 2)) : '';               
                $temp = explode('.', $value);                
                $temp_catatan[] = $temp[0];
            } 
            // $detail_dok = $this->m_jenis_dokumen->get_jenis_dokumen_by_id(2);
            $data[$v->id_industri]['catatan_debit'] = $this->functions->check_periode($temp_catatan, 'triwulan');          
                        
            # extract kualitas udara emisi            
            $temp_catatan = array();
            $laporan = explode(';', $v->kualitas_emisi);
            foreach ($laporan as $key => $value) {                    
                $temp = explode('.', $value);
                $temp_catatan[] = $temp[0];
            }
            // $detail_dok = $this->m_jenis_dokumen->get_jenis_dokumen_by_id(2);
            $data[$v->id_industri]['kualitas_emisi'] = $this->functions->check_periode($temp_catatan, 'semester');            
            
            # extract kualitas ambien
            $temp_catatan = array();
            $laporan = explode(';', $v->kualitas_ambien);
            foreach ($laporan as $key => $value) {
                $temp = explode('.', $value);
                $temp_catatan[] = $temp[0];
            }
            $data[$v->id_industri]['kualitas_ambien'] = $this->functions->check_periode($temp_catatan, 'semester');            

            # extract kualitas ambien
            $temp_catatan = array();
            $laporan = explode(';', $v->manifest);
            foreach ($laporan as $key => $value) {
                $temp = explode('.', $value);
                $temp_catatan[] = $temp[0];
            }
            $data[$v->id_industri]['manifest'] = $this->functions->check_periode($temp_catatan, 'triwulan');

            # extract kualitas ambien
            $temp_catatan = array();
            $laporan = explode(';', $v->neraca_b3);
            foreach ($laporan as $key => $value) {
                $temp = explode('.', $value);
                $temp_catatan[] = $temp[0];
            }
            $data[$v->id_industri]['neraca_b3'] = $this->functions->check_periode($temp_catatan, 'triwulan');
        }     

        return $data;
    }     
}
