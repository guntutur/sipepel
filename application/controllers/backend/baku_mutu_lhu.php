<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class baku_mutu_lhu extends CI_Controller {

    function __construct() {
        parent::__construct();	

        # get database
        $this->load->model('m_lhu_parameter', '', TRUE);
		$this->load->model('m_jenis_dokumen', '', TRUE);
		$this->load->model('m_industry_type', '', TRUE);
		$this->load->model('m_business_activity', '', TRUE);
        
        $this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
    }

    public function index($act='list') {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');        

		# get all types    
		$jenis_industri = $this->m_industry_type->get_dropdown();
		$usaha_kegiatan = $this->m_business_activity->get_dropdown();	
				
		# response paramter
		$data['jenis_industri'] = $jenis_industri;
		$data['usaha_kegiatan'] = $usaha_kegiatan;
		
		# load view
        $this->load->view('include/header');
        $this->load->view('backend/v_lhu_parameter', $data);
        $this->load->view('include/footer');
    }

    public function get_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage');
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

		$where = "p.status = 0 AND (q.ket LIKE '%". $query ."%' OR u.ket LIKE '%". $query ."%' OR p.ket LIKE '%". $query ."%' OR p.dasar_hukum_internal LIKE '%". $query ."%' OR p.dasar_hukum_eksternal LIKE '%". $query ."%' OR p.jenis_lhu LIKE '%". $query ."%')";	

     	$total = $this->m_lhu_parameter->get_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_lhu_parameter->get_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }
	
	public function get() {		
		echo json_encode($this->m_lhu_parameter->get_lhu_parameter_by_id($this->input->post('id_parameter_lhu')));
		exit();
	}
	
	public function edit() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(
			'id_usaha_kegiatan' => $this->input->post('usaha_kegiatan'),
			'id_jenis_industri' => $this->input->post('jenis_industri'),
			'dasar_hukum_internal' => $this->input->post('hukum_internal'),
			'dasar_hukum_eksternal' => $this->input->post('hukum_eksternal'),
			'jenis_lhu' => $this->input->post('jenis_lhu'),
			'status' => 0,
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		$baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($this->input->post('id'));
		$usaha_kegiatan = $baku_mutu->id_usaha_kegiatan;
		$jenis_industri = $baku_mutu->id_jenis_industri;
		$jenis_lhu = $baku_mutu->jenis_lhu;

		$param2 = array(
			'id_usaha_kegiatan' => $this->input->post('usaha_kegiatan'),
			'id_jenis_industri' => $this->input->post('jenis_industri'),			
			'jenis_lhu' => $this->input->post('jenis_lhu'),
			'diupdate_oleh' => $this->tank_auth->get_personname(),
			'tgl_update' => $tgl_update
		);
		
		if($this->m_lhu_parameter->change_lhu_parameter($param, $this->input->post('id'))){
			$this->m_lhu_parameter->change_lhu_parameter2($param2, $usaha_kegiatan, $jenis_industri, $jenis_lhu);
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		}else{
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
		}
			
		redirect('backend/baku_mutu_lhu');
	}
	
	public function register() {
		if($this->validate()) {
			date_default_timezone_set("Asia/Jakarta");
			$tgl_pembuatan = date("Y-m-d H:i:s:");
			$tgl_update = date("Y-m-d H:i:s:");
			$param = array(
				'id_usaha_kegiatan' => $this->input->post('usaha_kegiatan'),
				'id_jenis_industri' => $this->input->post('jenis_industri'),
				'dasar_hukum_internal' => $this->input->post('hukum_internal'),
				'dasar_hukum_eksternal' => $this->input->post('hukum_eksternal'),
				'jenis_lhu' => $this->input->post('jenis_lhu'),
				'status' => 0,
				'dibuat_oleh' => $this->tank_auth->get_personname(),
				'tgl_pembuatan' => $tgl_update
			);
			
			if($this->m_lhu_parameter->insert_lhu_parameter($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}
		
		redirect('backend/baku_mutu_lhu');
	}
	
	public function delete() {
		$baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($this->input->post('id'));
		$usaha_kegiatan = $baku_mutu->id_usaha_kegiatan;
		$jenis_industri = $baku_mutu->id_jenis_industri;
		$jenis_lhu = $baku_mutu->jenis_lhu;
		
		$this->m_lhu_parameter->delete_lhu_parameter($usaha_kegiatan, $jenis_industri, $jenis_lhu);
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect('backend/baku_mutu_lhu');
	}
	
	public function detail() {
		# check login
        if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

        $id = $this->uri->segment(4);
        $baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($id);


        $usaha_kegiatan = $baku_mutu->usaha_kegiatan;
        if ($baku_mutu->nama_industri != null ){
			$jenis_industri = $baku_mutu->nama_industri;
		} else {
			$jenis_industri = "-";
		}
        $jenis_lhu = $baku_mutu->jenis_lhu;

        $data['title'] = "PARAMETER BAKU MUTU ".$jenis_lhu;
		$data['sub_title'] = "Jenis Usaha/Kegiatan : <b>".$usaha_kegiatan."</b>, Jenis/Kelas/Tipe : <b>".$jenis_industri."</b>";

        # load view
        $this->load->view('include/header');
        $this->load->view('backend/v_lhu_parameter_detail', $data);
        $this->load->view('include/footer');
	}
	
	public function get_detail() {		
		echo json_encode($this->m_lhu_parameter->get_detail_parameter_by_id($this->input->post('id_parameter_lhu')));
		exit();
	}

	public function get_detail_list() {
    	# get parameter pagination
		$input = array('dataperpage', 'query', 'curpage', 'id');
			
		foreach ($input as $val)
     		$$val = $this->input->post($val); 

     	$where = "(status != 0 AND id_jenis_industri = (select id_jenis_industri from parameter_lhu where id_parameter_lhu = '".$id."') 
					AND id_usaha_kegiatan = (select id_usaha_kegiatan from parameter_lhu where id_parameter_lhu = '".$id."') AND jenis_lhu = (select jenis_lhu from parameter_lhu where id_parameter_lhu = '".$id."') AND ket LIKE '%". $query ."%' )";
    
     	$total = $this->m_lhu_parameter->get_detail_num_rows($where);
    	$npage = ceil($total / $dataperpage);

    	$start = $curpage * $dataperpage;    	
	    $end = $start + $dataperpage;

	    $types = $this->m_lhu_parameter->get_detail_all($where, $dataperpage, $start);
	    $data = array(
	      	'data' => $types,
	      	'pagination' => '',
	      	'numpage' => $npage - 1,
	      	'total' => $total
	    );

	    $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

	    echo json_encode($data);
		exit();
     	
    }

	public function insert_detail() {
		
		if($this->validate_detail()) {
			$id_par = $this->input->post('id');
			$baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($id_par);
			$usaha_kegiatan = $baku_mutu->id_usaha_kegiatan;
			$jenis_industri = $baku_mutu->id_jenis_industri;
			$jenis_lhu = $baku_mutu->jenis_lhu;
			$dh_internal = $baku_mutu->dasar_hukum_internal;
			$dh_eksternal = $baku_mutu->dasar_hukum_eksternal;
			
			$param = array(
					'ket' => $this->input->post('ket'),
					'satuan' => $this->input->post('satuan'),
					'nilai_parameter_lhu_internal' => $this->input->post('nilai_internal'),
					'nilai_parameter_lhu_eksternal' => $this->input->post('nilai_eksternal'),
					'id_usaha_kegiatan' => $usaha_kegiatan,
					'id_jenis_industri' => $jenis_industri,
					'jenis_lhu' => $jenis_lhu,
					'dasar_hukum_internal' => $dh_internal,
					'dasar_hukum_eksternal' => $dh_eksternal,
					'status' => $this->input->post('status')
				);

			if($this->m_lhu_parameter->insert_detail($param))
				$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil disimpan'));
			else
				$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal disimpan'));
		}		
	
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function edit_detail() {
		date_default_timezone_set("Asia/Jakarta");
		$tgl_update = date("Y-m-d H:i:s:");
		$param = array(			
				'ket' => $this->input->post('ket'),
				'satuan' => $this->input->post('satuan'),
				'nilai_parameter_lhu_internal' => $this->input->post('nilai_internal'),
				'nilai_parameter_lhu_eksternal' => $this->input->post('nilai_eksternal'),
				'status' => $this->input->post('status')
			);
		
		if($this->m_lhu_parameter->change_detail_parameter($param, $this->input->post('id')))
			$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil diperbaharui'));
		else
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data gagal diperbaharui'));
			
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_detail() {
		$this->m_lhu_parameter->delete_detail_parameter($this->input->post("id"));
		
		$this->session->set_flashdata('msg', $this->functions->build_message('success', 'Data berhasil dihapus'));
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	public function validate() {
		# check data yang duplicate
		if($this->m_lhu_parameter->is_duplicate($this->input->post('usaha_kegiatan'), $this->input->post('jenis_industri'), $this->input->post('jenis_lhu'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect('backend/baku_mutu_lhu');
		}

		return true;
	}
	
	public function validate_detail() {
		# check data yang duplicate
			$id_par = $this->input->post('id');
			$baku_mutu = $this->m_lhu_parameter->get_lhu_parameter_detail_by_id($id_par);
			$usaha_kegiatan = $baku_mutu->id_usaha_kegiatan;
			$jenis_industri = $baku_mutu->id_jenis_industri;
			$jenis_lhu = $baku_mutu->jenis_lhu;
			
		if($this->m_lhu_parameter->is_duplicate_detail($usaha_kegiatan, $jenis_industri, $jenis_lhu, $this->input->post('ket'))) {
			$this->session->set_flashdata('msg', $this->functions->build_message('danger', 'Data sudah tersedia'));
			redirect($_SERVER['HTTP_REFERER']);
		}

		return true;
	}
}
