<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class teguran_adm extends CI_Controller {

	public function __construct() {
		parent::__construct();		

		$this->load->model('m_teguran_adm');
     
		$this->load->library('tank_auth_groups', '', 'tank_auth');
        $this->lang->load('tank_auth');
		
		$this->load->config('tank_auth', TRUE);
	}

	public function index() {
		if (!$this->tank_auth->is_logged_in()) redirect('/auth/login/');

		# load view
        $this->load->view('include/header');        
        $this->load->view('backend/v_teguran_adm');
        $this->load->view('include/footer');
	}

	public function get_list() {
        # get parameter pagination
        $input = array('dataperpage', 'query', 'curpage', 'tahun');
        foreach ($input as $val)
            $$val = $this->input->post($val); 
     
        // $where = "`al`.`jenis_lhu` = 'LHU Udara Emisi' AND (`i`.`nama_industri` LIKE '%".$query."%' OR `l`.`nama_lab` LIKE '%".$query."%' OR `al`.`no_lhu` LIKE '%".$query."%' OR `al`.`laporan_bulan_tahun` LIKE '%".$query."%')";
        $where = '';       

        $total = $this->m_teguran_adm->get_num_rows($where, (string)$tahun);        
        $npage = ceil($total / $dataperpage);     

        $start = $curpage * $dataperpage;       
        $end = $start + $dataperpage;

        $types = $this->m_teguran_adm->get_all($where, $dataperpage, $start, (string)$tahun);
        $data = array(
            'data' => $types,
            'pagination' => '',
            'numpage' => $npage - 1,
            'total' => $total
        );

        $data['pagination'] = $this->functions->create_links($npage, $curpage, 3);

        echo json_encode($data);
        exit();
        
    }

    public function generate_teguran($id_industri) {

        $where = ' where i.id_industri = '.$id_industri;
        $data = $this->m_teguran_adm->get_all($where, 1, 0);            

        // $this->functions->debug_var($data);
 
        $bulan = $this->functions->get_month((integer)date('m'));

        $file = file_get_contents(site_url('assets/form/surat_teguran_periode.rtf'));

        # header
        $file = str_replace("#nomor_surat", "", $file);
        $file = str_replace("#tanggal_hari_ini", date("d").' '.$bulan.' '.date('Y'), $file);
        $file = str_replace("#nama_industri", $data[0]->nama_industri, $file);
        $file = str_replace("#tempat", $data[0]->alamat, $file);

        # kualitas air
        $file = str_replace("#langgar_air", $this->convert_name($data[0]->kualitas_air), $file);  
        $file = str_replace("#langgar_catatan", $this->convert_name($data[0]->catatan_debit), $file);
        $file = str_replace("#langgar_ambien", $this->convert_name($data[0]->kualitas_ambien), $file);
        $file = str_replace("#langgar_emisi", $this->convert_name($data[0]->kualitas_emisi), $file);
        $file = str_replace("#langgar_neraca", $this->convert_name($data[0]->neraca_b3), $file);
        $file = str_replace("#langgar_manifest", $this->convert_name($data[0]->manifest), $file);

        header("Content-type: application/msword");
        header("Content-disposition: inline; filename=teguran_adm_".str_replace(" ", '_', strtolower($data[0]->nama_industri)).".doc");
        header("Content-length: ".strlen($file));

        echo $file;
    }

    protected function convert_name($data) {

        $tipe = explode(':', $data);        
        if(empty($tipe[1])) { return '-'; }

        $result = array();
        if(strpos($tipe[1], ',') !== FALSE) {
            $bulan = explode(',', $tipe[1]);
            foreach ($bulan as $key => $value) {
                $result[] = $this->functions->get_month((int)trim($value));
            }
        }else{
            $result[] = $this->functions->get_month((int)trim($tipe[1]));
        }
        
        return join(', ', $result);
    }

}